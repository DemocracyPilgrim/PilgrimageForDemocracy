<?php
$ok = true;
while ($ok && !file_exists('LICENSE.md')) {
	$ok = chdir('..');
}
require_once('include/preprocess.php');

$source_dir = '';

function create_wikipedia_section($link) {
	global $source_dir;
	global $wikipedia_body_parts;
	$summary = '';

	$link = urldecode($link);
	$titlePath = strstr($link, 'wiki/', false);
	$titlePath = substr($titlePath, 5);

	$titlePath = tidy_titlePath($titlePath);

	$title = str_replace("_", " ", $titlePath);;

	$content = file_get_contents($source_dir . 'templates/wikipedia.section.php');
	$content = str_replace("%link%", $link, $content);
	$content = str_replace("%titlePath%", $titlePath, $content);
	$content = str_replace("%title%", $title, $content);
	$content = str_replace("%summary%", $summary, $content);

	$wikipedia_body_parts[] = preg_replace('/%titlePath%/', $titlePath, "\$page->body(\$div_wikipedia_%titlePath%);\n");

	return  $content;
}
