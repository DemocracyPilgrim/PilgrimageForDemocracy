#!/usr/bin/php
<?php
$ok = true;
while ($ok && !file_exists('LICENSE.md')) {
	$ok = chdir('..');
}

require_once('include/preprocess.php');
include_once('include/init.php');
require_once('index/redirect.php');
require_once('index/src_by_change_time.php');


// Typical use:
// $ bin/mv.php src/primary_features_of_democracy.php src/first_level_of_democracy.php
// sed -i "s#primary features of democracy#first level of democracy#g" *.php


if (empty($argv[1])) {
	echo "Please provide the source file name as argument.\n";
	exit;
}
$src = $argv[1];

if (!isset($preprocess_index_src[$src])) {
	echo "The source article \"$src\" does not exist. Aborting.\n";
	exit;
}

if (empty($argv[2])) {
	echo "Please provide the destination file name as argument.\n";
	exit;
}
$dest = $argv[2];

if (isset($preprocess_index_src[$dest])) {
	echo "The destination article \"$dest\" already exists. Aborting.\n";
	exit;
}


$src_by_change_time[$dest] = $src_by_change_time[$src];
unset($src_by_change_time[$src]);
save_array('src_by_change_time', 'index/src_by_change_time.php');

$preprocess_index_src[$dest] = $preprocess_index_src[$src];
unset($preprocess_index_src[$src]);
$previousFileName = substr($preprocess_index_src[$dest]['dest'], 5);
$orig_dest = substr($preprocess_index_src[$dest]['dest'], 5);

$fileName = substr($dest, 4, -4);

$preprocess_index_src[$dest]['dest'] = 'http/' . $fileName . '.html';
$preprocess_index_dest[$orig_dest]['include'] = $dest;
$new_dest = $fileName . '.html';
$preprocess_index_dest[$new_dest] = $preprocess_index_dest[$orig_dest];
unset($preprocess_index_dest[$orig_dest]);


$command = "git mv -v $src $dest";

$redirect["/$previousFileName"] = $fileName . '.html';

$fileSRC = substr($src, 4, -4);
$fileDES = substr($dest, 4, -4);
$sed1 = "sed -i \"s#'${fileSRC}.html'#'${fileDES}.html'#g\"               ";
$sed2 = "sed -i \"s#'/${fileSRC}.html'#'/${fileDES}.html'#g\"             ";
$sed3 = "sed -i \"s#\\\"/${fileSRC}.html\\\"#\\\"/${fileDES}.html\\\"#g\" ";
$sed4 = "sed -i \"s#'${fileSRC}.html'#'${fileDES}.html'#g\" index/*.php";
echo "\nSed command:\n\t$sed1\n\t$sed2\n\t$sed3\n\t$sed4\n";

echo "\n$command\n\n";
echo "Moving:\t http/$previousFileName \nto:\t http/$fileName.html\n\n";
echo "Moving:\t $src \nto:\t $dest\n\n";



$proceed = (string)readline("Proceed? (Yes/No) ");
$yes = array (1, "1", 'y', 'Y', 'yes', 'Yes');

if (!in_array($proceed, $yes)) {
	echo "Aborted. Exiting.\n";
	exit;
}

exec ($command);
save_array('redirect', 'index/redirect.php');
update_preprocess_index();

foreach (glob("src/*.php") as $filename) {
	echo "$filename \n";
	shell_exec("touch -r '$filename' 'preserve_ctime_dummy_file'");
	shell_exec("$sed1 $filename");
	shell_exec("$sed2 $filename");
	shell_exec("$sed3 $filename");
	shell_exec("touch -r 'preserve_ctime_dummy_file' '$filename'");
}

shell_exec("rm 'preserve_ctime_dummy_file'");
shell_exec($sed4);
