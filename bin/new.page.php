#!/usr/bin/php
<?php
include_once('include.php');
require_once('include/preprocess.php');

require_once('include/new.article.php');
require_once('include/new.award.php');
require_once('include/new.book.php');
require_once('include/new.country.php');
require_once('include/new.documentary.php');
require_once('include/new.movie.php');
require_once('include/new.organisation.php');
require_once('include/new.page.php');
require_once('include/new.person.php');
require_once('include/new.report.php');
require_once('include/new.video.php');


function get_selection($prompt, $objects) {
	echo "\n\n";

	foreach ($objects AS $id => $object) {
		if (is_object($object)) {
			echo "[$id] " . $object->name() . "\n";
		}
		else {
			echo "[$id] " . $object . "\n";
		}
	}

	$selected = readline("$prompt   ");

	if (!is_numeric($selected)) {
		echo "Please select a valid entry! \n";
		return get_selection($prompt, $objects);
	}
	$selected = (int) $selected;

	if (!isset($objects[$selected])) {
		echo "Please select a valid entry! \n";
		return get_selection($prompt, $objects);
	}

	$objects[$selected]->set_id($selected);

	return $objects[$selected];
}


$types = array();
$types[] = new newArticle();
$types[] = new newAward();
$types[] = new newBook();
$types[] = new newCountry();
$types[] = new newDocumentary();
$types[] = new newMovie();
$types[] = new newOrganisation();
$types[] = new newPerson();
$types[] = new newReport();
$types[] = new newVideo();

$newPage = get_selection("What type?", $types);
$newPage->select_parent_page();
$newPage->select_tags();
$newPage->select_article_name();


$wikipedia_body_parts = array();


function get_wikipedia_sources() {

	$sections = '';
	while (true) {
		$wikipediaLink = (string)readline("Link to wikipedia article (leave empty to quit): ");
		if (!$wikipediaLink) {
			break;
		}
		// $wikipedia_body_parts will be populated there.
		$wikipediaSection = create_wikipedia_section($wikipediaLink);
		$sections .= $wikipediaSection;
	}
	return $sections;
}




$directory = "";
if (!empty($argv[1])) {
	$directory = $argv[1];
}



$fileName = strtolower($newPage->article_name());
$fileName = str_replace(" ", "_", $fileName);
$fileName = tidy_titlePath($fileName);

$src  = 'src/' . $directory . $fileName . ".php";
$dest = 'http/' . $directory . $fileName . ".html";

if (isset($preprocess_index_src[$src])) {
	echo "The file \"$src\" already exists. Aborting.\n";
	exit;
}


$content = file_get_contents('templates/article.php');
$content = str_replace("%ArticleName%", $newPage->article_name(),         $content);
$content = str_replace("%alphaSort%",   $newPage->content_alphaSort(),    $content);
$content = str_replace("%newPage%",     $newPage->content_newPage(),      $content);
$content = str_replace("%Tag%",         $newPage->tags_as_string(),       $content);
$content = str_replace("%titlePath%",   $fileName,                        $content);

$content .= get_wikipedia_sources();

$body_parts = array();
$body_parts[] = "\n";
$body_parts[] = "\n";
$body_parts[] = "\$page->parent('" . $newPage->parent_page() . "');\n";
$body_parts[] = "\$page->template(\"stub\");\n";
$body_parts[] = "\$page->body(\$div_introduction);\n";
$body_parts[] = "\n";
$body_parts[] = "\n";
$body_parts[] = "\n";
$body_parts[] = "\$page->related_tag(\"" . $newPage->article_name() . "\");\n";
$body_parts   = array_merge($body_parts, $newPage->extra_body_parts());

foreach ($body_parts AS $line) {
	$content .= $line;
}
foreach ($wikipedia_body_parts AS $line) {
	$content .= $line;
}


file_put_contents($src, $content);
chmod($src, 0755);


echo "\n";
echo $src;
echo "\n";

$preprocess_index_src[$src] = array(
	'dest' => $dest,
	'API' => '1',
);

$dest = $fileName . ".html";
$preprocess_index_dest[$directory . $dest] = array(
	'include' => $src,
	'name' => 'page',
	'keyword' => $newPage->article_name(),
);

update_preprocess_index();
