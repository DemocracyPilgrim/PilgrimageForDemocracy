#!/usr/bin/php
<?php
require_once('include/preprocess.php');
require_once('include/init.php');
global $preprocess_index_src;

function remove_trailing_empty_lines(&$out) {
	while (true) {
		end($out);
		$key = key($out);
		$line = current($out);
		if ($line == "\n") {
			unset($out[$key]);
		}
		else {
			break;
		}
	}
}

function clean_section($name, $include) {
	$content = file($include);

	$out = array();
	$append = true;
	foreach ($content AS $nb => $line) {
		if (preg_match("#\\\$$name = (.*)#", $line)) {
			$append = false;
		}

		if (preg_match("/\tHTML;/", $line)) {
			if (!$append) {
				$append = true;
				continue;
			}
		}


		if ($append) {
			$out[] = $line;
		}
	}

	remove_trailing_empty_lines($out);
	$page = '';
	foreach ($out AS $line) {
		$page .= $line;
	}
	file_put_contents($include, $page);
}

$options = '';
$test = false;
$src = '';

if (!isset($argv[1])) {
	echo "Which file?\n";
	exit;
}

if ($argv[1] == "test") {
	$test = true;
	$src = $argv[2];
}
else {
	$src = $argv[1];
}

if (!isset($preprocess_index_src[$src])) {
	echo "	!! The source ${src} is not valid.\n";
	exit;
}

if (isset($preprocess_index_src[$src]['API'])) {
	echo "	!! The source ${src} is already upgraded to API 1.\n";
	exit;
}

$content = file($src);
$dest = substr($preprocess_index_src[$src]['dest'], 5);

if ($content[1] != '$page = new Page();') {
	$content = array_merge(array_slice($content, 0, 1), array("\$page = new Page();\n"), array_slice($content, 1));
}

$out = array();
$section_preview_name = '';

foreach ($content AS $nb => $line) {

	// Temporarily keep until everything is migrated:
	//         include_once('section/something.php');
	$pattern = "@include_once\('(section/(.*).php)'\);@";
	if (preg_match($pattern, $line, $matches)) {
		$pattern = '/include_once/';
		$replacement = 'include';
		$line = preg_replace($pattern, $replacement, $line);

		// Many old pages are not listed in $preprocess_index_dest!
		// We need to initialize it here.
		if (!isset($preprocess_index_dest[$dest])) {
			$preprocess_index_dest[$dest] = array(
				'name' => 'todo',
				'include' => $matches[1],
			);
			// $dest = 'something.html'
			// => $section = 'div_section_something';
			$section = 'div_section_' . substr($dest, 0, -5);
			include $preprocess_index_dest[$dest]['include'];
			if (isset($section)) {
				$preprocess_index_dest[$dest]['name'] = $section;
				$section_preview_name = $section;
			}
			else {
				echo "$section\n";
				print_r($preprocess_index_dest[$dest]);
				echo "WIP\n";
				exit;
			}
		}
		$out[] = $line;
		continue;
	}


	//    $snp['description'] = "something";
	// => $page->snp('description', "something");
	$pattern = '/\$snp\[\'description\'\] = (.*);/';
	$replacement = '\$page->snp(\'description\', $1);';
	$line = preg_replace($pattern, $replacement, $line);


	//    $snp['image'] = "something";
	// => $page->snp('image',       'something');
	$pattern = '/\$snp\[\'image\'\] = (.*);/';
	$replacement = '\$page->snp(\'image\', $1);';
	$line = preg_replace($pattern, $replacement, $line);


	// Remove:
	//         include() and include_once().
	if (substr($line, 0, 7)  == 'include') {
		continue;
	}


	//    $h1['en'] = 'Title';
	// => $page->h1('Title');
	if (substr($line, 0, 12)  == '$h1[\'en\'] = ') {
		$pattern = '/\$h1\[\'en\'] = (.*);/';
		preg_match($pattern, $line, $matches);
		$title = $matches[1];
		$replacement = '$page->h1($1);';
		$out[] = preg_replace($pattern, $replacement, $line);

		// Add page preview from stored section object/array.
		include $preprocess_index_dest[$dest]['include'];
		$section = ${$preprocess_index_dest[$dest]['name']};
		$section_preview_name = $preprocess_index_dest[$dest]['name'];
		if (is_array($section)) {
			$preview = $section['en'];
			$preview_lines = explode("\n", $preview);
			if ($preview_lines[1] == "") {
				unset($preview_lines[1]);
			}
			unset($preview_lines[0]);
		}
		else if (is_object($section)) {
			$preview = $section->content;
			$preview_lines = explode("\n", $preview);
			if ($preview_lines[1] == "") {
				unset($preview_lines[1]);
			}
			unset($preview_lines[0]);
		}
		else {
			$preview_lines = array();
		}


		// Add stars.
		if (is_array($section)) {
			$out[] = "\$page->stars(${section['stars']});\n";
		}
		else if (is_object($section)) {
			$stars = $section->getStars();
			$out[] = "\$page->stars(${stars});\n";
		}
		else {
			// A few pages (countries) are missing the $section altogether.
			echo "The section does not exist. Initializing starts at 0.\n";
			$out[] = "\$page->stars(0);\n";
		}


		// Add keywords.
		$out[] = "\$page->keywords($title);\n";
		$out[] = "\n";


		// Add preview section.
		$out[] = "\$page->preview( <<<HTML\n";
		foreach ($preview_lines AS $preview_line) {
			$out[] = "\t$preview_line\n";
		}

		$out[] = "	HTML );\n";

		continue;
	}

	//     $something = newSection();
	// =>  $something = new ContentSection();
	$pattern = '/\$(.*) = newSection\(\);/';
	$replacement = '\$$1 = new ContentSection();';
	$line = preg_replace($pattern, $replacement, $line);


	// Remove:
	//         $something['stars']   = -1;
	if (preg_match('/\$(.*)\[\'stars\'\]( *) = -1/', $line)) {
		continue;
	}


	// Remove:
	//         $something['class'][] = '';
	if (preg_match('/\$(.*)\[\'class\'\]\[\] = \'\';/', $line)) {
		continue;
	}


	//    $something['en'] = <<<HTML
	// => $something->content = <<<HTML
	$pattern = '/\$(.*)\[\'en\'\] = <<<HTML/';
	$replacement = '\$$1->content = <<<HTML';
	$line = preg_replace($pattern, $replacement, $line);


	//    $something = newSection('wikipedia', 'https://en.wikipedia.org/wiki/Something', 'Something');
	// => $something = new WikipediaContentSection();
	// +  $something->setTitleText('Something');
	// +  $something->setTitleLink('https://en.wikipedia.org/wiki/Something');
	$pattern = '/\$(.*) = newSection\(\'wikipedia\', \'(.*)\', \'(.*)\'\);/';
	if (preg_match($pattern, $line, $matches)) {
		$out[] = "\$${matches[1]} = new WikipediaContentSection();\n";
		$out[] = "\$${matches[1]}->setTitleText('${matches[3]}');\n";
		$out[] = "\$${matches[1]}->setTitleLink('${matches[2]}');\n";
		continue;
	}
	$pattern = '/\$(.*) = newSection\(\'wikipedia\'/';
	if (preg_match($pattern, $line, $matches)) {
		echo $line;
		echo "Make sure wikipedia sections are declared on a single line for the upgrade script to work properly.\n";
		exit;
	}


	//    $something = newSection('codeberg', '123', 'something');
	// => $something = new CodebergContentSection();
	//    $something->setTitleText('something');
	//    $something->setTitleLink('https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy/issues/123');
	$pattern = '/\$(.*) = newSection\(\'codeberg\', \'(.*)\', \'(.*)\'\);/';
	if (preg_match($pattern, $line, $matches)) {
		$out[] = "\$${matches[1]} = new CodebergContentSection();\n";
		$out[] = "\$${matches[1]}->setTitleText('${matches[3]}');\n";
		$out[] = "\$${matches[1]}->setTitleLink('https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy/issues/${matches[2]}');\n";
		continue;
	}


	//     $body .= printCountryIndices('Some Country');
	// =>  $page->body('Country indices');
	$pattern = '/\$body .= printCountryIndices\(\'(.*)\'\);/';
	if (preg_match($pattern, $line, $matches)) {
		$out[1] = "\$page = new CountryPage('${matches[1]}');\n";
		$out[] = "\$page->body('Country indices');\n";
		continue;
	}


	//    $body .= printSection($something);
	// => $page->body($something);
	$pattern = '/\$body .= printSection\((.*)\);/';
	$replacement = '\$page->body($1);';
	$line = preg_replace($pattern, $replacement, $line);


	// $body .= printPageSection('something.html');
	// => $page->body('something.html');
	$pattern = '/\$body .= printPageSection\((.*)\);/';
	$replacement = '\$page->body($1);';
	$line = preg_replace($pattern, $replacement, $line);


	//    $body .= $something->print();
	// => $page->body($something);
	$pattern = '/\$body .= \$(.*)->print\(\);/';
	$replacement = '\$page->body(\$$1);';
	$line = preg_replace($pattern, $replacement, $line);



	// Remove:
	//        $h2_something = newH2();
	$pattern = '/\$h2_(.*) = newH2\(\);/';
	if (preg_match($pattern, $line, $matches)) {
		continue;
	}

	//    $h2_something['en'] = 'Something';
	// => $h2_something = new h2HeaderContent('Something');
	$pattern = '/\$h2_(.*)\[\'en\'\] = (.*);/';
	$replacement = '\$h2_$1 = new h2HeaderContent($2);';
	$line = preg_replace($pattern, $replacement, $line);

	//    $body .= printH2($h2_something);
	// => $page->body($h2_something);
	$pattern = '/\$body .= printH2\(\$(.*)\);/';
	$replacement = '\$page->body(\$$1);';
	$line = preg_replace($pattern, $replacement, $line);



	//    $r1 = newRef('something', 'something');
	// => $r1 = $page->ref('something', 'something');
	$pattern = '/\$r([0-9]*) = newRef\(\'(.*)\', \'(.*)\'\);/';
	if (preg_match($pattern, $line, $matches)) {
		$out[] = "\$r${matches[1]} = \$page->ref('${matches[2]}', '${matches[3]}');\n";
		continue;
	}

	$pattern = '/\$r([0-9]*) = newRef\(\'(.*)\'/';
	if (preg_match($pattern, $line, $matches)) {
		echo "Make sure newRef calls are made on a single line for the upgrade script to work properly.\n";
		exit;
	}


	$out[] = $line;
}

remove_trailing_empty_lines($out);

$sed1 = "sed -i \"s#printSection[(]\\\$${section_preview_name}[)]#printPageSection('${dest}')#g\" src/*.php";
$sed2 = "sed -i \"s#page->body[(]\\\$${section_preview_name}[)]#page->body('${dest}')#g\" src/*.php";

if ($test) {
	foreach ($out AS $line) {
		echo $line;
	}
	echo "\n\nSed command:\n\t$sed1\n\n\t$sed2\n";
	$grep = "grep --color=always --dereference-recursive '(\$${section_preview_name})'";
	echo "\n\nGrep command:\n\t$grep\n";
	echo shell_exec($grep);
}
else {
	$page = '';
	foreach ($out AS $line) {
		$page .= $line;
	}
	file_put_contents($src, $page);
	$preprocess_index_src[$src]['API'] = 1;

	// Clean section;
	$name    = $preprocess_index_dest[$dest]['name'];
	$include = $preprocess_index_dest[$dest]['include'];
	clean_section($name, $include);

	$preprocess_index_dest[$dest]['name'] = 'page';
	$preprocess_index_dest[$dest]['include'] = $src;
	update_preprocess_index();
	shell_exec($sed1);
	shell_exec($sed2);
}
