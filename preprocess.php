#!/usr/bin/php
<?php
$start_time = hrtime(true);
require_once('include/preprocess.php');
require_once('include/init.php');
global $preprocess_index_src;

$nb_articles_by_stars = array(
	-1 => 0,
	0 => 0,
	1 => 0,
	2 => 0,
	3 => 0,
	4 => 0,
	5 => 0,
);

$process_all = true;
$src = '';

if (isset($argv[1])) {
	if ($argv[1] == "wip") {
		$show_wip = true;

		if (isset($argv[2])) {
			$src = $argv[2];
			$process_all = false;
		}
	}
	else {
		$src = $argv[1];
		$process_all = false;
	}
}

$keywords_index = array();
$tags_index     = array();

function spawn_latest_page($list_Latest) {
	global $preprocess_index_dest;
	global $process_fully;
	$process_fully = true;

	$page = new Page();
	$page->dest("http/latest.html");
	$page->h1("Latest updated files");
	$page->snp('description', 'Last files changed in the project');
	$page->snp('image', "/free/latest.1200-630.png");
	$page->viewport_background('/free/latest.png');

	$div_list_Latest = new ContentSection();
	$div_list_Latest->content = $list_Latest->print();
	$page->body($div_list_Latest);

	$out = $page->print("body");
	file_put_contents("http/latest.html", $out);
}

if ($process_all) {
	foreach(glob("http/*.html") as $file) {
		unlink($file);
	}

	echo "Preprocessing all targets.\r";
	$list_Latest = new latestUpdatedPagesBySrc();

	foreach ($preprocess_index_src AS $src => $data) {
		if (!isset($src_by_change_time[$src])) {
			$src_by_change_time[$src] = array(
				'git_changed_t' => 0,
				'fs_changed_t' => 0,
			);
		}
		$list_Latest->add($src);

		$dest = $data["dest"];
		$format = $show_wip ? 'wip' : 'body';
		process_page($src, $format);
	}

	spawn_latest_page($list_Latest);
	$src_by_change_time["settings"]["last_checked_t"] = time();

	save_array('keywords_index', 'index/keywords.php');
	save_array('tags_index',     'index/tags.php');
	save_array('src_by_change_time', 'index/src_by_change_time.php');

	spawn_keywords();
	echo "\033[2K";

	foreach ($nb_articles_by_stars AS $stars => $nb) {
		echo "\t $stars stars: $nb articles \n";
	}
}
else {
	if (isset($preprocess_index_src[$src])) {
		echo "Preprocessing $src .\n";
		$dest = $preprocess_index_src[$src]["dest"];
		$format = $show_wip ? 'wip' : 'body';
		process_page($src, $format);
	}
	else {
		echo "	!! The source ${src} is not valid.\n";
	}
}

update_preprocess_index();

$count_commits = shell_exec("git rev-list --count HEAD");
echo "Commits in HEAD:\t $count_commits";

$count_files = shell_exec("git ls-files | wc -l");
echo "Number of files:\t $count_files";

$count_articles = count($preprocess_index_src);
echo "Number of articles:\t $count_articles\n";

$memory =  memory_get_peak_usage(1) / 1024 / 1024;
echo "Memory used:\t\t ${memory} MiB\n";

$end_time = hrtime(true);
$duration = round(($end_time - $start_time) / 1000000000, 2);
echo "Time used:\t\t ${duration} seconds\n";
