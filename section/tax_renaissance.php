<?php
$Multiple_Crises_Caused_by_a_Broken_Fiscal_System = new stdClass();
$The_Fiscal_Renaissance_A_Vision_for_the_Future   = new stdClass();

function taxes_menu(&$page) {
	global $Multiple_Crises_Caused_by_a_Broken_Fiscal_System;
	global $The_Fiscal_Renaissance_A_Vision_for_the_Future;


	$h2_taxes = new h2HeaderContent('About Taxes');

	$Multiple_Crises_Caused_by_a_Broken_Fiscal_System = new ContentSection();
	$Multiple_Crises_Caused_by_a_Broken_Fiscal_System->content = <<<HTML
		<h3>Multiple Crises Caused by a Broken Fiscal System</h3>
		HTML;


	$The_Fiscal_Renaissance_A_Vision_for_the_Future = new ContentSection();
	$The_Fiscal_Renaissance_A_Vision_for_the_Future->content = <<<HTML
		<h3>The Fiscal Renaissance: A Vision for the Future</h3>
		HTML;

	$page->body($h2_taxes);
	$page->body('democracy_taxes.html');
	$page->body('tax_renaissance.html');

	tax_common_menu($page);
}

function tax_renaissance_menu(&$page) {
	global $Multiple_Crises_Caused_by_a_Broken_Fiscal_System;
	global $The_Fiscal_Renaissance_A_Vision_for_the_Future;

	$Multiple_Crises_Caused_by_a_Broken_Fiscal_System = new h2HeaderContent("Multiple Crises Caused by a Broken Fiscal System");
	$The_Fiscal_Renaissance_A_Vision_for_the_Future = new h2HeaderContent("The Fiscal Renaissance: A Vision for the Future");

	tax_common_menu($page);

}

function tax_common_menu(&$page) {
	global $Multiple_Crises_Caused_by_a_Broken_Fiscal_System;
	global $The_Fiscal_Renaissance_A_Vision_for_the_Future;

	$page->body($Multiple_Crises_Caused_by_a_Broken_Fiscal_System);
	$page->body('labor_taxes.html');
	$page->body('externalities.html');
	$page->body('redistribution_of_wealth_a_false_solution.html');

	$page->body($The_Fiscal_Renaissance_A_Vision_for_the_Future);
	$page->body('pigouvian_tax.html');
	$page->body('organic_taxes.html');
	$page->body('organic_taxes_the_cure_for_healthcare_funding.html');

}
