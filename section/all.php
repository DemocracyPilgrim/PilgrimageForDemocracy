<?php
require_once 'section/duverger.php';
require_once 'section/external_threats.php';
require_once 'section/fair_share.php';
require_once 'section/leadership.php';
require_once 'section/speech.php';
require_once 'section/tax_renaissance.php';
require_once 'section/threats.php';
require_once 'section/tweed.php';
require_once 'section/voting.php';
require_once 'section/world.php';
