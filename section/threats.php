<?php

function threats_menu(&$page) {
	$h2_Threats_to_Democracy = new h2HeaderContent("About the Threats to Democracy");
	$page->body($h2_Threats_to_Democracy);
	$page->body("the_two_sides_of_democratic_dysfunction.html");
	$page->body("duverger_syndrome.html");
	$page->body("tweed_syndrome.html");
	$page->body("external_threats.html");

}
