<?php
$header_voting_methods_problems  = new stdClass();
$header_voting_methods_solutions = new stdClass();

function voting_methods_menu(&$page) {
	global $header_voting_methods_problems;
	global $header_voting_methods_solutions;

	$h2_Building_democracy_with_good_voting_methods = new h2HeaderContent("About Building Democracy with Good Voting Methods");

	$header_voting_methods_problems  = new ContentSection();
	$header_voting_methods_problems->content = <<<HTML
		<h3>The problems</h3>
		HTML;

	$header_voting_methods_solutions = new ContentSection();
	$header_voting_methods_solutions->content = <<<HTML
		<h3>The solutions</h3>
		HTML;

	$page->body($h2_Building_democracy_with_good_voting_methods);

	$page->body('voting_methods.html');




	voting_methods_common_menu($page);
}

function voting_methods_article_menu(&$page) {
	global $header_voting_methods_problems;
	global $header_voting_methods_solutions;

	$header_voting_methods_problems  = new h2HeaderContent("The problems");
	$header_voting_methods_solutions = new h2HeaderContent("The solutions");

	voting_methods_common_menu($page);
	$h2_Related_topics = new h2HeaderContent("Related topics");
	$page->body($h2_Related_topics);
	$page->related_tag("Voting Methods");
}

function voting_methods_common_menu(&$page) {
	global $header_voting_methods_problems;
	global $header_voting_methods_solutions;

	$page->body($header_voting_methods_problems);
	$page->body('duverger_syndrome.html');
	$page->body('single_choice_voting.html');
	$page->body('plurality_voting.html');
	$page->body('primary_elections.html');

	$page->body($header_voting_methods_solutions);
	$page->body('approval_voting.html');
	$page->body('score_voting.html');
	$page->body('informed_score_voting.html');
	$page->body('the_i_dont_know_revolution.html');
	$page->body('informed_score_voting_for_stronger_democracy.html');
	$page->body('the_end_of_political_parties.html');
}
