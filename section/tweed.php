<?php
$header_tweed_syndrome  = new stdClass();
$header_boss_tweed      = new stdClass();
$header_tweed_solutions = new stdClass();

function tweed_syndrome_menu(&$page) {
	global $header_tweed_syndrome;
	global $header_boss_tweed    ;
	global $header_tweed_solutions;

	$h2_Tweed_Syndrome = new h2HeaderContent("About the Tweed Syndrome: A Corrupt Use of the System");

	$header_tweed_syndrome = new ContentSection();
	$header_tweed_syndrome->content = <<<HTML
		<h3>The symptoms of the Tweed Syndrome</h3>
		HTML;


	$header_boss_tweed= new ContentSection();
	$header_boss_tweed->content = <<<HTML
		<h3>Boss Tweed</h3>
		HTML;


	$header_tweed_solutions = new ContentSection();
	$header_tweed_solutions->content = <<<HTML
		<h3>Solutions</h3>
		HTML;

	$page->body($h2_Tweed_Syndrome);
	$page->body('tweed_syndrome.html');

	tweed_syndrome_common_menu($page);
}

function tweed_syndrome_article_menu(&$page) {
	global $header_tweed_syndrome;
	global $header_boss_tweed    ;
	global $header_tweed_solutions;

	$header_tweed_syndrome  = new h2HeaderContent("The symptoms of the Tweed Syndrome");
	$header_boss_tweed      = new h2HeaderContent("Boss Tweed");
	$header_tweed_solutions = new h2HeaderContent("Solutions");

	tweed_syndrome_common_menu($page);
}

function tweed_syndrome_common_menu(&$page) {
	global $header_tweed_syndrome;
	global $header_boss_tweed    ;
	global $header_tweed_solutions;



	$page->body($header_tweed_syndrome);
	$page->body('tweed_syndrome_primaries.html');
	$page->body('tweed_symptom_uncontested_elections.html');

	$page->body($header_boss_tweed);
	$page->body('william_m_tweed.html');
	$page->body('tweedism.html');
	$page->body('tweed_syndrome_systemic_corruption.html');

	$page->body($header_tweed_solutions);
	$page->body('the_end_of_political_parties.html');
}
