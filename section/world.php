<?php

function world_menu(&$page) {
	$h2_democracy_world = new h2HeaderContent('About the World: countries and international organisations');
	$page->body($h2_democracy_world);
	$page->body('world.html');
	$page->body('democratic_osmosis.html');
	$page->body('the_power_of_example_improving_our_democracies.html');
}
