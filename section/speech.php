<?php

function speech_menu(&$page) {
	$h2_Right_speech_and_constructive_discourse = new h2HeaderContent('About Right speech and constructive discourse');
	$page->body($h2_Right_speech_and_constructive_discourse);
	$page->body('political_discourse.html');
	$page->body('disinformation.html');
	$page->body('freedom_of_speech.html');
}
