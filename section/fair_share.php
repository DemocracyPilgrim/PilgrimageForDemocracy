<?php

$header_fair_share_crises  = new stdClass();
$header_fair_share_principles  = new stdClass();
$header_fair_share_solutions  = new stdClass();

function fair_share_menu(&$page) {
	global $header_fair_share_crises;
	global $header_fair_share_principles;
	global $header_fair_share_solutions;

	$header_fair_share_crises = new ContentSection();
	$header_fair_share_crises->content = <<<HTML
		<h3>Crises</h3>
		HTML;

	$header_fair_share_principles = new ContentSection();
	$header_fair_share_principles->content = <<<HTML
		<h3>Principles</h3>
		HTML;

	$header_fair_share_solutions = new ContentSection();
	$header_fair_share_solutions->content = <<<HTML
		<h3>Solutions</h3>
		HTML;

	$h2_fair_share = new h2HeaderContent('About Getting a Fair Share');
	$page->body($h2_fair_share);
	$page->body('fair_share.html');
	fair_share_common_menu($page);
}

function fair_share_article_menu(&$page) {
	global $header_fair_share_crises;
	global $header_fair_share_principles;
	global $header_fair_share_solutions;

	$header_fair_share_crises  = new h2HeaderContent("Crises");
	$header_fair_share_principles  = new h2HeaderContent("Principles");
	$header_fair_share_solutions  = new h2HeaderContent("Solutions");

	fair_share_common_menu($page);
}

function fair_share_common_menu(&$page) {
	global $header_fair_share_crises;
	global $header_fair_share_principles;
	global $header_fair_share_solutions;

	$page->body($header_fair_share_crises);
	$page->body('economic_injustice.html');
	$page->body('desperation_economy.html');
	$page->body('wealth_inequality_in_america.html');
	$page->body('poverty.html');
	$page->body('profitable_factories_shutting_down.html');
	$page->body('offshoring.html');
	$page->body('how_the_wealthy_pass_on_economic_burden_to_the_poor.html');

	$page->body($header_fair_share_principles);
	$page->body('labour_and_value_creation.html');
	$page->body('capital.html');
	$page->body('factors_of_production.html');
	$page->body('factors_of_production_and_the_stewardship_of_the_commons.html');

	$page->body($header_fair_share_solutions);
	$page->body('fair_share_of_profits.html');
	$page->body('fair_share_of_responsibilities.html');
	$page->body('universal_basic_income.html');
}
