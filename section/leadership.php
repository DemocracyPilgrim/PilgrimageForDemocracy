<?php

function leadership_menu(&$page) {
	$h2_Leadership = new h2HeaderContent("About Leadership: Do Character and Competence Still Matter?");
	$page->body($h2_Leadership);

	leadership_common_menu($page);
}

function leadership_article_menu(&$page) {
	leadership_common_menu($page);
}

function leadership_common_menu(&$page) {
	$page->body('ignorant_leader_rising.html');
}
