<?php
$header_duverger_syndrome = new stdClass();
$header_duverger_theory   = new stdClass();
$header_duverger_examples = new stdClass();

function duverger_syndrome_menu(&$page) {
	global $header_duverger_syndrome;
	global $header_duverger_theory;
	global $header_duverger_examples;

	$header_duverger_syndrome = new ContentSection();
	$header_duverger_syndrome->content = <<<HTML
		<h3>The symptoms of the Duverger Syndrome</h3>
		HTML;


	$header_duverger_theory= new ContentSection();
	$header_duverger_theory->content = <<<HTML
		<h3>Duverger's Law</h3>
		HTML;


	$header_duverger_examples = new ContentSection();
	$header_duverger_examples->content = <<<HTML
		<h3>Historical examples</h3>
		HTML;


	$h2_Duverger = new h2HeaderContent("About the Duverger Syndrome: A Corrupt System");

	$page->body($h2_Duverger);

	$page->body('duverger_syndrome.html');

	duverger_syndrome_common_menu($page);
}

function duverger_syndrome_article_menu(&$page) {
	global $header_duverger_syndrome;
	global $header_duverger_theory;
	global $header_duverger_examples;

	$header_duverger_syndrome = new h2HeaderContent('The symptoms of the Duverger Syndrome');
	$header_duverger_theory   = new h2HeaderContent("Duverger's Law");
	$header_duverger_examples = new h2HeaderContent('Historical examples');

	duverger_syndrome_common_menu($page);
}

function duverger_syndrome_common_menu(&$page) {
	global $header_duverger_syndrome;
	global $header_duverger_theory;
	global $header_duverger_examples;

	$page->body($header_duverger_syndrome);
	$page->body('duverger_syndrome_duality.html');
	$page->body('duverger_syndrome_alternance.html');
	$page->body('duverger_syndrome_party_politics.html');
	$page->body('duverger_syndrome_negative_campaigning.html');
	$page->body('duverger_syndrome_lack_choice.html');
	$page->body('duverger_syndrome_extremism.html');
	$page->body('duverger_syndrome_political_realignments.html');

	$page->body($header_duverger_theory);
	$page->body('duverger_law.html');

	$page->body($header_duverger_examples);
	$page->body('duverger_usa_19_century.html');
	$page->body('duverger_usa_21_century.html');
	$page->body('duverger_taiwan_2000_2004.html');
	$page->body('duverger_taiwan_2024.html');
	$page->body('duverger_france_alternance_danger.html');
}
