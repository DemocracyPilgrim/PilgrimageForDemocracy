<?php

function external_threats_menu(&$page) {
	$h2_External_Threats = new h2HeaderContent("About the External Threats to Democracy");
	$page->body($h2_External_Threats);

	$page->body("external_threats.html");

	external_threats_common_menu($page);

}

function external_threats_article_menu(&$page) {
	external_threats_common_menu($page);
}

function external_threats_common_menu(&$page) {
	$page->body('authoritarianism.html');
	$page->body('authoritarian_exploitation_of_democratic_weaknesses.html');
	$page->body('duverger_trap.html');
	$page->body('transnational_authoritarianism.html');
	$page->body('cyberwarfare.html');
}
