<?php


$preprocess_index_src = array(
	'src/60_minutes.php' => array(
		'dest'	 => 'http/60_minutes.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/60_minutes_conflict_between_china_philippines_could_involve_u_s_and_lead_to_a_clash_of_superpowers.php' => array(
		'dest'	 => 'http/60_minutes_conflict_between_china_philippines_could_involve_u_s_and_lead_to_a_clash_of_superpowers.html',
		'API'	 => '1',
		'stars'	 => '2',
	),
	'src/a_j_muste.php' => array(
		'dest'	 => 'http/a_j_muste.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/a_night_at_the_garden.php' => array(
		'dest'	 => 'http/a_night_at_the_garden.html',
		'API'	 => '1',
		'stars'	 => '2',
	),
	'src/aba_task_force_for_american_democracy.php' => array(
		'dest'	 => 'http/aba_task_force_for_american_democracy.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/accountability.php' => array(
		'dest'	 => 'http/accountability.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/adav_noti.php' => array(
		'dest'	 => 'http/adav_noti.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/advance_democracy.php' => array(
		'dest'	 => 'http/advance_democracy.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/advertising.php' => array(
		'dest'	 => 'http/advertising.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/alexis_de_tocqueville.php' => array(
		'dest'	 => 'http/alexis_de_tocqueville.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/algorithms.php' => array(
		'dest'	 => 'http/algorithms.html',
		'API'	 => '1',
		'stars'	 => '2',
	),
	'src/ali_velshi.php' => array(
		'dest'	 => 'http/ali_velshi.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/ali_velshi_s_hope_for_the_middle_east_and_humanity.php' => array(
		'dest'	 => 'http/ali_velshi_s_hope_for_the_middle_east_and_humanity.html',
		'API'	 => '1',
		'stars'	 => '4',
	),
	'src/all_labor_has_dignity.php' => array(
		'dest'	 => 'http/all_labor_has_dignity.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/alleged_bribery_of_donald_trump_by_the_egyptian_president.php' => array(
		'dest'	 => 'http/alleged_bribery_of_donald_trump_by_the_egyptian_president.html',
		'API'	 => '1',
		'stars'	 => '2',
	),
	'src/amber_scorah.php' => array(
		'dest'	 => 'http/amber_scorah.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/american_bar_association.php' => array(
		'dest'	 => 'http/american_bar_association.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/american_civil_liberties_union.php' => array(
		'dest'	 => 'http/american_civil_liberties_union.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/american_constitution_society.php' => array(
		'dest'	 => 'http/american_constitution_society.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/american_fascism.php' => array(
		'dest'	 => 'http/american_fascism.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/american_friends_service_committee.php' => array(
		'dest'	 => 'http/american_friends_service_committee.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/american_immigration_council.php' => array(
		'dest'	 => 'http/american_immigration_council.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/amnesty_international.php' => array(
		'dest'	 => 'http/amnesty_international.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/andrew_weissmann.php' => array(
		'dest'	 => 'http/andrew_weissmann.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/andy_borowitz.php' => array(
		'dest'	 => 'http/andy_borowitz.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/anne_applebaum.php' => array(
		'dest'	 => 'http/anne_applebaum.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/anne_applebaum_on_autocratic_threats_around_the_world.php' => array(
		'dest'	 => 'http/anne_applebaum_on_autocratic_threats_around_the_world.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/apollo_robbins.php' => array(
		'dest'	 => 'http/apollo_robbins.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/appropriate_technology.php' => array(
		'dest'	 => 'http/appropriate_technology.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/approval_voting.php' => array(
		'dest'	 => 'http/approval_voting.html',
		'API'	 => '1',
		'stars'	 => '3',
	),
	'src/approval_voting_party.php' => array(
		'dest'	 => 'http/approval_voting_party.html',
		'API'	 => '1',
		'stars'	 => '2',
	),
	'src/archives/index.php' => array(
		'dest'	 => 'http/archives/index.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/archives/russian_federation_and_people_s_republic_of_china_joint_statement_february_2022.php' => array(
		'dest'	 => 'http/archives/russian_federation_and_people_s_republic_of_china_joint_statement_february_2022.html',
		'API'	 => '1',
		'stars'	 => '3',
	),
	'src/argentina.php' => array(
		'dest'	 => 'http/argentina.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/ari_melber.php' => array(
		'dest'	 => 'http/ari_melber.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/ari_melber_interviews_yuval_harari_debunking_trump_s_lies.php' => array(
		'dest'	 => 'http/ari_melber_interviews_yuval_harari_debunking_trump_s_lies.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/arte.php' => array(
		'dest'	 => 'http/arte.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/artificial_intelligence.php' => array(
		'dest'	 => 'http/artificial_intelligence.html',
		'API'	 => '1',
		'stars'	 => '2',
	),
	'src/asylum_seeker.php' => array(
		'dest'	 => 'http/asylum_seeker.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/at_war_with_ourselves.php' => array(
		'dest'	 => 'http/at_war_with_ourselves.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/australia.php' => array(
		'dest'	 => 'http/australia.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/australian_strategic_policy_institute.php' => array(
		'dest'	 => 'http/australian_strategic_policy_institute.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/authoritarian_exploitation_of_democratic_weaknesses.php' => array(
		'dest'	 => 'http/authoritarian_exploitation_of_democratic_weaknesses.html',
		'API'	 => '1',
		'stars'	 => '3',
	),
	'src/authoritarianism.php' => array(
		'dest'	 => 'http/authoritarianism.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/b_lab.php' => array(
		'dest'	 => 'http/b_lab.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/bail.php' => array(
		'dest'	 => 'http/bail.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/bangladesh.php' => array(
		'dest'	 => 'http/bangladesh.html',
		'API'	 => '1',
		'stars'	 => '2',
	),
	'src/barack_obama.php' => array(
		'dest'	 => 'http/barack_obama.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/barack_obama_keynote_address_at_the_2004_democratic_national_convention.php' => array(
		'dest'	 => 'http/barack_obama_keynote_address_at_the_2004_democratic_national_convention.html',
		'API'	 => '1',
		'stars'	 => '3',
	),
	'src/bbc_world_service.php' => array(
		'dest'	 => 'http/bbc_world_service.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/belgium.php' => array(
		'dest'	 => 'http/belgium.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/beliefs.php' => array(
		'dest'	 => 'http/beliefs.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/berkman_klein_center_for_internet_society.php' => array(
		'dest'	 => 'http/berkman_klein_center_for_internet_society.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/bhimrao_ramji_ambedkar.php' => array(
		'dest'	 => 'http/bhimrao_ramji_ambedkar.html',
		'API'	 => '1',
		'stars'	 => '2',
	),
	'src/bioneers.php' => array(
		'dest'	 => 'http/bioneers.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/blasphemy.php' => array(
		'dest'	 => 'http/blasphemy.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/blowback_a_warning_to_save_democracy.php' => array(
		'dest'	 => 'http/blowback_a_warning_to_save_democracy.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/bolivia.php' => array(
		'dest'	 => 'http/bolivia.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/bootleggers_and_baptists.php' => array(
		'dest'	 => 'http/bootleggers_and_baptists.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/branches_of_government.php' => array(
		'dest'	 => 'http/branches_of_government.html',
		'API'	 => '1',
		'stars'	 => '2',
	),
	'src/brandenburg_v_ohio.php' => array(
		'dest'	 => 'http/brandenburg_v_ohio.html',
		'API'	 => '1',
		'stars'	 => '2',
	),
	'src/brazil.php' => array(
		'dest'	 => 'http/brazil.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/brian_tyler_cohen.php' => array(
		'dest'	 => 'http/brian_tyler_cohen.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/brian_tyler_cohen_trump_goes_off_the_rails_with_insulting_message_to_women.php' => array(
		'dest'	 => 'http/brian_tyler_cohen_trump_goes_off_the_rails_with_insulting_message_to_women.html',
		'API'	 => '1',
		'stars'	 => '2',
	),
	'src/brics.php' => array(
		'dest'	 => 'http/brics.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/brown_v_board_of_education.php' => array(
		'dest'	 => 'http/brown_v_board_of_education.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/buddhist_economics.php' => array(
		'dest'	 => 'http/buddhist_economics.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/burwell_v_hobby_lobby_stores_inc.php' => array(
		'dest'	 => 'http/burwell_v_hobby_lobby_stores_inc.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/bush_v_gore.php' => array(
		'dest'	 => 'http/bush_v_gore.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/business_ethics_magazine.php' => array(
		'dest'	 => 'http/business_ethics_magazine.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/campaign_finance.php' => array(
		'dest'	 => 'http/campaign_finance.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/campaign_legal_center.php' => array(
		'dest'	 => 'http/campaign_legal_center.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/capital.php' => array(
		'dest'	 => 'http/capital.html',
		'API'	 => '1',
		'stars'	 => '4',
	),
	'src/capital_in_the_twenty_first_century.php' => array(
		'dest'	 => 'http/capital_in_the_twenty_first_century.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/capital_punishment.php' => array(
		'dest'	 => 'http/capital_punishment.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/caribbean_community.php' => array(
		'dest'	 => 'http/caribbean_community.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/carnegie_endowment_for_international_peace.php' => array(
		'dest'	 => 'http/carnegie_endowment_for_international_peace.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/censorship.php' => array(
		'dest'	 => 'http/censorship.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/center_for_countering_digital_hate.php' => array(
		'dest'	 => 'http/center_for_countering_digital_hate.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/center_for_democracy_and_technology.php' => array(
		'dest'	 => 'http/center_for_democracy_and_technology.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/center_for_election_innovation_research.php' => array(
		'dest'	 => 'http/center_for_election_innovation_research.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/center_for_election_science.php' => array(
		'dest'	 => 'http/center_for_election_science.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/centre_for_humanitarian_data.php' => array(
		'dest'	 => 'http/centre_for_humanitarian_data.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/centre_for_information_resilience.php' => array(
		'dest'	 => 'http/centre_for_information_resilience.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/centre_for_law_and_policy_research.php' => array(
		'dest'	 => 'http/centre_for_law_and_policy_research.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/certifying_elections.php' => array(
		'dest'	 => 'http/certifying_elections.html',
		'API'	 => '1',
		'stars'	 => '2',
	),
	'src/charles_hamilton_houston_institute_for_race_and_justice.php' => array(
		'dest'	 => 'http/charles_hamilton_houston_institute_for_race_and_justice.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/charlie_chaplin.php' => array(
		'dest'	 => 'http/charlie_chaplin.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/charlie_chaplin_final_speech_from_the_great_dictator.php' => array(
		'dest'	 => 'http/charlie_chaplin_final_speech_from_the_great_dictator.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/chevron_u_s_a_inc_v_natural_resources_defense_council_inc.php' => array(
		'dest'	 => 'http/chevron_u_s_a_inc_v_natural_resources_defense_council_inc.html',
		'API'	 => '1',
		'stars'	 => '2',
	),
	'src/chi_huang.php' => array(
		'dest'	 => 'http/chi_huang.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/child_labour.php' => array(
		'dest'	 => 'http/child_labour.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/children_s_rights.php' => array(
		'dest'	 => 'http/children_s_rights.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/chile.php' => array(
		'dest'	 => 'http/chile.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/china_and_taiwan.php' => array(
		'dest'	 => 'http/china_and_taiwan.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/chinese_expansionism.php' => array(
		'dest'	 => 'http/chinese_expansionism.html',
		'API'	 => '1',
		'stars'	 => '2',
	),
	'src/citizens_united_v_federal_election_commission.php' => array(
		'dest'	 => 'http/citizens_united_v_federal_election_commission.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/clark_cunningham.php' => array(
		'dest'	 => 'http/clark_cunningham.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/cnn_hero_payton_mcgriff_on_school_uniform_for_school_girl_education_in_togo.php' => array(
		'dest'	 => 'http/cnn_hero_payton_mcgriff_on_school_uniform_for_school_girl_education_in_togo.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/colombia.php' => array(
		'dest'	 => 'http/colombia.html',
		'API'	 => '1',
		'stars'	 => '2',
	),
	'src/colorado.php' => array(
		'dest'	 => 'http/colorado.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/committee_for_children.php' => array(
		'dest'	 => 'http/committee_for_children.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/committee_to_protect_journalists.php' => array(
		'dest'	 => 'http/committee_to_protect_journalists.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/community_of_democracies.php' => array(
		'dest'	 => 'http/community_of_democracies.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/community_organized_relief_effort.php' => array(
		'dest'	 => 'http/community_organized_relief_effort.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/comparative_study_of_electoral_systems.php' => array(
		'dest'	 => 'http/comparative_study_of_electoral_systems.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/compromised_book.php' => array(
		'dest'	 => 'http/compromised_book.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/concession.php' => array(
		'dest'	 => 'http/concession.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/concession_speech.php' => array(
		'dest'	 => 'http/concession_speech.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/conflict_resolution.php' => array(
		'dest'	 => 'http/conflict_resolution.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/conservatism.php' => array(
		'dest'	 => 'http/conservatism.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/conspiracy_theory.php' => array(
		'dest'	 => 'http/conspiracy_theory.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/constitution.php' => array(
		'dest'	 => 'http/constitution.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/constitution_annotated.php' => array(
		'dest'	 => 'http/constitution_annotated.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/constitution_of_india.php' => array(
		'dest'	 => 'http/constitution_of_india.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/constitution_of_the_philippines.php' => array(
		'dest'	 => 'http/constitution_of_the_philippines.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/consumer_financial_protection_bureau.php' => array(
		'dest'	 => 'http/consumer_financial_protection_bureau.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/corporate_crime_and_punishment.php' => array(
		'dest'	 => 'http/corporate_crime_and_punishment.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/corruption.php' => array(
		'dest'	 => 'http/corruption.html',
		'API'	 => '1',
		'stars'	 => '3',
	),
	'src/corruption_in_the_united_states.php' => array(
		'dest'	 => 'http/corruption_in_the_united_states.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/costa_rica.php' => array(
		'dest'	 => 'http/costa_rica.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/council_on_foreign_relations.php' => array(
		'dest'	 => 'http/council_on_foreign_relations.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/counterinsurgency_mathematics.php' => array(
		'dest'	 => 'http/counterinsurgency_mathematics.html',
		'API'	 => '1',
		'stars'	 => '2',
	),
	'src/counting_the_vote_a_firing_line_special_with_margaret_hoover.php' => array(
		'dest'	 => 'http/counting_the_vote_a_firing_line_special_with_margaret_hoover.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/courtlistener.php' => array(
		'dest'	 => 'http/courtlistener.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/covert_political_repression.php' => array(
		'dest'	 => 'http/covert_political_repression.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/credo23.php' => array(
		'dest'	 => 'http/credo23.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/criminal_justice.php' => array(
		'dest'	 => 'http/criminal_justice.html',
		'API'	 => '1',
		'stars'	 => '3',
	),
	'src/cuba.php' => array(
		'dest'	 => 'http/cuba.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/cyberwarfare.php' => array(
		'dest'	 => 'http/cyberwarfare.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/cynicism.php' => array(
		'dest'	 => 'http/cynicism.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/dan_goldman.php' => array(
		'dest'	 => 'http/dan_goldman.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/dangers.php' => array(
		'dest'	 => 'http/dangers.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/data_activism.php' => array(
		'dest'	 => 'http/data_activism.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/data_pop_alliance.php' => array(
		'dest'	 => 'http/data_pop_alliance.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/david_becker.php' => array(
		'dest'	 => 'http/david_becker.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/david_dill.php' => array(
		'dest'	 => 'http/david_dill.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/death_and_taxes.php' => array(
		'dest'	 => 'http/death_and_taxes.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/death_tax.php' => array(
		'dest'	 => 'http/death_tax.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/deaths_of_migrants.php' => array(
		'dest'	 => 'http/deaths_of_migrants.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/debt.php' => array(
		'dest'	 => 'http/debt.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/decentralized_autonomous_organization.php' => array(
		'dest'	 => 'http/decentralized_autonomous_organization.html',
		'API'	 => '1',
		'stars'	 => '2',
	),
	'src/deepfake.php' => array(
		'dest'	 => 'http/deepfake.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/defending_democracy_meet_the_republicans_voting_against_trump_with_sarah_longwell.php' => array(
		'dest'	 => 'http/defending_democracy_meet_the_republicans_voting_against_trump_with_sarah_longwell.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/defending_democracy_podcast.php' => array(
		'dest'	 => 'http/defending_democracy_podcast.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/defending_democracy_rep_dan_goldman_on_project_2025_fixing_the_supreme_court_and_gop_extremism.php' => array(
		'dest'	 => 'http/defending_democracy_rep_dan_goldman_on_project_2025_fixing_the_supreme_court_and_gop_extremism.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/democracy.php' => array(
		'dest'	 => 'http/democracy.html',
		'API'	 => '1',
		'stars'	 => '2',
	),
	'src/democracy_docket.php' => array(
		'dest'	 => 'http/democracy_docket.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/democracy_in_america.php' => array(
		'dest'	 => 'http/democracy_in_america.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/democracy_international.php' => array(
		'dest'	 => 'http/democracy_international.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/democracy_taxes.php' => array(
		'dest'	 => 'http/democracy_taxes.html',
		'API'	 => '1',
		'stars'	 => '3',
	),
	'src/democracy_wip.php' => array(
		'dest'	 => 'http/democracy_wip.html',
		'API'	 => '1',
		'stars'	 => '3',
	),
	'src/democratic_backsliding_in_the_united_states.php' => array(
		'dest'	 => 'http/democratic_backsliding_in_the_united_states.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/democratic_osmosis.php' => array(
		'dest'	 => 'http/democratic_osmosis.html',
		'API'	 => '1',
		'stars'	 => '3',
	),
	'src/democratic_republic_of_the_congo.php' => array(
		'dest'	 => 'http/democratic_republic_of_the_congo.html',
		'API'	 => '1',
		'stars'	 => '2',
	),
	'src/denis_mukwege.php' => array(
		'dest'	 => 'http/denis_mukwege.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/denise_dresser.php' => array(
		'dest'	 => 'http/denise_dresser.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/denmark.php' => array(
		'dest'	 => 'http/denmark.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/desperation_economy.php' => array(
		'dest'	 => 'http/desperation_economy.html',
		'API'	 => '1',
		'stars'	 => '4',
	),
	'src/development_policy_centre.php' => array(
		'dest'	 => 'http/development_policy_centre.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/diamond_open_access.php' => array(
		'dest'	 => 'http/diamond_open_access.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/differ_we_must.php' => array(
		'dest'	 => 'http/differ_we_must.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/digital_habitat.php' => array(
		'dest'	 => 'http/digital_habitat.html',
		'API'	 => '1',
		'stars'	 => '3',
	),
	'src/disfranchisement.php' => array(
		'dest'	 => 'http/disfranchisement.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/disinformation.php' => array(
		'dest'	 => 'http/disinformation.html',
		'API'	 => '1',
		'stars'	 => '4',
	),
	'src/disinformation_and_hate_speech_on_social_media.php' => array(
		'dest'	 => 'http/disinformation_and_hate_speech_on_social_media.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/district_of_columbia_v_heller.php' => array(
		'dest'	 => 'http/district_of_columbia_v_heller.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/dominican_republic.php' => array(
		'dest'	 => 'http/dominican_republic.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/don_miguel_ruiz.php' => array(
		'dest'	 => 'http/don_miguel_ruiz.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/donald_trump.php' => array(
		'dest'	 => 'http/donald_trump.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/donald_trump_policy_and_discourse_on_immigration_and_asylum.php' => array(
		'dest'	 => 'http/donald_trump_policy_and_discourse_on_immigration_and_asylum.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/drawing_lessons_from_donald_trump_political_carrer_and_its_aftermath.php' => array(
		'dest'	 => 'http/drawing_lessons_from_donald_trump_political_carrer_and_its_aftermath.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/duverger_france_alternance_danger.php' => array(
		'dest'	 => 'http/duverger_france_alternance_danger.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/duverger_law.php' => array(
		'dest'	 => 'http/duverger_law.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/duverger_syndrome.php' => array(
		'dest'	 => 'http/duverger_syndrome.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/duverger_syndrome_alternance.php' => array(
		'dest'	 => 'http/duverger_syndrome_alternance.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/duverger_syndrome_duality.php' => array(
		'dest'	 => 'http/duverger_syndrome_duality.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/duverger_syndrome_extremism.php' => array(
		'dest'	 => 'http/duverger_syndrome_extremism.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/duverger_syndrome_lack_choice.php' => array(
		'dest'	 => 'http/duverger_syndrome_lack_choice.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/duverger_syndrome_negative_campaigning.php' => array(
		'dest'	 => 'http/duverger_syndrome_negative_campaigning.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/duverger_syndrome_party_politics.php' => array(
		'dest'	 => 'http/duverger_syndrome_party_politics.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/duverger_syndrome_political_realignments.php' => array(
		'dest'	 => 'http/duverger_syndrome_political_realignments.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/duverger_taiwan_2000_2004.php' => array(
		'dest'	 => 'http/duverger_taiwan_2000_2004.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/duverger_taiwan_2024.php' => array(
		'dest'	 => 'http/duverger_taiwan_2024.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/duverger_trap.php' => array(
		'dest'	 => 'http/duverger_trap.html',
		'API'	 => '1',
		'stars'	 => '2',
	),
	'src/duverger_usa_19_century.php' => array(
		'dest'	 => 'http/duverger_usa_19_century.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/duverger_usa_21_century.php' => array(
		'dest'	 => 'http/duverger_usa_21_century.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/echo_chamber.php' => array(
		'dest'	 => 'http/echo_chamber.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/economic_injustice.php' => array(
		'dest'	 => 'http/economic_injustice.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/economic_policy_institute.php' => array(
		'dest'	 => 'http/economic_policy_institute.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/economic_social_and_cultural_rights.php' => array(
		'dest'	 => 'http/economic_social_and_cultural_rights.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/economist_democracy_index.php' => array(
		'dest'	 => 'http/economist_democracy_index.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/economy.php' => array(
		'dest'	 => 'http/economy.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/ecowas.php' => array(
		'dest'	 => 'http/ecowas.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/ecuador.php' => array(
		'dest'	 => 'http/ecuador.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/eddie_glaude.php' => array(
		'dest'	 => 'http/eddie_glaude.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/education.php' => array(
		'dest'	 => 'http/education.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/egypt.php' => array(
		'dest'	 => 'http/egypt.html',
		'API'	 => '1',
		'stars'	 => '2',
	),
	'src/eighth_level_of_democracy.php' => array(
		'dest'	 => 'http/eighth_level_of_democracy.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/el_salvador.php' => array(
		'dest'	 => 'http/el_salvador.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/election_denialism.php' => array(
		'dest'	 => 'http/election_denialism.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/election_integrity.php' => array(
		'dest'	 => 'http/election_integrity.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/election_official.php' => array(
		'dest'	 => 'http/election_official.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/elections.php' => array(
		'dest'	 => 'http/elections.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/electoral_system.php' => array(
		'dest'	 => 'http/electoral_system.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/eminent_domain.php' => array(
		'dest'	 => 'http/eminent_domain.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/environment.php' => array(
		'dest'	 => 'http/environment.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/environmental_defenders.php' => array(
		'dest'	 => 'http/environmental_defenders.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/environmental_justice_foundation.php' => array(
		'dest'	 => 'http/environmental_justice_foundation.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/environmental_stewardship.php' => array(
		'dest'	 => 'http/environmental_stewardship.html',
		'API'	 => '1',
		'stars'	 => '2',
	),
	'src/erasing_history_how_fascists_rewrite_the_past_to_control_the_future.php' => array(
		'dest'	 => 'http/erasing_history_how_fascists_rewrite_the_past_to_control_the_future.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/ernst_friedrich_schumacher.php' => array(
		'dest'	 => 'http/ernst_friedrich_schumacher.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/erwin_chemerinsky.php' => array(
		'dest'	 => 'http/erwin_chemerinsky.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/estate_tax.php' => array(
		'dest'	 => 'http/estate_tax.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/estonia.php' => array(
		'dest'	 => 'http/estonia.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/ethiopia.php' => array(
		'dest'	 => 'http/ethiopia.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/eu_tax_observatory.php' => array(
		'dest'	 => 'http/eu_tax_observatory.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/euromaidan.php' => array(
		'dest'	 => 'http/euromaidan.html',
		'API'	 => '1',
		'stars'	 => '2',
	),
	'src/european_union.php' => array(
		'dest'	 => 'http/european_union.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/evolution_of_the_republican_party.php' => array(
		'dest'	 => 'http/evolution_of_the_republican_party.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/external_threats.php' => array(
		'dest'	 => 'http/external_threats.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/externalities.php' => array(
		'dest'	 => 'http/externalities.html',
		'API'	 => '1',
		'stars'	 => '2',
	),
	'src/factors_of_production.php' => array(
		'dest'	 => 'http/factors_of_production.html',
		'API'	 => '1',
		'stars'	 => '3',
	),
	'src/factors_of_production_and_the_stewardship_of_the_commons.php' => array(
		'dest'	 => 'http/factors_of_production_and_the_stewardship_of_the_commons.html',
		'API'	 => '1',
		'stars'	 => '3',
	),
	'src/fair_share.php' => array(
		'dest'	 => 'http/fair_share.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/fair_share_of_profits.php' => array(
		'dest'	 => 'http/fair_share_of_profits.html',
		'API'	 => '1',
		'stars'	 => '3',
	),
	'src/fair_share_of_responsibilities.php' => array(
		'dest'	 => 'http/fair_share_of_responsibilities.html',
		'API'	 => '1',
		'stars'	 => '3',
	),
	'src/fake_accounts_on_social_networks.php' => array(
		'dest'	 => 'http/fake_accounts_on_social_networks.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/famine_in_madagascar.php' => array(
		'dest'	 => 'http/famine_in_madagascar.html',
		'API'	 => '1',
		'stars'	 => '2',
	),
	'src/fascism.php' => array(
		'dest'	 => 'http/fascism.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/fatma_karume.php' => array(
		'dest'	 => 'http/fatma_karume.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/ferdinand_marcos.php' => array(
		'dest'	 => 'http/ferdinand_marcos.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/fifth_level_of_democracy.php' => array(
		'dest'	 => 'http/fifth_level_of_democracy.html',
		'API'	 => '1',
		'stars'	 => '2',
	),
	'src/fighting_disinfo_in_maga_era_ari_melber_talks_facts_tech_with_microsoft_legend_steve_ballmer.php' => array(
		'dest'	 => 'http/fighting_disinfo_in_maga_era_ari_melber_talks_facts_tech_with_microsoft_legend_steve_ballmer.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/firing_line.php' => array(
		'dest'	 => 'http/firing_line.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/first_level_of_democracy.php' => array(
		'dest'	 => 'http/first_level_of_democracy.html',
		'API'	 => '1',
		'stars'	 => '3',
	),
	'src/flat_tax.php' => array(
		'dest'	 => 'http/flat_tax.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/food_and_agriculture_organization.php' => array(
		'dest'	 => 'http/food_and_agriculture_organization.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/foreign_influence_local_media.php' => array(
		'dest'	 => 'http/foreign_influence_local_media.html',
		'API'	 => '1',
		'stars'	 => '2',
	),
	'src/foreign_workers_in_taiwan.php' => array(
		'dest'	 => 'http/foreign_workers_in_taiwan.html',
		'API'	 => '1',
		'stars'	 => '2',
	),
	'src/fourth_level_of_democracy.php' => array(
		'dest'	 => 'http/fourth_level_of_democracy.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/france.php' => array(
		'dest'	 => 'http/france.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/free_law_project.php' => array(
		'dest'	 => 'http/free_law_project.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/freedom.php' => array(
		'dest'	 => 'http/freedom.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/freedom_house.php' => array(
		'dest'	 => 'http/freedom_house.html',
		'API'	 => '1',
		'stars'	 => '2',
	),
	'src/freedom_of_association.php' => array(
		'dest'	 => 'http/freedom_of_association.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/freedom_of_conscience.php' => array(
		'dest'	 => 'http/freedom_of_conscience.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/freedom_of_romance.php' => array(
		'dest'	 => 'http/freedom_of_romance.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/freedom_of_speech.php' => array(
		'dest'	 => 'http/freedom_of_speech.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/from_russia_with_lev.php' => array(
		'dest'	 => 'http/from_russia_with_lev.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/frontline_short_docs_democracy_on_trial.php' => array(
		'dest'	 => 'http/frontline_short_docs_democracy_on_trial.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/funding_for_elections.php' => array(
		'dest'	 => 'http/funding_for_elections.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/gabon.php' => array(
		'dest'	 => 'http/gabon.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/gaming_the_vote_why_elections_aren_t_fair.php' => array(
		'dest'	 => 'http/gaming_the_vote_why_elections_aren_t_fair.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/gar_alperovitz.php' => array(
		'dest'	 => 'http/gar_alperovitz.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/george_orwell.php' => array(
		'dest'	 => 'http/george_orwell.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/georgia.php' => array(
		'dest'	 => 'http/georgia.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/germany.php' => array(
		'dest'	 => 'http/germany.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/gerrymandering.php' => array(
		'dest'	 => 'http/gerrymandering.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/gideon_v_wainwright.php' => array(
		'dest'	 => 'http/gideon_v_wainwright.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/glenn_kirschner.php' => array(
		'dest'	 => 'http/glenn_kirschner.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/glenn_kirschner_on_trump_s_legal_issues_for_haitian_immigrant_hoax.php' => array(
		'dest'	 => 'http/glenn_kirschner_on_trump_s_legal_issues_for_haitian_immigrant_hoax.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/global_impunity_index.php' => array(
		'dest'	 => 'http/global_impunity_index.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/global_issues.php' => array(
		'dest'	 => 'http/global_issues.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/global_migration_data_analysis_centre.php' => array(
		'dest'	 => 'http/global_migration_data_analysis_centre.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/global_natural_resources.php' => array(
		'dest'	 => 'http/global_natural_resources.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/global_peace_index.php' => array(
		'dest'	 => 'http/global_peace_index.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/global_witness.php' => array(
		'dest'	 => 'http/global_witness.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/green_new_deal.php' => array(
		'dest'	 => 'http/green_new_deal.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/green_new_deal_network.php' => array(
		'dest'	 => 'http/green_new_deal_network.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/greenwashing.php' => array(
		'dest'	 => 'http/greenwashing.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/greg_boyle.php' => array(
		'dest'	 => 'http/greg_boyle.html',
		'API'	 => '1',
		'stars'	 => '2',
	),
	'src/grift.php' => array(
		'dest'	 => 'http/grift.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/ground_news.php' => array(
		'dest'	 => 'http/ground_news.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/guatemala.php' => array(
		'dest'	 => 'http/guatemala.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/gun_control.php' => array(
		'dest'	 => 'http/gun_control.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/habitat.php' => array(
		'dest'	 => 'http/habitat.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/haiti.php' => array(
		'dest'	 => 'http/haiti.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/harvard_law_school.php' => array(
		'dest'	 => 'http/harvard_law_school.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/harvard_law_school_human_rights_program.php' => array(
		'dest'	 => 'http/harvard_law_school_human_rights_program.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/health_and_human_rights_journal.php' => array(
		'dest'	 => 'http/health_and_human_rights_journal.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/health_care.php' => array(
		'dest'	 => 'http/health_care.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/henry_george.php' => array(
		'dest'	 => 'http/henry_george.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/history_of_democracy_and_social_justice_in_the_united_states.php' => array(
		'dest'	 => 'http/history_of_democracy_and_social_justice_in_the_united_states.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/holodomor.php' => array(
		'dest'	 => 'http/holodomor.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/homeboy_industries.php' => array(
		'dest'	 => 'http/homeboy_industries.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/honduras.php' => array(
		'dest'	 => 'http/honduras.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/hong_kong.php' => array(
		'dest'	 => 'http/hong_kong.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/hope.php' => array(
		'dest'	 => 'http/hope.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/housing.php' => array(
		'dest'	 => 'http/housing.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/how_the_wealthy_pass_on_economic_burden_to_the_poor.php' => array(
		'dest'	 => 'http/how_the_wealthy_pass_on_economic_burden_to_the_poor.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/how_to_steal_a_presidential_election.php' => array(
		'dest'	 => 'http/how_to_steal_a_presidential_election.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/how_to_talk_with_victims_of_disinformation.php' => array(
		'dest'	 => 'http/how_to_talk_with_victims_of_disinformation.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/human_rights.php' => array(
		'dest'	 => 'http/human_rights.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/human_rights_watch.php' => array(
		'dest'	 => 'http/human_rights_watch.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/humanity.php' => array(
		'dest'	 => 'http/humanity.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/hunger.php' => array(
		'dest'	 => 'http/hunger.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/ignorant_leader_rising.php' => array(
		'dest'	 => 'http/ignorant_leader_rising.html',
		'API'	 => '1',
		'stars'	 => '3',
	),
	'src/illiberal_america_a_history.php' => array(
		'dest'	 => 'http/illiberal_america_a_history.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/immigration.php' => array(
		'dest'	 => 'http/immigration.html',
		'API'	 => '1',
		'stars'	 => '2',
	),
	'src/immunity.php' => array(
		'dest'	 => 'http/immunity.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/in_deep_trouble_surfacing_tech_powered_sexual_harassment_in_k_12_schools.php' => array(
		'dest'	 => 'http/in_deep_trouble_surfacing_tech_powered_sexual_harassment_in_k_12_schools.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/index.php' => array(
		'dest'	 => 'http/index.html',
		'API'	 => '1',
		'stars'	 => '4',
	),
	'src/india.php' => array(
		'dest'	 => 'http/india.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/individuals.php' => array(
		'dest'	 => 'http/individuals.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/industry_self_regulation.php' => array(
		'dest'	 => 'http/industry_self_regulation.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/inflation.php' => array(
		'dest'	 => 'http/inflation.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/information.php' => array(
		'dest'	 => 'http/information.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/information_overload.php' => array(
		'dest'	 => 'http/information_overload.html',
		'API'	 => '1',
		'stars'	 => '2',
	),
	'src/information_wars_how_we_lost_the_global_battle_against_disinformation.php' => array(
		'dest'	 => 'http/information_wars_how_we_lost_the_global_battle_against_disinformation.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/informed_score_voting.php' => array(
		'dest'	 => 'http/informed_score_voting.html',
		'API'	 => '1',
		'stars'	 => '4',
	),
	'src/informed_score_voting_for_stronger_democracy.php' => array(
		'dest'	 => 'http/informed_score_voting_for_stronger_democracy.html',
		'API'	 => '1',
		'stars'	 => '3',
	),
	'src/inheritance_tax.php' => array(
		'dest'	 => 'http/inheritance_tax.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/injustice.php' => array(
		'dest'	 => 'http/injustice.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/insidious_political_repression.php' => array(
		'dest'	 => 'http/insidious_political_repression.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/institute_for_economics_and_peace.php' => array(
		'dest'	 => 'http/institute_for_economics_and_peace.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/institute_for_policy_integrity.php' => array(
		'dest'	 => 'http/institute_for_policy_integrity.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/institutional_vs_human_factors_in_democracy.php' => array(
		'dest'	 => 'http/institutional_vs_human_factors_in_democracy.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/institutions.php' => array(
		'dest'	 => 'http/institutions.html',
		'API'	 => '1',
		'stars'	 => '2',
	),
	'src/integrity_institute.php' => array(
		'dest'	 => 'http/integrity_institute.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/integrity_institute_new_zealand.php' => array(
		'dest'	 => 'http/integrity_institute_new_zealand.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/international.php' => array(
		'dest'	 => 'http/international.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/international_children_s_peace_prize.php' => array(
		'dest'	 => 'http/international_children_s_peace_prize.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/international_crisis_group.php' => array(
		'dest'	 => 'http/international_crisis_group.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/international_democracy_community.php' => array(
		'dest'	 => 'http/international_democracy_community.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/international_institute_for_democracy_and_electoral_assistance.php' => array(
		'dest'	 => 'http/international_institute_for_democracy_and_electoral_assistance.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/international_monetary_fund.php' => array(
		'dest'	 => 'http/international_monetary_fund.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/international_organization_for_migration.php' => array(
		'dest'	 => 'http/international_organization_for_migration.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/interpersonal_relationships.php' => array(
		'dest'	 => 'http/interpersonal_relationships.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/invisible_rulers_the_people_who_turn_lies_into_reality.php' => array(
		'dest'	 => 'http/invisible_rulers_the_people_who_turn_lies_into_reality.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/invoking_democracy.php' => array(
		'dest'	 => 'http/invoking_democracy.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/iran.php' => array(
		'dest'	 => 'http/iran.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/israel.php' => array(
		'dest'	 => 'http/israel.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/israel_palestine.php' => array(
		'dest'	 => 'http/israel_palestine.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/italy.php' => array(
		'dest'	 => 'http/italy.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/jamie_raskin.php' => array(
		'dest'	 => 'http/jamie_raskin.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/jan_werner_mueller.php' => array(
		'dest'	 => 'http/jan_werner_mueller.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/jason_stanley.php' => array(
		'dest'	 => 'http/jason_stanley.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/jessie_daniel_ames.php' => array(
		'dest'	 => 'http/jessie_daniel_ames.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/jimmy_carter.php' => array(
		'dest'	 => 'http/jimmy_carter.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/joe_biden.php' => array(
		'dest'	 => 'http/joe_biden.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/john_abrams.php' => array(
		'dest'	 => 'http/john_abrams.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/jonathan_haidt.php' => array(
		'dest'	 => 'http/jonathan_haidt.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/jonathan_wilson_hartgrove.php' => array(
		'dest'	 => 'http/jonathan_wilson_hartgrove.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/justice.php' => array(
		'dest'	 => 'http/justice.html',
		'API'	 => '1',
		'stars'	 => '2',
	),
	'src/kamala_harris.php' => array(
		'dest'	 => 'http/kamala_harris.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/kamala_harris_concession_speech.php' => array(
		'dest'	 => 'http/kamala_harris_concession_speech.html',
		'API'	 => '1',
		'stars'	 => '3',
	),
	'src/karoline_wiesner.php' => array(
		'dest'	 => 'http/karoline_wiesner.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/keep_america_beautiful.php' => array(
		'dest'	 => 'http/keep_america_beautiful.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/kelo_v_city_of_new_london.php' => array(
		'dest'	 => 'http/kelo_v_city_of_new_london.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/ken_burns.php' => array(
		'dest'	 => 'http/ken_burns.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/ken_burns_h24_keynote_address_to_brandeis_university_2024_graduates.php' => array(
		'dest'	 => 'http/ken_burns_h24_keynote_address_to_brandeis_university_2024_graduates.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/ken_burns_on_the_incredibly_dangerous_party_that_the_gop_has_morphed_into.php' => array(
		'dest'	 => 'http/ken_burns_on_the_incredibly_dangerous_party_that_the_gop_has_morphed_into.html',
		'API'	 => '1',
		'stars'	 => '3',
	),
	'src/ken_burns_we_need_to_reach_out_to_trump_voters_and_not_other_them.php' => array(
		'dest'	 => 'http/ken_burns_we_need_to_reach_out_to_trump_voters_and_not_other_them.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/kenya.php' => array(
		'dest'	 => 'http/kenya.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/kerry_washington.php' => array(
		'dest'	 => 'http/kerry_washington.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/labor_taxes.php' => array(
		'dest'	 => 'http/labor_taxes.html',
		'API'	 => '1',
		'stars'	 => '2',
	),
	'src/labor_theory_of_value.php' => array(
		'dest'	 => 'http/labor_theory_of_value.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/labour.php' => array(
		'dest'	 => 'http/labour.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/labour_and_value_creation.php' => array(
		'dest'	 => 'http/labour_and_value_creation.html',
		'API'	 => '1',
		'stars'	 => '2',
	),
	'src/labour_economics.php' => array(
		'dest'	 => 'http/labour_economics.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/lai_ching_te.php' => array(
		'dest'	 => 'http/lai_ching_te.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/land.php' => array(
		'dest'	 => 'http/land.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/land_value_tax.php' => array(
		'dest'	 => 'http/land_value_tax.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/laurence_tribe.php' => array(
		'dest'	 => 'http/laurence_tribe.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/lawmaking.php' => array(
		'dest'	 => 'http/lawmaking.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/lawrence_lessig.php' => array(
		'dest'	 => 'http/lawrence_lessig.html',
		'API'	 => '1',
		'stars'	 => '2',
	),
	'src/lawrence_o_donnell.php' => array(
		'dest'	 => 'http/lawrence_o_donnell.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/lawyers.php' => array(
		'dest'	 => 'http/lawyers.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/leadership.php' => array(
		'dest'	 => 'http/leadership.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/league_of_women_voters.php' => array(
		'dest'	 => 'http/league_of_women_voters.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/league_of_women_voters_making_democracy_work.php' => array(
		'dest'	 => 'http/league_of_women_voters_making_democracy_work.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/league_of_women_voters_of_colorado.php' => array(
		'dest'	 => 'http/league_of_women_voters_of_colorado.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/lessig_our_democracy_no_longer_represents_the_people.php' => array(
		'dest'	 => 'http/lessig_our_democracy_no_longer_represents_the_people.html',
		'API'	 => '1',
		'stars'	 => '4',
	),
	'src/let_them_eat_cake.php' => array(
		'dest'	 => 'http/let_them_eat_cake.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/lev_parnas.php' => array(
		'dest'	 => 'http/lev_parnas.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/liberia.php' => array(
		'dest'	 => 'http/liberia.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/liberty_medal.php' => array(
		'dest'	 => 'http/liberty_medal.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/list_of_awards.php' => array(
		'dest'	 => 'http/list_of_awards.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/list_of_books.php' => array(
		'dest'	 => 'http/list_of_books.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/list_of_documentaries.php' => array(
		'dest'	 => 'http/list_of_documentaries.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/list_of_indices.php' => array(
		'dest'	 => 'http/list_of_indices.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/list_of_movies.php' => array(
		'dest'	 => 'http/list_of_movies.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/list_of_organisations.php' => array(
		'dest'	 => 'http/list_of_organisations.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/list_of_people.php' => array(
		'dest'	 => 'http/list_of_people.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/list_of_research_and_reports.php' => array(
		'dest'	 => 'http/list_of_research_and_reports.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/list_of_taxes.php' => array(
		'dest'	 => 'http/list_of_taxes.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/list_of_videos.php' => array(
		'dest'	 => 'http/list_of_videos.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/lists.php' => array(
		'dest'	 => 'http/lists.html',
		'API'	 => '1',
		'stars'	 => '2',
	),
	'src/living.php' => array(
		'dest'	 => 'http/living.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/living_together.php' => array(
		'dest'	 => 'http/living_together.html',
		'API'	 => '1',
		'stars'	 => '2',
	),
	'src/logical_fallacies.php' => array(
		'dest'	 => 'http/logical_fallacies.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/long_walk_to_freedom.php' => array(
		'dest'	 => 'http/long_walk_to_freedom.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/loper_bright_enterprises_v_raimondo.php' => array(
		'dest'	 => 'http/loper_bright_enterprises_v_raimondo.html',
		'API'	 => '1',
		'stars'	 => '2',
	),
	'src/louis_brandeis.php' => array(
		'dest'	 => 'http/louis_brandeis.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/loving_v_virginia.php' => array(
		'dest'	 => 'http/loving_v_virginia.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/lucky_loser_how_donald_trump_squandered_his_father_s_fortune_and_created_the_illusion_of_success.php' => array(
		'dest'	 => 'http/lucky_loser_how_donald_trump_squandered_his_father_s_fortune_and_created_the_illusion_of_success.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/making_democracy_work.php' => array(
		'dest'	 => 'http/making_democracy_work.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/making_democracy_work_civic_traditions_in_modern_italy.php' => array(
		'dest'	 => 'http/making_democracy_work_civic_traditions_in_modern_italy.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/malala_yousafzai.php' => array(
		'dest'	 => 'http/malala_yousafzai.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/malaysia.php' => array(
		'dest'	 => 'http/malaysia.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/mali.php' => array(
		'dest'	 => 'http/mali.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/manisha_sinha.php' => array(
		'dest'	 => 'http/manisha_sinha.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/marc_elias.php' => array(
		'dest'	 => 'http/marc_elias.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/margaret_hoover.php' => array(
		'dest'	 => 'http/margaret_hoover.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/maria_ressa.php' => array(
		'dest'	 => 'http/maria_ressa.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/marriage.php' => array(
		'dest'	 => 'http/marriage.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/mary_mccord.php' => array(
		'dest'	 => 'http/mary_mccord.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/mary_smith.php' => array(
		'dest'	 => 'http/mary_smith.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/maslow_s_hierarchy_of_needs.php' => array(
		'dest'	 => 'http/maslow_s_hierarchy_of_needs.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/mass_shootings_the_human_factor.php' => array(
		'dest'	 => 'http/mass_shootings_the_human_factor.html',
		'API'	 => '1',
		'stars'	 => '3',
	),
	'src/matthew_seligman.php' => array(
		'dest'	 => 'http/matthew_seligman.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/maurice_allais.php' => array(
		'dest'	 => 'http/maurice_allais.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/maurice_duverger.php' => array(
		'dest'	 => 'http/maurice_duverger.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/mcculloch_v_maryland.php' => array(
		'dest'	 => 'http/mcculloch_v_maryland.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/mcdonald_v_city_of_chicago.php' => array(
		'dest'	 => 'http/mcdonald_v_city_of_chicago.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/media.php' => array(
		'dest'	 => 'http/media.html',
		'API'	 => '1',
		'stars'	 => '3',
	),
	'src/menu.php' => array(
		'dest'	 => 'http/menu.html',
		'API'	 => '1',
		'stars'	 => '-1',
	),
	'src/mexico.php' => array(
		'dest'	 => 'http/mexico.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/michael_honey.php' => array(
		'dest'	 => 'http/michael_honey.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/michael_luttig.php' => array(
		'dest'	 => 'http/michael_luttig.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/michael_schmidt.php' => array(
		'dest'	 => 'http/michael_schmidt.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/michael_schmidt_how_donald_trump_s_threats_led_to_investigations_and_audits_of_his_enemies_while_he_was_president.php' => array(
		'dest'	 => 'http/michael_schmidt_how_donald_trump_s_threats_led_to_investigations_and_audits_of_his_enemies_while_he_was_president.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/migrant_workers.php' => array(
		'dest'	 => 'http/migrant_workers.html',
		'API'	 => '1',
		'stars'	 => '2',
	),
	'src/migration_law.php' => array(
		'dest'	 => 'http/migration_law.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/miles_taylor.php' => array(
		'dest'	 => 'http/miles_taylor.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/military.php' => array(
		'dest'	 => 'http/military.html',
		'API'	 => '1',
		'stars'	 => '2',
	),
	'src/military_and_security_developments_involving_the_people_s_republic_of_china_2023_dod_report.php' => array(
		'dest'	 => 'http/military_and_security_developments_involving_the_people_s_republic_of_china_2023_dod_report.html',
		'API'	 => '1',
		'stars'	 => '2',
	),
	'src/miranda_v_arizona.php' => array(
		'dest'	 => 'http/miranda_v_arizona.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/missing_migrants_project.php' => array(
		'dest'	 => 'http/missing_migrants_project.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/mo_gawdat.php' => array(
		'dest'	 => 'http/mo_gawdat.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/montesquieu.php' => array(
		'dest'	 => 'http/montesquieu.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/museum_of_political_corruption.php' => array(
		'dest'	 => 'http/museum_of_political_corruption.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/myanmar.php' => array(
		'dest'	 => 'http/myanmar.html',
		'API'	 => '1',
		'stars'	 => '2',
	),
	'src/myanmar_the_rebel_army_arte_tv_documentary.php' => array(
		'dest'	 => 'http/myanmar_the_rebel_army_arte_tv_documentary.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/narges_mohammadi.php' => array(
		'dest'	 => 'http/narges_mohammadi.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/national_chengchi_university.php' => array(
		'dest'	 => 'http/national_chengchi_university.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/national_coalition_against_censorship.php' => array(
		'dest'	 => 'http/national_coalition_against_censorship.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/national_constitution_center.php' => array(
		'dest'	 => 'http/national_constitution_center.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/national_constitution_center_a_conversation_with_justice_neil_gorsuch_on_the_human_toll_of_too_much_law.php' => array(
		'dest'	 => 'http/national_constitution_center_a_conversation_with_justice_neil_gorsuch_on_the_human_toll_of_too_much_law.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/national_constitution_center_the_2024_liberty_medal_ceremony_honoring_ken_burns.php' => array(
		'dest'	 => 'http/national_constitution_center_the_2024_liberty_medal_ceremony_honoring_ken_burns.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/national_constitution_center_we_the_people.php' => array(
		'dest'	 => 'http/national_constitution_center_we_the_people.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/national_federation_of_independent_business_v_sebelius.php' => array(
		'dest'	 => 'http/national_federation_of_independent_business_v_sebelius.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/national_immigrant_justice_center.php' => array(
		'dest'	 => 'http/national_immigrant_justice_center.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/neil_gorsuch.php' => array(
		'dest'	 => 'http/neil_gorsuch.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/nelson_mandela.php' => array(
		'dest'	 => 'http/nelson_mandela.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/new_zealand.php' => array(
		'dest'	 => 'http/new_zealand.html',
		'API'	 => '1',
		'stars'	 => '2',
	),
	'src/nexus_a_brief_history_of_information_networks_from_the_stone_age_to_ai.php' => array(
		'dest'	 => 'http/nexus_a_brief_history_of_information_networks_from_the_stone_age_to_ai.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/nicaragua.php' => array(
		'dest'	 => 'http/nicaragua.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/niger.php' => array(
		'dest'	 => 'http/niger.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/nineteen_eighty_four.php' => array(
		'dest'	 => 'http/nineteen_eighty_four.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/no_democracy_lasts_forever.php' => array(
		'dest'	 => 'http/no_democracy_lasts_forever.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/nobel_peace_prize.php' => array(
		'dest'	 => 'http/nobel_peace_prize.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/north_american_congress_on_latin_america.php' => array(
		'dest'	 => 'http/north_american_congress_on_latin_america.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/obergefell_v_hodges.php' => array(
		'dest'	 => 'http/obergefell_v_hodges.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/obligations_in_democracy.php' => array(
		'dest'	 => 'http/obligations_in_democracy.html',
		'API'	 => '1',
		'stars'	 => '2',
	),
	'src/observer_expectancy_effect.php' => array(
		'dest'	 => 'http/observer_expectancy_effect.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/obstruction_of_justice.php' => array(
		'dest'	 => 'http/obstruction_of_justice.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/oecd.php' => array(
		'dest'	 => 'http/oecd.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/offshoring.php' => array(
		'dest'	 => 'http/offshoring.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/oligarchy.php' => array(
		'dest'	 => 'http/oligarchy.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/one_country_two_systems.php' => array(
		'dest'	 => 'http/one_country_two_systems.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/open_source_democracy.php' => array(
		'dest'	 => 'http/open_source_democracy.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/open_source_voting_system.php' => array(
		'dest'	 => 'http/open_source_voting_system.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/orange_revolution.php' => array(
		'dest'	 => 'http/orange_revolution.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/organic_taxes.php' => array(
		'dest'	 => 'http/organic_taxes.html',
		'API'	 => '1',
		'stars'	 => '3',
	),
	'src/organic_taxes_the_cure_for_healthcare_funding.php' => array(
		'dest'	 => 'http/organic_taxes_the_cure_for_healthcare_funding.html',
		'API'	 => '1',
		'stars'	 => '3',
	),
	'src/organization_of_american_states.php' => array(
		'dest'	 => 'http/organization_of_american_states.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/orwellian_doublespeak.php' => array(
		'dest'	 => 'http/orwellian_doublespeak.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/osce.php' => array(
		'dest'	 => 'http/osce.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/over_ruled_the_human_toll_of_too_much_law.php' => array(
		'dest'	 => 'http/over_ruled_the_human_toll_of_too_much_law.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/oyez_project.php' => array(
		'dest'	 => 'http/oyez_project.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/pacer_public_access_to_court_electronic_records.php' => array(
		'dest'	 => 'http/pacer_public_access_to_court_electronic_records.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/pakistan.php' => array(
		'dest'	 => 'http/pakistan.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/palestine.php' => array(
		'dest'	 => 'http/palestine.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/panama.php' => array(
		'dest'	 => 'http/panama.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/payton_mcgriff.php' => array(
		'dest'	 => 'http/payton_mcgriff.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/peace.php' => array(
		'dest'	 => 'http/peace.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/peace_pilgrim.php' => array(
		'dest'	 => 'http/peace_pilgrim.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/peaceful_resistance_in_times_of_war.php' => array(
		'dest'	 => 'http/peaceful_resistance_in_times_of_war.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/permission_structures.php' => array(
		'dest'	 => 'http/permission_structures.html',
		'API'	 => '1',
		'stars'	 => '2',
	),
	'src/personal_responsibility.php' => array(
		'dest'	 => 'http/personal_responsibility.html',
		'API'	 => '1',
		'stars'	 => '3',
	),
	'src/personal_responsibility_rhetoric.php' => array(
		'dest'	 => 'http/personal_responsibility_rhetoric.html',
		'API'	 => '1',
		'stars'	 => '3',
	),
	'src/philippine_human_rights_act.php' => array(
		'dest'	 => 'http/philippine_human_rights_act.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/philippines.php' => array(
		'dest'	 => 'http/philippines.html',
		'API'	 => '1',
		'stars'	 => '2',
	),
	'src/pigouvian_tax.php' => array(
		'dest'	 => 'http/pigouvian_tax.html',
		'API'	 => '1',
		'stars'	 => '3',
	),
	'src/plessy_v_ferguson.php' => array(
		'dest'	 => 'http/plessy_v_ferguson.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/plurality_voting.php' => array(
		'dest'	 => 'http/plurality_voting.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/political_discourse.php' => array(
		'dest'	 => 'http/political_discourse.html',
		'API'	 => '1',
		'stars'	 => '2',
	),
	'src/political_polarisation.php' => array(
		'dest'	 => 'http/political_polarisation.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/political_repression.php' => array(
		'dest'	 => 'http/political_repression.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/political_science_in_taiwan.php' => array(
		'dest'	 => 'http/political_science_in_taiwan.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/politics.php' => array(
		'dest'	 => 'http/politics.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/pollution.php' => array(
		'dest'	 => 'http/pollution.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/poor_people_s_campaign_a_national_call_for_a_moral_revival.php' => array(
		'dest'	 => 'http/poor_people_s_campaign_a_national_call_for_a_moral_revival.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/populism.php' => array(
		'dest'	 => 'http/populism.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/poverty.php' => array(
		'dest'	 => 'http/poverty.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/prc_china.php' => array(
		'dest'	 => 'http/prc_china.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/prequel_an_american_fight_against_fascism.php' => array(
		'dest'	 => 'http/prequel_an_american_fight_against_fascism.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/president_as_saviour_syndrome.php' => array(
		'dest'	 => 'http/president_as_saviour_syndrome.html',
		'API'	 => '1',
		'stars'	 => '4',
	),
	'src/president_biden_farewell_address.php' => array(
		'dest'	 => 'http/president_biden_farewell_address.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/president_biden_interview_with_lawrence_o_donnell_january_2025.php' => array(
		'dest'	 => 'http/president_biden_interview_with_lawrence_o_donnell_january_2025.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/president_of_the_united_states.php' => array(
		'dest'	 => 'http/president_of_the_united_states.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/primary_elections.php' => array(
		'dest'	 => 'http/primary_elections.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/professionalism_without_elitism.php' => array(
		'dest'	 => 'http/professionalism_without_elitism.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/profiles_in_ignorance_how_america_s_politicians_got_dumb_and_dumber.php' => array(
		'dest'	 => 'http/profiles_in_ignorance_how_america_s_politicians_got_dumb_and_dumber.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/profitable_factories_shutting_down.php' => array(
		'dest'	 => 'http/profitable_factories_shutting_down.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/progress_and_poverty.php' => array(
		'dest'	 => 'http/progress_and_poverty.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/project/codeberg.php' => array(
		'dest'	 => 'http/project/codeberg.html',
		'API'	 => '1',
		'stars'	 => '2',
	),
	'src/project/copyright.php' => array(
		'dest'	 => 'http/project/copyright.html',
		'API'	 => '1',
		'stars'	 => '3',
	),
	'src/project/development_scripts.php' => array(
		'dest'	 => 'http/project/development_scripts.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/project/development_website.php' => array(
		'dest'	 => 'http/project/development_website.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/project/git.php' => array(
		'dest'	 => 'http/project/git.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/project/index.php' => array(
		'dest'	 => 'http/project/index.html',
		'API'	 => '1',
		'stars'	 => '3',
	),
	'src/project/participate.php' => array(
		'dest'	 => 'http/project/participate.html',
		'API'	 => '1',
		'stars'	 => '3',
	),
	'src/project/sister_projects.php' => array(
		'dest'	 => 'http/project/sister_projects.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/project/standing_on_the_shoulders_of_giants.php' => array(
		'dest'	 => 'http/project/standing_on_the_shoulders_of_giants.html',
		'API'	 => '1',
		'stars'	 => '2',
	),
	'src/project/taiwan_democracy.php' => array(
		'dest'	 => 'http/project/taiwan_democracy.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/project/updates.php' => array(
		'dest'	 => 'http/project/updates.html',
		'API'	 => '1',
		'stars'	 => '-1',
	),
	'src/project/videos_tv_programs_interviews_and_documentaries.php' => array(
		'dest'	 => 'http/project/videos_tv_programs_interviews_and_documentaries.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/project/vim.php' => array(
		'dest'	 => 'http/project/vim.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/project/wikipedia.php' => array(
		'dest'	 => 'http/project/wikipedia.html',
		'API'	 => '1',
		'stars'	 => '2',
	),
	'src/project_2025.php' => array(
		'dest'	 => 'http/project_2025.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/project_honey_pot.php' => array(
		'dest'	 => 'http/project_honey_pot.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/propaganda.php' => array(
		'dest'	 => 'http/propaganda.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/property.php' => array(
		'dest'	 => 'http/property.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/property_tax.php' => array(
		'dest'	 => 'http/property_tax.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/propublica.php' => array(
		'dest'	 => 'http/propublica.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/prosecuting_donald_trump.php' => array(
		'dest'	 => 'http/prosecuting_donald_trump.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/prosecuting_donald_trump_election_security_matters.php' => array(
		'dest'	 => 'http/prosecuting_donald_trump_election_security_matters.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/public_broadcasting_service.php' => array(
		'dest'	 => 'http/public_broadcasting_service.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/quantity_of_information.php' => array(
		'dest'	 => 'http/quantity_of_information.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/rachel_maddow.php' => array(
		'dest'	 => 'http/rachel_maddow.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/rachel_maddow_interviews_lev_parnas_part_1.php' => array(
		'dest'	 => 'http/rachel_maddow_interviews_lev_parnas_part_1.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/rachel_maddow_interviews_lev_parnas_part_2.php' => array(
		'dest'	 => 'http/rachel_maddow_interviews_lev_parnas_part_2.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/racism.php' => array(
		'dest'	 => 'http/racism.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/ranked_voting.php' => array(
		'dest'	 => 'http/ranked_voting.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/rated_voting.php' => array(
		'dest'	 => 'http/rated_voting.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/redistribution_of_wealth_a_false_solution.php' => array(
		'dest'	 => 'http/redistribution_of_wealth_a_false_solution.html',
		'API'	 => '1',
		'stars'	 => '3',
	),
	'src/refugee_camps.php' => array(
		'dest'	 => 'http/refugee_camps.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/refugees.php' => array(
		'dest'	 => 'http/refugees.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/religion.php' => array(
		'dest'	 => 'http/religion.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/renee_diresta.php' => array(
		'dest'	 => 'http/renee_diresta.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/renewable_energy.php' => array(
		'dest'	 => 'http/renewable_energy.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/reporters_committee_for_freedom_of_the_press.php' => array(
		'dest'	 => 'http/reporters_committee_for_freedom_of_the_press.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/reporters_without_borders.php' => array(
		'dest'	 => 'http/reporters_without_borders.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/results_organization.php' => array(
		'dest'	 => 'http/results_organization.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/revolution_of_dignity.php' => array(
		'dest'	 => 'http/revolution_of_dignity.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/richard_bluhm.php' => array(
		'dest'	 => 'http/richard_bluhm.html',
		'API'	 => '1',
		'stars'	 => '2',
	),
	'src/richard_daynard.php' => array(
		'dest'	 => 'http/richard_daynard.html',
		'API'	 => '1',
		'stars'	 => '2',
	),
	'src/richard_hasen.php' => array(
		'dest'	 => 'http/richard_hasen.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/richard_stengel.php' => array(
		'dest'	 => 'http/richard_stengel.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/right_to_marriage.php' => array(
		'dest'	 => 'http/right_to_marriage.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/rights.php' => array(
		'dest'	 => 'http/rights.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/robert_putnam.php' => array(
		'dest'	 => 'http/robert_putnam.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/robert_reich.php' => array(
		'dest'	 => 'http/robert_reich.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/robert_reich_the_republican_party_is_over.php' => array(
		'dest'	 => 'http/robert_reich_the_republican_party_is_over.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/robert_reich_you_are_being_lied_to_about_inflation.php' => array(
		'dest'	 => 'http/robert_reich_you_are_being_lied_to_about_inflation.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/roe_v_wade.php' => array(
		'dest'	 => 'http/roe_v_wade.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/rohingya_people.php' => array(
		'dest'	 => 'http/rohingya_people.html',
		'API'	 => '1',
		'stars'	 => '2',
	),
	'src/rolf_dobelli.php' => array(
		'dest'	 => 'http/rolf_dobelli.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/russia.php' => array(
		'dest'	 => 'http/russia.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/russian_interference_in_us_elections.php' => array(
		'dest'	 => 'http/russian_interference_in_us_elections.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/ruth_ben_ghiat.php' => array(
		'dest'	 => 'http/ruth_ben_ghiat.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/rwanda.php' => array(
		'dest'	 => 'http/rwanda.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/saints_and_little_devils.php' => array(
		'dest'	 => 'http/saints_and_little_devils.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/sarah_longwell.php' => array(
		'dest'	 => 'http/sarah_longwell.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/sarcasm.php' => array(
		'dest'	 => 'http/sarcasm.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/saudi_arabia.php' => array(
		'dest'	 => 'http/saudi_arabia.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/schumacher_center_for_a_new_economics.php' => array(
		'dest'	 => 'http/schumacher_center_for_a_new_economics.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/score_voting.php' => array(
		'dest'	 => 'http/score_voting.html',
		'API'	 => '1',
		'stars'	 => '3',
	),
	'src/sean_penn.php' => array(
		'dest'	 => 'http/sean_penn.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/second_level_of_democracy.php' => array(
		'dest'	 => 'http/second_level_of_democracy.html',
		'API'	 => '1',
		'stars'	 => '4',
	),
	'src/separation_of_powers.php' => array(
		'dest'	 => 'http/separation_of_powers.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/seventh_level_of_democracy.php' => array(
		'dest'	 => 'http/seventh_level_of_democracy.html',
		'API'	 => '1',
		'stars'	 => '2',
	),
	'src/shameless_republicans_deliberate_dysfunction_and_the_battle_to_preserve_democracy.php' => array(
		'dest'	 => 'http/shameless_republicans_deliberate_dysfunction_and_the_battle_to_preserve_democracy.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/sharon_brous.php' => array(
		'dest'	 => 'http/sharon_brous.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/shelby_county_v_holder.php' => array(
		'dest'	 => 'http/shelby_county_v_holder.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/single_choice_voting.php' => array(
		'dest'	 => 'http/single_choice_voting.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/sixth_level_of_democracy.php' => array(
		'dest'	 => 'http/sixth_level_of_democracy.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/small_government.php' => array(
		'dest'	 => 'http/small_government.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/small_is_beautiful.php' => array(
		'dest'	 => 'http/small_is_beautiful.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/social_capital.php' => array(
		'dest'	 => 'http/social_capital.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/social_dress_code.php' => array(
		'dest'	 => 'http/social_dress_code.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/social_justice.php' => array(
		'dest'	 => 'http/social_justice.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/social_media_and_democracy.php' => array(
		'dest'	 => 'http/social_media_and_democracy.html',
		'API'	 => '1',
		'stars'	 => '2',
	),
	'src/social_media_and_election_integrity.php' => array(
		'dest'	 => 'http/social_media_and_election_integrity.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/social_media_ban.php' => array(
		'dest'	 => 'http/social_media_ban.html',
		'API'	 => '1',
		'stars'	 => '2',
	),
	'src/social_networks.php' => array(
		'dest'	 => 'http/social_networks.html',
		'API'	 => '1',
		'stars'	 => '2',
	),
	'src/society.php' => array(
		'dest'	 => 'http/society.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/somalia.php' => array(
		'dest'	 => 'http/somalia.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/soochow_university.php' => array(
		'dest'	 => 'http/soochow_university.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/south_africa.php' => array(
		'dest'	 => 'http/south_africa.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/south_mountain_company.php' => array(
		'dest'	 => 'http/south_mountain_company.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/special_counsel_jack_smith_report_on_2020_election_subversion_efforts.php' => array(
		'dest'	 => 'http/special_counsel_jack_smith_report_on_2020_election_subversion_efforts.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/spirit.php' => array(
		'dest'	 => 'http/spirit.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/springfield_ohio_cat_eating_hoax.php' => array(
		'dest'	 => 'http/springfield_ohio_cat_eating_hoax.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/staffan_lindberg.php' => array(
		'dest'	 => 'http/staffan_lindberg.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/stanford_internet_observatory.php' => array(
		'dest'	 => 'http/stanford_internet_observatory.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/steve_ballmer.php' => array(
		'dest'	 => 'http/steve_ballmer.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/steven_hahn.php' => array(
		'dest'	 => 'http/steven_hahn.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/steven_hassan.php' => array(
		'dest'	 => 'http/steven_hassan.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/stochastic_terrorism.php' => array(
		'dest'	 => 'http/stochastic_terrorism.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/strongmen_mussolini_to_the_present.php' => array(
		'dest'	 => 'http/strongmen_mussolini_to_the_present.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/style_her_empowered.php' => array(
		'dest'	 => 'http/style_her_empowered.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/subsidiarity.php' => array(
		'dest'	 => 'http/subsidiarity.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/supreme_court.php' => array(
		'dest'	 => 'http/supreme_court.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/supreme_court_of_the_united_states.php' => array(
		'dest'	 => 'http/supreme_court_of_the_united_states.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/sweden.php' => array(
		'dest'	 => 'http/sweden.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/systemic_justice_project.php' => array(
		'dest'	 => 'http/systemic_justice_project.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/tactical_voting.php' => array(
		'dest'	 => 'http/tactical_voting.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/taiwan.php' => array(
		'dest'	 => 'http/taiwan.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/taiwan_election_and_democratization_study.php' => array(
		'dest'	 => 'http/taiwan_election_and_democratization_study.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/taiwan_ukraine_relations.php' => array(
		'dest'	 => 'http/taiwan_ukraine_relations.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/talent_is_evenly_distributed_worldwide_but_opportunity_is_not.php' => array(
		'dest'	 => 'http/talent_is_evenly_distributed_worldwide_but_opportunity_is_not.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/tanzania.php' => array(
		'dest'	 => 'http/tanzania.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/tax_on_gambling.php' => array(
		'dest'	 => 'http/tax_on_gambling.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/tax_renaissance.php' => array(
		'dest'	 => 'http/tax_renaissance.html',
		'API'	 => '1',
		'stars'	 => '2',
	),
	'src/taxation_of_labor_vs_taxation_of_capital.php' => array(
		'dest'	 => 'http/taxation_of_labor_vs_taxation_of_capital.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/taxes.php' => array(
		'dest'	 => 'http/taxes.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/taxes_in_taiwan.php' => array(
		'dest'	 => 'http/taxes_in_taiwan.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/teaching_democracy.php' => array(
		'dest'	 => 'http/teaching_democracy.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/teamwork.php' => array(
		'dest'	 => 'http/teamwork.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/tech_policy_press.php' => array(
		'dest'	 => 'http/tech_policy_press.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/technology.php' => array(
		'dest'	 => 'http/technology.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/technology_and_democracy.php' => array(
		'dest'	 => 'http/technology_and_democracy.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/ted_talk_the_art_of_misdirection.php' => array(
		'dest'	 => 'http/ted_talk_the_art_of_misdirection.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/ted_talks.php' => array(
		'dest'	 => 'http/ted_talks.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/texas.php' => array(
		'dest'	 => 'http/texas.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/the_american_journalist_under_attack_media_trust_and_democracy.php' => array(
		'dest'	 => 'http/the_american_journalist_under_attack_media_trust_and_democracy.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/the_art_of_thinking_clearly.php' => array(
		'dest'	 => 'http/the_art_of_thinking_clearly.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/the_big_truth_upholding_democracy_in_the_age_of_the_big_lie.php' => array(
		'dest'	 => 'http/the_big_truth_upholding_democracy_in_the_age_of_the_big_lie.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/the_children_s_peace_movement_columbia.php' => array(
		'dest'	 => 'http/the_children_s_peace_movement_columbia.html',
		'API'	 => '1',
		'stars'	 => '2',
	),
	'src/the_cult_of_trump_a_leading_cult_expert_explains_how_the_president_uses_mind_control.php' => array(
		'dest'	 => 'http/the_cult_of_trump_a_leading_cult_expert_explains_how_the_president_uses_mind_control.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/the_despot_s_apprentice_donald_trump_s_attack_on_democracy.php' => array(
		'dest'	 => 'http/the_despot_s_apprentice_donald_trump_s_attack_on_democracy.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/the_difference_between_the_impossible_and_the_possible_is_merely_a_measure_of_man_s_determination.php' => array(
		'dest'	 => 'http/the_difference_between_the_impossible_and_the_possible_is_merely_a_measure_of_man_s_determination.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/the_end_of_political_parties.php' => array(
		'dest'	 => 'http/the_end_of_political_parties.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/the_final_minutes_of_president_obama_s_farewell_address_yes_we_can.php' => array(
		'dest'	 => 'http/the_final_minutes_of_president_obama_s_farewell_address_yes_we_can.html',
		'API'	 => '1',
		'stars'	 => '3',
	),
	'src/the_four_agreements.php' => array(
		'dest'	 => 'http/the_four_agreements.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/the_great_dictator.php' => array(
		'dest'	 => 'http/the_great_dictator.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/the_hidden_dimension_in_democracy.php' => array(
		'dest'	 => 'http/the_hidden_dimension_in_democracy.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/the_i_dont_know_revolution.php' => array(
		'dest'	 => 'http/the_i_dont_know_revolution.html',
		'API'	 => '1',
		'stars'	 => '3',
	),
	'src/the_monk_and_the_gun.php' => array(
		'dest'	 => 'http/the_monk_and_the_gun.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/the_power_of_example_improving_our_democracies.php' => array(
		'dest'	 => 'http/the_power_of_example_improving_our_democracies.html',
		'API'	 => '1',
		'stars'	 => '3',
	),
	'src/the_remains_of_the_day.php' => array(
		'dest'	 => 'http/the_remains_of_the_day.html',
		'API'	 => '1',
		'stars'	 => '3',
	),
	'src/the_righteous_mind.php' => array(
		'dest'	 => 'http/the_righteous_mind.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/the_sixth.php' => array(
		'dest'	 => 'http/the_sixth.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/the_social_cost_of_donald_trump_political_carreer.php' => array(
		'dest'	 => 'http/the_social_cost_of_donald_trump_political_carreer.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/the_soloist_and_the_choir.php' => array(
		'dest'	 => 'http/the_soloist_and_the_choir.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/the_two_sides_of_democratic_dysfunction.php' => array(
		'dest'	 => 'http/the_two_sides_of_democratic_dysfunction.html',
		'API'	 => '1',
		'stars'	 => '4',
	),
	'src/the_watermelons_soldiers_spying_for_pro_democracy_rebels_bbc_world_service_documentary.php' => array(
		'dest'	 => 'http/the_watermelons_soldiers_spying_for_pro_democracy_rebels_bbc_world_service_documentary.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/think_like_a_voter.php' => array(
		'dest'	 => 'http/think_like_a_voter.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/third_level_of_democracy.php' => array(
		'dest'	 => 'http/third_level_of_democracy.html',
		'API'	 => '1',
		'stars'	 => '4',
	),
	'src/thomas_paine.php' => array(
		'dest'	 => 'http/thomas_paine.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/thomas_piketty.php' => array(
		'dest'	 => 'http/thomas_piketty.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/transnational_authoritarianism.php' => array(
		'dest'	 => 'http/transnational_authoritarianism.html',
		'API'	 => '1',
		'stars'	 => '2',
	),
	'src/transparency_international.php' => array(
		'dest'	 => 'http/transparency_international.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/truemedia_org.php' => array(
		'dest'	 => 'http/truemedia_org.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/trump_v_united_states.php' => array(
		'dest'	 => 'http/trump_v_united_states.html',
		'API'	 => '1',
		'stars'	 => '2',
	),
	'src/trump_v_vance.php' => array(
		'dest'	 => 'http/trump_v_vance.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/truth_and_reality_with_chinese_characteristics.php' => array(
		'dest'	 => 'http/truth_and_reality_with_chinese_characteristics.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/tsai_ing_wen.php' => array(
		'dest'	 => 'http/tsai_ing_wen.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/tunisia.php' => array(
		'dest'	 => 'http/tunisia.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/turkey.php' => array(
		'dest'	 => 'http/turkey.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/tweed_symptom_uncontested_elections.php' => array(
		'dest'	 => 'http/tweed_symptom_uncontested_elections.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/tweed_syndrome.php' => array(
		'dest'	 => 'http/tweed_syndrome.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/tweed_syndrome_primaries.php' => array(
		'dest'	 => 'http/tweed_syndrome_primaries.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/tweed_syndrome_systemic_corruption.php' => array(
		'dest'	 => 'http/tweed_syndrome_systemic_corruption.html',
		'API'	 => '1',
		'stars'	 => '4',
	),
	'src/tweedism.php' => array(
		'dest'	 => 'http/tweedism.html',
		'API'	 => '1',
		'stars'	 => '3',
	),
	'src/twilight_of_democracy.php' => array(
		'dest'	 => 'http/twilight_of_democracy.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/types_of_democracy.php' => array(
		'dest'	 => 'http/types_of_democracy.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/ukraine.php' => array(
		'dest'	 => 'http/ukraine.html',
		'API'	 => '1',
		'stars'	 => '2',
	),
	'src/unicef.php' => array(
		'dest'	 => 'http/unicef.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/united_arab_emirates.php' => array(
		'dest'	 => 'http/united_arab_emirates.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/united_auto_workers.php' => array(
		'dest'	 => 'http/united_auto_workers.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/united_for_peace_and_justice.php' => array(
		'dest'	 => 'http/united_for_peace_and_justice.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/united_nations.php' => array(
		'dest'	 => 'http/united_nations.html',
		'API'	 => '1',
		'stars'	 => '2',
	),
	'src/united_nations_high_commissioner_for_refugees.php' => array(
		'dest'	 => 'http/united_nations_high_commissioner_for_refugees.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/united_nations_human_rights_office.php' => array(
		'dest'	 => 'http/united_nations_human_rights_office.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/united_nations_office_for_the_coordination_of_humanitarian_affairs.php' => array(
		'dest'	 => 'http/united_nations_office_for_the_coordination_of_humanitarian_affairs.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/united_nations_regional_information_centre.php' => array(
		'dest'	 => 'http/united_nations_regional_information_centre.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/united_nations_volunteers.php' => array(
		'dest'	 => 'http/united_nations_volunteers.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/united_states.php' => array(
		'dest'	 => 'http/united_states.html',
		'API'	 => '1',
		'stars'	 => '2',
	),
	'src/universal_basic_income.php' => array(
		'dest'	 => 'http/universal_basic_income.html',
		'API'	 => '1',
		'stars'	 => '4',
	),
	'src/unsung_heroes_of_democracy_award.php' => array(
		'dest'	 => 'http/unsung_heroes_of_democracy_award.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/us_states.php' => array(
		'dest'	 => 'http/us_states.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/us_taiwan_relations_in_a_new_era.php' => array(
		'dest'	 => 'http/us_taiwan_relations_in_a_new_era.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/usafacts.php' => array(
		'dest'	 => 'http/usafacts.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/v_dem_institute.php' => array(
		'dest'	 => 'http/v_dem_institute.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/verified_voting_foundation.php' => array(
		'dest'	 => 'http/verified_voting_foundation.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/vietnam.php' => array(
		'dest'	 => 'http/vietnam.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/vision_of_humanity.php' => array(
		'dest'	 => 'http/vision_of_humanity.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/volodymyr_zelenskyy.php' => array(
		'dest'	 => 'http/volodymyr_zelenskyy.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/vote_smart.php' => array(
		'dest'	 => 'http/vote_smart.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/voter_suppression.php' => array(
		'dest'	 => 'http/voter_suppression.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/voting_methods.php' => array(
		'dest'	 => 'http/voting_methods.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/we_are_the_leaders_we_have_been_looking_for.php' => array(
		'dest'	 => 'http/we_are_the_leaders_we_have_been_looking_for.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/wealth.php' => array(
		'dest'	 => 'http/wealth.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/wealth_inequality_in_america.php' => array(
		'dest'	 => 'http/wealth_inequality_in_america.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/weaponization_of_the_justice_department_by_donald_trump.php' => array(
		'dest'	 => 'http/weaponization_of_the_justice_department_by_donald_trump.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/web.php' => array(
		'dest'	 => 'http/web.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/wedge_issues.php' => array(
		'dest'	 => 'http/wedge_issues.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/what_democracy_can_learn_from_the_aviation_industry.php' => array(
		'dest'	 => 'http/what_democracy_can_learn_from_the_aviation_industry.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/what_is_populism.php' => array(
		'dest'	 => 'http/what_is_populism.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/where_law_ends.php' => array(
		'dest'	 => 'http/where_law_ends.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/whistleblowers.php' => array(
		'dest'	 => 'http/whistleblowers.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/white_poverty_how_exposing_myths_about_race_and_class_can_reconstruct_american_democracy.php' => array(
		'dest'	 => 'http/white_poverty_how_exposing_myths_about_race_and_class_can_reconstruct_american_democracy.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/why_are_trumpism_and_the_maga_movement_so_successful.php' => array(
		'dest'	 => 'http/why_are_trumpism_and_the_maga_movement_so_successful.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/william_barber_ii.php' => array(
		'dest'	 => 'http/william_barber_ii.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/william_m_tweed.php' => array(
		'dest'	 => 'http/william_m_tweed.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/wip.php' => array(
		'dest'	 => 'http/wip.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/women_s_rights.php' => array(
		'dest'	 => 'http/women_s_rights.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/world.php' => array(
		'dest'	 => 'http/world.html',
		'API'	 => '1',
		'stars'	 => '1',
	),
	'src/world_central_kitchen.php' => array(
		'dest'	 => 'http/world_central_kitchen.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/world_food_programme.php' => array(
		'dest'	 => 'http/world_food_programme.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/world_future_council.php' => array(
		'dest'	 => 'http/world_future_council.html',
		'API'	 => '1',
		'stars'	 => '2',
	),
	'src/world_happiness_report.php' => array(
		'dest'	 => 'http/world_happiness_report.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/world_inequality_database.php' => array(
		'dest'	 => 'http/world_inequality_database.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/world_inequality_lab.php' => array(
		'dest'	 => 'http/world_inequality_lab.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/world_s_children_s_prize_for_the_rights_of_the_child.php' => array(
		'dest'	 => 'http/world_s_children_s_prize_for_the_rights_of_the_child.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
	'src/yuval_noah_harari.php' => array(
		'dest'	 => 'http/yuval_noah_harari.html',
		'API'	 => '1',
		'stars'	 => '0',
	),
);


$preprocess_index_dest = array(
	'60_minutes.html' => array(
		'include'	 => 'src/60_minutes.php',
		'name'	 => 'page',
		'keyword'	 => '60 Minutes',
	),
	'60_minutes_conflict_between_china_philippines_could_involve_u_s_and_lead_to_a_clash_of_superpowers.html' => array(
		'include'	 => 'src/60_minutes_conflict_between_china_philippines_could_involve_u_s_and_lead_to_a_clash_of_superpowers.php',
		'name'	 => 'page',
		'keyword'	 => '60 Minutes: Conflict between China, Philippines could involve U.S. and lead to a clash of superpowers',
	),
	'a_j_muste.html' => array(
		'include'	 => 'src/a_j_muste.php',
		'name'	 => 'page',
		'keyword'	 => 'A. J. Muste',
	),
	'a_night_at_the_garden.html' => array(
		'include'	 => 'src/a_night_at_the_garden.php',
		'name'	 => 'page',
		'keyword'	 => 'A Night at the Garden',
	),
	'aba_task_force_for_american_democracy.html' => array(
		'include'	 => 'src/aba_task_force_for_american_democracy.php',
		'name'	 => 'page',
		'keyword'	 => 'ABA Task Force for American Democracy',
	),
	'accountability.html' => array(
		'include'	 => 'src/accountability.php',
		'name'	 => 'page',
		'keyword'	 => 'Accountability',
	),
	'adav_noti.html' => array(
		'include'	 => 'src/adav_noti.php',
		'name'	 => 'page',
		'keyword'	 => 'Adav Noti',
	),
	'advance_democracy.html' => array(
		'include'	 => 'src/advance_democracy.php',
		'name'	 => 'page',
		'keyword'	 => 'Advance Democracy',
	),
	'advertising.html' => array(
		'include'	 => 'src/advertising.php',
		'name'	 => 'page',
		'keyword'	 => 'Advertising',
	),
	'alexis_de_tocqueville.html' => array(
		'include'	 => 'src/alexis_de_tocqueville.php',
		'name'	 => 'page',
		'keyword'	 => 'Alexis de Tocqueville',
	),
	'algorithms.html' => array(
		'include'	 => 'src/algorithms.php',
		'name'	 => 'page',
		'keyword'	 => 'Algorithms',
	),
	'ali_velshi.html' => array(
		'include'	 => 'src/ali_velshi.php',
		'name'	 => 'page',
		'keyword'	 => 'Ali Velshi',
	),
	'ali_velshi_s_hope_for_the_middle_east_and_humanity.html' => array(
		'include'	 => 'src/ali_velshi_s_hope_for_the_middle_east_and_humanity.php',
		'name'	 => 'page',
		'keyword'	 => 'Ali Velshi\'s hope for the Middle East and humanity',
	),
	'all_labor_has_dignity.html' => array(
		'include'	 => 'src/all_labor_has_dignity.php',
		'name'	 => 'page',
		'keyword'	 => 'All labor has dignity',
	),
	'alleged_bribery_of_donald_trump_by_the_egyptian_president.html' => array(
		'include'	 => 'src/alleged_bribery_of_donald_trump_by_the_egyptian_president.php',
		'name'	 => 'page',
		'keyword'	 => 'Alleged bribery of Donald Trump by the Egyptian president',
	),
	'amber_scorah.html' => array(
		'include'	 => 'src/amber_scorah.php',
		'name'	 => 'page',
		'keyword'	 => 'Amber Scorah',
	),
	'american_bar_association.html' => array(
		'include'	 => 'src/american_bar_association.php',
		'name'	 => 'page',
		'keyword'	 => 'American Bar Association',
	),
	'american_civil_liberties_union.html' => array(
		'include'	 => 'src/american_civil_liberties_union.php',
		'name'	 => 'page',
		'keyword'	 => 'American Civil Liberties Union',
	),
	'american_constitution_society.html' => array(
		'include'	 => 'src/american_constitution_society.php',
		'name'	 => 'page',
		'keyword'	 => 'American Constitution Society',
	),
	'american_fascism.html' => array(
		'include'	 => 'src/american_fascism.php',
		'name'	 => 'page',
		'keyword'	 => 'American Fascism',
	),
	'american_friends_service_committee.html' => array(
		'include'	 => 'src/american_friends_service_committee.php',
		'name'	 => 'page',
		'keyword'	 => 'American Friends Service Committee',
	),
	'american_immigration_council.html' => array(
		'include'	 => 'src/american_immigration_council.php',
		'name'	 => 'page',
		'keyword'	 => 'American Immigration Council',
	),
	'amnesty_international.html' => array(
		'include'	 => 'src/amnesty_international.php',
		'name'	 => 'page',
		'keyword'	 => 'Amnesty International',
	),
	'andrew_weissmann.html' => array(
		'include'	 => 'src/andrew_weissmann.php',
		'name'	 => 'page',
		'keyword'	 => 'Andrew Weissmann',
	),
	'andy_borowitz.html' => array(
		'include'	 => 'src/andy_borowitz.php',
		'name'	 => 'page',
		'keyword'	 => 'Andy Borowitz',
	),
	'anne_applebaum.html' => array(
		'include'	 => 'src/anne_applebaum.php',
		'name'	 => 'page',
		'keyword'	 => 'Anne Applebaum',
	),
	'anne_applebaum_on_autocratic_threats_around_the_world.html' => array(
		'include'	 => 'src/anne_applebaum_on_autocratic_threats_around_the_world.php',
		'name'	 => 'page',
		'keyword'	 => 'Anne Applebaum on Autocratic Threats Around the World',
	),
	'apollo_robbins.html' => array(
		'include'	 => 'src/apollo_robbins.php',
		'name'	 => 'page',
		'keyword'	 => 'Apollo Robbins',
	),
	'appropriate_technology.html' => array(
		'include'	 => 'src/appropriate_technology.php',
		'name'	 => 'page',
		'keyword'	 => 'Appropriate technology',
	),
	'approval_voting.html' => array(
		'include'	 => 'src/approval_voting.php',
		'name'	 => 'page',
		'keyword'	 => 'Approval Voting',
	),
	'approval_voting_party.html' => array(
		'include'	 => 'src/approval_voting_party.php',
		'name'	 => 'page',
		'keyword'	 => 'Approval Voting Party',
	),
	'archives/index.html' => array(
		'include'	 => 'src/archives/index.php',
		'name'	 => 'page',
		'keyword'	 => 'Index',
	),
	'archives/russian_federation_and_people_s_republic_of_china_joint_statement_february_2022.html' => array(
		'include'	 => 'src/archives/russian_federation_and_people_s_republic_of_china_joint_statement_february_2022.php',
		'name'	 => 'page',
		'keyword'	 => 'Russian Federation and People\'s Republic of China joint statement February 2022',
	),
	'argentina.html' => array(
		'include'	 => 'src/argentina.php',
		'name'	 => 'page',
		'keyword'	 => 'Argentina',
	),
	'ari_melber.html' => array(
		'include'	 => 'src/ari_melber.php',
		'name'	 => 'page',
		'keyword'	 => 'Ari Melber',
	),
	'ari_melber_interviews_yuval_harari_debunking_trump_s_lies.html' => array(
		'include'	 => 'src/ari_melber_interviews_yuval_harari_debunking_trump_s_lies.php',
		'name'	 => 'page',
		'keyword'	 => 'Ari Melber interviews Yuval Harari: Debunking Trump\'s lies',
	),
	'arte.html' => array(
		'include'	 => 'src/arte.php',
		'name'	 => 'page',
		'keyword'	 => 'Arte',
	),
	'artificial_intelligence.html' => array(
		'include'	 => 'src/artificial_intelligence.php',
		'name'	 => 'page',
		'keyword'	 => 'Artificial intelligence',
	),
	'asylum_seeker.html' => array(
		'include'	 => 'src/asylum_seeker.php',
		'name'	 => 'page',
		'keyword'	 => 'Asylum seeker',
	),
	'at_war_with_ourselves.html' => array(
		'include'	 => 'src/at_war_with_ourselves.php',
		'name'	 => 'page',
		'keyword'	 => 'At War with Ourselves',
	),
	'australia.html' => array(
		'include'	 => 'src/australia.php',
		'name'	 => 'page',
		'keyword'	 => 'Australia',
	),
	'australian_strategic_policy_institute.html' => array(
		'include'	 => 'src/australian_strategic_policy_institute.php',
		'name'	 => 'page',
		'keyword'	 => 'Australian Strategic Policy Institute',
	),
	'authoritarian_exploitation_of_democratic_weaknesses.html' => array(
		'include'	 => 'src/authoritarian_exploitation_of_democratic_weaknesses.php',
		'name'	 => 'page',
		'keyword'	 => 'Authoritarian Exploitation of Democratic Weaknesses',
	),
	'authoritarianism.html' => array(
		'include'	 => 'src/authoritarianism.php',
		'name'	 => 'page',
		'keyword'	 => 'Authoritarianism',
	),
	'b_lab.html' => array(
		'include'	 => 'src/b_lab.php',
		'name'	 => 'page',
		'keyword'	 => 'B Lab',
	),
	'bail.html' => array(
		'include'	 => 'src/bail.php',
		'name'	 => 'page',
		'keyword'	 => 'Bail',
	),
	'bangladesh.html' => array(
		'include'	 => 'src/bangladesh.php',
		'name'	 => 'page',
		'keyword'	 => 'Bangladesh',
	),
	'barack_obama.html' => array(
		'include'	 => 'src/barack_obama.php',
		'name'	 => 'page',
		'keyword'	 => 'Barack Obama',
	),
	'barack_obama_keynote_address_at_the_2004_democratic_national_convention.html' => array(
		'include'	 => 'src/barack_obama_keynote_address_at_the_2004_democratic_national_convention.php',
		'name'	 => 'page',
		'keyword'	 => 'Barack Obama: Keynote Address at the 2004 Democratic National Convention',
	),
	'bbc_world_service.html' => array(
		'include'	 => 'src/bbc_world_service.php',
		'name'	 => 'page',
		'keyword'	 => 'BBC World Service',
	),
	'belgium.html' => array(
		'include'	 => 'src/belgium.php',
		'name'	 => 'page',
		'keyword'	 => 'Belgium',
	),
	'beliefs.html' => array(
		'include'	 => 'src/beliefs.php',
		'name'	 => 'page',
		'keyword'	 => 'beliefs',
	),
	'berkman_klein_center_for_internet_society.html' => array(
		'include'	 => 'src/berkman_klein_center_for_internet_society.php',
		'name'	 => 'page',
		'keyword'	 => 'Berkman Klein Center for Internet & Society',
	),
	'bhimrao_ramji_ambedkar.html' => array(
		'include'	 => 'src/bhimrao_ramji_ambedkar.php',
		'name'	 => 'page',
		'keyword'	 => 'Bhimrao Ramji Ambedkar',
	),
	'bioneers.html' => array(
		'include'	 => 'src/bioneers.php',
		'name'	 => 'page',
		'keyword'	 => 'Bioneers',
	),
	'blasphemy.html' => array(
		'include'	 => 'src/blasphemy.php',
		'name'	 => 'page',
		'keyword'	 => 'Blasphemy',
	),
	'blowback_a_warning_to_save_democracy.html' => array(
		'include'	 => 'src/blowback_a_warning_to_save_democracy.php',
		'name'	 => 'page',
		'keyword'	 => 'Blowback: A Warning to Save Democracy',
	),
	'bolivia.html' => array(
		'include'	 => 'src/bolivia.php',
		'name'	 => 'page',
		'keyword'	 => 'Bolivia',
	),
	'bootleggers_and_baptists.html' => array(
		'include'	 => 'src/bootleggers_and_baptists.php',
		'name'	 => 'page',
		'keyword'	 => 'Bootleggers and Baptists',
	),
	'branches_of_government.html' => array(
		'include'	 => 'src/branches_of_government.php',
		'name'	 => 'page',
		'keyword'	 => 'Branches of government',
	),
	'brandenburg_v_ohio.html' => array(
		'include'	 => 'src/brandenburg_v_ohio.php',
		'name'	 => 'page',
		'keyword'	 => 'Brandenburg v. Ohio',
	),
	'brazil.html' => array(
		'include'	 => 'src/brazil.php',
		'name'	 => 'page',
		'keyword'	 => 'Brazil',
	),
	'brian_tyler_cohen.html' => array(
		'include'	 => 'src/brian_tyler_cohen.php',
		'name'	 => 'page',
		'keyword'	 => 'Brian Tyler Cohen',
	),
	'brian_tyler_cohen_trump_goes_off_the_rails_with_insulting_message_to_women.html' => array(
		'include'	 => 'src/brian_tyler_cohen_trump_goes_off_the_rails_with_insulting_message_to_women.php',
		'name'	 => 'page',
		'keyword'	 => 'Brian Tyler Cohen: Trump goes off the rails with insulting message to women',
	),
	'brics.html' => array(
		'include'	 => 'src/brics.php',
		'name'	 => 'page',
		'keyword'	 => 'BRICS',
	),
	'brown_v_board_of_education.html' => array(
		'include'	 => 'src/brown_v_board_of_education.php',
		'name'	 => 'page',
		'keyword'	 => 'Brown v. Board of Education',
	),
	'buddhist_economics.html' => array(
		'include'	 => 'src/buddhist_economics.php',
		'name'	 => 'page',
		'keyword'	 => 'Buddhist economics',
	),
	'burwell_v_hobby_lobby_stores_inc.html' => array(
		'include'	 => 'src/burwell_v_hobby_lobby_stores_inc.php',
		'name'	 => 'page',
		'keyword'	 => 'Burwell v. Hobby Lobby Stores, Inc.',
	),
	'bush_v_gore.html' => array(
		'include'	 => 'src/bush_v_gore.php',
		'name'	 => 'page',
		'keyword'	 => 'Bush v. Gore',
	),
	'business_ethics_magazine.html' => array(
		'include'	 => 'src/business_ethics_magazine.php',
		'name'	 => 'page',
		'keyword'	 => 'Business Ethics Magazine',
	),
	'campaign_finance.html' => array(
		'include'	 => 'src/campaign_finance.php',
		'name'	 => 'page',
		'keyword'	 => 'Campaign finance',
	),
	'campaign_legal_center.html' => array(
		'include'	 => 'src/campaign_legal_center.php',
		'name'	 => 'page',
		'keyword'	 => 'Campaign Legal Center',
	),
	'capital.html' => array(
		'include'	 => 'src/capital.php',
		'name'	 => 'page',
		'keyword'	 => 'Capital',
	),
	'capital_in_the_twenty_first_century.html' => array(
		'include'	 => 'src/capital_in_the_twenty_first_century.php',
		'name'	 => 'page',
		'keyword'	 => 'Capital in the Twenty-First Century',
	),
	'capital_punishment.html' => array(
		'include'	 => 'src/capital_punishment.php',
		'name'	 => 'page',
		'keyword'	 => 'Capital punishment',
	),
	'caribbean_community.html' => array(
		'include'	 => 'src/caribbean_community.php',
		'name'	 => 'page',
		'keyword'	 => 'Caribbean Community',
	),
	'carnegie_endowment_for_international_peace.html' => array(
		'include'	 => 'src/carnegie_endowment_for_international_peace.php',
		'name'	 => 'page',
		'keyword'	 => 'Carnegie Endowment for International Peace',
	),
	'censorship.html' => array(
		'include'	 => 'src/censorship.php',
		'name'	 => 'page',
		'keyword'	 => 'Censorship',
	),
	'center_for_countering_digital_hate.html' => array(
		'include'	 => 'src/center_for_countering_digital_hate.php',
		'name'	 => 'page',
		'keyword'	 => 'Center for Countering Digital Hate',
	),
	'center_for_democracy_and_technology.html' => array(
		'include'	 => 'src/center_for_democracy_and_technology.php',
		'name'	 => 'page',
		'keyword'	 => 'Center for Democracy and Technology',
	),
	'center_for_election_innovation_research.html' => array(
		'include'	 => 'src/center_for_election_innovation_research.php',
		'name'	 => 'page',
		'keyword'	 => 'Center for Election Innovation & Research',
	),
	'center_for_election_science.html' => array(
		'include'	 => 'src/center_for_election_science.php',
		'name'	 => 'page',
		'keyword'	 => 'Center for Election Science',
	),
	'centre_for_humanitarian_data.html' => array(
		'include'	 => 'src/centre_for_humanitarian_data.php',
		'name'	 => 'page',
		'keyword'	 => 'Centre for Humanitarian Data',
	),
	'centre_for_information_resilience.html' => array(
		'include'	 => 'src/centre_for_information_resilience.php',
		'name'	 => 'page',
		'keyword'	 => 'Centre for Information Resilience',
	),
	'centre_for_law_and_policy_research.html' => array(
		'include'	 => 'src/centre_for_law_and_policy_research.php',
		'name'	 => 'page',
		'keyword'	 => 'Centre for Law and Policy Research',
	),
	'certifying_elections.html' => array(
		'include'	 => 'src/certifying_elections.php',
		'name'	 => 'page',
		'keyword'	 => 'Certifying Elections',
	),
	'charles_hamilton_houston_institute_for_race_and_justice.html' => array(
		'include'	 => 'src/charles_hamilton_houston_institute_for_race_and_justice.php',
		'name'	 => 'page',
		'keyword'	 => ' Charles Hamilton Houston Institute for Race and Justice',
	),
	'charlie_chaplin.html' => array(
		'include'	 => 'src/charlie_chaplin.php',
		'name'	 => 'page',
		'keyword'	 => 'Charlie Chaplin',
	),
	'charlie_chaplin_final_speech_from_the_great_dictator.html' => array(
		'include'	 => 'src/charlie_chaplin_final_speech_from_the_great_dictator.php',
		'name'	 => 'page',
		'keyword'	 => 'Charlie Chaplin: Final Speech from The Great Dictator',
	),
	'chevron_u_s_a_inc_v_natural_resources_defense_council_inc.html' => array(
		'include'	 => 'src/chevron_u_s_a_inc_v_natural_resources_defense_council_inc.php',
		'name'	 => 'page',
		'keyword'	 => 'Chevron U.S.A., Inc. v. Natural Resources Defense Council, Inc.',
	),
	'chi_huang.html' => array(
		'include'	 => 'src/chi_huang.php',
		'name'	 => 'page',
		'keyword'	 => 'Chi Huang',
	),
	'child_labour.html' => array(
		'include'	 => 'src/child_labour.php',
		'name'	 => 'page',
		'keyword'	 => 'child labour',
	),
	'children_s_rights.html' => array(
		'include'	 => 'src/children_s_rights.php',
		'name'	 => 'page',
		'keyword'	 => 'Children\'s Rights',
	),
	'chile.html' => array(
		'include'	 => 'src/chile.php',
		'name'	 => 'page',
		'keyword'	 => 'Chile',
	),
	'china_and_taiwan.html' => array(
		'include'	 => 'src/china_and_taiwan.php',
		'name'	 => 'page',
		'keyword'	 => 'China and Taiwan',
	),
	'chinese_expansionism.html' => array(
		'include'	 => 'src/chinese_expansionism.php',
		'name'	 => 'page',
		'keyword'	 => 'Chinese expansionism',
	),
	'citizens_united_v_federal_election_commission.html' => array(
		'include'	 => 'src/citizens_united_v_federal_election_commission.php',
		'name'	 => 'page',
		'keyword'	 => 'Citizens United v. Federal Election Commission',
	),
	'clark_cunningham.html' => array(
		'include'	 => 'src/clark_cunningham.php',
		'name'	 => 'page',
		'keyword'	 => 'Clark Cunningham',
	),
	'cnn_hero_payton_mcgriff_on_school_uniform_for_school_girl_education_in_togo.html' => array(
		'include'	 => 'src/cnn_hero_payton_mcgriff_on_school_uniform_for_school_girl_education_in_togo.php',
		'name'	 => 'page',
		'keyword'	 => 'CNN Hero: Payton McGriff on school uniform for school girl education in Togo',
	),
	'colombia.html' => array(
		'include'	 => 'src/colombia.php',
		'name'	 => 'page',
		'keyword'	 => 'Colombia',
	),
	'colorado.html' => array(
		'include'	 => 'src/colorado.php',
		'name'	 => 'page',
		'keyword'	 => 'Colorado',
	),
	'committee_for_children.html' => array(
		'include'	 => 'src/committee_for_children.php',
		'name'	 => 'page',
		'keyword'	 => ' Committee for Children',
	),
	'committee_to_protect_journalists.html' => array(
		'include'	 => 'src/committee_to_protect_journalists.php',
		'name'	 => 'page',
		'keyword'	 => 'Committee to Protect Journalists',
	),
	'community_of_democracies.html' => array(
		'include'	 => 'src/community_of_democracies.php',
		'name'	 => 'page',
		'keyword'	 => 'Community of Democracies',
	),
	'community_organized_relief_effort.html' => array(
		'include'	 => 'src/community_organized_relief_effort.php',
		'name'	 => 'page',
		'keyword'	 => 'Community Organized Relief Effort',
	),
	'comparative_study_of_electoral_systems.html' => array(
		'include'	 => 'src/comparative_study_of_electoral_systems.php',
		'name'	 => 'page',
		'keyword'	 => 'Comparative Study of Electoral Systems',
	),
	'compromised_book.html' => array(
		'include'	 => 'src/compromised_book.php',
		'name'	 => 'page',
		'keyword'	 => 'Compromised book',
	),
	'concession.html' => array(
		'include'	 => 'src/concession.php',
		'name'	 => 'page',
		'keyword'	 => 'Concession',
	),
	'concession_speech.html' => array(
		'include'	 => 'src/concession_speech.php',
		'name'	 => 'page',
		'keyword'	 => 'Concession speech',
	),
	'conflict_resolution.html' => array(
		'include'	 => 'src/conflict_resolution.php',
		'name'	 => 'page',
		'keyword'	 => 'Conflict resolution',
	),
	'conservatism.html' => array(
		'include'	 => 'src/conservatism.php',
		'name'	 => 'page',
		'keyword'	 => 'Conservatism',
	),
	'conspiracy_theory.html' => array(
		'include'	 => 'src/conspiracy_theory.php',
		'name'	 => 'page',
		'keyword'	 => 'Conspiracy theory',
	),
	'constitution.html' => array(
		'name'	 => 'page',
		'include'	 => 'src/constitution.php',
	),
	'constitution_annotated.html' => array(
		'include'	 => 'src/constitution_annotated.php',
		'name'	 => 'page',
		'keyword'	 => 'Constitution Annotated',
	),
	'constitution_of_india.html' => array(
		'include'	 => 'src/constitution_of_india.php',
		'name'	 => 'page',
		'keyword'	 => 'Constitution of India',
	),
	'constitution_of_the_philippines.html' => array(
		'include'	 => 'src/constitution_of_the_philippines.php',
		'name'	 => 'page',
		'keyword'	 => 'Constitution of the Philippines',
	),
	'consumer_financial_protection_bureau.html' => array(
		'include'	 => 'src/consumer_financial_protection_bureau.php',
		'name'	 => 'page',
		'keyword'	 => 'Consumer Financial Protection Bureau',
	),
	'corporate_crime_and_punishment.html' => array(
		'include'	 => 'src/corporate_crime_and_punishment.php',
		'name'	 => 'page',
		'keyword'	 => 'Corporate Crime and Punishment',
	),
	'corruption.html' => array(
		'include'	 => 'src/corruption.php',
		'name'	 => 'page',
		'keyword'	 => 'corruption',
	),
	'corruption_in_the_united_states.html' => array(
		'include'	 => 'src/corruption_in_the_united_states.php',
		'name'	 => 'page',
		'keyword'	 => 'Corruption in the United States',
	),
	'costa_rica.html' => array(
		'name'	 => 'page',
		'include'	 => 'src/costa_rica.php',
		'keyword'	 => 'Costa Rica',
	),
	'council_on_foreign_relations.html' => array(
		'include'	 => 'src/council_on_foreign_relations.php',
		'name'	 => 'page',
		'keyword'	 => 'Council on Foreign Relations',
	),
	'counterinsurgency_mathematics.html' => array(
		'include'	 => 'src/counterinsurgency_mathematics.php',
		'name'	 => 'page',
		'keyword'	 => 'Counterinsurgency mathematics',
	),
	'counting_the_vote_a_firing_line_special_with_margaret_hoover.html' => array(
		'include'	 => 'src/counting_the_vote_a_firing_line_special_with_margaret_hoover.php',
		'name'	 => 'page',
		'keyword'	 => 'Counting the Vote: A Firing Line Special with Margaret Hoover',
	),
	'courtlistener.html' => array(
		'include'	 => 'src/courtlistener.php',
		'name'	 => 'page',
		'keyword'	 => 'CourtListener',
	),
	'covert_political_repression.html' => array(
		'include'	 => 'src/covert_political_repression.php',
		'name'	 => 'page',
		'keyword'	 => 'Covert political repression',
	),
	'credo23.html' => array(
		'include'	 => 'src/credo23.php',
		'name'	 => 'page',
		'keyword'	 => 'Credo23',
	),
	'criminal_justice.html' => array(
		'include'	 => 'src/criminal_justice.php',
		'name'	 => 'page',
		'keyword'	 => 'Criminal justice',
	),
	'cuba.html' => array(
		'include'	 => 'src/cuba.php',
		'name'	 => 'page',
		'keyword'	 => 'Cuba',
	),
	'cyberwarfare.html' => array(
		'include'	 => 'src/cyberwarfare.php',
		'name'	 => 'page',
		'keyword'	 => 'cyberwarfare',
	),
	'cynicism.html' => array(
		'include'	 => 'src/cynicism.php',
		'name'	 => 'page',
		'keyword'	 => 'Cynicism',
	),
	'dan_goldman.html' => array(
		'include'	 => 'src/dan_goldman.php',
		'name'	 => 'page',
		'keyword'	 => 'Dan Goldman',
	),
	'dangers.html' => array(
		'include'	 => 'src/dangers.php',
		'name'	 => 'page',
		'keyword'	 => 'Dangers',
	),
	'data_activism.html' => array(
		'include'	 => 'src/data_activism.php',
		'name'	 => 'page',
		'keyword'	 => 'data activism',
	),
	'data_pop_alliance.html' => array(
		'include'	 => 'src/data_pop_alliance.php',
		'name'	 => 'page',
		'keyword'	 => 'Data-Pop Alliance',
	),
	'david_becker.html' => array(
		'include'	 => 'src/david_becker.php',
		'name'	 => 'page',
		'keyword'	 => 'David Becker',
	),
	'david_dill.html' => array(
		'include'	 => 'src/david_dill.php',
		'name'	 => 'page',
		'keyword'	 => 'David Dill',
	),
	'death_and_taxes.html' => array(
		'include'	 => 'src/death_and_taxes.php',
		'name'	 => 'page',
		'keyword'	 => 'Death and taxes',
	),
	'death_tax.html' => array(
		'include'	 => 'src/death_tax.php',
		'name'	 => 'page',
		'keyword'	 => 'Death Tax',
	),
	'deaths_of_migrants.html' => array(
		'include'	 => 'src/deaths_of_migrants.php',
		'name'	 => 'page',
		'keyword'	 => 'Deaths of migrants',
	),
	'debt.html' => array(
		'include'	 => 'src/debt.php',
		'name'	 => 'page',
		'keyword'	 => 'Debt',
	),
	'decentralized_autonomous_organization.html' => array(
		'include'	 => 'src/decentralized_autonomous_organization.php',
		'name'	 => 'page',
		'keyword'	 => 'decentralized autonomous organization',
	),
	'deepfake.html' => array(
		'include'	 => 'src/deepfake.php',
		'name'	 => 'page',
		'keyword'	 => 'Deepfake',
	),
	'defending_democracy_meet_the_republicans_voting_against_trump_with_sarah_longwell.html' => array(
		'include'	 => 'src/defending_democracy_meet_the_republicans_voting_against_trump_with_sarah_longwell.php',
		'name'	 => 'page',
		'keyword'	 => 'Defending Democracy: Meet the Republicans Voting Against Trump with Sarah Longwell',
	),
	'defending_democracy_podcast.html' => array(
		'include'	 => 'src/defending_democracy_podcast.php',
		'name'	 => 'page',
		'keyword'	 => 'Defending Democracy Podcast',
	),
	'defending_democracy_rep_dan_goldman_on_project_2025_fixing_the_supreme_court_and_gop_extremism.html' => array(
		'include'	 => 'src/defending_democracy_rep_dan_goldman_on_project_2025_fixing_the_supreme_court_and_gop_extremism.php',
		'name'	 => 'page',
		'keyword'	 => 'Defending Democracy: Rep. Dan Goldman on Project 2025, Fixing the Supreme Court and GOP Extremism',
	),
	'democracy.html' => array(
		'include'	 => 'src/democracy.php',
		'name'	 => 'page',
		'keyword'	 => 'democracy',
	),
	'democracy_docket.html' => array(
		'include'	 => 'src/democracy_docket.php',
		'name'	 => 'page',
		'keyword'	 => 'Democracy Docket',
	),
	'democracy_in_america.html' => array(
		'include'	 => 'src/democracy_in_america.php',
		'name'	 => 'page',
		'keyword'	 => 'Democracy in America',
	),
	'democracy_international.html' => array(
		'include'	 => 'src/democracy_international.php',
		'name'	 => 'page',
		'keyword'	 => 'Democracy International ',
	),
	'democracy_taxes.html' => array(
		'include'	 => 'src/democracy_taxes.php',
		'name'	 => 'page',
		'keyword'	 => 'Democracy Taxes',
	),
	'democracy_wip.html' => array(
		'name'	 => 'page',
		'include'	 => 'src/democracy_wip.php',
	),
	'democratic_backsliding_in_the_united_states.html' => array(
		'include'	 => 'src/democratic_backsliding_in_the_united_states.php',
		'name'	 => 'page',
		'keyword'	 => 'Democratic backsliding in the United States',
	),
	'democratic_osmosis.html' => array(
		'include'	 => 'src/democratic_osmosis.php',
		'name'	 => 'page',
		'keyword'	 => 'Democratic Osmosis',
	),
	'democratic_republic_of_the_congo.html' => array(
		'include'	 => 'src/democratic_republic_of_the_congo.php',
		'name'	 => 'page',
		'keyword'	 => 'Democratic Republic of the Congo',
	),
	'denis_mukwege.html' => array(
		'include'	 => 'src/denis_mukwege.php',
		'name'	 => 'page',
		'keyword'	 => 'Denis Mukwege',
	),
	'denise_dresser.html' => array(
		'include'	 => 'src/denise_dresser.php',
		'name'	 => 'page',
		'keyword'	 => 'Denise Dresser',
	),
	'denmark.html' => array(
		'include'	 => 'src/denmark.php',
		'name'	 => 'page',
		'keyword'	 => 'Denmark',
	),
	'desperation_economy.html' => array(
		'include'	 => 'src/desperation_economy.php',
		'name'	 => 'page',
		'keyword'	 => 'Desperation Economy',
	),
	'development_policy_centre.html' => array(
		'include'	 => 'src/development_policy_centre.php',
		'name'	 => 'page',
		'keyword'	 => 'Development Policy Centre',
	),
	'diamond_open_access.html' => array(
		'include'	 => 'src/diamond_open_access.php',
		'name'	 => 'page',
		'keyword'	 => 'Diamond open access',
	),
	'differ_we_must.html' => array(
		'include'	 => 'src/differ_we_must.php',
		'name'	 => 'page',
		'keyword'	 => 'Differ We Must',
	),
	'digital_habitat.html' => array(
		'include'	 => 'src/digital_habitat.php',
		'name'	 => 'page',
		'keyword'	 => 'Digital habitat',
	),
	'disfranchisement.html' => array(
		'include'	 => 'src/disfranchisement.php',
		'name'	 => 'page',
		'keyword'	 => 'Disfranchisement',
	),
	'disinformation.html' => array(
		'include'	 => 'src/disinformation.php',
		'name'	 => 'page',
		'keyword'	 => 'Disinformation',
	),
	'disinformation_and_hate_speech_on_social_media.html' => array(
		'include'	 => 'src/disinformation_and_hate_speech_on_social_media.php',
		'name'	 => 'page',
		'keyword'	 => 'Disinformation and hate speech on social media',
	),
	'district_of_columbia_v_heller.html' => array(
		'include'	 => 'src/district_of_columbia_v_heller.php',
		'name'	 => 'page',
		'keyword'	 => 'District of Columbia v. Heller',
	),
	'dominican_republic.html' => array(
		'include'	 => 'src/dominican_republic.php',
		'name'	 => 'page',
		'keyword'	 => 'Dominican Republic',
	),
	'don_miguel_ruiz.html' => array(
		'include'	 => 'src/don_miguel_ruiz.php',
		'name'	 => 'page',
		'keyword'	 => 'Don Miguel Ruiz',
	),
	'donald_trump.html' => array(
		'include'	 => 'src/donald_trump.php',
		'name'	 => 'page',
		'keyword'	 => 'Donald Trump',
	),
	'donald_trump_policy_and_discourse_on_immigration_and_asylum.html' => array(
		'include'	 => 'src/donald_trump_policy_and_discourse_on_immigration_and_asylum.php',
		'name'	 => 'page',
		'keyword'	 => 'Donald Trump policy and discourse on immigration and asylum policy',
	),
	'drawing_lessons_from_donald_trump_political_carrer_and_its_aftermath.html' => array(
		'include'	 => 'src/drawing_lessons_from_donald_trump_political_carrer_and_its_aftermath.php',
		'name'	 => 'page',
		'keyword'	 => 'Drawing lessons from Donald Trump political carrer and its aftermath',
	),
	'duverger_france_alternance_danger.html' => array(
		'name'	 => 'page',
		'include'	 => 'src/duverger_france_alternance_danger.php',
	),
	'duverger_law.html' => array(
		'name'	 => 'page',
		'include'	 => 'src/duverger_law.php',
	),
	'duverger_syndrome.html' => array(
		'name'	 => 'page',
		'include'	 => 'src/duverger_syndrome.php',
	),
	'duverger_syndrome_alternance.html' => array(
		'name'	 => 'page',
		'include'	 => 'src/duverger_syndrome_alternance.php',
	),
	'duverger_syndrome_duality.html' => array(
		'name'	 => 'page',
		'include'	 => 'src/duverger_syndrome_duality.php',
	),
	'duverger_syndrome_extremism.html' => array(
		'include'	 => 'src/duverger_syndrome_extremism.php',
		'name'	 => 'page',
		'keyword'	 => 'Duverger syndrome extremism',
	),
	'duverger_syndrome_lack_choice.html' => array(
		'name'	 => 'page',
		'include'	 => 'src/duverger_syndrome_lack_choice.php',
	),
	'duverger_syndrome_negative_campaigning.html' => array(
		'name'	 => 'page',
		'include'	 => 'src/duverger_syndrome_negative_campaigning.php',
	),
	'duverger_syndrome_party_politics.html' => array(
		'name'	 => 'page',
		'include'	 => 'src/duverger_syndrome_party_politics.php',
	),
	'duverger_syndrome_political_realignments.html' => array(
		'include'	 => 'src/duverger_syndrome_political_realignments.php',
		'name'	 => 'page',
	),
	'duverger_taiwan_2000_2004.html' => array(
		'name'	 => 'page',
		'include'	 => 'src/duverger_taiwan_2000_2004.php',
	),
	'duverger_taiwan_2024.html' => array(
		'include'	 => 'src/duverger_taiwan_2024.php',
		'name'	 => 'page',
		'keyword'	 => 'Duverger Taiwan 2024',
	),
	'duverger_trap.html' => array(
		'include'	 => 'src/duverger_trap.php',
		'name'	 => 'page',
		'keyword'	 => 'Duverger Trap',
	),
	'duverger_usa_19_century.html' => array(
		'name'	 => 'page',
		'include'	 => 'src/duverger_usa_19_century.php',
	),
	'duverger_usa_21_century.html' => array(
		'include'	 => 'src/duverger_usa_21_century.php',
		'name'	 => 'page',
		'keyword'	 => 'duverger usa 21 century',
	),
	'echo_chamber.html' => array(
		'include'	 => 'src/echo_chamber.php',
		'name'	 => 'page',
		'keyword'	 => 'Echo chamber',
	),
	'economic_injustice.html' => array(
		'include'	 => 'src/economic_injustice.php',
		'name'	 => 'page',
		'keyword'	 => 'Economic inequality',
	),
	'economic_policy_institute.html' => array(
		'include'	 => 'src/economic_policy_institute.php',
		'name'	 => 'page',
		'keyword'	 => 'Economic Policy Institute',
	),
	'economic_social_and_cultural_rights.html' => array(
		'include'	 => 'src/economic_social_and_cultural_rights.php',
		'name'	 => 'page',
		'keyword'	 => 'Economic, social and cultural rights',
	),
	'economist_democracy_index.html' => array(
		'include'	 => 'src/economist_democracy_index.php',
		'name'	 => 'page',
		'keyword'	 => 'Economist Democracy Index',
	),
	'economy.html' => array(
		'include'	 => 'src/economy.php',
		'name'	 => 'page',
		'keyword'	 => 'Economy',
	),
	'ecowas.html' => array(
		'include'	 => 'src/ecowas.php',
		'name'	 => 'page',
		'keyword'	 => 'ECOWAS',
	),
	'ecuador.html' => array(
		'include'	 => 'src/ecuador.php',
		'name'	 => 'page',
		'keyword'	 => 'Ecuador',
	),
	'eddie_glaude.html' => array(
		'include'	 => 'src/eddie_glaude.php',
		'name'	 => 'page',
		'keyword'	 => 'Eddie Glaude',
	),
	'education.html' => array(
		'include'	 => 'src/education.php',
		'name'	 => 'page',
		'keyword'	 => 'education',
	),
	'egypt.html' => array(
		'include'	 => 'src/egypt.php',
		'name'	 => 'page',
		'keyword'	 => 'Egypt',
	),
	'eighth_level_of_democracy.html' => array(
		'include'	 => 'src/eighth_level_of_democracy.php',
		'name'	 => 'page',
		'keyword'	 => 'Eighth level of democracy',
	),
	'el_salvador.html' => array(
		'include'	 => 'src/el_salvador.php',
		'name'	 => 'page',
		'keyword'	 => 'El Salvador',
	),
	'election_denialism.html' => array(
		'include'	 => 'src/election_denialism.php',
		'name'	 => 'page',
		'keyword'	 => 'Election denialism',
	),
	'election_integrity.html' => array(
		'name'	 => 'page',
		'include'	 => 'src/election_integrity.php',
	),
	'election_official.html' => array(
		'include'	 => 'src/election_official.php',
		'name'	 => 'page',
		'keyword'	 => 'Election official',
	),
	'elections.html' => array(
		'include'	 => 'src/elections.php',
		'name'	 => 'page',
		'keyword'	 => 'Elections',
	),
	'electoral_system.html' => array(
		'include'	 => 'src/electoral_system.php',
		'name'	 => 'page',
		'keyword'	 => 'Electoral system',
	),
	'eminent_domain.html' => array(
		'include'	 => 'src/eminent_domain.php',
		'name'	 => 'page',
		'keyword'	 => 'Eminent domain',
	),
	'environment.html' => array(
		'include'	 => 'src/environment.php',
		'name'	 => 'page',
		'keyword'	 => 'Environment',
	),
	'environmental_defenders.html' => array(
		'include'	 => 'src/environmental_defenders.php',
		'name'	 => 'page',
		'keyword'	 => 'Environmental defenders',
	),
	'environmental_justice_foundation.html' => array(
		'include'	 => 'src/environmental_justice_foundation.php',
		'name'	 => 'page',
		'keyword'	 => 'Environmental Justice Foundation',
	),
	'environmental_stewardship.html' => array(
		'include'	 => 'src/environmental_stewardship.php',
		'name'	 => 'page',
		'keyword'	 => 'Environmental Stewardship',
	),
	'erasing_history_how_fascists_rewrite_the_past_to_control_the_future.html' => array(
		'include'	 => 'src/erasing_history_how_fascists_rewrite_the_past_to_control_the_future.php',
		'name'	 => 'page',
		'keyword'	 => 'Erasing History: How Fascists rewrite the Past to Control the Future',
	),
	'ernst_friedrich_schumacher.html' => array(
		'include'	 => 'src/ernst_friedrich_schumacher.php',
		'name'	 => 'page',
		'keyword'	 => 'Ernst Friedrich Schumacher',
	),
	'erwin_chemerinsky.html' => array(
		'include'	 => 'src/erwin_chemerinsky.php',
		'name'	 => 'page',
		'keyword'	 => 'Erwin Chemerinsky',
	),
	'estate_tax.html' => array(
		'include'	 => 'src/estate_tax.php',
		'name'	 => 'page',
		'keyword'	 => 'Estate Tax',
	),
	'estonia.html' => array(
		'include'	 => 'src/estonia.php',
		'name'	 => 'page',
		'keyword'	 => 'Estonia',
	),
	'ethiopia.html' => array(
		'include'	 => 'src/ethiopia.php',
		'name'	 => 'page',
		'keyword'	 => 'Ethiopia',
	),
	'eu_tax_observatory.html' => array(
		'include'	 => 'src/eu_tax_observatory.php',
		'name'	 => 'page',
		'keyword'	 => 'EU Tax Observatory',
	),
	'euromaidan.html' => array(
		'include'	 => 'src/euromaidan.php',
		'name'	 => 'page',
		'keyword'	 => 'Euromaidan
',
	),
	'european_union.html' => array(
		'include'	 => 'src/european_union.php',
		'name'	 => 'page',
		'keyword'	 => 'European Union',
	),
	'evolution_of_the_republican_party.html' => array(
		'include'	 => 'src/evolution_of_the_republican_party.php',
		'name'	 => 'page',
		'keyword'	 => 'Evolution of the Republican Party',
	),
	'external_threats.html' => array(
		'include'	 => 'src/external_threats.php',
		'name'	 => 'page',
		'keyword'	 => 'democracy under attack',
	),
	'externalities.html' => array(
		'include'	 => 'src/externalities.php',
		'name'	 => 'page',
		'keyword'	 => 'Externalities',
	),
	'factors_of_production.html' => array(
		'include'	 => 'src/factors_of_production.php',
		'name'	 => 'page',
		'keyword'	 => 'Factors of Production',
	),
	'factors_of_production_and_the_stewardship_of_the_commons.html' => array(
		'include'	 => 'src/factors_of_production_and_the_stewardship_of_the_commons.php',
		'name'	 => 'page',
		'keyword'	 => 'Factors of Production and the Stewardship of the Commons:',
	),
	'fair_share.html' => array(
		'name'	 => 'page',
		'include'	 => 'src/fair_share.php',
	),
	'fair_share_of_profits.html' => array(
		'include'	 => 'src/fair_share_of_profits.php',
		'name'	 => 'page',
		'keyword'	 => 'Fair Share of Profits',
	),
	'fair_share_of_responsibilities.html' => array(
		'include'	 => 'src/fair_share_of_responsibilities.php',
		'name'	 => 'page',
		'keyword'	 => 'Fair Share of Responsibilities',
	),
	'fake_accounts_on_social_networks.html' => array(
		'include'	 => 'src/fake_accounts_on_social_networks.php',
		'name'	 => 'page',
		'keyword'	 => 'Fake accounts on social networks',
	),
	'famine_in_madagascar.html' => array(
		'include'	 => 'src/famine_in_madagascar.php',
		'name'	 => 'page',
		'keyword'	 => 'Famine in Madagascar',
	),
	'fascism.html' => array(
		'include'	 => 'src/fascism.php',
		'name'	 => 'page',
		'keyword'	 => 'Fascism',
	),
	'fatma_karume.html' => array(
		'include'	 => 'src/fatma_karume.php',
		'name'	 => 'page',
		'keyword'	 => 'Fatma Karume',
	),
	'ferdinand_marcos.html' => array(
		'include'	 => 'src/ferdinand_marcos.php',
		'name'	 => 'page',
		'keyword'	 => 'Ferdinand Marcos',
	),
	'fifth_level_of_democracy.html' => array(
		'include'	 => 'src/fifth_level_of_democracy.php',
		'name'	 => 'page',
		'keyword'	 => '5: Fifth level of democracy',
	),
	'fighting_disinfo_in_maga_era_ari_melber_talks_facts_tech_with_microsoft_legend_steve_ballmer.html' => array(
		'include'	 => 'src/fighting_disinfo_in_maga_era_ari_melber_talks_facts_tech_with_microsoft_legend_steve_ballmer.php',
		'name'	 => 'page',
		'keyword'	 => 'Fighting disinfo in MAGA era: Ari Melber talks facts & tech with Microsoft legend Steve Ballmer',
	),
	'firing_line.html' => array(
		'include'	 => 'src/firing_line.php',
		'name'	 => 'page',
		'keyword'	 => 'Firing Line',
	),
	'first_level_of_democracy.html' => array(
		'include'	 => 'src/first_level_of_democracy.php',
		'name'	 => 'page',
		'keyword'	 => 'first level of democracy',
	),
	'flat_tax.html' => array(
		'include'	 => 'src/flat_tax.php',
		'name'	 => 'page',
		'keyword'	 => 'flat tax',
	),
	'food_and_agriculture_organization.html' => array(
		'include'	 => 'src/food_and_agriculture_organization.php',
		'name'	 => 'page',
		'keyword'	 => 'Food and Agriculture Organization',
	),
	'foreign_influence_local_media.html' => array(
		'name'	 => 'page',
		'include'	 => 'src/foreign_influence_local_media.php',
	),
	'foreign_workers_in_taiwan.html' => array(
		'include'	 => 'src/foreign_workers_in_taiwan.php',
		'name'	 => 'page',
		'keyword'	 => 'Foreign Workers in Taiwan',
	),
	'fourth_level_of_democracy.html' => array(
		'include'	 => 'src/fourth_level_of_democracy.php',
		'name'	 => 'page',
		'keyword'	 => 'Quaternary features of democracy',
	),
	'france.html' => array(
		'include'	 => 'src/france.php',
		'name'	 => 'page',
		'keyword'	 => 'France',
	),
	'free_law_project.html' => array(
		'include'	 => 'src/free_law_project.php',
		'name'	 => 'page',
		'keyword'	 => 'Free Law Project',
	),
	'freedom.html' => array(
		'include'	 => 'src/freedom.php',
		'name'	 => 'page',
		'keyword'	 => 'Freedom',
	),
	'freedom_house.html' => array(
		'include'	 => 'src/freedom_house.php',
		'name'	 => 'page',
		'keyword'	 => 'Freedom House',
	),
	'freedom_of_association.html' => array(
		'include'	 => 'src/freedom_of_association.php',
		'name'	 => 'page',
		'keyword'	 => 'Freedom of association',
	),
	'freedom_of_conscience.html' => array(
		'include'	 => 'src/freedom_of_conscience.php',
		'name'	 => 'page',
		'keyword'	 => 'Freedom of conscience',
	),
	'freedom_of_romance.html' => array(
		'include'	 => 'src/freedom_of_romance.php',
		'name'	 => 'page',
		'keyword'	 => 'Freedom of Romance',
	),
	'freedom_of_speech.html' => array(
		'name'	 => 'page',
		'include'	 => 'src/freedom_of_speech.php',
	),
	'from_russia_with_lev.html' => array(
		'include'	 => 'src/from_russia_with_lev.php',
		'name'	 => 'page',
		'keyword'	 => 'From Russia With Lev',
	),
	'frontline_short_docs_democracy_on_trial.html' => array(
		'include'	 => 'src/frontline_short_docs_democracy_on_trial.php',
		'name'	 => 'page',
		'keyword'	 => 'FRONTLINE Short Docs: Democracy on Trial',
	),
	'funding_for_elections.html' => array(
		'include'	 => 'src/funding_for_elections.php',
		'name'	 => 'page',
		'keyword'	 => 'Funding for elections',
	),
	'gabon.html' => array(
		'include'	 => 'src/gabon.php',
		'name'	 => 'page',
		'keyword'	 => 'Gabon',
	),
	'gaming_the_vote_why_elections_aren_t_fair.html' => array(
		'include'	 => 'src/gaming_the_vote_why_elections_aren_t_fair.php',
		'name'	 => 'page',
		'keyword'	 => ' Gaming the Vote: Why Elections Aren\'t Fair',
	),
	'gar_alperovitz.html' => array(
		'include'	 => 'src/gar_alperovitz.php',
		'name'	 => 'page',
		'keyword'	 => 'Gar Alperovitz',
	),
	'george_orwell.html' => array(
		'include'	 => 'src/george_orwell.php',
		'name'	 => 'page',
		'keyword'	 => 'George Orwell',
	),
	'georgia.html' => array(
		'name'	 => 'page',
		'include'	 => 'src/georgia.php',
	),
	'germany.html' => array(
		'name'	 => 'page',
		'include'	 => 'src/germany.php',
		'keyword'	 => 'Germany',
	),
	'gerrymandering.html' => array(
		'include'	 => 'src/gerrymandering.php',
		'name'	 => 'page',
		'keyword'	 => 'Gerrymandering',
	),
	'gideon_v_wainwright.html' => array(
		'include'	 => 'src/gideon_v_wainwright.php',
		'name'	 => 'page',
		'keyword'	 => 'Gideon v. Wainwright',
	),
	'glenn_kirschner.html' => array(
		'include'	 => 'src/glenn_kirschner.php',
		'name'	 => 'page',
		'keyword'	 => 'Glenn Kirschner',
	),
	'glenn_kirschner_on_trump_s_legal_issues_for_haitian_immigrant_hoax.html' => array(
		'include'	 => 'src/glenn_kirschner_on_trump_s_legal_issues_for_haitian_immigrant_hoax.php',
		'name'	 => 'page',
		'keyword'	 => 'Glenn Kirschner on Trump\'s legal issues for Haitian immigrant hoax',
	),
	'global_impunity_index.html' => array(
		'include'	 => 'src/global_impunity_index.php',
		'name'	 => 'page',
		'keyword'	 => 'Global Impunity Index',
	),
	'global_issues.html' => array(
		'include'	 => 'src/global_issues.php',
		'name'	 => 'page',
		'keyword'	 => 'global issues',
	),
	'global_migration_data_analysis_centre.html' => array(
		'include'	 => 'src/global_migration_data_analysis_centre.php',
		'name'	 => 'page',
		'keyword'	 => 'Global Migration Data Analysis Centre',
	),
	'global_natural_resources.html' => array(
		'name'	 => 'page',
		'include'	 => 'src/global_natural_resources.php',
	),
	'global_peace_index.html' => array(
		'include'	 => 'src/global_peace_index.php',
		'name'	 => 'page',
		'keyword'	 => 'Global Peace Index',
	),
	'global_witness.html' => array(
		'include'	 => 'src/global_witness.php',
		'name'	 => 'page',
		'keyword'	 => 'Global Witness',
	),
	'green_new_deal.html' => array(
		'include'	 => 'src/green_new_deal.php',
		'name'	 => 'page',
		'keyword'	 => 'Green New Deal',
	),
	'green_new_deal_network.html' => array(
		'include'	 => 'src/green_new_deal_network.php',
		'name'	 => 'page',
		'keyword'	 => 'Green New Deal Network',
	),
	'greenwashing.html' => array(
		'include'	 => 'src/greenwashing.php',
		'name'	 => 'page',
		'keyword'	 => 'Greenwashing',
	),
	'greg_boyle.html' => array(
		'include'	 => 'src/greg_boyle.php',
		'name'	 => 'page',
		'keyword'	 => 'Greg Boyle',
	),
	'grift.html' => array(
		'include'	 => 'src/grift.php',
		'name'	 => 'page',
		'keyword'	 => 'Grift',
	),
	'ground_news.html' => array(
		'include'	 => 'src/ground_news.php',
		'name'	 => 'page',
		'keyword'	 => 'Ground News',
	),
	'guatemala.html' => array(
		'include'	 => 'src/guatemala.php',
		'name'	 => 'page',
		'keyword'	 => 'Guatemala',
	),
	'gun_control.html' => array(
		'include'	 => 'src/gun_control.php',
		'name'	 => 'page',
		'keyword'	 => 'Gun control',
	),
	'habitat.html' => array(
		'include'	 => 'src/habitat.php',
		'name'	 => 'page',
		'keyword'	 => 'Habitat',
	),
	'haiti.html' => array(
		'include'	 => 'src/haiti.php',
		'name'	 => 'page',
		'keyword'	 => 'Haiti',
	),
	'harvard_law_school.html' => array(
		'include'	 => 'src/harvard_law_school.php',
		'name'	 => 'page',
		'keyword'	 => 'Harvard Law School',
	),
	'harvard_law_school_human_rights_program.html' => array(
		'include'	 => 'src/harvard_law_school_human_rights_program.php',
		'name'	 => 'page',
		'keyword'	 => 'Harvard Law School Human Rights Program',
	),
	'health_and_human_rights_journal.html' => array(
		'include'	 => 'src/health_and_human_rights_journal.php',
		'name'	 => 'page',
		'keyword'	 => 'Health and Human Rights Journal',
	),
	'health_care.html' => array(
		'include'	 => 'src/health_care.php',
		'name'	 => 'page',
		'keyword'	 => 'Health care',
	),
	'henry_george.html' => array(
		'include'	 => 'src/henry_george.php',
		'name'	 => 'page',
		'keyword'	 => 'Henry George',
	),
	'history_of_democracy_and_social_justice_in_the_united_states.html' => array(
		'include'	 => 'src/history_of_democracy_and_social_justice_in_the_united_states.php',
		'name'	 => 'page',
		'keyword'	 => 'History of Democracy and Social Justice in the United States',
	),
	'holodomor.html' => array(
		'include'	 => 'src/holodomor.php',
		'name'	 => 'page',
		'keyword'	 => 'Holodomor',
	),
	'homeboy_industries.html' => array(
		'include'	 => 'src/homeboy_industries.php',
		'name'	 => 'page',
		'keyword'	 => 'Homeboy Industries',
	),
	'honduras.html' => array(
		'include'	 => 'src/honduras.php',
		'name'	 => 'page',
		'keyword'	 => 'Honduras',
	),
	'hong_kong.html' => array(
		'include'	 => 'src/hong_kong.php',
		'name'	 => 'page',
		'keyword'	 => 'Hong Kong',
	),
	'hope.html' => array(
		'include'	 => 'src/hope.php',
		'name'	 => 'page',
		'keyword'	 => 'Hope',
	),
	'housing.html' => array(
		'include'	 => 'src/housing.php',
		'name'	 => 'page',
		'keyword'	 => 'Housing',
	),
	'how_the_wealthy_pass_on_economic_burden_to_the_poor.html' => array(
		'include'	 => 'src/how_the_wealthy_pass_on_economic_burden_to_the_poor.php',
		'name'	 => 'page',
		'keyword'	 => 'How the wealthy pass on economic burden to the poor',
	),
	'how_to_steal_a_presidential_election.html' => array(
		'include'	 => 'src/how_to_steal_a_presidential_election.php',
		'name'	 => 'page',
		'keyword'	 => 'How to Steal a Presidential Election',
	),
	'how_to_talk_with_victims_of_disinformation.html' => array(
		'include'	 => 'src/how_to_talk_with_victims_of_disinformation.php',
		'name'	 => 'page',
		'keyword'	 => 'How to talk with victims of disinformation',
	),
	'human_rights.html' => array(
		'include'	 => 'src/human_rights.php',
		'name'	 => 'page',
		'keyword'	 => 'Human rights',
	),
	'human_rights_watch.html' => array(
		'include'	 => 'src/human_rights_watch.php',
		'name'	 => 'page',
		'keyword'	 => 'Human Rights Watch',
	),
	'humanity.html' => array(
		'include'	 => 'src/humanity.php',
		'name'	 => 'page',
		'keyword'	 => 'HUmanity',
	),
	'hunger.html' => array(
		'name'	 => 'page',
		'include'	 => 'src/hunger.php',
	),
	'ignorant_leader_rising.html' => array(
		'include'	 => 'src/ignorant_leader_rising.php',
		'name'	 => 'page',
		'keyword'	 => 'Ignorant leader rising',
	),
	'illiberal_america_a_history.html' => array(
		'include'	 => 'src/illiberal_america_a_history.php',
		'name'	 => 'page',
		'keyword'	 => 'Illiberal America: a History',
	),
	'immigration.html' => array(
		'include'	 => 'src/immigration.php',
		'name'	 => 'page',
		'keyword'	 => 'Immigration',
	),
	'immunity.html' => array(
		'include'	 => 'src/immunity.php',
		'name'	 => 'page',
		'keyword'	 => 'Immunity',
	),
	'in_deep_trouble_surfacing_tech_powered_sexual_harassment_in_k_12_schools.html' => array(
		'include'	 => 'src/in_deep_trouble_surfacing_tech_powered_sexual_harassment_in_k_12_schools.php',
		'name'	 => 'page',
		'keyword'	 => 'In Deep Trouble: Surfacing Tech-Powered Sexual Harassment in K-12 Schools',
	),
	'index.html' => array(
		'name'	 => 'page',
		'include'	 => 'src/index.php',
	),
	'india.html' => array(
		'include'	 => 'src/india.php',
		'name'	 => 'page',
		'keyword'	 => 'India',
	),
	'individuals.html' => array(
		'include'	 => 'src/individuals.php',
		'name'	 => 'page',
		'keyword'	 => '',
	),
	'industry_self_regulation.html' => array(
		'include'	 => 'src/industry_self_regulation.php',
		'name'	 => 'page',
		'keyword'	 => 'Industry self-regulation',
	),
	'inflation.html' => array(
		'include'	 => 'src/inflation.php',
		'name'	 => 'page',
		'keyword'	 => 'Inflation',
	),
	'information.html' => array(
		'include'	 => 'src/information.php',
		'name'	 => 'page',
		'keyword'	 => 'Information',
	),
	'information_overload.html' => array(
		'include'	 => 'src/information_overload.php',
		'name'	 => 'page',
		'keyword'	 => 'information overload',
	),
	'information_wars_how_we_lost_the_global_battle_against_disinformation.html' => array(
		'include'	 => 'src/information_wars_how_we_lost_the_global_battle_against_disinformation.php',
		'name'	 => 'page',
		'keyword'	 => 'Information Wars: How We Lost the Global Battle Against Disinformation',
	),
	'informed_score_voting.html' => array(
		'include'	 => 'src/informed_score_voting.php',
		'name'	 => 'page',
		'keyword'	 => 'Informed Score Voting',
	),
	'informed_score_voting_for_stronger_democracy.html' => array(
		'include'	 => 'src/informed_score_voting_for_stronger_democracy.php',
		'name'	 => 'page',
		'keyword'	 => 'Informed Score Voting for Stronger Democracy',
	),
	'inheritance_tax.html' => array(
		'include'	 => 'src/inheritance_tax.php',
		'name'	 => 'page',
		'keyword'	 => 'Inheritance tax',
	),
	'injustice.html' => array(
		'include'	 => 'src/injustice.php',
		'name'	 => 'page',
		'keyword'	 => 'Injustice',
	),
	'insidious_political_repression.html' => array(
		'include'	 => 'src/insidious_political_repression.php',
		'name'	 => 'page',
		'keyword'	 => 'Insidious political repression',
	),
	'institute_for_economics_and_peace.html' => array(
		'include'	 => 'src/institute_for_economics_and_peace.php',
		'name'	 => 'page',
		'keyword'	 => 'Institute for Economics and Peace',
	),
	'institute_for_policy_integrity.html' => array(
		'include'	 => 'src/institute_for_policy_integrity.php',
		'name'	 => 'page',
		'keyword'	 => 'Institute for Policy Integrity',
	),
	'institutional_vs_human_factors_in_democracy.html' => array(
		'include'	 => 'src/institutional_vs_human_factors_in_democracy.php',
		'name'	 => 'page',
		'keyword'	 => 'Institutional vs Human Factors in Democracy',
	),
	'institutions.html' => array(
		'include'	 => 'src/institutions.php',
		'name'	 => 'page',
		'keyword'	 => 'institutions',
	),
	'integrity_institute.html' => array(
		'include'	 => 'src/integrity_institute.php',
		'name'	 => 'page',
		'keyword'	 => 'Integrity Institute',
	),
	'integrity_institute_new_zealand.html' => array(
		'include'	 => 'src/integrity_institute_new_zealand.php',
		'name'	 => 'page',
		'keyword'	 => 'Integrity Institute (New Zealand)',
	),
	'international.html' => array(
		'include'	 => 'src/international.php',
		'name'	 => 'page',
		'keyword'	 => 'International',
	),
	'international_children_s_peace_prize.html' => array(
		'include'	 => 'src/international_children_s_peace_prize.php',
		'name'	 => 'page',
		'keyword'	 => 'International Children\'s Peace Prize',
	),
	'international_crisis_group.html' => array(
		'include'	 => 'src/international_crisis_group.php',
		'name'	 => 'page',
		'keyword'	 => 'International Crisis Group',
	),
	'international_democracy_community.html' => array(
		'include'	 => 'src/international_democracy_community.php',
		'name'	 => 'page',
		'keyword'	 => 'International Democracy Community',
	),
	'international_institute_for_democracy_and_electoral_assistance.html' => array(
		'include'	 => 'src/international_institute_for_democracy_and_electoral_assistance.php',
		'name'	 => 'page',
		'keyword'	 => 'International Institute for Democracy and Electoral Assistance ',
	),
	'international_monetary_fund.html' => array(
		'include'	 => 'src/international_monetary_fund.php',
		'name'	 => 'page',
		'keyword'	 => 'International Monetary Fund',
	),
	'international_organization_for_migration.html' => array(
		'include'	 => 'src/international_organization_for_migration.php',
		'name'	 => 'page',
		'keyword'	 => 'International Organization for Migration',
	),
	'interpersonal_relationships.html' => array(
		'include'	 => 'src/interpersonal_relationships.php',
		'name'	 => 'page',
		'keyword'	 => 'Interpersonal relationships',
	),
	'invisible_rulers_the_people_who_turn_lies_into_reality.html' => array(
		'include'	 => 'src/invisible_rulers_the_people_who_turn_lies_into_reality.php',
		'name'	 => 'page',
		'keyword'	 => 'Invisible Rulers: The People Who Turn Lies into Reality',
	),
	'invoking_democracy.html' => array(
		'include'	 => 'src/invoking_democracy.php',
		'name'	 => 'page',
		'keyword'	 => 'Invoking Democracy',
	),
	'iran.html' => array(
		'name'	 => 'page',
		'include'	 => 'src/iran.php',
	),
	'israel.html' => array(
		'include'	 => 'src/israel.php',
		'name'	 => 'page',
		'keyword'	 => 'Israel',
	),
	'israel_palestine.html' => array(
		'include'	 => 'src/israel_palestine.php',
		'name'	 => 'page',
		'keyword'	 => 'Israel Palestine',
	),
	'italy.html' => array(
		'include'	 => 'src/italy.php',
		'name'	 => 'page',
		'keyword'	 => 'Italy',
	),
	'jamie_raskin.html' => array(
		'include'	 => 'src/jamie_raskin.php',
		'name'	 => 'page',
		'keyword'	 => 'Jamie Raskin',
	),
	'jan_werner_mueller.html' => array(
		'include'	 => 'src/jan_werner_mueller.php',
		'name'	 => 'page',
		'keyword'	 => 'Jan-Werner Müller',
	),
	'jason_stanley.html' => array(
		'include'	 => 'src/jason_stanley.php',
		'name'	 => 'page',
		'keyword'	 => 'Jason Stanley',
	),
	'jessie_daniel_ames.html' => array(
		'include'	 => 'src/jessie_daniel_ames.php',
		'name'	 => 'page',
		'keyword'	 => 'Jessie Daniel Ames',
	),
	'jimmy_carter.html' => array(
		'include'	 => 'src/jimmy_carter.php',
		'name'	 => 'page',
		'keyword'	 => 'Jimmy Carter',
	),
	'joe_biden.html' => array(
		'include'	 => 'src/joe_biden.php',
		'name'	 => 'page',
		'keyword'	 => 'Joe Biden',
	),
	'john_abrams.html' => array(
		'include'	 => 'src/john_abrams.php',
		'name'	 => 'page',
		'keyword'	 => 'John Abrams',
	),
	'jonathan_haidt.html' => array(
		'include'	 => 'src/jonathan_haidt.php',
		'name'	 => 'page',
		'keyword'	 => 'Jonathan Haidt',
	),
	'jonathan_wilson_hartgrove.html' => array(
		'include'	 => 'src/jonathan_wilson_hartgrove.php',
		'name'	 => 'page',
		'keyword'	 => 'Jonathan Wilson-Hartgrove',
	),
	'justice.html' => array(
		'name'	 => 'page',
		'include'	 => 'src/justice.php',
	),
	'kamala_harris.html' => array(
		'include'	 => 'src/kamala_harris.php',
		'name'	 => 'page',
		'keyword'	 => 'Kamala Harris',
	),
	'kamala_harris_concession_speech.html' => array(
		'include'	 => 'src/kamala_harris_concession_speech.php',
		'name'	 => 'page',
		'keyword'	 => 'Kamala Harris Concession Speech',
	),
	'karoline_wiesner.html' => array(
		'include'	 => 'src/karoline_wiesner.php',
		'name'	 => 'page',
		'keyword'	 => 'Karoline Wiesner',
	),
	'keep_america_beautiful.html' => array(
		'include'	 => 'src/keep_america_beautiful.php',
		'name'	 => 'page',
		'keyword'	 => 'Keep America Beautiful',
	),
	'kelo_v_city_of_new_london.html' => array(
		'include'	 => 'src/kelo_v_city_of_new_london.php',
		'name'	 => 'page',
		'keyword'	 => 'Kelo v. City of New London',
	),
	'ken_burns.html' => array(
		'include'	 => 'src/ken_burns.php',
		'name'	 => 'page',
		'keyword'	 => 'Ken Burns',
	),
	'ken_burns_h24_keynote_address_to_brandeis_university_2024_graduates.html' => array(
		'include'	 => 'src/ken_burns_h24_keynote_address_to_brandeis_university_2024_graduates.php',
		'name'	 => 'page',
		'keyword'	 => 'Ken Burns: H24 Keynote Address to Brandeis University 2024 Graduates',
	),
	'ken_burns_on_the_incredibly_dangerous_party_that_the_gop_has_morphed_into.html' => array(
		'include'	 => 'src/ken_burns_on_the_incredibly_dangerous_party_that_the_gop_has_morphed_into.php',
		'name'	 => 'page',
		'keyword'	 => 'Ken Burns on the ‘incredibly dangerous’ party that the GOP has ‘morphed’ into',
	),
	'ken_burns_we_need_to_reach_out_to_trump_voters_and_not_other_them.html' => array(
		'include'	 => 'src/ken_burns_we_need_to_reach_out_to_trump_voters_and_not_other_them.php',
		'name'	 => 'page',
		'keyword'	 => 'Ken Burns: We need to reach out to Trump voters and not \'other\' them',
	),
	'kenya.html' => array(
		'include'	 => 'src/kenya.php',
		'name'	 => 'page',
		'keyword'	 => 'Kenya',
	),
	'kerry_washington.html' => array(
		'include'	 => 'src/kerry_washington.php',
		'name'	 => 'page',
		'keyword'	 => 'Kerry Washington',
	),
	'labor_taxes.html' => array(
		'include'	 => 'src/labor_taxes.php',
		'name'	 => 'page',
		'keyword'	 => 'Labor Taxes',
	),
	'labor_theory_of_value.html' => array(
		'include'	 => 'src/labor_theory_of_value.php',
		'name'	 => 'page',
		'keyword'	 => 'Labor theory of value',
	),
	'labour.html' => array(
		'include'	 => 'src/labour.php',
		'name'	 => 'page',
		'keyword'	 => 'Labour',
	),
	'labour_and_value_creation.html' => array(
		'include'	 => 'src/labour_and_value_creation.php',
		'name'	 => 'page',
		'keyword'	 => 'Labour and Value Creation',
	),
	'labour_economics.html' => array(
		'include'	 => 'src/labour_economics.php',
		'name'	 => 'page',
		'keyword'	 => 'Labour economics',
	),
	'lai_ching_te.html' => array(
		'include'	 => 'src/lai_ching_te.php',
		'name'	 => 'page',
		'keyword'	 => 'Lai Ching-te',
	),
	'land.html' => array(
		'include'	 => 'src/land.php',
		'name'	 => 'page',
		'keyword'	 => 'Land',
	),
	'land_value_tax.html' => array(
		'include'	 => 'src/land_value_tax.php',
		'name'	 => 'page',
		'keyword'	 => 'Land value tax',
	),
	'laurence_tribe.html' => array(
		'include'	 => 'src/laurence_tribe.php',
		'name'	 => 'page',
		'keyword'	 => 'Laurence Tribe',
	),
	'lawmaking.html' => array(
		'include'	 => 'src/lawmaking.php',
		'name'	 => 'page',
		'keyword'	 => 'lawmaking',
	),
	'lawrence_lessig.html' => array(
		'include'	 => 'src/lawrence_lessig.php',
		'name'	 => 'page',
		'keyword'	 => 'Lawrence Lessig',
	),
	'lawrence_o_donnell.html' => array(
		'include'	 => 'src/lawrence_o_donnell.php',
		'name'	 => 'page',
		'keyword'	 => 'Lawrence O\'Donnell',
	),
	'lawyers.html' => array(
		'include'	 => 'src/lawyers.php',
		'name'	 => 'page',
		'keyword'	 => 'Lawyers',
	),
	'leadership.html' => array(
		'include'	 => 'src/leadership.php',
		'name'	 => 'page',
		'keyword'	 => 'Leadership',
	),
	'league_of_women_voters.html' => array(
		'include'	 => 'src/league_of_women_voters.php',
		'name'	 => 'page',
		'keyword'	 => 'League of Women Voters',
	),
	'league_of_women_voters_making_democracy_work.html' => array(
		'include'	 => 'src/league_of_women_voters_making_democracy_work.php',
		'name'	 => 'page',
		'keyword'	 => 'League of Women Voters: Making Democracy Work',
	),
	'league_of_women_voters_of_colorado.html' => array(
		'include'	 => 'src/league_of_women_voters_of_colorado.php',
		'name'	 => 'page',
		'keyword'	 => 'League of Women Voters of Colorado ',
	),
	'lessig_our_democracy_no_longer_represents_the_people.html' => array(
		'include'	 => 'src/lessig_our_democracy_no_longer_represents_the_people.php',
		'name'	 => 'page',
		'keyword'	 => 'Lessig: Our democracy no longer represents the people',
	),
	'let_them_eat_cake.html' => array(
		'include'	 => 'src/let_them_eat_cake.php',
		'name'	 => 'page',
		'keyword'	 => 'Let them eat cake',
	),
	'lev_parnas.html' => array(
		'include'	 => 'src/lev_parnas.php',
		'name'	 => 'page',
		'keyword'	 => 'Lev Parnas',
	),
	'liberia.html' => array(
		'include'	 => 'src/liberia.php',
		'name'	 => 'page',
		'keyword'	 => 'Liberia',
	),
	'liberty_medal.html' => array(
		'include'	 => 'src/liberty_medal.php',
		'name'	 => 'page',
		'keyword'	 => 'Liberty Medal',
	),
	'list_of_awards.html' => array(
		'include'	 => 'src/list_of_awards.php',
		'name'	 => 'page',
		'keyword'	 => 'List of awards',
	),
	'list_of_books.html' => array(
		'include'	 => 'src/list_of_books.php',
		'name'	 => 'page',
		'keyword'	 => 'list of books',
	),
	'list_of_documentaries.html' => array(
		'include'	 => 'src/list_of_documentaries.php',
		'name'	 => 'page',
		'keyword'	 => 'List of documentaries',
	),
	'list_of_indices.html' => array(
		'include'	 => 'src/list_of_indices.php',
		'name'	 => 'page',
		'keyword'	 => 'list of indices',
	),
	'list_of_movies.html' => array(
		'include'	 => 'src/list_of_movies.php',
		'name'	 => 'page',
		'keyword'	 => 'list of movies',
	),
	'list_of_organisations.html' => array(
		'include'	 => 'src/list_of_organisations.php',
		'name'	 => 'page',
		'keyword'	 => 'list of organisations',
	),
	'list_of_people.html' => array(
		'include'	 => 'src/list_of_people.php',
		'name'	 => 'page',
		'keyword'	 => 'list of people',
	),
	'list_of_research_and_reports.html' => array(
		'include'	 => 'src/list_of_research_and_reports.php',
		'name'	 => 'page',
		'keyword'	 => 'list of research and reports',
	),
	'list_of_taxes.html' => array(
		'include'	 => 'src/list_of_taxes.php',
		'name'	 => 'page',
		'keyword'	 => 'List of taxes',
	),
	'list_of_videos.html' => array(
		'include'	 => 'src/list_of_videos.php',
		'name'	 => 'page',
		'keyword'	 => 'List of videos',
	),
	'lists.html' => array(
		'include'	 => 'src/lists.php',
		'name'	 => 'page',
		'keyword'	 => 'lists',
	),
	'living.html' => array(
		'include'	 => 'src/living.php',
		'name'	 => 'page',
		'keyword'	 => 'Living',
	),
	'living_together.html' => array(
		'name'	 => 'page',
		'include'	 => 'src/living_together.php',
	),
	'logical_fallacies.html' => array(
		'include'	 => 'src/logical_fallacies.php',
		'name'	 => 'page',
		'keyword'	 => 'Logical fallacies',
	),
	'long_walk_to_freedom.html' => array(
		'include'	 => 'src/long_walk_to_freedom.php',
		'name'	 => 'page',
		'keyword'	 => 'Long Walk to Freedom',
	),
	'loper_bright_enterprises_v_raimondo.html' => array(
		'include'	 => 'src/loper_bright_enterprises_v_raimondo.php',
		'name'	 => 'page',
		'keyword'	 => 'Loper Bright Enterprises v. Raimondo',
	),
	'louis_brandeis.html' => array(
		'include'	 => 'src/louis_brandeis.php',
		'name'	 => 'page',
		'keyword'	 => 'Louis Brandeis',
	),
	'loving_v_virginia.html' => array(
		'include'	 => 'src/loving_v_virginia.php',
		'name'	 => 'page',
		'keyword'	 => 'Loving v. Virginia',
	),
	'lucky_loser_how_donald_trump_squandered_his_father_s_fortune_and_created_the_illusion_of_success.html' => array(
		'include'	 => 'src/lucky_loser_how_donald_trump_squandered_his_father_s_fortune_and_created_the_illusion_of_success.php',
		'name'	 => 'page',
		'keyword'	 => 'Lucky Loser: How Donald Trump Squandered His Father’s Fortune and Created the Illusion of Success',
	),
	'making_democracy_work.html' => array(
		'include'	 => 'src/making_democracy_work.php',
		'name'	 => 'page',
		'keyword'	 => 'Making Democracy Work',
	),
	'making_democracy_work_civic_traditions_in_modern_italy.html' => array(
		'include'	 => 'src/making_democracy_work_civic_traditions_in_modern_italy.php',
		'name'	 => 'page',
		'keyword'	 => 'Making Democracy Work: Civic Traditions in Modern Italy',
	),
	'malala_yousafzai.html' => array(
		'include'	 => 'src/malala_yousafzai.php',
		'name'	 => 'page',
		'keyword'	 => 'Malala Yousafzai',
	),
	'malaysia.html' => array(
		'include'	 => 'src/malaysia.php',
		'name'	 => 'page',
		'keyword'	 => 'Malaysia',
	),
	'mali.html' => array(
		'name'	 => 'page',
		'include'	 => 'src/mali.php',
	),
	'manisha_sinha.html' => array(
		'include'	 => 'src/manisha_sinha.php',
		'name'	 => 'page',
		'keyword'	 => 'Manisha Sinha',
	),
	'marc_elias.html' => array(
		'include'	 => 'src/marc_elias.php',
		'name'	 => 'page',
		'keyword'	 => 'Marc Elias',
	),
	'margaret_hoover.html' => array(
		'include'	 => 'src/margaret_hoover.php',
		'name'	 => 'page',
		'keyword'	 => 'Margaret Hoover',
	),
	'maria_ressa.html' => array(
		'include'	 => 'src/maria_ressa.php',
		'name'	 => 'page',
		'keyword'	 => 'Maria Ressa',
	),
	'marriage.html' => array(
		'include'	 => 'src/marriage.php',
		'name'	 => 'page',
		'keyword'	 => 'Marriage',
	),
	'mary_mccord.html' => array(
		'include'	 => 'src/mary_mccord.php',
		'name'	 => 'page',
		'keyword'	 => 'Mary McCord',
	),
	'mary_smith.html' => array(
		'include'	 => 'src/mary_smith.php',
		'name'	 => 'page',
		'keyword'	 => 'Mary Smith',
	),
	'maslow_s_hierarchy_of_needs.html' => array(
		'include'	 => 'src/maslow_s_hierarchy_of_needs.php',
		'name'	 => 'page',
		'keyword'	 => 'Maslow\'s hierarchy of needs',
	),
	'mass_shootings_the_human_factor.html' => array(
		'include'	 => 'src/mass_shootings_the_human_factor.php',
		'name'	 => 'page',
		'keyword'	 => 'Mass shootings: the human factor',
	),
	'matthew_seligman.html' => array(
		'include'	 => 'src/matthew_seligman.php',
		'name'	 => 'page',
		'keyword'	 => 'Matthew Seligman',
	),
	'maurice_allais.html' => array(
		'include'	 => 'src/maurice_allais.php',
		'name'	 => 'page',
		'keyword'	 => 'Maurice Allais',
	),
	'maurice_duverger.html' => array(
		'include'	 => 'src/maurice_duverger.php',
		'name'	 => 'page',
		'keyword'	 => 'Maurice Duverger',
	),
	'mcculloch_v_maryland.html' => array(
		'include'	 => 'src/mcculloch_v_maryland.php',
		'name'	 => 'page',
		'keyword'	 => 'McCulloch v. Maryland',
	),
	'mcdonald_v_city_of_chicago.html' => array(
		'include'	 => 'src/mcdonald_v_city_of_chicago.php',
		'name'	 => 'page',
		'keyword'	 => 'McDonald v. City of Chicago',
	),
	'media.html' => array(
		'include'	 => 'src/media.php',
		'name'	 => 'page',
		'keyword'	 => 'media',
	),
	'menu.html' => array(
		'include'	 => 'src/menu.php',
		'name'	 => 'page',
		'keyword'	 => 'menu',
	),
	'mexico.html' => array(
		'include'	 => 'src/mexico.php',
		'name'	 => 'page',
		'keyword'	 => 'Mexico',
	),
	'michael_honey.html' => array(
		'include'	 => 'src/michael_honey.php',
		'name'	 => 'page',
		'keyword'	 => 'Michael Honey',
	),
	'michael_luttig.html' => array(
		'include'	 => 'src/michael_luttig.php',
		'name'	 => 'page',
		'keyword'	 => ' Michael Luttig',
	),
	'michael_schmidt.html' => array(
		'include'	 => 'src/michael_schmidt.php',
		'name'	 => 'page',
		'keyword'	 => 'Michael Schmidt',
	),
	'michael_schmidt_how_donald_trump_s_threats_led_to_investigations_and_audits_of_his_enemies_while_he_was_president.html' => array(
		'include'	 => 'src/michael_schmidt_how_donald_trump_s_threats_led_to_investigations_and_audits_of_his_enemies_while_he_was_president.php',
		'name'	 => 'page',
		'keyword'	 => 'Michael Schmidt: How Donald Trump’s threats led to investigations and audits of his enemies while he was President',
	),
	'migrant_workers.html' => array(
		'include'	 => 'src/migrant_workers.php',
		'name'	 => 'page',
		'keyword'	 => 'Migrant worker',
	),
	'migration_law.html' => array(
		'include'	 => 'src/migration_law.php',
		'name'	 => 'page',
		'keyword'	 => 'Migration law',
	),
	'miles_taylor.html' => array(
		'include'	 => 'src/miles_taylor.php',
		'name'	 => 'page',
		'keyword'	 => 'Miles Taylor',
	),
	'military.html' => array(
		'include'	 => 'src/military.php',
		'name'	 => 'page',
		'keyword'	 => 'Military',
	),
	'military_and_security_developments_involving_the_people_s_republic_of_china_2023_dod_report.html' => array(
		'include'	 => 'src/military_and_security_developments_involving_the_people_s_republic_of_china_2023_dod_report.php',
		'name'	 => 'page',
		'keyword'	 => 'Military and Security Developments Involving the People\'s Republic of China 2023 DOD report',
	),
	'miranda_v_arizona.html' => array(
		'include'	 => 'src/miranda_v_arizona.php',
		'name'	 => 'page',
		'keyword'	 => 'Miranda v. Arizona',
	),
	'missing_migrants_project.html' => array(
		'include'	 => 'src/missing_migrants_project.php',
		'name'	 => 'page',
		'keyword'	 => 'Missing Migrants Project',
	),
	'mo_gawdat.html' => array(
		'include'	 => 'src/mo_gawdat.php',
		'name'	 => 'page',
		'keyword'	 => 'Mo Gawdat',
	),
	'montesquieu.html' => array(
		'include'	 => 'src/montesquieu.php',
		'name'	 => 'page',
		'keyword'	 => 'Montesquieu',
	),
	'museum_of_political_corruption.html' => array(
		'include'	 => 'src/museum_of_political_corruption.php',
		'name'	 => 'page',
		'keyword'	 => 'Museum of Political Corruption',
	),
	'myanmar.html' => array(
		'include'	 => 'src/myanmar.php',
		'name'	 => 'page',
		'keyword'	 => 'Myanmar',
	),
	'myanmar_the_rebel_army_arte_tv_documentary.html' => array(
		'include'	 => 'src/myanmar_the_rebel_army_arte_tv_documentary.php',
		'name'	 => 'page',
		'keyword'	 => 'Myanmar: The Rebel Army | ARTE.tv Documentary',
	),
	'narges_mohammadi.html' => array(
		'include'	 => 'src/narges_mohammadi.php',
		'name'	 => 'page',
		'keyword'	 => 'Narges Mohammadi',
	),
	'national_chengchi_university.html' => array(
		'include'	 => 'src/national_chengchi_university.php',
		'name'	 => 'page',
		'keyword'	 => 'National Chengchi University',
	),
	'national_coalition_against_censorship.html' => array(
		'include'	 => 'src/national_coalition_against_censorship.php',
		'name'	 => 'page',
		'keyword'	 => 'National Coalition Against Censorship',
	),
	'national_constitution_center.html' => array(
		'include'	 => 'src/national_constitution_center.php',
		'name'	 => 'page',
		'keyword'	 => 'National Constitution Center',
	),
	'national_constitution_center_a_conversation_with_justice_neil_gorsuch_on_the_human_toll_of_too_much_law.html' => array(
		'include'	 => 'src/national_constitution_center_a_conversation_with_justice_neil_gorsuch_on_the_human_toll_of_too_much_law.php',
		'name'	 => 'page',
		'keyword'	 => 'National Constitution Center: A Conversation With Justice Neil Gorsuch on ‘The Human Toll of Too Much Law’',
	),
	'national_constitution_center_the_2024_liberty_medal_ceremony_honoring_ken_burns.html' => array(
		'include'	 => 'src/national_constitution_center_the_2024_liberty_medal_ceremony_honoring_ken_burns.php',
		'name'	 => 'page',
		'keyword'	 => 'National Constitution Center: The 2024 Liberty Medal Ceremony Honoring Ken Burns',
	),
	'national_constitution_center_we_the_people.html' => array(
		'include'	 => 'src/national_constitution_center_we_the_people.php',
		'name'	 => 'page',
		'keyword'	 => 'National Constitution Center: We the People',
	),
	'national_federation_of_independent_business_v_sebelius.html' => array(
		'include'	 => 'src/national_federation_of_independent_business_v_sebelius.php',
		'name'	 => 'page',
		'keyword'	 => 'National Federation of Independent Business v. Sebelius',
	),
	'national_immigrant_justice_center.html' => array(
		'include'	 => 'src/national_immigrant_justice_center.php',
		'name'	 => 'page',
		'keyword'	 => 'National Immigrant Justice Center',
	),
	'neil_gorsuch.html' => array(
		'include'	 => 'src/neil_gorsuch.php',
		'name'	 => 'page',
		'keyword'	 => 'Neil Gorsuch',
	),
	'nelson_mandela.html' => array(
		'include'	 => 'src/nelson_mandela.php',
		'name'	 => 'page',
		'keyword'	 => 'Nelson Mandela',
	),
	'new_zealand.html' => array(
		'include'	 => 'src/new_zealand.php',
		'name'	 => 'page',
		'keyword'	 => 'New Zealand',
	),
	'nexus_a_brief_history_of_information_networks_from_the_stone_age_to_ai.html' => array(
		'include'	 => 'src/nexus_a_brief_history_of_information_networks_from_the_stone_age_to_ai.php',
		'name'	 => 'page',
		'keyword'	 => 'Nexus: A Brief History of Information Networks from the Stone Age to AI ',
	),
	'nicaragua.html' => array(
		'include'	 => 'src/nicaragua.php',
		'name'	 => 'page',
		'keyword'	 => 'Nicaragua',
	),
	'niger.html' => array(
		'include'	 => 'src/niger.php',
		'name'	 => 'page',
		'keyword'	 => 'Niger',
	),
	'nineteen_eighty_four.html' => array(
		'include'	 => 'src/nineteen_eighty_four.php',
		'name'	 => 'page',
		'keyword'	 => 'Nineteen Eighty-Four',
	),
	'no_democracy_lasts_forever.html' => array(
		'include'	 => 'src/no_democracy_lasts_forever.php',
		'name'	 => 'page',
		'keyword'	 => 'No Democracy Lasts Forever',
	),
	'nobel_peace_prize.html' => array(
		'include'	 => 'src/nobel_peace_prize.php',
		'name'	 => 'page',
		'keyword'	 => 'Nobel Peace Prize',
	),
	'north_american_congress_on_latin_america.html' => array(
		'include'	 => 'src/north_american_congress_on_latin_america.php',
		'name'	 => 'page',
		'keyword'	 => 'North American Congress on Latin America',
	),
	'obergefell_v_hodges.html' => array(
		'include'	 => 'src/obergefell_v_hodges.php',
		'name'	 => 'page',
		'keyword'	 => 'Obergefell v. Hodges',
	),
	'obligations_in_democracy.html' => array(
		'include'	 => 'src/obligations_in_democracy.php',
		'name'	 => 'page',
		'keyword'	 => 'Obligations in democracy',
	),
	'observer_expectancy_effect.html' => array(
		'include'	 => 'src/observer_expectancy_effect.php',
		'name'	 => 'page',
		'keyword'	 => 'Observer-expectancy effect',
	),
	'obstruction_of_justice.html' => array(
		'include'	 => 'src/obstruction_of_justice.php',
		'name'	 => 'page',
		'keyword'	 => 'Obstruction of justice',
	),
	'oecd.html' => array(
		'include'	 => 'src/oecd.php',
		'name'	 => 'page',
		'keyword'	 => 'OECD',
	),
	'offshoring.html' => array(
		'include'	 => 'src/offshoring.php',
		'name'	 => 'page',
		'keyword'	 => 'Offshoring',
	),
	'oligarchy.html' => array(
		'include'	 => 'src/oligarchy.php',
		'name'	 => 'page',
		'keyword'	 => 'Oligarchy',
	),
	'one_country_two_systems.html' => array(
		'include'	 => 'src/one_country_two_systems.php',
		'name'	 => 'page',
		'keyword'	 => 'One country, two systems',
	),
	'open_source_democracy.html' => array(
		'include'	 => 'src/open_source_democracy.php',
		'name'	 => 'page',
		'keyword'	 => 'Open Source Democracy',
	),
	'open_source_voting_system.html' => array(
		'include'	 => 'src/open_source_voting_system.php',
		'name'	 => 'page',
		'keyword'	 => 'Open-source voting system',
	),
	'orange_revolution.html' => array(
		'include'	 => 'src/orange_revolution.php',
		'name'	 => 'page',
		'keyword'	 => 'Orange Revolution',
	),
	'organic_taxes.html' => array(
		'include'	 => 'src/organic_taxes.php',
		'name'	 => 'page',
		'keyword'	 => 'Organic Taxes',
	),
	'organic_taxes_the_cure_for_healthcare_funding.html' => array(
		'include'	 => 'src/organic_taxes_the_cure_for_healthcare_funding.php',
		'name'	 => 'page',
		'keyword'	 => 'Organic Taxes: The Cure for Healthcare Funding',
	),
	'organization_of_american_states.html' => array(
		'include'	 => 'src/organization_of_american_states.php',
		'name'	 => 'page',
		'keyword'	 => 'Organization of American States',
	),
	'orwellian_doublespeak.html' => array(
		'include'	 => 'src/orwellian_doublespeak.php',
		'name'	 => 'page',
		'keyword'	 => 'Orwellian Doublespeak in today\'s political discourse',
	),
	'osce.html' => array(
		'name'	 => 'page',
		'include'	 => 'src/osce.php',
	),
	'over_ruled_the_human_toll_of_too_much_law.html' => array(
		'include'	 => 'src/over_ruled_the_human_toll_of_too_much_law.php',
		'name'	 => 'page',
		'keyword'	 => 'Over Ruled: The Human Toll of Too Much Law',
	),
	'oyez_project.html' => array(
		'include'	 => 'src/oyez_project.php',
		'name'	 => 'page',
		'keyword'	 => 'Oyez Project',
	),
	'pacer_public_access_to_court_electronic_records.html' => array(
		'include'	 => 'src/pacer_public_access_to_court_electronic_records.php',
		'name'	 => 'page',
		'keyword'	 => 'PACER Public Access to Court Electronic Records ',
	),
	'pakistan.html' => array(
		'include'	 => 'src/pakistan.php',
		'name'	 => 'page',
		'keyword'	 => 'Pakistan',
	),
	'palestine.html' => array(
		'include'	 => 'src/palestine.php',
		'name'	 => 'page',
		'keyword'	 => 'Palestine',
	),
	'panama.html' => array(
		'include'	 => 'src/panama.php',
		'name'	 => 'page',
		'keyword'	 => 'Panama',
	),
	'payton_mcgriff.html' => array(
		'include'	 => 'src/payton_mcgriff.php',
		'name'	 => 'page',
		'keyword'	 => 'Payton McGriff',
	),
	'peace.html' => array(
		'include'	 => 'src/peace.php',
		'name'	 => 'page',
		'keyword'	 => 'Peace',
	),
	'peace_pilgrim.html' => array(
		'include'	 => 'src/peace_pilgrim.php',
		'name'	 => 'page',
		'keyword'	 => 'Peace Pilgrim',
	),
	'peaceful_resistance_in_times_of_war.html' => array(
		'name'	 => 'page',
		'include'	 => 'src/peaceful_resistance_in_times_of_war.php',
	),
	'permission_structures.html' => array(
		'include'	 => 'src/permission_structures.php',
		'name'	 => 'page',
		'keyword'	 => 'Permission Structure',
	),
	'personal_responsibility.html' => array(
		'include'	 => 'src/personal_responsibility.php',
		'name'	 => 'page',
		'keyword'	 => ' Personal Responsibility',
	),
	'personal_responsibility_rhetoric.html' => array(
		'include'	 => 'src/personal_responsibility_rhetoric.php',
		'name'	 => 'page',
		'keyword'	 => 'Personal Responsibility Rhetoric ',
	),
	'philippine_human_rights_act.html' => array(
		'include'	 => 'src/philippine_human_rights_act.php',
		'name'	 => 'page',
		'keyword'	 => 'Philippine Human Rights Act',
	),
	'philippines.html' => array(
		'include'	 => 'src/philippines.php',
		'name'	 => 'page',
		'keyword'	 => 'Philippines',
	),
	'pigouvian_tax.html' => array(
		'include'	 => 'src/pigouvian_tax.php',
		'name'	 => 'page',
		'keyword'	 => 'Pigouvian tax',
	),
	'plessy_v_ferguson.html' => array(
		'include'	 => 'src/plessy_v_ferguson.php',
		'name'	 => 'page',
		'keyword'	 => 'Plessy v. Ferguson',
	),
	'plurality_voting.html' => array(
		'include'	 => 'src/plurality_voting.php',
		'name'	 => 'page',
		'keyword'	 => 'Plurality voting',
	),
	'political_discourse.html' => array(
		'include'	 => 'src/political_discourse.php',
		'name'	 => 'page',
		'keyword'	 => 'political discourse',
	),
	'political_polarisation.html' => array(
		'include'	 => 'src/political_polarisation.php',
		'name'	 => 'page',
	),
	'political_repression.html' => array(
		'include'	 => 'src/political_repression.php',
		'name'	 => 'page',
		'keyword'	 => 'Political repression',
	),
	'political_science_in_taiwan.html' => array(
		'include'	 => 'src/political_science_in_taiwan.php',
		'name'	 => 'page',
		'keyword'	 => 'Political science in Taiwan',
	),
	'politics.html' => array(
		'include'	 => 'src/politics.php',
		'name'	 => 'page',
		'keyword'	 => 'politics',
	),
	'pollution.html' => array(
		'include'	 => 'src/pollution.php',
		'name'	 => 'page',
		'keyword'	 => 'Pollution',
	),
	'poor_people_s_campaign_a_national_call_for_a_moral_revival.html' => array(
		'include'	 => 'src/poor_people_s_campaign_a_national_call_for_a_moral_revival.php',
		'name'	 => 'page',
		'keyword'	 => 'Poor People\'s Campaign: A National Call for a Moral Revival',
	),
	'populism.html' => array(
		'include'	 => 'src/populism.php',
		'name'	 => 'page',
		'keyword'	 => 'Populism',
	),
	'poverty.html' => array(
		'include'	 => 'src/poverty.php',
		'name'	 => 'page',
		'keyword'	 => 'Poverty',
	),
	'prc_china.html' => array(
		'include'	 => 'src/prc_china.php',
		'name'	 => 'page',
		'keyword'	 => 'China',
	),
	'prequel_an_american_fight_against_fascism.html' => array(
		'include'	 => 'src/prequel_an_american_fight_against_fascism.php',
		'name'	 => 'page',
		'keyword'	 => 'Prequel: An American Fight Against Fascism',
	),
	'president_as_saviour_syndrome.html' => array(
		'include'	 => 'src/president_as_saviour_syndrome.php',
		'name'	 => 'page',
		'keyword'	 => 'President as Savior Syndrome',
	),
	'president_biden_farewell_address.html' => array(
		'include'	 => 'src/president_biden_farewell_address.php',
		'name'	 => 'page',
		'keyword'	 => 'President Biden Farewell Address',
	),
	'president_biden_interview_with_lawrence_o_donnell_january_2025.html' => array(
		'include'	 => 'src/president_biden_interview_with_lawrence_o_donnell_january_2025.php',
		'name'	 => 'page',
		'keyword'	 => 'President Biden: Last interview with Lawrence O\'Donnell',
	),
	'president_of_the_united_states.html' => array(
		'include'	 => 'src/president_of_the_united_states.php',
		'name'	 => 'page',
		'keyword'	 => 'President of the United States',
	),
	'primary_elections.html' => array(
		'include'	 => 'src/primary_elections.php',
		'name'	 => 'page',
		'keyword'	 => 'Primary Elections',
	),
	'professionalism_without_elitism.html' => array(
		'include'	 => 'src/professionalism_without_elitism.php',
		'name'	 => 'page',
		'keyword'	 => 'professionalism without elitism',
	),
	'profiles_in_ignorance_how_america_s_politicians_got_dumb_and_dumber.html' => array(
		'include'	 => 'src/profiles_in_ignorance_how_america_s_politicians_got_dumb_and_dumber.php',
		'name'	 => 'page',
		'keyword'	 => 'Profiles in Ignorance – How America\'s Politicians Got Dumb and Dumber',
	),
	'profitable_factories_shutting_down.html' => array(
		'include'	 => 'src/profitable_factories_shutting_down.php',
		'name'	 => 'page',
		'keyword'	 => 'Profitable Factories Shutting Down',
	),
	'progress_and_poverty.html' => array(
		'include'	 => 'src/progress_and_poverty.php',
		'name'	 => 'page',
		'keyword'	 => 'Progress and Poverty',
	),
	'project/codeberg.html' => array(
		'include'	 => 'src/project/codeberg.php',
		'name'	 => 'page',
		'keyword'	 => 'Codeberg',
	),
	'project/copyright.html' => array(
		'include'	 => 'src/project/copyright.php',
		'name'	 => 'page',
		'keyword'	 => 'copyright',
	),
	'project/development_scripts.html' => array(
		'include'	 => 'src/project/development_scripts.php',
		'name'	 => 'page',
		'keyword'	 => 'development scripts',
	),
	'project/development_website.html' => array(
		'include'	 => 'src/project/development_website.php',
		'name'	 => 'page',
		'keyword'	 => 'development website',
	),
	'project/git.html' => array(
		'include'	 => 'src/project/git.php',
		'name'	 => 'page',
		'keyword'	 => 'git',
	),
	'project/index.html' => array(
		'include'	 => 'src/project/index.php',
		'name'	 => 'page',
		'keyword'	 => 'project',
	),
	'project/participate.html' => array(
		'name'	 => 'page',
		'include'	 => 'src/project/participate.php',
	),
	'project/sister_projects.html' => array(
		'include'	 => 'src/project/sister_projects.php',
		'name'	 => 'page',
		'keyword'	 => 'Sister projects',
	),
	'project/standing_on_the_shoulders_of_giants.html' => array(
		'include'	 => 'src/project/standing_on_the_shoulders_of_giants.php',
		'name'	 => 'page',
		'keyword'	 => 'standing on the shoulders of giants',
	),
	'project/taiwan_democracy.html' => array(
		'include'	 => 'src/project/taiwan_democracy.php',
		'name'	 => 'page',
		'keyword'	 => 'Taiwan Democracy',
	),
	'project/updates.html' => array(
		'name'	 => 'page',
		'include'	 => 'src/project/updates.php',
	),
	'project/videos_tv_programs_interviews_and_documentaries.html' => array(
		'include'	 => 'src/project/videos_tv_programs_interviews_and_documentaries.php',
		'name'	 => 'page',
		'keyword'	 => 'Videos, TV programs, interviews and documentaries',
	),
	'project/vim.html' => array(
		'include'	 => 'src/project/vim.php',
		'name'	 => 'page',
		'keyword'	 => 'Vim',
	),
	'project/wikipedia.html' => array(
		'include'	 => 'src/project/wikipedia.php',
		'name'	 => 'page',
		'keyword'	 => 'wikipedia',
	),
	'project_2025.html' => array(
		'include'	 => 'src/project_2025.php',
		'name'	 => 'page',
		'keyword'	 => 'Project 2025',
	),
	'project_honey_pot.html' => array(
		'include'	 => 'src/project_honey_pot.php',
		'name'	 => 'page',
		'keyword'	 => 'Project Honey Pot',
	),
	'propaganda.html' => array(
		'include'	 => 'src/propaganda.php',
		'name'	 => 'page',
		'keyword'	 => 'Propaganda',
	),
	'property.html' => array(
		'include'	 => 'src/property.php',
		'name'	 => 'page',
		'keyword'	 => 'Property',
	),
	'property_tax.html' => array(
		'include'	 => 'src/property_tax.php',
		'name'	 => 'page',
		'keyword'	 => 'Property Tax',
	),
	'propublica.html' => array(
		'include'	 => 'src/propublica.php',
		'name'	 => 'page',
		'keyword'	 => 'ProPublica',
	),
	'prosecuting_donald_trump.html' => array(
		'include'	 => 'src/prosecuting_donald_trump.php',
		'name'	 => 'page',
		'keyword'	 => 'Prosecuting Donald Trump',
	),
	'prosecuting_donald_trump_election_security_matters.html' => array(
		'include'	 => 'src/prosecuting_donald_trump_election_security_matters.php',
		'name'	 => 'page',
		'keyword'	 => 'Prosecuting Donald Trump: Election Security Matters',
	),
	'public_broadcasting_service.html' => array(
		'include'	 => 'src/public_broadcasting_service.php',
		'name'	 => 'page',
		'keyword'	 => 'Public Broadcasting Service',
	),
	'quantity_of_information.html' => array(
		'include'	 => 'src/quantity_of_information.php',
		'name'	 => 'page',
		'keyword'	 => 'quantity of information',
	),
	'rachel_maddow.html' => array(
		'include'	 => 'src/rachel_maddow.php',
		'name'	 => 'page',
		'keyword'	 => 'Rachel Maddow',
	),
	'rachel_maddow_interviews_lev_parnas_part_1.html' => array(
		'include'	 => 'src/rachel_maddow_interviews_lev_parnas_part_1.php',
		'name'	 => 'page',
		'keyword'	 => 'Rachel Maddow Interviews Lev Parnas - Part 1',
	),
	'rachel_maddow_interviews_lev_parnas_part_2.html' => array(
		'include'	 => 'src/rachel_maddow_interviews_lev_parnas_part_2.php',
		'name'	 => 'page',
		'keyword'	 => 'Rachel Maddow Interviews Lev Parnas - Part 2',
	),
	'racism.html' => array(
		'include'	 => 'src/racism.php',
		'name'	 => 'page',
		'keyword'	 => 'Racism',
	),
	'ranked_voting.html' => array(
		'include'	 => 'src/ranked_voting.php',
		'name'	 => 'page',
		'keyword'	 => 'Ranked Voting',
	),
	'rated_voting.html' => array(
		'include'	 => 'src/rated_voting.php',
		'name'	 => 'page',
		'keyword'	 => 'Rated Voting',
	),
	'redistribution_of_wealth_a_false_solution.html' => array(
		'include'	 => 'src/redistribution_of_wealth_a_false_solution.php',
		'name'	 => 'page',
		'keyword'	 => 'Redistribution of wealth: a false solution',
	),
	'refugee_camps.html' => array(
		'include'	 => 'src/refugee_camps.php',
		'name'	 => 'page',
		'keyword'	 => 'Refugee camps',
	),
	'refugees.html' => array(
		'include'	 => 'src/refugees.php',
		'name'	 => 'page',
		'keyword'	 => 'Refugees',
	),
	'religion.html' => array(
		'include'	 => 'src/religion.php',
		'name'	 => 'page',
		'keyword'	 => 'Religion',
	),
	'renee_diresta.html' => array(
		'include'	 => 'src/renee_diresta.php',
		'name'	 => 'page',
		'keyword'	 => 'Renée DiResta',
	),
	'renewable_energy.html' => array(
		'include'	 => 'src/renewable_energy.php',
		'name'	 => 'page',
		'keyword'	 => 'Renewable energy',
	),
	'reporters_committee_for_freedom_of_the_press.html' => array(
		'include'	 => 'src/reporters_committee_for_freedom_of_the_press.php',
		'name'	 => 'page',
		'keyword'	 => 'Reporters Committee for Freedom of the Press',
	),
	'reporters_without_borders.html' => array(
		'name'	 => 'page',
		'include'	 => 'src/reporters_without_borders.php',
	),
	'results_organization.html' => array(
		'include'	 => 'src/results_organization.php',
		'name'	 => 'page',
		'keyword'	 => 'RESULTS ((organization)',
	),
	'revolution_of_dignity.html' => array(
		'include'	 => 'src/revolution_of_dignity.php',
		'name'	 => 'page',
		'keyword'	 => 'Revolution of Dignity',
	),
	'richard_bluhm.html' => array(
		'include'	 => 'src/richard_bluhm.php',
		'name'	 => 'page',
		'keyword'	 => 'Richard Bluhm',
	),
	'richard_daynard.html' => array(
		'include'	 => 'src/richard_daynard.php',
		'name'	 => 'page',
		'keyword'	 => 'Richard Daynard',
	),
	'richard_hasen.html' => array(
		'include'	 => 'src/richard_hasen.php',
		'name'	 => 'page',
		'keyword'	 => 'Richard Hasen',
	),
	'richard_stengel.html' => array(
		'include'	 => 'src/richard_stengel.php',
		'name'	 => 'page',
		'keyword'	 => 'Richard Stengel',
	),
	'right_to_marriage.html' => array(
		'include'	 => 'src/right_to_marriage.php',
		'name'	 => 'page',
		'keyword'	 => 'Right to Marriage',
	),
	'rights.html' => array(
		'include'	 => 'src/rights.php',
		'name'	 => 'page',
		'keyword'	 => 'Rights',
	),
	'robert_putnam.html' => array(
		'include'	 => 'src/robert_putnam.php',
		'name'	 => 'page',
		'keyword'	 => 'Robert Putnam',
	),
	'robert_reich.html' => array(
		'include'	 => 'src/robert_reich.php',
		'name'	 => 'page',
		'keyword'	 => 'Robert Reich',
	),
	'robert_reich_the_republican_party_is_over.html' => array(
		'include'	 => 'src/robert_reich_the_republican_party_is_over.php',
		'name'	 => 'page',
		'keyword'	 => 'Robert Reich: The Republican Party Is Over',
	),
	'robert_reich_you_are_being_lied_to_about_inflation.html' => array(
		'include'	 => 'src/robert_reich_you_are_being_lied_to_about_inflation.php',
		'name'	 => 'page',
		'keyword'	 => 'Robert Reich: You Are Being Lied to About Inflation',
	),
	'roe_v_wade.html' => array(
		'include'	 => 'src/roe_v_wade.php',
		'name'	 => 'page',
		'keyword'	 => 'Roe v. Wade',
	),
	'rohingya_people.html' => array(
		'include'	 => 'src/rohingya_people.php',
		'name'	 => 'page',
		'keyword'	 => 'Rohingya people',
	),
	'rolf_dobelli.html' => array(
		'include'	 => 'src/rolf_dobelli.php',
		'name'	 => 'page',
		'keyword'	 => 'Rolf Dobelli',
	),
	'russia.html' => array(
		'include'	 => 'src/russia.php',
		'name'	 => 'page',
		'keyword'	 => 'Russia',
	),
	'russian_interference_in_us_elections.html' => array(
		'include'	 => 'src/russian_interference_in_us_elections.php',
		'name'	 => 'page',
		'keyword'	 => 'Russian interference in US elections',
	),
	'ruth_ben_ghiat.html' => array(
		'include'	 => 'src/ruth_ben_ghiat.php',
		'name'	 => 'page',
		'keyword'	 => 'Ruth Ben-Ghiat',
	),
	'rwanda.html' => array(
		'include'	 => 'src/rwanda.php',
		'name'	 => 'page',
		'keyword'	 => 'Rwanda',
	),
	'saints_and_little_devils.html' => array(
		'include'	 => 'src/saints_and_little_devils.php',
		'name'	 => 'page',
		'keyword'	 => 'Saints and Little Devils',
	),
	'sarah_longwell.html' => array(
		'include'	 => 'src/sarah_longwell.php',
		'name'	 => 'page',
		'keyword'	 => 'Sarah Longwell',
	),
	'sarcasm.html' => array(
		'include'	 => 'src/sarcasm.php',
		'name'	 => 'page',
		'keyword'	 => 'Sarcasm',
	),
	'saudi_arabia.html' => array(
		'include'	 => 'src/saudi_arabia.php',
		'name'	 => 'page',
		'keyword'	 => 'Saudi Arabia',
	),
	'schumacher_center_for_a_new_economics.html' => array(
		'include'	 => 'src/schumacher_center_for_a_new_economics.php',
		'name'	 => 'page',
		'keyword'	 => 'Schumacher Center for a New Economics',
	),
	'score_voting.html' => array(
		'include'	 => 'src/score_voting.php',
		'name'	 => 'page',
		'keyword'	 => 'Score voting',
	),
	'sean_penn.html' => array(
		'include'	 => 'src/sean_penn.php',
		'name'	 => 'page',
		'keyword'	 => 'Sean Penn',
	),
	'second_level_of_democracy.html' => array(
		'include'	 => 'src/second_level_of_democracy.php',
		'name'	 => 'page',
		'keyword'	 => 'secondary features of democracy',
	),
	'separation_of_powers.html' => array(
		'include'	 => 'src/separation_of_powers.php',
		'name'	 => 'page',
		'keyword'	 => 'Separation of powers',
	),
	'seventh_level_of_democracy.html' => array(
		'include'	 => 'src/seventh_level_of_democracy.php',
		'name'	 => 'page',
		'keyword'	 => 'Seventh level of democracy',
	),
	'shameless_republicans_deliberate_dysfunction_and_the_battle_to_preserve_democracy.html' => array(
		'include'	 => 'src/shameless_republicans_deliberate_dysfunction_and_the_battle_to_preserve_democracy.php',
		'name'	 => 'page',
		'keyword'	 => 'Shameless Republicans Deliberate Dysfunction and the Battle to Preserve Democracy',
	),
	'sharon_brous.html' => array(
		'include'	 => 'src/sharon_brous.php',
		'name'	 => 'page',
		'keyword'	 => 'Sharon Brous',
	),
	'shelby_county_v_holder.html' => array(
		'include'	 => 'src/shelby_county_v_holder.php',
		'name'	 => 'page',
		'keyword'	 => 'Shelby County v. Holder',
	),
	'single_choice_voting.html' => array(
		'include'	 => 'src/single_choice_voting.php',
		'name'	 => 'page',
		'keyword'	 => 'Single Choice Voting',
	),
	'sixth_level_of_democracy.html' => array(
		'include'	 => 'src/sixth_level_of_democracy.php',
		'name'	 => 'page',
		'keyword'	 => 'Sixth level of democracy',
	),
	'small_government.html' => array(
		'include'	 => 'src/small_government.php',
		'name'	 => 'page',
		'keyword'	 => 'Small government',
	),
	'small_is_beautiful.html' => array(
		'include'	 => 'src/small_is_beautiful.php',
		'name'	 => 'page',
		'keyword'	 => 'Small Is Beautiful',
	),
	'social_capital.html' => array(
		'include'	 => 'src/social_capital.php',
		'name'	 => 'page',
		'keyword'	 => 'Social Capital',
	),
	'social_dress_code.html' => array(
		'include'	 => 'src/social_dress_code.php',
		'name'	 => 'page',
		'keyword'	 => 'social dress code',
	),
	'social_justice.html' => array(
		'include'	 => 'src/social_justice.php',
		'name'	 => 'page',
		'keyword'	 => 'Social justice',
	),
	'social_media_and_democracy.html' => array(
		'include'	 => 'src/social_media_and_democracy.php',
		'name'	 => 'page',
		'keyword'	 => 'Social Media and Democracy',
	),
	'social_media_and_election_integrity.html' => array(
		'include'	 => 'src/social_media_and_election_integrity.php',
		'name'	 => 'page',
		'keyword'	 => 'Social Media and Election Integrity',
	),
	'social_media_ban.html' => array(
		'include'	 => 'src/social_media_ban.php',
		'name'	 => 'page',
		'keyword'	 => 'Social media ban',
	),
	'social_networks.html' => array(
		'include'	 => 'src/social_networks.php',
		'name'	 => 'page',
		'keyword'	 => 'Social networks',
	),
	'society.html' => array(
		'include'	 => 'src/society.php',
		'name'	 => 'page',
		'keyword'	 => 'Society',
	),
	'somalia.html' => array(
		'include'	 => 'src/somalia.php',
		'name'	 => 'page',
		'keyword'	 => 'Somalia',
	),
	'soochow_university.html' => array(
		'include'	 => 'src/soochow_university.php',
		'name'	 => 'page',
		'keyword'	 => 'Soochow University',
	),
	'south_africa.html' => array(
		'include'	 => 'src/south_africa.php',
		'name'	 => 'page',
		'keyword'	 => 'South Africa',
	),
	'south_mountain_company.html' => array(
		'include'	 => 'src/south_mountain_company.php',
		'name'	 => 'page',
		'keyword'	 => 'South Mountain Company',
	),
	'special_counsel_jack_smith_report_on_2020_election_subversion_efforts.html' => array(
		'include'	 => 'src/special_counsel_jack_smith_report_on_2020_election_subversion_efforts.php',
		'name'	 => 'page',
		'keyword'	 => 'Special Counsel Jack Smith Report on 2020 Election Subversion Efforts',
	),
	'spirit.html' => array(
		'include'	 => 'src/spirit.php',
		'name'	 => 'page',
		'keyword'	 => 'Spirit',
	),
	'springfield_ohio_cat_eating_hoax.html' => array(
		'include'	 => 'src/springfield_ohio_cat_eating_hoax.php',
		'name'	 => 'page',
		'keyword'	 => 'Springfield, Ohio, cat-eating hoax',
	),
	'staffan_lindberg.html' => array(
		'include'	 => 'src/staffan_lindberg.php',
		'name'	 => 'page',
		'keyword'	 => 'Staffan Lindberg',
	),
	'stanford_internet_observatory.html' => array(
		'include'	 => 'src/stanford_internet_observatory.php',
		'name'	 => 'page',
		'keyword'	 => 'Stanford Internet Observatory',
	),
	'steve_ballmer.html' => array(
		'include'	 => 'src/steve_ballmer.php',
		'name'	 => 'page',
		'keyword'	 => 'Steve Ballmer',
	),
	'steven_hahn.html' => array(
		'include'	 => 'src/steven_hahn.php',
		'name'	 => 'page',
		'keyword'	 => 'Steven Hahn',
	),
	'steven_hassan.html' => array(
		'include'	 => 'src/steven_hassan.php',
		'name'	 => 'page',
		'keyword'	 => 'Steven Hassan',
	),
	'stochastic_terrorism.html' => array(
		'include'	 => 'src/stochastic_terrorism.php',
		'name'	 => 'page',
		'keyword'	 => 'Stochastic terrorism',
	),
	'strongmen_mussolini_to_the_present.html' => array(
		'include'	 => 'src/strongmen_mussolini_to_the_present.php',
		'name'	 => 'page',
		'keyword'	 => 'Strongmen: From Mussolini to the Present',
	),
	'style_her_empowered.html' => array(
		'include'	 => 'src/style_her_empowered.php',
		'name'	 => 'page',
		'keyword'	 => 'Style Her Empowered',
	),
	'subsidiarity.html' => array(
		'include'	 => 'src/subsidiarity.php',
		'name'	 => 'page',
		'keyword'	 => 'Subsidiarity',
	),
	'supreme_court.html' => array(
		'include'	 => 'src/supreme_court.php',
		'name'	 => 'page',
		'keyword'	 => 'Supreme court',
	),
	'supreme_court_of_the_united_states.html' => array(
		'include'	 => 'src/supreme_court_of_the_united_states.php',
		'name'	 => 'page',
		'keyword'	 => 'Supreme Court of the United States',
	),
	'sweden.html' => array(
		'include'	 => 'src/sweden.php',
		'name'	 => 'page',
		'keyword'	 => 'Sweden',
	),
	'systemic_justice_project.html' => array(
		'include'	 => 'src/systemic_justice_project.php',
		'name'	 => 'page',
		'keyword'	 => 'Systemic Justice Project',
	),
	'tactical_voting.html' => array(
		'include'	 => 'src/tactical_voting.php',
		'name'	 => 'page',
		'keyword'	 => 'Tactical voting',
	),
	'taiwan.html' => array(
		'include'	 => 'src/taiwan.php',
		'name'	 => 'page',
		'keyword'	 => 'Taiwan',
	),
	'taiwan_election_and_democratization_study.html' => array(
		'include'	 => 'src/taiwan_election_and_democratization_study.php',
		'name'	 => 'page',
		'keyword'	 => 'Taiwan Election and Democratization Study',
	),
	'taiwan_ukraine_relations.html' => array(
		'include'	 => 'src/taiwan_ukraine_relations.php',
		'name'	 => 'page',
		'keyword'	 => 'Taiwan–Ukraine relations',
	),
	'talent_is_evenly_distributed_worldwide_but_opportunity_is_not.html' => array(
		'include'	 => 'src/talent_is_evenly_distributed_worldwide_but_opportunity_is_not.php',
		'name'	 => 'page',
		'keyword'	 => 'Talent is evenly distributed worldwide but opportunity is not',
	),
	'tanzania.html' => array(
		'include'	 => 'src/tanzania.php',
		'name'	 => 'page',
		'keyword'	 => 'Tanzania',
	),
	'tax_on_gambling.html' => array(
		'include'	 => 'src/tax_on_gambling.php',
		'name'	 => 'page',
		'keyword'	 => 'Tax on gambling',
	),
	'tax_renaissance.html' => array(
		'include'	 => 'src/tax_renaissance.php',
		'name'	 => 'page',
		'keyword'	 => 'Tax Renaissance',
	),
	'taxation_of_labor_vs_taxation_of_capital.html' => array(
		'include'	 => 'src/taxation_of_labor_vs_taxation_of_capital.php',
		'name'	 => 'page',
		'keyword'	 => 'Taxation of labor vs taxation of capital ',
	),
	'taxes.html' => array(
		'name'	 => 'page',
		'include'	 => 'src/taxes.php',
	),
	'taxes_in_taiwan.html' => array(
		'include'	 => 'src/taxes_in_taiwan.php',
		'name'	 => 'page',
		'keyword'	 => 'Taxes in Taiwan',
	),
	'teaching_democracy.html' => array(
		'include'	 => 'src/teaching_democracy.php',
		'name'	 => 'page',
		'keyword'	 => 'Teaching democracy',
	),
	'teamwork.html' => array(
		'include'	 => 'src/teamwork.php',
		'name'	 => 'page',
		'keyword'	 => 'Teamwork',
	),
	'tech_policy_press.html' => array(
		'include'	 => 'src/tech_policy_press.php',
		'name'	 => 'page',
		'keyword'	 => 'Tech Policy Press',
	),
	'technology.html' => array(
		'include'	 => 'src/technology.php',
		'name'	 => 'page',
		'keyword'	 => 'Technology',
	),
	'technology_and_democracy.html' => array(
		'include'	 => 'src/technology_and_democracy.php',
		'name'	 => 'page',
		'keyword'	 => 'technology and democracy',
	),
	'ted_talk_the_art_of_misdirection.html' => array(
		'include'	 => 'src/ted_talk_the_art_of_misdirection.php',
		'name'	 => 'page',
		'keyword'	 => 'TED Talk: The art of misdirection',
	),
	'ted_talks.html' => array(
		'include'	 => 'src/ted_talks.php',
		'name'	 => 'page',
		'keyword'	 => 'TED talks',
	),
	'texas.html' => array(
		'include'	 => 'src/texas.php',
		'name'	 => 'page',
		'keyword'	 => 'Texas',
	),
	'the_american_journalist_under_attack_media_trust_and_democracy.html' => array(
		'include'	 => 'src/the_american_journalist_under_attack_media_trust_and_democracy.php',
		'name'	 => 'page',
		'keyword'	 => 'The American Journalist Under Attack: Media, Trust, and Democracy',
	),
	'the_art_of_thinking_clearly.html' => array(
		'include'	 => 'src/the_art_of_thinking_clearly.php',
		'name'	 => 'page',
		'keyword'	 => 'The Art of Thinking Clearly',
	),
	'the_big_truth_upholding_democracy_in_the_age_of_the_big_lie.html' => array(
		'include'	 => 'src/the_big_truth_upholding_democracy_in_the_age_of_the_big_lie.php',
		'name'	 => 'page',
		'keyword'	 => 'The Big Truth: Upholding Democracy in the Age of “The Big Lie”',
	),
	'the_children_s_peace_movement_columbia.html' => array(
		'include'	 => 'src/the_children_s_peace_movement_columbia.php',
		'name'	 => 'page',
		'keyword'	 => 'The Children\'s Peace Movement (Columbia)',
	),
	'the_cult_of_trump_a_leading_cult_expert_explains_how_the_president_uses_mind_control.html' => array(
		'include'	 => 'src/the_cult_of_trump_a_leading_cult_expert_explains_how_the_president_uses_mind_control.php',
		'name'	 => 'page',
		'keyword'	 => 'The Cult of Trump: A Leading Cult Expert Explains How the President Uses Mind Control',
	),
	'the_despot_s_apprentice_donald_trump_s_attack_on_democracy.html' => array(
		'include'	 => 'src/the_despot_s_apprentice_donald_trump_s_attack_on_democracy.php',
		'name'	 => 'page',
	),
	'the_difference_between_the_impossible_and_the_possible_is_merely_a_measure_of_man_s_determination.html' => array(
		'include'	 => 'src/the_difference_between_the_impossible_and_the_possible_is_merely_a_measure_of_man_s_determination.php',
		'name'	 => 'page',
		'keyword'	 => 'The difference between the impossible and the possible is merely a measure of man\'s determination',
	),
	'the_end_of_political_parties.html' => array(
		'include'	 => 'src/the_end_of_political_parties.php',
		'name'	 => 'page',
		'keyword'	 => 'The end of political parties',
	),
	'the_final_minutes_of_president_obama_s_farewell_address_yes_we_can.html' => array(
		'include'	 => 'src/the_final_minutes_of_president_obama_s_farewell_address_yes_we_can.php',
		'name'	 => 'page',
		'keyword'	 => 'The Final Minutes of President Obama\'s Farewell Address: Yes, we can.',
	),
	'the_four_agreements.html' => array(
		'include'	 => 'src/the_four_agreements.php',
		'name'	 => 'page',
		'keyword'	 => 'The Four Agreements',
	),
	'the_great_dictator.html' => array(
		'include'	 => 'src/the_great_dictator.php',
		'name'	 => 'page',
		'keyword'	 => 'The Great Dictator',
	),
	'the_hidden_dimension_in_democracy.html' => array(
		'include'	 => 'src/the_hidden_dimension_in_democracy.php',
		'name'	 => 'page',
		'keyword'	 => 'The Hidden Dimension in Democracy',
	),
	'the_i_dont_know_revolution.html' => array(
		'include'	 => 'src/the_i_dont_know_revolution.php',
		'name'	 => 'page',
	),
	'the_monk_and_the_gun.html' => array(
		'include'	 => 'src/the_monk_and_the_gun.php',
		'name'	 => 'page',
		'keyword'	 => 'The Monk and the Gun',
	),
	'the_power_of_example_improving_our_democracies.html' => array(
		'include'	 => 'src/the_power_of_example_improving_our_democracies.php',
		'name'	 => 'page',
		'keyword'	 => ' The Power of Example: Improving Our Democracies',
	),
	'the_remains_of_the_day.html' => array(
		'include'	 => 'src/the_remains_of_the_day.php',
		'name'	 => 'page',
		'keyword'	 => 'The Remains of the Day',
	),
	'the_righteous_mind.html' => array(
		'include'	 => 'src/the_righteous_mind.php',
		'name'	 => 'page',
		'keyword'	 => 'The Righteous Mind',
	),
	'the_sixth.html' => array(
		'include'	 => 'src/the_sixth.php',
		'name'	 => 'page',
		'keyword'	 => 'The Sixth',
	),
	'the_social_cost_of_donald_trump_political_carreer.html' => array(
		'include'	 => 'src/the_social_cost_of_donald_trump_political_carreer.php',
		'name'	 => 'page',
		'keyword'	 => 'The social cost of Donald Trump political carreer',
	),
	'the_soloist_and_the_choir.html' => array(
		'include'	 => 'src/the_soloist_and_the_choir.php',
		'name'	 => 'page',
		'keyword'	 => 'the Soloist and the Choir',
	),
	'the_two_sides_of_democratic_dysfunction.html' => array(
		'include'	 => 'src/the_two_sides_of_democratic_dysfunction.php',
		'name'	 => 'page',
		'keyword'	 => 'The Two Sides of Democratic Dysfunction',
	),
	'the_watermelons_soldiers_spying_for_pro_democracy_rebels_bbc_world_service_documentary.html' => array(
		'include'	 => 'src/the_watermelons_soldiers_spying_for_pro_democracy_rebels_bbc_world_service_documentary.php',
		'name'	 => 'page',
		'keyword'	 => 'The Watermelons: Soldiers spying for pro-democracy rebels - BBC World Service Documentary',
	),
	'think_like_a_voter.html' => array(
		'include'	 => 'src/think_like_a_voter.php',
		'name'	 => 'page',
		'keyword'	 => 'Think Like a Voter',
	),
	'third_level_of_democracy.html' => array(
		'include'	 => 'src/third_level_of_democracy.php',
		'name'	 => 'page',
		'keyword'	 => 'tertiary features of democracy',
	),
	'thomas_paine.html' => array(
		'include'	 => 'src/thomas_paine.php',
		'name'	 => 'page',
		'keyword'	 => 'Thomas Paine',
	),
	'thomas_piketty.html' => array(
		'include'	 => 'src/thomas_piketty.php',
		'name'	 => 'page',
		'keyword'	 => 'Thomas Piketty',
	),
	'transnational_authoritarianism.html' => array(
		'include'	 => 'src/transnational_authoritarianism.php',
		'name'	 => 'page',
		'keyword'	 => 'transnational authoritarianism',
	),
	'transparency_international.html' => array(
		'include'	 => 'src/transparency_international.php',
		'name'	 => 'page',
		'keyword'	 => 'Transparency International',
	),
	'truemedia_org.html' => array(
		'include'	 => 'src/truemedia_org.php',
		'name'	 => 'page',
		'keyword'	 => ' TrueMedia.org',
	),
	'trump_v_united_states.html' => array(
		'include'	 => 'src/trump_v_united_states.php',
		'name'	 => 'page',
		'keyword'	 => 'Trump v. United States',
	),
	'trump_v_vance.html' => array(
		'include'	 => 'src/trump_v_vance.php',
		'name'	 => 'page',
		'keyword'	 => 'Trump v. Vance',
	),
	'truth_and_reality_with_chinese_characteristics.html' => array(
		'include'	 => 'src/truth_and_reality_with_chinese_characteristics.php',
		'name'	 => 'page',
	),
	'tsai_ing_wen.html' => array(
		'include'	 => 'src/tsai_ing_wen.php',
		'name'	 => 'page',
		'keyword'	 => 'Tsai Ing-wen',
	),
	'tunisia.html' => array(
		'include'	 => 'src/tunisia.php',
		'name'	 => 'page',
		'keyword'	 => 'Tunisia',
	),
	'turkey.html' => array(
		'name'	 => 'page',
		'include'	 => 'src/turkey.php',
	),
	'tweed_symptom_uncontested_elections.html' => array(
		'include'	 => 'src/tweed_symptom_uncontested_elections.php',
		'name'	 => 'page',
		'keyword'	 => 'Tweed Symptom: Uncontested Elections',
	),
	'tweed_syndrome.html' => array(
		'include'	 => 'src/tweed_syndrome.php',
		'name'	 => 'page',
		'keyword'	 => 'Tweed Syndrome',
	),
	'tweed_syndrome_primaries.html' => array(
		'include'	 => 'src/tweed_syndrome_primaries.php',
		'name'	 => 'page',
		'keyword'	 => 'Tweed syndrome primaries',
	),
	'tweed_syndrome_systemic_corruption.html' => array(
		'include'	 => 'src/tweed_syndrome_systemic_corruption.php',
		'name'	 => 'page',
		'keyword'	 => 'Tweed Syndrome: Systemic Corruption',
	),
	'tweedism.html' => array(
		'include'	 => 'src/tweedism.php',
		'name'	 => 'page',
		'keyword'	 => 'Tweedism',
	),
	'twilight_of_democracy.html' => array(
		'include'	 => 'src/twilight_of_democracy.php',
		'name'	 => 'page',
		'keyword'	 => 'Twilight of Democracy',
	),
	'types_of_democracy.html' => array(
		'include'	 => 'src/types_of_democracy.php',
		'name'	 => 'page',
		'keyword'	 => 'types of democracy',
	),
	'ukraine.html' => array(
		'include'	 => 'src/ukraine.php',
		'name'	 => 'page',
		'keyword'	 => 'Ukraine',
	),
	'unicef.html' => array(
		'include'	 => 'src/unicef.php',
		'name'	 => 'page',
		'keyword'	 => 'UNICEF',
	),
	'united_arab_emirates.html' => array(
		'include'	 => 'src/united_arab_emirates.php',
		'name'	 => 'page',
		'keyword'	 => 'United Arab Emirates',
	),
	'united_auto_workers.html' => array(
		'include'	 => 'src/united_auto_workers.php',
		'name'	 => 'page',
		'keyword'	 => 'United Auto Workers',
	),
	'united_for_peace_and_justice.html' => array(
		'include'	 => 'src/united_for_peace_and_justice.php',
		'name'	 => 'page',
		'keyword'	 => 'United for Peace and Justice',
	),
	'united_nations.html' => array(
		'name'	 => 'page',
		'include'	 => 'src/united_nations.php',
	),
	'united_nations_high_commissioner_for_refugees.html' => array(
		'include'	 => 'src/united_nations_high_commissioner_for_refugees.php',
		'name'	 => 'page',
		'keyword'	 => 'United Nations High Commissioner for Refugees',
	),
	'united_nations_human_rights_office.html' => array(
		'include'	 => 'src/united_nations_human_rights_office.php',
		'name'	 => 'page',
		'keyword'	 => 'United Nations Human Rights Office',
	),
	'united_nations_office_for_the_coordination_of_humanitarian_affairs.html' => array(
		'include'	 => 'src/united_nations_office_for_the_coordination_of_humanitarian_affairs.php',
		'name'	 => 'page',
		'keyword'	 => 'United Nations Office for the Coordination of Humanitarian Affairs',
	),
	'united_nations_regional_information_centre.html' => array(
		'include'	 => 'src/united_nations_regional_information_centre.php',
		'name'	 => 'page',
		'keyword'	 => 'United Nations Regional Information Centre',
	),
	'united_nations_volunteers.html' => array(
		'include'	 => 'src/united_nations_volunteers.php',
		'name'	 => 'page',
		'keyword'	 => 'United Nations Volunteers',
	),
	'united_states.html' => array(
		'include'	 => 'src/united_states.php',
		'name'	 => 'page',
		'keyword'	 => 'United States',
	),
	'universal_basic_income.html' => array(
		'include'	 => 'src/universal_basic_income.php',
		'name'	 => 'page',
		'keyword'	 => 'Universal basic income',
	),
	'unsung_heroes_of_democracy_award.html' => array(
		'include'	 => 'src/unsung_heroes_of_democracy_award.php',
		'name'	 => 'page',
		'keyword'	 => 'Unsung Heroes of Democracy Award',
	),
	'us_states.html' => array(
		'include'	 => 'src/us_states.php',
		'name'	 => 'page',
		'keyword'	 => 'US states',
	),
	'us_taiwan_relations_in_a_new_era.html' => array(
		'include'	 => 'src/us_taiwan_relations_in_a_new_era.php',
		'name'	 => 'page',
		'keyword'	 => 'U.S.-Taiwan Relations in a New Era',
	),
	'usafacts.html' => array(
		'include'	 => 'src/usafacts.php',
		'name'	 => 'page',
		'keyword'	 => 'USAFacts',
	),
	'v_dem_institute.html' => array(
		'include'	 => 'src/v_dem_institute.php',
		'name'	 => 'page',
		'keyword'	 => 'V-Dem Institute',
	),
	'verified_voting_foundation.html' => array(
		'include'	 => 'src/verified_voting_foundation.php',
		'name'	 => 'page',
		'keyword'	 => 'Verified Voting Foundation',
	),
	'vietnam.html' => array(
		'include'	 => 'src/vietnam.php',
		'name'	 => 'page',
		'keyword'	 => 'Vietnam',
	),
	'vision_of_humanity.html' => array(
		'include'	 => 'src/vision_of_humanity.php',
		'name'	 => 'page',
		'keyword'	 => 'Vision of Humanity',
	),
	'volodymyr_zelenskyy.html' => array(
		'include'	 => 'src/volodymyr_zelenskyy.php',
		'name'	 => 'page',
		'keyword'	 => 'Volodymyr Zelensky',
	),
	'vote_smart.html' => array(
		'include'	 => 'src/vote_smart.php',
		'name'	 => 'page',
		'keyword'	 => 'Vote Smart',
	),
	'voter_suppression.html' => array(
		'include'	 => 'src/voter_suppression.php',
		'name'	 => 'page',
		'keyword'	 => 'Voter suppression',
	),
	'voting_methods.html' => array(
		'include'	 => 'src/voting_methods.php',
		'name'	 => 'page',
		'keyword'	 => 'Election methods',
	),
	'we_are_the_leaders_we_have_been_looking_for.html' => array(
		'include'	 => 'src/we_are_the_leaders_we_have_been_looking_for.php',
		'name'	 => 'page',
		'keyword'	 => 'We Are the Leaders We Have Been Looking For',
	),
	'wealth.html' => array(
		'include'	 => 'src/wealth.php',
		'name'	 => 'page',
		'keyword'	 => 'Wealth',
	),
	'wealth_inequality_in_america.html' => array(
		'include'	 => 'src/wealth_inequality_in_america.php',
		'name'	 => 'page',
		'keyword'	 => 'Wealth Inequality in America',
	),
	'weaponization_of_the_justice_department_by_donald_trump.html' => array(
		'include'	 => 'src/weaponization_of_the_justice_department_by_donald_trump.php',
		'name'	 => 'page',
		'keyword'	 => 'Weaponization of the Justice Department by Donald Trump',
	),
	'web.html' => array(
		'include'	 => 'src/web.php',
		'name'	 => 'page',
		'keyword'	 => 'Web',
	),
	'wedge_issues.html' => array(
		'include'	 => 'src/wedge_issues.php',
		'name'	 => 'page',
		'keyword'	 => 'Wedge issues',
	),
	'what_democracy_can_learn_from_the_aviation_industry.html' => array(
		'include'	 => 'src/what_democracy_can_learn_from_the_aviation_industry.php',
		'name'	 => 'page',
		'keyword'	 => 'What Democracy can learn from the aviation industry',
	),
	'what_is_populism.html' => array(
		'include'	 => 'src/what_is_populism.php',
		'name'	 => 'page',
		'keyword'	 => 'What is populism?',
	),
	'where_law_ends.html' => array(
		'include'	 => 'src/where_law_ends.php',
		'name'	 => 'page',
		'keyword'	 => 'Where Law Ends',
	),
	'whistleblowers.html' => array(
		'include'	 => 'src/whistleblowers.php',
		'name'	 => 'page',
		'keyword'	 => 'Whistleblower',
	),
	'white_poverty_how_exposing_myths_about_race_and_class_can_reconstruct_american_democracy.html' => array(
		'include'	 => 'src/white_poverty_how_exposing_myths_about_race_and_class_can_reconstruct_american_democracy.php',
		'name'	 => 'page',
		'keyword'	 => 'White Poverty How Exposing Myths About Race and Class Can Reconstruct American Democracy',
	),
	'why_are_trumpism_and_the_maga_movement_so_successful.html' => array(
		'include'	 => 'src/why_are_trumpism_and_the_maga_movement_so_successful.php',
		'name'	 => 'page',
		'keyword'	 => 'Why are Trumpism and the MAGA movement so successful',
	),
	'william_barber_ii.html' => array(
		'include'	 => 'src/william_barber_ii.php',
		'name'	 => 'page',
		'keyword'	 => 'William Barber II',
	),
	'william_m_tweed.html' => array(
		'include'	 => 'src/william_m_tweed.php',
		'name'	 => 'page',
		'keyword'	 => 'William M. Tweed',
	),
	'wip.html' => array(
		'include'	 => 'src/wip.php',
		'name'	 => 'page',
		'keyword'	 => 'WIP',
	),
	'women_s_rights.html' => array(
		'include'	 => 'src/women_s_rights.php',
		'name'	 => 'page',
		'keyword'	 => 'Women\'s Rights',
	),
	'world.html' => array(
		'include'	 => 'src/world.php',
		'name'	 => 'page',
		'keyword'	 => 'world',
	),
	'world_central_kitchen.html' => array(
		'include'	 => 'src/world_central_kitchen.php',
		'name'	 => 'page',
		'keyword'	 => 'World Central Kitchen',
	),
	'world_food_programme.html' => array(
		'include'	 => 'src/world_food_programme.php',
		'name'	 => 'page',
		'keyword'	 => 'World Food Program',
	),
	'world_future_council.html' => array(
		'include'	 => 'src/world_future_council.php',
		'name'	 => 'page',
		'keyword'	 => 'World Future Council',
	),
	'world_happiness_report.html' => array(
		'include'	 => 'src/world_happiness_report.php',
		'name'	 => 'page',
		'keyword'	 => 'World Happiness Report',
	),
	'world_inequality_database.html' => array(
		'include'	 => 'src/world_inequality_database.php',
		'name'	 => 'page',
		'keyword'	 => 'World Inequality Database',
	),
	'world_inequality_lab.html' => array(
		'include'	 => 'src/world_inequality_lab.php',
		'name'	 => 'page',
		'keyword'	 => 'World Inequality Lab',
	),
	'world_s_children_s_prize_for_the_rights_of_the_child.html' => array(
		'include'	 => 'src/world_s_children_s_prize_for_the_rights_of_the_child.php',
		'name'	 => 'page',
		'keyword'	 => 'World\'s Children\'s Prize for the Rights of the Child',
	),
	'yuval_noah_harari.html' => array(
		'include'	 => 'src/yuval_noah_harari.php',
		'name'	 => 'page',
		'keyword'	 => 'Yuval Noah Harari',
	),
);
