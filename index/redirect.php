<?php


$redirect = array(
	'/belief.html'	 => 'beliefs.html',
	'/copyright.html'	 => 'project/copyright.html',
	'/democracy_under_attack.html'	 => 'external_threats.html',
	'/election_methods.html'	 => 'voting_methods.html',
	'/integrity.html'	 => 'election_integrity.html',
	'/orwellian_doublespeak_in_today_s_political_discourse.html'	 => 'orwellian_doublespeak.html',
	'/participate.html'	 => 'project/participate.html',
	'/permission_structure.html'	 => 'permission_structures.html',
	'/president_biden_last_interview_with_lawrence_o_donnell.html'	 => 'president_biden_interview_with_lawrence_o_donnell_january_2025.html',
	'/primary_features_of_democracy.html'	 => 'first_level_of_democracy.html',
	'/quaternary_features_of_democracy.html'	 => 'fourth_level_of_democracy.html',
	'/rights.html'	 => 'economic_social_and_cultural_rights.html',
	'/secondary_features_of_democracy.html'	 => 'second_level_of_democracy.html',
	'/strongmen_from_mussolini_to_the_present.html'	 => 'strongmen_mussolini_to_the_present.html',
	'/tertiary_features_of_democracy.html'	 => 'third_level_of_democracy.html',
	'/updates.html'	 => 'project/updates.html',
	'/volodymyr_zelensky.html'	 => 'volodymyr_zelenskyy.html',
);
