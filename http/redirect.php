<?php
chdir('..');
include_once 'include/init.php';
include_once 'index/redirect.php';

$request = parse_url($_SERVER['REQUEST_URI']);
$url = $request['path'];
$domain = parse_url($_SERVER['SERVER_NAME']);

$page = new Page();

$div_redirect = new ContentSection();

if (isset($redirect[$url])) {
	$page->h1('The requested page has moved');
	$div_redirect->content = <<<HTML
		<h3>Redirect</h3>
		<p>Please go to: <a href="/{$redirect[$url]}">{$domain['path']}/{$redirect[$url]}</a>.</p>
		HTML;
}
else {
	$page->h1('Page not found');
	$div_redirect->content = <<<HTML
		<h3>Sorry</h3>
		<p>The requested page cannot be found.</p>
		HTML;
}

$page->body($div_redirect);
$out = $page->print('body');
echo $out;
