<?php
$page = new Page();
$page->h1("The American Journalist Under Attack: Media, Trust, and Democracy");
$page->stars(1);

//$page->snp("description", "");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;



$div_The_American_Journalist_Under_Attack_Media_Trust_and_Democracy = new WebsiteContentSection();
$div_The_American_Journalist_Under_Attack_Media_Trust_and_Democracy->setTitleText("The American Journalist Under Attack: Media, Trust, and Democracy");
$div_The_American_Journalist_Under_Attack_Media_Trust_and_Democracy->setTitleLink("https://www.theamericanjournalist.org/post/media-trust-democracy");
$div_The_American_Journalist_Under_Attack_Media_Trust_and_Democracy->content = <<<HTML
	<p>Over the previous five decades—beginning in the 1970s with the foundational work by Johnstone, Slawski and Bowman—our research
	on the values, ethics and work of American journalists has provided an extensive portrait of the profession.
	The broad sweep of that work suggests this:
	While driven by a professed sense of altruism, journalism may have been so absorbed in its own conception of an "interpretive-watchdog" role
	that the overall health of the democracy was taken for granted.
	At least, that appears to be the interpretation emerging from some scholars and even critics within the profession itself.</p>

	<p>The contours of the critique of journalists as being somewhat apathetic about the fragility of democracy are not new.
	Herbert Gans (2009), a pioneering media scholar, wrote that journalists have done "less on behalf of democracy than they believe"
	as they “operate with their own theory of democracy” that is affected by what is "commercially feasible."</p>

	<p>The criticism of the supposed journalistic apathy, however, clearly sharpened during the presidential election of 2016.
	Journalists, the critics claimed, engaged in what might be called a de facto apathy
	resulting from a narrow pursuit of objectivity, scandal, and "false equivalence" in election coverage.</p>
	HTML;




$div_Key_Findings_From_The_2022_American_Journalist_Study = new WebsiteContentSection();
$div_Key_Findings_From_The_2022_American_Journalist_Study->setTitleText("Key Findings From The 2022 American Journalist Study");
$div_Key_Findings_From_The_2022_American_Journalist_Study->setTitleLink("https://www.theamericanjournalist.org/post/american-journalist-findings");
$div_Key_Findings_From_The_2022_American_Journalist_Study->content = <<<HTML
	<p>This survey continues the series of major national studies of U.S. journalists begun in 1971 by sociologist John Johnstone
	and continued in 1982, 1992, 2002, and 2013 by David Weaver, Cleve Wilhoit and their colleagues at Indiana University.
	Few studies of a profession as important as journalism can claim a half-century’s analytical perspective on the work, professional attitudes, and ethics
	from large samples of the people working in it.
	That’s what this study does with its contribution of important decennial measures of the pulse of American journalism.</p>

	<p>This present study, based on an online survey with 1,600 U.S. journalists conducted in early 2022,
	updates these findings and adds new ones concerning democracy and threats to journalism.
	Overall, the findings suggest that the past decade has had significant effects on U.S. journalists, some more negative than positive.</p>
	HTML;



$page->parent('list_of_research_and_reports.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_The_American_Journalist_Under_Attack_Media_Trust_and_Democracy);
$page->body($div_Key_Findings_From_The_2022_American_Journalist_Study);
