<?php
$page = new VideoPage();
$page->h1("Ken Burns: We need to reach out to Trump voters and not 'other' them");
$page->tags("Video: Interview", "Ken Burns", "MAGA", "Society", "Information: Discourse");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;



$div_youtube_Ken_Burns_We_need_to_reach_out_to_Trump_voters_and_not_other_them = new YoutubeContentSection();
$div_youtube_Ken_Burns_We_need_to_reach_out_to_Trump_voters_and_not_other_them->setTitleText("Ken Burns: We need to reach out to Trump voters and not &#39;other&#39; them");
$div_youtube_Ken_Burns_We_need_to_reach_out_to_Trump_voters_and_not_other_them->setTitleLink("https://www.youtube.com/watch?v=YzIp1CZn9kk");
$div_youtube_Ken_Burns_We_need_to_reach_out_to_Trump_voters_and_not_other_them->content = <<<HTML
	<p>Emmy Award-winning filmmaker Ken Burns joins Morning Joe to discuss the themes from his commencement speech at Brandeis University.</p>
	HTML;



$page->parent('list_of_videos.html');
$page->template("stub");
$page->body($div_introduction);

$page->body($div_youtube_Ken_Burns_We_need_to_reach_out_to_Trump_voters_and_not_other_them);
