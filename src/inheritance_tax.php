<?php
$page = new Page();
$page->h1("Inheritance tax");
$page->tags("Taxes", "Tax");
$page->keywords("Inheritance tax");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Inheritance_tax = new WikipediaContentSection();
$div_wikipedia_Inheritance_tax->setTitleText("Inheritance tax");
$div_wikipedia_Inheritance_tax->setTitleLink("https://en.wikipedia.org/wiki/Inheritance_tax");
$div_wikipedia_Inheritance_tax->content = <<<HTML
	<p>International tax law distinguishes between an estate tax and an inheritance tax. An inheritance tax is a tax paid by a person who inherits money or property of a person who has died, whereas an estate tax is a levy on the estate (money and property) of a person who has died. However, this distinction is not always observed; for example, the UK's "inheritance tax" is a tax on the assets of the deceased, and strictly speaking is therefore an estate tax.</p>
	HTML;


$page->parent('list_of_taxes.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Inheritance tax");
$page->body($div_wikipedia_Inheritance_tax);
