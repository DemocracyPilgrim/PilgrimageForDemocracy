<?php
$page = new VideoPage();
$page->h1("President Biden Farewell Address");
$page->viewport_background("");
$page->keywords("President Biden Farewell Address");
$page->stars(0);
$page->tags("Video: Speech", "Joe Biden", "POTUS", "Oligarchy", "Danger");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;




$div_youtube_President_Biden_Farewell_Address_to_the_Nation = new YoutubeContentSection();
$div_youtube_President_Biden_Farewell_Address_to_the_Nation->setTitleText("President Biden Farewell Address to the Nation");
$div_youtube_President_Biden_Farewell_Address_to_the_Nation->setTitleLink("https://www.youtube.com/watch?v=-pGZRQ00kN0");
$div_youtube_President_Biden_Farewell_Address_to_the_Nation->content = <<<HTML
	<p>
	</p>
	HTML;



$page->parent('list_of_videos.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("President Biden Farewell Address");
$page->body($div_youtube_President_Biden_Farewell_Address_to_the_Nation);
