<?php
$page = new Page();
$page->h1("8: Humanity");
$page->tags("Topic portal");
$page->keywords("Humanity", "humanity");
$page->stars(1);
$page->viewport_background("/free/humanity.png");

$page->snp("description", "Humanity: compassion, solidarity and mutual help");
$page->snp("image",       "/free/humanity.1200-630.png");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>This is the directory of articles related to the ${'eighth level of democracy'}.</p>
	HTML;


$list_Humanity = ListOfPeoplePages::WithTags("Humanity");
$print_list_Humanity = $list_Humanity->print();

$div_list_Humanity = new ContentSection();
$div_list_Humanity->content = <<<HTML
	<h3>Related content</h3>

	$print_list_Humanity
	HTML;



$div_wikipedia_Humanity_virtue = new WikipediaContentSection();
$div_wikipedia_Humanity_virtue->setTitleText("Humanity virtue");
$div_wikipedia_Humanity_virtue->setTitleLink("https://en.wikipedia.org/wiki/Humanity_(virtue)");
$div_wikipedia_Humanity_virtue->content = <<<HTML
	<p>Humanity is a virtue linked with altruistic ethics derived from the human condition.
	It signifies human love and compassion towards each other.
	Humanity differs from mere justice in that there is a level of altruism towards individuals included in humanity more so than in the fairness found in justice.
	That is, humanity, and the acts of love, altruism, and social intelligence are typically individual strengths
	while fairness is generally expanded to all.
	Humanity is one of six virtues that are consistent across all cultures.</p>
	HTML;

$div_wikipedia_Humanitarianism = new WikipediaContentSection();
$div_wikipedia_Humanitarianism->setTitleText("Humanitarianism");
$div_wikipedia_Humanitarianism->setTitleLink("https://en.wikipedia.org/wiki/Humanitarianism");
$div_wikipedia_Humanitarianism->content = <<<HTML
	<p>Humanitarianism is an ideology centered on the value of human life,
	whereby humans practice benevolent treatment and provide assistance to other humans to reduce suffering
	and improve the conditions of humanity for moral, altruistic, and emotional reasons.</p>
	HTML;


$page->parent('eighth_level_of_democracy.html');
$page->previous("international.html");
$page->next("wip.html");

$page->body($div_introduction);

$page->body($div_list_Humanity);

$page->body($div_wikipedia_Humanity_virtue);
$page->body($div_wikipedia_Humanitarianism);
