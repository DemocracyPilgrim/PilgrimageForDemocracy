<?php
$page = new Page();
$page->h1('Political repression');
$page->keywords('Political repression', 'political repression');
$page->stars(0);

//$page->snp('description', '');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Political_repression = new WikipediaContentSection();
$div_wikipedia_Political_repression->setTitleText('Political repression');
$div_wikipedia_Political_repression->setTitleLink('https://en.wikipedia.org/wiki/Political_repression');
$div_wikipedia_Political_repression->content = <<<HTML
	<p>Political repression is the act of a state entity controlling a citizenry by force for political reasons,
	particularly for the purpose of restricting or preventing the citizenry's ability to take part in the political life of a society,
	thereby reducing their standing among their fellow citizens.
	Repression tactics target the citizenry who are most likely to challenge the political ideology of the state
	in order for the government to remain in control.</p>
	HTML;


$page->parent('authoritarianism.html');
$page->template("stub");
$page->body($div_introduction);

$page->body('covert_political_repression.html');
$page->body('insidious_political_repression.html');


$page->body($div_wikipedia_Political_repression);
