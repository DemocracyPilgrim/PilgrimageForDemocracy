<?php
$page = new Page();
$page->h1("Marc Elias");
$page->alpha_sort("Elias, Marc");
$page->tags("Person", "Lawyer", "USA", "Elections", "Democracy Docket");
$page->keywords("Marc Elias");
$page->stars(1);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

$r1 = $page->ref("https://www.youtube.com/watch?v=PC9hz83TxpM", "Democracy Watch episode 181 (3'00)");
// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Marc Elias is an $American $lawyer.
	He is the founder of ${'Democracy Docket'}.
	He and his firm litigate court cases regarding ${'election integrity'}, voter $disfranchisement, etc.</p>

	<p>Elias appears frequently on Democracy Watch with ${'Brian Tyler Cohen'}.</p>

	<p>In 2024, Marc Elias and his firm were so good at fighting off frivolous lawsuits aiming for voter suppression,
	that a Fox New anchor suggested the Republicans ought to pay Elias 500 million dollars for him to switch side.
	Elias declined the offer.$r1</p>
	HTML;

$div_wikipedia_Marc_Elias = new WikipediaContentSection();
$div_wikipedia_Marc_Elias->setTitleText("Marc Elias");
$div_wikipedia_Marc_Elias->setTitleLink("https://en.wikipedia.org/wiki/Marc_Elias");
$div_wikipedia_Marc_Elias->content = <<<HTML
	<p>Marc Erik Elias (born February 1, 1969) is an American Democratic Party elections lawyer.
	In 2020, he founded Democracy Docket, a website focused on voting rights and election litigation in the United States,
	and he left his position as a partner at Perkins Coie to start the Elias Law Group in 2021.</p>

	<p>Elias served as general counsel for the Hillary Clinton 2016 presidential campaign and John Kerry 2004 presidential campaign.
	In 2020 and 2021, on behalf of the Biden campaign and the Democratic National Committee,
	Elias oversaw the state-by-state response to lawsuits filed by the Trump campaign contesting the 2020 presidential election results.
	Of the 64 cases, he won all but one minor case, which was later overturned in his favor.
	Elias was hired by the Kamala Harris 2024 presidential campaign to focus on potential recounts and post-election litigation.</p>
	HTML;


$page->parent('list_of_people.html');
$page->template("stub");
$page->body($div_introduction);


$page->related_tag("Marc Elias");
$page->body($div_wikipedia_Marc_Elias);
