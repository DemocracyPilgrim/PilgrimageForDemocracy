<?php
$page = new Page();
$page->h1('Democracy and social justice in the world.');
$page->stars(1);
$page->keywords('world', 'countries', 'country', 'global', 'nation', 'Country', 'International Organisation');
$page->tags("International");
$page->viewport_background('/free/world.png');

$page->snp("description", "Democracy and Social Justice throughout the world.");
$page->snp('image', "/free/world.1200-630.png");

$page->preview( <<<HTML
	<p>Although we are only getting started, we aim to progressively extend our coverage of countries around the world.</p>
	HTML );



$list_International_Organisation = ListOfPages::WithTags("International Organisation");
$print_list_International_Organisation= $list_International_Organisation->print();

$div_International_organisations = new ContentSection();
$div_International_organisations->content = <<<HTML
	<h3>International organisations</h3>
	$print_list_International_Organisation
	HTML;



$list_Countries = ListOfPages::WithTags("Country");
$print_list_Countries = $list_Countries->print();

$div_countries = new ContentSection();
$div_countries->content = <<<HTML
	<h3>Countries</h3>
	$print_list_Countries
	HTML;


$page->parent('lists.html');


$page->body($div_International_organisations);
$page->body($div_countries);
