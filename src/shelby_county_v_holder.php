<?php
$page = new Page();
$page->h1("Shelby County v. Holder (2013)");
$page->tags("Institutions: Judiciary", "SCOTUS Landmark Decision", "Elections", "USA", "Electoral System");
$page->keywords("Shelby County v. Holder");
$page->stars(1);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>"Shelby County v. Holder" is a 2013 landmark decision by the ${'Supreme Court of the United States'}.</p>

	<p>This decision struck down a key provision of the Voting Rights Act of 1965,
	which required states with a history of racial discrimination to obtain federal approval before making changes to their voting laws.</p>

	<p>This ruling has made it easier for states to enact discriminatory voting laws, potentially disenfranchising minority voters and undermining the right to vote.</p>

	<p>Supporters of the decision argue that it was based on outdated data
	and that the Voting Rights Act's preclearance requirement was no longer necessary.</p>

	<p>This decision, which struck down a key provision of the Voting Rights Act, continues to have a significant impact on voting rights.
	It has made it easier for states to implement voting restrictions, potentially disenfranchising minority voters.</p>
	HTML;

$div_wikipedia_Shelby_County_v_Holder = new WikipediaContentSection();
$div_wikipedia_Shelby_County_v_Holder->setTitleText("Shelby County v Holder");
$div_wikipedia_Shelby_County_v_Holder->setTitleLink("https://en.wikipedia.org/wiki/Shelby_County_v._Holder");
$div_wikipedia_Shelby_County_v_Holder->content = <<<HTML
	<p>Shelby County v. Holder, 570 U.S. 529 (2013), was a landmark decision of the Supreme Court of the United States
	regarding the constitutionality of two provisions of the Voting Rights Act of 1965: Section 5,
	which requires certain states and local governments to obtain federal preclearance before implementing any changes to their voting laws or practices;
	and subsection (b) of Section 4, which contains the coverage formula that determines which jurisdictions are subject to preclearance
	based on their histories of racial discrimination in voting.</p>

	<p>On June 25, 2013, the Court ruled by a 5 to 4 vote that Section 4(b) was unconstitutional because the coverage formula was based on data over 40 years old,
	making it no longer responsive to current needs and therefore an impermissible burden
	on the constitutional principles of federalism and equal sovereignty of the states.
	The Court did not strike down Section 5, but without Section 4(b), no jurisdiction will be subject to Section 5 preclearance
	unless Congress enacts a new coverage formula.</p>

	<p>The ruling has made it easier for state officials to engage in voter suppression.
	Research shows that preclearance led to increases in minority congressional representation and minority voter turnout.
	Five years after the ruling, nearly 1,000 U.S. polling places had closed, many of them in predominantly African-American counties.
	Research shows that changing and reducing voting locations can reduce voter turnout.
	There were also cuts to early voting, purges of voter rolls, and imposition of strict voter ID laws.
	A 2020 study found that jurisdictions that had previously been covered by preclearance substantially increased their voter registration purges after Shelby.
	Virtually all voting restrictions after the ruling were enacted by Republicans.</p>
	HTML;


$page->parent('supreme_court_of_the_united_states.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_wikipedia_Shelby_County_v_Holder);
