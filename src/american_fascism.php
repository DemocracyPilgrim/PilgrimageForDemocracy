<?php
$page = new Page();
$page->h1('American Fascism');
$page->tags("USA", "Donald Trump", "Fascism", "History of the United States");
$page->stars(0);

$page->snp('description', 'American fascism, from the 1930s to Trumpism.');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p>Fascism in America already has a long history, from the 1930's to today Trumpist MAGA movement...</p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Fascism in America already has a long history, from the 1930's to today Trumpist MAGA movement...</p>
	HTML;


$list = new ListOfPages();
$list->add('a_night_at_the_garden.html');
$list->add('donald_trump.html');
$list->add('duverger_usa_21_century.html');
$list->add('glenn_kirschner.html');
$list->add('prequel_an_american_fight_against_fascism.html');
$list->add('project_2025.html');
$list->add('the_despot_s_apprentice_donald_trump_s_attack_on_democracy.html');
$list->add('the_sixth.html');
$list->add('why_are_trumpism_and_the_maga_movement_so_successful.html');
$list->add('blowback_a_warning_to_save_democracy.html');
$print_list = $list->print();

$div_list = new ContentSection();
$div_list->content = <<<HTML
	<h3>Related content</h3>

	$print_list
	HTML;


$div_wikipedia_Fascism_in_North_America = new WikipediaContentSection();
$div_wikipedia_Fascism_in_North_America->setTitleText('Fascism in North America');
$div_wikipedia_Fascism_in_North_America->setTitleLink('https://en.wikipedia.org/wiki/Fascism_in_North_America');
$div_wikipedia_Fascism_in_North_America->content = <<<HTML
	<p>Fascism in the United States, from 1920 to the present days.</p>
	HTML;

$div_wikipedia_Radical_right_United_States = new WikipediaContentSection();
$div_wikipedia_Radical_right_United_States->setTitleText('Radical right United States');
$div_wikipedia_Radical_right_United_States->setTitleLink('https://en.wikipedia.org/wiki/Radical_right_(United_States)');
$div_wikipedia_Radical_right_United_States->content = <<<HTML
	<p>In United States politics, the radical right is a political preference that leans towards ultraconservatism,
	white supremacy, or other right-wing to far-right ideologies in a hierarchical structure paired
	with conspiratorial rhetoric alongside traditionalist and reactionary aspirations.</p>
	HTML;

$div_wikipedia_Trumpism = new WikipediaContentSection();
$div_wikipedia_Trumpism->setTitleText('Trumpism');
$div_wikipedia_Trumpism->setTitleLink('https://en.wikipedia.org/wiki/Trumpism');
$div_wikipedia_Trumpism->content = <<<HTML
	<p>Trumpism is the political ideologies, social emotions, style of governance, political movement,
	and set of mechanisms for autocratization and authoritarianism that are associated with 45th U.S. president Donald Trump and his political base.
	Certain characteristics within public relations and Trump's political base have exhibited symptoms of a cult of personality.</p>
	HTML;


$page->parent('fascism.html');
$page->template("stub");
$page->body($div_introduction);

$page->body('fascism.html');
$page->body('united_states.html');
$page->body($div_list);


$page->body($div_wikipedia_Fascism_in_North_America);
$page->body($div_wikipedia_Radical_right_United_States);
$page->body($div_wikipedia_Trumpism);
