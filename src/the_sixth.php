<?php
$page = new Page();
$page->h1("The Sixth");
$page->tags("USA", "Documentary");
$page->keywords("The Sixth");
$page->stars(0);

$page->snp("description", "Documentary about the January 6th attack on the U.S. Capitol.");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );

$r1 = $page->ref('https://www.imdb.com/title/tt29644446/', 'IMDB: The Sixth');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>The Sixth is a documentary by A24 films $r1, directed by Andrea Nix Fine and Sean Fine,
	about the January 6th attack on the U.S. Capitol,
	as seen through the eyes of six witnesses.</p>

	<p>US president ${'Joe Biden'}, and congressman ${'Jamie Raskin'}
	make appearances in the documentary.</p>
	HTML;


$div_truth = new ContentSection();
$div_truth->content = <<<HTML
	<h3>Truth</h3>

	<blockquote>
		Democracy needs a ground to stand on and that ground is the truth.<br>
		So, we have to tell the truth. If we can handle it.
		<br>- ${'Jamie Raskin'}, US congressman.
	</blockquote>

	<p>Factual truth, and the ability of the media to inform the citizens, but not manipulate, is the ${'fifth level of democracy'}.</p>
	HTML;


$div_a24films_The_Sixth = new WebsiteContentSection();
$div_a24films_The_Sixth->setTitleText("a24films The Sixth ");
$div_a24films_The_Sixth->setTitleLink("https://a24films.com/docs/the-sixth");
$div_a24films_The_Sixth->content = <<<HTML
	<p>A visceral portrayal of a city and nation under siege, as well as a testament to the importance of truth,
	told through the eyes of six individuals who will be forever changed by the January 6th attack on the U.S. Capitol.</p>
	HTML;



$div_youtube_THE_SIXTH_Trailer = new YoutubeContentSection();
$div_youtube_THE_SIXTH_Trailer->setTitleText("The Sixth | Trailer");
$div_youtube_THE_SIXTH_Trailer->setTitleLink("https://www.youtube.com/watch?v=TN10Bb3-P8k");



$div_wikipedia_The_Sixth_2024_film = new WikipediaContentSection();
$div_wikipedia_The_Sixth_2024_film->setTitleText("The Sixth (2024 film)");
$div_wikipedia_The_Sixth_2024_film->setTitleLink("https://en.wikipedia.org/wiki/The_Sixth_(2024_film)");
$div_wikipedia_The_Sixth_2024_film->content = <<<HTML
	<p>The Sixth is a 2024 American documentary film,
	directed and produced by Andrea Nix Fine and Sean Fine.
	It follows the January 6 United States Capitol attack through the perspective of individuals who lived it.</p>
	HTML;



$page->parent('the_sixth.html');
$page->parent('american_fascism.html');
$page->template("stub");
$page->body($div_introduction);
$page->body($div_truth);

$page->body($div_a24films_The_Sixth);
$page->body($div_youtube_THE_SIXTH_Trailer);
$page->body($div_wikipedia_The_Sixth_2024_film);
