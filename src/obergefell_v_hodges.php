<?php
$page = new Page();
$page->h1("Obergefell v. Hodges (2015)");
$page->tags("Institutions: Judiciary", "SCOTUS Landmark Decision", "USA", "Marriage");
$page->keywords("Obergefell v. Hodges");
$page->stars(1);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>"Obergefell v. Hodges" is a 2015 landmark decision by the ${'Supreme Court of the United States'}.</p>

	<p>This decision legalized same-sex marriage nationwide, recognizing the right of same-sex couples to marry under the Fourteenth Amendment.</p>

	<p>Supporters of this ruling say that it was a significant victory for LGBTQ+ rights and equality,
	recognizing the fundamental right to marry as a core component of human dignity and equality.</p>
	HTML;

$div_wikipedia_Obergefell_v_Hodges = new WikipediaContentSection();
$div_wikipedia_Obergefell_v_Hodges->setTitleText("Obergefell v Hodges");
$div_wikipedia_Obergefell_v_Hodges->setTitleLink("https://en.wikipedia.org/wiki/Obergefell_v._Hodges");
$div_wikipedia_Obergefell_v_Hodges->content = <<<HTML
	<p>Obergefell v. Hodges, 576 U.S. 644 (2015), is a landmark decision of the Supreme Court of the United States
	which ruled that the fundamental right to marry is guaranteed to same-sex couples
	by both the Due Process Clause and the Equal Protection Clause of the Fourteenth Amendment of the Constitution.
	The 5–4 ruling requires all 50 states, the District of Columbia, and the Insular Areas to perform and recognize the marriages of same-sex couples
	on the same terms and conditions as the marriages of opposite-sex couples, with equal rights and responsibilities.
	Prior to Obergefell, same-sex marriage had already been established by statute, court ruling,
	or voter initiative in 36 states, the District of Columbia, and Guam.</p>
	HTML;


$page->parent('supreme_court_of_the_united_states.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_wikipedia_Obergefell_v_Hodges);
