<?php
$page = new Page();
$page->h1("Offshoring");
$page->viewport_background("");
$page->keywords("Offshoring");
$page->stars(0);
$page->tags("Living: Economy", "Economy", "Fair Share");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Offshoring = new WikipediaContentSection();
$div_wikipedia_Offshoring->setTitleText("Offshoring");
$div_wikipedia_Offshoring->setTitleLink("https://en.wikipedia.org/wiki/Offshoring");
$div_wikipedia_Offshoring->content = <<<HTML
	<p>
	</p>
	HTML;


$page->parent('fair_share.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Offshoring");
$page->body($div_wikipedia_Offshoring);
