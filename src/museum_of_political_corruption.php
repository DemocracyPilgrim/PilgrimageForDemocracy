<?php
$page = new Page();
$page->h1("Museum of Political Corruption");
$page->keywords("Museum of Political Corruption");
$page->stars(0);
$page->tags("Organisation", "Corruption");

//$page->snp("description", "");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );

$r1 = $page->ref('https://museumofpoliticalcorruption.org/the-nellie-bly-award-for-investigative-reporting/',
                 'Nellie Bly Award for Investigative Reporting');
$r2 = $page->ref('https://www.propublica.org/atpropublica/propublica-wins-nellie-bly-award-for-supreme-court-coverage',
                 'ProPublica Wins Nellie Bly Award for Supreme Court Coverage');
$r3 = $page->ref('https://drive.google.com/file/d/14KhKt1cvd6OCmoUROO_MtxYochtWPlWm/view',
                 'ProPublica’s Supreme Court Investigation Team: Winner of the 2024 Nellie Bly Award for Investigative Reporting');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>In 2024, The Museum of Political Corruption bestowed the Nellie Bly Award for Investigative Reporting $r1 to $ProPublica. $r2 $r3
	This award is bestowed by the Museum of Political Corruption for distinguished reporting on $corruption.
	“ProPublica’s courageous reporting on the Supreme Court was a seismic wake-up call for the public.
	It forced a long overdue reckoning on the Court regarding its ethical standards,” said museum founder and president Bruce Roter.</p>
	HTML;



$div_Museum_of_Political_Corruption_website = new WebsiteContentSection();
$div_Museum_of_Political_Corruption_website->setTitleText("Museum of Political Corruption website");
$div_Museum_of_Political_Corruption_website->setTitleLink("https://museumofpoliticalcorruption.org/");
$div_Museum_of_Political_Corruption_website->content = <<<HTML
	<p>The Museum of Political Corruption, a 501 (C)(3) nonprofit, nonpartisan institution,
	is dedicated to educating and empowering the public by providing a better understanding of political corruption,
	and encouraging solutions that promote ethics reform and honest governance.</p>
	HTML;



$div_wikipedia_Museum_of_Political_Corruption = new WikipediaContentSection();
$div_wikipedia_Museum_of_Political_Corruption->setTitleText("Museum of Political Corruption");
$div_wikipedia_Museum_of_Political_Corruption->setTitleLink("https://en.wikipedia.org/wiki/Museum_of_Political_Corruption");
$div_wikipedia_Museum_of_Political_Corruption->content = <<<HTML
	<p>The Museum of Political Corruption is an online museum that was originally planned to be in a physical space in Albany, New York.
	The online museum focuses on political corruption.</p>
	HTML;


$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_Museum_of_Political_Corruption_website);

$page->body('corruption.html');

$page->body($div_wikipedia_Museum_of_Political_Corruption);
