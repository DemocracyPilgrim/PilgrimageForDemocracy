<?php
$page = new Page();
$page->h1("Diamond open access");
$page->tags("Information", "Open Source");
$page->keywords("Diamond open access");
$page->stars(0);
$page->viewport_background("/free/diamond_open_access.png");

//$page->snp("description", "");
$page->snp("image",       "/free/diamond_open_access.1200-630.png");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Diamond_open_access = new WikipediaContentSection();
$div_wikipedia_Diamond_open_access->setTitleText("Diamond open access");
$div_wikipedia_Diamond_open_access->setTitleLink("https://en.wikipedia.org/wiki/Diamond_open_access");
$div_wikipedia_Diamond_open_access->content = <<<HTML
	<p>Diamond open access refers to academic texts (such as monographs, edited collections, and journal articles) published/distributed/preserved with no fees to either reader or author. Alternative labels include platinum open access, non-commercial open access, cooperative open access or, more recently, open access commons.</p>
	HTML;


$page->parent('information.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Diamond open access");
$page->body($div_wikipedia_Diamond_open_access);
