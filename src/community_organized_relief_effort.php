<?php
$page = new OrganisationPage();
$page->h1("Community Organized Relief Effort");
$page->viewport_background("");
$page->keywords("Community Organized Relief Effort", "CORE");
$page->stars(0);
$page->tags("Organisation", "Sean Penn", "Charity", "Relief Organization", "Humanity");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Community Organized Relief Effort is a charitable relief organization co-founded by ${'Sean Penn'} and Ann Lee.</p>
	HTML;



$div_CORE_Response = new WebsiteContentSection();
$div_CORE_Response->setTitleText("CORE Response ");
$div_CORE_Response->setTitleLink("https://www.coreresponse.org/");
$div_CORE_Response->content = <<<HTML
	<p>We are a global humanitarian organization that delivers immediate response and long-term recovery solutions to underserved communities across the globe.</p>
	HTML;



$div_wikipedia_Community_Organized_Relief_Effort = new WikipediaContentSection();
$div_wikipedia_Community_Organized_Relief_Effort->setTitleText("Community Organized Relief Effort");
$div_wikipedia_Community_Organized_Relief_Effort->setTitleLink("https://en.wikipedia.org/wiki/Community_Organized_Relief_Effort");
$div_wikipedia_Community_Organized_Relief_Effort->content = <<<HTML
	<p>Community Organized Relief Effort, also known as CORE Response and formerly as J/P Haitian Relief Organization is a non-profit organization founded by actor Sean Penn and Ann Lee in response to the January 12, 2010 earthquake in Haiti. The organization was founded in 2010 and changed its name in 2019 to receive contracts and donations to work globally. In March 2020, CORE began administering free COVID-19 tests in the U.S. amid the COVID-19 pandemic.</p>
	HTML;


$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Community Organized Relief Effort");
$page->body($div_CORE_Response);
$page->body($div_wikipedia_Community_Organized_Relief_Effort);
