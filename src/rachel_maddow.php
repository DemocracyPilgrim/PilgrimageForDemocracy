<?php
$page = new PersonPage();
$page->h1("Rachel Maddow");
$page->alpha_sort("Maddow, Rachel");
$page->tags("Person", "Information: Media");
$page->keywords("Rachel Maddow");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Rachel_Maddow = new WikipediaContentSection();
$div_wikipedia_Rachel_Maddow->setTitleText("Rachel Maddow");
$div_wikipedia_Rachel_Maddow->setTitleLink("https://en.wikipedia.org/wiki/Rachel_Maddow");
$div_wikipedia_Rachel_Maddow->content = <<<HTML
	<p>Rachel Anne Maddow is an American television news program host and liberal political commentator.
	Maddow hosts The Rachel Maddow Show, a weekly television show on MSNBC, and serves as the cable network's special event co-anchor.</p>
	HTML;


$page->parent('list_of_people.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Rachel Maddow");
$page->body($div_wikipedia_Rachel_Maddow);
