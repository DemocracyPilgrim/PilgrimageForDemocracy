<?php
$page = new Page();
$page->h1('3: Institutions');
$page->stars(2);
$page->tags("Topic portal");
$page->keywords("Institutions", "institutions", "institutional", "system",
                "Institutions: Executive", "Institutions: Legislative",'Institutions: Judiciary', "Institutions: Security");
$page->viewport_background('/free/institutions.png');


$page->snp('description', "The pillars of democracy");
$page->snp('image', "/free/institutions.1200-630.png");


$page->preview( <<<HTML
	<p>Healthy institutions are the most critical parts to safeguard democracy.</p>
	HTML );



$div_institutions = new ContentSection();
$div_institutions->content = <<<HTML
	<h3>The Bedrock of Democracy</h3>

	<p>Democracy thrives on a strong foundation of well-functioning institutions –
	the structures that govern our societies, provide essential services, and protect our rights.
	These institutions are the backbone of the ${'Third Level of Democracy'},
	acting as a vital bulwark against the erosion of democratic principles and the encroachment of authoritarianism.
	In times of crisis, when societal norms and individual actions falter,
	robust institutions are often the last line of defence, safeguarding the values of freedom, justice, and equality.
	This page aims to serve as a comprehensive outline of the key elements of a healthy democratic system,
	exploring the crucial role of institutions in shaping a just and equitable society.</p>
	HTML;


$h2_Public_Institutions = new h2HeaderContent("Public Institutions");


$list_core_institutions = new ListOfPages();
$list_core_institutions->add('separation_of_powers.html');
$list_core_institutions->add('branches_of_government.html');
$list_core_institutions->add('constitution.html');
$list_core_institutions->add('institutional_vs_human_factors_in_democracy.html');
$list_core_institutions->add('small_government.html');
$list_core_institutions->add('taxes.html');
$print_list_core_institutions = $list_core_institutions->print();

$div_list_core_institutions = new ContentSection();
$div_list_core_institutions->content = <<<HTML
	<h3>Core Institutions</h3>

	$print_list_core_institutions
	HTML;


$list_Institutions_Executive = ListOfPeoplePages::WithTags("Institutions: Executive");
$print_list_Institutions_Executive = $list_Institutions_Executive->print();

$div_list_Institutions_Executive = new ContentSection();
$div_list_Institutions_Executive->content = <<<HTML
	<h3>Executive</h3>

	$print_list_Institutions_Executive
	HTML;



$list_Institutions_Legislative = ListOfPeoplePages::WithTags("Institutions: Legislative");
$print_list_Institutions_Legislative = $list_Institutions_Legislative->print();

$div_list_Institutions_Legislative = new ContentSection();
$div_list_Institutions_Legislative->content = <<<HTML
	<h3>Legislative</h3>

	$print_list_Institutions_Legislative
	HTML;





$list_Judiciary = new ListOfPages();
$list_Judiciary->add('criminal_justice.html');
$list_Judiciary->add('corruption.html');
$list_Judiciary->add('justice.html');
$list_Judiciary->add('immunity.html');
$print_list_Judiciary = $list_Judiciary->print();


$list_Judiciary_Topics = ListOfPeoplePages::WithTags("Institutions: Judiciary");
$print_list_Judiciary_Topics = $list_Judiciary_Topics->print();


$div_Judiciary = new ContentSection();
$div_Judiciary->content = <<<HTML
	<h3>Judiciary</h3>

	<p>Topics to be covered include: judicial independence and accountability...</p>

	<p>It is often said that with great power comes great responsibility.
	We have to make sure <em>not</em> to disconnect power and responsibility.</p>

	$print_list_Judiciary

	<p>Related topics:</p>

	$print_list_Judiciary_Topics
	HTML;

$list_Electoral_Institutions = new ListOfPages();
$list_Electoral_Institutions->add('fourth_level_of_democracy.html');
$list_Electoral_Institutions->add('voting_methods.html');
$list_Electoral_Institutions->add('duverger_syndrome.html');
$list_Electoral_Institutions->add('election_integrity.html');
$print_list_Electoral_Institutions = $list_Electoral_Institutions->print();

$div_list_Electoral_Institutions = new ContentSection();
$div_list_Electoral_Institutions->content = <<<HTML
	<h3>Electoral Institutions</h3>

	$print_list_Electoral_Institutions
	HTML;


$list_Security_Topics = ListOfPeoplePages::WithTags("Institutions: Security");
$print_list_Security_Topics = $list_Security_Topics->print();

$div_Security = new ContentSection();
$div_Security->content = <<<HTML
	<h3>Security</h3>

	$print_list_Security_Topics
	HTML;



$h2_Private_institutions = new h2HeaderContent("Private institutions");

$list_Private_Economy = new ListOfPages();
$list_Private_Economy->add('industry_self_regulation.html');
$list_Private_Economy->add('sixth_level_of_democracy.html');
$print_list_Private_Economy = $list_Private_Economy->print();

$div_list_Private_Economy = new ContentSection();
$div_list_Private_Economy->content = <<<HTML
	<h3>Private Economy</h3>

	$print_list_Private_Economy
	HTML;



$h2_Non_Profit_Sector = new h2HeaderContent("Non-Profit Sector");


$list_Non_Profit_Sector = new ListOfPages();
$list_Non_Profit_Sector->add('list_of_organisations.html');
$list_Non_Profit_Sector->add('eighth_level_of_democracy.html');
$print_list_Non_Profit_Sector = $list_Non_Profit_Sector->print();

$div_list_Non_Profit_Sector = new ContentSection();
$div_list_Non_Profit_Sector->content = <<<HTML
	<h3>Non-Profit Sector</h3>

	$print_list_Non_Profit_Sector
	HTML;




$list_Institutions = ListOfPeoplePages::WithTags("Institutions");
$print_list_Institutions = $list_Institutions->print();

$div_list_Institutions = new ContentSection();
$div_list_Institutions->content = <<<HTML
	<h3>Related content</h3>

	$print_list_Institutions
	HTML;



$page->parent('third_level_of_democracy.html');
$page->previous("society.html");
$page->next("elections.html");


$page->body($div_institutions);

$page->body($h2_Public_Institutions);
$page->body($div_list_core_institutions);
$page->body($div_list_Institutions_Executive);
$page->body($div_list_Institutions_Legislative);
$page->body($div_Judiciary);
$page->body($div_list_Electoral_Institutions);
$page->body($div_Security);

$page->body($h2_Private_institutions);
$page->body($div_list_Private_Economy);

$page->body($h2_Non_Profit_Sector);
$page->body($div_list_Non_Profit_Sector);

$page->body($div_list_Institutions);
