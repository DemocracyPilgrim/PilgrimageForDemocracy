<?php
$page = new Page();
$page->h1("Open-source voting system");
$page->tags("Elections", "Open Source");
$page->keywords("Open-source Voting Systems", "open-source voting systems");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;


$list_Open_source_Voting_Systems = ListOfPeoplePages::WithTags("Open-source Voting Systems");
$print_list_Open_source_Voting_Systems = $list_Open_source_Voting_Systems->print();

$div_list_Open_source_Voting_Systems = new ContentSection();
$div_list_Open_source_Voting_Systems->content = <<<HTML
	<h3>Related content</h3>

	$print_list_Open_source_Voting_Systems
	HTML;



$div_wikipedia_Open_source_voting_system = new WikipediaContentSection();
$div_wikipedia_Open_source_voting_system->setTitleText("Open source voting system");
$div_wikipedia_Open_source_voting_system->setTitleLink("https://en.wikipedia.org/wiki/Open-source_voting_system");
$div_wikipedia_Open_source_voting_system->content = <<<HTML
	<p>An open-source voting system (OSVS), also known as open-source voting (or OSV), is a voting system that uses open-source software (and/or hardware)
	that is completely transparent in its design in order to be checked by anyone for bugs or issues.
	Free and open-source systems can be adapted and used by others without paying licensing fees,
	improving the odds they achieve the scale usually needed for long-term success.
	The development of open-source voting technology has shown a small but steady trend towards increased adoption
	since the first system was put into practice in Choctaw County, Mississippi in 2019.</p>
	HTML;


$page->parent('elections.html');
$page->template("stub");
$page->body($div_introduction);

$page->body($div_list_Open_source_Voting_Systems);

$page->body($div_wikipedia_Open_source_voting_system);
