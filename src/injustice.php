<?php
$page = new Page();
$page->h1("Injustice");
$page->tags("Society", "Justice", "Institutions: Judiciary");
$page->keywords("Injustice", "injustice", "unjust");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Injustice = new WikipediaContentSection();
$div_wikipedia_Injustice->setTitleText("Injustice");
$div_wikipedia_Injustice->setTitleLink("https://en.wikipedia.org/wiki/Injustice");
$div_wikipedia_Injustice->content = <<<HTML
	<p>Injustice is a quality relating to unfairness or undeserved outcomes. The term may be applied in reference to a particular event or situation, or to a larger status quo. In Western philosophy and jurisprudence, injustice is very commonly—but not always—defined as either the absence or the opposite of justice.</p>
	HTML;


$page->parent('society.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Injustice");
$page->body($div_wikipedia_Injustice);
