<?php
$page = new Page();
$page->h1('Development website');
$page->stars(1);

$page->snp('description', "How to set up a development website on your own computer.");
$page->snp('image', "/copyrighted/domenico-loia-hGV2TfOh0ns.1200-630.jpg");

$page->preview( <<<HTML
	<p>This page provides instructions on how to set up a development website on your own computer.
	You can then change the source code of the website and see the results straight away.</p>
	HTML );

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Below are basic instructions on how to install a copy of the website <a href="https://pildem.org/">pildem.org</a> on your own computer.</p>
	HTML;


$div_codeberg = new CodebergContentSection();
$div_codeberg->setTitleText('Get help at Codeberg');
$div_codeberg->setTitleLink('https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy/issues/');
$div_codeberg->content = <<<HTML
	<p>If the instructions in this page are unclear or incomplete,
	open a new issue in our issue tracker at Codeberg, and we shall assist you.</p>
	HTML;

$h2_source = new h2HeaderContent('Source code');

$div_source = new ContentSection();
$div_source->content = <<<HTML
	<p>The first step is to get a full copy of the source code of the project.</p>

	<p>Clone the repository.</p>

	<code>$ git clone https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy.git</code>

	<p>Make note of where the sourced get cloned. The precise location will be needed when configuring Apache below.</p>

	<p>See also the page about the git workflow.</p>
	HTML;

$h2_apache_webserver = new h2HeaderContent('Apache web server');

$div_apache_webserver = new ContentSection();
$div_apache_webserver->content = <<<HTML
	<p>Install a web server. Here we recommend the Apache web server, but other web servers like nginx would do too.</p>

	<h3>Linux</h3>

	<p>Use the regular package manager tool used in your Linux distribution. Here is the command for Gentoo:</p>

	<code># emerge --ask www-servers/apache</code>

	<p>Edit the file "<em>/etc/hosts</em>" to register the local domain name under which you will run the website:</p>

	<code>127.0.0.1       pildem-development</code>

	<p>In the Apache vhost directory (on Gentoo: "<em>/etc/apache2/vhosts.d/</em>"),
	create a new configuration file (for example named: "<em>pildem.conf</em>") based on the following one.
	Adjust the settings based on the server name that you have configured, and the directory in which you installed the source code.</p>

	<code>&lt;VirtualHost *:80&gt;
		ServerAdmin webmaster@127.0.0.1
		ServerName pildem-development
		DirectoryIndex index.html
		DocumentRoot /home/theo/PilgrimageForDemocracy/http/
	&lt;/VirtualHost&gt;
	</code>

	<h3>Windows</h3>

	<p>Check the following official documentation for Apache.
	You can also find many handy tutorials on the internet by searching "<em>install Apache on Windows</em>".</p>

	<ul>
	<li><a href="https://httpd.apache.org/docs/2.4/platform/windows.html">Using Apache HTTP Server on Microsoft Windows</a></li>
	</ul>
	HTML;

$h2_php = new h2HeaderContent('PHP');

$div_php = new ContentSection();
$div_php->content = <<<HTML
	<h3>Linux</h3>

	<p>Use the regular package manager tool used in your Linux distribution. Here is the command for Gentoo:</p>

	<code># emerge --ask dev-lang/php</code>

	<h3>Windows</h3>

	<p>Check the following official documentation for Apache.
	You can also find many handy tutorials on the internet by searching "<em>install Apache on Windows</em>".</p>

	<ul>
	<li><a href="https://windows.php.net/download">Download PHP For Windows</a></li>
	</ul>
	HTML;


$page->parent('project/index.html');

$page->body($div_introduction);
$page->body($div_codeberg);

$page->body($h2_source);
$page->body($div_source);
$page->body('project/git.html');

$page->body($h2_apache_webserver);
$page->body($div_apache_webserver);

$page->body($h2_php);
$page->body($div_php);
