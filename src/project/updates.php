<?php
$page = new Page();
$page->h1('Updates');
$page->stars(-1);
$page->keywords('updates', 'updated');
$page->viewport_background('/free/updates.png');

$page->snp("description", "What's new in the Pilgrimage for Democracy and Social Justice...");
$page->snp("image",       "/free/updates.1200-630.png");


$page->preview( <<<HTML
	<p>If you are already very familiar with the existing content of this website,
	this page makes it convenient to check all the newest updates and additions since your last visit.</p>
	HTML );

$h2_code = new h2HeaderContent('Code update');

$div_code = new ContentSection();
$div_code->content = <<<HTML
	<p>This website is open source.
	The whole source code is published within the <a href="https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy">Pilgrimage for Democracy</a> at Codeberg.
	If you are interested in following every minor change in the website,
	you can browse the project's code repository and <a href="https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy/commits/branch/master">check every single commit</a>.
	</p>
	<p>See below for the most important content updates, with new articles, expanded articles and expanded sections
	or check the <a href="/latest.html">files changed</a>.</p>
	HTML;

$h2_content = new h2HeaderContent('Content update');

$div_update = new ContentSection();
$div_update->content = <<<HTML
	<p>See below the list of <em>major</em> updates since your last visit:</p>
	<ul>
	HTML;





// function addUpdate(&$div_update, $date, $stars_from, $stars_to, $link, $title_en, $text_en = '')
//addUpdate($div_update, '2025-02-1', 0, 1, '/', "", "");


addUpdate($div_update, '2025-02-10', 0, 3, '/informed_score_voting_for_stronger_democracy.html', "Informed Score Voting as the Vanguard for a Stronger Democracy", "");
addUpdate($div_update, '2025-02-07', 0, 3, '/the_i_dont_know_revolution.html', 'The "I Don\'t Know" Revolution: How a Simple Option Can Transform Democracy', "");
addUpdate($div_update, '2025-02-06', 0, 4, '/informed_score_voting.html', "Informed Score Voting: Elevating Knowledge, Empowering Choice", "");
addUpdate($div_update, '2025-02-06', 0, 1, '/voting_methods.html', "Voting methods", "");
addUpdate($div_update, '2025-02-06', 0, 3, '/score_voting.html', "Score voting: Rate, Don't Just Pick", "");
addUpdate($div_update, '2025-02-05', 0, 3, '/approval_voting.html', "Approval Voting: A Simple Path to Stronger Democracies", "");
addUpdate($div_update, '2025-02-04', 0, 4, '/desperation_economy.html', "Desperation Economy: When Survival Becomes a Struggle", "");
addUpdate($div_update, '2025-02-03', 0, 4, '/disinformation.html', "Disinformation", "");
addUpdate($div_update, '2025-02-03', 0, 4, '/universal_basic_income.html', "Universal basic income: A Foundation for a Just and Sustainable Future", "");
addUpdate($div_update, '2025-02-02', 0, 3, '/fair_share_of_responsibilities.html', "A Fair Share of Responsibilities", "");
addUpdate($div_update, '2025-02-01', 0, 3, '/fair_share_of_profits.html', "A Fair Share of the Profits", "");
addUpdate($div_update, '2025-02-01', 0, 3, '/factors_of_production_and_the_stewardship_of_the_commons.html', "Factors of Production and the Stewardship of the Commons", "");
addUpdate($div_update, '2025-02-01', 0, 3, '/factors_of_production.html', "Factors of Production", "");

addUpdate($div_update, '2025-01-31', 0, 4, '/capital.html', "Capital", "");
addUpdate($div_update, '2025-01-30', 0, 2, '/labour_and_value_creation.html', "Labour: The Human Contribution to Value Creation", "");
addUpdate($div_update, '2025-01-29', 0, 3, '/personal_responsibility_rhetoric.html', "Personal Responsibility Rhetoric", "");
addUpdate($div_update, '2025-01-29', 0, 3, '/personal_responsibility.html', "Personal Responsibility", "");
addUpdate($div_update, '2025-01-29', 0, 2, '/richard_daynard.html', "Richard Daynard", "");
addUpdate($div_update, '2025-01-27', 0, 4, '/lessig_our_democracy_no_longer_represents_the_people.html', "Lessig: Our democracy no longer represents the people. Here's how to fix it.", "");
addUpdate($div_update, '2025-01-26', 0, 2, '/lawrence_lessig.html', "Lawrence Lessig", "");
addUpdate($div_update, '2025-01-26', 0, 1, '/international.html', "International", "");
addUpdate($div_update, '2025-01-26', 0, 3, '/democratic_osmosis.html', "Democratic Osmosis: How Our Example Can Change the World", "");
addUpdate($div_update, '2025-01-25', 0, 3, '/the_power_of_example_improving_our_democracies.html', "The Power of Example: How Improving Our Democracies Can Help Those Under Oppression", "");
addUpdate($div_update, '2025-01-25', 0, 2, '/duverger_trap.html', "The Duverger Trap: How a Flawed Electoral System Opens the Door to Authoritarian Exploitation", "");
addUpdate($div_update, '2025-01-25', 0, 3, '/authoritarian_exploitation_of_democratic_weaknesses.html', "How Authoritarian Regimes use Our Democratic Weaknesses Against Us.", "");
addUpdate($div_update, '2025-01-23', 0, 1, '/tweed_syndrome.html', "Tweed Syndrome", "");
addUpdate($div_update, '2025-01-23', 0, 4, '/the_two_sides_of_democratic_dysfunction.html', "The Two Sides of Democratic Dysfunction: Understanding Duverger Syndrome and Tweed Syndrome", "");
addUpdate($div_update, '2025-01-23', 0, 4, '/tweed_syndrome_systemic_corruption.html', "The Enduring Threat of Tweed Syndrome: Systemic Corruption in the Modern Age", "");
addUpdate($div_update, '2025-01-22', 0, 3, '/tweedism.html', "Tweedism: A Legacy of Corruption", "");
addUpdate($div_update, '2025-01-20', 0, 3, '/ignorant_leader_rising.html', "The Rise of the Dunce: How Ignorance Undermines Democracy", "");
addUpdate($div_update, '2025-01-19', 0, 2, '/externalities.html', "Externalities", "");
addUpdate($div_update, '2025-01-19', 0, 3, '/organic_taxes_the_cure_for_healthcare_funding.html', "Organic Taxes: The Cure for Healthcare Funding", "");
addUpdate($div_update, '2025-01-18', 0, 2, '/labor_taxes.html', "Labor Taxes: A Heavy and Unjust Burden", "");
addUpdate($div_update, '2025-01-18', 0, 2, '/tax_renaissance.html', "The Tax Renaissance: A New Era for Justice and Sustainability", "");
addUpdate($div_update, '2025-01-18', 0, 3, '/pigouvian_tax.html', "Pigouvian taxes: Correcting Market Failures and Promoting Efficiency", "");
addUpdate($div_update, '2025-01-18', 0, 3, '/organic_taxes.html', "Organic Taxes: A Path Towards a Balanced and Sustainable Future", "");
addUpdate($div_update, '2025-01-17', 0, 3, '/redistribution_of_wealth_a_false_solution.html', "Redistribution of wealth: a false solution", "");
addUpdate($div_update, '2025-01-17', 0, 3, '/democracy_taxes.html', "Addendum E: Democracy — The small matter of taxation", "");
addUpdate($div_update, '2025-01-17', 0, 3, '/digital_habitat.html', "Addendum G: Democracy — Our Digital Habitat: Towards an Enlightened Web?", "");
addUpdate($div_update, '2025-01-16', 0, 1, '/william_m_tweed.html', "William M. Tweed, a.k.a. Boss Tweed", "");
addUpdate($div_update, '2025-01-16', 0, 1, '/oligarchy.html', "Oligarchy", "");
addUpdate($div_update, '2025-01-16', 0, 2, '/obligations_in_democracy.html', "Obligations in democracy", "");

addUpdate($div_update, '2025-01-15', -1, -1, 'https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy/commits/branch/master', '1600 commits!', "There are now 1600 commits in the project's code repository, with a total of 671 articles and 981 files.");

addUpdate($div_update, '2025-01-15', 0, 2, '/environmental_stewardship.html', "Addendum F: Democracy — Environmental Stewardship", "");
addUpdate($div_update, '2025-01-13', 0, 2, '/democratic_republic_of_the_congo.html', "Democratic Republic of the Congo", "");
addUpdate($div_update, '2025-01-13', 0, 2, '/egypt.html', "Egypt", "");
addUpdate($div_update, '2025-01-13', 0, 1, '/tech_policy_press.html', "Tech Policy Press", "");
addUpdate($div_update, '2025-01-13', 0, 2, '/new_zealand.html', "New Zealand", "");
addUpdate($div_update, '2025-01-13', 0, 2, '/algorithms.html', "Algorithms", "");
addUpdate($div_update, '2025-01-13', 0, 2, '/social_media_and_democracy.html', "Social Media and Democracy", "");
addUpdate($div_update, '2025-01-12', 0, 2, '/bangladesh.html', "Bangladesh", "");
addUpdate($div_update, '2025-01-12', 1, 2, '/philippines.html', "The Philippines", "");
addUpdate($div_update, '2025-01-11', 0, 2, '/colombia.html', "Colombia", "");
addUpdate($div_update, '2025-01-11', 0, 1, '/peace.html', "Peace", "");
addUpdate($div_update, '2025-01-11', 0, 2, '/the_children_s_peace_movement_columbia.html', "The Children's Peace Movement: Planting Seeds of Hope in Colombia", "");
addUpdate($div_update, '2025-01-11', 0, 1, '/humanity.html', "8: Humanity", "");
addUpdate($div_update, '2025-01-11', 0, 2, '/rohingya_people.html', "Rohingya people", "");
addUpdate($div_update, '2025-01-11', 0, 2, '/myanmar.html', "Myanmar", "");
addUpdate($div_update, '2025-01-10', 1, 2, '/fifth_level_of_democracy.html', "Fifth level of democracy — We, the Media", "");
addUpdate($div_update, '2025-01-10', 1, 3, '/media.html', "Media", "");
addUpdate($div_update, '2025-01-10', 0, 2, '/social_networks.html', "Social Networks", "");
addUpdate($div_update, '2025-01-10', 0, 2, '/social_media_ban.html', "Social media ban", "");


show_nb_articles_by_stars($div_update, '2025-01-01', 441, 121, 45, 14, 5, 0);

addUpdate($div_update, '2024-12-31', 1, 2, '/united_states.html', "United States of America", "");
addUpdate($div_update, '2024-12-14', -1, -1, 'https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy/commits/branch/master', '1500 commits!', "There are now 1500 commits in the project's code repository, with a total of 623 articles and 865 files.");

addUpdate($div_update, '2024-12-13', 0, 1, '/fourth_level_of_democracy.html', "Fourth level of democracy — We, the People", "");
addUpdate($div_update, '2024-12-10', 0, 1, '/sixth_level_of_democracy.html', "Sixth level of democracy — We, the Breadwinners", "");
addUpdate($div_update, '2024-12-10', 0, 1, '/centre_for_law_and_policy_research.html', "Centre for Law & Policy Research", "");
addUpdate($div_update, '2024-12-09', 0, 2, '/bhimrao_ramji_ambedkar.html', "B.R. Ambedkar", "");
addUpdate($div_update, '2024-11-21', 0, 3, '/kamala_harris_concession_speech.html', "Kamala Harris Concession Speech", "");
addUpdate($div_update, '2024-11-19', 0, 2, '/approval_voting_party.html', "Approval Voting Party", "");
addUpdate($div_update, '2024-11-14', 0, 2, '/seventh_level_of_democracy.html', "Seventh level of democracy — We, the Earthlings", "");
addUpdate($div_update, '2024-10-24', 0, 2, '/euromaidan.html', "Euromaidan", "");
addUpdate($div_update, '2024-10-22', 0, 4, '/ali_velshi_s_hope_for_the_middle_east_and_humanity.html', "Ali Velshi's hope for the Middle East and humanity", "");
addUpdate($div_update, '2024-10-21', 0, 1, '/talent_is_evenly_distributed_worldwide_but_opportunity_is_not.html', "Talent is evenly distributed worldwide but opportunity is not", "");
addUpdate($div_update, '2024-10-21', 0, 1, '/cnn_hero_payton_mcgriff_on_school_uniform_for_school_girl_education_in_togo.html', "CNN Hero: Payton McGriff on school uniform for school girl education in Togo", "");
addUpdate($div_update, '2024-10-21', 0, 1, '/style_her_empowered.html', "Style Her Empowered (SHE)", "");
addUpdate($div_update, '2024-10-04', 0, 2, '/famine_in_madagascar.html', "Famine in Madagascar", "");

addUpdate($div_update, '2024-09-29', -1, -1, 'https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy/commits/branch/master', '1400 commits!', "There are now 1400 commits in the project's code repository, with a total of 507 articles and 644 files.");

addUpdate($div_update, '2024-09-29', 0, 1, '/marc_elias.html', "Marc Elias", "");
addUpdate($div_update, '2024-09-29', 0, 1, '/in_deep_trouble_surfacing_tech_powered_sexual_harassment_in_k_12_schools.html', "In Deep Trouble: Surfacing Tech-Powered Sexual Harassment in K-12 Schools", "");
addUpdate($div_update, '2024-09-29', 0, 1, '/center_for_democracy_and_technology.html', "Center for Democracy and Technology", "");
addUpdate($div_update, '2024-09-29', 0, 1, '/charlie_chaplin_final_speech_from_the_great_dictator.html', "Charlie Chaplin: Final Speech from The Great Dictator", "");
addUpdate($div_update, '2024-09-29', 0, 3, '/barack_obama_keynote_address_at_the_2004_democratic_national_convention.html', "Barack Obama: Keynote Address at the 2004 Democratic National Convention", "");
addUpdate($div_update, '2024-09-27', 0, 1, '/information.html', "Information", "");
addUpdate($div_update, '2024-09-27', 1, 2, '/political_discourse.html', "Political discourse", "");
addUpdate($div_update, '2024-09-26', 0, 4, '/president_as_saviour_syndrome.html', "President as Saviour Syndrome", "");
addUpdate($div_update, '2024-09-26', 0, 3, '/the_final_minutes_of_president_obama_s_farewell_address_yes_we_can.html', "The Final Minutes of President Obama's Farewell Address: Yes, we can.", "");
addUpdate($div_update, '2024-09-26', 0, 3, '/ken_burns_on_the_incredibly_dangerous_party_that_the_gop_has_morphed_into.html', "Ken Burns on the evolution of the Republican Party", "");
addUpdate($div_update, '2024-09-23', 0, 2, '/permission_structures.html', "Permission Structure", "");
addUpdate($div_update, '2024-09-23', 0, 1, '/the_righteous_mind.html', "The Righteous Mind: Why Good People are Divided by Politics and Religion", "");
addUpdate($div_update, '2024-09-20', 0, 1, '/supreme_court_of_the_united_states.html', "Supreme Court of the United States (SCOTUS)", "");
addUpdate($div_update, '2024-09-20', 0, 2, '/brandenburg_v_ohio.html', "Brandenburg v. Ohio (1969)", "");
addUpdate($div_update, '2024-09-20', -1, -1, 'https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy/commits/branch/master', '1300 commits!', "There are now 1300 commits in the project's code repository, with a total of 507 articles and 644 files.");
addUpdate($div_update, '2024-09-20', 0, 1, '/orwellian_doublespeak.html', "Orwellian Doublespeak in today's political discourse", "");
addUpdate($div_update, '2024-09-18', 0, 1, '/glenn_kirschner_on_trump_s_legal_issues_for_haitian_immigrant_hoax.html', "Glenn Kirschner on Trump's legal issues for Haitian immigrant hoax", "");
addUpdate($div_update, '2024-09-18', 0, 1, '/erasing_history_how_fascists_rewrite_the_past_to_control_the_future.html', "Erasing History: How Fascists Rewrite the Past to Control the Future", "");
addUpdate($div_update, '2024-09-18', 0, 2, '/60_minutes_conflict_between_china_philippines_could_involve_u_s_and_lead_to_a_clash_of_superpowers.html', "60 Minutes: Conflict between China, Philippines could involve U.S. and lead to a clash of superpowers", "");
addUpdate($div_update, '2024-09-18', 1, 2, '/project/standing_on_the_shoulders_of_giants.html', "Standing on the shoulders of giants", "");
addUpdate($div_update, '2024-09-18', 0, 1, '/project/videos_tv_programs_interviews_and_documentaries.html', "Videos, TV programs, interviews and documentaries", "");
addUpdate($div_update, '2024-09-10', 0, 1, '/invisible_rulers_the_people_who_turn_lies_into_reality.html', "Invisible Rulers: The People Who Turn Lies into Reality", "");
addUpdate($div_update, '2024-09-10', 0, 1, '/stanford_internet_observatory.html', "Stanford Internet Observatory", "");
addUpdate($div_update, '2024-09-09', 0, 1, '/the_big_truth_upholding_democracy_in_the_age_of_the_big_lie.html', "The Big Truth: Upholding Democracy in the Age of “The Big Lie”", "");
addUpdate($div_update, '2024-09-09', 0, 1, '/unsung_heroes_of_democracy_award.html', "Unsung Heroes of Democracy Award", "");
addUpdate($div_update, '2024-09-09', 0, 1, '/freedom_of_speech.html', "Freedom of speech", "");
addUpdate($div_update, '2024-09-08', 0, 1, '/aba_task_force_for_american_democracy.html', "ABA Task Force for American Democracy", "");
addUpdate($div_update, '2024-09-08', 0, 1, '/list_of_organisations.html', "List of organisations", "");
addUpdate($div_update, '2024-09-08', 0, 1, '/american_bar_association.html', "American Bar Association", "");
addUpdate($div_update, '2024-09-07', 0, 1, '/trump_v_vance.html', "Trump v. Vance (2020)", "");
addUpdate($div_update, '2024-09-07', 1, 2, '/artificial_intelligence.html', "Artificial Intelligence", "");
addUpdate($div_update, '2024-09-06', 0, 2, '/certifying_elections.html', "Certifying Elections", "");

addUpdate($div_update, '2024-09-05', -1, -1, 'https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy/commits/branch/master', '1200 commits!', "There are now 1200 commits in the project's code repository, with a total of 504 articles and 572 files.");

addUpdate($div_update, '2024-09-04', 0, 2, '/foreign_workers_in_taiwan.html', "Foreign Workers in Taiwan", "");
addUpdate($div_update, '2024-09-04', 0, 2, '/migrant_workers.html', "Migrant Workers", "");
addUpdate($div_update, '2024-09-04', 0, 2, '/institutions.html', "Institutions", "");
addUpdate($div_update, '2024-09-04', 0, 2, '/loper_bright_enterprises_v_raimondo.html', "Loper Bright Enterprises v. Raimondo (2024)", "");
addUpdate($div_update, '2024-09-04', 0, 2, '/chevron_u_s_a_inc_v_natural_resources_defense_council_inc.html', "Chevron U.S.A., Inc. v. Natural Resources Defense Council, Inc. (1984)", "");

addUpdate($div_update, '2024-09-03', 0, 1, '/national_federation_of_independent_business_v_sebelius.html', "National Federation of Independent Business v. Sebelius (2012)", "");
addUpdate($div_update, '2024-09-03', 0, 1, '/kelo_v_city_of_new_london.html', "Kelo v. City of New London (2005)", "");
addUpdate($div_update, '2024-09-03', 0, 1, '/burwell_v_hobby_lobby_stores_inc.html', "Burwell v. Hobby Lobby Stores, Inc. (2014)", "");
addUpdate($div_update, '2024-09-03', 0, 1, '/mcdonald_v_city_of_chicago.html', "McDonald v. City of Chicago (2010)", "");
addUpdate($div_update, '2024-09-03', 0, 1, '/gun_control.html', "Gun Control", "");
addUpdate($div_update, '2024-09-03', 0, 1, '/district_of_columbia_v_heller.html', "District of Columbia v. Heller (2008)", "");
addUpdate($div_update, '2024-09-03', 0, 1, '/miranda_v_arizona.html', "Miranda v. Arizona (1966)", "");
addUpdate($div_update, '2024-09-03', 0, 1, '/gideon_v_wainwright.html', "Gideon v. Wainwright (1963)", "");
addUpdate($div_update, '2024-09-03', 0, 1, '/obergefell_v_hodges.html', "Obergefell v. Hodges (2015)", "");

addUpdate($div_update, '2024-09-02', 0, 1, '/loving_v_virginia.html', "Loving v. Virginia (1967)", "");
addUpdate($div_update, '2024-09-02', 0, 1, '/roe_v_wade.html', "Roe v. Wade (1973)", "");
addUpdate($div_update, '2024-09-02', 0, 1, '/elections.html', "Elections", "");
addUpdate($div_update, '2024-09-02', 0, 1, '/brown_v_board_of_education.html', "Brown v. Board of Education (1954)", "");
addUpdate($div_update, '2024-09-02', 0, 1, '/plessy_v_ferguson.html', "Plessy v. Ferguson (1896)", "");
addUpdate($div_update, '2024-09-02', 0, 2, '/trump_v_united_states.html', "Trump v. United States (2024)", "");
addUpdate($div_update, '2024-09-02', 0, 1, '/subsidiarity.html', "Subsidiarity", "");

show_nb_articles_by_stars($div_update, '2024-09-01', 302, 80, 23, 10, 3, 0);

addUpdate($div_update, '2024-09-01', 0, 1, '/mcculloch_v_maryland.html', "McCulloch v. Maryland (1819)", "");
addUpdate($div_update, '2024-09-01', 0, 1, '/bush_v_gore.html', "Bush v. Gore (2000)", "");
addUpdate($div_update, '2024-09-01', 0, 1, '/shelby_county_v_holder.html', "Shelby County v. Holder (2013)", "");
addUpdate($div_update, '2024-09-01', 0, 1, '/citizens_united_v_federal_election_commission.html', "Citizens United v. Federal Election Commission (2010)", "");
addUpdate($div_update, '2024-08-31', 0, 1, '/disfranchisement.html', "Disfranchisement", "");
addUpdate($div_update, '2024-08-30', 0, 2, '/justice.html', "Justice", "");
addUpdate($div_update, '2024-08-29', 0, 0, '/individuals.html', "Individuals", "");
addUpdate($div_update, '2024-08-29', 0, 0, '/lawyers.html', "Lawyers", "");
addUpdate($div_update, '2024-08-29', 0, 1, '/women_s_rights.html', "Women's Rights", "");
addUpdate($div_update, '2024-08-29', 0, 1, '/fatma_karume.html', "Fatma Karume", "");
addUpdate($div_update, '2024-08-29', 0, 1, '/tanzania.html', "Tanzania", "");
addUpdate($div_update, '2024-08-29', 0, 1, '/at_war_with_ourselves.html', "At War with Ourselves", " a book by Lt. Gen. H.R. McMaster.");
addUpdate($div_update, '2024-08-28', 0, 2, '/military.html', "Military", "");
addUpdate($div_update, '2024-08-28', -1, -1, 'https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy/commits/branch/master', '1100 commits!', "There are now 1100 commits in the project's code repository, with a total of 391 articles and 490 files.");
addUpdate($div_update, '2024-08-27', 0, 1, '/donald_trump.html', "Donald Trump", "");
addUpdate($div_update, '2024-08-26', 0, 1, '/institutions.html', "Institutions", "");
addUpdate($div_update, '2024-08-26', 0, 1, '/list_of_people.html', "List of People", "");
addUpdate($div_update, '2024-08-23', 2, 4, '/third_level_of_democracy.html', "Third level of democracy — We, the Professionals", "");
addUpdate($div_update, '2024-08-23', 0, 1, '/separation_of_powers.html', "Separation of powers", "");
addUpdate($div_update, '2024-08-22', 0, 2, '/branches_of_government.html', "Branches of government", "");
addUpdate($div_update, '2024-08-21', 0, 1, '/michael_luttig.html', "Judge Michael Luttig", "");
addUpdate($div_update, '2024-08-20', 0, 3, '/corruption.html', "Corruption", "");
addUpdate($div_update, '2024-08-19', 0, 3, '/criminal_justice.html', "Criminal Justice", "");
addUpdate($div_update, '2024-08-16', 0, 2, '/greg_boyle.html', "Greg Boyle", "");
addUpdate($div_update, '2024-08-16', 0, 1, '/homeboy_industries.html', "Homeboy Industries", "");
addUpdate($div_update, '2024-07-07', 0, 1, '/white_poverty_how_exposing_myths_about_race_and_class_can_reconstruct_american_democracy.html', "White Poverty: How Exposing Myths About Race and Class Can Reconstruct American Democracy", "");
addUpdate($div_update, '2024-06-11', 0, 1, '/information_wars_how_we_lost_the_global_battle_against_disinformation.html', "Information Wars: How We Lost the Global Battle Against Disinformation", "");
addUpdate($div_update, '2024-06-11', 0, 0, '/volodymyr_zelenskyy.html', "Volodymyr Zelenskyy", "Video: 'You saved Europe'.");
addUpdate($div_update, '2024-06-10', 0, 1, '/ted_talks.html', "TED talk:", "The next global superpower isn't who you think | Ian Bremmer");

show_nb_articles_by_stars($div_update, '2024-06-10', 278, 62, 20, 8, 2, 0);

addUpdate($div_update, '2024-06-09', 0, 3, '/archives/russian_federation_and_people_s_republic_of_china_joint_statement_february_2022.html', "Russian Federation and People's Republic of China joint statement, February 2022", '');
addUpdate($div_update, '2024-06-08', 0, 0, '/invoking_democracy.html', 'Preface: Invoking Democracy — We, the Autocrats', '');
addUpdate($div_update, '2024-06-04', 0, 1, '/fifth_level_of_democracy.html', 'Fifth level of democracy — We, the Media', '');
addUpdate($div_update, '2024-05-26', -1, -1, 'https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy/commits/branch/master', '1000 commits!', "There are now 1000 commits in the project's code repository, with a total of 352 articles and 408 files.");
addUpdate($div_update, '2024-05-26', 0, 1, '/national_chengchi_university.html', 'National Chengchi University (國立政治大學)', '');
addUpdate($div_update, '2024-05-23', 1, 2, '/third_level_of_democracy.html', 'Third level of democracy — We, the Professionals', '');
addUpdate($div_update, '2024-05-20', 0, 1, '/project/taiwan_democracy.html', 'Taiwan Democracy project', ', official launch.');
addUpdate($div_update, '2024-05-20', 0, 1, '/all_labor_has_dignity.html', 'All labor has dignity', '');
addUpdate($div_update, '2024-05-15', 0, 1, '/duverger_usa_19_century.html', 'Duverger example: USA, 19th century', '');
addUpdate($div_update, '2024-05-15', 0, 1, '/truth_and_reality_with_chinese_characteristics.html', 'Truth and reality with Chinese characteristics', '');
addUpdate($div_update, '2024-05-14', 3, 3, '/project/copyright.html', 'Copyright and licenses', 'Update: added copyright status of excerpts and third-party sources.');
addUpdate($div_update, '2024-05-13', 0, 0, '/duverger_syndrome_political_realignments.html', 'Duverger symptom 7: Periodical political realignments', '');
addUpdate($div_update, '2024-05-13', 0, 1, '/soochow_university.html', 'Soochow University', '');
addUpdate($div_update, '2024-05-12', 0, 0, '/duverger_usa_19_century.html', 'Duverger example: USA, 19th century', '');
addUpdate($div_update, '2024-05-11', 0, 1, '/duverger_law.html', "Duverger's Law", '');
addUpdate($div_update, '2024-05-10', 0, 0, '/plurality_voting.html', 'Plurality voting', '');
addUpdate($div_update, '2024-05-09', 0, 0, '/duverger_syndrome_extremism.html', 'Duverger symptom 6: extremism', '');
addUpdate($div_update, '2024-05-08', 0, 0, '/duverger_syndrome_party_politics.html', 'Duverger symptom 3: party politics', '');
addUpdate($div_update, '2024-05-06', 0, 1, '/duverger_syndrome_alternance.html', 'Duverger symptom 2: destructive alternance', '');
addUpdate($div_update, '2024-05-05', 0, 0, '/duverger_syndrome_duality.html', 'Duverger symptom 1: divisive dualism', '');
addUpdate($div_update, '2024-05-05', 0, 0, '/saints_and_little_devils.html', 'Democracy — Saints and Little Devils', '');
addUpdate($div_update, '2024-05-04', -1, -1, '/project/sister_projects.html', 'Sister projects', 'A new official fork and sister project will be announced on May the 20th 2024!!');
addUpdate($div_update, '2024-05-02', 0, 0, '/the_soloist_and_the_choir.html', 'Addendum B: Democracy — the Soloist and the Choir', '');
addUpdate($div_update, '2024-05-02', -1, -1, 'https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy/commits/branch/master', '900 commits!', "There are now 900 commits in the project's code repository, with a total of 311 articles and 369 files.");
addUpdate($div_update, '2024-05-01', 0, 0, '/eighth_level_of_democracy.html', 'Eighth level of democracy — We, Humans', '');
addUpdate($div_update, '2024-05-01', 0, 0, '/sixth_level_of_democracy.html', 'Sixth level of democracy — We, the Breadwinners', '');

show_nb_articles_by_stars($div_update, '2024-05-01', 229, 52, 19, 7, 2, 0);

addUpdate($div_update, '2024-04-30', 0, 0, '/fifth_level_of_democracy.html', 'Fifth level of democracy — We, the Media', '');
addUpdate($div_update, '2024-04-30', 0, 0, '/fourth_level_of_democracy.html', 'Fourth level of democracy — We, the People', '');
addUpdate($div_update, '2024-04-29', 0, 0, '/seventh_level_of_democracy.html', 'Seventh level of democracy — We, the Earthlings', '');
addUpdate($div_update, '2024-04-29', 0, 1, '/third_level_of_democracy.html', 'Third level of democracy — We, the Professionals', '');
addUpdate($div_update, '2024-03-07', 0, 0, '/tertiary_features_of_democracy.html', 'Tertiary features of democracy — We, the Professionals', '');
addUpdate($div_update, '2024-01-04', 0, 1, '/the_american_journalist_under_attack_media_trust_and_democracy.html', 'The American Journalist Under Attack: Media, Trust, and Democracy', '');
addUpdate($div_update, '2023-12-18', 0, 1, '/religion.html', 'Religion', 'Religion and democracy');
addUpdate($div_update, '2023-11-28', 0, 1, '/global_migration_data_analysis_centre.html', 'Global Migration Data Analysis Centre', '');
addUpdate($div_update, '2023-11-24', 0, 1, '/missing_migrants_project.html', 'Missing Migrants Project', '');
addUpdate($div_update, '2023-11-22', 0, 2, '/a_night_at_the_garden.html', 'A Night at the Garden', '1939 Nazi rally in New York City.');
addUpdate($div_update, '2023-11-17', 0, 1, '/duverger_taiwan_2024.html', 'Duverger example: Taiwan 2024 presidential election', '');
addUpdate($div_update, '2023-11-17', -1, -1, 'https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy/commits/branch/master', '800 commits!', "After continuous daily development, there are now 800 commits in the project's code repository, with a total of 261 articles and 317 files.");
addUpdate($div_update, '2023-11-09', 0, 1, '/ecuador.html', 'Ecuador', '');
addUpdate($div_update, '2023-11-06', 0, 1, '/fair_share.html', 'Fair share', '');
addUpdate($div_update, '2023-11-05', 0, 1, '/sharon_brous.html', 'Sharon Brous', '');
addUpdate($div_update, '2023-11-02', 0, 2, '/counterinsurgency_mathematics.html', 'Counterinsurgency mathematics', '');
addUpdate($div_update, '2023-11-01', 0, 1, '/small_government.html', 'Small government', '');
addUpdate($div_update, '2023-10-30', 0, 3, '/mass_shootings_the_human_factor.html', 'Mass shootings: the human factor', '');
addUpdate($div_update, '2023-10-29', 0, 1, '/institutional_vs_human_factors_in_democracy.html', 'Institutional vs Human Factors in Democracy', '');
addUpdate($div_update, '2023-10-27', 0, 1, '/b_lab.html', 'B Lab', 'Certifying B Corp companies.');
addUpdate($div_update, '2023-10-25', 0, 2, '/military_and_security_developments_involving_the_people_s_republic_of_china_2023_dod_report.html', 'Military and Security Developments Involving the People\'s Republic of China', '2023 DOD report');
addUpdate($div_update, '2023-10-08', 0, 1, '/deaths_of_migrants.html', 'Deaths of migrants', '');
addUpdate($div_update, '2023-10-08', 1, 2, '/immigration.html', 'Immigration', '');
addUpdate($div_update, '2023-10-07', 0, 1, '/narges_mohammadi.html', 'Narges Mohammadi', '');
addUpdate($div_update, '2023-10-05', 0, 1, '/immigration.html', 'Immigration', '');
addUpdate($div_update, '2023-09-24', -1, -1, 'https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy/commits/branch/master', '700 commits!', "After continuous daily development, there are now 700 commits in the project's code repository, with a total of 208 articles and 263 files.");
addUpdate($div_update, '2023-09-11', 0, 1, '/haiti.html', 'Haiti', '');
addUpdate($div_update, '2023-08-30', 0, 1, '/blasphemy.html', 'Blasphemy', '');
addUpdate($div_update, '2023-08-30', 0, 1, '/immigration.html', 'Immigration', '');
addUpdate($div_update, '2023-08-29', 0, 1, '/election_integrity.html', 'Election Integrity', '');
addUpdate($div_update, '2023-08-28', -1, -1, 'https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy/commits/branch/master', '600 commits!', "Development of the $Pilgrimage continues every day! There are now 600 commits in the project's code repository, with a total of 145 articles and 200 files.");
addUpdate($div_update, '2023-08-27', -1, -1, '', '', 'All API 0 pages have now fully been upgraded to API 1!');
addUpdate($div_update, '2023-08-27', 0, 1, '/clark_cunningham.html', 'Clark D. Cunningham', '');
addUpdate($div_update, '2023-08-25', 0, 1, '/obstruction_of_justice.html', 'Obstruction of justice', '');
addUpdate($div_update, '2023-08-25', 0, 1, '/mo_gawdat.html', 'Mo Gawdat', '');
addUpdate($div_update, '2023-08-25', 0, 1, '/artificial_intelligence.html', 'Artificial intelligence', '');
addUpdate($div_update, '2023-08-24', -1, -1, '', '', 'Many new pages are added, almost daily: those are stub articles and placeholders and they are rarely mentioned in these update notices. We are laying the groundwork so that adding new content and analysis on each topic will be easy.');
addUpdate($div_update, '2023-08-24', -1, -1, '', '', 'Migrating all the old pages using API 0 to API 1 is almost completed. Out of about 100 original pages, only 14 remain to be migrated.');
addUpdate($div_update, '2023-08-24', 0, 1, '/social_justice.html', 'Social justice', '');
addUpdate($div_update, '2023-08-14', 1, 2, '/chinese_expansionism.html', 'Chinese expansionism', '');
addUpdate($div_update, '2023-08-14', -1, -1, 'https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy/commits/branch/master', '500 commits!', "Development of the $Pilgrimage continues every day! There are now 500 commits in the project's code repository, with a total of 126 articles and 195 files.");
addUpdate($div_update, '2023-08-13', 0, 1, '/global_witness.html', 'Global Witness', '');
addUpdate($div_update, '2023-08-12', 0, 1, '/environmental_defenders.html', 'Environmental defenders', '');
addUpdate($div_update, '2023-08-12', 0, 0, '/colombia.html', 'Colombia', '');
addUpdate($div_update, '2023-08-12', 0, 0, '/saudi_arabia.html', 'Saudi Arabia', '');
addUpdate($div_update, '2023-08-11', 0, 0, '/ecuador.html', 'Ecuador', '');
addUpdate($div_update, '2023-08-06', 2, 2, '/transnational_authoritarianism.html', 'Transnational authoritarianism', '');
addUpdate($div_update, '2023-08-06', 0, 0, '/hong_kong.html', 'Hong Kong', '');
addUpdate($div_update, '2023-08-06', 0, 1, '/prc_china.html', "The People's Republic of China", '');
addUpdate($div_update, '2023-08-03', 0, 0, '/haiti.html', 'Haiti', '');
addUpdate($div_update, '2023-08-02', 0, 1, '/property.html', 'Property', '');
addUpdate($div_update, '2023-07-31', 1, 1, '/project/standing_on_the_shoulders_of_giants.html', 'Standing on the shoulders of giants', "Added the ${'World Future Council'} to the list of $giants.");
addUpdate($div_update, '2023-07-31', -1, -1, 'https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy/commits/branch/master', '400 commits!', "Development of the $Pilgrimage continues <strong>daily</strong>! There are now 400 commits in the project's code repository, and a total of 175 files.");
addUpdate($div_update, '2023-07-28', -1, -1, '', '', <<<HTML
	'The project development has been ongoing on a daily basis, although the result is not immediately apparent within the website.
	The early architecture of the source code was not scalable, so a lot of development time has been spent on creating the API version 1,
	which is better suited to the nature of this project.
	The API 1 is now operational and almost stable.
	Each existing page must individually be upgraded from API 0 to API 1.
	A specially crafted upgrade script helps with the process. There are over 80 pages left to upgrade.
	Once the whole website has been migrated to API 1, more time will be spent on developing the actual content of the website.
	HTML);
addUpdate($div_update, '2023-07-24', 0, 2, '/world_future_council.html', 'World Future Council', '');
addUpdate($div_update, '2023-07-22', 0, 2, '/lists.html', 'Lists', '');
addUpdate($div_update, '2023-07-15', 0, 1, '/technology_and_democracy.html', 'Technology and democracy', '');
addUpdate($div_update, '2023-07-12', 0, 1, '/data_activism.html', 'Data activism', '');
addUpdate($div_update, '2023-07-11', 0, 1, '/data_pop_alliance.html', 'Data-Pop Alliance', '');
addUpdate($div_update, '2023-07-10', -1, -1, 'https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy/commits/branch/master', '300 commits!', 'There are 300 commits in the project\'s code repository, 186 files having changed.');
addUpdate($div_update, '2023-07-10', 0, 2, '/richard_bluhm.html', 'Richard Bluhm', '');
addUpdate($div_update, '2023-07-06', -1, -1, '/lists.html', 'Lists and indices.', 'We are building up a list of resources, organizations, people, reports, etc. At a later stage we shall be able to dig into these resources in order to extract valuable and productive information.');
addUpdate($div_update, '2023-07-04', 0, 2, '/decentralized_autonomous_organization.html', 'Decentralized Autonomous Organization (DAO)', '');
addUpdate($div_update, '2023-07-03', 0, 1, '/taiwan.html', 'Taiwan', '');
addUpdate($div_update, '2023-07-02', 0, 1, '/political_discourse.html', 'Political discourse', '');
addUpdate($div_update, '2023-07-02', 0, 1, '/denise_dresser.html', 'Denise Dresser', '');
addUpdate($div_update, '2023-06-30', 0, 3, '/the_remains_of_the_day.html', 'The Remains of the Day', 'Novel and film');
addUpdate($div_update, '2023-06-29', 0, 1, '/maria_ressa.html', 'Maria Ressa', 'Journalist from the Philippines and Nobel Peace Prize laureate.');
addUpdate($div_update, '2023-06-29', 0, 2, '/project/codeberg.html', 'Codeberg', 'Participate');
addUpdate($div_update, '2023-06-28', 0, 1, '/ground_news.html', 'Ground News', '');
addUpdate($div_update, '2023-06-28', 1, 2, '/information_overload.html', 'Information overload', '');
addUpdate($div_update, '2023-06-27', 0, 1, '/information_overload.html', 'Information overload', '');
addUpdate($div_update, '2023-06-26', 0, 1, '/karoline_wiesner.html', 'Karoline Wiesner', '');
addUpdate($div_update, '2023-06-26', 0, 1, '/staffan_lindberg.html', 'Staffan Lindberg', '');
addUpdate($div_update, '2023-06-26', 0, 1, '/v_dem_institute.html', 'V-Dem Institute', '');
addUpdate($div_update, '2023-06-23', -1, -1, 'https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy/commits/branch/master', '200 commits!', 'There are 200 commits in the project\'s code repository, 137 files having changed.');
addUpdate($div_update, '2023-06-23', 0, 1, '/constitution.html', 'Constitution', '');
addUpdate($div_update, '2023-06-20', 0, 1, '/mali.html', 'Mali', '');
addUpdate($div_update, '2023-06-19', 0, 2, '/transnational_authoritarianism.html', 'Transnational authoritarianism', '');
addUpdate($div_update, '2023-06-17', 1, 2, '/freedom_house.html', 'Freedom House', 'Policy recommendations and research methodology.');
addUpdate($div_update, '2023-06-15', 0, 1, '/philippines.html', 'Philippines', '');
addUpdate($div_update, '2023-06-14', 1, 1, '/world.html', 'Country profiles', 'Added Freedom House Internet freedom profile and index to each country.');
addUpdate($div_update, '2023-06-14', 0, 1, '/professionalism_without_elitism.html', 'Professionalism without Elitism', '');
addUpdate($div_update, '2023-06-12', 0, 0, '/lawmaking.html', 'Lawmaking', 'New stub');
addUpdate($div_update, '2023-06-11', 1, 1, '/world.html', 'Country profiles', 'Added Freedom House Index to each country.');
addUpdate($div_update, '2023-06-10', 0, 1, '/world.html', 'Country profiles', 'Added Freedom House country profiles to each country.');
addUpdate($div_update, '2023-06-08', 0, 1, '/beliefs.html', 'Beliefs', '');
addUpdate($div_update, '2023-06-07', 0, 1, '/freedom_house.html', 'Freedom House', '');
addUpdate($div_update, '2023-06-06', 3, 4, '/secondary_features_of_democracy.html', 'Secondary features of democracy — We, the Society', 'minor update');
addUpdate($div_update, '2023-06-05', 0, 1, '/project/standing_on_the_shoulders_of_giants.html', 'Standing on the shoulders of giants', '');
addUpdate($div_update, '2023-06-04', 1, 2, '/project/wikipedia.html', 'Wikipedia', 'Similarities and differences.');
addUpdate($div_update, '2023-06-02', 0, 2, '/ukraine.html', 'Ukraine', '');
addUpdate($div_update, '2023-06-01', 0, 1, '/duverger_syndrome.html', 'Duverger syndrome', 'Section outline');
addUpdate($div_update, '2023-05-31', 0, 1, '/project/wikipedia.html', 'Wikipedia', 'Similarities and differences.');
addUpdate($div_update, '2023-05-30', 0, 1, '/peaceful_resistance_in_times_of_war.html', 'Peaceful resistance in times of war', '');
addUpdate($div_update, '2023-05-29', 0, 0, '/global_issues.html', 'Global issues', '');
addUpdate($div_update, '2023-05-29', 0, 1, '/hunger.html', 'Hunger', '');
addUpdate($div_update, '2023-05-24', 0, 2, '/living_together.html', 'Living Together', '');
addUpdate($div_update, '2023-05-23', 1, 3, '/democracy_wip.html', 'Democracy — a work in progress', '');
addUpdate($div_update, '2023-05-22', 0, 1, '/democracy_wip.html', 'Democracy — a work in progress', '');
addUpdate($div_update, '2023-05-12', 0, 1, '/costa_rica.html', 'Costa Rica', '');
addUpdate($div_update, '2023-05-11', 0, 3, '/secondary_features_of_democracy.html', 'Secondary features of democracy — We, the Society', '');
addUpdate($div_update, '2023-04-15', 0, 1, '/reporters_without_borders.html', 'Reporters Without Borders', '');
addUpdate($div_update, '2023-04-12', 0, 1, '/media.html', 'Media', '');
addUpdate($div_update, '2023-04-12', 0, 2, '/foreign_influence_local_media.html', 'Foreign influence in local media landscape', '');
addUpdate($div_update, '2023-04-11', 0, 1, '/russia.html', 'Russia', '');
addUpdate($div_update, '2023-04-08', 0, 1, '/project/development_website.html', 'Development website', 'How to set up a copy of this website on your computer.');
addUpdate($div_update, '2023-04-07', 0, 1, '/project/git.html', 'Git', 'Using git to contribute to the project.');
addUpdate($div_update, '2023-03-13', 0, 1, '/georgia.html', 'Georgia', '');
addUpdate($div_update, '2023-01-28', 0, 0, '/osce.html', 'OSCE (Organization for Security and Co-operation in Europe)', '');
addUpdate($div_update, '2023-01-27', 0, 2, '/united_nations.html', 'The United Nations', '');
addUpdate($div_update, '2023-01-22', 0, 1, '/iran.html', 'Iran', 'Start with resources from wikipedia.');
addUpdate($div_update, '2022-12-26', 0, 1, '/chinese_expansionism.html', 'Chinese expansionism', '');
addUpdate($div_update, '2022-12-12', 1, 3, '/project/participate.html', 'Participate', 'Content');
addUpdate($div_update, '2022-12-08', 1, 3, '/primary_features_of_democracy.html', 'Primary features of democracy', '');
addUpdate($div_update, '2022-11-26', 0, 0, '/justice.html', 'Justice', '');
addUpdate($div_update, '2022-11-25', 0, 0, '/media.html', 'Media', '');
addUpdate($div_update, '2022-11-25', 0, 1, '/corruption.html', 'Corruption', '');
addUpdate($div_update, '2022-11-23', 0, 1, '/fair_share.html', 'Fair share', '');
addUpdate($div_update, '2022-11-20', 0, 1, '/integrity.html', 'Election integrity', '');
addUpdate($div_update, '2022-11-14', 0, 1, '/primary_features_of_democracy.html', 'Primary features of democracy', '');
addUpdate($div_update, '2022-11-13', 0, 1, '/project/participate.html', 'Participate', '');
addUpdate($div_update, '2022-11-12', 0, 3, '/project/copyright.html', 'Copyright?', '');
addUpdate($div_update, '2022-11-11', 0, 1, '/project/updates.html', 'Updates');
addUpdate($div_update, '2022-11-09', 0, 1, '/index.html', 'Front page');
addUpdate($div_update, '2022-11-05', -1, -1, '', '', 'Beginning of the project. Resgistration of the domain pildem.org.');

$div_update->content .= "</ul>";




$page->parent('project/index.html');
$page->body($h2_code);
$page->body($div_code);

$page->body($h2_content);
$page->body($div_stars);
$page->body($div_update);

