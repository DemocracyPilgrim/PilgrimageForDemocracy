<?php
$page = new Page();
$page->h1("Taiwan Democracy");
$page->keywords("Taiwan Democracy");
$page->stars(1);

//$page->snp("description", "");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p><strong>Taiwan Democracy</strong> is the first sister project to the $Pilgrimage.
	It was launched on May 20th 2024.</p>
	HTML );

// $r1 = $page->ref('', '');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p><strong>Taiwan Democracy</strong> is the first sister project to the $Pilgrimage.
	It was launched on May 20th 2024.</p>

	<p>$Taiwan (${'Republic of China'}), ranked the 10th democracy in the world,
	has made a remarkable, peaceful transition from $authoritarianism to $democracy.
	With only 12 $countries around the world having official democratic relationship,
	Taiwan's situation in the world is unique and unprecedented:
	it is at the same time a celebrated democracy and yet isolated,
	while facing existential threats from the ${"People's Republic of China"}.</p>

	HTML;


$div_codeberg_TaiwanDemocracy_Codeberg = new CodebergContentSection();
$div_codeberg_TaiwanDemocracy_Codeberg->setTitleText("TaiwanDemocracy at Codeberg");
$div_codeberg_TaiwanDemocracy_Codeberg->setTitleLink("https://codeberg.org/DemocracyPilgrim/TaiwanDemocracy");
$div_codeberg_TaiwanDemocracy_Codeberg->content = <<<HTML
	<p>The official source code repository for the Taiwan Democracy project is hosted at Codeberg.</p>
	HTML;




$div_Taiwan_Democracy_website = new WebsiteContentSection();
$div_Taiwan_Democracy_website->setTitleText("Taiwan Democracy website ");
$div_Taiwan_Democracy_website->setTitleLink("https://taiwandemocracy.tw/");
$div_Taiwan_Democracy_website->content = <<<HTML
	<p>Taiwan Democracy: past, present and future:
	challenges and priorities to protect and deepen democracy in Taiwan.</p>
	HTML;




$page->parent('project/index.html');
$page->parent('project/sister_projects.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_codeberg_TaiwanDemocracy_Codeberg);
$page->body($div_Taiwan_Democracy_website);
