<?php
$page = new Page();
$page->h1('development scripts');
$page->stars(0);
$page->keywords('scripts');

//$page->snp('description', "");
//$page->snp('image', "/copyrighted/");


$page->preview( <<<HTML
	<p>A variety of scripts are used to aid during the development.</p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>A variety of scripts are used to aid during the development.</p>
	HTML;


$div_new_article = new ContentSection();
$div_new_article->content = <<<HTML
	<h3>New article</h3>

	<p>Running the following script and answering simple questions will automatically do the initial set up for a new page:</p>

	<code>PilgrimageForDemocracy  $ ./bin/new.article.php
	</code>
	HTML;


$div_New_section = new ContentSection();
$div_New_section->content = <<<HTML
	<h3>New section</h3>

	<p>...</p>

	<code>PilgrimageForDemocracy  $ ./bin/new.section.php
	</code>
	HTML;



$div_codeberg_Porting_scripts_for_Windows = new CodebergContentSection();
$div_codeberg_Porting_scripts_for_Windows->setTitleText('Porting scripts for Windows');
$div_codeberg_Porting_scripts_for_Windows->setTitleLink('https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy/issues/39');
$div_codeberg_Porting_scripts_for_Windows->content = <<<HTML
	<p>The scripts work well under Linux. They need to be ported and tested for Windows.</p>
	HTML;



$page->parent('project/index.html');

$page->body($div_introduction);
$page->body($div_codeberg_Porting_scripts_for_Windows);
$page->body($div_new_article);
$page->body($div_New_section);
