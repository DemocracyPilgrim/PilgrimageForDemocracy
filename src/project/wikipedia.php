<?php
$page = new Page();
$page->h1('Wikipedia');
$page->stars(2);
$page->keywords('Wikipedia');

$page->preview( <<<HTML
	<p>Discusses some differences and similarities between the Wikipedia project and the Pilgrimage for Democracy and Social Justice project.</p>
	HTML );

$page->snp('description', "Differences and similarities with the Wikipedia project.");
$page->snp('image', "/copyrighted/oberon-copeland-veryinformed-com-EtCxIuaG-zU.1200-630.jpg");


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Many pages in this project prominently link to Wikipedia articles.
	Below, we shall discuss some of the differences and some of the similarities
	between the <strong>Wikipedia</strong> project and the <strong>Pilgrimage for Democracy and Social Justice</strong> project.</p>
	HTML;


$h2_Differences = new h2HeaderContent('Differences');


$div_Goals = new ContentSection();
$div_Goals->content = <<<HTML
	<h3>Goals</h3>

	<p>Wikipedia is an encyclopedia of all human knowledge, with a goal to democratize knowledge.
	It has no direct stated goal beyond aggregating knowledge into its website.</p>

	<p>Our goal is to further peace, democracy and social justice.
	We aim to research solutions to known problems, educate people about known solutions,
	until, hopefully one day, those solutions are put into practice for the benefit of all.</p>

	<p>This project is our humble contribution to facilitate change in our society.
	The fact is that, as we can witness any day by watching the news, there is a lot of suffering and suffering throughout the world.
	Many causes are understood; many solutions are known.
	So, why are the things still the same?
	Because of this observation, a small project like ours can have its usefulness.</p>
	HTML;



$div_Original_research = new ContentSection();
$div_Original_research->content = <<<HTML
	<h3>Original research</h3>

	<p>Our project will thrive on original research.
	We have stated our points of view.
	One of our primary, albeit long term, goal is to contribute making our society a better place.
	Original research is part of our strategy towards that goal.</p>
	HTML;

$div_Scope = new ContentSection();
$div_Scope->content = <<<HTML
	<h3>Scope</h3>

	<p>Wikipedia has no fixed scope.
	It covers all areas of human knowledge,
	including the most frivolous, superficial and sometimes harmful to human development.</p>

	<p>Our starting points are: democracy and social justice,
	or, generally speaking, making making our global society a better society.
	This general objective provides a compass, something to aim for,
	but on our journey we shall cover a very wide variety of topics.</p>
	HTML;



$div_Point_of_view = new ContentSection();
$div_Point_of_view->content = <<<HTML
	<h3>Point of view</h3>

	<p>Wikipedia has a strong Neutral point of view policy
	while we take a strong stance on our primary topics of interest:
	democracy is good, but our democracies are flawed,
	every human being matter, etc.</p>
	HTML;



$div_Nature_and_style_of_content = new ContentSection();
$div_Nature_and_style_of_content->content = <<<HTML
	<h3>Nature and style of content</h3>

	<p>For obvious reasons, each Wikipedia article is encyclopedic in nature, with a writing style to match.</p>

	<p>Our content can be multi-faceted, with educational articles, inspiring stories, academic content,
	technical details on policy, videos, etc. See <a href="/project/participate.html">how to contribute</a>
	some of you own content.</p>
	HTML;


$h2_Similarities = new h2HeaderContent('Similarities');

$div_Reliable_sources = new ContentSection();
$div_Reliable_sources->content = <<<HTML
	<h3>Reliable sources</h3>

	<p>The information we present must be factual and based on reliable sources.
	The Wikipedia "Reliable sources" policy and guidelines can serve as a good basis for contributions to this project.</p>
	HTML;




$div_wikipedia_Wikipedia_No_original_research = new WikipediaContentSection();
$div_wikipedia_Wikipedia_No_original_research->setTitleText('Wikipedia:No original research');
$div_wikipedia_Wikipedia_No_original_research->setTitleLink('https://en.wikipedia.org/wiki/Wikipedia:No_original_research');
$div_wikipedia_Wikipedia_No_original_research->content = <<<HTML
	<p>Wikipedia articles must not contain original research.</p>
	HTML;

$div_wikipedia_Wikipedia_Reliable_sources = new WikipediaContentSection();
$div_wikipedia_Wikipedia_Reliable_sources->setTitleText('Wikipedia:Reliable sources');
$div_wikipedia_Wikipedia_Reliable_sources->setTitleLink('https://en.wikipedia.org/wiki/Wikipedia:Reliable_sources');
$div_wikipedia_Wikipedia_Reliable_sources->content = <<<HTML
	<p>Articles should be based on reliable, independent, published sources
	with a reputation for fact-checking and accuracy.</p>
	HTML;

$div_wikipedia_Wikipedia_Neutral_point_of_view = new WikipediaContentSection();
$div_wikipedia_Wikipedia_Neutral_point_of_view->setTitleText('Wikipedia:Neutral point of view');
$div_wikipedia_Wikipedia_Neutral_point_of_view->setTitleLink('https://en.wikipedia.org/wiki/Wikipedia:Neutral_point_of_view');
$div_wikipedia_Wikipedia_Neutral_point_of_view->content = <<<HTML
	<p>All encyclopedic content on Wikipedia must be written from a neutral point of view (NPOV),
	which means representing fairly, proportionately, and, as far as possible, without editorial bias,
	all the significant views that have been published by reliable sources on a topic.</p>
	HTML;


$div_wikipedia_Wikipedia = new WikipediaContentSection();
$div_wikipedia_Wikipedia->setTitleText('Wikipedia');
$div_wikipedia_Wikipedia->setTitleLink('https://en.wikipedia.org/wiki/Wikipedia');
$div_wikipedia_Wikipedia->content = <<<HTML
	<p>Wikipedia is a multilingual crowdsourced online encyclopedia.
	The content on Wikipedia is available without charge and is distributed under free content licenses,
	allowing for widespread use and furthering its goal of democratizing knowledge.</p>
	HTML;



$page->parent('project/index.html');
$page->body($div_introduction);




$page->body($h2_Differences);

$page->body($div_Goals);
$page->body($div_wikipedia_Wikipedia);

$page->body($div_Scope);

$page->body($div_Nature_and_style_of_content);

$page->body($div_Point_of_view);
$page->body($div_wikipedia_Wikipedia_Neutral_point_of_view);

$page->body($div_Original_research);
$page->body($div_wikipedia_Wikipedia_No_original_research);



$page->body($h2_Similarities);

$page->body($div_Reliable_sources);
$page->body($div_wikipedia_Wikipedia_Reliable_sources);

$page->body('project/standing_on_the_shoulders_of_giants.html');
