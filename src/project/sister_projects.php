<?php
$page = new Page();
$page->h1("Sister projects");
$page->stars(1);

//$page->snp("description", "");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p>The first sister project was launched on the 20th May 2024.</p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>The first sister project was launched on the 20th May 2024.</p>
	HTML;



$h2_Official_sister_projects = new h2HeaderContent("Official sister projects");


$div_official_sister_projects = new ContentSection();
$div_official_sister_projects->content = <<<HTML
	<p>The $Pilgrimage and its team of contributors maintain official sister projects.
	The projects are set up to cover a given country in more details
	and in the local language.</p>
	HTML;



$page->parent('project/index.html');

$page->body($div_introduction);


$page->body($h2_Official_sister_projects);
$page->body($div_official_sister_projects);
$page->body('project/taiwan_democracy.html');
