<?php
$page = new Page();
$page->h1("Videos, TV programs, interviews and documentaries");
$page->tags("Project", "Information: Media");
$page->stars(1);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>In accordance with our policy of ${"standing on the shoulders of giants"},
	the website features many videos, movies, documentaries, interviews, TV programs and important news clips.</p>

	<p>As the project grows, this page will attempt to provide guidelines on what types of videos may deem acceptable for inclusion.
	It will also include formatting and titling guidelines.</p>
	HTML;


$div_Criteria_for_inclusion = new ContentSection();
$div_Criteria_for_inclusion->content = <<<HTML
	<h3>Criteria for inclusion</h3>

	<p>Generally speaking, only include high quality, professionally produced videos, interviews, short documentaries
	from trustworthy sources and that help advance in a compelling way the mission of the $Pilgrimage.</p>
	HTML;


$div_Video_source = new ContentSection();
$div_Video_source->content = <<<HTML
	<h3>Video source</h3>

	<p>As a rule, prefer to link to the video on the producers' own websites rather than to third-party distributors like YouTube.</p>
	HTML;


$div_Title = new ContentSection();
$div_Title->content = <<<HTML
	<h3>Title</h3>

	<p>Generally speaking, keep the title of the original producer.
	The title may be prefixed by the producer's name or the name of the TV program.</p>

	<p>However, pay attention when including a video linking straight to third-party distributors like YouTube,
	where titles are often partly misleading, not descriptive enough of the actual content of the video, and worded as click-baits.
	In such case, consider rewording the title to better direct the readers of this website.</p>
	HTML;


$div_Feature_length_and_short_videos = new ContentSection();
$div_Feature_length_and_short_videos->content = <<<HTML
	<h3>Feature length and short videos</h3>

	<p>Feature films are usually over 1 hour in length, and are listed in the ${'list of movies'}.</p>

	<p>Feature documentaries are usually over 40 minutes in length, and are listed in the ${'list of documentaries'}.</p>

	<p>Interviews (regardless of length), TV events (regardless of length),
	as well as shorter TV documentaries, noteworthy news clips and other videos
	are listed in the 'list of videos'.</p>
	HTML;




$list_lists_of_videos_and_movies = new ListOfPages();
$list_lists_of_videos_and_movies->add('list_of_movies.html');
$list_lists_of_videos_and_movies->add('list_of_documentaries.html');
$list_lists_of_videos_and_movies->add('list_of_videos.html');
$print_list_lists_of_videos_and_movies = $list_lists_of_videos_and_movies->print();

$div_list_lists_of_videos_and_movies = new ContentSection();
$div_list_lists_of_videos_and_movies->content = <<<HTML
	<h3>Related content</h3>

	$print_list_lists_of_videos_and_movies
	HTML;



$page->parent('project/index.html');
$page->body($div_introduction);

$page->body($div_Criteria_for_inclusion);
$page->body($div_Video_source);
$page->body($div_Title);
$page->body($div_Feature_length_and_short_videos);

$page->body($div_list_lists_of_videos_and_movies);
$page->body('project/standing_on_the_shoulders_of_giants.html');
