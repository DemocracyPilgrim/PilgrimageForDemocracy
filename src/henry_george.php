<?php
$page = new PersonPage();
$page->h1("Henry George");
$page->alpha_sort("George, Henry");
$page->keywords("Henry George");
$page->stars(0);
$page->tags("Person", "Taxes", "Organic Taxes", "Land Value Tax");

//$page->snp("description", "");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Henry_George = new WikipediaContentSection();
$div_wikipedia_Henry_George->setTitleText("Henry George");
$div_wikipedia_Henry_George->setTitleLink("https://en.wikipedia.org/wiki/Henry_George");
$div_wikipedia_Henry_George->content = <<<HTML
	<p>Henry George (September 2, 1839 – October 29, 1897) was an American political economist and journalist.
	His writing was immensely popular in 19th-century America and sparked several reform movements of the Progressive Era.
	He inspired the economic philosophy known as Georgism, the belief that people should own the value they produce themselves,
	but that the economic value of land (including natural resources) should belong equally to all members of society.
	George famously argued that a single tax on land values would create a more productive and just society.</p>
	HTML;


$page->parent('list_of_people.html');
$page->template("stub");
$page->body($div_introduction);


$page->body('land_value_tax.html');
$page->body('progress_and_poverty.html');
$page->body($div_wikipedia_Henry_George);
