<?php
$page = new Page();
$page->h1("Loving v. Virginia (1967)");
//$page->alpha_sort("Loving v. Virginia");
$page->tags("Institutions: Judiciary", "SCOTUS Landmark Decision", "USA", "Marriage");
$page->keywords("Loving v. Virginia");
$page->stars(1);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>"Loving v. Virginia" is a 1967 landmark decision by the ${'Supreme Court of the United States'}.</p>

	<p>This decision struck down laws prohibiting interracial marriage,
	recognizing that bans on such marriages violated the Fourteenth Amendment's Equal Protection Clause.</p>

	<p>This ruling was a major victory for civil rights and racial equality.
	It recognized the fundamental right to marry whomever one chooses, regardless of race.</p>
	</p>
	HTML;

$div_wikipedia_Loving_v_Virginia = new WikipediaContentSection();
$div_wikipedia_Loving_v_Virginia->setTitleText("Loving v Virginia");
$div_wikipedia_Loving_v_Virginia->setTitleLink("https://en.wikipedia.org/wiki/Loving_v._Virginia");
$div_wikipedia_Loving_v_Virginia->content = <<<HTML
	<p>Loving v. Virginia, 388 U.S. 1 (1967), was a landmark civil rights decision of the U.S. Supreme Court
	which ruled that laws banning interracial marriage violate the Equal Protection and Due Process Clauses of the Fourteenth Amendment to the U.S. Constitution.
	Beginning in 2013, the decision was cited as precedent in U.S. federal court decisions
	ruling that restrictions on same-sex marriage in the United States were unconstitutional, including in the Supreme Court decision Obergefell v. Hodges (2015).</p>
	HTML;


$page->parent('supreme_court_of_the_united_states.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_wikipedia_Loving_v_Virginia);
