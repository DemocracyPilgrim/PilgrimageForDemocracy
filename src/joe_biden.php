<?php
$page = new Page();
$page->h1("Joe Biden");
$page->alpha_sort("Biden, Joe");
$page->tags("Person", "USA", "POTUS");
$page->keywords("Joe Biden");
$page->stars(0);
$page->viewport_background('/free/joe_biden.png');

$page->snp("description", "46th president of the United States.");
$page->snp("image",       "/free/joe_biden.1200-630.png");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref('', '');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Joe Biden is the 46th president of the ${'United States'}.</p>

	<p>Biden contributed to the cause of $democracy both abroad and at home.
	He steadfastly supported $Ukraine during $Russia's full scale invasion.
	He also supports $Taiwan as it faces threats from the ${"People's Republic of China"}.
	Within the United States, Biden represents the continuity of democracy,
	as the $country democratic institutions face existential threats from $Trumpism and the $MAGA movement.
	Biden appears in the documentary about the January 6 2021 attack on the Capitol, ${"The Sixth"}.</p>
	</p>

	<p>With regard to ${'social justice'}, Joe Biden has constantly opposed
	the Republican (and $Trump's) tax cuts for the wealthiest people in the country.
	Biden supports workers' right and he has been endorsed by labor unions.</p>
	HTML;

$div_wikipedia_Joe_Biden = new WikipediaContentSection();
$div_wikipedia_Joe_Biden->setTitleText("Joe Biden");
$div_wikipedia_Joe_Biden->setTitleLink("https://en.wikipedia.org/wiki/Joe_Biden");
$div_wikipedia_Joe_Biden->content = <<<HTML
	<p>Joseph Robinette Biden Jr. (born November 20, 1942) is an American politician
	who is the 46th and current president of the United States since 2021.
	A member of the Democratic Party, he previously served as the 47th vice president from 2009 to 2017 under President Barack Obama
	and represented Delaware in the United States Senate from 1973 to 2009.</p>
	HTML;


$page->parent('list_of_people.html');
$page->parent('united_states.html');
$page->template("stub");
$page->body($div_introduction);
$page->related_tag("Joe Biden");


$page->body($div_wikipedia_Joe_Biden);
