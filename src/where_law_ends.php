<?php
$page = new Page();
$page->h1("Where Law Ends: Inside the Mueller Investigation");
$page->tags("Book", "USA", "Russia", "Justice", "Donald Trump");
$page->keywords("Where Law Ends", "Where Law Ends: Inside the Mueller Investigation");
$page->stars(0);
$page->viewport_background("/free/where_law_ends.png");

$page->snp("description", "Insider view of the investigation of the Russian interference in the 2016 US presidential election.");
$page->snp("image",       "/free/where_law_ends.1200-630.png");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Where Law Ends: Inside the Mueller Investigation is a best-selling non-fiction book written by ${'Andrew Weissmann'},
	Released in 2020, the book gives an insider's view into the $US Department of Justice special counsel Robert Mueller's
	highly controversial investigation of $Russian interference in the 2016 presidential election of ${'Donald Trump'}.</p>
	HTML;

$div_wikipedia_Where_Law_Ends = new WikipediaContentSection();
$div_wikipedia_Where_Law_Ends->setTitleText("Where Law Ends");
$div_wikipedia_Where_Law_Ends->setTitleLink("https://en.wikipedia.org/wiki/Where_Law_Ends");
$div_wikipedia_Where_Law_Ends->content = <<<HTML
	<p>Where Law Ends: Inside the Mueller Investigation is a best-selling non-fiction book written by Andrew Weissmann,
	a former Assistant United States Attorney (AUSA), and later a General Counsel of the Federal Bureau of Investigation from 2011 to 2013.
	Released by Random House on September 29, 2020,
	the widely read book gives an insider's view into Department of Justice special counsel Robert Mueller's
	highly controversial investigation of Russian interference in the 2016 presidential election of Donald Trump.</p>
	HTML;


$page->parent('list_of_books.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_wikipedia_Where_Law_Ends);
