<?php
$page = new Page();
$page->h1('Freedom of conscience');
$page->keywords('Freedom of conscience', 'freedom of conscience');
$page->tags("Individual: Liberties", "Religion");
$page->stars(0);

//$page->snp('description', '');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Freedom of conscience is one of the most basic $freedom
	and part of the ${'first level of democracy'}.</p>
	HTML;

$div_wikipedia_Freedom_of_thought = new WikipediaContentSection();
$div_wikipedia_Freedom_of_thought->setTitleText('Freedom of thought');
$div_wikipedia_Freedom_of_thought->setTitleLink('https://en.wikipedia.org/wiki/Freedom_of_thought');
$div_wikipedia_Freedom_of_thought->content = <<<HTML
	<p>Freedom of thought (also called freedom of conscience) is the freedom of an individual
	to hold or consider a fact, viewpoint, or thought, independent of others' viewpoints.</p>
	HTML;


$page->parent('individuals.html');
$page->template("stub");
$page->body($div_introduction);

$page->body('religion.html');

$page->body($div_wikipedia_Freedom_of_thought);
