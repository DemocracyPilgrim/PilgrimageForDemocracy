<?php
$page = new Page();
$page->h1("David L. Dill");
$page->alpha_sort("Dill, David");
$page->tags("Person", "Elections", "Election Integrity", "Open-source Voting Systems");
$page->keywords("David Dill");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>David L. Dill is the founder of ${'Verified Voting Foundation'}.
	He authored the "Resolution on Electronic Voting", which calls for a voter-verifiable audit trail on all voting equipment.</p>
	HTML;

$div_wikipedia_David_L_Dill = new WikipediaContentSection();
$div_wikipedia_David_L_Dill->setTitleText("David L Dill");
$div_wikipedia_David_L_Dill->setTitleLink("https://en.wikipedia.org/wiki/David_L._Dill");
$div_wikipedia_David_L_Dill->content = <<<HTML
	<p>David Lansing Dill (born January 8, 1957) is a computer scientist and academic
	noted for contributions to formal verification, electronic voting security, and computational systems biology.</p>

	<p>In 2013, Dill was elected as a member into the National Academy of Engineering
	for the development of techniques to verify hardware, software, and electronic voting systems.</p>
	</p>
	HTML;


$page->parent('list_of_people.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_wikipedia_David_L_Dill);
