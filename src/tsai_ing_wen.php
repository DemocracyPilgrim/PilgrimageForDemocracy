<?php
$page = new Page();
$page->h1("Tsai Ing-wen (蔡英文)");
$page->alpha_sort("Tsai, Ing-wen");
$page->tags("Person", "Taiwan");
$page->keywords("Tsai Ing-wen");
$page->stars(0);

$page->snp("description", "Former president of the Republic of China (Taiwan).");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p>Tsai Ing-wen is a Taiwanese politician who served as the 7th president of the Republic of China (Taiwan) from 2016 to 2024.</p>
	HTML );

// $r1 = $page->ref('', '');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Tsai Ing-wen is a Taiwanese politician who served as the 7th president of the ${'Republic of China'} ($Taiwan) from 2016 to 2024.</p>
	HTML;



$div_youtube_BBC_interview_with_Tsai_Ing_wen = new YoutubeContentSection();
$div_youtube_BBC_interview_with_Tsai_Ing_wen->setTitleText("Taiwan President Tsai Ing-wen on her legacy, China and the future | BBC News");
$div_youtube_BBC_interview_with_Tsai_Ing_wen->setTitleLink("https://www.youtube.com/watch?v=ngqJaek_GoQ");
$div_youtube_BBC_interview_with_Tsai_Ing_wen->content = <<<HTML
	<p>Interview in English with Chinese subtitles.</p>
	HTML;



$div_wikipedia_Tsai_Ing_wen = new WikipediaContentSection();
$div_wikipedia_Tsai_Ing_wen->setTitleText("Tsai Ing wen");
$div_wikipedia_Tsai_Ing_wen->setTitleLink("https://en.wikipedia.org/wiki/Tsai_Ing-wen");
$div_wikipedia_Tsai_Ing_wen->content = <<<HTML
	<p>Tsai Ing-wen (Chinese: 蔡英文; born 31 August 1956) is a Taiwanese politician
	who served as the 7th president of the Republic of China (Taiwan) from 2016 to 2024,
	and was the first woman to hold that position.
	A member of the Democratic Progressive Party (DPP),
	she intermittently served as chair of the DPP from 2008 to 2012, 2014 to 2018, and 2020 to 2022.</p>
	HTML;


$page->parent('list_of_people.html');
$page->parent('taiwan.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_youtube_BBC_interview_with_Tsai_Ing_wen);
$page->body($div_wikipedia_Tsai_Ing_wen);
