<?php
$page = new OrganisationPage();
$page->h1('B Lab');
$page->keywords('B Lab', 'B Corp');
$page->stars(1);
$page->tags("Organisation", "Fair Share");

$page->snp('description', 'Certifying corporations beneficial for society.');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p>B Lab is a non-profit organisation providing "B Corp" certification to for-profit companies,
	where "B" stands for "beneficial" to all stakeholders.</p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>B Lab is a non-profit organisation providing "B Corp" certification to for-profit companies,
	where "B" stands for "beneficial" to all stakeholders.</p>
	HTML;



$div_codeberg_B_Lab_how_do_they_rate_B_Corp_companies = new CodebergContentSection();
$div_codeberg_B_Lab_how_do_they_rate_B_Corp_companies->setTitleText('B Lab: how do they rate B Corp companies?');
$div_codeberg_B_Lab_how_do_they_rate_B_Corp_companies->setTitleLink('https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy/issues/48');
$div_codeberg_B_Lab_how_do_they_rate_B_Corp_companies->content = <<<HTML
	<p>It would be interesting to get more information on the criteria that B Lab uses to rate B Corp companies, and extract interesting key points...</p>
	HTML;


$div_B_Corporation_website = new WebsiteContentSection();
$div_B_Corporation_website->setTitleText('B Corporation website ');
$div_B_Corporation_website->setTitleLink('https://www.bcorporation.net/en-us/');
$div_B_Corporation_website->content = <<<HTML
	<p>"B Lab is the nonprofit network transforming the global economy to benefit all people, communities, and the planet."</p>

	<p>"We won't stop until all business is a force for good."</p>
	HTML;



$div_Stakeholder_Governance = new WebsiteContentSection();
$div_Stakeholder_Governance->setTitleText('Stakeholder Governance');
$div_Stakeholder_Governance->setTitleLink('https://www.bcorporation.net/en-us/movement/stakeholder-governance/');
$div_Stakeholder_Governance->content = <<<HTML
	<p>Operating under a doctrine known as shareholder primacy, companies
	are able to prioritize profits — even when those profits are derived from behaviors that create inequality, environmental damage, and social fragmentation.</p>

	<p>The failure of shareholder primacy is increasingly acknowledged by business and finance leaders around the world.
	Many are now calling for a shift to corporate governance that prioritizes all stakeholders, commonly known as stakeholder governance or benefit governance.
	This kind of corporate governance ensures that companies are required to consider the interest
	of all of their stakeholders — customers, workers, suppliers, communities, investors, and the environment — in their decision making.
	Put simply: stakeholder governance ensures we have better businesses that are accountable to people and planet.</p>
	HTML;


$div_wikipedia_B_Lab = new WikipediaContentSection();
$div_wikipedia_B_Lab->setTitleText('B Lab');
$div_wikipedia_B_Lab->setTitleLink('https://en.wikipedia.org/wiki/B_Lab');
$div_wikipedia_B_Lab->content = <<<HTML
	<p>B Lab is a non-profit organization that was founded in 2006 in Berwyn, Pennsylvania.
	B Lab created, and awards, the B corporation certification for for-profit organizations.
	The "B" stands for beneficial and indicates that the certified organizations voluntarily meet certain standards of
	transparency, accountability, sustainability, and performance, with an aim to create value for society,
	not just for traditional stakeholders such as the shareholders.</p>
	HTML;

$div_wikipedia_B_Corporation_certification = new WikipediaContentSection();
$div_wikipedia_B_Corporation_certification->setTitleText('B Corporation certification');
$div_wikipedia_B_Corporation_certification->setTitleLink('https://en.wikipedia.org/wiki/B_Corporation_(certification)');
$div_wikipedia_B_Corporation_certification->content = <<<HTML
	<p>In business, a B Corporation (also B Corp) is a for-profit corporation certified by B Lab for its social impact.
	B Corp certification is conferred by B Lab, a global nonprofit organization.
	To be granted and to maintain certification, companies must receive a minimum score of 80
	from an assessment of its social and environmental performance, integrate B Corp commitments to stakeholders into company governing documents,
	and pay an annual fee based on annual sales.
	Companies must re-certify every three years to retain B Corporation status.
	As of August 2023, there are 7,351 certified B Corporations across 161 industries in 92 countries.</p>
	HTML;


$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);

$page->body($div_codeberg_B_Lab_how_do_they_rate_B_Corp_companies);
$page->body('fair_share.html');

$page->body($div_B_Corporation_website);
$page->body($div_Stakeholder_Governance);

$page->body($div_wikipedia_B_Lab);
$page->body($div_wikipedia_B_Corporation_certification);
