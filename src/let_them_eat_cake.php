<?php
$page = new Page();
$page->h1("Let them eat cake");
$page->tags("Living: Fair Income", "Economic Injustice");
$page->keywords("Let them eat cake", "Let them eat croissant");
$page->stars(0);
$page->viewport_background("/free/let_them_eat_cake.png");

$page->snp("description", '"Or better yet: let them eat croissant!"');
$page->snp("image",       "/free/let_them_eat_cake.1200-630.png");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Let_them_eat_cake = new WikipediaContentSection();
$div_wikipedia_Let_them_eat_cake->setTitleText("Let them eat cake");
$div_wikipedia_Let_them_eat_cake->setTitleLink("https://en.wikipedia.org/wiki/Let_them_eat_cake");
$div_wikipedia_Let_them_eat_cake->content = <<<HTML
	<p>"Let them eat cake" is the traditional translation of the French phrase "Qu'ils mangent de la brioche", said to have been spoken in the 18th century by "a great princess" upon being told that the peasants had no bread. The phrase "let them eat cake" is conventionally attributed to Marie Antoinette, although there is no evidence that she ever uttered it, and it is now generally regarded as a journalistic cliché. The French phrase mentions brioche, a bread enriched with butter and eggs, considered a luxury food. The quote is taken to reflect either the princess's frivolous disregard for the starving peasants or her poor understanding of their plight.</p>
	HTML;


$page->parent('economic_injustice.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Let them eat cake");
$page->body($div_wikipedia_Let_them_eat_cake);
