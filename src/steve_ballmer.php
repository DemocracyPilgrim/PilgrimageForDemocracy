<?php
$page = new PersonPage();
$page->h1("Steve Ballmer");
$page->alpha_sort("Ballmer, Steve");
$page->tags("Person");
$page->keywords("Steve Ballmer");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Steve_Ballmer = new WikipediaContentSection();
$div_wikipedia_Steve_Ballmer->setTitleText("Steve Ballmer");
$div_wikipedia_Steve_Ballmer->setTitleLink("https://en.wikipedia.org/wiki/Steve_Ballmer");
$div_wikipedia_Steve_Ballmer->content = <<<HTML
	<p>Steven Anthony Ballmer (/ˈbɔːlmər/; March 24, 1956) is an American businessman and investor who was the chief executive officer of Microsoft from 2000 to 2014. He is the owner of the Los Angeles Clippers of the National Basketball Association (NBA). He is a co-founder of Ballmer Group, a philanthropic investment company.</p>
	HTML;


$page->parent('list_of_people.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Steve Ballmer");
$page->body($div_wikipedia_Steve_Ballmer);
