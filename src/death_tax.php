<?php
$page = new Page();
$page->h1("Death Tax");
$page->tags("Taxes", "Tax", "USA", "Orwellian Doublespeak", "Information: Discourse");
$page->keywords("Death Tax");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

$r1 = $page->ref("https://en.wikipedia.org/wiki/Estate_tax_in_the_United_States#The_term_%22death_tax%22", 'The term "death tax"');
// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>The "${'estate tax'}", only levied from extremely rich estate holders when they pass, was being advertised as a "Death Tax"
	which increased the tax's unpopularity with middle class and poor people, who wouldn't pay it anyway. $r1
	As such, "Death Tax" is an example of ${'Orwellian Doublespeak'}.</p>
	HTML;


$page->parent('orwellian_doublespeak.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Death Tax");
