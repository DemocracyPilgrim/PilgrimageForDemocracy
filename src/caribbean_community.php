<?php
$page = new Page();
$page->h1('Caribbean Community');
$page->tags("International Organisation");
$page->keywords('Caribbean Community');
$page->stars(0);

//$page->snp('description', '');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$list = new ListOfPages();
$list->add('haiti.html');
$print_list = $list->print();

$div_list = new ContentSection();
$div_list->content = <<<HTML
	<h3>Member countries</h3>

	$print_list
	HTML;

$div_wikipedia_Caribbean_Community = new WikipediaContentSection();
$div_wikipedia_Caribbean_Community->setTitleText('Caribbean Community');
$div_wikipedia_Caribbean_Community->setTitleLink('https://en.wikipedia.org/wiki/Caribbean_Community');
$div_wikipedia_Caribbean_Community->content = <<<HTML
	<p>The Caribbean Community (CARICOM or CC) is an intergovernmental organisation
	that is a political and economic union of 15 member states throughout the Americas and Atlantic Ocean.
	They have primary objectives to promote economic integration and cooperation among its members,
	ensure that the benefits of integration are equitably shared, and coordinate foreign policy.</p>
	HTML;


$page->parent('world.html');
$page->template("stub");
$page->body($div_introduction);

$page->body($div_list);


$page->body($div_wikipedia_Caribbean_Community);
