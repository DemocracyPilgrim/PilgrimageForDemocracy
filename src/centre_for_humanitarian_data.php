<?php
$page = new Page();
$page->h1("Centre for Humanitarian Data");
$page->keywords("Centre for Humanitarian Data");
$page->tags("Organisation");
$page->stars(0);

//$page->snp("description", "");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );


$r1 = $page->ref('https://centre.humdata.org/what-we-do/', 'Centre for Humanitarian Data: What We Do');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>The Center for Humanitarian Data is a project of the ${'United Nations Office for the Coordination of Humanitarian Affairs'} (OCHA).</p>
	HTML;



$div_Centre_for_Humanitarian_Data_website = new WebsiteContentSection();
$div_Centre_for_Humanitarian_Data_website->setTitleText("Centre for Humanitarian Data website ");
$div_Centre_for_Humanitarian_Data_website->setTitleLink("https://centre.humdata.org/");
$div_Centre_for_Humanitarian_Data_website->content = <<<HTML
	<p>The Centre for Humanitarian Data is focused on increasing the use and impact of data in the humanitarian sector.
	It is managed by the United Nations Office for the Coordination of Humanitarian Affairs (OCHA).
	As part of OCHA’s Information Management Branch,
	the Centre’s role is to enable the organization to deliver on its ambition to be more data-driven and analytical,
	and to ensure a responsible approach to technology and data management.</p>
	HTML;


$page->parent('united_nations_office_for_the_coordination_of_humanitarian_affairs.html');
$page->template("stub");
$page->body($div_introduction);

$page->body($div_Centre_for_Humanitarian_Data_website);
