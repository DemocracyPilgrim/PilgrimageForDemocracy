<?php
$page = new Page();
$page->h1("What is populism?");
$page->tags("Book");
$page->keywords("What is populism?");
$page->stars(0);

//$page->snp("description", "");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref('', '');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>"What is $populism?" is a book by historian ${"Jan-Werner Müller"}.</p>
	HTML;



$div_The_Rise_and_Rise_of_Populism = new WebsiteContentSection();
$div_The_Rise_and_Rise_of_Populism->setTitleText("[PDF] Chapter: The Rise and Rise of Populism ");
$div_The_Rise_and_Rise_of_Populism->setTitleLink("https://www.bbvaopenmind.com/wp-content/uploads/2018/03/BBVA-OpenMind-Jan-Werner-Muller-The-Rise-and-Rise-of-Populism-1.pdf");
$div_The_Rise_and_Rise_of_Populism->content = <<<HTML
	<p>The chapter
	argues that populism should not be understood as
	primarily a form of anti-elitism. Rather, the hallmark of populists is
	that they claim that they, and they alone, represent the people (or
	what populists very often refer to as “the real people”). Populists
	deny the legitimacy of all other contenders for power and also
	suggest that citizens who do not support them can have their status
	as properly belonging to the people put in doubt. The chapter
	also analyzes the behavior of populists in power – arguing that
	we can see the emergence of a distinctive pattern of authoritarian
	governance where populists have large enough majorities and
	countervailing forces are too weak. Finally, the chapter suggests a
	number of strategies of how populism can be countered.</p>
	HTML;



$page->parent('list_of_books.html');
$page->template("stub");
$page->body($div_introduction);
$page->body($div_The_Rise_and_Rise_of_Populism);
$page->body('jan_werner_mueller.html');
$page->body('populism.html');
