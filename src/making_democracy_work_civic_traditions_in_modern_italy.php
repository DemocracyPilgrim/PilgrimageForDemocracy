<?php
$page = new BookPage();
$page->h1("Making Democracy Work: Civic Traditions in Modern Italy");
$page->tags("Book", "Robert Putnam", "Society", "Institutions", "Italy", "Making Democracy Work");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>"${'Making Democracy Work'}: Civic Traditions in Modern $Italy" is a $book by ${'Robert Putnam'}.</p>
	HTML;

$div_wikipedia_Making_Democracy_Work = new WikipediaContentSection();
$div_wikipedia_Making_Democracy_Work->setTitleText("Making Democracy Work");
$div_wikipedia_Making_Democracy_Work->setTitleLink("https://en.wikipedia.org/wiki/Making_Democracy_Work");
$div_wikipedia_Making_Democracy_Work->content = <<<HTML
	<p>Making Democracy Work: Civic Traditions in Modern Italy is a 1993 book written by Robert D. Putnam (with Robert Leonardi and Raffaella Y. Nanetti).
	Published by Princeton University Press, the book's central thesis is that social capital is key to high institutional performance and the maintenance of democracy.</p>
	HTML;


$page->parent('list_of_books.html');
$page->template("stub");
$page->body($div_introduction);



$page->body($div_wikipedia_Making_Democracy_Work);
