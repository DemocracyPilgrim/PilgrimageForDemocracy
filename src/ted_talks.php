<?php
$page = new Page();
$page->h1("TED Talks");
$page->keywords("TED", "TED Talks", "TED Talk");
$page->tags("Organisation", "Information: Media");
$page->stars(1);
$page->viewport_background('/free/ted_talks.png');

$page->snp("description", "TED Conferences (Technology, Entertainment, Design), a non-profit media organization");
$page->snp("image",       "/free/ted_talks.1200-630.png");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref('', '');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>The famous TED talk covers all sorts of "ideas worth spreading",
	many of which are intimately related to the topics covered by the $Pilgrimage.</p>

	<p>We shall provide here a curated list of TED talks related to $democracy, ${'political discourse'}, ${'social justice'}
	and other humanitarian issues.</p>
	HTML;


$list_TED_Talk = ListOfPeoplePages::WithTags("TED Talk");
$print_list_TED_Talk = $list_TED_Talk->print();

$div_list_TED_Talk = new ContentSection();
$div_list_TED_Talk->content = <<<HTML
	<h3>TED Talks and TED Talk speakers</h3>

	$print_list_TED_Talk
	HTML;




$div_TED_The_next_global_superpower_isnt_who_you_think = new TEDtalkContentSection();
$div_TED_The_next_global_superpower_isnt_who_you_think->setTitleText("The next global superpower isn't who you think | Ian Bremmer");
$div_TED_The_next_global_superpower_isnt_who_you_think->setTitleLink("https://www.ted.com/talks/ian_bremmer_the_next_global_superpower_isn_t_who_you_think");
$div_TED_The_next_global_superpower_isnt_who_you_think->content = <<<HTML
	<p>We have three orders:</p>
	<ul>
		<li>A global security order,</li>
		<li>A global economic order,</li>
		<li>A digital order.</li>
	</ul>

	<p>About the economic order:</p>

	<blockquote>Tens of millions of citizens in the ${'United States'} and other wealthy $democracies felt left behind by globalization.
	This has been ignored for decades.
	But as a consequence, they felt that their governments and their leaders were more illegitimate.
	</blockquote>

	<p>About the digital order:</p>

	<p>It used to be just nature and nurture determining our identities, now its nature, nurture & $algorithms.</p>

	<blockquote>
		<p>These technology companies are not just Fortune 50 and 100 actors.
		These technology titans are not just men worth 50 or 100 billion dollars or more.
		They are increasingly the most powerful people on the planet with influence over our futures.
		And we need to know, are they going to act accountably as they release new and powerful ${'artificial intelligence'}?
		What are they going to do with this unprecedented amount of data that they are collecting on us and our environment?
		And the one that I think should concern us all right now the most:
		Will they persist with these $advertising models driving so much revenues
		that are turning citizens into products and driving hate and misinformation and ripping apart our society?</p>

		<p>When I was a student back in 1989, and the Wall fell, the United States was the principal exporter of democracy in the world.
		Not always successfully.
		Often hypocritically.
		But number one, nonetheless.
		Today, the United States has become the principal exporter of tools that destroy democracy.
		The technology leaders who create and control these tools, are they OK with that?
		Or are they going to do something about it? We need to know.</p>
	</blockquote>
	HTML;


$div_wikipedia_TED_conference = new WikipediaContentSection();
$div_wikipedia_TED_conference->setTitleText("TED conference");
$div_wikipedia_TED_conference->setTitleLink("https://en.wikipedia.org/wiki/TED_(conference)");
$div_wikipedia_TED_conference->content = <<<HTML
	<p>TED Conferences, LLC (Technology, Entertainment, Design) is an American-Canadian non-profit media organization
	that posts international talks online for free distribution under the slogan "ideas worth spreading".</p>
	HTML;


$page->parent('list_of_organisations.html');
$page->body($div_introduction);

$page->body($div_list_TED_Talk);

$page->body($div_TED_The_next_global_superpower_isnt_who_you_think);

$page->body($div_wikipedia_TED_conference);
