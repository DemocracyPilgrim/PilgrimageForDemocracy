<?php
$page = new Page();
$page->h1("Asylum seeker");
$page->tags("Humanity", "Immigration");
$page->keywords("Asylum Seeker", "asylum seeker");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;



$list_Asylum_Seeker = ListOfPeoplePages::WithTags("Asylum Seeker");
$print_list_Asylum_Seeker = $list_Asylum_Seeker->print();

$div_list_Asylum_Seeker = new ContentSection();
$div_list_Asylum_Seeker->content = <<<HTML
	<h3>Related content</h3>

	$print_list_Asylum_Seeker
	HTML;



$div_wikipedia_Asylum_seeker = new WikipediaContentSection();
$div_wikipedia_Asylum_seeker->setTitleText("Asylum seeker");
$div_wikipedia_Asylum_seeker->setTitleLink("https://en.wikipedia.org/wiki/Asylum_seeker");
$div_wikipedia_Asylum_seeker->content = <<<HTML
	<p>An asylum seeker is a person who leaves their country of residence, enters another country,
	and makes in that other country a formal application for the right of asylum according to the Universal Declaration of Human Rights Article 14.
	A person keeps the status of asylum seeker until the right of asylum application has concluded.</p>
	HTML;


$page->parent('humanity.html');
$page->template("stub");
$page->body($div_introduction);
$page->body($div_list_Asylum_Seeker);


$page->body($div_wikipedia_Asylum_seeker);
