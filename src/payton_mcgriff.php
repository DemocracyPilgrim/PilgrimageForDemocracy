<?php
$page = new PersonPage();
$page->h1("Payton McGriff");
$page->alpha_sort("McGriff, Payton");
$page->tags("Person", "Education", "Humanity", "Style Her Empowered");
$page->keywords("Payton McGriff");
$page->stars(0);
$page->viewport_background("/free/payton_mcgriff.png");

$page->snp("description", "Founder of Style Her Empowered");
$page->snp("image",       "/free/payton_mcgriff.1200-630.png");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Payton McGriff is the founder of ${'Style Her Empowered'}.
	She is the 2023 winner of the Distinction in International Service Award (DISA).</p>
	HTML;


$page->parent('list_of_people.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Payton McGriff");
