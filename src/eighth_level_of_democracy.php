<?php
$page = new Page();
$page->h1("8: Eighth level of democracy — We, Humans");
$page->stars(0);
$page->keywords('eighth level of democracy', 'eighth level');
$page->viewport_background('/free/eighth_level_of_democracy.png');

//$page->snp("description", "");
$page->snp("image",       "/free/eighth_level_of_democracy.1200-630.png");

$page->preview( <<<HTML
	<p>We started with individual rights, and worked our way up for the collective good of humankind.
	We now reach the culmination of our exploration of what the essence of democracy is.
	It finds its truest expression in compassion, mutual help, solidarity and empathy for our shared humanity.
	Ultimately, democracy is the best social contract enabling the physical, emotional, intellectual, ethical and spiritual development of all human beings.</p>
	HTML );


$div_quote_fraternity_amdebkar = new ContentSection();
$div_quote_fraternity_amdebkar->content = <<<HTML
	<blockquote>
		Without fraternity, equality and liberty will be no deeper than coats of paint.
		<br>
		— ${'B.R. Amdebkar'}, 25 November 1949.
	</blockquote>
	HTML;



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>The definition of $democracy started at its ${'first level'} with the individual.
	At the ${'Second Level'}, we did consider society as a whole,
	but mostly inasmuch as the personal liberties of one individual were limited by that of other individuals.<p>

	<p>We now conclude our thorough definition of democracy
	with humanity as a whole and the best it has to offer and what it means to be truly human:
	compassion, solidarity, mutual help, etc.</p>
	HTML;



$div_Civic_engagement = new ContentSection();
$div_Civic_engagement->content = <<<HTML
	<h3>Civic engagement</h3>

	<p>The most important actors of democracy are ourselves.
	Through our civic engagement, we help shape the society.</p>
	HTML;



$div_Charities = new ContentSection();
$div_Charities->content = <<<HTML
	<h3>Charities</h3>

	<p>There are countless charities and $organisations doing admirable work to help humanity...</p>
	HTML;


$div_Human_development = new ContentSection();
$div_Human_development->content = <<<HTML
	<h3>Human development</h3>

	<p>Each human being is at a different stage of human development.
	Some are intellectually, artistically, humanely, spiritually more developed than others.
	Some are successful, well established members of society,
	others obviously struggle to deal with themselves and their lives.</p>

	<p>As humans, we cannot help but want to help others grow,
	so that economically, humanely and spiritually, they can learn to stand on their own two feet.</p>
	HTML;


$h2_We_humans = new h2HeaderContent("We, Humans");


$h2_Humanity_in_action = new h2HeaderContent("Humanity in Action");


$list_all_related_topics = new ListOfPages();
$list_all_related_topics->add('humanity.html');
$print_list_all_related_topics = $list_all_related_topics->print();

$div_list_all_related_topics = new ContentSection();
$div_list_all_related_topics->content = <<<HTML
	$print_list_all_related_topics
	HTML;



$page->parent('democracy.html');
$page->previous('seventh_level_of_democracy.html');
$page->next('democracy_wip.html');

$page->body($div_quote_fraternity_amdebkar);
$page->body($div_introduction);

$page->body($h2_We_humans);

$page->body($div_Human_development);



$page->body($h2_Humanity_in_action);
$page->body($div_Civic_engagement);
$page->body($div_Charities);



$page->body($div_list_all_related_topics);

