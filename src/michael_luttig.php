<?php
$page = new Page();
$page->h1('J. Michael Luttig');
$page->alpha_sort("Luttig, Michael");
$page->tags("Person", "Institutions: Judiciary", "Donald Trump", "Lawyer");
$page->keywords('Michael Luttig');
$page->viewport_background('/free/michael_luttig.png');
$page->stars(1);

$page->snp('description', 'American Former Federal Appeals Court Judge');
$page->snp('image',       '/free/michael_luttig.1200-630.png');

$page->preview( <<<HTML
	<p></p>
	HTML );

$r1 = $page->ref("https://edition.cnn.com/2024/08/19/politics/conservative-republican-endorses-harris-calls-trump-a-threat-to-democracy",
                 "Exclusive: Conservative Republican endorses Harris, calls Trump a threat to democracy");
$r2 = $page->ref("https://www.documentcloud.org/documents/25050952-luttig-endorsement",
                 "Endorsement of Vice President of the United States Kamala Harris");


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>J. Michael Luttig is an American retired conservative judge.
	He worked both in Ronald Reagan and George H. W. Bush governments.
	He is a co-chair of the ${'ABA Task Force for American Democracy'}.</p>

	<p>Luttig said that "Donald Trump and his allies and supporters are a clear and present danger to American $democracy."</p>

	<p>Together with liberal legal scholar ${'Laurence Tribe'}, he argued that ${'Donald Trump'} is illegible to be
	candidate in the 2024 presidential election due to the 14th amendment to the US $constitution.</p>

	<p>In August 2024, he endorsed Kamala Harris in the 2024 presidential $election, writing: $r1</p>
	<blockquote>
		"In the presidential election of 2024 there is only one political party and one candidate for the presidency
		that can claim the mantle of defender and protector of America’s Democracy, the $Constitution, and the Rule of Law.
		As a result, I will unhesitatingly vote for the Democratic Party’s candidate for the Presidency of the United States,
		Vice President of the United States, Kamala Harris."
	</blockquote>
	HTML;


$div_Endorsement_of_Harris = new ContentSection();
$div_Endorsement_of_Harris->content = <<<HTML
	<h3>Endorsement of Kamala Harris for president</h3>

	<p>In August 2024, Luttig published an open letter in which he endorsed  Vice President of the ${'United States'} Kamala Harris for president. $r2</p>

	<p>Judge Luttig argues that former President Trump's continued denial of the 2020 election results
	and his glorification of the January 6th Capitol riot represent a profound threat to American democracy.
	He emphasizes the following points:</p>

	<ul>
		<li><strong>Trump's actions have eroded faith in elections</strong>:
		Trump's persistent, knowingly false claims about a "stolen" election have led millions of Americans to doubt the legitimacy of US elections,
		damaging trust in democratic institutions.</li>
		<li><strong>Trump's actions have undermined confidence in democracy</strong>:
		Many Americans, particularly young people, are questioning the viability of constitutional democracy due to Trump's actions
		and the ongoing attacks on the election process.</li>
		<li><strong>Trump's actions are an unprecedented attack on democracy</strong>:
		The former President's actions, including attempting to overturn the election results and refusing to accept the peaceful transfer of power,
		are unprecedented in American history and represent a dangerous assault on democratic norms.</li>
		<li><strong>The Republican Party's stance poses a further threat</strong>:
		The Republican Party's continued embrace of Trump's false claims and their refusal to commit to upholding election results are deeply concerning,
		creating the potential for further unrest and violence.</li>
		<li><strong>The peaceful transfer of power is essential</strong>:
		The peaceful transition of power between presidents, a cornerstone of American democracy,
		is under threat due to the former president's actions and the Republican Party's unwillingness to denounce his behavior.</li>
	</ul>

	<p>Judge Luttig ultimately argues that these actions are not abstract concepts but rather a tangible threat to the very foundation of American democracy.
	He calls for a robust defense of the democratic process and a rejection of those who seek to undermine it.</p>

	<p>He wrote: $r1</p>

	<blockquote>
	The first thing all of us are taught is right from wrong. We're not taught right from left.
	</blockquote>
	HTML;




$div_Jeh_Johnson_and_Michael_Luttig_Discuss_the_State_of_Democracy_in_America = new WebsiteContentSection();
$div_Jeh_Johnson_and_Michael_Luttig_Discuss_the_State_of_Democracy_in_America->setTitleText("Jeh Johnson and Michael Luttig Discuss the State of Democracy in America ");
$div_Jeh_Johnson_and_Michael_Luttig_Discuss_the_State_of_Democracy_in_America->setTitleLink("https://www.c-span.org/video/?535383-1/jeh-johnson-michael-luttig-discuss-state-democracy-america");
$div_Jeh_Johnson_and_Michael_Luttig_Discuss_the_State_of_Democracy_in_America->content = <<<HTML
	<p>Former Homeland Security Secretary Jeh Johnson and retired U.S. Court of Appeals Fourth Circuit Judge Michael Luttig
	sat down for a conversation on the state of democracy hosted by the American Bar Association.
	Judge Luttig was outspoken about the January 6 Capitol attack’s negative impact on democracy,
	having testified before the House select committee tasked with investigating January 6.
	This discussion was a part of the American Bar Association Litigation Section’s conference in Washington, DC.</p>
	HTML;



$div_wikipedia_J_Michael_Luttig = new WikipediaContentSection();
$div_wikipedia_J_Michael_Luttig->setTitleText('J Michael Luttig');
$div_wikipedia_J_Michael_Luttig->setTitleLink('https://en.wikipedia.org/wiki/J._Michael_Luttig');
$div_wikipedia_J_Michael_Luttig->content = <<<HTML
	<p>John Michael Luttig (born June 13, 1954) is an American corporate lawyer and jurist
	who was a judge of the United States Court of Appeals for the Fourth Circuit from 1991 to 2006.
	Luttig resigned from the court of appeals in 2006 to become general counsel of Boeing, a position he held until 2019.</p>
	HTML;


$page->parent('list_of_people.html');
$page->template("stub");

$page->body($div_introduction);
$page->body($div_Endorsement_of_Harris);


$page->body($div_Jeh_Johnson_and_Michael_Luttig_Discuss_the_State_of_Democracy_in_America);
$page->body($div_wikipedia_J_Michael_Luttig);
