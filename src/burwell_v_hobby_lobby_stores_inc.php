<?php
$page = new Page();
$page->h1("Burwell v. Hobby Lobby Stores, Inc. (2014)");
$page->tags("Institutions: Judiciary", "SCOTUS Landmark Decision", "USA", "Women's Rights", "Religion");
$page->keywords("Burwell v. Hobby Lobby Stores, Inc.");
$page->stars(1);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>"Burwell v. Hobby Lobby Stores, Inc." is a 2014 landmark decision by the ${'Supreme Court of the United States'}.</p>

	<p>This decision allowed closely held for-profit corporations to be exempt from a federal regulation
	requiring employers to provide health insurance coverage for certain types of contraception.
	This decision has been seen by some as a victory for $religious $freedom, while others argue that it undermines $women's health and reproductive rights.</p>
	HTML;

$div_wikipedia_Burwell_v_Hobby_Lobby_Stores_Inc = new WikipediaContentSection();
$div_wikipedia_Burwell_v_Hobby_Lobby_Stores_Inc->setTitleText("Burwell v Hobby Lobby Stores Inc");
$div_wikipedia_Burwell_v_Hobby_Lobby_Stores_Inc->setTitleLink("https://en.wikipedia.org/wiki/Burwell_v._Hobby_Lobby_Stores,_Inc.");
$div_wikipedia_Burwell_v_Hobby_Lobby_Stores_Inc->content = <<<HTML
	<p>Burwell v. Hobby Lobby Stores, Inc., 573 U.S. 682 (2014), is a landmark decision in United States corporate law by the United States Supreme Court
	allowing privately held for-profit corporations to be exempt from a regulation that its owners religiously object to,
	if there is a less restrictive means of furthering the law's interest, according to the provisions of the Religious Freedom Restoration Act of 1993.
	It is the first time that the Court has recognized a for-profit corporation's claim of religious belief, but it is limited to privately held corporations.
	The decision does not address whether such corporations are protected by the free exercise of religion clause of the First Amendment of the Constitution.</p>
	HTML;


$page->parent('supreme_court_of_the_united_states.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_wikipedia_Burwell_v_Hobby_Lobby_Stores_Inc);
