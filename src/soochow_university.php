<?php
$page = new Page();
$page->h1("Soochow University (東吳大學)");
$page->keywords("Soochow University");
$page->tags("Organisation", "Taiwan");
$page->stars(1);

$page->snp("description", "Private university in Taipei, Taiwan.");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p>Private university in Taipei, $Taiwan.</p>
	HTML );

// $r1 = $page->ref('', '');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Soochow University (東吳大學) is a private university in Taipei, $Taiwan.</p>

	<p>Their Department of Political Science (政治學系) is
	part of the School of Liberal Arts and Social Sciences (人文社會學院).</p>
	HTML;



$div_Soochow_University_website = new WebsiteContentSection();
$div_Soochow_University_website->setTitleText("Soochow University website ");
$div_Soochow_University_website->setTitleLink("https://www-en.scu.edu.tw/");
$div_Soochow_University_website->content = <<<HTML
	<p>Soochow University, founded in 1900 in Suzhou, was the first western-style university in China.
	After the government of the Republic of China moved to Taiwan, some of its former alumni exerted tireless efforts to reestablish their alma mater in Taipei.
	In 1951, a board of directors was created and premises were rented on Hankou Street in downtown Taipei to set up the so-called Soochow Preparatory School.
	After the Ministry of Education approved the reestablishment plan at the end of 1954, the School of Law was the first to be established,
	which included the Departments of Law, Political Science, Economics, and Accounting, along with an attached Foreign Languages and Literature Department.
	The school became the first private university in Taiwan.</p>
	HTML;



$div_Soochow_University_Department_of_Political_Science = new WebsiteContentSection();
$div_Soochow_University_Department_of_Political_Science->setTitleText("Soochow University Department of Political Science 政治學系");
$div_Soochow_University_Department_of_Political_Science->setTitleLink("https://web-en.scu.edu.tw/politicial/web_page/815");
$div_Soochow_University_Department_of_Political_Science->content = <<<HTML
	<p>The department of political science of Soochow University was among the first four departments
	established after members of the Soochow Alumni Association had fled from mainland China to Taiwan
	after the Chinese Civil War and founded the University in 1954.
	The Department's graduate programs were established in 1991 and 1998, respectively.
	Currently, 461 undergraduate students, as well as 47 masters and 12 doctoral students, register for the Department of Political Science.
	Soochow University implemented higher education evaluation in 2003.
	The Department of Political Science won the excellence award, got the school subsidy,
	and promoted numerous teaching, researching, counseling development plans.</p>

	<p>Chinese website: <a href="https://scups.ppo.scu.edu.tw/">東吳大學政治學系</a>.</p>
	HTML;





$div_wikipedia_Soochow_University_Taipei = new WikipediaContentSection();
$div_wikipedia_Soochow_University_Taipei->setTitleText("Soochow University Taipei");
$div_wikipedia_Soochow_University_Taipei->setTitleLink("https://en.wikipedia.org/wiki/Soochow_University_(Taipei)");
$div_wikipedia_Soochow_University_Taipei->content = <<<HTML
	<p>Soochow University (Chinese: 東吳大學) is a private university in Taipei, Taiwan.
	The university is noted for studies in comparative law and accounting.</p>
	HTML;

$div_wikipedia_Department_of_Political_Science_Soochow_University_Taiwan = new WikipediaContentSection();
$div_wikipedia_Department_of_Political_Science_Soochow_University_Taiwan->setTitleText("Department of Political Science Soochow University Taiwan");
$div_wikipedia_Department_of_Political_Science_Soochow_University_Taiwan->setTitleLink("https://en.wikipedia.org/wiki/Department_of_Political_Science_Soochow_University_(Taiwan)");
$div_wikipedia_Department_of_Political_Science_Soochow_University_Taiwan->content = <<<HTML
	<p>The Department of Political Science of Soochow University is one of the oldest departement of the university.
	This departement exists since 1954, when Soochow University was reestablished in Taiwan.</p>
	HTML;


$page->parent('list_of_organisations.html');
$page->parent('political_science_in_taiwan.html');
$page->template("stub");
$page->body($div_introduction);

$page->body($div_Soochow_University_website);
$page->body($div_Soochow_University_Department_of_Political_Science);

$page->body($div_wikipedia_Soochow_University_Taipei);
$page->body($div_wikipedia_Department_of_Political_Science_Soochow_University_Taiwan);
