<?php
$page = new Page();
$page->h1("Evolution of the Republican Party");
$page->tags("Elections", "Party Politics", "USA", "WIP");
$page->keywords("Evolution of the Republican Party", "Republican Party Evolution", "Republican Party");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	We aim to study the evolution of the $American Republican Party in light of ${"Duverger's Law"} and its symptoms,
	how the ${'voting method'} has affected the evolution of the party, from its creation from the ashes of the Whig Party,
	to the $Trump and $MAGA cult take over of the party.
	What is in store for the future?
	Post Trump, will the Republican Party disintegrate into two parties: one MAGA party and one traditional conservative party?</p>
	HTML;

$div_wikipedia_Republican_Party_United_States = new WikipediaContentSection();
$div_wikipedia_Republican_Party_United_States->setTitleText("Republican Party United States");
$div_wikipedia_Republican_Party_United_States->setTitleLink("https://en.wikipedia.org/wiki/Republican_Party_(United_States)");
$div_wikipedia_Republican_Party_United_States->content = <<<HTML
	<p>The Republican Party, also known as the GOP (Grand Old Party), is one of the two major contemporary political parties in the United States. It emerged as the main political rival of the then-dominant Democratic Party in the mid-1850s, and the two parties have dominated American politics ever since.</p>
	HTML;

$div_wikipedia_History_of_the_Republican_Party_United_States = new WikipediaContentSection();
$div_wikipedia_History_of_the_Republican_Party_United_States->setTitleText("History of the Republican Party United States");
$div_wikipedia_History_of_the_Republican_Party_United_States->setTitleLink("https://en.wikipedia.org/wiki/History_of_the_Republican_Party_(United_States)");
$div_wikipedia_History_of_the_Republican_Party_United_States->content = <<<HTML
	<p>In 1854, the Republican Party emerged to combat the expansion of slavery into western territories after the passing of the Kansas–Nebraska Act.</p>
	HTML;


$page->parent('duverger_syndrome_party_politics.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag(array("Republican Party Evolution", "Republican Party"));
$page->body($div_wikipedia_Republican_Party_United_States);
$page->body($div_wikipedia_History_of_the_Republican_Party_United_States);
