<?php
$page = new Page();
$page->h1("Propaganda");
$page->keywords("propaganda", "Propaganda");
$page->tags("Information: Government", "Political Discourse", "Disinformation");
$page->stars(0);

//$page->snp("description", "");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref('', '');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	HTML;

$div_wikipedia_Propaganda = new WikipediaContentSection();
$div_wikipedia_Propaganda->setTitleText("Propaganda");
$div_wikipedia_Propaganda->setTitleLink("https://en.wikipedia.org/wiki/Propaganda");
$div_wikipedia_Propaganda->content = <<<HTML
	<p>Propaganda is communication that is primarily used to influence or persuade an audience to further an agenda,
	which may not be objective and may be selectively presenting facts to encourage a particular synthesis or perception,
	or using loaded language to produce an emotional rather than a rational response to the information that is being presented.</p>
	HTML;


$page->parent('disinformation.html');
$page->template("stub");
$page->body($div_introduction);

$page->related_tag("Propaganda");
$page->body($div_wikipedia_Propaganda);
