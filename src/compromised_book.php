<?php
$page = new Page();
$page->h1("Compromised book");
$page->tags("Book");
$page->keywords("Compromised book");
$page->stars(0);

//$page->snp("description", "");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref('', '');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Compromised_book = new WikipediaContentSection();
$div_wikipedia_Compromised_book->setTitleText("Compromised book");
$div_wikipedia_Compromised_book->setTitleLink("https://en.wikipedia.org/wiki/Compromised_(book)");
$div_wikipedia_Compromised_book->content = <<<HTML
	<p>Compromised: Counterintelligence and the Threat of Donald J. Trump is a 2020 non-fiction book authored by former FBI agent Peter Strzok.
	As Deputy Assistant Director of the FBI counterintelligence division,
	Strzok led the FBI's Crossfire Hurricane investigation of alleged Russian influence upon President Donald Trump and Trump's 2016 campaign.
	Strzok's book recaps the full arc of the investigation
	and portrays Trump as profoundly corrupt, and a serious threat to national security.</p>
	HTML;

$div_wikipedia_Peter_Strzok = new WikipediaContentSection();
$div_wikipedia_Peter_Strzok->setTitleText("Peter Strzok");
$div_wikipedia_Peter_Strzok->setTitleLink("https://en.wikipedia.org/wiki/Peter_Strzok");
$div_wikipedia_Peter_Strzok->content = <<<HTML
	<p>Peter Paul Strzok II is a former United States Federal Bureau of Investigation (FBI) agent.
	He was the Deputy Assistant Director of the FBI's Counterintelligence Division
	and led the investigation into Russian interference in the 2016 United States elections.</p>
	HTML;


$page->parent('list_of_books.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_wikipedia_Compromised_book);
$page->body($div_wikipedia_Peter_Strzok);
