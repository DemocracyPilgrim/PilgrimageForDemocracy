<?php
$page = new Page();
$page->h1("Brandenburg v. Ohio (1969)");
$page->tags("Institutions: Judiciary", "SCOTUS Landmark Decision", "USA", "Freedom of Speech", "Stochastic Terrorism", "Racism", "Donald Trump");
$page->keywords("Brandenburg v. Ohio");
$page->stars(2);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>"Brandenburg v. Ohio" is a 1969 landmark decision by the ${'Supreme Court of the United States'}.</p>

	<p>The decision redefined the First Amendment's protection of speech.
	The case involved Clarence Brandenburg, a Ku Klux Klan leader, who was convicted
	under an Ohio law prohibiting advocating "crime, sabotage, violence, or unlawful methods of terrorism as a means of accomplishing industrial or political reform."</p>

	<p>Brandenburg had made a speech at a Klan rally, which was later filmed and shown on local television.
	In his speech, he made inflammatory remarks, including a call to "take revenge" if the government did not stop oppressing white people.</p>

	<p>The Supreme Court, in a unanimous decision, overturned Brandenburg's conviction.
	They established the "Brandenburg test", a two-part standard for determining when speech can be punished:</p>

	<ol>
		<li>Imminent lawless action: The speech must be directed at inciting or producing imminent lawless action.</li>
		<li>Likely to incite: The speech must be likely to incite or produce such action.</li>
	</ol>

	<p>The Brandenburg test significantly strengthened the First Amendment's protection of free speech, particularly for unpopular or controversial viewpoints.
	It narrowed the scope of government's ability to restrict speech, emphasizing that speech can only be punished
	when it is directly intended to incite imminent lawless action.</p>

	<p>On one hand, the decision empowers individuals to express their political opinions and dissent, even if those opinions are unpopular or offensive.
	This is crucial for a functioning democracy, where diverse voices are needed for healthy debate and decision-making.
	The Brandenburg test encourages open and free exchange of ideas, even if those ideas are controversial or potentially harmful.</p>

	<p>On the other hand, the decision allows hate speech to flourish, as it may be difficult to prove that such speech is directly inciting imminent lawless action.
	The Brandenburg test may create a gray area for speech that falls short of directly inciting imminent lawless action
	but still contributes to an atmosphere of violence or hostility.</p>

	<p>Overall, <em>Brandenburg v. Ohio</em> goes too far in protecting inflammatory speech, especially with the rise of $disinformation
	and politically motivated hate speech.</p>
	HTML;




$div_The_landmark_Klan_free_speech_case_behind_Trump_s_impeachment_defense = new WebsiteContentSection();
$div_The_landmark_Klan_free_speech_case_behind_Trump_s_impeachment_defense->setTitleText("The landmark Klan free-speech case behind Trump’s impeachment defense");
$div_The_landmark_Klan_free_speech_case_behind_Trump_s_impeachment_defense->setTitleLink("https://www.washingtonpost.com/history/2021/02/10/brandenburg-trump-supreme-court-klan-free-speech/");
$div_The_landmark_Klan_free_speech_case_behind_Trump_s_impeachment_defense->content = <<<HTML
	<p>Though Trump’s impeachment is not a criminal trial, his lawyers in their legal briefs referenced Brandenburg v. Ohio, arguing that Trump didn’t direct his supporters to attack the Capitol. House managers prosecuting the case took the opposite view on Brandenburg and Trump, saying he crossed the line set up by the Supreme Court.</p>
	HTML;





$div_Oyez_Brandenburg_v_Ohio = new WebsiteContentSection();
$div_Oyez_Brandenburg_v_Ohio->setTitleText("Oyez: Brandenburg v. Ohio ");
$div_Oyez_Brandenburg_v_Ohio->setTitleLink("https://www.oyez.org/cases/1968/492");
$div_Oyez_Brandenburg_v_Ohio->content = <<<HTML
	<p><strong>Facts of the case</strong>:
	Brandenburg, a leader in the Ku Klux Klan, made a speech at a Klan rally and was later convicted under an Ohio criminal syndicalism law. The law made illegal advocating "crime, sabotage, violence, or unlawful methods of terrorism as a means of accomplishing industrial or political reform," as well as assembling "with any society, group, or assemblage of persons formed to teach or advocate the doctrines of criminal syndicalism."</p>

	<p><strong>Question</strong>:
	Did Ohio's criminal syndicalism law, prohibiting public speech that advocates various illegal activities, violate Brandenburg's right to free speech as protected by the First and Fourteenth Amendments?	</p>
	HTML;




$div_The_First_Amendment_doesn_t_protect_Trump_s_incitement = new WebsiteContentSection();
$div_The_First_Amendment_doesn_t_protect_Trump_s_incitement->setTitleText("The First Amendment doesn’t protect Trump’s incitement");
$div_The_First_Amendment_doesn_t_protect_Trump_s_incitement->setTitleLink("https://www.washingtonpost.com/outlook/2021/01/14/trump-brandenburg-impeachment-first-amendment/");





$div_wikipedia_Brandenburg_v_Ohio = new WikipediaContentSection();
$div_wikipedia_Brandenburg_v_Ohio->setTitleText("Brandenburg v Ohio");
$div_wikipedia_Brandenburg_v_Ohio->setTitleLink("https://en.wikipedia.org/wiki/Brandenburg_v._Ohio");
$div_wikipedia_Brandenburg_v_Ohio->content = <<<HTML
	<p>Brandenburg v. Ohio, 395 U.S. 444 (1969), is a landmark decision of the United States Supreme Court interpreting the First Amendment to the U.S. Constitution. The Court held that the government cannot punish inflammatory speech unless that speech is "directed to inciting or producing imminent lawless action and is likely to incite or produce such action".  Specifically, the Court struck down Ohio's criminal syndicalism statute, because that statute broadly prohibited the mere advocacy of violence. In the process, Whitney v. California (1927) was explicitly overruled, and Schenck v. United States (1919), Abrams v. United States (1919), Gitlow v. New York (1925), and Dennis v. United States (1951) were overturned.</p>
	HTML;


$page->parent('supreme_court_of_the_united_states.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Brandenburg v. Ohio");

$page->body($div_Oyez_Brandenburg_v_Ohio);
$page->body($div_The_landmark_Klan_free_speech_case_behind_Trump_s_impeachment_defense);
$page->body($div_The_First_Amendment_doesn_t_protect_Trump_s_incitement);


$page->body($div_wikipedia_Brandenburg_v_Ohio);
