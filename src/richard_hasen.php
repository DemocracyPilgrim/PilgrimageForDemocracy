<?php
$page = new Page();
$page->h1("Richard Hasen");
$page->alpha_sort("Hasen, Richard");
$page->tags("Person", "USA", "Lawyer", "Electoral System", "Campaign Finance", "Election Integrity");
$page->keywords("Richard Hasen");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Richard Hasen is an $American law professor, specialised in $election law and campaign finance.</p>

	<p>He is the author of the books:</p>

	<ul>
		<li>The Voting Wars: From Florida 2000 to the Next Election Meltdown (2012)</li>
		<li>Plutocrats United: Campaign Money, the Supreme Court, and the Distortion of American Elections (2016)</li>
		<li>The Justice of Contradictions: Antonin Scalia and the Politics of Disruption (2018)</li>
		<li>Election Meltdown: Dirty Tricks, Distrust, and the Threat to American Democracy (2020)</li>
		<li>Cheap Speech: How Disinformation Poisons Our Politics―and How to Cure It (2022)</li>
		<li>A Real Right to Vote: How a Constitutional Amendment Can Safeguard American Democracy (2024)</li>
	</ul>
	HTML;

$div_wikipedia_Richard_Hasen = new WikipediaContentSection();
$div_wikipedia_Richard_Hasen->setTitleText("Richard Hasen");
$div_wikipedia_Richard_Hasen->setTitleLink("https://en.wikipedia.org/wiki/Richard_Hasen");
$div_wikipedia_Richard_Hasen->content = <<<HTML
	<p>Richard L. Hasen is an American legal scholar and law professor at the University of California, Los Angeles.
	He is an expert in legislation, election law and campaign finance.</p>
	HTML;


$page->parent('list_of_people.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_wikipedia_Richard_Hasen);
