<?php
$page = new CountryPage('Honduras');
$page->h1('Honduras');
$page->tags("Country");
$page->keywords('Honduras');
$page->stars(0);

$page->snp('description', '9.5 million inhabitants.');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Honduras = new WikipediaContentSection();
$div_wikipedia_Honduras->setTitleText('Honduras');
$div_wikipedia_Honduras->setTitleLink('https://en.wikipedia.org/wiki/Honduras');
$div_wikipedia_Honduras->content = <<<HTML
	<p>The Republic of Honduras is a country in Central America.
	The republic of Honduras is bordered to the west by Guatemala, to the southwest by El Salvador,
	to the southeast by Nicaragua, to the south by the Pacific Ocean at the Gulf of Fonseca,
	and to the north by the Gulf of Honduras, a large inlet of the Caribbean Sea.</p>
	HTML;

$div_wikipedia_Politics_of_Honduras = new WikipediaContentSection();
$div_wikipedia_Politics_of_Honduras->setTitleText('Politics of Honduras');
$div_wikipedia_Politics_of_Honduras->setTitleLink('https://en.wikipedia.org/wiki/Politics_of_Honduras');
$div_wikipedia_Politics_of_Honduras->content = <<<HTML
	<p>Politics of Honduras takes place in a framework of a multi-party system presidential representative democratic republic.</p>
	HTML;


$page->parent('world.html');
$page->template("stub");
$page->body($div_introduction);
$page->body('Country indices');

$page->body($div_wikipedia_Honduras);
$page->body($div_wikipedia_Politics_of_Honduras);
