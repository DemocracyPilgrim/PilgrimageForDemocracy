<?php
$page = new BookPage();
$page->h1("Gaming the Vote: Why Elections Aren't Fair");
$page->tags("Book", "Elections", "Approval Voting");
$page->keywords("Gaming the Vote: Why Elections Aren't Fair");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

 $r1 = $page->ref("https://william-poundstone.com/books", "William Poundstone: Books");
 $r2 = $page->ref("https://electionscience.org/about-us", "CES: Contributors.");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>"Gaming the Vote: Why Elections Aren't Fair (and What We Can Do About It)" is a 2008 book by William Poundstone $r1.</p>

	<p>The book discusses better voting methods and concludes that range (a.k.a. score) voting and ${'approval voting'}
	are better than instant-runoff voting (the most popular form of ranked choice voting).</p>

	<p>Poundstone is listed as a Contributor on the website for the ${'Center for Election Science'} $r2 which advocates for approval voting.</p>
	HTML;


$div_wikipedia_William_Poundstone = new WikipediaContentSection();
$div_wikipedia_William_Poundstone->setTitleText("William Poundstone");
$div_wikipedia_William_Poundstone->setTitleLink("https://en.wikipedia.org/wiki/William_Poundstone");
$div_wikipedia_William_Poundstone->content = <<<HTML
	<p>William Poundstone is an American author, columnist, and skeptic.
	He has written a number of books including the Big Secrets series and a biography of Carl Sagan.</p>
	HTML;



$page->parent('list_of_books.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_wikipedia_William_Poundstone);
