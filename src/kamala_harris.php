<?php
$page = new PersonPage();
$page->h1("Kamala Harris");
$page->alpha_sort("Harris, Kamala");
$page->tags("Person", "USA", "POTUS");
$page->keywords("Kamala Harris");
$page->stars(0);
$page->viewport_background("/free/kamala_harris.png");

$page->snp("description", "Vice president of the United States");
$page->snp("image",       "/free/kamala_harris.1200-630.png");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Kamala_Harris = new WikipediaContentSection();
$div_wikipedia_Kamala_Harris->setTitleText("Kamala Harris");
$div_wikipedia_Kamala_Harris->setTitleLink("https://en.wikipedia.org/wiki/Kamala_Harris");
$div_wikipedia_Kamala_Harris->content = <<<HTML
	<p>Kamala Devi Harris (born October 20, 1964) is an American politician and attorney serving as the 49th and current vice president of the United States since 2021 under President Joe Biden. Harris is the Democratic Party's nominee for president in the 2024 presidential election. She also served in the United States Senate from 2017 to 2021 representing California, and earlier as the attorney general of California.</p>
	HTML;


$page->parent('list_of_people.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Kamala Harris");
$page->body($div_wikipedia_Kamala_Harris);
