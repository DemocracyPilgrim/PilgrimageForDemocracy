<?php
$page = new OrganisationPage();
$page->h1("Economic Policy Institute");
$page->keywords("Economic Policy Institute", "EPI");
$page->stars(0);
$page->tags("Organisation", "Taxes", "Fair Share");

$page->snp("description", "American think tank on workers' rights");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p>American think working on inequality, low wages and weak benefits for working people.</p>
	HTML );

$r1 = $page->ref('https://www.epi.org/about/', 'About the Economic Policy Institute');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>The Economic Policy Institute (EPI) is American think working on inequality, low wages and weak benefits for working people.</p>

	<p>The EPI is also doing some research and makes policy proposals in the domain of $taxes.</p>
	HTML;



$div_Economic_Policy_Institute_website = new WebsiteContentSection();
$div_Economic_Policy_Institute_website->setTitleText("Economic Policy Institute website");
$div_Economic_Policy_Institute_website->setTitleLink("https://www.epi.org/");
$div_Economic_Policy_Institute_website->content = <<<HTML
	<p>The Economic Policy Institute’s vision is an economy that is just and strong, sustainable, and equitable — where every job is good, every worker can join a union, and every family and community can thrive.</p>

	<p>The Economic Policy Institute (EPI) is a nonprofit, nonpartisan think tank working for the last 30 years to counter rising inequality, low wages and weak benefits for working people, slower economic growth, unacceptable employment conditions, and a widening racial wage gap. We intentionally center low- and middle-income working families in economic policy discussions at the federal, state, and local levels as we fight for a world where every worker has access to a good job with fair pay, affordable health care, retirement security, and a union.</p>

	<p>We also know that research on its own is not enough—that’s why we intentionally pair our research with effective outreach and advocacy efforts as we fight to make concrete change in everyday people’s lives.</p>
	HTML;



$div_Budget_Taxes_and_Public_Investment = new WebsiteContentSection();
$div_Budget_Taxes_and_Public_Investment->setTitleText("EPI: Budget, Taxes, and Public Investment ");
$div_Budget_Taxes_and_Public_Investment->setTitleLink("https://www.epi.org/research/budget-taxes-and-public-investment/");
$div_Budget_Taxes_and_Public_Investment->content = <<<HTML
	<p>EPI’s work on federal fiscal policy analyzes revenues, spending and deficits, but always within the context of the overall economy. EPI believes that the federal budget is the embodiment of our nation’s priorities, but recognizes that the state of budget balance is simply a tool to meet larger economic goals, not an end-goal in itself.</p>
	HTML;




$div_wikipedia_Economic_Policy_Institute = new WikipediaContentSection();
$div_wikipedia_Economic_Policy_Institute->setTitleText("Economic Policy Institute");
$div_wikipedia_Economic_Policy_Institute->setTitleLink("https://en.wikipedia.org/wiki/Economic_Policy_Institute");
$div_wikipedia_Economic_Policy_Institute->content = <<<HTML
	<p>The Economic Policy Institute (EPI) is a 501(c)(3) non-profit American think tank based in Washington, D.C.,
	that carries out economic research and analyzes the economic impact of policies and proposals.
	Affiliated with the labor movement, the EPI is usually described as presenting a left-leaning and pro-union viewpoint on public policy issues.</p>
	HTML;


$page->parent('list_of_organisations.html');
$page->parent('fair_share.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_Economic_Policy_Institute_website);
$page->body($div_Budget_Taxes_and_Public_Investment);

$page->body($div_wikipedia_Economic_Policy_Institute);
