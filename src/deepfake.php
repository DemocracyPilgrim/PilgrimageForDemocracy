<?php
$page = new Page();
$page->h1('Deepfake');
$page->keywords('deepfake', 'fake');
$page->tags("Disinformation", "Media");
$page->stars(0);

$page->snp('description', 'Misleading made-up photographs and videos: new threats for society and for democracy?');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>In 2023, pictures began circulating online of ${'Donald Trump'} being forcibly restrained by New York Police Department officers.
	The images were deep fakes created with an ${'artificial intelligence'} image generating software,
	and were shared on ${'social networks'} without context, leading many to believe that the images were genuine.</p>

	<p>That same year, another AI-generated picture of Pope Francis wearing a luxury white puffer jacket
	also fooled many people.</p>

	<p>The rapidly advancing artificial intelligence and realistic-looking image generation technologies
	is not without consequences for our society and for $democracy.
	Deep fake videos have not yet reached the same level of realism, but it is only a question of time.</p>

	<p>This is obviously very concerning.
	Many people, and most importantly, many children and very young adults,
	lack the ability to distinguish true from false.</p>

	<p>The questions now are:
	how to deal with these new threats?
	How to educate our youths to grow up to be discerning?
	What regulation to adopt?</p>
	HTML;

$div_wikipedia_Deepfake = new WikipediaContentSection();
$div_wikipedia_Deepfake->setTitleText('Deepfake');
$div_wikipedia_Deepfake->setTitleLink('https://en.wikipedia.org/wiki/Deepfake');
$div_wikipedia_Deepfake->content = <<<HTML
	<p>The spreading of disinformation and hate speech through deepfakes
	has a potential to undermine core functions and norms of democratic systems
	by interfering with people's ability to participate in decisions that affect them,
	determine collective agendas and express political will through informed decision-making.
	This has elicited responses from both industry and government to detect and limit their use.</p>
	HTML;


$page->parent('media.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_wikipedia_Deepfake);
