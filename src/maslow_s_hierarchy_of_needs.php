<?php
$page = new Page();
$page->h1("Maslow's hierarchy of needs");
$page->tags("Individual: Development");
$page->keywords("Maslow's hierarchy of needs");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Maslow_s_hierarchy_of_needs = new WikipediaContentSection();
$div_wikipedia_Maslow_s_hierarchy_of_needs->setTitleText("Maslow s hierarchy of needs");
$div_wikipedia_Maslow_s_hierarchy_of_needs->setTitleLink("https://en.wikipedia.org/wiki/Maslow's_hierarchy_of_needs");
$div_wikipedia_Maslow_s_hierarchy_of_needs->content = <<<HTML
	<p>Maslow's hierarchy of needs is an idea in psychology proposed by American psychologist Abraham Maslow in his 1943 paper "A Theory of Human Motivation" in the journal Psychological Review. Maslow subsequently extended the idea to include his observations of humans' innate curiosity. His theories parallel many other theories of human developmental psychology, some of which focus on describing the stages of growth in humans. The theory is a classification system intended to reflect the universal needs of society as its base, then proceeding to more acquired emotions.</p>
	HTML;


$page->parent('individuals.html');
$page->template("stub");
$page->body($div_introduction);



$page->body('sixth_level_of_democracy.html');
$page->body('living.html');
$page->related_tag("Maslow's hierarchy of needs");
$page->body($div_wikipedia_Maslow_s_hierarchy_of_needs);
