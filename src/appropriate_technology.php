<?php
$page = new Page();
$page->h1("Appropriate technology");
$page->keywords("Appropriate technology");
$page->stars(0);
$page->tags("Fair Share");

//$page->snp("description", "");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Appropriate_technology = new WikipediaContentSection();
$div_wikipedia_Appropriate_technology->setTitleText("Appropriate technology");
$div_wikipedia_Appropriate_technology->setTitleLink("https://en.wikipedia.org/wiki/Appropriate_technology");
$div_wikipedia_Appropriate_technology->content = <<<HTML
	<p>Appropriate technology is a movement (and its manifestations) encompassing technological choice and application
	that is small-scale, affordable by locals, decentralized, labor-intensive, energy-efficient, environmentally sustainable, and locally autonomous.
	It was originally articulated as intermediate technology by the economist Ernst Friedrich "Fritz" Schumacher in his work Small Is Beautiful.
	Both Schumacher and many modern-day proponents of appropriate technology also emphasize the technology as people-centered.</p>
	HTML;


$page->parent('fair_share.html');
$page->template("stub");
$page->body($div_introduction);

$page->body('ernst_friedrich_schumacher.html');

$page->body($div_wikipedia_Appropriate_technology);
