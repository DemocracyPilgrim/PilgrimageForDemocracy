<?php
$page = new Page();
$page->h1("United Nations Office for the Coordination of Humanitarian Affairs (OCHA)");
$page->keywords("United Nations Office for the Coordination of Humanitarian Affairs", "OCHA");
$page->tags("Organisation");
$page->stars(0);

//$page->snp("description", "");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;



$div_OCHA_website = new WebsiteContentSection();
$div_OCHA_website->setTitleText("OCHA website ");
$div_OCHA_website->setTitleLink("https://www.unocha.org/");
$div_OCHA_website->content = <<<HTML
	<p>We support humanitarian organizations to respond effectively to the needs of people caught in crises,
	to understand and analyse their needs, and to mobilize international assistance.
	We provide tools and services to help humanitarian organizations ensure that no one affected by a crisis is left behind.</p>
	HTML;



$div_wikipedia_United_Nations_Office_for_the_Coordination_of_Humanitarian_Affairs = new WikipediaContentSection();
$div_wikipedia_United_Nations_Office_for_the_Coordination_of_Humanitarian_Affairs->setTitleText("United Nations Office for the Coordination of Humanitarian Affairs");
$div_wikipedia_United_Nations_Office_for_the_Coordination_of_Humanitarian_Affairs->setTitleLink("https://en.wikipedia.org/wiki/United_Nations_Office_for_the_Coordination_of_Humanitarian_Affairs");
$div_wikipedia_United_Nations_Office_for_the_Coordination_of_Humanitarian_Affairs->content = <<<HTML
	<p>The United Nations Office for the Coordination of Humanitarian Affairs (OCHA)
	is a United Nations (UN) body established in December 1991 by the General Assembly
	to strengthen the international response to complex emergencies and natural disasters.</p>
	HTML;


$page->parent('united_nations.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_OCHA_website);
$page->body('centre_for_humanitarian_data.html');
$page->body($div_wikipedia_United_Nations_Office_for_the_Coordination_of_Humanitarian_Affairs);
