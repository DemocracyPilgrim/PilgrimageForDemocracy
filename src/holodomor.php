<?php
$page = new Page();
$page->h1("Holodomor");
$page->tags("International", "Ukraine");
$page->keywords("Holodomor");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Holodomor = new WikipediaContentSection();
$div_wikipedia_Holodomor->setTitleText("Holodomor");
$div_wikipedia_Holodomor->setTitleLink("https://en.wikipedia.org/wiki/Holodomor");
$div_wikipedia_Holodomor->content = <<<HTML
	<p>The Holodomor also known as the Terror-Famine or the Great Famine,
	was a man-made famine in Soviet Ukraine from 1932 to 1933 that killed millions of Ukrainians.</p>
	HTML;


$page->parent('ukraine.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Holodomor");
$page->body($div_wikipedia_Holodomor);
