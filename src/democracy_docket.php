<?php
$page = new Page();
$page->h1("Democracy Docket");
$page->keywords("Democracy Docket");
$page->tags("Institutions: Judiciary", "USA", "Elections", "Organisation", "Marc Elias");
$page->stars(0);

//$page->snp("description", "");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Democracy docket was founded by ${'Marc Elias'}.</p>
	HTML;


$div_Democracy_Docket_website = new WebsiteContentSection();
$div_Democracy_Docket_website->setTitleText("Democracy Docket website ");
$div_Democracy_Docket_website->setTitleLink("https://www.democracydocket.com/");
$div_Democracy_Docket_website->content = <<<HTML
	<p>Democracy Docket is the leading progressive news platform for information, analysis and opinion
	about voting rights and elections in the courts.</p>

	<p>Founded in 2020, Democracy Docket is a reliable source of news that delivers detailed analysis and expert commentary
	on voting rights and election litigation and policy that will shape our elections and democratic institutions for years to come.</p>

	<p>With a comprehensive database of over 650 cases,
	Democracy Docket tracks and reports on the latest election and voting-related litigation,
	providing filings, in-depth analysis and up-to-date developments about court proceedings.</p>
	HTML;



$div_wikipedia_Democracy_Docket = new WikipediaContentSection();
$div_wikipedia_Democracy_Docket->setTitleText("Democracy Docket");
$div_wikipedia_Democracy_Docket->setTitleLink("https://en.wikipedia.org/wiki/Democracy_Docket");
$div_wikipedia_Democracy_Docket->content = <<<HTML
	<p>Democracy Docket is a liberal-leaning voting rights and media platform that tracks election litigation.
	It was founded in 2020 by lawyer Marc Elias and is owned by Democracy Docket, LLC.
	Elias launched Democracy Docket on March 5, 2020, with the stated goal of educating the public on voting rights and redistricting litigation.
	Elias was concerned that the Republican Party would have a newfound freedom in its efforts,
	as a court order prohibiting the party from past voter suppression tactics had expired.</p>
	HTML;


$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_Democracy_Docket_website);
$page->related_tag("Democracy Docket");
$page->body($div_wikipedia_Democracy_Docket);
