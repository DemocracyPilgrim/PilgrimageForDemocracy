<?php
$page = new Page();
$page->h1('Committee to Protect Journalists');
$page->keywords('Committee to Protect Journalists', 'CPJ');
$page->tags("Organisation", "Media");
$page->stars(0);

//$page->snp('description', '');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>The Committee to Protect Journalists (CPJ), publishes the ${'Global Impunity Index'}.</p>
	HTML;


$div_Committe_to_Protect_Journalists_website = new WebsiteContentSection();
$div_Committe_to_Protect_Journalists_website->setTitleText('Committe to Protect Journalists website ');
$div_Committe_to_Protect_Journalists_website->setTitleLink('https://cpj.org/');
$div_Committe_to_Protect_Journalists_website->content = <<<HTML
	<p>The Committee to Protect Journalists promotes press freedom worldwide
	and defends the right of journalists to report the news safely and without fear of reprisal.
	CPJ protects the free flow of news and commentary by taking action wherever journalists are under threat.</p>
	HTML;


$div_wikipedia_Committee_to_Protect_Journalists = new WikipediaContentSection();
$div_wikipedia_Committee_to_Protect_Journalists->setTitleText('Committee to Protect Journalists');
$div_wikipedia_Committee_to_Protect_Journalists->setTitleLink('https://en.wikipedia.org/wiki/Committee_to_Protect_Journalists');
$div_wikipedia_Committee_to_Protect_Journalists->content = <<<HTML
	<p>The Committee to Protect Journalists (CPJ) is an American independent non-profit, non-governmental organization,
	based in New York City, with correspondents around the world.
	CPJ promotes press freedom and defends the rights of journalists.
	The American Journalism Review has called the organization, "Journalism's Red Cross."
	Since late 1980s, the organization has been publishing an annual census of journalists killed or imprisoned in relation to their work.</p>
	HTML;


$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_Committe_to_Protect_Journalists_website);
$page->body('global_impunity_index.html');
$page->body('media.html');

$page->body($div_wikipedia_Committee_to_Protect_Journalists);
