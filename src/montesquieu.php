<?php
$page = new Page();
$page->h1("Montesquieu");
$page->alpha_sort("Montesquieu");
$page->tags("Person");
$page->keywords("Montesquieu");
$page->stars(1);
$page->viewport_background("/free/montesquieu.png");

$page->snp("description", "18th century French political philosopher");
$page->snp("image",       "/free/montesquieu.1200-630.png");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p> In <em>The Spirit of Laws</em>, published in 1748, Montesquieu advocated separating and balancing powers
	between the executive, $legislative, and $judicial ${'branches of government'} as a means of guaranteeing the $freedom of the $individual.
	This doctrine also helped to form the philosophical basis for the $US $Constitution,
	with its division of power among the presidency, the Congress, and the judiciary.</p>
	HTML;

$div_wikipedia_Montesquieu = new WikipediaContentSection();
$div_wikipedia_Montesquieu->setTitleText("Montesquieu");
$div_wikipedia_Montesquieu->setTitleLink("https://en.wikipedia.org/wiki/Montesquieu");
$div_wikipedia_Montesquieu->content = <<<HTML
	<p>Charles Louis de Secondat, Baron de La Brède et de Montesquieu (18 January 1689 – 10 February 1755),
	generally referred to as simply Montesquieu, was a French judge, man of letters, historian, and political philosopher.</p>

	<p>He is the principal source of the theory of separation of powers, which is implemented in many constitutions throughout the world.
	He is also known for doing more than any other author to secure the place of the word despotism in the political lexicon.
	His anonymously published The Spirit of Law (1748), which was received well in both Great Britain and the American colonies,
	influenced the Founding Fathers of the United States in drafting the U.S. Constitution.</p>
	HTML;


$page->parent('list_of_people.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_wikipedia_Montesquieu);
