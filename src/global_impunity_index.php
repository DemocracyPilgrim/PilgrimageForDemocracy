<?php
$page = new Page();
$page->h1("Global Impunity Index");
$page->keywords("Global Impunity Index");
$page->stars(0);

$page->snp("description", "Unsolved journalist murders.");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p>The Global Impunity Index, published by the Committee to Protect Journalists (CPJ),
	calculates unsolved journalist murders as a percentage of a country's population.</p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>The Global Impunity Index, published by the ${'Committee to Protect Journalists'} (CPJ),
	calculates unsolved journalist murders as a percentage of a $country's population.</p>
	HTML;


$page->parent('list_of_indices.html');
$page->template("stub");
$page->body($div_introduction);

$page->body('committee_to_protect_journalists.html');
