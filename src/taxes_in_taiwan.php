<?php
$page = new Page();
$page->h1('Taxes in Taiwan');
$page->tags("Taiwan", "Taxes");
$page->stars(0);

//$page->snp('description', '');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );

$r1 = $page->ref('https://www.taipeitimes.com/News/taiwan/archives/2023/11/11/2003809008', 'Recycling fees for plastic products to be subsidized');


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>$Taiwan, the Republic of China, is well known around the world for being one of the most vibrant $democracies in the world,
	standing up to the autocratic nature of the ${"People's Republic of China"}.</p>

	<p>Taiwan also stands out for some interesting, progressive tax system
	which, while not enough, go in the right direction in terms of ethical and progressive taxes.</p>
	HTML;


$div_codeberg_Taxes_in_Taiwan_Republic_of_China = new CodebergContentSection();
$div_codeberg_Taxes_in_Taiwan_Republic_of_China->setTitleText('Taxes in Taiwan, Republic of China');
$div_codeberg_Taxes_in_Taiwan_Republic_of_China->setTitleLink('https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy/issues/50');
$div_codeberg_Taxes_in_Taiwan_Republic_of_China->content = <<<HTML
	<p>Investigate the global budget of the Republic of China, and the sources of income by taxes.
	What is the list of major taxes in Taiwan, and the percentage of revenue each represents for the governments, local and national.</p>
	HTML;

$div_Land_value_tax = new ContentSection();
$div_Land_value_tax->content = <<<HTML
	<h3>Land value tax</h3>

	<p>Taiwan is one of the few countries around the world to have an effective ${'land value tax'}.</p>
	HTML;


$div_recycling_and_clean_up_treatment_fees = new ContentSection();
$div_recycling_and_clean_up_treatment_fees->content = <<<HTML
	<h3>Recycling and clean-up treatment fees</h3>

	<p>Taiwan’s waste recycling sector is built upon the concept of extended producer responsibility,
	collecting recycling and clean-up treatment fees at the source, whether that be a manufacturer or an importer.</p>

	<p>Fees are NT$7 to NT$10 per kilogram of product category that a firm makes or imports.</p>

	<p>The Resource Circulation Administration is charging manufacturers and importers a recycling and clean-up treatment fee
	for all plastic containers.
	The income is then used to finance consumer waste collecting, processing and recycling.</p>

	<p>They offer a discount on plastic containers made of recycled plastic.$r1
	Products made with at least 25% recycled materials, as well as those made from a single material,
	with no added colouring and limited labelling, are eligible for the discount.</p>
	HTML;




$div_ROC_Ministry_of_Finance_website = new WebsiteContentSection();
$div_ROC_Ministry_of_Finance_website->setTitleText("ROC Ministry of Finance (English website)");
$div_ROC_Ministry_of_Finance_website->setTitleLink("https://www.mof.gov.tw/Eng");



$div_ROC_Taxation_Administration_website = new WebsiteContentSection();
$div_ROC_Taxation_Administration_website->setTitleText("ROC Taxation Administration (English website)");
$div_ROC_Taxation_Administration_website->setTitleLink("https://www.dot.gov.tw/Eng");



$page->parent('taxes.html');
$page->template("stub");
$page->body($div_introduction);

$page->body($div_Land_value_tax);
$page->body('land_value_tax.html');

$page->body($div_recycling_and_clean_up_treatment_fees);

$page->body($div_codeberg_Taxes_in_Taiwan_Republic_of_China);

$page->body($div_ROC_Ministry_of_Finance_website);
$page->body($div_ROC_Taxation_Administration_website);
