<?php
$page = new Page();
$page->h1("Constitution of India");
$page->tags("Institutions: Legislative", "Constitution", "India");
$page->keywords("Constitution of India");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

$r1 = $page->ref("https://www.constitutionofindia.net/about-us/", "ConstitutionofIndia: About Us.");



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;




$div_Constituent_Assembly_Draft_Making_Debates = new WebsiteContentSection();
$div_Constituent_Assembly_Draft_Making_Debates->setTitleText("Constituent Assembly Draft Making Debates ");
$div_Constituent_Assembly_Draft_Making_Debates->setTitleLink("https://eparlib.nic.in/handle/123456789/760448");
$div_Constituent_Assembly_Draft_Making_Debates->content = <<<HTML
	<p>
	</p>
	HTML;



$div_Constituent_Assembly_Legislative_Debates = new WebsiteContentSection();
$div_Constituent_Assembly_Legislative_Debates->setTitleText("Constituent Assembly Legislative Debates ");
$div_Constituent_Assembly_Legislative_Debates->setTitleLink("https://eparlib.nic.in/handle/123456789/4");
$div_Constituent_Assembly_Legislative_Debates->content = <<<HTML
	<p>
	</p>
	HTML;



$div_wikipedia_Constitution_of_India = new WikipediaContentSection();
$div_wikipedia_Constitution_of_India->setTitleText("Constitution of India");
$div_wikipedia_Constitution_of_India->setTitleLink("https://en.wikipedia.org/wiki/Constitution_of_India");
$div_wikipedia_Constitution_of_India->content = <<<HTML
	<p>The Constitution of India is the supreme legal document of India. The document lays down the framework that demarcates fundamental political code, structure, procedures, powers, and duties of government institutions and sets out fundamental rights, directive principles, and the duties of citizens. It is the longest written national constitution in the world.</p>
	HTML;

$div_wikipedia_Constituent_Assembly_of_India = new WikipediaContentSection();
$div_wikipedia_Constituent_Assembly_of_India->setTitleText("Constituent Assembly of India");
$div_wikipedia_Constituent_Assembly_of_India->setTitleLink("https://en.wikipedia.org/wiki/Constituent_Assembly_of_India");
$div_wikipedia_Constituent_Assembly_of_India->content = <<<HTML
	<p>The Constituent Assembly of India was partly elected and partly nominated body to frame the Constitution of India. It was elected by the Provincial assemblies of British India following the Provincial Assembly elections held in 1946 and nominated by princely states. After India's independence from the British in August 1947, its members served as the nation's 'Provisional Parliament', as well as the Constituent Assembly.</p>
	HTML;

$div_Constitution_of_India = new WebsiteContentSection();
$div_Constitution_of_India->setTitleText("Constitution of India .net");
$div_Constitution_of_India->setTitleLink("https://www.constitutionofindia.net/");
$div_Constitution_of_India->content = <<<HTML
	<p>Building a robust constitutional culture in India through a shared understanding of constitutional origins. $r1</p>
	HTML;



$page->parent('constitution.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Constitution of India");
$page->body($div_Constituent_Assembly_Draft_Making_Debates);
$page->body($div_Constituent_Assembly_Legislative_Debates);
$page->body($div_Constitution_of_India);
$page->body($div_wikipedia_Constitution_of_India);
$page->body($div_wikipedia_Constituent_Assembly_of_India);
