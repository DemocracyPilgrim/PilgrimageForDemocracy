<?php
$page = new Page();
$page->h1("Verified Voting Foundation");
$page->tags("Organisation", "Elections", "Election Integrity", "Open-source Voting Systems");
$page->keywords("Verified Voting Foundation");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>The Verified Voting Foundation was founded by ${'David Dill'}.</p>
	HTML;


$div_Verified_Voting = new WebsiteContentSection();
$div_Verified_Voting->setTitleText("Verified Voting ");
$div_Verified_Voting->setTitleLink("https://verifiedvoting.org/");
$div_Verified_Voting->content = <<<HTML
	<p>Verified Voting’s mission is to strengthen democracy for all voters by promoting the responsible use of technology in elections.</p>

	<p>Since its founding, our team, Board of Directors, and Board of Advisors have been leading experts on relevant issues in election technology.
	Our collective technical knowledge has been foundational in understanding where vulnerabilities lie in our elections
	and in promoting policies and best practices that mitigate risks.</p>

	<p>We work to understand other experts in the field – experts in election administration because context matters for policy recommendations,
	experts in diverse voter experiences because not every voter has access to the ballot in the same way,
	and experts in voter outreach and education because ultimately this work is done for voters.
	The U.S. has moved toward broader and fairer access to voting, but not without contention and backlash.
	In this struggle, we are not neutral: we stand with voters.
	We repudiate attacks on voting rights couched as security measures and believe that security, resilience, and verifiability are integral to free and fair elections.
	And we recognize that some uses of election technology can systematically disenfranchise voters with disabilities, rural voters, and voters of color.</p>

	<p>We work with election officials, policymakers, and democracy defenders across party lines to help voters vote
	and to promote policies that support justified public confidence in elections.</p>
	HTML;



$div_wikipedia_Verified_Voting_Foundation = new WikipediaContentSection();
$div_wikipedia_Verified_Voting_Foundation->setTitleText("Verified Voting Foundation");
$div_wikipedia_Verified_Voting_Foundation->setTitleLink("https://en.wikipedia.org/wiki/Verified_Voting_Foundation");
$div_wikipedia_Verified_Voting_Foundation->content = <<<HTML
	<p>The Verified Voting Foundation is a non-governmental, nonpartisan organization founded in 2004 by David L. Dill,
	a computer scientist from Stanford University, focused on how technology impacts the administration of US elections.
	The organization's mission is to "strengthen democracy for all voters by promoting the responsible use of technology in elections."
	Verified Voting works with election officials, elected leaders, and other policymakers who are responsible for managing local and state election systems
	to mitigate the risks associated with novel voting technologies.</p>
	HTML;


$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_Verified_Voting);
$page->body($div_wikipedia_Verified_Voting_Foundation);
