<?php
$page = new Page();
$page->h1("Preface: Invoking Democracy — We, the Autocrats");
$page->stars(0);
$page->viewport_background('/free/invoking_democracy.png');

$page->snp("description", "The word 'democracy' is often the first refuge of the tyrant.");
$page->snp("image",       "/free/invoking_democracy.1200-630.png");

$page->preview( <<<HTML
	<p>The word 'democracy' is often the first refuge of the tyrant.
	We examine the deceptive language and actions of those who cloak authoritarianism in democratic garb,
	recognizing that the seeds of autocracy can lie within us all.</p>
	HTML );

$r1 = $page->ref('https://freedomhouse.org/article/autocrats-favorite-word-democracy', 'Autocrats’ Favorite Word? Democracy.');
$r2 = $page->ref('http://en.kremlin.ru/supplement/5770', 'Joint Statement of the Russian Federation and the People’s Republic of China');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>The word 'democracy' is often the first refuge of the tyrant.
	We examine the deceptive language and actions of those who cloak authoritarianism in democratic garb,
	recognizing that the seeds of autocracy can lie within us all.</p>
	HTML;


$div_quotes = new ContentSection();
$div_quotes->content = <<<HTML
	<h3>Quotes</h3>

	<blockquote>
		The sides share the understanding that democracy is a universal human value,
		rather than a privilege of a limited number of states,
		and that its promotion and protection is a common responsibility of the entire world community.<br>
		—
		Joint statement of the ${'Russian Federation'} and the ${"People's Republic of China"},<br>
		February 4, 2022 $r1 $r2
	</blockquote>

	<p>The full statement includes the word "democracy" 12 times.
	It was made mere weeks prior to $Russia's full-scale invasion of $Ukraine in 2022.</p>
	HTML;



$div_freedomhouse_Autocrats_Favorite_Word_Democracy = new FreedomHouseContentSection();
$div_freedomhouse_Autocrats_Favorite_Word_Democracy->setTitleText("Autocrats’ Favorite Word? Democracy.");
$div_freedomhouse_Autocrats_Favorite_Word_Democracy->setTitleLink("https://freedomhouse.org/article/autocrats-favorite-word-democracy");
$div_freedomhouse_Autocrats_Favorite_Word_Democracy->content = <<<HTML
	<p>Authoritarian leaders are using the language of democracy
	in an attempt to conceal the abusive systems through which they cling to power.</p>

	<p>Today, the world’s illiberal leaders are developing ever-more sophisticated ways
	to spread poisoned messages beyond territorial borders.
	In this environment, the ability to recognize and explain the difference between hollow “democratic” rhetoric
	and the tenets of a truly democratic system is fundamental to countering authoritarian abuses,
	and promoting democratic systems where leaders are accountable to voters and fundamental rights and freedoms are upheld.</p>
	HTML;

$list_all_related_topics = new ListOfPages();
$list_all_related_topics->add('dangers.html');
$print_list_all_related_topics = $list_all_related_topics->print();

$div_list_all_related_topics = new ContentSection();
$div_list_all_related_topics->content = <<<HTML
	$print_list_all_related_topics
	HTML;





$page->parent('democracy.html');
$page->next('first_level_of_democracy.html');
$page->template("stub");
$page->body($div_introduction);

$page->body($div_quotes);
$page->body($div_freedomhouse_Autocrats_Favorite_Word_Democracy);


$page->body($div_list_all_related_topics);
