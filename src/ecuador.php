<?php
$page = new CountryPage('Ecuador');
$page->h1('Ecuador');
$page->tags("Country");
$page->keywords('Ecuador');
$page->stars(1);

$page->preview( <<<HTML
	<p></p>
	HTML );

$page->snp('description', '18 million inhabitants.');
//$page->snp('image',       '/copyrighted/');


$r1 = $page->ref('https://cpj.org/reports/2023/06/ecuador-on-edge-political-paralysis-and-spiking-crime-pose-new-threats-to-press-freedom/', 'Ecuador on edge: Political paralysis and spiking crime pose new threats to press freedom');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Fernando Villavicencio, a prominent $anticorruption candidate to the 2023 presidential elections in Ecuador
	was assassinated in August, just prior to the election.</p>
	HTML;


$div_CPJ_Ecuador = new WebsiteContentSection();
$div_CPJ_Ecuador->setTitleText('Committee to Protect Journalists: Ecuador profile page');
$div_CPJ_Ecuador->setTitleLink('https://cpj.org/americas/ecuador/');
$div_CPJ_Ecuador->content = <<<HTML
	<p>In Ecuador, political turmoil and a deepening security crisis are putting reporters and press freedom at increasing risk.
	President Guillermo Lasso, who dissolved the country’s National Assembly in May as it took steps to impeach him,
	has proved ineffective at stemming the rise in violent crime and journalists are watching uneasily
	as the party of the most anti-press politician in recent memory,
	former President Rafael Correa, seeks to increase its power in upcoming August elections. $r1</p>
	HTML;


$div_wikipedia_Ecuador = new WikipediaContentSection();
$div_wikipedia_Ecuador->setTitleText('Ecuador');
$div_wikipedia_Ecuador->setTitleLink('https://en.wikipedia.org/wiki/Ecuador');
$div_wikipedia_Ecuador->content = <<<HTML
	<p>The Republic of Ecuador is a country in northwestern South America,
	bordered by Colombia on the north, Peru on the east and south, and the Pacific Ocean on the west.</p>
	HTML;

$div_wikipedia_History_of_Ecuador_1990_present = new WikipediaContentSection();
$div_wikipedia_History_of_Ecuador_1990_present->setTitleText('History of Ecuador 1990 present');
$div_wikipedia_History_of_Ecuador_1990_present->setTitleLink('https://en.wikipedia.org/wiki/History_of_Ecuador_(1990–present)');
$div_wikipedia_History_of_Ecuador_1990_present->content = <<<HTML
	<p>History of Ecuador since 1990, after its return to democracy in 1979.</p>
	HTML;

$div_wikipedia_Rights_of_nature_in_Ecuador = new WikipediaContentSection();
$div_wikipedia_Rights_of_nature_in_Ecuador->setTitleText('Rights of nature in Ecuador');
$div_wikipedia_Rights_of_nature_in_Ecuador->setTitleLink('https://en.wikipedia.org/wiki/Rights_of_nature_in_Ecuador');
$div_wikipedia_Rights_of_nature_in_Ecuador->content = <<<HTML
	<p>With the adoption of a new constitution in 2008 under president Rafael Correa,
	Ecuador became the first country in the world to enshrine a set of codified Rights of Nature
	and to inform a more clarified content to those rights.</p>
	HTML;

$div_wikipedia_Politics_of_Ecuador = new WikipediaContentSection();
$div_wikipedia_Politics_of_Ecuador->setTitleText('Politics of Ecuador');
$div_wikipedia_Politics_of_Ecuador->setTitleLink('https://en.wikipedia.org/wiki/Politics_of_Ecuador');
$div_wikipedia_Politics_of_Ecuador->content = <<<HTML
	<p>The politics of Ecuador are multi-party.
	The central government polity is a quadrennially elected presidential, unicameral representative democracy.</p>
	HTML;


$page->parent('world.html');
$page->template("stub");

$page->body($div_introduction);
$page->body($div_CPJ_Ecuador);

$page->body('Country indices');

$page->body($div_wikipedia_Ecuador);
$page->body($div_wikipedia_History_of_Ecuador_1990_present);
$page->body($div_wikipedia_Rights_of_nature_in_Ecuador);
$page->body($div_wikipedia_Politics_of_Ecuador);
