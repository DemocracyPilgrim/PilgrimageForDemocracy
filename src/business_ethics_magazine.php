<?php
$page = new Page();
$page->h1("Business Ethics Magazine");
$page->keywords("Business Ethics Magazine");
$page->tags("Organisation");
$page->stars(0);

//$page->snp("description", "");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;



$div_Business_Ethics_Magazine_website = new WebsiteContentSection();
$div_Business_Ethics_Magazine_website->setTitleText("Business Ethics Magazine website ");
$div_Business_Ethics_Magazine_website->setTitleLink("https://business-ethics.com/");
$div_Business_Ethics_Magazine_website->content = <<<HTML
	<p>Business Ethics is an online magazine
	with a strong heritage in the fields of ethics, governance, corporate responsibility and socially responsible investing.</p>

	<p>Now available only on the web, Business Ethics was launched in 1987 and published for 20 years as a quarterly print magazine.
	The mission of Business Ethics – now, as then – is “to promote ethical business practices,
	to serve that growing community of professionals and individuals striving to work and invest in responsible ways.”</p>

	<p>We seek to do that by offering our readers information, opinion and cutting-edge analysis
	about business and the intersection of business and society.</p>
	</p>
	HTML;



$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_Business_Ethics_Magazine_website);
