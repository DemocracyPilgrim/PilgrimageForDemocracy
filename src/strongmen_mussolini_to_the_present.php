<?php
$page = new Page();
$page->h1('Strongmen: Mussolini to the Present');
$page->tags("Book", "Donald Trump");
$page->stars(0);

//$page->snp('description', '');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p>From the 20th century to the 21st, how autocratic leaders used propaganda, virility, corruption and violence to stay in power...</p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>"Strongmen: Mussolini to the Present" is a book by ${'Ruth Ben-Ghiat'}.</p>
	HTML;



$div_Strongmen = new WebsiteContentSection();
$div_Strongmen->setTitleText("Ruth Ben-Ghiat: Strongmen");
$div_Strongmen->setTitleLink("https://ruthbenghiat.com/strongmen/");
$div_Strongmen->content = <<<HTML
	<p>Ours is the age of the strongman, of heads of state who damage or destroy democracy use masculinity as a tool of political legitimacy,
	and promise law and order rule – and then legitimize lawless behavior by financial, sexual, and other predators.
	Covering a century of tyranny, this book examines how authoritarians use propaganda, virility, corruption,
	and violence to stay in power, and how they can be opposed.</p>
	HTML;



$div_Strongmen_review_a_chilling_history_for_one_nation_no_longer_under_Trump = new WebsiteContentSection();
$div_Strongmen_review_a_chilling_history_for_one_nation_no_longer_under_Trump->setTitleText("The Guardian: Strongmen review: a chilling history for one nation no longer under Trump");
$div_Strongmen_review_a_chilling_history_for_one_nation_no_longer_under_Trump->setTitleLink("https://www.theguardian.com/us-news/2020/nov/26/strongmen-review-ruth-ben-ghiat-donald-trump-fascism-hitler-mussolini-franco");
$div_Strongmen_review_a_chilling_history_for_one_nation_no_longer_under_Trump->content = <<<HTML
	<p>(2020) This terrific history of strongmen since Mussolini makes it clear that
	despite a horrific pandemic and massive economic disruption,
	ordinary democratic Americans have more to be thankful for this Thanksgiving than ever before.<p>

	<p>Comparing the gruesome, granular details of the reigns of Mussolini, Franco, Hitler, Gaddafi, Pinochet, Mobuto, Berlusconi and Erdoğan
	to the acts and aspirations of Donald Trump, New York University professor Ruth Ben-Ghiat makes a powerful argument that
	on the scary road to fascism, America just came perilously close to the point of no return.</p>

	<p>Almost everything Trump has done has come straight from the authoritarian playbook.
	Every dictator, for example, has built on the accomplishments of his predecessors. (...)</p>
	HTML;



$page->parent('list_of_books.html');
$page->template("stub");
$page->body($div_introduction);
$page->body($div_Strongmen);

$page->body('ruth_ben_ghiat.html');
$page->body($div_Strongmen_review_a_chilling_history_for_one_nation_no_longer_under_Trump);
