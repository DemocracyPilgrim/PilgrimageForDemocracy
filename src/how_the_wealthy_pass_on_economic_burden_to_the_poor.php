<?php
$page = new Page();
$page->h1("How the wealthy pass on economic burden to the poor");
$page->tags("Living: Fair Income", "Economic Injustice", "Taxes", "Poverty", "Orwellian Doublespeak");
$page->keywords("how the wealthy pass on economic burden to the poor");
$page->stars(1);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

$r1 = $page->ref("https://www.youtube.com/watch?v=PC9hz83TxpM", "Democracy Watch episode 181 (3'00)");
// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;



$div_Orwellian_Doublespeak = new ContentSection();
$div_Orwellian_Doublespeak->content = <<<HTML
	<h3>Orwellian Doublespeak</h3>

	<p>How wealthy people use ${'Orwellian Doublespeak'} to make middle class and poor people like what would favour the wealthy and dislike what would favour the poor.
	See for example the so-called ${'Death Tax'}.</p>
	HTML;


$div_Buying_off_legal_opposition = new ContentSection();
$div_Buying_off_legal_opposition->content = <<<HTML
	<h3>Buying off legal opposition</h3>

	<p>Pro-democracy $lawyer ${'Marc Elias'} and his firm were so good at fighting the frivolous voter suppression lawsuits filed by Republican State officials,
	that a Fox News anchor suggested on air that they ought to pay Elias 500 million dollars to win him over to their side!
	Marc Elias declined the offer. $r1</p>

	<p>This type of scenario played out during the 2008 Financial Crisis.
	In its very early stage, the financial crisis started in Ireland where bank deregulation started the snowball effect which eventually reached the USA.
	Poorly paid Irish government lawyers were fighting off, often alone, a whole roaster of very rich banking sector private lawyers
	who wanted deregulation for their clients.
	Whenever a government lawyer was any good, the banks paid him off to win them over to their side.</p>
	HTML;



$div_Favourable_Tax_Structures = new ContentSection();
$div_Favourable_Tax_Structures->content = <<<HTML
	<h3>Favourable Tax Structures</h3>

	<p>See ${'Death Tax'} and the articles linked below.</p>
	HTML;




$div_The_super_rich_pay_lower_taxes_than_you_and_here_s_how_they_do_it = new WebsiteContentSection();
$div_The_super_rich_pay_lower_taxes_than_you_and_here_s_how_they_do_it->setTitleText("The super-rich pay lower taxes than you – and here’s how they do it…");
$div_The_super_rich_pay_lower_taxes_than_you_and_here_s_how_they_do_it->setTitleLink("https://views-voices.oxfam.org.uk/2023/01/how-super-rich-pay-lower-taxes-than-you/");
$div_The_super_rich_pay_lower_taxes_than_you_and_here_s_how_they_do_it->content = <<<HTML
	<p>Oxfam’s report for this week’s Davos gathering traces the remarkable decline in taxes on the rich – and the explosion in wealth of the top 1 percent that has followed. In this blog, I want to unpack why the top 1 percent often get away with much lower taxes in proportion to their income and wealth than the rest of us. The crucial point is that this is not just about falling tax rates over the past decades: in fact, the rich also tend to pay less because the forms of income they often rely on are taxed much lower than the income of a typical person who has to rely on a salary.</p>
	HTML;



$div_Ten_Ways_Billionaires_Avoid_Taxes_on_an_Epic_Scale = new WebsiteContentSection();
$div_Ten_Ways_Billionaires_Avoid_Taxes_on_an_Epic_Scale->setTitleText(" Ten Ways Billionaires Avoid Taxes on an Epic Scale");
$div_Ten_Ways_Billionaires_Avoid_Taxes_on_an_Epic_Scale->setTitleLink("https://www.propublica.org/article/billionaires-tax-avoidance-techniques-irs-files");
$div_Ten_Ways_Billionaires_Avoid_Taxes_on_an_Epic_Scale->content = <<<HTML
	<p>After a year of reporting on the tax machinations of the ultrawealthy, ProPublica spotlights the top tax-avoidance techniques that provide massive benefits to billionaires.</p>
	HTML;



$page->parent('economic_injustice.html');
$page->template("stub");
$page->body($div_introduction);

$page->body($div_Orwellian_Doublespeak);
$page->body($div_Buying_off_legal_opposition);
$page->body($div_Favourable_Tax_Structures);


$page->body($div_Ten_Ways_Billionaires_Avoid_Taxes_on_an_Epic_Scale);
$page->body($div_The_super_rich_pay_lower_taxes_than_you_and_here_s_how_they_do_it);
