<?php
$page = new Page();
$page->h1("List of videos, interviews, TV events and news clips");
$page->tags("Documentary", "Information: Media");
$page->keywords("list of videos", "Video: Interview", "Video: News Story", "Video: Speech", "Video: Podcast");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;


$page->parent('lists.html');
$page->body($div_introduction);


$div_news_story = new ContentSection();
$div_news_story->content = <<<HTML
	<h3>News stories</h3>

	<p>
	</p>
	HTML;

$div_Interviews = new ContentSection();
$div_Interviews->content = <<<HTML
	<h3>Interviews</h3>

	<p>
	</p>
	HTML;

$div_Speeches = new ContentSection();
$div_Speeches->content = <<<HTML
	<h3>Speeches</h3>

	<p>
	</p>
	HTML;


$div_Podcast = new ContentSection();
$div_Podcast->content = <<<HTML
	<h3>Podcast</h3>

	<p>
	</p>
	HTML;


$page->related_tag("Video: News Story", $div_news_story);
$page->related_tag("Video: Interview",  $div_Interviews);
$page->related_tag("Video: Speech",     $div_Speeches);
$page->related_tag("Video: Podcast",    $div_Podcast);

$page->body('project/videos_tv_programs_interviews_and_documentaries.html');
