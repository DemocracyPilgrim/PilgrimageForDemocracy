<?php
$page = new Page();
$page->h1('United Nations Regional Information Centre for Western Europe');
$page->keywords('United Nations Regional Information Centre', 'UNRIC');
$page->tags("Organisation");
$page->stars(0);

//$page->snp('description', '');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;


$div_UNRIC_website = new WebsiteContentSection();
$div_UNRIC_website->setTitleText('UNRIC website ');
$div_UNRIC_website->setTitleLink('https://unric.org/en/');
$div_UNRIC_website->content = <<<HTML
	<p>The United Nations Regional Information Centre for Western Europe (UNRIC) is located in Brussels, the capital of Europe.
	Its mission is to communicate the values, history and mandate of the United Nations and
	its actions in building a more peaceful, fair and sustainable world.</p>
	HTML;


$div_wikipedia_United_Nations_Regional_Information_Centre_for_Western_Europe = new WikipediaContentSection();
$div_wikipedia_United_Nations_Regional_Information_Centre_for_Western_Europe->setTitleText('United Nations Regional Information Centre for Western Europe');
$div_wikipedia_United_Nations_Regional_Information_Centre_for_Western_Europe->setTitleLink('https://en.wikipedia.org/wiki/United_Nations_Regional_Information_Centre_for_Western_Europe');
$div_wikipedia_United_Nations_Regional_Information_Centre_for_Western_Europe->content = <<<HTML
	<p>The United Nations Regional Information Centre (UNRIC) is one of 63 ${'United Nations'} Information Centres (UNICs) around the world.
	Their main task is to spread the UN message, raise awareness and create understanding of issues relating to the United Nations' objectives.
	UNRIC serves the Western European Region by providing and disseminating
	UN information material, UN reports and documents, press kits, posters, fact sheets and brochures.</p>
	HTML;


$page->parent('united_nations.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_UNRIC_website);
$page->body($div_wikipedia_United_Nations_Regional_Information_Centre_for_Western_Europe);
