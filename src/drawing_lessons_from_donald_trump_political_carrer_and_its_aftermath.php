<?php
$page = new Page();
$page->h1("Drawing lessons from Donald Trump political carrer and its aftermath");
$page->tags("Society", "Donald Trump", "Donald Trump: Research", "WIP");
$page->keywords("Drawing lessons from Donald Trump political carrer and its aftermath");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;


$div_voting_method = new ContentSection();
$div_voting_method->content = <<<HTML
	<h3>voting method</h3>

	<p>Around 70 million people will vote for ${'Donald Trump'} in November 2024, many millions of which will do so holding their nose, understanding full well Trump's personal flaws.
	Given ${"Duverger's Law"}, those millions of people, who cannot bring themselves to vote for a Democratic candidate
	(whatever the reasons) are deprived from the opportunity to vote for decent conservative Republicans.</p>

	<p>Those millions of Trump voters would have found a better outlet for expressing their values if the USA had a better ${'voting method'} in place
	like ${'Approval Voting'}.</p>
	HTML;


$page->parent('society.html');
$page->template("stub");
$page->body($div_introduction);



$page->body($div_voting_method);
