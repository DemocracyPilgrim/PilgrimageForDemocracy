<?php
$page = new Page();
$page->h1("EU Tax Observatory");
$page->keywords("EU Tax Observatory");
$page->tags("Organisation", "Taxes");
$page->stars(0);

//$page->snp("description", "");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref('', '');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>See also: $taxes.</p>
	HTML;



$div_EU_Tax_Observatory_website = new WebsiteContentSection();
$div_EU_Tax_Observatory_website->setTitleText("EU Tax Observatory website ");
$div_EU_Tax_Observatory_website->setTitleLink("https://www.taxobservatory.eu/");
$div_EU_Tax_Observatory_website->content = <<<HTML
	<p>The EU Tax Observatory is an independent research laboratory hosted at the Paris School of Economics.
	It conducts innovative research on taxation, contributes to a democratic and inclusive debate on the future of taxation,
	and fosters a dialogue between the scientific community, civil society, and policymakers in the European Union and worldwide.</p>
	HTML;



$div_wikipedia_EU_Tax_Observatory = new WikipediaContentSection();
$div_wikipedia_EU_Tax_Observatory->setTitleText("EU Tax Observatory");
$div_wikipedia_EU_Tax_Observatory->setTitleLink("https://en.wikipedia.org/wiki/EU_Tax_Observatory");
$div_wikipedia_EU_Tax_Observatory->content = <<<HTML
	<p>The EU Tax Observatory (EUTO, Eutax) is an independent research laboratory
	dedicated to studying tax evasion and avoidance in the European Union, opened June 2021.</p>
	HTML;


$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);

$page->body($div_EU_Tax_Observatory_website);

$page->body($div_wikipedia_EU_Tax_Observatory);
