<?php
$page = new VideoPage();
$page->h1("Brian Tyler Cohen: Trump goes off the rails with insulting message to women");
$page->tags("Video: News Story", "Brian Tyler Cohen", "Donald Trump", "Political Discourse", "President Saviour Syndrome", "Women's Rights");
$page->stars(2);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>The following videos features an excerpt from a speech that ${'Donald Trump'} gave at a September 2024 campaign rally,
	displaying a very marked example of the ${'President as Saviour Syndrome'}.
	In the speech, quoted below, Trump promises to single-handedly resolve just about every women's problem,
	appointing himself as their personal protector.</p>

	<p>${'Brian Tyler Cohen'} discusses the issues of ${"Women's Rights"} with his guest, Mark Cuban.</p>
	HTML;



$div_youtube_Trump_goes_OFF_THE_RAILS_with_DISGUSTING_message_to_women = new YoutubeContentSection();
$div_youtube_Trump_goes_OFF_THE_RAILS_with_DISGUSTING_message_to_women->setTitleText("Trump goes OFF THE RAILS with DISGUSTING message to women");
$div_youtube_Trump_goes_OFF_THE_RAILS_with_DISGUSTING_message_to_women->setTitleLink("https://www.youtube.com/watch?v=tKzF6ywUoR0");
$div_youtube_Trump_goes_OFF_THE_RAILS_with_DISGUSTING_message_to_women->content = <<<HTML
	<p>
	</p>
	HTML;


$div_Quote = new ContentSection();
$div_Quote->content = <<<HTML
	<h3>Quote</h3>

	<blockquote><strong>Donald Trump</strong>:
	I always thought women liked me.
	I never thought I had a problem but the fake news keeps saying women don't like me.
	I don't believe it.
	I think, you know why they like to have strong borders.
	They like to have safety, nothing personal.
	I think they like me but I make this statement - thank you I love you too, I love you too, thank you -
	but I think they like me because I represent something that's very important.
	I make this statement to the great women of our country.
	Sadly women are poorer than they were four years ago, much poorer;
	are less healthy than they were four years ago;
	are less safe on the streets than they were four years ago;
	are paying much higher prices for groceries and everything else than they were four years ago;
	are more stressed and depressed and unhappy than they were four years ago;
	and are less optimistic and confident in the future than they were four years ago.
	I believe that.
	I will fix all of that and fast and at long last this nation and National nightmare will end.
	It will end.
	We've got to end this National nightmare,
	because I am your protector.
	I want to be your protector.
	<strong>As president, I have to be your protector.</strong>
	I hope, you don't make too much of it.
	I hope the fake news does: "Oh, he wants to be their protector!"
	Well I am, as president.
	I have to be your protector.
	I will make you safe at the border, on the sidewalks of your now violent cities,
	in the suburbs where you are under migrant criminal Siege,
	and with our military protecting you from foreign enemies
	of which we have many today because of the incompetent leadership that we have.
	<strong>
	You will no longer be abandoned lonely or scared.
	You will no longer be in danger.
	You're not going to be in danger any longer.
	You will no longer have anxiety from all of the problems our country has today.
	You will be protected and I will be your protector.
	Women will be happy, healthy, confident, and free.
	You will no longer be thinking about abortion.
	</strong>
	It's all they talk about: abortion,
	because we've done something that nobody else could have done.
	It is now where it always had to be with the states and a vote of the people.
	</blockquote>
	HTML;



$page->parent('list_of_videos.html');
$page->body($div_introduction);
$page->body($div_youtube_Trump_goes_OFF_THE_RAILS_with_DISGUSTING_message_to_women);

$page->body($div_Quote);
