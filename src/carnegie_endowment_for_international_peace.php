<?php
$page = new OrganisationPage();
$page->h1("Carnegie Endowment for International Peace");
$page->viewport_background("");
$page->keywords("Carnegie Endowment for International Peace");
$page->stars(0);
$page->tags("Organisation", "International", "Diplomacy", "Think Tank");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;



$div_Carnegie_Endowment_for_International_Peace = new WebsiteContentSection();
$div_Carnegie_Endowment_for_International_Peace->setTitleText("Carnegie Endowment for International Peace ");
$div_Carnegie_Endowment_for_International_Peace->setTitleLink("https://carnegieendowment.org/");
$div_Carnegie_Endowment_for_International_Peace->content = <<<HTML
	<p>The Carnegie Endowment for International Peace generates strategic ideas and independent analysis, supports diplomacy, and trains the next generation of scholar-practitioners to help countries and institutions take on the most difficult global problems and advance peace.</p>
	HTML;



$div_wikipedia_Carnegie_Endowment_for_International_Peace = new WikipediaContentSection();
$div_wikipedia_Carnegie_Endowment_for_International_Peace->setTitleText("Carnegie Endowment for International Peace");
$div_wikipedia_Carnegie_Endowment_for_International_Peace->setTitleLink("https://en.wikipedia.org/wiki/Carnegie_Endowment_for_International_Peace");
$div_wikipedia_Carnegie_Endowment_for_International_Peace->content = <<<HTML
	<p>The Carnegie Endowment for International Peace (CEIP) is a nonpartisan international affairs think tank headquartered in Washington, D.C., with operations in Europe, South Asia, East Asia, and the Middle East, as well as the United States. Founded in 1910 by Andrew Carnegie, the organization describes itself as being dedicated to advancing cooperation between countries, reducing global conflict, and promoting active international engagement between the United States and countries around the world. It engages leaders from multiple sectors and across the political spectrum.</p>
	HTML;


$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Carnegie Endowment for International Peace");
$page->body($div_wikipedia_Carnegie_Endowment_for_International_Peace);

$page->body($div_Carnegie_Endowment_for_International_Peace);
