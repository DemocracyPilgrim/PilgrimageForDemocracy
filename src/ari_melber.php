<?php
$page = new PersonPage();
$page->h1("Ari Melber");
$page->alpha_sort("Melber, Ari");
$page->tags("Person", "Information: Media");
$page->keywords("Ari Melber");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Ari_Melber = new WikipediaContentSection();
$div_wikipedia_Ari_Melber->setTitleText("Ari Melber");
$div_wikipedia_Ari_Melber->setTitleLink("https://en.wikipedia.org/wiki/Ari_Melber");
$div_wikipedia_Ari_Melber->content = <<<HTML
	<p>Ari Naftali Melber is an American attorney and Emmy-winning journalist who is the chief legal correspondent for MSNBC and host of The Beat with Ari Melber.</p>
	HTML;


$page->parent('list_of_people.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Ari Melber");
$page->body($div_wikipedia_Ari_Melber);
