<?php
$page = new Page();
$page->h1('Authoritarianism');
$page->keywords('Authoritarianism', 'authoritarianism', 'authoritarian');
$page->stars(0);
$page->tags("Danger");

//$page->snp('description', '');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p>List of topics related to authoritarianism and autocratic regimes.</p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Authoritarianism is the organized opposite of $democracy.</p>
	HTML;

$list = new ListOfPages();
$list->add('american_fascism.html');
$list->add('covert_political_repression.html');
$list->add('cyberwarfare.html');
$list->add('fascism.html');
$list->add('political_repression.html');
$list->add('insidious_political_repression.html');
$list->add('covert_political_repression.html');
$list->add('transnational_authoritarianism.html');
$print_list = $list->print();


$list_countries = new ListOfPages();
$list_countries->add('prc_china.html');
$list_countries->add('russia.html');
$list_countries->add('iran.html');
$print_list_countries = $list_countries->print();



$div_list = new ContentSection();
$div_list->content = <<<HTML
	<h3>Related content</h3>

	$print_list

	<h3>Main authoritarian countries</h3>

	$print_list_countries
	HTML;


$div_wikipedia_Authoritarianism = new WikipediaContentSection();
$div_wikipedia_Authoritarianism->setTitleText('Authoritarianism');
$div_wikipedia_Authoritarianism->setTitleLink('https://en.wikipedia.org/wiki/Authoritarianism');
$div_wikipedia_Authoritarianism->content = <<<HTML
	<p>Authoritarianism is a political system characterized by the rejection of political plurality,
	the use of strong central power to preserve the political status quo,
	and reductions in the rule of law, separation of powers, and democratic voting.</p>
	HTML;


$page->parent('external_threats.html');
$page->template("stub");
$page->body($div_introduction);
$page->body($div_list);


$page->body($div_wikipedia_Authoritarianism);
