<?php
$page = new Page();
$page->h1("Immunity");
$page->keywords("immunity", "Immunity");
$page->stars(0);

//$page->snp("description", "");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref('', '');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$list_Immunity = ListOfPeoplePages::WithTags("Immunity");
$print_list_Immunity = $list_Immunity->print();

$div_list_Immunity = new ContentSection();
$div_list_Immunity->content = <<<HTML
	<h3>Related content</h3>

	$print_list_Immunity
	HTML;




$div_Trump_Pressures_Republicans_to_Pass_a_Law_to_Keep_Him_Out_of_Jail_Forever = new WebsiteContentSection();
$div_Trump_Pressures_Republicans_to_Pass_a_Law_to_Keep_Him_Out_of_Jail_Forever->setTitleText("Trump Pressures Republicans to Pass a Law to Keep Him Out of Jail Forever");
$div_Trump_Pressures_Republicans_to_Pass_a_Law_to_Keep_Him_Out_of_Jail_Forever->setTitleLink("https://www.rollingstone.com/politics/politics-features/trump-republicans-pass-law-jail-1235027139/");
$div_Trump_Pressures_Republicans_to_Pass_a_Law_to_Keep_Him_Out_of_Jail_Forever->content = <<<HTML
	<p>The former president ${'Donald Trump'} is convinced state prosecutors will target him again after a second term,
	and wants the GOP to solve that problem for him</p>

	<p>If Trump retakes the presidency this November, a new MAGAfied attorney general can shut down
	Special Counsel Jack Smith’s federal criminal cases against Trump, therefore killing the two planned federal trials,
	neither of which is expected to happen during this election year.
	But in a scenario where Trump defeats Biden in November,
	he would in theory still have criminal charges and a trial waiting for him in Georgia,
	where he would not be able to pardon himself or have his Justice Department quash the case.</p>
	HTML;




$div_wikipedia_Legal_immunity = new WikipediaContentSection();
$div_wikipedia_Legal_immunity->setTitleText("Legal immunity");
$div_wikipedia_Legal_immunity->setTitleLink("https://en.wikipedia.org/wiki/Legal_immunity");
$div_wikipedia_Legal_immunity->content = <<<HTML
	<p>Legal immunity, or immunity from prosecution, is a legal status wherein an individual or entity
	cannot be held liable for a violation of the law,
	in order to facilitate societal aims that outweigh the value of imposing liability in such cases.
	Such legal immunity may be from criminal prosecution,
	or from civil liability (being subject of lawsuit), or both.</p>
	HTML;

$div_wikipedia_Absolute_immunity = new WikipediaContentSection();
$div_wikipedia_Absolute_immunity->setTitleText("Absolute immunity");
$div_wikipedia_Absolute_immunity->setTitleLink("https://en.wikipedia.org/wiki/Absolute_immunity");
$div_wikipedia_Absolute_immunity->content = <<<HTML
	<p>In United States law, absolute immunity is a type of sovereign immunity for government officials
	that confers complete immunity from criminal prosecution and suits for damages,
	so long as officials are acting within the scope of their duties.
	The Supreme Court of the United States has consistently held that government officials deserve some type of immunity from lawsuits for damages,
	and that the common law recognized this immunity.
	The Court reasons that this immunity is necessary to protect public officials
	from excessive interference with their responsibilities and from "potentially disabling threats of liability."</p>
	HTML;

$div_wikipedia_Judicial_immunity = new WikipediaContentSection();
$div_wikipedia_Judicial_immunity->setTitleText("Judicial immunity");
$div_wikipedia_Judicial_immunity->setTitleLink("https://en.wikipedia.org/wiki/Judicial_immunity");
$div_wikipedia_Judicial_immunity->content = <<<HTML
	<p>Judicial immunity is a form of sovereign immunity, which protects judges and others employed by the judiciary
	from liability resulting from their judicial actions.
	It is intended to ensure that judges can make decisions free from improper influence exercised on them,
	contributing to the impartiality of the judiciary and the rule of law.
	In modern times, the main purpose of "judicial immunity [is to shield] judges from the suits of ordinary people",
	primarily litigants who may be dissatisfied with the outcome of a case decided by the judge.</p>
	HTML;

$div_wikipedia_Parliamentary_immunity = new WikipediaContentSection();
$div_wikipedia_Parliamentary_immunity->setTitleText("Parliamentary immunity");
$div_wikipedia_Parliamentary_immunity->setTitleLink("https://en.wikipedia.org/wiki/Parliamentary_immunity");
$div_wikipedia_Parliamentary_immunity->content = <<<HTML
	<p>Parliamentary immunity, also known as legislative immunity, is a system in which political leadership position holders
	such as president, vice president, minister, governor, lieutenant governor, speaker, deputy speaker, member of parliament,
	member of legislative assembly, member of legislative council, senator, member of congress, corporator, councilor etc.
	are granted full immunity from legal prosecution, both civil prosecution and criminal prosecution,
	in the course of the execution of their official duties.</p>
	HTML;

$div_wikipedia_Qualified_immunity = new WikipediaContentSection();
$div_wikipedia_Qualified_immunity->setTitleText("Qualified immunity");
$div_wikipedia_Qualified_immunity->setTitleLink("https://en.wikipedia.org/wiki/Qualified_immunity");
$div_wikipedia_Qualified_immunity->content = <<<HTML
	<p>In the United States, qualified immunity is a legal principle of federal constitutional law
	that grants government officials performing discretionary (optional) functions immunity from lawsuits for damages
	unless the plaintiff shows that the official violated
	"clearly established statutory or constitutional rights of which a reasonable person would have known".
	It is comparable to sovereign immunity, though it protects government employees rather than the government itself.
	It is less strict than absolute immunity, which protects officials who "make reasonable but mistaken judgments about open legal questions",
	extending to "all [officials] but the plainly incompetent or those who knowingly violate the law".
	Qualified immunity applies only to government officials in civil litigation,
	and does not protect the government itself from suits arising from officials' actions.</p>
	HTML;


$page->parent('institutions.html');
$page->template("stub");
$page->body($div_introduction);

$page->body($div_list_Immunity);

$page->body($div_Trump_Pressures_Republicans_to_Pass_a_Law_to_Keep_Him_Out_of_Jail_Forever);

$page->body($div_wikipedia_Legal_immunity);
$page->body($div_wikipedia_Absolute_immunity);
$page->body($div_wikipedia_Judicial_immunity);
$page->body($div_wikipedia_Parliamentary_immunity);
$page->body($div_wikipedia_Qualified_immunity);
