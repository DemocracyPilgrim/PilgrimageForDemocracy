<?php
$page = new Page();
$page->h1("Firing Line");
$page->tags("Organisation", "Information: Media", "Political Discourse");
$page->keywords("Firing Line");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

$r1 = $page->ref("https://www.pbs.org/wnet/firing-line/about/", "About Firing Line");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>"Firing Line" is a TV program distributed by $PBS, currently hosted by ${'Margaret Hoover'}.</p>
	HTML;


$list_Firing_Line = ListOfPeoplePages::WithTags("Firing Line");
$print_list_Firing_Line = $list_Firing_Line->print();

$div_list_Firing_Line = new ContentSection();
$div_list_Firing_Line->content = <<<HTML
	<h3>Related content</h3>

	$print_list_Firing_Line
	HTML;



$div_PBS_Firing_Line = new WebsiteContentSection();
$div_PBS_Firing_Line->setTitleText("PBS: Firing Line ");
$div_PBS_Firing_Line->setTitleLink("https://www.pbs.org/wnet/firing-line/");
$div_PBS_Firing_Line->content = <<<HTML
	<p>Firing Line with Margaret Hoover is a refreshing reprisal of William F. Buckley’s iconic PBS program, a smart, civil and engaging contest of ideas.
	The series maintains the character of the original, providing a platform that is diligent in its commitment to civility and the rigorous exchange of opinion.
	Firing Line with Margaret Hoover comes at a time when meaningful discourse is needed more than ever.
	Interviews and debates will highlight leading lights from the left and right, complemented by archival footage from the original Firing Line
	to remind viewers of longstanding conservative and liberal arguments, where they’ve been disproved or reinforced over time.
	It is an opportunity to engage in the debate about the America
	that we want to create for the 21st century — and summon Americans of every political persuasion to a rigorous examination
	of the choices we must make together in the challenging years ahead.</p>
	HTML;



$div_PBS_Firing_Line_episodes = new WebsiteContentSection();
$div_PBS_Firing_Line_episodes->setTitleText("PBS: Firing Line episodes ");
$div_PBS_Firing_Line_episodes->setTitleLink("https://www.pbs.org/show/firing-line/");
$div_PBS_Firing_Line_episodes->content = <<<HTML
	<p>A public affairs show that delivers a civil and engaging contest of ideas.</p>
	HTML;




$div_wikipedia_Firing_Line_TV_program = new WikipediaContentSection();
$div_wikipedia_Firing_Line_TV_program->setTitleText("Firing Line TV program");
$div_wikipedia_Firing_Line_TV_program->setTitleLink("https://en.wikipedia.org/wiki/Firing_Line_(TV_program)");
$div_wikipedia_Firing_Line_TV_program->content = <<<HTML
	<p>Firing Line is an American public affairs television show.
	It first ran from 1966 to 1999, with conservative author and columnist William F. Buckley Jr. as host.
	It was relaunched in 2018 with Margaret Hoover as host.</p>
	HTML;


$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_list_Firing_Line);

$page->body($div_PBS_Firing_Line);
$page->body($div_PBS_Firing_Line_episodes);

$page->body($div_wikipedia_Firing_Line_TV_program);
