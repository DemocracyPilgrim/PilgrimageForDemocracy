<?php
$page = new Page();
$page->h1("Concession");
$page->tags("Elections");
$page->keywords("Concession", "concession", "concede", "conceded");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Concession_politics = new WikipediaContentSection();
$div_wikipedia_Concession_politics->setTitleText("Concession politics");
$div_wikipedia_Concession_politics->setTitleLink("https://en.wikipedia.org/wiki/Concession_(politics)");
$div_wikipedia_Concession_politics->content = <<<HTML
	<p>In politics, a concession is the act of a losing candidate publicly yielding to a winning candidate after an election after the overall result of the vote has become clear. A concession speech is usually made after an election.</p>
	HTML;

$div_wikipedia_Concession = new WikipediaContentSection();
$div_wikipedia_Concession->setTitleText("Concession");
$div_wikipedia_Concession->setTitleLink("https://en.wikipedia.org/wiki/Concession");
$div_wikipedia_Concession->content = <<<HTML
	<p><em>Disambiguation page.</em></p>
	HTML;


$page->parent('elections.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Concession");
$page->body($div_wikipedia_Concession_politics);
$page->body($div_wikipedia_Concession);
