<?php
$page = new Page();
$page->h1("Euromaidan (2023 ~ 2024)");
$page->tags("International", "Ukraine");
$page->keywords("Euromaidan");
$page->stars(2);
$page->viewport_background("/free/euromaidan.png");

$page->snp("description", "Protests in Ukraine (November 2013 to February 2014)");
$page->snp("image",       "/free/euromaidan.1200-630.png");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Euromaidan was a series of protests and demonstrations that took place in $Ukraine from November 2013 to February 2014.
	It was a pivotal event in Ukrainian history, marking a turning point in the country's relationship with $Russia and its aspirations towards $European integration.</p>

	<p>In November 2013, the $Ukrainian government, under President Viktor Yanukovych,
	abruptly suspended preparations for signing an association agreement with the ${'European Union'}.
	This decision was widely perceived as a result of pressure from Russia, which opposed Ukraine's closer ties to the West.</p>

	<p>Protests erupted in Maidan Nezalezhnosti (Independence Square) in Kyiv, demanding closer ties with Europe and condemning the government's decision.
	The protests, initially peaceful, grew in size and intensity as the government responded with violence and repression.</p>
	HTML;


$div_Events = new ContentSection();
$div_Events->content = <<<HTML
	<h3>Key Events</h3>

	<p><strong>Student Protests</strong>:
	Initially, the protests were largely student-led, expressing frustration with the government's corruption and its turn towards Russia.</p>

	<p><strong>Government Crackdown</strong>:
	The government used force to disperse protesters, leading to injuries and deaths. This sparked further outrage and condemnation.</p>

	<p><strong>International Condemnation</strong>:
	The EU and the US condemned the government's actions and imposed sanctions on Ukrainian officials.</p>

	<p><strong>Yanukovych's Fleeing</strong>:
	In February 2014, facing mounting pressure and the threat of impeachment, Yanukovych fled Ukraine. This marked the end of the Euromaidan protests.</p>
	HTML;


$div_Aftermath = new ContentSection();
$div_Aftermath->content = <<<HTML
	<h3>Aftermath</h3>

	<p>Euromaidan led to a change in government and paved the way for pro-European leaders.
	The protests, however, triggered a backlash from Russia, which annexed Crimea and supported separatists in eastern Ukraine, leading to a protracted conflict.</p>
	HTML;

$div_Significance = new ContentSection();
$div_Significance->content = <<<HTML
	<h3>Significance</h3>

	<p>Euromaidan represented a popular uprising for democracy, European integration, and national identity.</p>

	<p>It demonstrated the Ukrainian people's desire to break free from Russian influence and embrace a different future.</p>

	<p>It marked a turning point in the relationship between Ukraine and Russia, leading to the current conflict.</p>

	<p>Euromaidan serves as a powerful reminder of the struggle for democracy, the importance of citizen engagement,
	and the challenges faced by nations striving for freedom and self-determination.</p>
	HTML;


$div_wikipedia_Euromaidan = new WikipediaContentSection();
$div_wikipedia_Euromaidan->setTitleText("Euromaidan");
$div_wikipedia_Euromaidan->setTitleLink("https://en.wikipedia.org/wiki/Euromaidan");
$div_wikipedia_Euromaidan->content = <<<HTML
	<p>Euromaidan, or the Maidan Uprising, was a wave of demonstrations and civil unrest in Ukraine, which began on 21 November 2013 with large protests in Maidan Nezalezhnosti (Independence Square) in Kyiv.</p>
	HTML;


$page->parent('ukraine.html');

$page->body($div_introduction);
$page->body($div_Events);
$page->body($div_Aftermath);
$page->body($div_Significance);



$page->related_tag("Euromaidan");
$page->body($div_wikipedia_Euromaidan);
