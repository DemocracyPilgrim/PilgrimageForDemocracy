<?php
$page = new Page();
$page->h1("Maurice Duverger");
$page->alpha_sort("Duverger, Maurice");
$page->tags("Person");
$page->keywords("Maurice Duverger");
$page->stars(0);

//$page->snp("description", "");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref('', '');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Maurice Duverger (1917 - 2014) was a $French political scientist.</p>

	<p>Duverger most famously described
	the effects of plurality voting in single winner elections:
	it is now a political law that bears his name: ${"Duverger's Law"}.</p>

	<p>The $Pilgrimage is describing the effects of that law
	in the ${"Duverger Syndrome"}.</p>

	<p>Duverger's Law has been discussed since the 1950s,
	but the lessons have yet to be learned in the 2020s.
	Fixing our electoral system by adopting a better ${'voting method'}
	is probably the most critical issue for our $democracies.</p>
	HTML;

$div_wikipedia_Maurice_Duverger = new WikipediaContentSection();
$div_wikipedia_Maurice_Duverger->setTitleText("Maurice Duverger");
$div_wikipedia_Maurice_Duverger->setTitleLink("https://en.wikipedia.org/wiki/Maurice_Duverger");
$div_wikipedia_Maurice_Duverger->content = <<<HTML
	<p>Maurice Duverger was a French jurist, sociologist, political scientist and politician born in Angoulême, Charente.
	Starting his career as a jurist at the University of Bordeaux, Duverger became more and more involved in political science
	and in 1948 founded one of the first faculties for political science in Bordeaux, France.
	An emeritus professor of the Sorbonne and member of the FNSP,
	he has published many books and articles in newspapers,
	such as Corriere della Sera, la Repubblica, El País, and especially Le Monde.</p>
	HTML;


$page->parent('list_of_people.html');
$page->template("stub");
$page->body($div_introduction);

$page->body('duverger_law.html');
$page->body('duverger_syndrome.html');

$page->body($div_wikipedia_Maurice_Duverger);
