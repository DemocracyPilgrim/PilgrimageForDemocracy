<?php
$page = new Page();
$page->h1('Volodymyr Zelenskyy');
$page->alpha_sort("Zelenskyy, Volodymyr");
$page->tags("Person");
$page->keywords('Volodymyr Zelensky', 'Zelensky', 'Volodymyr Zelenskyy', 'Zelenskyy');
$page->stars(0);


$page->snp('description', 'President of Ukraine.');
//$page->snp('image',       '/copyrighted/');


$page->preview( <<<HTML
	<p></p>
	HTML );


$r1 = $page->ref("https://www.youtube.com/watch?v=r28nYM0eMks", "Zelenskyy shares emotional and powerful moment with veterans on 80th Anniversary of D-Day");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Volodymyr Zelenskyy is the president of $Ukraine and a hero of Ukrainian freedom and $democracy.</p>

	<p>In a televised interview on Aug. 27 2023, President Zelensky said he has submitted a proposal to parliament
	to equate $corruption with treason while martial law is in effect in Ukraine.</p>
	HTML;


$div_You_saved_Europe = new ContentSection();
$div_You_saved_Europe->content = <<<HTML
	<h3>You saved Europe</h3>

	<p>During the D-Day 80th anniversary ceremonies, commemorating the sacrifice and the acts of courage
	that the soldiers of Allied nations who landed in Normandy, France, on June 6th 1944,
	president Volodymyr Zelenskyy met with WWII veteran Melvin Hurwitz (493rd Bomb Group, 863rd Bomb Squad, 8th Air Force).
	In a spontaneous, very emotional and highly symbolic moment, they embraced each other. $r1
	See video below.</p>
	HTML;


$div_youtube_You_are_my_hero_WWII_veteran_shares_emotional_embrace_with_Ukraine_s_Zelenskyy = new YoutubeContentSection();
$div_youtube_You_are_my_hero_WWII_veteran_shares_emotional_embrace_with_Ukraine_s_Zelenskyy->setTitleText("&quot;You&#39;re my hero!&quot;: WWII veteran shares emotional embrace with Ukraine&#39;s Zelenskyy");
$div_youtube_You_are_my_hero_WWII_veteran_shares_emotional_embrace_with_Ukraine_s_Zelenskyy->setTitleLink("https://www.youtube.com/watch?v=KBofHkLqXZE");
$div_youtube_You_are_my_hero_WWII_veteran_shares_emotional_embrace_with_Ukraine_s_Zelenskyy->content = <<<HTML
	<p>
		<strong>WWII veteran</strong>: You are the savior of the people.<br>
		<strong>President Zelenskyy</strong>: No, no. You saved Europe.<br>
		<strong>WWII veteran</strong>: My hero.<br>
		<strong>President Zelenskyy</strong>: No, you are our hero.<br>
		<strong>WWII veteran</strong>: I pray for you.<br>
		<strong>President Zelenskyy</strong>: Thank you so much!<br>
	</p>
	HTML;



$div_wikipedia_Volodymyr_Zelenskyy = new WikipediaContentSection();
$div_wikipedia_Volodymyr_Zelenskyy->setTitleText('Volodymyr Zelenskyy');
$div_wikipedia_Volodymyr_Zelenskyy->setTitleLink('https://en.wikipedia.org/wiki/Volodymyr_Zelenskyy');
$div_wikipedia_Volodymyr_Zelenskyy->content = <<<HTML
	<p>Volodymyr Zelenskyy (born 25 January 1978) is a Ukrainian politician and former comedian and actor
	who is the sixth and current president of Ukraine since 2019.</p>
	HTML;


$page->parent('list_of_people.html');
$page->parent('ukraine.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_You_saved_Europe);
$page->body($div_youtube_You_are_my_hero_WWII_veteran_shares_emotional_embrace_with_Ukraine_s_Zelenskyy);

$page->body($div_wikipedia_Volodymyr_Zelenskyy);
