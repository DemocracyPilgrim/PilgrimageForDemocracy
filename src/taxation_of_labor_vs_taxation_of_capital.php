<?php
$page = new Page();
$page->h1("Taxation of labor vs taxation of capital ");
$page->tags("Taxes");
$page->stars(0);

//$page->snp("description", "");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref('', '');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;



$div_Globalization_and_Factor_Income_Taxation = new WebsiteContentSection();
$div_Globalization_and_Factor_Income_Taxation->setTitleText("Globalization and Factor Income Taxation ");
$div_Globalization_and_Factor_Income_Taxation->setTitleLink("https://globaltaxation.world/");
$div_Globalization_and_Factor_Income_Taxation->content = <<<HTML
	<p>How has globalization affected the taxation of $labor and capital incomes?
	To address this question, we build and analyze a new, global database on
	effective macroeconomic tax rates in more than 150 countries since 1965,
	combining national accounts data with government revenue statistics,
	including from a wide variety of archival records.</p>
	HTML;



$page->parent('taxes.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_Globalization_and_Factor_Income_Taxation);
