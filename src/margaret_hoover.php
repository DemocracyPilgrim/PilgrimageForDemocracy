<?php
$page = new Page();
$page->h1("Margaret Hoover");
$page->alpha_sort("Hoover, Margaret");
$page->tags("Person", "Information: Media", "Reporter");
$page->keywords("Margaret Hoover");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Margaret Hoover is an $American media personality, and the host of the $PBS show ${"Firing Line"}.</p>
	HTML;

$div_wikipedia_Margaret_Hoover = new WikipediaContentSection();
$div_wikipedia_Margaret_Hoover->setTitleText("Margaret Hoover");
$div_wikipedia_Margaret_Hoover->setTitleLink("https://en.wikipedia.org/wiki/Margaret_Hoover");
$div_wikipedia_Margaret_Hoover->content = <<<HTML
	<p>Margaret Claire Hoover (born December 11, 1977) is an American conservative political commentator, political strategist, media personality,
	author, and great-granddaughter of Herbert Hoover, the 31st U.S. president.
	She is author of the book American Individualism: How A New Generation of Conservatives Can Save the Republican Party,
	published by Crown Forum in 2011. Hoover hosts PBS's reboot of the conservative interview show Firing Line.</p>
	HTML;


$page->parent('list_of_people.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_wikipedia_Margaret_Hoover);
