<?php
$page = new Page();
$page->h1("Health and Human Rights Journal");
$page->tags("Organisation", "Humanity", "Individual: Rights", "Healthcare");
$page->keywords("Health and Human Rights Journal", "HHR");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

$r1 = $page->ref("https://www.hhrjournal.org/about-hhr/", "About HHR");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;



$div_Health_and_Human_Rights_Journal = new WebsiteContentSection();
$div_Health_and_Human_Rights_Journal->setTitleText("Health and Human Rights Journal ");
$div_Health_and_Human_Rights_Journal->setTitleLink("https://www.hhrjournal.org/");
$div_Health_and_Human_Rights_Journal->content = <<<HTML
	<p>Health and Human Rights provides an inclusive forum for action-oriented dialogue among human rights practitioners and scholars.
	The journal endeavors to increase access to human rights knowledge in the health field by linking an expanded community of readers and contributors.
	Following the lead of a growing number of open access publications,
	the full text of Health and Human Rights is freely available to anyone with internet access.</p>

	<p>Health and Human Rights focuses rigorous scholarly analysis on the conceptual foundations and challenges of rights discourse and action in relation to health.
	The journal is dedicated to empowering new voices from the field — highlighting the innovative work of groups and individuals
	in direct engagement with human rights struggles as they relate to health.
	We seek to foster engaged scholarship and reflective activism.
	In doing so, we invite informed action to realize the full spectrum of human rights.$r1</p>
	HTML;



$div_wikipedia_Health_and_Human_Rights = new WikipediaContentSection();
$div_wikipedia_Health_and_Human_Rights->setTitleText("Health and Human Rights");
$div_wikipedia_Health_and_Human_Rights->setTitleLink("https://en.wikipedia.org/wiki/Health_and_Human_Rights");
$div_wikipedia_Health_and_Human_Rights->content = <<<HTML
	<p>Health and Human Rights is a biannual peer-reviewed public health journal that was established in 1994.
	It covers research on the conceptual foundations of human rights and social justice in relation to health.
	The founding editor-in-chief was Jonathan Mann, who was succeeded by Sofia Gruskin in 1997.
	Since 2007, the journal is edited by Paul Farmer.
	Harvard University Press became the publisher of the journal in 2013,
	succeeding the François-Xavier Bagnoud Center for Health and Human Rights at Harvard School of Public Health.</p>
	HTML;


$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);
$page->body($div_Health_and_Human_Rights_Journal);


$page->body($div_wikipedia_Health_and_Human_Rights);
