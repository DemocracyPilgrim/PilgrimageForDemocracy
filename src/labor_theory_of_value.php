<?php
$page = new Page();
$page->h1("Labor theory of value");
$page->viewport_background("");
$page->keywords("Labor Theory of Value", "labor theory of value", "Labour Theory of Value", "labour theory of value");
$page->stars(0);
$page->tags("Living: Economy", "Economy", "Labor");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Labor_theory_of_value = new WikipediaContentSection();
$div_wikipedia_Labor_theory_of_value->setTitleText("Labor theory of value");
$div_wikipedia_Labor_theory_of_value->setTitleLink("https://en.wikipedia.org/wiki/Labor_theory_of_value");
$div_wikipedia_Labor_theory_of_value->content = <<<HTML
	<p>The labor theory of value (LTV) is a theory of value that argues that the exchange value of a good or service is determined by the total amount of "socially necessary labor" required to produce it. The contrasting system is typically known as the subjective theory of value.</p>
	HTML;


$page->parent('economy.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Labor theory of value");
$page->body($div_wikipedia_Labor_theory_of_value);
