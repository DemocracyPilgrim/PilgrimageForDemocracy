<?php
$page = new Page();
$page->h1("David Becker");
$page->alpha_sort("Becker, David");
$page->tags("Person", "Lawyer", "Elections", "USA");
$page->keywords("David Becker");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>David Becker is a $lawyer and the founder of the ${"Center for Election Innovation & Research"}.</p>

	<p>He is the co-author of ${"The Big Truth: Upholding Democracy in the Age of “The Big Lie”"}.</p>
	HTML;


$div_Media_appearances = new ContentSection();
$div_Media_appearances->content = <<<HTML
	<h3>Media appearances</h3>

	<p>David Becker appears and is interviewed in the documentary: ${"Counting the Vote: A Firing Line Special with Margaret Hoover"}.</p>
	HTML;



$div_David_Becker_Executive_Director_Founder_of_CEIR = new WebsiteContentSection();
$div_David_Becker_Executive_Director_Founder_of_CEIR->setTitleText("David Becker Executive Director & Founder of CEIR");
$div_David_Becker_Executive_Director_Founder_of_CEIR->setTitleLink("https://electioninnovation.org/team/david-becker/");
$div_David_Becker_Executive_Director_Founder_of_CEIR->content = <<<HTML
	<p>David Becker is the Executive Director and Founder of the nonpartisan, non-profit Center for Election Innovation & Research,
	working with election officials of both parties, all around the country, to ensure elections voters should trust, and do trust.
	A key element of David’s work with CEIR is managing the Election Official Legal Defense Network,
	providing pro bono legal assistance to election officials who are threatened with frivolous criminal prosecution, harassment, or physical violence.</p>
	HTML;






$div_Jocelyn_Benson_Benjamin_Ginsberg_and_David_Becker = new WebsiteContentSection();
$div_Jocelyn_Benson_Benjamin_Ginsberg_and_David_Becker->setTitleText("Jocelyn Benson, Benjamin Ginsberg and David Becker");
$div_Jocelyn_Benson_Benjamin_Ginsberg_and_David_Becker->setTitleLink("https://www.pbs.org/wnet/firing-line/video/jocelyn-benson-benjamin-ginsberg-and-david-becker-wsqtmv/");
$div_Jocelyn_Benson_Benjamin_Ginsberg_and_David_Becker->content = <<<HTML
	<p>Michigan Secretary of State Jocelyn Benson, GOP lawyer Benjamin Ginsberg and election expert David Becker
	discuss the security and integrity of American elections ahead of November’s contest between former President Trump and Vice President Harris.</p>
	HTML;




$page->parent('list_of_people.html');
$page->template("stub");
$page->body($div_introduction);
$page->body($div_Media_appearances);
$page->related_tag("David Becker");

$page->body($div_David_Becker_Executive_Director_Founder_of_CEIR);
$page->body($div_Jocelyn_Benson_Benjamin_Ginsberg_and_David_Becker);
