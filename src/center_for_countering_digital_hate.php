<?php
$page = new OrganisationPage();
$page->h1("Center for Countering Digital Hate");
$page->keywords("Center for Countering Digital Hate");
$page->viewport_background("");
$page->stars(0);
$page->tags("Organisation", "The Web Defenders", "Hate Speech", "Freedom of Speech", "Social Networks");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;


$div_CCDH_Center_for_Countering_Digital_Hate = new WebsiteContentSection();
$div_CCDH_Center_for_Countering_Digital_Hate->setTitleText("CCDH — Center for Countering Digital Hate");
$div_CCDH_Center_for_Countering_Digital_Hate->setTitleLink("https://counterhate.com/");
$div_CCDH_Center_for_Countering_Digital_Hate->content = <<<HTML
	<p>Our mission is to protect human rights and civil liberties online.
	Social media companies erode basic human rights and civil liberties by enabling the spread of online hate and disinformation.
	Social Media companies deny the problem, deflect the blame, and delay taking responsibility.
	The Center for Countering Digital Hate holds them accountable and responsible for their business choices by highlighting their failures, educating the public, and advocating change from platforms and governments to protect our communities.</p>
	HTML;



$div_wikipedia_Center_for_Countering_Digital_Hate = new WikipediaContentSection();
$div_wikipedia_Center_for_Countering_Digital_Hate->setTitleText("Center for Countering Digital Hate");
$div_wikipedia_Center_for_Countering_Digital_Hate->setTitleLink("https://en.wikipedia.org/wiki/Center_for_Countering_Digital_Hate");
$div_wikipedia_Center_for_Countering_Digital_Hate->content = <<<HTML
	<p>The Center for Countering Digital Hate (CCDH), formerly Brixton Endeavors, is a British-American not-for-profit NGO company with offices in London and Washington, D.C. with the stated purpose of stopping the spread of online hate speech and disinformation. It campaigns to deplatform people that it believes promote hate or misinformation, and campaigns to restrict media organisations such as The Daily Wire from advertising. CCDH is a member of the Stop Hate For Profit coalition.</p>
	HTML;


$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Center for Countering Digital Hate");

$page->body($div_CCDH_Center_for_Countering_Digital_Hate);
$page->body($div_wikipedia_Center_for_Countering_Digital_Hate);
