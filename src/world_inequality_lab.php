<?php
$page = new Page();
$page->h1("World Inequality Lab");
$page->keywords("World Inequality Lab");
$page->tags("Organisation", "Taxes");
$page->stars(0);

//$page->snp("description", "");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref('', '');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>The World Inequality Lab publishes the ${'World Inequality Database'}.</p>

	<p>See also: $taxes, $poverty.</p>
	HTML;



$div_World_Inequality_Lab_website = new WebsiteContentSection();
$div_World_Inequality_Lab_website->setTitleText("World Inequality Lab website ");
$div_World_Inequality_Lab_website->setTitleLink("https://inequalitylab.world/en/");
$div_World_Inequality_Lab_website->content = <<<HTML
	<p>Empowering civil society, reinforcing democracy. Powered with data.
	The World Inequality Lab gathers social scientists
	committed to helping everyone understand the drivers of inequality worldwide through evidence-based research.</p>
	HTML;



$div_wikipedia_World_Inequality_Database = new WikipediaContentSection();
$div_wikipedia_World_Inequality_Database->setTitleText("World Inequality Database");
$div_wikipedia_World_Inequality_Database->setTitleLink("https://en.wikipedia.org/wiki/World_Inequality_Database");
$div_wikipedia_World_Inequality_Database->content = <<<HTML
	<p>The World Inequality Lab aims to promote research on global inequality dynamics.
	Its missions are the intension of the World Inequality Database,
	the production of analysis on global inequality dynamics, and the dissemination in the public debate.</p>

	<p>The Lab works in close coordination with a large international network of researchers
	(over one hundred researchers covering nearly seventy countries) contributing to the database,
	in a collaborative effort to extend the existing database,
	which provides data on both distribution of income and wealth,
	"as well as the distribution of different forms of capital assets, in the analyzed countries".</p>
	HTML;


$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);

$page->body($div_World_Inequality_Lab_website);

$page->body($div_wikipedia_World_Inequality_Database);
