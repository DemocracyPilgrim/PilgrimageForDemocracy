<?php
$page = new PersonPage();
$page->h1("Barack Obama");
$page->alpha_sort("Obama, Barack");
$page->tags("Person", "USA", "POTUS", "Elections");
$page->keywords("Barack Obama");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Barack_Obama = new WikipediaContentSection();
$div_wikipedia_Barack_Obama->setTitleText("Barack Obama");
$div_wikipedia_Barack_Obama->setTitleLink("https://en.wikipedia.org/wiki/Barack_Obama");
$div_wikipedia_Barack_Obama->content = <<<HTML
	<p>Barack Hussein Obama II is an American politician who served as the 44th president of the United States from 2009 to 2017.
	As a member of the Democratic Party, he was the first African-American president in U.S. history.
	Obama previously served as a U.S. senator representing Illinois from 2005 to 2008 and as an Illinois state senator from 1997 to 2004.</p>
	HTML;


$page->parent('list_of_people.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Barack Obama");
$page->body($div_wikipedia_Barack_Obama);
