<?php
$page = new Page();
$page->h1("Eminent domain");
$page->tags("Society");
$page->keywords("eminent domain", "Eminent Domain");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$list_Eminent_Domain = ListOfPeoplePages::WithTags("Eminent Domain");
$print_list_Eminent_Domain = $list_Eminent_Domain->print();

$div_list_Eminent_Domain = new ContentSection();
$div_list_Eminent_Domain->content = <<<HTML
	<h3>Related content</h3>

	$print_list_Eminent_Domain
	HTML;


$div_wikipedia_Eminent_domain = new WikipediaContentSection();
$div_wikipedia_Eminent_domain->setTitleText("Eminent domain");
$div_wikipedia_Eminent_domain->setTitleLink("https://en.wikipedia.org/wiki/Eminent_domain");
$div_wikipedia_Eminent_domain->content = <<<HTML
	<p>Eminent domain (also known as land acquisition, compulsory purchase, resumption, resumption/compulsory acquisition, or expropriation)
	is the power to take private property for public use.
	It does not include the power to take and transfer ownership of private property from one property owner to another private property owner
	without a valid public purpose.
	This power can be legislatively delegated by the state to municipalities, government subdivisions, or even to private persons or corporations,
	when they are authorized to exercise the functions of public character.</p>
	HTML;


$page->parent('society.html');
$page->template("stub");
$page->body($div_introduction);
$page->body($div_list_Eminent_Domain);


$page->body($div_wikipedia_Eminent_domain);
