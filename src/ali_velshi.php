<?php
$page = new PersonPage();
$page->h1("Ali Velshi");
$page->alpha_sort("Velshi, Ali");
$page->tags("Person");
$page->keywords("Ali Velshi");
$page->stars(0);
$page->viewport_background("/free/ali_velshi.png");

$page->snp("description", "Canadian television journalist.");
$page->snp("image",       "/free/ali_velshi.1200-630.png");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Ali_Velshi = new WikipediaContentSection();
$div_wikipedia_Ali_Velshi->setTitleText("Ali Velshi");
$div_wikipedia_Ali_Velshi->setTitleLink("https://en.wikipedia.org/wiki/Ali_Velshi");
$div_wikipedia_Ali_Velshi->content = <<<HTML
	<p>Ali Velshi is a Canadian television journalist, a senior economic and business correspondent for NBC News, and an anchor for MSNBC. He is also a substitute anchor for The Last Word with Lawrence O'Donnell on MSNBC on Friday night. Velshi is based in New York City. Known for his work on CNN, he was CNN's Chief Business Correspondent, anchor of CNN's Your Money and a co-host of CNN International's weekday business show World Business Today. In 2013, he joined Al Jazeera America, a channel that launched in August of that year. He hosted Ali Velshi on Target until Al Jazeera America ceased operations on April 12, 2016. He has worked for MSNBC since October 2016.</p>
	HTML;


$page->parent('list_of_people.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Ali Velshi");
$page->body($div_wikipedia_Ali_Velshi);
