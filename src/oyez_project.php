<?php
$page = new Page();
$page->h1("Oyez Project");
$page->tags("Organisation", "USA", "Institutions: Judiciary", "SCOTUS");
$page->keywords("Oyez Project");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

$r1 = $page->ref("https://www.oyez.org/about", "About Oyez");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;



$div_Oyez = new WebsiteContentSection();
$div_Oyez->setTitleText("Oyez ");
$div_Oyez->setTitleLink("https://www.oyez.org/");
$div_Oyez->content = <<<HTML
	<p>
	Oyez (pronounced OH-yay)—a free law project from Cornell’s Legal Information Institute (LII), Justia, and Chicago-Kent College of Law—is a multimedia archive devoted to making the Supreme Court of the United States accessible to everyone. It is the most complete and authoritative source for all of the Court’s audio since the installation of a recording system in October 1955. Oyez offers transcript-synchronized and searchable audio, plain-English case summaries, illustrated decision information, and full-text Supreme Court opinions (through Justia). Oyez also provides detailed information on every justice throughout the Court’s history and offers a panoramic tour of the Supreme Court building, including the chambers of several justices.
	</p>
	HTML;



$div_wikipedia_Oyez_Project = new WikipediaContentSection();
$div_wikipedia_Oyez_Project->setTitleText("Oyez Project");
$div_wikipedia_Oyez_Project->setTitleLink("https://en.wikipedia.org/wiki/Oyez_Project");
$div_wikipedia_Oyez_Project->content = <<<HTML
	<p>The Oyez Project is an unofficial online multimedia archive website for the Supreme Court of the United States.
	It was initiated by the Illinois Institute of Technology's Chicago-Kent College of Law
	and now also sponsored by Cornell Law School Legal Information Institute and Justia.</p>

	<p>The website has emphasis on the court's audio of oral arguments.
	The website "aims to be a complete and authoritative source for all audio recorded in the Court since [...] October 1955."
	The website also includes biographical information of both incumbent and historical justices of the Court and advocates who have argued before the court.
	The project's name refers to the interjection, "Oyez", that is spoken by the Supreme Court Marshal at the beginning of each argument session.
	The website was founded by Jerry Goldman, a research professor of law at the Chicago-Kent College of Law at Illinois Institute of Technology.</p>
	HTML;


$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);
$page->body($div_Oyez);


$page->body($div_wikipedia_Oyez_Project);
