<?php
$page = new PersonPage();
$page->h1("Malala Yousafzai");
$page->alpha_sort("Yousafzai, Malala");
$page->tags("Person", "Pakistan", "Nobel Peace Prize", "Women's Rights", "Society");
$page->keywords("Malala Yousafzai");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Malala_Yousafzai = new WikipediaContentSection();
$div_wikipedia_Malala_Yousafzai->setTitleText("Malala Yousafzai");
$div_wikipedia_Malala_Yousafzai->setTitleLink("https://en.wikipedia.org/wiki/Malala_Yousafzai");
$div_wikipedia_Malala_Yousafzai->content = <<<HTML
	<p>Malala Yousafzai is a Pakistani female education activist, film and television producer, and the 2014 Nobel Peace Prize laureate at the age of 17. She is the youngest Nobel Prize laureate in history, the second Pakistani and the only Pashtun to receive a Nobel Prize. Yousafzai is a human rights advocate for the education of women and children in her native homeland, Swat, where the Pakistani Taliban had at times banned girls from attending school. Her advocacy has grown into an international movement, and according to former Prime Minister Shahid Khaqan Abbasi, she has become Pakistan's "most prominent citizen."</p>
	HTML;


$page->parent('list_of_people.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Malala Yousafzai");
$page->body($div_wikipedia_Malala_Yousafzai);
