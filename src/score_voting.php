<?php
$page = new Page();
$page->h1("Score voting: Rate, Don't Just Pick");
$page->viewport_background("/free/score_voting.png");
$page->keywords("Score Voting", "score voting", "score", "scoring");
$page->stars(3);
$page->tags("Elections", "Voting Methods", "Rated Voting");

$page->snp("description", "Fully express your opinion on each candidate.");
$page->snp("image",       "/free/score_voting.1200-630.png");

$page->preview( <<<HTML
	<p>Tired of feeling limited by "choose-one" voting?
	Score Voting empowers you to rate candidates on a scale,
	expressing the strength of your support and helping elect leaders who truly represent your values.</p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<h3>Introduction: Beyond the Checkmark – A Need for Deeper Expression</h3>

	<p>Do you ever feel like choosing a candidate is like picking the "least bad" option?
	Are you tired of elections where you have to compromise your values and vote strategically?
	At $Pilgrimage, we believe in empowering every voice.
	Score Voting offers a simple yet powerful solution, allowing you to rate candidates based on your honest assessment,
	rather than just picking one.
	It’s a way to break free from the constraints of single-choice voting and build a more representative and effective democracy.</p>
	HTML;


$div_What_is_Score_Voting_Scoring_Simplified = new ContentSection();
$div_What_is_Score_Voting_Scoring_Simplified->content = <<<HTML
	<h3>What is Score Voting? Scoring, Simplified</h3>

	<p>Score Voting is a voting system where voters assign a numerical score to each candidate on the ballot.
	The candidate with the highest average score wins.
	Think of it like grading a candidate – you give them a score that reflects their strengths and weaknesses.</p>

	<p>Here’s how it works:</p>

	<ol>
	<li><strong>Ballot:</strong>
	Voters receive a ballot listing all the candidates.</li>

	<li><strong>Scoring:</strong>
	Voters assign a score to each candidate using a defined range (e.g., 0 to 5, where 0 is the lowest and 5 is the highest).</li>

	<li><strong>Tabulation:</strong>
	Election officials calculate the average score for each candidate by adding up all the scores and dividing by the number of voters.
	The candidate with the highest average score wins.</li>

	</ol>

	<p>That is all there is to it.</p>
	HTML;


$div_Approval_Voting_A_Familiar_Cousin = new ContentSection();
$div_Approval_Voting_A_Familiar_Cousin->content = <<<HTML
	<h3>Approval Voting: A Familiar Cousin</h3>

	<p>If you're familiar with Approval Voting, you already understand the basic principle of Score Voting.
	In fact, you can think of Approval Voting as a simplified version of Score Voting,
	where you can only give a candidate a score of either 0 (disapprove) or 1 (approve).
	Score Voting just gives you more shades of gray, allowing you to express the intensity of your support.</p>
	HTML;


$div_Scoring_Systems_All_Relative = new ContentSection();
$div_Scoring_Systems_All_Relative->content = <<<HTML
	<h3>Scoring Systems: It's All Relative (and Cultural)</h3>

	<p>Scoring is a concept we all understand, even if we're used to different scales.
	In school, some countries use a 0-10 scale, others a 0-20 scale,
	and in the USA, many teachers use letter grades (A, B, C, D, F) or a 0-100 scale for homework and tests.
	We are also familiar with sports competition like gymnastics or figure skating
	where a panel of judges will assign a score to each athlete after their performance.
	Whether you’re used to letter grades, number scores, or Olympic judging, the basic idea of Score Voting is the same:
	you're evaluating each single candidates based on their merits.</p>
	HTML;


$div_Scoring_Ranges_How_Much_Nuance_Do_We_Need = new ContentSection();
$div_Scoring_Ranges_How_Much_Nuance_Do_We_Need->content = <<<HTML
	<h3>Scoring Ranges: How Much Nuance Do We Need?</h3>

	<p>The range of scores you can use can affect how you vote.</p>

	<ul>
	<li><strong>Narrow Range</strong>
	(e.g., 0-3, 0-5): A smaller range might make it easier to choose,
	helping prevent voters from clustering all the scores to the middle (which would make the whole system useless).</li>

	<li><strong>Wide Range</strong>
	(e.g., 0-10, 0-100): a larger range lets you really show how you feel about each candidate.</li>

	</ul>

	<p>The best range depends on the specific context and the goals of the election.</p>

	<h4>A Note on Negative Scores</h4>

	<p>While Score Voting typically uses a positive range (e.g., 0 to 5),
	it’s also possible to use a range that includes negative scores (e.g., -5 to +5).
	This can allow voters to express strong disapproval of certain candidates.
	We'll explore this variant of Score Voting in a later article.</p>

	HTML;



$div_Bullet_Voting_Is_It_Strategic = new ContentSection();
$div_Bullet_Voting_Is_It_Strategic->content = <<<HTML
	<h3>Bullet Voting: Is It a good Strategy?</h3>

	<p>A common concern with Score Voting is "bullet voting" – giving the maximum score to only one's favorite candidate and zero to everyone else.
	While this is certainly a possible strategy, it’s not always the most effective.
	Honest scoring, where you evaluate all candidates based on their merits, can be more impactful.
	Think of it this way: If you only give your top score to one candidate, you’re essentially saying that all the other candidates are equally terrible.
	This might not accurately reflect your views, and it might not help elect the candidate you truly prefer.</p>
	HTML;



$div_Informed_Nomination_A_New_Way_to_Shape_the_Candidate_Pool = new ContentSection();
$div_Informed_Nomination_A_New_Way_to_Shape_the_Candidate_Pool->content = <<<HTML
	<h3>Informed Nomination: A New Way to Shape the Candidate Pool</h3>

	<p>One of the most exciting aspects of Score Voting is its potential to change the way candidates are nominated and campaigns are conducted.
	In traditional primary systems, voters often feel limited to choosing between a few pre-selected candidates.
	With Score Voting, you can help ensure that a wider range of candidates are considered in the general election,
	giving voters more meaningful choices.
	By giving visibility to all candidates running for a given position, the citizens help the media give exposure to previously unknown candidates.
	Score Voting enables voters to give influence to candidates whom they like, or candidates who share their opinions.
	This enables voters to shape the candidate pool and elect more representative leaders.
	Instead of "strategic nomination," this is really "informed nomination,"
	where voters have a more direct influence on which candidates will be visible during the election.</p>
	HTML;


$div_Benefits_of_Score_Voting_A_Stronger_More_Representative_Democracy = new ContentSection();
$div_Benefits_of_Score_Voting_A_Stronger_More_Representative_Democracy->content = <<<HTML
	<h3>Benefits of Score Voting: A Stronger, More Representative Democracy</h3>

	<p><strong>Greater Expression:</strong>
	Score Voting empowers you to express the intensity of your support for different candidates, rather than just choosing one.</p>

	<p><strong>Reduced Strategic Voting:</strong>
	While strategic voting is possible, Score Voting encourages honest evaluation and can lead to more representative outcomes.</p>

	<p><strong>More Representative Elections:</strong>
	Score Voting can increase participation by giving voters a real voice and decreasing the sense that the result is a foregone conclusion.</p>

	<p><strong>Breaks the Duverger Syndrome:</strong>
	Gives power to voters instead of the current system, dictated by money and party politics. The people are more fairly represented.</p>

	<p><strong>More representative candidates:</strong>
	Candidates are incentivized to represent all members of the electorate in order to have a fairer shot at being elected.
	The resulting leaders are much more fairly representative of the population.</p>
	HTML;



$div_Making_the_Switch_A_Simple_Step_Towards_Better_Elections = new ContentSection();
$div_Making_the_Switch_A_Simple_Step_Towards_Better_Elections->content = <<<HTML
	<h3>Making the Switch: A Simple Step Towards Better Elections</h3>

	<p>Implementing Score Voting is surprisingly straightforward.
	Ballots can be easily adapted to include a scoring range for each candidate.
	Existing election equipment can be used, and voter education materials can be created to explain the system.
	The result is a voting system that more accurately reflects the will of the people.</p>
	HTML;



$div_Tie_Breaking = new ContentSection();
$div_Tie_Breaking->content = <<<HTML
	<h3>Tie Breaking</h3>

	<p>While ties are unlikely in Score Voting, they are still possible.
	In the rare event of a tie, established election tie-breaking methods will be followed,
	as defined by local laws and existing regulations.
	This might involve a coin toss, a special runoff election, or other procedures.</p>
	HTML;



$div_Conclusion_Your_Score_Matters = new ContentSection();
$div_Conclusion_Your_Score_Matters->content = <<<HTML
	<h3>Conclusion: Your Score Matters</h3>

	<p>Score Voting offers a refreshingly simple, yet profoundly effective, path to stronger democracies.
	It empowers voters, encourages honest evaluation, and can lead to more representative outcomes.
	As part of our "Pilgrimage for Democracy and Social Justice,"
	we believe that exploring and implementing innovative solutions like Score Voting
	is essential for building a more just and representative world.
	What is more, the system is easy to use and it requires a very small adjustment from the average voter.</p>

	<p>We encourage you to learn more about Score Voting,
	get involved in advocating for electoral reform, and help us build a stronger democracy for all.
	Please contribute to this project with new ideas and information.</p>
	HTML;



$div_wikipedia_Score_voting = new WikipediaContentSection();
$div_wikipedia_Score_voting->setTitleText("Score voting");
$div_wikipedia_Score_voting->setTitleLink("https://en.wikipedia.org/wiki/Score_voting");
$div_wikipedia_Score_voting->content = <<<HTML
	<p>Score voting, sometimes called range voting, is an electoral system for single-seat elections. Voters give each candidate a numerical score, and the candidate with the highest average score is elected. Score voting includes the well-known approval voting (used to calculate approval ratings), but also lets voters give partial (in-between) approval ratings to candidates.</p>
	HTML;


$page->parent('voting_methods.html');
$page->previous('approval_voting.html');
$page->next('informed_score_voting.html');

$page->body($div_introduction);
$page->body($div_What_is_Score_Voting_Scoring_Simplified);
$page->body($div_Approval_Voting_A_Familiar_Cousin);
$page->body($div_Scoring_Systems_All_Relative);
$page->body($div_Scoring_Ranges_How_Much_Nuance_Do_We_Need);
$page->body($div_Bullet_Voting_Is_It_Strategic);
$page->body($div_Informed_Nomination_A_New_Way_to_Shape_the_Candidate_Pool);
$page->body($div_Benefits_of_Score_Voting_A_Stronger_More_Representative_Democracy);
$page->body($div_Making_the_Switch_A_Simple_Step_Towards_Better_Elections);
$page->body($div_Tie_Breaking);
$page->body($div_Conclusion_Your_Score_Matters);



$page->related_tag("Score voting");
$page->body($div_wikipedia_Score_voting);
