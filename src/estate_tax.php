<?php
$page = new Page();
$page->h1("Estate Tax");
$page->tags("Taxes", "Tax");
$page->keywords("Estate Tax", "estate tax");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Estate_tax_in_the_United_States = new WikipediaContentSection();
$div_wikipedia_Estate_tax_in_the_United_States->setTitleText("Estate tax in the United States");
$div_wikipedia_Estate_tax_in_the_United_States->setTitleLink("https://en.wikipedia.org/wiki/Estate_tax_in_the_United_States");
$div_wikipedia_Estate_tax_in_the_United_States->content = <<<HTML
	<p>In the United States, the estate tax is a federal tax on the transfer of the estate of a person who dies. The tax applies to property that is transferred by will or, if the person has no will, according to state laws of intestacy. Other transfers that are subject to the tax can include those made through a trust and the payment of certain life insurance benefits or financial accounts. The estate tax is part of the federal unified gift and estate tax in the United States. The other part of the system, the gift tax, applies to transfers of property during a person's life.</p>
	HTML;


$page->parent('list_of_taxes.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Estate Tax");
$page->body($div_wikipedia_Estate_tax_in_the_United_States);
