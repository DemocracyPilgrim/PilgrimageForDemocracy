<?php
$page = new Page();
$page->h1("Economy");
$page->tags("Living: Economy");
$page->keywords("Economy", "economy", "economies", "economic");
$page->stars(0);
$page->viewport_background("/free/economy.png");

$page->snp("description", "Can we marry economy, social justice and environment protection?");
$page->snp("image",       "/free/economy.1200-630.png");

$page->preview( <<<HTML
	<p>Can we marry economy, social justice and environment protection?</p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>We are going to review the connections between economy and social justice, economy and democracy, as well as economy and environmental protection.</p>
	HTML;

$div_wikipedia_Economy = new WikipediaContentSection();
$div_wikipedia_Economy->setTitleText("Economy");
$div_wikipedia_Economy->setTitleLink("https://en.wikipedia.org/wiki/Economy");
$div_wikipedia_Economy->content = <<<HTML
	<p>An economy is an area of the production, distribution and trade, as well as consumption of goods and services. In general, it is defined as a social domain that emphasize the practices, discourses, and material expressions associated with the production, use, and management of resources. A given economy is a set of processes that involves its culture, values, education, technological evolution, history, social organization, political structure, legal systems, and natural resources as main factors. These factors give context, content, and set the conditions and parameters in which an economy functions. In other words, the economic domain is a social domain of interrelated human practices and transactions that does not stand alone.</p>
	HTML;


$page->parent('living.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Economy");
$page->body($div_wikipedia_Economy);
