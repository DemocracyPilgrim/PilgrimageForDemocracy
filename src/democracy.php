<?php
$page = new Page();
$page->h1('Democracy');
$page->stars(2);
$page->keywords('democracy', 'Democracy', 'democratic', 'Democratic', 'Democracies', 'democracies');
$page->viewport_background('/free/democracy.png');

$page->preview( <<<HTML
	<p>Where we explore a full, comprehensive definition of democracy.</p>
	HTML );



$page->snp("description", "What is democracy anyway?");
$page->snp("image",       "/free/democracy.1200-630.png");



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Global democracy is the ultimate goal of the $Pilgrimage.</p>
	HTML;




$div_wikipedia_Democracy = new WikipediaContentSection();
$div_wikipedia_Democracy->setTitleText('Democracy');
$div_wikipedia_Democracy->setTitleLink('https://en.wikipedia.org/wiki/Democracy');
$div_wikipedia_Democracy->content = <<<HTML
	<p>Democracy is a form of government in which the people have the authority to deliberate and decide legislation ("direct democracy"),
	or to choose governing officials to do so ("representative democracy").
	Who is considered part of "the people" and how authority is shared among or delegated by the people has changed over time
	and at different rates in different countries.
	Features of democracy often include freedom of assembly, association, property rights, freedom of religion and speech, citizenship,
	consent of the governed, voting rights, freedom from unwarranted governmental deprivation of the right to life and liberty, and minority rights.</p>
	HTML;






$page->parent('menu.html');
$page->body($div_introduction);


// Preface
$page->body('invoking_democracy.html');

// Eight Levels
$page->body('first_level_of_democracy.html');
$page->body('second_level_of_democracy.html');
$page->body('third_level_of_democracy.html');
$page->body('fourth_level_of_democracy.html');
$page->body('fifth_level_of_democracy.html');
$page->body('sixth_level_of_democracy.html');
$page->body('seventh_level_of_democracy.html');
$page->body('eighth_level_of_democracy.html');

// Addenda
$page->body('democracy_wip.html');
$page->body('the_soloist_and_the_choir.html');
$page->body('saints_and_little_devils.html');
$page->body('teaching_democracy.html');
$page->body('democracy_taxes.html');
$page->body('environmental_stewardship.html');
$page->body('digital_habitat.html');




$page->body($div_wikipedia_Democracy);
