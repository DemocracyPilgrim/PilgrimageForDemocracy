<?php
$page = new Page();
$page->h1("National Chengchi University (國立政治大學)");
$page->keywords("National Chengchi University");
$page->tags("Organisation", "Taiwan");
$page->stars(1);

$page->snp("description", "Public research university in Taipei, Taiwan.");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p>National Chengchi University (國立政治大學) is a public research university in Taipei, $Taiwan.</p>
	HTML );

$r1 = $page->ref('https://www.nccu.edu.tw/p/426-1000-5.php?Lang=en', 'National Chengchi University: Introduction');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>National Chengchi University (國立政治大學) is a public research university in Taipei, $Taiwan.</p>

	<p>The university's College of Social Sciences (社會科學學院) has a Department of Political Science (政治學系)</p>
	HTML;


$list = new ListOfPages();
$list->add('chi_huang.html');
$print_list = $list->print();

$div_list = new ContentSection();
$div_list->content = <<<HTML
	<h3>Related content</h3>

	<p>People associated with the National Chengchi University:</p>

	$print_list
	HTML;




$div_National_Chengchi_University_website = new WebsiteContentSection();
$div_National_Chengchi_University_website->setTitleText("National Chengchi University website ");
$div_National_Chengchi_University_website->setTitleLink("https://www.nccu.edu.tw/index.php?Lang=en");
$div_National_Chengchi_University_website->content = <<<HTML
	<p>National Chengchi University (NCCU) was founded in 1927, and for more than 9 decades of re-formation and development,
	we have been upholding our motto, “Harmony, Independence, Balance and Preeminence”,
	We continue to refine our teaching methods and research in order to nurture talents for our country and the world. $r1</p>

	<p>We currently have 12 colleges excelling in the Liberal Arts, Law, Commerce, Science, Foreign Languages, Social Sciences,
	Communication, International Affairs, Education, Innovation, Informatics, Global Banking and Finance.
	We have 34 departments, one undeclared major in the College of Communication, 43 master's programs and 34 doctoral programs.
	Additionally, NCCU offers 12 on-the-job Master’s Programs,
	and also 7 distinguished International Master's Programs and International Doctoral Program.</p>

	<p>NCCU also has 10 university-level research centers：the Institute of International Relations, Election Study Center, Center for the Third Sector,
	Center for Creativity and Innovation Studies, Taiwan Studies Center, Center for China Studies, Humanities Research Center,
	Center for Aboriginal Studies, Center for Mind, Brain and Learning and Center for the Study of Chinese Religions.
	In addition, NCCU forms an educational system from pre-school level to the Doctoral program with its affiliated institutions,
	including a high school, an experimental elementary school and a kindergarten.</p>

	<ul>
		<li>Total Students: 16,000</li>
		<li>International Degree-Seeking Students: 1,000+</li>
		<li>International Exchange Students: 780+</li>
		<li>Chinese Learning Center Students: 900+</li>
		<li>Total Faculty Members: 1,400+ (Full time and Part time)</li>
	</ul>
	HTML;




$div_Department_of_Political_Science = new WebsiteContentSection();
$div_Department_of_Political_Science->setTitleText("Department of Political Science ");
$div_Department_of_Political_Science->setTitleLink("https://www.nccu.edu.tw/index.php?Plugin=o_nccu&Action=nccuunitdet&Lang=en&unit=202");
$div_Department_of_Political_Science->content = <<<HTML
	<p>The Department of Political Science is Taiwan’s leading institute of political research and theoretical development,
	producing generations of outstanding scholars and practitioners in academia and real world political circles.
	With 17 full-time faculty members and 7 adjunct faculty, the Department has over 220 undergraduate majors,
	and over 100 graduate students in the master’s and doctoral programs.
	In addition to teaching, faculty members are actively involved in research and community enterprise.</p>

	<p>Chinese: <a href="https://css.nccu.edu.tw/">社會科學學院</a> > <a href="https://politics.nccu.edu.tw/">政治學系</a>.</p>
	HTML;





$div_wikipedia_National_Chengchi_University = new WikipediaContentSection();
$div_wikipedia_National_Chengchi_University->setTitleText("National Chengchi University");
$div_wikipedia_National_Chengchi_University->setTitleLink("https://en.wikipedia.org/wiki/National_Chengchi_University");
$div_wikipedia_National_Chengchi_University->content = <<<HTML
	<p>National Chengchi University (國立政治大學) is a public research university in Taipei, $Taiwan.
	The university is also considered as the earliest public service training facility of the ${'Republic of China'}.
	First established in Nanjing in 1927, the university was subsequently relocated to Taipei
	and resumed full operation in 1954 as the second National re-established University in Taiwan.</p>

	<p>The university, abbreviated as NCCU, specializes in arts and humanities, mass media, linguistics and literature,
	social sciences, economics, management, politics, and international affairs.</p>
	HTML;


$page->parent('list_of_organisations.html');
$page->parent('political_science_in_taiwan.html');
$page->template("stub");
$page->body($div_introduction);

$page->body($div_list);

$page->body($div_National_Chengchi_University_website);
$page->body($div_Department_of_Political_Science);

$page->body($div_wikipedia_National_Chengchi_University);
