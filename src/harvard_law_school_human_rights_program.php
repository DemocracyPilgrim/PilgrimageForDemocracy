<?php
$page = new OrganisationPage();
$page->h1("Harvard Law School Human Rights Program");
$page->viewport_background("");
$page->keywords("Harvard Law School Human Rights Program");
$page->stars(0);
$page->tags("Organisation", "Harvard Law School", "Human Rights");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>THe Human Rights Program is a research program by the ${"Harvard Law School"}.</p>
	HTML;




$div_Human_Rights_Program = new WebsiteContentSection();
$div_Human_Rights_Program->setTitleText("Human Rights Program ");
$div_Human_Rights_Program->setTitleLink("https://hrp.law.harvard.edu/");
$div_Human_Rights_Program->content = <<<HTML
	<p>The Human Rights Program seeks to inspire critical engagement with the human rights project and to inform developments in the field through impartial, innovative, and rigorous research. Founded by Professor Emeritus Henry Steiner in 1984, the Program helps students, advocates, and scholars deepen and disseminate their knowledge of human rights.</p>
	HTML;



$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Harvard Law School Human Rights Program");
$page->body($div_Human_Rights_Program);
