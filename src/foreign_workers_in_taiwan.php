<?php
$page = new Page();
$page->h1("Foreign Workers in Taiwan");
$page->tags("Living: Fair Treatment", "Taiwan", "Foreign Workers", "International");
$page->keywords("Foreign Workers in Taiwan");
$page->stars(2);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$h2_Brokerage_System = new h2HeaderContent("The Brokerage System");


$div_Brokerage_System = new ContentSection();
$div_Brokerage_System->content = <<<HTML
	<p>$Taiwan's historical reliance on a brokerage system for recruitment of foreign workers has led to significant problems.
	Brokers often charge high fees to workers, creating substantial debt that can trap them in exploitative situations.
	This debt bondage can make it difficult for workers to leave their jobs, even if conditions are poor, as they fear being unable to repay their debts.</p>

	<p>This system has been subject to criticism for creating a vulnerability for workers to exploitation and for contributing to human trafficking.
	Recent efforts have been made to move towards a direct hiring system, but the brokerage system remains prevalent, particularly in certain sectors.</p>
	HTML;


$h2_The_Infantilization_of_Migrant_Caregivers = new h2HeaderContent("The Infantilization of Migrant Caregivers");

$div_The_Infantilization_of_Migrant_Caregivers = new ContentSection();
$div_The_Infantilization_of_Migrant_Caregivers->content = <<<HTML
	<p>Migrant caregivers, predominantly women in their 20s, 30s, and 40s, often find themselves in a paradoxical situation.
	While legally adults, they are frequently treated as children, subject to the authority of both their brokers and employers.
	This infantilization manifests in various ways. </p>
	HTML;


$div_Restricted_Freedom_and_Control = new ContentSection();
$div_Restricted_Freedom_and_Control->content = <<<HTML
	<h3>Restricted Freedom and Control</h3>

	<p>Despite working long hours, often exceeding their contractual obligations and being on call 24/7,
	caregivers are often denied their legally stipulated free days.
	Even when granted a day off, they may require permission from their employers,
	effectively stripping them of the autonomy to exercise their legal rights.</p>
	HTML;



$div_Social_Isolation_and_Restrictions = new ContentSection();
$div_Social_Isolation_and_Restrictions->content = <<<HTML
	<h3>Social Isolation and Restrictions</h3>

	<p>Furthermore, these adult women are often subjected to restrictions on their personal lives.
	They may be prevented from socializing or going out, often under the guise of "protecting" them from perceived dangers.
	This infantilizing approach effectively isolates them from their peers and hinders their ability to build social connections.
	Their romantic lives are also severely restricted, with employers
	often forbidding them from bringing partners home or sleeping away from their assigned residences.</p>
	HTML;


$div_A_Parallel_to_Historical_Discrimination = new ContentSection();
$div_A_Parallel_to_Historical_Discrimination->content = <<<HTML
	<h3>A Parallel to Historical Discrimination</h3>

	<p>This practice of treating adult women as irresponsible children, denying them basic freedoms and control over their lives,
	echoes the discriminatory laws against interracial marriage that existed in the United States until 1967 (${'Loving v. Virginia'}).
	In Taiwan, foreign caregivers, despite being mature adults, are subjected to similar restrictions,
	effectively barring them from forming romantic relationships and hindering their ability to experience a full and independent life.</p>
	HTML;







$div_wikipedia_Migrant_caregivers_in_Taiwan = new WikipediaContentSection();
$div_wikipedia_Migrant_caregivers_in_Taiwan->setTitleText("Migrant caregivers in Taiwan");
$div_wikipedia_Migrant_caregivers_in_Taiwan->setTitleLink("https://en.wikipedia.org/wiki/Migrant_caregivers_in_Taiwan");
$div_wikipedia_Migrant_caregivers_in_Taiwan->content = <<<HTML
	<p>Taiwan, with a population of 23.58 million as of 2022, is home to around 750,000 foreign workers, commonly known as "外勞" or "wailao".
	Among them, around 230,000 are care workers, mainly from Southeast Asia. They represent 2% of the total workforce in Taiwan.</p>
	HTML;

$div_wikipedia_Human_rights_in_Taiwan = new WikipediaContentSection();
$div_wikipedia_Human_rights_in_Taiwan->setTitleText("Human rights in Taiwan");
$div_wikipedia_Human_rights_in_Taiwan->setTitleLink("https://en.wikipedia.org/wiki/Human_rights_in_Taiwan");
$div_wikipedia_Human_rights_in_Taiwan->content = <<<HTML
	<p>The human rights record in Taiwan is generally held to have experienced significant transformation since the 1990s.</p>
	HTML;


$page->parent('migrant_workers.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($h2_Brokerage_System);
$page->body($div_Brokerage_System);


$page->body($h2_The_Infantilization_of_Migrant_Caregivers);
$page->body($div_The_Infantilization_of_Migrant_Caregivers);
$page->body($div_Restricted_Freedom_and_Control);
$page->body($div_Social_Isolation_and_Restrictions);
$page->body($div_A_Parallel_to_Historical_Discrimination);

$page->body($div_wikipedia_Migrant_caregivers_in_Taiwan);
$page->body($div_wikipedia_Human_rights_in_Taiwan);
