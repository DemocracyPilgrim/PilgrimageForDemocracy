<?php
$page = new VideoPage();
$page->h1("Prosecuting Donald Trump: Election Security Matters");
$page->tags("Video: Podcast", "Prosecuting Donald Trump", "Adav Noti", "Campaign Legal Center", "Election Integrity", "Elections", "Donald Trump");
$page->keywords("Prosecuting Donald Trump: Election Security Matters");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;


$div_Election_Security_Matters = new WebsiteContentSection();
$div_Election_Security_Matters->setTitleText("Election Security Matters");
$div_Election_Security_Matters->setTitleLink("https://www.msnbc.com/msnbc-podcast/prosecuting-donald-trump-election-security-matters-rcna171645");
$div_Election_Security_Matters->content = <<<HTML
	<p>As the FBI continues to investigate the second apparent Trump assassination attempt, MSNBC legal analysts Andrew Weissmann and Mary McCord describe the charges brought and the reasoning behind them. Then, they move to the latest out of Georgia after Judge McAfee dismissed three counts from the 41-count indictment, while allowing the rest to go forward. And lastly, Mary and Andrew welcome Adav Noti, Executive Director of the Campaign Legal Center, to calm some concerns about certain jurisdictions refusing to certify this November’s election results.</p>
	HTML;



$page->parent('list_of_videos.html');
$page->template("stub");
$page->body($div_introduction);
$page->body($div_Election_Security_Matters);
