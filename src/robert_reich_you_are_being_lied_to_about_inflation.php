<?php
$page = new VideoPage();
$page->h1("Robert Reich: You Are Being Lied to About Inflation");
$page->tags("Video: News Story", "Robert Reich", "Inflation", "Living: Economy");
$page->keywords("Robert Reich: You Are Being Lied to About Inflation");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;



$div_youtube_You_Are_Being_Lied_to_About_Inflation_Robert_Reich = new YoutubeContentSection();
$div_youtube_You_Are_Being_Lied_to_About_Inflation_Robert_Reich->setTitleText("You Are Being Lied to About Inflation | Robert Reich");
$div_youtube_You_Are_Being_Lied_to_About_Inflation_Robert_Reich->setTitleLink("https://www.youtube.com/watch?v=Zi4KMCQuQYE");
$div_youtube_You_Are_Being_Lied_to_About_Inflation_Robert_Reich->content = <<<HTML
	<p>
	</p>
	HTML;



$page->parent('list_of_videos.html');
$page->template("stub");
$page->body($div_introduction);

$page->body($div_youtube_You_Are_Being_Lied_to_About_Inflation_Robert_Reich);
