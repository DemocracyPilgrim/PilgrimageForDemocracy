<?php
$page = new Page();
$page->h1("Counting the Vote: A Firing Line Special with Margaret Hoover");
$page->tags("Documentary", "USA", "Electoral System", "Donald Trump", "Elections", "Firing Line", "David Becker");
$page->keywords("Counting the Vote: A Firing Line Special with Margaret Hoover");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>"Counting the Vote" is a one-hour documentary produced by the $PBS program ${'Firing Line'}
	about the vote counting process during the $American 2020 presidential $election.</p>

	<p>It discuss the so-called "red mirage" in battleground swing states.</p>

	<p>${"David Becker"}, founder of the ${"Center for Election Innovation & Research"}, is among the interviewees in the program.</p>
	HTML;




$div_Counting_The_Vote_A_Firing_Line_Special_with_Margaret_Hoover = new WebsiteContentSection();
$div_Counting_The_Vote_A_Firing_Line_Special_with_Margaret_Hoover->setTitleText("Counting The Vote: A Firing Line Special with Margaret Hoover");
$div_Counting_The_Vote_A_Firing_Line_Special_with_Margaret_Hoover->setTitleLink("https://www.pbs.org/video/counting-the-vote-a-firing-line-special-with-margaret-hoover-uvcwma/");
$div_Counting_The_Vote_A_Firing_Line_Special_with_Margaret_Hoover->content = <<<HTML
	<p>In this one-hour documentary, Margaret Hoover embarks on a journey to explore voting systems across the United States.
	She examines methods to increase voter confidence and sheds light on states that face challenges in their vote count processes as the 2024 election approaches.</p>
	HTML;



$div_youtube_Counting_the_Vote_A_Firing_Line_Special_with_Margaret_Hoover = new YoutubeContentSection();
$div_youtube_Counting_the_Vote_A_Firing_Line_Special_with_Margaret_Hoover->setTitleText("Counting the Vote: A Firing Line Special with Margaret Hoover");
$div_youtube_Counting_the_Vote_A_Firing_Line_Special_with_Margaret_Hoover->setTitleLink("https://www.youtube.com/watch?v=ZVSTFw2AgO8");
$div_youtube_Counting_the_Vote_A_Firing_Line_Special_with_Margaret_Hoover->content = <<<HTML
	<p>In this one-hour documentary, Margaret Hoover embarks on a journey to explore voting systems across the United States. She examines methods to increase voter confidence and sheds light on states that face challenges in their vote count processes as the 2024 election approaches.

	“Counting the Vote” looks back at two of the most bitterly contested elections in American history—2000 and 2020—and examines subsequent efforts to make the casting and counting of ballots more efficient and inclusive. Several pivotal swing states have taken steps to improve election efficiency since 2020, including Michigan, but other potential tipping points in a close presidential contest like Pennsylvania and Wisconsin could be careening toward a crisis.

	How did Florida become the gold standard for election administration after the chaos of 2000? Why have some states succeeded in implementing common sense reforms while lawmakers in others struggle to find bipartisan consensus to solve known problems? Amid a flood of disinformation and conspiracy theories, how confident can voters be that their ballots will be counted quickly and accurately this November?

	In “Counting the Vote,” personal stories and expert voices from across the country–including interviews with those currently overseeing elections like Republican Georgia Secretary of State Brad Raffensperger and Democratic Michigan Secretary of State Jocelyn Benson–provide a comprehensive understanding of the most powerful tool in American democracy, the threats that could undermine the will of the people, and what it takes to protect the vote count.</p>
	HTML;



$page->parent('list_of_documentaries.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_Counting_The_Vote_A_Firing_Line_Special_with_Margaret_Hoover);
$page->body($div_youtube_Counting_the_Vote_A_Firing_Line_Special_with_Margaret_Hoover);
