<?php
$page = new Page();
$page->h1("Land value tax");
$page->keywords("Land Value Tax", "land value tax", "LVT");
$page->tags("Tax", "Land", "Organic Tax");
$page->stars(0);

//$page->snp("description", "");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>The Land Value Tax (LVT) is an organic tax based on the natural value of a tract of land,
	disregarding any man-made modification of the land.</p>

	<p>The ${'Republic of China'} ($Taiwan) is one of the few countries around the world to have an effective land value tax.</p>
	HTML;

$div_wikipedia_Land_value_tax = new WikipediaContentSection();
$div_wikipedia_Land_value_tax->setTitleText("Land value tax");
$div_wikipedia_Land_value_tax->setTitleLink("https://en.wikipedia.org/wiki/Land_value_tax");
$div_wikipedia_Land_value_tax->content = <<<HTML
	<p>A land value tax (LVT) is a levy on the value of land without regard to buildings, personal property and other improvements upon it.
	It is also known as a location value tax, a point valuation tax, a site valuation tax, split rate tax, or a site-value rating.</p>

	<p>Some economists favor LVT, arguing they do not cause economic inefficiency, and help reduce economic inequality.
	A land value tax is a progressive tax, in that the tax burden falls on land owners, because land ownership is correlated with wealth and income.
	The land value tax has been referred to as "the perfect tax" and
	the economic efficiency of a land value tax has been accepted since the eighteenth century.
	Economists since Adam Smith and David Ricardo have advocated this tax because it does not hurt economic activity,
	and encourages development without subsidies.</p>
	HTML;


$page->parent('taxes.html');
$page->template("stub");
$page->body($div_introduction);

$page->body('henry_george.html');

$page->related_tag("Land Value Tax");

$page->body($div_wikipedia_Land_value_tax);
