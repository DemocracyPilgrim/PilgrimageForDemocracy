<?php
$page = new VideoPage();
$page->h1("Ari Melber interviews Yuval Harari: Debunking Trump's lies");
$page->tags("Video: Interview", "Ari Melber", "Yuval Harari", "Disinformation", "Donald Trump");
$page->keywords("Ari Melber interviews Yuval Harari: Debunking Trump's lies");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;



$div_youtube_Debunking_Trump_s_lies_Obama_s_fave_historian_Yuval_Harari_busts_MAGA_playbook_in_Ari_Melber_intv = new YoutubeContentSection();
$div_youtube_Debunking_Trump_s_lies_Obama_s_fave_historian_Yuval_Harari_busts_MAGA_playbook_in_Ari_Melber_intv->setTitleText("Debunking Trump’s lies: Obama’s fave historian Yuval Harari busts MAGA playbook in Ari Melber intv");
$div_youtube_Debunking_Trump_s_lies_Obama_s_fave_historian_Yuval_Harari_busts_MAGA_playbook_in_Ari_Melber_intv->setTitleLink("https://www.youtube.com/watch?v=toF5PIClNZ4");
$div_youtube_Debunking_Trump_s_lies_Obama_s_fave_historian_Yuval_Harari_busts_MAGA_playbook_in_Ari_Melber_intv->content = <<<HTML
	<p>
	</p>
	HTML;


$page->parent('list_of_videos.html');
$page->template("stub");
$page->body($div_introduction);



$page->body($div_youtube_Debunking_Trump_s_lies_Obama_s_fave_historian_Yuval_Harari_busts_MAGA_playbook_in_Ari_Melber_intv);
