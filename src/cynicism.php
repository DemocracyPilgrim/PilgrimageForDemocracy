<?php
$page = new Page();
$page->h1('Cynicism');
$page->keywords('cynicism');
$page->tags("Information: Discourse", "Political Discourse");
$page->stars(0);

//$page->snp('description', '');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Cynicism_contemporary = new WikipediaContentSection();
$div_wikipedia_Cynicism_contemporary->setTitleText('Cynicism contemporary');
$div_wikipedia_Cynicism_contemporary->setTitleLink('https://en.wikipedia.org/wiki/Cynicism_(contemporary)');
$div_wikipedia_Cynicism_contemporary->content = <<<HTML
	<p>Cynicism is an attitude characterized by a general distrust of the motives of others.
	A cynic may have a general lack of faith or hope in people motivated
	by ambition, desire, greed, gratification, materialism, goals, and opinions
	that a cynic perceives as vain, unobtainable, or ultimately meaningless.
	The term originally derives from the ancient Greek philosophers, the Cynics,
	who rejected conventional goals of wealth, power, and honor.
	They practiced shameless nonconformity with social norms in religion, manners, housing, dress, or decency,
	instead advocating the pursuit of virtue in accordance with a simple and natural way of life.</p>
	HTML;


$page->parent('political_discourse.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_wikipedia_Cynicism_contemporary);
