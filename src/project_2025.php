<?php
$page = new Page();
$page->h1("Project 2025");
$page->tags("USA", "Donald Trump", "Organisation");
$page->keywords("Project 2025");
$page->stars(0);

//$page->snp("description", "");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref('', '');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Project_2025 = new WikipediaContentSection();
$div_wikipedia_Project_2025->setTitleText("Project 2025");
$div_wikipedia_Project_2025->setTitleLink("https://en.wikipedia.org/wiki/Project_2025");
$div_wikipedia_Project_2025->content = <<<HTML
	<p>Project 2025, also known as the Presidential Transition Project,
	is a collection of policy proposals to reshape the executive branch of the U.S. federal government
	at an unprecedented scale in the event of a Republican victory in the 2024 U.S. presidential election.
	Established in 2022, the project seeks to recruit tens of thousands of conservatives to the District of Columbia
	to replace existing federal civil service workers—whom Republicans characterize as part of the "deep state"—and to further the objectives
	of the next Republican president.
	Although the project cannot promote a specific presidential candidate,
	many contributors have close ties to Donald Trump and the his presidential campaign.
	The plan would perform a swift restructuring of the executive branch
	under a maximalist interpretation of the unitary executive theory—a challenged legal theory that contends
	the president has absolute power over the executive branch—upon inauguration.</p>
	HTML;


$page->parent('american_fascism.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_wikipedia_Project_2025);
