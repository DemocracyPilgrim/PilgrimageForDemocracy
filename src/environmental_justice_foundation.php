<?php
$page = new OrganisationPage();
$page->h1("Environmental Justice Foundation");
$page->viewport_background("");
$page->keywords("Environmental Justice Foundation");
$page->stars(0);
$page->tags("Organisation", "Humanity", "Justice", "Environment", "NGO");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;





$div_EJF_Website = new WebsiteContentSection();
$div_EJF_Website->setTitleText("EJF Website ");
$div_EJF_Website->setTitleLink("https://ejfoundation.org/");
$div_EJF_Website->content = <<<HTML
	<p>The Environmental Justice Foundation's (EJF) mission is to protect people and planet.</p>
	<p>The Environmental Justice Foundation’s global campaigns, programmes and projects focus on issues of environmental justice which have a link to international demand or government policies, and where international attention and action can secure change for good.</p>
	<p>Today, the Environmental Justice Foundation work focuses on protecting the world’s seas and oceans by exposing illegal ‘pirate’ fishing and exploitation in the seafood industry; highlighting the dangers of toxic pesticides and promoting viable organic alternatives; and exposing the impacts of our changing global climate on the most vulnerable people on our planet.</p>
	HTML;




$div_youtube_Environmental_Justice_Foundation = new YoutubeContentSection();
$div_youtube_Environmental_Justice_Foundation->setTitleText("Environmental Justice Foundation");
$div_youtube_Environmental_Justice_Foundation->setTitleLink("https://www.youtube.com/@EnvironmentalJusticeFoundation/featured");
$div_youtube_Environmental_Justice_Foundation->content = <<<HTML
	<p>
	</p>
	HTML;



$div_wikipedia_Environmental_Justice_Foundation = new WikipediaContentSection();
$div_wikipedia_Environmental_Justice_Foundation->setTitleText("Environmental Justice Foundation");
$div_wikipedia_Environmental_Justice_Foundation->setTitleLink("https://en.wikipedia.org/wiki/Environmental_Justice_Foundation");
$div_wikipedia_Environmental_Justice_Foundation->content = <<<HTML
	<p>The Environmental Justice Foundation (EJF) is a non-governmental organization (NGO) founded in 2000 by Steve Trent and Juliette Williams that works to secure a world where natural habitats and environments can sustain, and be sustained by, the communities that depend upon them for their basic needs and livelihoods. It promotes global environmental justice, which it defines as “equal access to a secure and healthy environment for all, in a world where wildlife can thrive alongside humanity.”</p>
	HTML;


$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);



$page->body($div_EJF_Website);
$page->body($div_youtube_Environmental_Justice_Foundation);
$page->related_tag("Environmental Justice Foundation");
$page->body($div_wikipedia_Environmental_Justice_Foundation);
