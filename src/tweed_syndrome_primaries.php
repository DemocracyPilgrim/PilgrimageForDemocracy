<?php
$page = new Page();
$page->h1("Tweed symptom 2: primaries");
$page->stars(0);

//$page->snp("description", "");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref('', '');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;



$div_wikipedia_Congressional_nominating_caucus = new WikipediaContentSection();
$div_wikipedia_Congressional_nominating_caucus->setTitleText("Congressional nominating caucus");
$div_wikipedia_Congressional_nominating_caucus->setTitleLink("https://en.wikipedia.org/wiki/Congressional_nominating_caucus");
$div_wikipedia_Congressional_nominating_caucus->content = <<<HTML
	<p>The congressional nominating caucus is the name for informal meetings in which American congressmen
	would agree on whom to nominate for the presidency and vice presidency from their political party.</p>
	HTML;


$div_wikipedia_United_States_presidential_nominating_convention = new WikipediaContentSection();
$div_wikipedia_United_States_presidential_nominating_convention->setTitleText("United States presidential nominating convention");
$div_wikipedia_United_States_presidential_nominating_convention->setTitleLink("https://en.wikipedia.org/wiki/United_States_presidential_nominating_convention");
$div_wikipedia_United_States_presidential_nominating_convention->content = <<<HTML
	<p>A United States presidential nominating convention is a political convention held every four years
	in the United States by most of the political parties who will be fielding nominees in the upcoming U.S. presidential election.
	The formal purpose of such a convention is to select the party's nominee for popular election as President,
	as well as to adopt a statement of party principles and goals known as the party platform and adopt the rules for the party's activities,
	including the presidential nominating process for the next election cycle.</p>
	HTML;



$page->parent('tweed_syndrome.html');
$page->template("stub");
$page->body($div_introduction);

$page->body($div_wikipedia_United_States_presidential_nominating_convention);
$page->body($div_wikipedia_Congressional_nominating_caucus);
