<?php
$page = new VideoPage();
$page->h1("Glenn Kirschner on Trump's legal issues for Haitian immigrant hoax");
$page->tags("Video", "Glenn Kirschner", "Brian Tyler Cohen", "Donald Trump", "Disinformation", "Haiti", "Freedom of Speech", "Stochastic Terrorism", "USA", "Brandenburg v. Ohio");
$page->stars(1);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>In the video below, Glenn Kirschner acknowledges the thorny area of where ${'free speech'} ends and unprotected speech that incites violence begins.</p>

	<p>The Supreme Court's Brandenburg case established that the First Amendment
	does not protect speech intended to incite imminent violence that is "reasonably likely" to occur.</p>

	<p>The discussion takes place in the context where presidential candidate ${'Donald Trump'} was instrumental in spreading disinformation regarding
	$immigrants eating pet cats in Springfield, Ohio, inciting violent threats taking place there.</p>

	<p>Kirschner makes a parallel with Trump's disinformation regarding COVID while he was president, which caused the deaths of countless Americans.</p>
	HTML;




$div_youtube_Trump_legal_issues_for_Haitian_immigrant_hoax = new YoutubeContentSection();
$div_youtube_Trump_legal_issues_for_Haitian_immigrant_hoax->setTitleText("The Legal Breakdown with Glenn Kirschner: Trump's legal issues for Haitian immigrant hoax");
$div_youtube_Trump_legal_issues_for_Haitian_immigrant_hoax->setTitleLink("https://www.youtube.com/watch?v=E9V3Dbn5UP8");
$div_youtube_Trump_legal_issues_for_Haitian_immigrant_hoax->content = <<<HTML
	<p>Legal Breakdown episode 374: Glenn Kirschner discusses Trump's lies about Haitian immigrants
	and whether he has any legal exposure for inciting violence against them.</p>
	HTML;



$page->parent('list_of_videos.html');
$page->template("stub");
$page->body($div_introduction);
$page->body('brandenburg_v_ohio.html');
$page->body('springfield_ohio_cat_eating_hoax.html');

$page->body($div_youtube_Trump_legal_issues_for_Haitian_immigrant_hoax);
