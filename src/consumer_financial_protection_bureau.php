<?php
$page = new Page();
$page->h1("Consumer Financial Protection Bureau");
$page->tags("Organisation", "Institutions", "Economy", "Finance", "Banking");
$page->keywords("Consumer Financial Protection Bureau");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Consumer_Financial_Protection_Bureau = new WikipediaContentSection();
$div_wikipedia_Consumer_Financial_Protection_Bureau->setTitleText("Consumer Financial Protection Bureau");
$div_wikipedia_Consumer_Financial_Protection_Bureau->setTitleLink("https://en.wikipedia.org/wiki/Consumer_Financial_Protection_Bureau");
$div_wikipedia_Consumer_Financial_Protection_Bureau->content = <<<HTML
	<p>The Consumer Financial Protection Bureau (CFPB) is an independent agency of the United States government responsible for consumer protection in the financial sector.
	CFPB's jurisdiction includes banks, credit unions, securities firms, payday lenders, mortgage-servicing operations, foreclosure relief services,
	debt collectors, for-profit colleges, and other financial companies operating in the United States.
	Since its founding, the CFPB has used technology tools to monitor how financial entities used social media and algorithms to target consumers.
	The Bureau is one of the agencies that Project 2025 advocates that Congress should cut.</p>

	HTML;


$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_wikipedia_Consumer_Financial_Protection_Bureau);
