<?php
$page = new Page();
$page->h1("North American Congress on Latin America");
$page->keywords("North American Congress on Latin America");
$page->tags("Organisation");
$page->stars(0);

//$page->snp("description", "");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;



$div_North_American_Congress_on_Latin_America_website = new WebsiteContentSection();
$div_North_American_Congress_on_Latin_America_website->setTitleText("North American Congress on Latin America website ");
$div_North_American_Congress_on_Latin_America_website->setTitleLink("https://nacla.org/");
$div_North_American_Congress_on_Latin_America_website->content = <<<HTML
	<p>The North American Congress on Latin America (NACLA) is an independent, nonprofit organization founded in 1966
	to examine and critique U.S. imperialism and political, economic, and military intervention in the Western hemisphere.
	In an evolving political and media landscape, we continue to work toward a world
	in which the nations and peoples of Latin America and the Caribbean are free from oppression,
	injustice, and economic and political subordination.</p>

	<p>For more than 50 years, NACLA has been a leading source of English-language research and analysis on Latin America and the Caribbean.
	Our mission has always been to publish historically and politically informed research and analysis on the region
	and its complex and changing relationships with the United States.
	We use the NACLA Report on the Americas, our web platform nacla.org, as well as public events and programming
	as tools for education, advocacy, dialogue, and solidarity.
	Our work aims to assist readers in interpreting the most pressing issues in Latin America and the Caribbean,
	and their connections with U.S. policy, to help further movement struggles throughout the hemisphere.</p>

	<p>Our mission is guided by our organizational values.
	NACLA offers a forum for debate among a range of voices and perspectives on the Left.
	As we enter our sixth decade, we maintain an editorial focus on issues
	related to political economy, race and indigeneity, gender/sexuality, and climate and the environment.
	NACLA also provides a platform for voices from the region,
	and has made a commitment to emphasize Black, Indigenous, Latinx, LGBTQI+, and feminist perspectives.</p>
	HTML;



$div_wikipedia_North_American_Congress_on_Latin_America = new WikipediaContentSection();
$div_wikipedia_North_American_Congress_on_Latin_America->setTitleText("North American Congress on Latin America");
$div_wikipedia_North_American_Congress_on_Latin_America->setTitleLink("https://en.wikipedia.org/wiki/North_American_Congress_on_Latin_America");
$div_wikipedia_North_American_Congress_on_Latin_America->content = <<<HTML
	<p>North American Congress in Latin America (NACLA) is a non-profit organization founded in 1966
	to provide information on trends in Latin America and relations between Latin America and the United States.
	The organization is best known for publishing the quarterly NACLA Report on the Americas,
	and also publishes "books, anthologies and pamphlets for classroom and activist use".</p>
	HTML;


$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_North_American_Congress_on_Latin_America_website);
$page->body($div_wikipedia_North_American_Congress_on_Latin_America);
