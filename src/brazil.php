<?php
$page = new CountryPage('Brazil');
$page->h1('Brazil');
$page->tags("Country");
$page->keywords('Brazil');
$page->stars(0);

$page->snp('description', '203 million inhabitants.');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Brazil is a state member of $BRICS.</p>
	HTML;

$div_wikipedia_Brazil = new WikipediaContentSection();
$div_wikipedia_Brazil->setTitleText('Brazil');
$div_wikipedia_Brazil->setTitleLink('https://en.wikipedia.org/wiki/Brazil');
$div_wikipedia_Brazil->content = <<<HTML
	<p>A major non-NATO ally of the United States, Brazil is a regional and middle power, and is also classified as an emerging power.
	Categorised as a developing country with a high Human Development Index, Brazil is considered an advanced emerging economy,
	having the tenth largest GDP in the world by nominal, and eighth by PPP measures, the largest in Latin America.</p>
	HTML;

$div_wikipedia_Elections_in_Brazil = new WikipediaContentSection();
$div_wikipedia_Elections_in_Brazil->setTitleText('Elections in Brazil');
$div_wikipedia_Elections_in_Brazil->setTitleLink('https://en.wikipedia.org/wiki/Elections_in_Brazil');
$div_wikipedia_Elections_in_Brazil->content = <<<HTML
	<p>Brazil elects on the national level a head of state—the president—and a legislature.
	The president is elected to a four-year term by absolute majority vote through a two-round system.
	The National Congress (Congresso Nacional) has two chambers.
	The Chamber of Deputies (Câmara dos Deputados) has 513 members, elected to a four-year term by proportional representation.
	The Federal Senate (Senado Federal) has 81 members, elected to an eight-year term,
	with elections every four years for alternatively one-third and two-thirds of the seats.
	Brazil has a multi-party system, with such numerous parties that often no one party has a chance of gaining power alone,
	and so they must work with each other to form coalition governments.</p>
	HTML;

$div_wikipedia_Politics_of_Brazil = new WikipediaContentSection();
$div_wikipedia_Politics_of_Brazil->setTitleText('Politics of Brazil');
$div_wikipedia_Politics_of_Brazil->setTitleLink('https://en.wikipedia.org/wiki/Politics_of_Brazil');
$div_wikipedia_Politics_of_Brazil->content = <<<HTML
	<p>The politics of Brazil take place in a framework of a federal presidential representative democratic republic,
	whereby the President is both head of state and head of government, and of a multi-party system.
	The political and administrative organization of Brazil comprises the federal government,
	the 26 states and a federal district, and the municipalities.</p>
	HTML;


$page->parent('world.html');
$page->template("stub");
$page->body($div_introduction);
$page->body('Country indices');

$page->body($div_wikipedia_Brazil);
$page->body($div_wikipedia_Elections_in_Brazil);
$page->body($div_wikipedia_Politics_of_Brazil);
