<?php
$page = new Page();
$page->h1("Fatma Karume");
$page->alpha_sort("Karume, Fatma");
$page->tags("Person", "Tanzania", "TED Talk", "Democracy", "Women's Rights", "Lawyer", "Institutions: Judiciary");
$page->keywords("Fatma Karume");
$page->stars(1);
$page->viewport_background("/free/fatma_karume.png");

$page->snp("description", "Tanzanian lawyer and women's rights activist.");
$page->snp("image",       "/free/fatma_karume.1200-630.png");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Fatma Karume (born 15 June 1969 ) is a $Tanzanian $lawyer,
	an activist who served as president of the Tanganyika Bar Association (TLS) in the period from 2018 to 2019.
	She is also a defender of ${"women's rights"} against the patriarchal system that has dominated the politics and culture of Tanzania.</p>
	HTML;



$div_TED_How_to_fight_for_democracy_in_the_shadow_of_autocracy = new TEDtalkContentSection();
$div_TED_How_to_fight_for_democracy_in_the_shadow_of_autocracy->setTitleText("How to fight for democracy in the shadow of autocracy");
$div_TED_How_to_fight_for_democracy_in_the_shadow_of_autocracy->setTitleLink("https://www.ted.com/talks/fatma_karume_how_to_fight_for_democracy_in_the_shadow_of_autocracy?subtitles=en");
$div_TED_How_to_fight_for_democracy_in_the_shadow_of_autocracy->content = <<<HTML
	<p>Democracy may be an abstract concept, but it holds the very essence of our autonomy and humanity, says lawyer and human rights advocate Fatma Karume.
	Sharing her journey navigating a tumultuous political transition in Tanzania that put her life at risk,
	she highlights the importance of speaking truth to power and fighting for a brighter democratic future.</p>
	HTML;




$div_TED_Fatma_Karume = new TEDtalkContentSection();
$div_TED_Fatma_Karume->setTitleText("Speaker: Fatma Karume");
$div_TED_Fatma_Karume->setTitleLink("https://www.ted.com/speakers/fatma_karume");
$div_TED_Fatma_Karume->content = <<<HTML
	<p>Fatma Karume is presently a non-practicing barrister who was an advocate practicing in Tanzania for the past 27 years.
	She has numerous cases reported in the East African Law Reports as well as the Tanzania Law Reports.
	She was the president of the Law Society from 2018-2019 and during her tenure advocated for the independence of bar, the judiciary, rule of law and democracy.
	She was disbarred in Tanzania Mainland in 2020 after challenging in court the President’s unconstitutional appointment of the Attorney General.
	The High Court recently overturned her disbarment for being illegal.
	She continues to advocate for democracy and rule of law.</p>
	HTML;



$div_wikipedia_Fatma_Karume = new WikipediaContentSection();
$div_wikipedia_Fatma_Karume->setTitleText("Fatma Karume");
$div_wikipedia_Fatma_Karume->setTitleLink("https://sw.wikipedia.org/wiki/Fatma_Karume");
$div_wikipedia_Fatma_Karume->content = <<<HTML
	<p>[Swahili] Fatma Karume (amezaliwa 15 Juni 1969) ni mwanasheria wa Kitanzania na wakili wa kujitegemea,
	mwanaharakati ambaye aliwahi kuwa rais wa Chama cha Wanasheria Tanganyika (TLS) katika kipindi cha mwaka 2018 hadi 2019.
	Alipata wadhifa huo baada ya kushinda katika uchaguzi wa uraisi wa chama hicho uliofanyika Aprili 2018 na hivyo kumrithi mtangulizi
	wake Tundu Lissu aliyekiongoza chama hicho kutoka mwaka 2017 hadi 2018.</p>
	HTML;


$page->parent('list_of_people.html');
$page->template("stub");
$page->body($div_introduction);

$page->body($div_TED_How_to_fight_for_democracy_in_the_shadow_of_autocracy);
$page->body($div_TED_Fatma_Karume);

$page->body($div_wikipedia_Fatma_Karume);
