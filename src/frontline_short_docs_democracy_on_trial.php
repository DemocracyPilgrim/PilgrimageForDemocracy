<?php
$page = new Page();
$page->h1("FRONTLINE Short Docs: Democracy on Trial");
$page->tags("USA", "Documentary", "Donald Trump");
$page->stars(0);

//$page->snp("description", "");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;



$div_youtube_FRONTLINE_Short_Docs_Democracy_on_Trial = new YoutubeContentSection();
$div_youtube_FRONTLINE_Short_Docs_Democracy_on_Trial->setTitleText("FRONTLINE Short Docs: Democracy on Trial");
$div_youtube_FRONTLINE_Short_Docs_Democracy_on_Trial->setTitleLink("https://www.youtube.com/playlist?list=PLt6fyfHNds7T0n4t5xy3KyBQvWR9fNYPY");
$div_youtube_FRONTLINE_Short_Docs_Democracy_on_Trial->content = <<<HTML
	<ol>
		<li>The Moment Trump Was Charged for Crimes in Office</li>
		<li>A Blueprint for Federal Criminal Charges Against Trump</li>
		<li>What Did Trump Know About 2020 Election Results?</li>
		<li>A Georgia Election Official’s Warning to Trump of Potential Violence</li>
		<li>Rusty Bowers, Then-Arizona House Speaker, Recalls Call With Trump</li>
		<li>Georgia Poll Workers Caught in an Election Fraud Conspiracy Theory</li>
		<li>The Georgia Election Phone Call Between Brad Raffensperger & Trump</li>
		<li>Trump’s Invitation for a “Big Protest” in D.C. on Jan. 6, 2021</li>
		<li>The Pressure on Trump’s DOJ in the 2020 Election’s Aftermath</li>
		<li>Pressure on VP Mike Pence Around the Jan. 6 Election Certification</li>
		<li>Cassidy Hutchinson’s Testimony Before the Jan. 6 Committee</li>
		<li>What Did President Trump Do for 187 Minutes on Jan. 6?</li>
	</ol>
	HTML;



$page->parent('list_of_documentaries.html');
$page->template("stub");
$page->body($div_introduction);

$page->body($div_youtube_FRONTLINE_Short_Docs_Democracy_on_Trial);
$page->body('donald_trump.html');
$page->body('duverger_usa_21_century.html');
