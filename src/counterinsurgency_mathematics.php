<?php
$page = new Page();
$page->h1('Counterinsurgency mathematics');
$page->keywords('counterinsurgency mathematics');
$page->stars(2);

$page->snp('description', 'When the maths of counting insurgents do not add up...');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p>When the maths of counting insurgents do not add up...</p>
	HTML );

$r1 = $page->ref('https://www.realclearpolitics.com/articles/2009/10/01/gen_mcchrystals_address_on_afghanistan_98537.html', 'Gen. McChrystal\'s Speech on Afghanistan');


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>"Counterinsurgency mathematics" is a phrase coined by US Gen. McChrystal,
	when speaking about the counterinsurgency US operations in Afghanistan.
	In a 2009 speech, Gen. McChrystal stated:</p>

	<blockquote>
	<p>There is another complexity that people do not understand and which the military have to learn:
	I call it ‘[Counterinsurgency] mathematics’.
	Intelligence will normally tell us how many insurgents are operating in an area.
	Let us say that there are 10 in a certain area.
	Following a military operation, two are killed.
	How many insurgents are left?
	Traditional mathematics would say that eight would be left, but there may only be two,
	because six of the living eight may have said,
	‘This business of insurgency is becoming dangerous so I am going to do something else.’</p>

	<p>There are more likely to be as many as 20, because each one you killed has a brother, father, son and friends,
	who do not necessarily think that they were killed because they were doing something wrong.
	It does not matter – you killed them.
	Suddenly, then, there may be 20, making the calculus of military operations very different.
	Yet we are asking young corporals, sergeants and lieutenants to make those kinds of calculations and requiring them to understand the situation.
	They have to – there is no simple workaround.</p>

	<p>It is that complex: where you build the well, what military operations to run, who you talk to.
	Everything that you do is part of a complex system with expected and unexpected,
	desired and undesired outcomes, and outcomes that you never find out about.
	In my experience, I have found that the best answers and approaches may be counterintuitive;
	i.e. the opposite of what it seems like you ought to do is what ought to be done.
	When I am asked what approach we should take in Afghanistan, I say ‘humility’.$r1</p>
	</blockquote>
	HTML;


$div_Gen_McChrystals_Speech_on_Afghanistan = new WebsiteContentSection();
$div_Gen_McChrystals_Speech_on_Afghanistan->setTitleText('Gen. McChrystal\'s Speech on Afghanistan [2009]');
$div_Gen_McChrystals_Speech_on_Afghanistan->setTitleLink('https://www.realclearpolitics.com/articles/2009/10/01/gen_mcchrystals_address_on_afghanistan_98537.html');
$div_Gen_McChrystals_Speech_on_Afghanistan->content = <<<HTML
	<p>I believe that the stakes are high for Afghanistan and for the region.
	An unstable Afghanistan not only negatively affects what happens within its borders but also affects its neighbours.
	Afghanistan is, in many ways, one of the keys to stability in south Asia.
	A state that can provide its own security is important to all international security,
	and certainly to that of the UK, the US and our international partnership.
	The Afghan people are worth bothering about and they deserve that.</p>
	HTML;


$div_Counterinsurgency_Math_Revisited = new WebsiteContentSection();
$div_Counterinsurgency_Math_Revisited->setTitleText('Cato Institute: Counterinsurgency Math Revisited [2018]');
$div_Counterinsurgency_Math_Revisited->setTitleLink('https://www.cato.org/blog/counterinsurgency-math-revisited');
$div_Counterinsurgency_Math_Revisited->content = <<<HTML
	<p>When does 32,200 – 60,000 = 109,000?
	That seemingly inaccurate equation represents the estimated number of Islamist‐inspired terrorists when the war on terror began,
	how many the U.S. has killed since 2015, and the number that fight today.
	And it begs the question of just how can the terror ranks grow so fast when they’re being depleted so rapidly.</p>
	HTML;

$div_Opinion_There_s_a_smarter_way_to_eliminate_Hamas = new WebsiteContentSection();
$div_Opinion_There_s_a_smarter_way_to_eliminate_Hamas->setTitleText('Robert A. Pape: There’s a smarter way to eliminate Hamas [2023]');
$div_Opinion_There_s_a_smarter_way_to_eliminate_Hamas->setTitleLink('https://edition.cnn.com/2023/11/01/opinions/israel-flawed-strategy-defeating-hamas-pape');
$div_Opinion_There_s_a_smarter_way_to_eliminate_Hamas->content = <<<HTML
	<p>Israel’s strategy for defeating Hamas — destroying its military and political capabilities
	to the point where the terrorist group can never again launch major attacks against Israeli civilians — is unlikely to work.</p>

	<p>Indeed, Israel is likely already producing more terrorists than it’s killing.</p>

	<p>To defeat terrorist groups like Hamas, it is important to separate the terrorists from the local population from which they emerge.
	Otherwise, the current generation of terrorists can be killed, only to be replaced by a new, larger generation of terrorists in the future.
	(This is described by experts as “counterinsurgency mathematics.” )</p>

	<p>Although the principle — of separating the terror group from the broader population — is simple,
	it is incredibly difficult to achieve in practice.</p>

	<p>This is why Israel and the United States have waged major military operations that killed large numbers of existing terrorists
	in the near term — but ultimately led to the rise of many more terrorists, often in a matter of months.</p>

	<p><em>[...]</em></p>
	HTML;




$page->parent('global_issues.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_Gen_McChrystals_Speech_on_Afghanistan);
$page->body($div_Counterinsurgency_Math_Revisited);
$page->body($div_Opinion_There_s_a_smarter_way_to_eliminate_Hamas);
