<?php
$page = new Page();
$page->h1("Homeboy Industries");
$page->keywords("Homeboy Industries");
$page->tags("Organisation");
$page->stars(1);

//$page->snp("description", "");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p>Non-profit organization based in Los Angeles, California,
	that provides training and employment opportunities to formerly incarcerated individuals and gang members,
	empowering them to transform their lives and become valuable contributors to society.</p>
	HTML );

$r1 = $page->ref('https://www.whitehouse.gov/briefing-room/statements-releases/2024/05/03/president-biden-announces-recipients-of-the-presidential-medal-of-freedom-2/',
                 'President Biden Announces Recipients of the Presidential Medal of Freedom');
$r2 = $page->ref('https://www.jesuitswest.org/stories/fr-greg-boyle-sj-receives-presidential-medal-of-freedom/',
                 'Fr. Greg Boyle, SJ, Receives Presidential Medal of Freedom');
$r3 = $page->ref('https://angelusnews.com/local/la-catholics/homeboy-industries-receives-humanitarian-prize/',
                 'Homeboy Industries receives humanitarian prize');
$r3 = $page->ref('https://homeboyindustries.org/our-story/about-homeboy/',
                 'Homeboy Industries: About Us');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p><strong>Homeboy Industries</strong> is a non-profit organization based in Los Angeles, California,
	that provides training and employment opportunities to formerly incarcerated individuals and gang members,
	empowering them to transform their lives and become valuable contributors to society.</p>
	HTML;


$div_Mission = new ContentSection();
$div_Mission->content = <<<HTML
	<h3>Mission</h3>

	<p>Homeboy Industries' mission is to provide formerly gang-involved and formerly incarcerated individuals
	with the tools, training, and support they need to lead productive and fulfilling lives.</p>
	HTML;


$div_Services = new ContentSection();
$div_Services->content = <<<HTML
	<h3>Services</h3>

	<p>Homeboy Industries offers a wide range of services, including:</p>

	<ul>
		<li><strong>Job training</strong>: Culinary arts, baking, silkscreen printing, tattoo removal, landscaping, and more.</li>
		<li><strong>Employment placement</strong>: Assistance in finding jobs and building a career.</li>
		<li><strong>Mentoring and support services</strong>: Counseling, case management, and life skills training.</li>
		<li><strong>Community outreach</strong>: Gang intervention programs, youth outreach, and community events.</li>
		<li><strong>Social enterprises</strong>: Bakery, café, tattoo removal clinic, and other businesses that provide employment opportunities.</li>
	</ul>
	HTML;


$div_Impact = new ContentSection();
$div_Impact->content = <<<HTML
	<h3>Impact</h3>

	<p>Homeboy Industries has a significant impact on the lives of formerly incarcerated individuals and gang members, helping them:</p>

	<ul>
		<li><strong>Reduce recidivism rates</strong>: By providing job training and support, Homeboy Industries helps individuals avoid returning to prison.</li>
		<li><strong>Rebuild their lives</strong>: The organization assists individuals in finding stable housing, securing employment, and reintegrating into society.</li>
		<li><strong>Break the cycle of violence</strong>:  Homeboy Industries' programs promote peace and reconciliation within communities affected by gang violence.</li>
		<li><strong>Create positive change</strong>: The organization's social enterprises provide economic opportunities and create a positive impact on the community.</li>
	</ul>
	HTML;


$div_Founder = new ContentSection();
$div_Founder->content = <<<HTML
	<h3>Founder</h3>

	<p>Homeboy Industries was founded by Father ${'Gregory Boyle'}, a Jesuit priest
	who has dedicated his life to serving the most marginalized communities in Los Angeles.</p>
	HTML;



$div_Recognition = new ContentSection();
$div_Recognition->content = <<<HTML
	<h3>Recognition</h3>

	<p>Homeboy Industries has been recognized for its innovative approach to social justice and has received numerous awards, including:</p>

	<ul>
	<li>Presidential Medal of Freedom (2024)$r1 $r2</li>
	<li>Conrad N. Hilton Humanitarian Prize (2020) $r3</li>
	</ul>
	HTML;




$div_Homeboy_Industries_website = new WebsiteContentSection();
$div_Homeboy_Industries_website->setTitleText("Homeboy Industries website ");
$div_Homeboy_Industries_website->setTitleLink("https://homeboyindustries.org/");
$div_Homeboy_Industries_website->content = <<<HTML
	<p>What began in 1988 as a way of improving the lives of former gang members in East Los Angeles
	has evolved into the largest gang intervention, rehab and re-entry program in the world.</p>

	<p>Each year we welcome thousands of people through our doors seeking to transform their lives.
	Whether joining our 18-month employment and re-entry program or seeking discrete services such as tattoo removal or substance abuse resources,
	our clients are embraced by a community of kinship and offered a variety of free wraparound services to facilitate healing and growth.
	In addition to serving almost 7,000 members of the immediate Los Angeles community in 2018,
	our flagship 18-month employment and re-entry program was offered to over 400 men and women.</p>
	HTML;




$div_wikipedia_Homeboy_Industries = new WikipediaContentSection();
$div_wikipedia_Homeboy_Industries->setTitleText("Homeboy Industries");
$div_wikipedia_Homeboy_Industries->setTitleLink("https://en.wikipedia.org/wiki/Homeboy_Industries");
$div_wikipedia_Homeboy_Industries->content = <<<HTML
	<p>Homeboy Industries is a youth program founded in 1992 by Father Greg Boyle
	following the work of the Christian base communities at Dolores Mission Church in Boyle Heights, Los Angeles.
	The program is intended to assist high-risk youth, former gang members and the recently incarcerated with a variety of free programs,
	such as mental health counseling, legal services, tattoo removal, curriculum and education classes, work-readiness training, and employment services.</p>
	HTML;


$page->parent('list_of_organisations.html');

$page->body($div_introduction);
$page->body($div_Mission);
$page->body($div_Services);
$page->body($div_Impact);
$page->body($div_Founder);
$page->body($div_Recognition);


$page->body('greg_boyle.html');
$page->body($div_Homeboy_Industries_website);
$page->body($div_wikipedia_Homeboy_Industries);
