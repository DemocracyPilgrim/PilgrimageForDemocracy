<?php
$page = new Page();
$page->h1("President of the United States");
$page->tags("Institutions: Executive");
$page->keywords("President of the United States", "POTUS");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_President_of_the_United_States = new WikipediaContentSection();
$div_wikipedia_President_of_the_United_States->setTitleText("President of the United States");
$div_wikipedia_President_of_the_United_States->setTitleLink("https://en.wikipedia.org/wiki/President_of_the_United_States");
$div_wikipedia_President_of_the_United_States->content = <<<HTML
	<p>The president of the United States (POTUS) is the head of state and head of government of the United States of America. The president directs the executive branch of the federal government and is the commander-in-chief of the United States Armed Forces.</p>
	HTML;


$page->parent('institutions.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag(array("President of the United States", "POTUS"));
$page->body($div_wikipedia_President_of_the_United_States);
