<?php
$page = new Page();
$page->h1("Nelson Mandela");
$page->alpha_sort("Mandela, Nelson");
$page->tags("Person");
$page->keywords("Nelson Mandela", "Mandela");
$page->stars(0);

$page->snp("description", "South African anti-apartheid activist, first president of South Africa.");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref('', '');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Nelson Mandela was a South African anti-apartheid activist, first president of ${'South Africa'}.</p>

	<p>He collaborated with ${'Richard Stengel'} to write his biography: "${'Long Walk to Freedom'}".</p>
	HTML;

$div_wikipedia_Nelson_Mandela = new WikipediaContentSection();
$div_wikipedia_Nelson_Mandela->setTitleText("Nelson Mandela");
$div_wikipedia_Nelson_Mandela->setTitleLink("https://en.wikipedia.org/wiki/Nelson_Mandela");
$div_wikipedia_Nelson_Mandela->content = <<<HTML
	<p>Nelson Rolihlahla Mandela (18 July 1918 – 5 December 2013) was a South African anti-apartheid activist, politician,
	and statesman who served as the first president of South Africa from 1994 to 1999.
	He was the country's first black head of state and the first elected in a fully representative democratic election.
	His government focused on dismantling the legacy of apartheid by fostering racial reconciliation.
	Ideologically an African nationalist and socialist,
	he served as the president of the African National Congress (ANC) party from 1991 to 1997.</p>
	HTML;


$page->parent('list_of_people.html');
$page->parent('south_africa.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_wikipedia_Nelson_Mandela);
