<?php
$page = new Page();
$page->h1("Rated Voting (class of voting methods)");
$page->tags("Elections", "Voting Methods");
$page->keywords("Rated Voting");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Rated_voting = new WikipediaContentSection();
$div_wikipedia_Rated_voting->setTitleText("Rated voting");
$div_wikipedia_Rated_voting->setTitleLink("https://en.wikipedia.org/wiki/Rated_voting");
$div_wikipedia_Rated_voting->content = <<<HTML
	<p>Rated, evaluative, graded, or cardinal voting rules are a class of voting methods that allow voters to state how strongly they support a candidate, by giving each one a grade on a separate scale.</p>
	HTML;


$page->parent('voting_methods.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Rated Voting");
$page->body($div_wikipedia_Rated_voting);
