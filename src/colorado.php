<?php
$page = new Page();
$page->h1("Colorado");
$page->tags("International", "USA", "US State");
$page->keywords("Colorado");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Issues related to democracy and social justice in the US state of Colorado.</p>
	HTML;

$div_wikipedia_Colorado = new WikipediaContentSection();
$div_wikipedia_Colorado->setTitleText("Colorado");
$div_wikipedia_Colorado->setTitleLink("https://en.wikipedia.org/wiki/Colorado");
$div_wikipedia_Colorado->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Government_of_Colorado = new WikipediaContentSection();
$div_wikipedia_Government_of_Colorado->setTitleText("Government of Colorado");
$div_wikipedia_Government_of_Colorado->setTitleLink("https://en.wikipedia.org/wiki/Government_of_Colorado");
$div_wikipedia_Government_of_Colorado->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Politics_of_Colorado = new WikipediaContentSection();
$div_wikipedia_Politics_of_Colorado->setTitleText("Politics of Colorado");
$div_wikipedia_Politics_of_Colorado->setTitleLink("https://en.wikipedia.org/wiki/Politics_of_Colorado");
$div_wikipedia_Politics_of_Colorado->content = <<<HTML
	<p>
	</p>
	HTML;


$page->parent('us_states.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Colorado");
$page->body($div_wikipedia_Colorado);
$page->body($div_wikipedia_Government_of_Colorado);
$page->body($div_wikipedia_Politics_of_Colorado);
