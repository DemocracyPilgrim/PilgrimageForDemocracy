<?php
$page = new Page();
$page->h1("Community of Democracies");
$page->keywords("Community of Democracies");
$page->tags("Organisation");
$page->stars(0);

//$page->snp("description", "");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;



$div_community_of_democracies_website = new WebsiteContentSection();
$div_community_of_democracies_website->setTitleText("community of democracies website ");
$div_community_of_democracies_website->setTitleLink("https://community-democracies.org/");
$div_community_of_democracies_website->content = <<<HTML
	<p>The Community of Democracies (CoD) is a global intergovernmental coalition
	comprised of the Governing Council Member States
	that support adherence to common democratic values and standards outlined in the Warsaw Declaration.</p>
	HTML;



$div_wikipedia_Community_of_Democracies = new WikipediaContentSection();
$div_wikipedia_Community_of_Democracies->setTitleText("Community of Democracies");
$div_wikipedia_Community_of_Democracies->setTitleLink("https://en.wikipedia.org/wiki/Community_of_Democracies");
$div_wikipedia_Community_of_Democracies->content = <<<HTML
	<p>The Community of Democracies (C.O.D), established in 2000, is an intergovernmental coalition of states.
	Its aim is to bring together governments, civil society and the private sector in the pursuit of the common goal of supporting democratic rules,
	expanding political participation, advancing and protecting democratic freedoms, and strengthening democratic norms and institutions around the world.</p>
	HTML;


$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_community_of_democracies_website);
$page->body($div_wikipedia_Community_of_Democracies);
