<?php
$page = new Page();
$page->h1("The Big Truth: Upholding Democracy in the Age of “The Big Lie”");
$page->tags("Book", "Elections", "Election Integrity", "Election Denialism", "USA");
$page->keywords("The Big Truth: Upholding Democracy in the Age of “The Big Lie”");
$page->stars(1);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>The Big Truth: Upholding Democracy in the Age of “The Big Lie” is a book by Major Garrett and ${'David Becker'}.</p>
	HTML;



$div_The_Big_Truth_Upholding_Democracy_in_the_Age_of_The_Big_Lie = new WebsiteContentSection();
$div_The_Big_Truth_Upholding_Democracy_in_the_Age_of_The_Big_Lie->setTitleText("The Big Truth: Upholding Democracy in the Age of “The Big Lie” ");
$div_The_Big_Truth_Upholding_Democracy_in_the_Age_of_The_Big_Lie->setTitleLink("https://thebigtruthbook.com/");
$div_The_Big_Truth_Upholding_Democracy_in_the_Age_of_The_Big_Lie->content = <<<HTML
	<p>When a seven million vote margin of victory and 306 electoral votes pointed to Joe Biden as the victor in the 2020 Presidential Election,
	Trump-fueled conspiracy theories – or the Big Lie – exploded.</p>

	<p>In “The Big Truth,” Garrett and Becker completely upend Trump’s Big Lie that has infected the Far Right
	by revealing concrete evidence proving that 2020 may have been the most secure US election ever.
	Profiles of and interviews with key swing state election officials, like Georgia Secretary of State Brad Raffensperger
	and Thomas Freitag, Bucks County, Pennsylvania, election director,
	show that these guardians of democracy ran fair and accurate elections.</p>

	<p>Garrett and Becker go beyond analyzing the key players in all six swing states,
	the pre-election litigation that clarified the rules for both parties, and the fully auditable and verifiable ballots used.
	They lay bare the methods being undertaken right now to undercut faith, belief, and effectiveness of elections.
	In addition, they identify the places where the most dire actions have been taken since 2020 – where election laws have been changed;
	or election officials been hounded out of office, retired out of frustration/fatigue, or been replaced by pro-Trump political figures.</p>

	<p>Therein lies The Big Truth we face in the 2024 election and beyond – when honest election workers of all parties will face lies, threats, abuse,
	and vilification, and the manipulation of voting laws and election boards threaten the sanctity of our democracy.</p>
	HTML;



$div_youtube_The_Big_Truth_Upholding_Democracy_in_the_Age_of_The_Big_Lie_Major_Garrett_and_David_Becker = new YoutubeContentSection();
$div_youtube_The_Big_Truth_Upholding_Democracy_in_the_Age_of_The_Big_Lie_Major_Garrett_and_David_Becker->setTitleText("The Big Truth: Upholding Democracy in the Age of “The Big Lie” - Major Garrett and David Becker");
$div_youtube_The_Big_Truth_Upholding_Democracy_in_the_Age_of_The_Big_Lie_Major_Garrett_and_David_Becker->setTitleLink("https://www.youtube.com/watch?v=zhu16B02YM0");
$div_youtube_The_Big_Truth_Upholding_Democracy_in_the_Age_of_The_Big_Lie_Major_Garrett_and_David_Becker->content = <<<HTML
	<p>Just about a month before the midterms, Major Garrett and David Becker explore the threats to American democracy,
	the validity of the 2020 election, the new pressures being placed on the voting system and ways in which voting can be improved.
	These are the issues the two expertly examine in their new book The Big Truth: Upholding Democracy in the Age of “The Big Lie”.
	The event, moderated by CBS’ chief political analyst, John Dickerson, will cover these timely topics,
	their thoughts on the current political climate, and their hopes for voter enfranchisement.</p>
	HTML;



$page->parent('list_of_books.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_The_Big_Truth_Upholding_Democracy_in_the_Age_of_The_Big_Lie);
$page->body($div_youtube_The_Big_Truth_Upholding_Democracy_in_the_Age_of_The_Big_Lie_Major_Garrett_and_David_Becker);
