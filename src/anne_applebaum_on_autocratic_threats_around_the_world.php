<?php
$page = new VideoPage();
$page->h1("Anne Applebaum on Autocratic Threats Around the World");
$page->tags("Video: Podcast", "Anne Applebaum", "Podcast: We the People", "Donald Trump", "Autocracy", "Social Networks", "Taxes", "Advertising");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;



$div_Anne_Applebaum_on_Autocratic_Threats_Around_the_World = new WebsiteContentSection();
$div_Anne_Applebaum_on_Autocratic_Threats_Around_the_World->setTitleText("Anne Applebaum on Autocratic Threats Around the World");
$div_Anne_Applebaum_on_Autocratic_Threats_Around_the_World->setTitleLink("https://constitutioncenter.org/news-debate/podcasts/anne-applebaum-on-autocratic-threats-around-the-world");
$div_Anne_Applebaum_on_Autocratic_Threats_Around_the_World->content = <<<HTML
	<p>In this episode, Anne Applebaum, Pulitzer Prize-winning historian and staff writer for The Atlantic, joins to discuss her newest book, Autocracy, Inc.: The Dictators Who Want to Rule the World, which explores how autocracies work together to undermine the democratic world, and how democracies should organize to defeat them. She joins Jeffrey Rosen to discuss new threats from autocratic leaders at home and around the world and how liberal democracies should fight these threats.</p>
	HTML;




$div_Quotes = new ContentSection();
$div_Quotes->content = <<<HTML
	<h3>Quotes</h3>

	<blockquote>You could also talk about regulating the internet, by which I don't mean censoring it,
	but I mean reducing the attraction for advertisers
	and for people seeking to make money online from outrageous language and extremist publications and statements.
	Whereas right now, the more outrageous you are, the more polarizing you are, the more followers you get,
	and then the more advertising you get, and it's possible to unpick that.
	For example, we could think harder about how to do that.
	We could give people access to algorithms or some choice about what it is they see and how they see it.
	We could give people control over their own data, and there are many ways in which you could think of reforming the internet
	that made it less susceptible to information more essentially.
	</blockquote>
	HTML;


$page->parent('list_of_videos.html');
$page->template("stub");
$page->body($div_introduction);

$page->body($div_Anne_Applebaum_on_Autocratic_Threats_Around_the_World);
$page->body($div_Quotes);
