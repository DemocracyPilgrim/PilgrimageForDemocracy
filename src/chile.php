<?php
$page = new CountryPage('Chile');
$page->h1('Chile');
$page->tags("Country");
$page->keywords('Chile');
$page->stars(0);

$page->snp('description', '18,5 million inhabitants.');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Chile = new WikipediaContentSection();
$div_wikipedia_Chile->setTitleText('Chile');
$div_wikipedia_Chile->setTitleLink('https://en.wikipedia.org/wiki/Chile');
$div_wikipedia_Chile->content = <<<HTML
	<p>Chile has a high-income economy and is one of the most economically and socially stable nations in South America,
	leading Latin America in competitiveness, per capita income, globalization, peace, and economic freedom.
	Chile also performs well in the region in terms of sustainability of the state and democratic development,
	and boasts the second lowest homicide rate in the Americas, following only Canada.</p>
	HTML;

$div_wikipedia_Politics_of_Chile = new WikipediaContentSection();
$div_wikipedia_Politics_of_Chile->setTitleText('Politics of Chile');
$div_wikipedia_Politics_of_Chile->setTitleLink('https://en.wikipedia.org/wiki/Politics_of_Chile');
$div_wikipedia_Politics_of_Chile->content = <<<HTML
	<p>Chile's government is a representative democratic republic,
	whereby the President of Chile is both head of state and head of government, and of a formal multi-party system.
	Executive power is exercised by the president and by their cabinet.
	Legislative power is vested in both the government and the two chambers of the National Congress.
	The judiciary is independent of the executive and the legislature of Chile.</p>
	HTML;

$div_wikipedia_Law_of_Chile = new WikipediaContentSection();
$div_wikipedia_Law_of_Chile->setTitleText('Law of Chile');
$div_wikipedia_Law_of_Chile->setTitleLink('https://en.wikipedia.org/wiki/Law_of_Chile');
$div_wikipedia_Law_of_Chile->content = <<<HTML
	<p>The legal system of Chile belongs to the Continental Law tradition.
	The basis for its public law is the 1980 Constitution, reformed in 1989 and 2005.
	According to it Chile is a democratic republic.
	There is a clear separation of functions, between the President of the Republic, the Congress, the judiciary and a Constitutional Court.</p>
	HTML;


$page->parent('world.html');
$page->template("stub");
$page->body($div_introduction);
$page->body('Country indices');

$page->body($div_wikipedia_Chile);
$page->body($div_wikipedia_Politics_of_Chile);
$page->body($div_wikipedia_Law_of_Chile);
