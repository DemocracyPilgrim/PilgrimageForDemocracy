<?php
$page = new Page();
$page->h1('Duverger example: USA, 21st century');
$page->tags("USA", "Donald Trump", "Elections: Duverger Syndrome");
$page->stars(0);

$page->snp('description', 'Where American democracy comes agonizingly close to its breaking point.');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p>American politics in the 21st century, and the rise of Trumpism, the MAGA movement, and post-truth politics
	offer a frightening example of all the symptoms of the Duverger Syndrome combined,
	pushing American democracy agonizingly close to its breaking point.</p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>American politics in the 21st century, and the rise of Trumpism, the MAGA movement, and post-truth politics
	offer a frightening example of all the symptoms of the ${'Duverger Syndrome'} combined,
	pushing $American $democracy agonizingly close to its breaking point.</p>

	<p>With the rise of Donald $Trump and $Trumpism (the $MAGA movement) since 2015,
	America is in peril of becoming, should Trump win the 2024 elections, a full-blown fascist state.</p>

	<p>It is critical to understand that Trump's rise to prominence in US politics and his accession to power in 2016
	would not have been possible without ${"Duverger's Law"}.
	If, instead of ${'plurality voting'}, a better ${'voting method'} had been used in American elections,
	Donald J. Trump's candidacy would have never been electorally viable and Trump would have never come anywhere close to power.</p>

	<p>There are two groups of people who caused Trump to be one of the two top candidates in the 2016, 2020 and 2024 presidential elections:
	first and foremost are the voters themselves, and secondly the enablers: Republican party insiders and right wind media opportunists.
	It is certain that in both categories, a majority of people actually disliked Trump.
	Many voted for Trump while holding their noses, because they identified too much with the Republican conservative brand
	and couldn't bring themselves to vote for the Democratic ticket.
	And top Republican party leaders, who had publicly repudiated Trump on numerous occasions,
	swore fealty to Trump once it was obvious he was their only path to power.</p>

	<p>Party politics as we know it is a known consequence of Duverger's Law,
	and $Trumpism is yet another symptom of the Duverger Syndrome.
	With a better ${'voting method'}, party politics would be very different than what it is today,
	and voters would have had different avenues to express their opinions, their values and political priorities,
	without having to fall back on such a flawed and morally corrupt individual.</p>

	<p>While we hope that US democracy will survive this high fever,
	and that America will come back from the brink of fascism,
	we must view what is currently happening in the USA as a cautionary tale for each and every one of the $world's democracies.
	The causes of what is happening are known. The solutions are known.
	It is up to each and every one of us to demand that our democratic institutions be upgraded.</p>
	HTML;


$list = new ListOfPages();
$list->add('donald_trump.html');
$list->add('american_fascism.html');
$list->add('why_are_trumpism_and_the_maga_movement_so_successful.html');
$list->add('frontline_short_docs_democracy_on_trial.html');
$print_list = $list->print();

$div_list = new ContentSection();
$div_list->content = <<<HTML
	<h3>Related content</h3>

	$print_list
	HTML;



$div_wikipedia_Political_parties_in_the_United_States = new WikipediaContentSection();
$div_wikipedia_Political_parties_in_the_United_States->setTitleText("Political parties in the United States #Seventh Party System (2016?–present)");
$div_wikipedia_Political_parties_in_the_United_States->setTitleLink("https://en.wikipedia.org/wiki/Political_parties_in_the_United_States#Seventh_Party_System_(2016?–present)");
$div_wikipedia_Political_parties_in_the_United_States->content = <<<HTML
	<p>While there is no consensus that a Seventh Party System has begun,
	many have noted unique features of a political era starting with the 2016 presidential campaign of Donald Trump.</p>
	HTML;




$page->parent('duverger_syndrome.html');
$page->template("stub");
$page->body($div_introduction);

$page->body('american_fascism.html');

$page->body($div_list);


$page->body($div_wikipedia_Political_parties_in_the_United_States);
