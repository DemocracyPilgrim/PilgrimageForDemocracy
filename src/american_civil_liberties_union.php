<?php
$page = new Page();
$page->h1("American Civil Liberties Union (ACLU)");
$page->tags("Organisation", "USA", "Individual: Liberties", "Society", "Humanity", "Living");
$page->keywords("American Civil Liberties Union", "ACLU");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

$r1 = $page->ref("https://www.aclu.org/about/aclu-history#introduction", "ACLU History");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;



$div_American_Civil_Liberties_Union = new WebsiteContentSection();
$div_American_Civil_Liberties_Union->setTitleText("American Civil Liberties Union");
$div_American_Civil_Liberties_Union->setTitleLink("https://www.aclu.org/");
$div_American_Civil_Liberties_Union->content = <<<HTML
	<p>The ACLU has evolved in the years since from this small group of idealists into the nation’s premier defender of the rights enshrined in the U.S. Constitution.
	With more than 1.7 million members, 500 staff attorneys, thousands of volunteer attorneys, and offices throughout the nation,
	the ACLU of today continues to fight government abuse and to vigorously defend individual freedoms including speech and religion, a woman’s right to choose,
	the right to due process, citizens’ rights to privacy and much more.
	The ACLU stands up for these rights even when the cause is unpopular, and sometimes when nobody else will.
	While not always in agreement with us on every issue, Americans have come to count on the ACLU for its unyielding dedication to principle.
	The ACLU has become so ingrained in American society that it is hard to imagine an America without it. $r1</p>
	HTML;



$div_wikipedia_American_Civil_Liberties_Union = new WikipediaContentSection();
$div_wikipedia_American_Civil_Liberties_Union->setTitleText("American Civil Liberties Union");
$div_wikipedia_American_Civil_Liberties_Union->setTitleLink("https://en.wikipedia.org/wiki/American_Civil_Liberties_Union");
$div_wikipedia_American_Civil_Liberties_Union->content = <<<HTML
	<p>The American Civil Liberties Union (ACLU) is an American nonprofit civil rights organization founded in 1920.
	ACLU affiliates are active in all 50 states, Washington, D.C., and Puerto Rico.
	The ACLU provides legal assistance in cases where it considers civil liberties at risk.
	Legal support from the ACLU can take the form of direct legal representation or preparation of amicus curiae briefs
	expressing legal arguments when another law firm is already providing representation.</p>

	<p>In addition to representing persons and organizations in lawsuits, the ACLU lobbies for policy positions established by its board of directors.
	The ACLU's current positions include opposing the death penalty;
	supporting same-sex marriage and the right of LGBT people to adopt;
	supporting reproductive rights such as birth control and abortion rights;
	eliminating discrimination against women, minorities, and LGBT people;
	decarceration in the United States;
	protecting housing and employment rights of veterans;
	reforming sex offender registries and protecting housing and employment rights of convicted first-time offenders;
	supporting the rights of prisoners and opposing torture;
	upholding the separation of church and state by opposing government preference for religion over non-religion or for particular faiths over others;
	and supporting the legality of gender-affirming treatments, including those that are government funded, for trans youth.</p>
	HTML;


$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_American_Civil_Liberties_Union);
$page->body($div_wikipedia_American_Civil_Liberties_Union);
