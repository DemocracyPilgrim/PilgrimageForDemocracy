<?php
$page = new Page();
$page->h1('List of books');
$page->stars(0);
$page->keywords('books', 'book', 'Book');
$page->viewport_background('/free/list_of_books.png');

$page->snp('description', "Books about democracy, social justice, history, global issues...");
$page->snp('image', '/free/list_of_books.1200-630.png');


$page->preview( <<<HTML
	<p>
	</p>
	HTML );


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Books about democracy, social justice, history, global issues...</p>
	HTML;

$list_Books = ListOfPages::WithTags("Book");
$print_list_Books = $list_Books->print();

$div_list = new ContentSection();
$div_list->content = <<<HTML
	<h3>List</h3>

	$print_list_Books
	HTML;

$page->parent('lists.html');

$page->body($div_introduction);
$page->body($div_list);

// Featured
$page->body('the_remains_of_the_day.html');
