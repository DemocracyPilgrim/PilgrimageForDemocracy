<?php
$page = new Page();
$page->h1("4: Fourth level of democracy — We, the People");
$page->stars(1);
$page->keywords('fourth level of democracy', 'fourth level');
$page->viewport_background('/free/fourth_level_of_democracy.png');

$page->snp("description", "Giving the Power back to the People.");
$page->snp("image",       "/free/fourth_level_of_democracy.1200-630.png");

$page->preview( <<<HTML
	<p>After the institutions, the true power rightfully returns to the people.
	This level examines how citizens can meaningfully shape their government through informed participation, elections, and effective voting methods.</p>
	HTML );



$div_qualities = new ContentSection();
$div_qualities->content = <<<HTML
	<p>The flaws of the fourth level of democracy are:
		<span class="democracy flaw">impose ideas</span>,
		<span class="democracy flaw">apathy</span>,
		<span class="democracy flaw">cynicism</span>,
		<span class="democracy flaw">indifference</span>.</p>
	<p>The virtues of the fourth level of democracy are:
		<span class="democracy virtue">contribute ideas</span>,
		<span class="democracy virtue">participation</span>,
		<span class="democracy virtue">hope</span>,
		<span class="democracy virtue">support</span>.</p>
	HTML;



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>When discussing the ${'third level of democracy'}, we strayed away from the concept of the power to the people.
	We spoke about institutions and institutional powers, about government, policing, laws and justice,
	all of which is somewhat imposed upon the citizens, limiting to a certain extent their personal $freedom.
	Now, we must make sure to give the power back to the People,
	so that we can come full circle back to the ${'first level of democracy'}.</p>

	<p>Often, people equate the notion of $democracy with that of voting once every four years,
	or whatever amount of time an elected term might be in any given country.
	"Vote then forget" is a very reductive view of democracy.
	It remains true, however, that voting in elections is an extremely critical part of democratic life,
	Elections are a corner stone of democratic $institutions.
	They are so important that we did not cover them in the ${'third level of democracy'} which covers institutions in general,
	just so that we can cover them in more details in the present article:
	they form their own level of democracy.</p>
	HTML;


$div_the_People_s_choice = new ContentSection();
$div_the_People_s_choice->content = <<<HTML
	<h3>The People's choice</h3>

	<p>Elections are when the People can gain back the power that they seemingly lost in the second and third levels.</p>

	<p>Electoral results are supposed to reflect the People's true choice.
	However, as we shall see, matters are not so simple.</p>
	HTML;


$div_Duverger_syndrome = new ContentSection();
$div_Duverger_syndrome->content = <<<HTML
	<h3>Duverger syndrome</h3>

	<p>In all of the $world's established $democracies, there is a common, fundamental flaw
	that critically undermine the democratic ideals they profess to pursue.</p>

	<p>If we are to improve our democracies in meaningful ways, we absolutely need to understand what the ${'Duverger Syndrome'} is,
	and how to remedy it.</p>
	HTML;


$div_voting_methods = new ContentSection();
$div_voting_methods->content = <<<HTML
	<h3>Voting methods</h3>

	<p>One of the worst possible ${'voting method'} is used throughout the $world, in almost every $democracies.
	We must understand how that voting method is the root cause of the ${'Duverger Syndrome'}.
	Then we can explore alternative voting methods which would dramatically strengthen our democratic institutions.</p>
	HTML;


$div_Election_integrity = new ContentSection();
$div_Election_integrity->content = <<<HTML
	<h3>Election integrity</h3>

	<p>In this age of election denialism, it is important to pursue every means possible to boost confidence in ${'election integrity'}.</p>
	HTML;


$h2_We_the_People = new h2HeaderContent("We, the People");


$list_all_related_topics = new ListOfPages();
$list_all_related_topics->add('elections.html');
$print_list_all_related_topics = $list_all_related_topics->print();

$div_list_all_related_topics = new ContentSection();
$div_list_all_related_topics->content = <<<HTML
	$print_list_all_related_topics
	HTML;



$page->parent('democracy.html');
$page->previous('third_level_of_democracy.html');
$page->next('fifth_level_of_democracy.html');

$page->template("stub");


$page->body($div_introduction);


$page->body($div_Duverger_syndrome);
$page->body($div_voting_methods);
$page->body($div_Election_integrity);

$page->body($h2_We_the_People);
$page->body($div_the_People_s_choice);

$page->body($div_qualities);
$page->body($div_list_all_related_topics);
