<?php
$page = new Page();
$page->h1('Duverger example: Taiwan 2024 presidential election');
$page->stars(1);
$page->tags("Elections: Duverger Syndrome", "Taiwan");

//$page->snp('description', '');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>The presidential election in the ${'Republic of China'} ($Taiwan) are scheduled to take place in January 2023.</p>

	<p>Since the lifting of martial law in the ROC and the beginning of the democratic era,
	Taiwan politics have been dominated by two large camps, a typical $Duverger symptom:
	the Green Camp, with the Democratic Progressive Party (DPP),
	and the Blue Camp, with the Kuomingtang (KMT).
	Since 1996, when Taiwanese citizens were first allowed to vote directly for the president,
	the presidency has been held alternatively by the KMT and the DPP.</p>

	<p>At the beginning of the 2024 presidential race, there were four announced serious contenders,
	a DPP candidate, a KMT candidate, a Taiwan People's Party (TPP) candidate, and an independent candidate,
	with polling result showing a very scattered electorate,
	no candidate having any overwhelming lead.</p>

	<p>President Tsai Ying-Wen from the DPP had been in office since 2016 and was term limited.
	Her vice-president, Lai, was designated to succeed her.</p>

	<p>The DPP having been in power for eight years, they have been portrayed as the establishment party.
	The DPP is strongly pro-democratic and against the influence of the Chinese Communist Party
	and the authoritarian threat it represented.
	On the other hand, the KMT was adopting a much more conciliatory attitude towards $China,
	wanting to build more ties with China and distance Taiwan from the $USA.</p>

	<p>TPP candidate Ko Wen-je, although historically very close to the DPP, had distanced himself a lot from the DPP,
	and wanted to incarnate an independent, centrist party.</p>

	<p>Terry Gou, without any party support, never had any chance in the presidential race,
	but garnered enough support to spoil the chances of other candidates, drawing enough votes from them.</p>

	<p>In an election using plurality voting, with its obvious ${'Duverger symptoms'},
	four candidates with support spread enough that there was no unavoidable front runner,
	it was obvious that there was one or two candidates too many, splitting the vote
	and giving Lai a good chance of winning the presidency while falling far short of a majority of the votes.</p>
	HTML;


$h2_The_candidates = new h2HeaderContent('The candidates');

$div_KMT_candidate = new ContentSection();
$div_KMT_candidate->content = <<<HTML
	<h3>KMT candidate: Hou You-yi (侯友宜)</h3>

	<p>The KMT is the old nationalist party that has ruled the Republic of China since its inception in mainland China in 1911,
	to the year 2000 when for the first time in Taiwan' history, there was a peaceful transfer of power from one party (the KMT)
	to another (the DPP).</p>

	<p>KMT candidate Hou You-yi represents a pro-China, anti-democratic stance in Taiwan politics.</p>
	HTML;

$div_DPP_candidate = new ContentSection();
$div_DPP_candidate->content = <<<HTML
	<h3>DPP candidate: William Lai (賴清德)</h3>

	<p>The Democratic Progressive Party was born out of the struggle for democracy in the 1980s,
	and against the KMT one-party rule.
	The DPP won the presidency in 2000 and 2004 (Chen Shui-bian)
	and again in 2016 and 2020 (Tsai Ying-wen).</p>

	<p>William Lai is Tsai Ying-wen's vice-president and is running to succeed her
	and continue her pro-democracy policies and on military preparedness against a communist Chinese military aggression.</p>
	HTML;

$div_TPP_candidate = new ContentSection();
$div_TPP_candidate->content = <<<HTML
	<h3>TPP candidate: Ko Wen-je (柯文哲)</h3>

	<p>Ko Wen-je was an independent who founded his own party: The Taiwan People's Party (TPP).
	He claimed to represent a choice that superseded the binary division of the DPP and the KMT.</p>
	HTML;


$h2_Duverger_Law_at_play = new h2HeaderContent("Duverger's Law at play");


$div_the_polls = new ContentSection();
$div_the_polls->content = <<<HTML
	<h3>The polls</h3>

	<p>At the beginning of the 2024 presidential race, there were four announced serious contenders,
	a DPP candidate, a KMT candidate, a Taiwan People's Party (TPP) candidate, and an independent candidate,
	with polling results fairly constant over many months:</p>
	<ul>
		<li><strong>William Lai (DPP)</strong> (over 30%)</li>
		<li><strong>Hou Yu-ih (KMT)</strong> (over 20%)</li>
		<li><strong>Ko Wen-je (TPP)</strong> (over 20%)</li>
		<li><strong>Terry Gou (Independent)</strong> (less than 10%)</li>
		<li>Undecided voters: around 20%</li>
	</ul>
	HTML;

$div_the_deal = new ContentSection();
$div_the_deal->content = <<<HTML
	<h3>The deal</h3>

	<p>DPP candidate William Lai was consistently leading in the polls.
	Well in accordance with ${"Duverger's Law"},
	the trailing candidates saw that their only path to victory was through alliance.
	Thus, Hou and Ko started courting each other,
	with both candidates ambitioning to be at the top of the ticket, having the other one as his vice-presidential running mate.
	On the 15th November 2023, 10 days before the official deadline to submit candidacies,
	Hou and Ko announced that they had reached a deal on forming a joined ticket,
	using polling results to decide who would be on top.</p>
	HTML;


$div_wikipedia_2024_Taiwanese_presidential_election = new WikipediaContentSection();
$div_wikipedia_2024_Taiwanese_presidential_election->setTitleText('2024 Taiwanese presidential election');
$div_wikipedia_2024_Taiwanese_presidential_election->setTitleLink('https://en.wikipedia.org/wiki/2024_Taiwanese_presidential_election');
$div_wikipedia_2024_Taiwanese_presidential_election->content = <<<HTML
	<p>Presidential elections are scheduled to be held in the Republic of China (Taiwan) on 13 January 2024.
	Incumbent President Tsai Ing-wen of the Democratic Progressive Party (DPP), who was reelected in 2020, is ineligible to seek a third term.
	The ruling DPP nominated Vice President Lai Ching-te in March 2023, having already secured the party chairmanship by acclamation.
	New Taipei Mayor Hou Yu-ih of the opposition Kuomintang (KMT) was selected to be the party’s presidential nominee in May 2023.
	Despite previously saying he would support Hou’s nomination, businessman Terry Gou declared his own independent bid in September 2023.
	The Taiwan People’s Party (TPP) has nominated its leader, Ko Wen-je, the former Mayor of Taipei.</p>
	HTML;



$page->parent('duverger_syndrome.html');
$page->template("stub");
$page->body($div_introduction);

$page->body($h2_The_candidates);
$page->body($div_KMT_candidate);
$page->body($div_DPP_candidate);
$page->body($div_TPP_candidate);

$page->body($h2_Duverger_Law_at_play);
$page->body($div_the_polls);
$page->body($div_the_deal);

$page->body($div_wikipedia_2024_Taiwanese_presidential_election);
