<?php
$page = new Page();
$page->h1("CourtListener");
$page->keywords("CourtListener");
$page->tags("Institutions: Judiciary");
$page->stars(0);

$page->snp("description", "Legal research website by Free Law Project.");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p>CourtListener is a free legal research website containing millions of legal opinions from federal and state courts.</p>
	HTML );

// $r1 = $page->ref('', '');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Court Listener is a project by ${'Free Law Project'}.</p>
	HTML;



$div_Court_Listener_website = new WebsiteContentSection();
$div_Court_Listener_website->setTitleText("Court Listener website ");
$div_Court_Listener_website->setTitleLink("https://www.courtlistener.com/");
$div_Court_Listener_website->content = <<<HTML
	<p>CourtListener is a free legal research website containing millions of legal opinions from federal and state courts.
	With CourtListener, lawyers, journalists, academics, and the public can research an important case,
	stay up to date with case law as it develops, or do deep analysis using our raw data.</p>
	HTML;



$page->parent('justice.html');
$page->template("stub");
$page->body($div_introduction);

$page->body($div_Court_Listener_website);
$page->body('free_law_project.html');
