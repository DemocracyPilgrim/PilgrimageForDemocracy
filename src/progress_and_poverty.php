<?php
$page = new Page();
$page->h1("Progress and Poverty");
$page->tags("Book");
$page->keywords("Progress and Poverty");
$page->stars(0);

//$page->snp("description", "");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Progress_and_Poverty = new WikipediaContentSection();
$div_wikipedia_Progress_and_Poverty->setTitleText("Progress and Poverty");
$div_wikipedia_Progress_and_Poverty->setTitleLink("https://en.wikipedia.org/wiki/Progress_and_Poverty");
$div_wikipedia_Progress_and_Poverty->content = <<<HTML
	<p><strong>Progress and Poverty: An Inquiry into the Cause of Industrial Depressions and of Increase of Want with Increase of Wealth: The Remedy</strong>
	is an 1879 book by social theorist and economist Henry George.
	It is a treatise on the questions of why poverty accompanies economic and technological progress
	and why economies exhibit a tendency toward cyclical boom and bust.
	George uses history and deductive logic to argue for a radical solution
	focusing on the capture of economic rent from natural resource and land titles.</p>
	HTML;


$page->parent('list_of_books.html');
$page->template("stub");
$page->body($div_introduction);

$page->body('henry_george.html');

$page->body($div_wikipedia_Progress_and_Poverty);
