<?php
$page = new Page();
$page->h1("Prosecuting Donald Trump");
$page->tags("Podcast", "USA", "Donald Trump", "Andrew Weissmann", "Mary McCord");
$page->keywords("Prosecuting Donald Trump");
$page->stars(0);
$page->viewport_background("/free/prosecuting_donald_trump.png");

$page->snp("description", "Award-winning podcast hosted by US prosecutors Andrew Weissmann and Mary McCord.");
$page->snp("image",       "/free/prosecuting_donald_trump.1200-630.png");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>"Prosecuting Donald Trump" is an award-winning podcast hosted by American prosecution attorneys ${'Andrew Weissmann'} and ${'Mary McCord'}.</p>
	HTML;



$div_Prosecuting_Donald_Trump = new WebsiteContentSection();
$div_Prosecuting_Donald_Trump->setTitleText("Prosecuting Donald Trump");
$div_Prosecuting_Donald_Trump->setTitleLink("https://www.msnbc.com/prosecuting-donald-trump");
$div_Prosecuting_Donald_Trump->content = <<<HTML
	<p>Veteran prosecutors Andrew Weissmann and Mary McCord discuss and dissect the cases against former President Donald Trump,
	including the historic indictments from the Manhattan D.A., Special Counsel Jack Smith and Fulton County D.A. Fani Willis.</p>
	HTML;



$page->parent('list_of_documentaries.html');
$page->template("stub");
$page->body($div_introduction);
$page->related_tag("Prosecuting Donald Trump");

$page->body($div_Prosecuting_Donald_Trump);
