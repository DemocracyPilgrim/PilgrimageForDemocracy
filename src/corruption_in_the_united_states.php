<?php
$page = new Page();
$page->h1("Corruption in the United States");
$page->viewport_background("");
$page->keywords("Corruption in the United States");
$page->stars(0);
$page->tags("Institutions", "USA", "Corruption");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Corruption_in_the_United_States = new WikipediaContentSection();
$div_wikipedia_Corruption_in_the_United_States->setTitleText("Corruption in the United States");
$div_wikipedia_Corruption_in_the_United_States->setTitleLink("https://en.wikipedia.org/wiki/Corruption_in_the_United_States");
$div_wikipedia_Corruption_in_the_United_States->content = <<<HTML
	<p>Corruption in the United States is the act of government officials abusing their political powers for private gain, typically through bribery or other methods, in the United States government. Corruption in the United States has been a perennial political issue, peaking in the Jacksonian era and the Gilded Age before declining with the reforms of the Progressive Era.</p>
	HTML;


$page->parent('united_states.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Corruption in the United States");
$page->body($div_wikipedia_Corruption_in_the_United_States);
