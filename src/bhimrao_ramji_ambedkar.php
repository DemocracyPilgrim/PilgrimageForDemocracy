<?php
$page = new PersonPage();
$page->h1("B.R. Ambedkar");
$page->alpha_sort("Ambedkar, Bhimrao Ramji");
$page->tags("Person", "India", "Constitution", "Constitution of India");
$page->keywords("Bhimrao Ramji Ambedkar", "B.R. Amdebkar", "Ambedkar");
$page->stars(2);
$page->viewport_background("/free/bhimrao_ramji_ambedkar.png");

$page->snp("description", "Indian economist, jurist, social reformer and political leader");
$page->snp("image",       "/free/bhimrao_ramji_ambedkar.1200-630.png");

$page->preview( <<<HTML
	<p></p>
	HTML );

$r1 = $page->ref("https://main.sci.gov.in/AMB/pdf/Closing%20speech%2025%20Nov%201949.pdf", "[PDF] Constituent Assembly Debates, 25th November, 1949.");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Bhimrao Ramji Ambedkar was an $Indian economist, jurist, social reformer and political leader.
	He chaired the committee that drafted the ${'Constitution of India'}.</p>
	HTML;

$div_selected_quotes = new ContentSection();
$div_selected_quotes->content = <<<HTML
	<h3>Selected quotes</h3>

	<p>On political and economic equality:</p>

	<blockquote>
		In politics, we will have equality; and in social and economic life, we will have inequality.
		In politics we will be recognizing the principle of one man one vote and one vote one value.
		In our social and economic life, we shall, by reason of our social and economic structure, continue to deny the principle of one man one value.
		How long shall we continue to live this life of contradictions?
		How long shall we continue to deny equality in our social and economic life?
		I we continue to deny it for long, we will do so only by putting our political democracy in peril.
	</blockquote>

	<p>On fraternity:</p>

	<blockquote>
		Fraternity can be a fact only when there is a nation.
		Without fraternity, equality and liberty will be no deeper than coats of paint.
	</blockquote>
	HTML;

$div_Ambedkar_and_the_Levels_of_Democracy = new ContentSection();
$div_Ambedkar_and_the_Levels_of_Democracy->content = <<<HTML
	<h3>Ambedkar and the Levels of Democracy</h3>

	<p>Excerpt from B.R. Ambedkar's speech to the Constituent Assembly:</p>

	<blockquote>
		Political democracy cannot last unless there lies at the base of it social democracy.
		What does social democracy mean?
		It means a way of life which recognizes liberty, equality and fraternity as the principles of life.
		These principles of liberty, equality and fraternity are not to be treated as separate items in a trinity.
		They form a union of trinity in the sense that to divorce one from the other is to defeat the very purpose of democracy.
		Liberty cannot be divorced from equality, equality cannot be divorced from liberty.
		Nor can liberty and equality be divorced from fraternity.
		Without equality, liberty would produce the supremacy of the few over the many.
		Equality without liberty would kill individual initiative.
		Without fraternity, liberty and equality could not become a natural course of things.
		It would require a constable to enforce them.
	</blockquote>

	<p>In Ambedkar's analysis:</p>

	<ul>
		<li><strong>Liberty</strong> corresponds to the ${'first level of democracy'}.</li>
		<li>
			<strong>Equality</strong> in this quote corresponds to the ${'sixth level of democracy'}.
			In the same speech, Ambedkar speaks of "political equality", the equal right to vote, which is the ${'fourth level of democracy'}.
			And he also speak of social and economic equality, which are the ${'second level'} and ${'sixth level'} respectively.
		</li>
		<li><strong>Fraternity</strong> corresponds to the ${'eighth level of democracy'}.</li>
	</ul>

	HTML;


$div_Speech_to_the_Constituent_Assembly_25_November_1949 = new ContentSection();
$div_Speech_to_the_Constituent_Assembly_25_November_1949->content = <<<HTML
	<h3>Speech to the Constituent Assembly, 25 November 1949</h3>

	<p>Excerpt:</p>

	<blockquote>
		<p>[We must not be] content with mere political democracy. We
		must make our political democracy a social democracy as well. Political democracy
		cannot last unless there lies at the base of it social democracy. What does social democracy
		mean? It means a way of life which recognizes liberty, equality and fraternity as the
		principles of life. These principles of liberty, equality and fraternity are not to be treated
		as separate items in a trinity. They form a union of trinity in the sense that to divorce one
		from the other is to defeat the very purpose of democracy. Liberty cannot be divorced
		from equality, equality cannot be divorced from liberty. Nor can liberty and equality be
		divorced from fraternity. Without equality, liberty would produce the supremacy of the
		few over the many. Equality without liberty would kill individual initiative. Without
		fraternity, liberty and equality could not become a natural course of things. It would
		require a constable to enforce them. We must begin by acknowledging the fact that there
		is complete absence of two things in Indian Society.</p>

		<p>One of these is equality. On the
		social plane, we have in India a society based on the principle of graded inequality which
		means elevation for some and degradation for others. On the economic plane, we have
		a society in which there are some who have immense wealth as against many who live
		in abject poverty. On the 26th of January 1950, we are going to enter into a life of
		contradictions. In politics we will have equality and in social and economic life we will
		have inequality. In politics we will be recognizing the principle of one man one vote and
		one vote one value. In our social and economic life, we shall, by reason of our social and
		economic structure, continue to deny the principle of one man one value. How long shall
		we continue to live this life of contradictions? How long shall we continue to deny
		equality in our social and economic life? I we continue to deny it for long, we will do
		so only by putting our political democracy in peril. We must remove this contradiction
		at the earliest possible moment or else those who suffer from inequality will blow up the
		structure of political democracy which this Assembly has so laboriously built up.</p>


		<p>The second thing we are wanting in is recognition of the principle of fraternity. What
		does fraternity mean? Fraternity means a sense of common brotherhood of all Indians—
		if Indians being one people. It is the principle which gives unity and solidarity to social
		life. It is a difficult thing to achieve. How difficult it is, can be realized from the story
		related by James Bryce in this volume on American Commonwealth about the United
		States of America. The story is—I propose to recount it in the words of Bryce himself—that—:</p>

		<blockquote>
			“Some years ago the American Protestant Episcopal Church was occupied at its triennial Convention
			in revising its liturgy. It was though desirable to introduce among the short sentence prayers a
			prayer for the whole people, and an eminent New England
			divine proposed the words ‘O Lord, bless our nation’. Accepted one afternoon, on the
			spur of the moment, the sentence was brought up next day for reconsideration, when
			so many objections were raised by the laity to the word ‘nation.’ as importing too
			definite a recognition of national unity, that it was dropped, and instead there were
			adopted the words ‘O Lord, bless these United States’.”
		</blockquote>

		<p>There was so little solidarity in the U.S.A. at the time when this incident occurred
		that the people of America did not think that they were a nation. If the people of the
		United State could not feel that they were a nation, how difficult it is for Indians to think
		that they are a nation. I remember the days when politically-minded Indians resented the
		expression “the people of India”. They preferred the expression “the Indian nation.” I am
		of opinion that in believing that we are a nation, we are cherishing a great delusion. How
		can people divided into several thousands of castes be a nation? The sooner we realize
		that we are not as yet a nation in the social and psychological sense of the world, the
		better for us. For then only we shall realize the necessity of becoming a nation and
		seriously think of ways and means of realizing the goal. The realization of this goal is
		going to be very difficult—far more difficult than it has been in the United States. The
		United States has no caste problem. In India there are castes. The castes are anti-national.
		In the first place because they bring about separation in social life. They are antinational
		also because they generate jealousy and antipathy between caste and caste. But we must
		overcome all these difficulties if we wish to become a nation in reality. For fraternity can
		be a fact only when there is a nation. Without fraternity equality and liberty will be no
		deeper than coats of paint.</p>


		<p>These are my reflections about the tasks that lie ahead of us. They may not be very
		pleasant to some. But there can be no gainsaying that political power in this country has
		too long been the monopoly of a few and the many are not only beasts of burden, but
		also beasts of prey. This monopoly has not merely deprived them of their chance of
		betterment, it has sapped them of what may be called the significance of life. These
		down-trodden classes are tired of being governed. They are impatient to govern themselves.
		This urge for self-realization in the down-trodden classes must not be allowed to devolve
		into a class struggle or class war. It would lead to a division of the House. That would
		indeed be a day of disaster. For, as has been well said by Abraham Lincoln, a House
		divided against itself cannot stand very long. Therefore the sooner room is made for the
		realization of their aspiration, the better for the few, the better for the country, the better
		for the maintenance for its independence and the better for the continuance of its democratic
		structure. This can only be done by the establishment of equality and fraternity in all
		spheres of life. That is why I have laid so much stress on them.</p>

		<p>I do not wish to weary the House any further. Independence is no doubt a matter of
		joy. But let us not forget that this independence has thrown on us great responsibilities.
		By independence, we have lost the excuse of blaming the British for anything going
		wrong. If hereafter things go wrong, we will have nobody to blame except ourselves.
		There is great danger of things going wrong. Times are fast changing. People including
		our own are being moved by new ideologies. They are getting tired of Government by
		the people. They are prepared to have Government for the people and are indifferent
		whether it is Government of the people and by the people. If we wish to preserve
		the Constitution in which we have sought to enshrine the principle of Government
		of the people, for the people and by the people, let us resolve not be tardy in the
		recognition of the evils that lie across our path and which induce people to prefer
		Government for the people to Government by the people, nor to be weak in our initiative
		to remove them. That is the only way to serve the country. I know of no better.</p>

	<blockquote>

	HTML;


$div_wikipedia_B_R_Ambedkar = new WikipediaContentSection();
$div_wikipedia_B_R_Ambedkar->setTitleText("B R Ambedkar");
$div_wikipedia_B_R_Ambedkar->setTitleLink("https://en.wikipedia.org/wiki/B._R._Ambedkar");
$div_wikipedia_B_R_Ambedkar->content = <<<HTML
	<p>Bhimrao Ramji Ambedkar (Bhīmrāo Rāmjī Āmbēḍkar; 14 April 1891 – 6 December 1956) was an Indian economist, jurist, social reformer and political leader who chaired the committee that drafted the Constitution of India based on the debates of the Constituent Assembly of India and the first draft of Sir Benegal Narsing Rau. Ambedkar served as Law and Justice minister in the first cabinet of Jawaharlal Nehru. He later converted to Buddhism and inspired the Dalit Buddhist movement after renouncing Hinduism.</p>
	HTML;


$page->parent('list_of_people.html');
$page->body($div_introduction);

$page->body($div_selected_quotes);
$page->body($div_Ambedkar_and_the_Levels_of_Democracy);


$page->body($div_Speech_to_the_Constituent_Assembly_25_November_1949);
$page->related_tag("Bhimrao Ramji Ambedkar");
$page->body($div_wikipedia_B_R_Ambedkar);
