<?php
$page = new Page();
$page->h1("The Children's Peace Movement (Columbia)");
$page->tags("Humanity", "Colombia", "Children's Rights", "Education", "Peace");
$page->keywords("The Children's Peace Movement");
$page->stars(2);
$page->viewport_background("/free/the_children_s_peace_movement_columbia.png");

$page->snp("description", "Amidst hardship, a powerful, youthful force for change emerged...");
$page->snp("image",       "/free/the_children_s_peace_movement_columbia.1200-630.png");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<h3>The Children's Peace Movement: Planting Seeds of Hope in Colombia</h3>

	<p>For decades, $Colombia has been scarred by conflict, leaving deep wounds across its landscape and in the hearts of its people.
	But amidst this hardship, a powerful force for change has emerged:
	the Children's Peace Movement, a grassroots initiative led by the very children and adolescents who have been most affected by the violence.</p>

	<p>This isn't just a group of kids talking about peace; it's a vibrant, active movement.
	Officially known as the "Movimiento Nacional de Niños, Niñas y Adolescentes Constructoras y Constructores de Paz"
	(National Movement of Children and Adolescents as Builders of Peace),
	it's a network of local groups across Colombia, driven by the belief that young people are not just victims of conflict, but powerful agents of change.</p>

	<p>Imagine a community devastated by displacement and fear.
	Now picture children coming together to learn how to resolve conflict, expressing themselves through art and music,
	and planting community gardens as symbols of hope.
	This is the essence of the Children's Peace Movement.
	They're not waiting for adults to fix the world; they're actively shaping it, one community at a time.</p>

	<p>Their activities are as diverse as the children themselves:</p>

	<ul>

	<li><strong>Peace Education:</strong>
	They participate in workshops that teach them about tolerance, respect, and how to communicate without violence.</li>

	<li><strong>Artistic Expression:</strong>
	They use their creativity through music, theater, and art to express their experiences and promote messages of peace.</li>

	<li><strong>Community Action:</strong>
	They take practical steps to improve their communities, from cleaning up parks to creating safe spaces for play and dialogue.</li>

	<li><strong>Advocacy:</strong>
	They learn to speak up for their rights and engage with local leaders to ensure children's voices are heard.</li>

	</ul>


	<p>These young peacemakers are often working in areas that have seen the worst of the conflict.
	Many have been displaced from their homes, lost loved ones, or witnessed violence firsthand.
	The movement provides a space for them to heal, share their stories, and build connections with others who have experienced similar traumas.</p>

	<p>The impact of the Children's Peace Movement extends far beyond individual healing.
	By fostering a culture of peace and understanding from a young age, they are planting the seeds for a more tolerant and just future for Colombia.
	Their efforts are supported by various local and international organizations, demonstrating the collective commitment to empowering these young peacebuilders.</p>

	<p>The Children's Peace Movement is more than just a program; it's a testament to the incredible resilience and potential of young people.
	It’s a reminder that even in the face of unimaginable challenges, hope can flourish, and children can lead the way towards a more peaceful world.
	These young Colombians are proving that the future of peace is not just a dream; it's a movement led by the very generation that will inherit it.</p>
	HTML;

$div_The_Colombian_Childrens_Peace_Movement = new WebsiteContentSection();
$div_The_Colombian_Childrens_Peace_Movement->setTitleText("The Colombian Children's Peace Movement");
$div_The_Colombian_Childrens_Peace_Movement->setTitleLink("https://www.uua.org/re/tapestry/youth/call/workshop8/172987.shtml");
$div_The_Colombian_Childrens_Peace_Movement->content = <<<HTML
	<p>Adults are usually regarded as the leaders in working for $peace and—for good reason, as many have made a tremendous difference.
	Yet children can make a tremendous difference, too. Farliz Calle is one of those children.
	She is one of twenty-six children who organized an election in her home country of $Colombia as it was being torn apart by violence.
	On October 25, 1996, 2.7 million children in Colombia cast their votes for 12 basic rights that included the right to love and family,
	the right to a clean environment, the right to justice, and the right to peace.
	With the aid of $UNICEF (The United Nations Children's Emergency Fund) and adults from the community,
	the children of Colombia started a movement that created "peace zones" in schools and parks.
	One year later, 10 million adults also voted for peace in a national election.</p>
	HTML;


$page->parent('humanity.html');
$page->body($div_introduction);
$page->template('cover-picture-ai-generated');



$page->related_tag("The Children's Peace Movement");
$page->body($div_The_Colombian_Childrens_Peace_Movement);
