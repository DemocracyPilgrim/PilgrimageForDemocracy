<?php
$page = new PersonPage();
$page->h1("Michael Schmidt");
$page->alpha_sort("Schmidt, Michael");
$page->tags("Person", "Information: Media", "Trump DOJ Weaponization");
$page->keywords("Michael Schmidt");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Michael_S_Schmidt = new WikipediaContentSection();
$div_wikipedia_Michael_S_Schmidt->setTitleText("Michael S Schmidt");
$div_wikipedia_Michael_S_Schmidt->setTitleLink("https://en.wikipedia.org/wiki/Michael_S._Schmidt");
$div_wikipedia_Michael_S_Schmidt->content = <<<HTML
	<p>Michael S. Schmidt (born September 23, 1983) is an American journalist, author, and correspondent for The New York Times in Washington, D.C. He is also a producer of a Netflix show. He covers national security and federal law enforcement, and has broken several high-profile stories about politics, media and sports. He is also a national security contributor for MSNBC and NBC News.</p>
	HTML;


$page->parent('list_of_people.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Michael Schmidt");
$page->body($div_wikipedia_Michael_S_Schmidt);
