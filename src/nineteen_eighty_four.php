<?php
$page = new BookPage();
$page->h1("Nineteen Eighty-Four");
$page->tags("Book", "Fascism", "Orwellian Doublespeak", "Disinformation");
$page->keywords("Nineteen Eighty-Four", "1984", "Orwellian");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>"Nineteen Eighty-Four" is a dystopian novel and cautionary tale by English writer ${'George Orwell'}.
	In the story, the totalitarian country of Oceania is ruled by Big Brother and "The Party".
	Oceania's four government ministries display the Party's three slogans – "WAR IS PEACE", "FREEDOM IS SLAVERY", "IGNORANCE IS STRENGTH".
	The ministries are deliberately named after the opposite (doublethink) of their true functions:
	the Ministry of Peace concerns itself with war, the Ministry of Truth with lies, the Ministry of Love with torture and the Ministry of Plenty with starvation.</p>
	HTML;

$div_wikipedia_Nineteen_Eighty_Four = new WikipediaContentSection();
$div_wikipedia_Nineteen_Eighty_Four->setTitleText("Nineteen Eighty Four");
$div_wikipedia_Nineteen_Eighty_Four->setTitleLink("https://en.wikipedia.org/wiki/Nineteen_Eighty-Four");
$div_wikipedia_Nineteen_Eighty_Four->content = <<<HTML
	<p>Nineteen Eighty-Four (also published as 1984) is a dystopian novel and cautionary tale by English writer George Orwell. It was published on 8 June 1949 by Secker & Warburg as Orwell's ninth and final book completed in his lifetime. Thematically, it centres on the consequences of totalitarianism, mass surveillance, and repressive regimentation of people and behaviours within society. Orwell, a staunch believer in democratic socialism and member of the anti-Stalinist Left, modelled the Britain under authoritarian socialism in the novel on the Soviet Union in the era of Stalinism and on the very similar practices of both censorship and propaganda in Nazi Germany. More broadly, the novel examines the role of truth and facts within societies and the ways in which they can be manipulated.</p>
	HTML;



$div_wikipedia_Doublethink = new WikipediaContentSection();
$div_wikipedia_Doublethink->setTitleText("Doublethink");
$div_wikipedia_Doublethink->setTitleLink("https://en.wikipedia.org/wiki/Doublethink");
$div_wikipedia_Doublethink->content = <<<HTML
	<p>
	</p>
	HTML;


$div_wikipedia_Politics_and_the_English_Language = new WikipediaContentSection();
$div_wikipedia_Politics_and_the_English_Language->setTitleText("Politics and the English Language");
$div_wikipedia_Politics_and_the_English_Language->setTitleLink("https://en.wikipedia.org/wiki/Politics_and_the_English_Language");
$div_wikipedia_Politics_and_the_English_Language->content = <<<HTML
	<p>
	</p>
	HTML;


$div_wikipedia_Doublespeak = new WikipediaContentSection();
$div_wikipedia_Doublespeak->setTitleText("Doublespeak");
$div_wikipedia_Doublespeak->setTitleLink("https://en.wikipedia.org/wiki/Doublespeak");
$div_wikipedia_Doublespeak->content = <<<HTML
	<p>
	</p>
	HTML;


$div_wikipedia_Newspeak = new WikipediaContentSection();
$div_wikipedia_Newspeak->setTitleText("Newspeak");
$div_wikipedia_Newspeak->setTitleLink("https://en.wikipedia.org/wiki/Newspeak");
$div_wikipedia_Newspeak->content = <<<HTML
	<p>
	</p>
	HTML;


$div_wikipedia_Orwellian = new WikipediaContentSection();
$div_wikipedia_Orwellian->setTitleText("Orwellian ");
$div_wikipedia_Orwellian->setTitleLink("https://en.wikipedia.org/wiki/Orwellian");
$div_wikipedia_Orwellian->content = <<<HTML
	<p>
	</p>
	HTML;



$page->parent('list_of_books.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Orwellian");
$page->body($div_wikipedia_Nineteen_Eighty_Four);

$page->body($div_wikipedia_Orwellian);
$page->body($div_wikipedia_Newspeak);
$page->body($div_wikipedia_Doublespeak);
$page->body($div_wikipedia_Politics_and_the_English_Language);
$page->body($div_wikipedia_Doublethink);
