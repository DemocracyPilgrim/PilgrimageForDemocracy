<?php
$page = new Page();
$page->h1('Political Grift');
$page->keywords('Grift', 'grift', 'political grift');
$page->stars(0);
$page->tags("Danger", "Economy", "Donald Trump", "Crime");
$page->viewport_background('');


$page->snp('description', 'Scamming one\'s own supporter base.');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Political grift is what politicians do when they scam their own supporters.
	Example: Donald Trump is using his own legal troubles to grift his supporters.</p>
	HTML;

$div_wikipedia_Grift = new WikipediaContentSection();
$div_wikipedia_Grift->setTitleText('Grift');
$div_wikipedia_Grift->setTitleLink('https://en.wikipedia.org/wiki/Grift');
$div_wikipedia_Grift->content = <<<HTML
	<p>Wikipedia does not have an article on political grift.
	The closest that this disambiguation page offers is: "Confidence trick".</p>
	HTML;


$page->parent('dangers.html');
$page->template("stub");
$page->body($div_introduction);


$page->related_tag("Grift");
$page->body($div_wikipedia_Grift);
