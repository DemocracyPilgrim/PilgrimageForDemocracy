<?php
$page = new Page();
$page->h1("Women's Rights");
$page->tags("Individual: Rights");
$page->keywords("Women's Rights", "women's rights", "women");
$page->stars(1);
$page->viewport_background("/free/women_s_rights.png");

$page->snp("description", "The Rights and Liberties of women and girls all over the world.");
$page->snp("image",       "/free/women_s_rights.1200-630.png");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Women's rights are fundamental ${'human rights'}, essential components of a just and equitable society, and a cornerstone of a thriving $democracy.
	However, the reality for women and girls around the $world paints a starkly different picture.
	From gender-based violence and discrimination to limited access to $education, healthcare, and economic opportunities,
	women and girls face countless challenges that hinder their full participation in society.
	The fight for women's rights is a global struggle, demanding a united effort to address issues
	like gender equality, reproductive rights, economic empowerment, and the eradication of violence against women and girls.</p>
	HTML;

$list_Women_s_Rights = ListOfPeoplePages::WithTags("Women's Rights");
$print_list_Women_s_Rights = $list_Women_s_Rights->print();

$div_list_Women_s_Rights = new ContentSection();
$div_list_Women_s_Rights->content = <<<HTML
	<h3>Related content</h3>

	$print_list_Women_s_Rights
	HTML;


$div_wikipedia_Women_s_rights = new WikipediaContentSection();
$div_wikipedia_Women_s_rights->setTitleText("Women s rights");
$div_wikipedia_Women_s_rights->setTitleLink("https://en.wikipedia.org/wiki/Women's_rights");
$div_wikipedia_Women_s_rights->content = <<<HTML
	<p>Women's rights are the rights and entitlements claimed for women and girls worldwide.
	They formed the basis for the women's rights movement in the 19th century and the feminist movements during the 20th and 21st centuries.
	In some countries, these rights are institutionalized or supported by law, local custom, and behaviour,
	whereas in others, they are ignored and suppressed.
	They differ from broader notions of human rights through claims of an inherent historical and traditional bias against the exercise of rights by women and girls,
	in favour of men and boys.</p>

	<p>Issues commonly associated with notions of women's rights include the right to bodily integrity and autonomy, to be free from sexual violence,
	to vote, to hold public office, to enter into legal contracts, to have equal rights in family law, to work,
	to fair wages or equal pay, to have reproductive rights, to own property, and to education.</p>
	HTML;


$page->parent('individuals.html');
$page->template("stub");
$page->body($div_introduction);
$page->body($div_list_Women_s_Rights);


$page->body($div_wikipedia_Women_s_rights);
