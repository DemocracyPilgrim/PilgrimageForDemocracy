<?php
$page = new Page();
$page->h1("Poor People's Campaign: A National Call for a Moral Revival");
$page->keywords("Poor People's Campaign: A National Call for a Moral Revival", "Poor People's Campaign");
$page->tags("Organisation", "Living", "Humanity");
$page->stars(0);

$page->snp("description", "");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );

$r1 = $page->ref('https://www.poorpeoplescampaign.org/about/our-principles/', "Poor People's Campaign: Our Principles");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Poor People's Campaign: A National Call for a Moral Revival is an American anti-poverty campaign led by ${'William Barber II'} and Liz Theoharis.</p>
	HTML;




$div_Poor_Peoples_Campaign_website = new WebsiteContentSection();
$div_Poor_Peoples_Campaign_website->setTitleText("Poor Peoples Campaign website ");
$div_Poor_Peoples_Campaign_website->setTitleLink("https://www.poorpeoplescampaign.org/");
$div_Poor_Peoples_Campaign_website->content = <<<HTML
	<p><strong>Fundamental Principles:</strong>$r1</p>

	<ol>
		<li>We are rooted in a moral analysis based on our deepest religious and constitutional values that demand justice for all. Moral revival is necessary to save the heart and soul of our democracy.</li>
		<li>We are committed to lifting up and deepening the leadership of those most affected by systemic racism, poverty, the war economy, and ecological devastation and to building unity across lines of division.</li>
		<li>We believe in the dismantling of unjust criminalization systems that exploit poor communities and communities of color and the transformation of the “War Economy” into a “Peace Economy” that values all humanity.</li>
		<li>We believe that equal protection under the law is non-negotiable.</li>
		<li>We believe that people should not live in or die from poverty in the richest nation ever to exist. Blaming the poor and claiming that the United States does not have an abundance of resources to overcome poverty are false narratives used to perpetuate economic exploitation, exclusion, and deep inequality.</li>
		<li>We recognize the centrality of systemic racism in maintaining economic oppression must be named, detailed and exposed empirically, morally and spiritually. Poverty and economic inequality cannot be understood apart from a society built on white supremacy.</li>
		<li>Whereas the distorted moral narrative of religious nationalism blames poor and oppressed people for our poverty and oppression, our deepest religious and constitutional values insist that the primary moral issues of our day must be how our society treats the poor, those on the margins, women, LGBTQIA2S+ folks, workers, immigrants, the disabled and the sick; equal protection under the law; and the desire for peace, love and harmony within and among nations.</li>
		<li>We will build up the power of people and state-based movements to serve as a vehicle for a powerful moral movement in the country and to transform the political, economic and moral structures of our society.</li>
		<li>We recognize the need to organize at the state and local level—many of the most regressive policies are being passed at the state level, and these policies will have long and lasting effect, past even executive orders. The movement is not from above but below.</li>
		<li>We will do our work in a non-partisan way—no elected officials or candidates get the stage or serve on the State Organizing Committee of the Campaign. This is not about left and right, Democrat or Republican but about right and wrong.</li>
		<li>We uphold the need to do a season of sustained moral direct action as a way to break through the tweets and shift the moral narrative. We are demonstrating the power of people coming together across issues and geography and putting our bodies on the line to the issues that are affecting us all.</li>
		<li>The Campaign and all its Participants and Endorsers embrace nonviolence. Violent tactics or actions will not be tolerated.</li>
	</ol>
	HTML;




$div_wikipedia_Poor_People_s_Campaign_A_National_Call_for_a_Moral_Revival = new WikipediaContentSection();
$div_wikipedia_Poor_People_s_Campaign_A_National_Call_for_a_Moral_Revival->setTitleText("Poor People s Campaign A National Call for a Moral Revival");
$div_wikipedia_Poor_People_s_Campaign_A_National_Call_for_a_Moral_Revival->setTitleLink("https://en.wikipedia.org/wiki/Poor_People's_Campaign:_A_National_Call_for_a_Moral_Revival");
$div_wikipedia_Poor_People_s_Campaign_A_National_Call_for_a_Moral_Revival->content = <<<HTML
	<p>Poor People's Campaign: A National Call for a Moral Revival is an American anti-poverty campaign led by William Barber II and Liz Theoharis.</p>
	HTML;

$div_wikipedia_Poor_People_s_Campaign = new WikipediaContentSection();
$div_wikipedia_Poor_People_s_Campaign->setTitleText("Poor People s Campaign");
$div_wikipedia_Poor_People_s_Campaign->setTitleLink("https://en.wikipedia.org/wiki/Poor_People's_Campaign");
$div_wikipedia_Poor_People_s_Campaign->content = <<<HTML
	<p>The Poor People's Campaign, or Poor People's March on Washington, was a 1968 effort to gain economic justice for poor people in the United States. It was organized by Martin Luther King Jr. and the Southern Christian Leadership Conference (SCLC), and carried out under the leadership of Ralph Abernathy in the wake of King's assassination in April 1968.</p>

	<p>The campaign demanded economic and human rights for poor Americans of diverse backgrounds. After presenting an organized set of demands to Congress and executive agencies, participants set up a 3,000-person protest camp on the Washington Mall, where they stayed for six weeks in the spring of 1968.</p>

	<p>The Poor People's Campaign was motivated by a desire for economic justice: the idea that all people should have what they need to live. King and the SCLC shifted their focus to these issues after observing that gains in civil rights had not improved the material conditions of life for many African Americans. The Poor People's Campaign was a multiracial effort—including African Americans, European Americans, Asian Americans, Hispanic Americans, and Native Americans—aimed at alleviating poverty regardless of race.</p>
	HTML;


$page->parent('list_of_organisations.html');
$page->parent('poverty.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_Poor_Peoples_Campaign_website);

$page->body('white_poverty_how_exposing_myths_about_race_and_class_can_reconstruct_american_democracy.html');

$page->body($div_wikipedia_Poor_People_s_Campaign_A_National_Call_for_a_Moral_Revival);
$page->body($div_wikipedia_Poor_People_s_Campaign);
