<?php
$page = new Page();
$page->h1("National Federation of Independent Business v. Sebelius (2012)");
$page->tags("Institutions: Judiciary", "SCOTUS Landmark Decision", "USA", "Healthcare", "Taxes");
$page->keywords("National Federation of Independent Business v. Sebelius");
$page->stars(1);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>"Kelo v. City of New London" is a 2005 landmark decision by the ${'Supreme Court of the United States'}.</p>

	<p>This case upheld the constitutionality of the Affordable Care Act (ACA),
	but also struck down the individual mandate requiring most Americans to have health insurance.
	This ruling has impacted the implementation of the ACA and contributed to ongoing debates about healthcare reform.</p>
	HTML;

$div_wikipedia_National_Federation_of_Independent_Business_v_Sebelius = new WikipediaContentSection();
$div_wikipedia_National_Federation_of_Independent_Business_v_Sebelius->setTitleText("National Federation of Independent Business v Sebelius");
$div_wikipedia_National_Federation_of_Independent_Business_v_Sebelius->setTitleLink("https://en.wikipedia.org/wiki/National_Federation_of_Independent_Business_v._Sebelius");
$div_wikipedia_National_Federation_of_Independent_Business_v_Sebelius->content = <<<HTML
	<p>National Federation of Independent Business v. Sebelius, 567 U.S. 519 (2012), is a landmark United States Supreme Court decision
	in which the Court upheld Congress's power to enact most provisions of the Patient Protection and Affordable Care Act (ACA), commonly called Obamacare,
	and the Health Care and Education Reconciliation Act (HCERA),
	including a requirement for most Americans to pay a penalty for forgoing health insurance by 2014.
	The Acts represented a major set of changes to the American health care system
	that had been the subject of highly contentious debate, largely divided on political party lines.</p>

	<p>The Supreme Court, in an opinion written by Chief Justice John Roberts,
	upheld by a vote of 5–4 the individual mandate to buy health insurance as a constitutional exercise of Congress's power
	under the Taxing and Spending Clause (taxing power).</p>

	<p>A majority of the justices, including Roberts,
	agreed that the individual mandate was not a proper use of Congress's Commerce Clause or Necessary and Proper Clause powers,
	although they did not join in a single opinion.</p>

	<p>A majority of the justices also agreed that another challenged provision of the Act, a significant expansion of Medicaid,
	was not a valid exercise of Congress's spending power, as it would coerce states to either accept the expansion or risk losing existing Medicaid funding.</p>
	HTML;


$page->parent('supreme_court_of_the_united_states.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_wikipedia_National_Federation_of_Independent_Business_v_Sebelius);
