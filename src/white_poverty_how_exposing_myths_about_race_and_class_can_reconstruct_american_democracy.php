<?php
$page = new Page();
$page->h1("White Poverty: How Exposing Myths About Race and Class Can Reconstruct American Democracy");
$page->tags("Book");
$page->stars(1);

$page->snp("description", "Exposing myths about poverty.");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref('', '');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>"White Poverty: How Exposing Myths About Race and Class Can Reconstruct American Democracy"
	is a book coauthored by ${'William Barber II'} and ${'Jonathan Wilson-Hartgrove'}</p>
	HTML;



$div_White_Poverty_Liveright_Publishing = new WebsiteContentSection();
$div_White_Poverty_Liveright_Publishing->setTitleText("White Poverty (Liveright Publishing)");
$div_White_Poverty_Liveright_Publishing->setTitleLink("https://wwnorton.com/books/9781324094876");
$div_White_Poverty_Liveright_Publishing->content = <<<HTML
	<p><strong>A generational work with far-ranging social and political implications,
	White Poverty, promises to be one of the most influential books in recent years.</strong></p>

	<p>One of the most pernicious and persistent myths in the United States is the association of Black skin with poverty. Though there are forty million more poor white people than Black people, most Americans, both Republicans and Democrats, continue to think of poverty—along with issues like welfare, unemployment, and food stamps—as solely a Black problem. Why is this so? What are the historical causes? And what are the political consequences that result?</p>

	<p>These are among the questions that the Reverend Dr. William J. Barber II, a leading advocate for the rights of the poor and the “closest person we have to Dr. King” (Cornel West), addresses in White Poverty, a groundbreaking work that exposes a legacy of historical myths that continue to define both white and Black people, creating in the process what might seem like an insuperable divide. Analyzing what has changed since the 1930s, when the face of American poverty was white, Barber, along with Jonathan Wilson-Hartgrove, addresses white poverty as a hugely neglected subject that just might provide the key to mitigating racism and bringing together tens of millions of working class and impoverished Americans.</p>

	<p>Thus challenging the very definition of who is poor in America, Barber writes about the lies that prevent us from seeing the pain of poor white families who have been offered little more than their “whiteness” and angry social media posts to sustain them in an economy where the costs of housing, healthcare, and education have skyrocketed while wages have stagnated for all but the very rich. Asserting in Biblically inspired language that there should never be shame in being poor, White Poverty lifts the hope for a new “moral fusion movement” that seeks to unite people “who have been pitted against one another by politicians (and billionaires) who depend on the poorest of us not being here.”</p>

	<p>Ultimately, White Poverty, a ringing work that braids poignant autobiographical recollections with astute historical analysis, contends that tens of millions of America’s poorest earners, the majority of whom don’t vote, have much in common, thus providing us with one of the most empathetic and visionary approaches to American poverty in decades.</p>
	HTML;



$page->parent('list_of_books.html');
$page->parent('poverty.html');

$page->template("stub");
$page->body($div_introduction);

$page->body($div_White_Poverty_Liveright_Publishing);

$page->body('poor_people_s_campaign_a_national_call_for_a_moral_revival.html');
