<?php
$page = new OrganisationPage();
$page->h1("Centre for Law & Policy Research");
$page->tags("Organisation", "India", "Institutions: Legislative", "Constitution of India");
$page->keywords("Centre for Law & Policy Research", "CLPR");
$page->stars(1);
$page->viewport_background("/free/centre_for_law_and_policy_research.png");

$page->snp("description", "Dedicated to the rule of law and defense of Indian constitutional values");
$page->snp("image",       "/free/centre_for_law_and_policy_research.1200-630.png");

$page->preview( <<<HTML
	<p></p>
	HTML );

$r1 = $page->ref("https://www.constitutionofindia.net/about-us/", "ConstitutionofIndia: About Us.");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>The Centre for Law & Policy Research is an $Indian non-profit educational think-tank promoting the ${'Constitution of India'}.</p>
	HTML;




$div_Centre_for_Law_and_Policy_Research = new WebsiteContentSection();
$div_Centre_for_Law_and_Policy_Research->setTitleText("Centre for Law and Policy Research ");
$div_Centre_for_Law_and_Policy_Research->setTitleLink("https://clpr.org.in/");
$div_Centre_for_Law_and_Policy_Research->content = <<<HTML
	<p>The Centre was established in recognition of the need to have a legal research organisation dedicated to the rule of law and defense of constitutional values.</p>

	<p>We, at CLPR, believe that the Constitution of India is the most authoritative and comprehensive point of reference and our core purpose is to ensure that the Constitution works for everyone.</p>
	HTML;




$div_Constitution_of_India = new WebsiteContentSection();
$div_Constitution_of_India->setTitleText("Constitution of India ");
$div_Constitution_of_India->setTitleLink("https://www.constitutionofindia.net/");
$div_Constitution_of_India->content = <<<HTML
	<p>Building a robust constitutional culture in India through a shared understanding of constitutional origins.</p>

	<p>ConstitutionofIndia.net is the flagship initiative by the Constitutional Culture team at the Centre for Law and Policy Research, Bangalore. The team aims to contribute to the realization of radical transformative goals of the Constitution of India, 1950 in the 21st century by creating a new Indian citizen who is critically aware of Indian constitutional history and its impact in shaping our everyday lives. $r1</p>
	HTML;




$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);



$page->body($div_Centre_for_Law_and_Policy_Research);
$page->body($div_Constitution_of_India);
$page->related_tag("Centre for Law & Policy Research");
