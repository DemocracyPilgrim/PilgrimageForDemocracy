<?php
$page = new Page();
$page->h1("6: Living");
$page->tags("Topic portal");
$page->keywords("Living", "Living: Fair Income", "Living: Fair Treatment", "Living: Economy");
$page->stars(0);
$page->viewport_background("/free/living.png");

$page->snp("description", "The need to make a living and provide for one's family.");
$page->snp("image",       "/free/living.1200-630.png");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Every $individual needs to make a living, have a job and an income in order to provide for oneself and one's family.</p>
	HTML;


$div_list_Living_Fair_Income = new ContentSection();
$div_list_Living_Fair_Income->content = <<<HTML
	<h3>Making a fair income.</h3>
	HTML;


$div_list_Living_Fair_Treatment = new ContentSection();
$div_list_Living_Fair_Treatment->content = <<<HTML
	<h3>Fair treatment of workers</h3>
	HTML;


$div_list_Economy = new ContentSection();
$div_list_Economy->content = <<<HTML
	<h3>Economy</h3>
	HTML;



$div_list_Living = new ContentSection();
$div_list_Living->content = <<<HTML
	<h3>Other related content</h3>
	HTML;



$page->parent('sixth_level_of_democracy.html');
$page->previous("information.html");
$page->next("international.html");

$page->body($div_introduction);

$page->related_tag("Living: Fair Income",    $div_list_Living_Fair_Income);
$page->related_tag("Living: Fair Treatment", $div_list_Living_Fair_Treatment);
$page->related_tag("Living: Economy",        $div_list_Economy);
$page->related_tag("Living",                 $div_list_Living);
