<?php
$page = new VideoPage();
$page->h1("Rachel Maddow Interviews Lev Parnas - Part 2");
$page->tags("Video: Interview", "Rachel Maddow", "Lev Parnas", "Donald Trump", "Ukraine", "From Russia With Lev");
$page->keywords("Rachel Maddow Interviews Lev Parnas - Part 2");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;



$div_youtube_Exclusive_Rachel_Maddow_Interviews_Lev_Parnas_Part_2_Rachel_Maddow_MSNBC = new YoutubeContentSection();
$div_youtube_Exclusive_Rachel_Maddow_Interviews_Lev_Parnas_Part_2_Rachel_Maddow_MSNBC->setTitleText("Exclusive: Rachel Maddow Interviews Lev Parnas - Part 2 | Rachel Maddow | MSNBC");
$div_youtube_Exclusive_Rachel_Maddow_Interviews_Lev_Parnas_Part_2_Rachel_Maddow_MSNBC->setTitleLink("https://www.youtube.com/watch?v=Xj-4V5ui8H4");
$div_youtube_Exclusive_Rachel_Maddow_Interviews_Lev_Parnas_Part_2_Rachel_Maddow_MSNBC->content = <<<HTML
	<p>
	</p>
	HTML;




$page->parent('list_of_videos.html');
$page->template("stub");
$page->body($div_introduction);



$page->body($div_youtube_Exclusive_Rachel_Maddow_Interviews_Lev_Parnas_Part_2_Rachel_Maddow_MSNBC);
$page->related_tag("Rachel Maddow Interviews Lev Parnas - Part 2");
