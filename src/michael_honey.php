<?php
$page = new PersonPage();
$page->h1("Michael Honey");
$page->alpha_sort("Honey, Michael");
$page->keywords("Michael Honey");
$page->stars(0);
$page->tags("Person", "Fair Share");

$page->snp("description", "American civil rights and labor historian.");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref('', '');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Michael Honey is an $American ${'civil rights'} and $labour historian.</p>

	<p>Honey is the editor of ${'All labor has dignity'}, a collection of speeches by Martin Luther King, Jr on labor and civil rights.</p>
	HTML;

$div_wikipedia_Michael_Honey = new WikipediaContentSection();
$div_wikipedia_Michael_Honey->setTitleText("Michael Honey");
$div_wikipedia_Michael_Honey->setTitleLink("https://en.wikipedia.org/wiki/Michael_Honey");
$div_wikipedia_Michael_Honey->content = <<<HTML
	<p>Michael K. Honey (born 1947) is an American historian, Guggenheim Fellow
	and Haley Professor of Humanities at the University of Washington Tacoma in the United States,
	where he teaches African-American, civil rights and labor history.</p>
	HTML;


$page->parent('list_of_people.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_wikipedia_Michael_Honey);
