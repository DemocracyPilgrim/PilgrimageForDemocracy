<?php
$page = new Page();
$page->h1('Democracy in America');
$page->tags("Book", "USA");
$page->keywords('Democracy in America');
$page->stars(0);

$page->snp('description', '1835 classic by Tocqueville.');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Democracy_in_America = new WikipediaContentSection();
$div_wikipedia_Democracy_in_America->setTitleText('Democracy in America');
$div_wikipedia_Democracy_in_America->setTitleLink('https://en.wikipedia.org/wiki/Democracy_in_America');
$div_wikipedia_Democracy_in_America->content = <<<HTML
	<p>De La Démocratie en Amérique (published in two volumes, the first in 1835 and the second in 1840)
	is a classic French text by Alexis de Tocqueville.
	Its title literally translates to Democracy in America.
	In the book, Tocqueville examines the democratic revolution that he believed had been occurring over the previous several hundred years.</p>
	HTML;


$page->parent('list_of_books.html');
$page->template("stub");
$page->body($div_introduction);
$page->body('alexis_de_tocqueville.html');


$page->body($div_wikipedia_Democracy_in_America);
