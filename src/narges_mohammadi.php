<?php
$page = new Page();
$page->h1('Narges Mohammadi');
$page->alpha_sort("Mohammadi, Narges");
$page->tags("Person");
$page->keywords('Narges Mohammadi');
$page->stars(1);

//$page->snp('description', '');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Narges Mohammadi is one of $Iran's leading human rights activists
	who has campaigned for women's rights and the abolition of the death penalty.</p>

	<p>In May 2021, Tehran sentenced Mohammadi to two-and-a-half years in prison, 80 lashes and two separate fines
	for charges including "spreading propaganda against the system."</p>

	<p>In 2023, she was awarded the ${'Nobel Peace Prize'}.</p>

	<p>"<em>Her brave struggle has come with tremendous personal costs.
	Altogether, the regime has arrested her 13 times, convicted her five times, and sentenced her to a total of 31 years in prison and 154 lashes</em>,"
	Berit Reiss-Andersen, head of the Norwegian Nobel Committee, said in Oslo during the announcement.</p>

	<p>She told The New York Times after the win she would never stop striving for democracy and equality – even if that meant staying in prison.
	The newspaper quoted her as saying:</p>

	<blockquote>I will continue to fight against the relentless discrimination,
	tyranny and gender-based oppression by the oppressive religious government until the liberation of women.
	</blockquote>
	HTML;

$div_wikipedia_Narges_Mohammadi = new WikipediaContentSection();
$div_wikipedia_Narges_Mohammadi->setTitleText('Narges Mohammadi');
$div_wikipedia_Narges_Mohammadi->setTitleLink('https://en.wikipedia.org/wiki/Narges_Mohammadi');
$div_wikipedia_Narges_Mohammadi->content = <<<HTML
	<p>Narges Mohammadi is an Iranian human rights activist, Nobel laureate, and scientist.
	She is the vice president of the Defenders of Human Rights Center (DHRC), headed by fellow Nobel Peace Prize laureate Shirin Ebadi.
	She is a vocal proponent of mass feminist civil disobedience against hijab in Iran and a vocal critic of the hijab and chastity program of 2023.
	In May 2016, she was sentenced in Tehran to 16 years' imprisonment
	for establishing and running "a human rights movement that campaigns for the abolition of the death penalty."
	She was released in 2020 but sent back to prison in 2021, where she has since given reports of the abuse of detained women.</p>

	<p>In October 2023, while in prison, she was awarded the 2023 Nobel Peace Prize
	"for her fight against the oppression of women in Iran and her fight to promote human rights and freedom for all."
	The Foreign Ministry of Iran condemned the decision to award Mohammadi.</p>
	HTML;


$page->parent('list_of_people.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_wikipedia_Narges_Mohammadi);
