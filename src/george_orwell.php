<?php
$page = new PersonPage();
$page->h1("George Orwell");
$page->alpha_sort("Orwell, George");
$page->tags("Person", "UK", "Fascism", "Orwellian");
$page->keywords("George Orwell");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>George Orwell is a British writer, author of the dystopian novel ${'Nineteen Eighty-Four'}.</p>
	HTML;

$div_wikipedia_George_Orwell = new WikipediaContentSection();
$div_wikipedia_George_Orwell->setTitleText("George Orwell");
$div_wikipedia_George_Orwell->setTitleLink("https://en.wikipedia.org/wiki/George_Orwell");
$div_wikipedia_George_Orwell->content = <<<HTML
	<p>Eric Arthur Blair (25 June 1903 – 21 January 1950) was a British novelist, poet, essayist, journalist, and critic who wrote under the pen name of George Orwell, a name inspired by his favourite place, the River Orwell. His work is characterised by lucid prose, social criticism, opposition to all totalitarianism (i.e. to both left-wing authoritarian communism and to right-wing fascism), and support of democratic socialism.</p>
	HTML;


$page->parent('list_of_people.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("George Orwell");
$page->body($div_wikipedia_George_Orwell);
