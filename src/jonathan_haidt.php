<?php
$page = new PersonPage();
$page->h1("Jonathan Haidt");
$page->alpha_sort("Jonathan Haidt");
$page->tags("Person", "Information: Discourse");
$page->keywords("Jonathan Haidt");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Jonathan Haidt is an $American social psychologist.
	He is the author of ${'The Righteous Mind'}.</p>
	HTML;

$div_wikipedia_Jonathan_Haidt = new WikipediaContentSection();
$div_wikipedia_Jonathan_Haidt->setTitleText("Jonathan Haidt");
$div_wikipedia_Jonathan_Haidt->setTitleLink("https://en.wikipedia.org/wiki/Jonathan_Haidt");
$div_wikipedia_Jonathan_Haidt->content = <<<HTML
	<p>Jonathan David Haidt is an American social psychologist and author.
	He is the Thomas Cooley Professor of Ethical Leadership at the New York University Stern School of Business.
	His main areas of study are the psychology of morality and moral emotions.</p>
	HTML;


$page->parent('list_of_people.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Jonathan Haidt");
$page->body($div_wikipedia_Jonathan_Haidt);
