<?php
$page = new CountryPage('Nicaragua');
$page->h1('Nicaragua');
$page->tags("Country");
$page->keywords('Nicaragua');
$page->stars(0);

$page->snp('description', '6.3 million inhabitants.');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Nicaragua = new WikipediaContentSection();
$div_wikipedia_Nicaragua->setTitleText('Nicaragua');
$div_wikipedia_Nicaragua->setTitleLink('https://en.wikipedia.org/wiki/Nicaragua');
$div_wikipedia_Nicaragua->content = <<<HTML
	<p>Nicaragua, officially the Republic of Nicaragua, is the largest country in Central America,
	bordered by Honduras to the north, the Caribbean to the east, Costa Rica to the south, and the Pacific Ocean to the west.</p>
	HTML;

$div_wikipedia_Politics_of_Nicaragua = new WikipediaContentSection();
$div_wikipedia_Politics_of_Nicaragua->setTitleText('Politics of Nicaragua');
$div_wikipedia_Politics_of_Nicaragua->setTitleLink('https://en.wikipedia.org/wiki/Politics_of_Nicaragua');
$div_wikipedia_Politics_of_Nicaragua->content = <<<HTML
	<p>Nicaragua is a presidential republic, in which the President of Nicaragua is both head of state and head of government,
	and there is a multi-party system.</p>
	HTML;

$div_wikipedia_Gender_equality_in_Nicaragua = new WikipediaContentSection();
$div_wikipedia_Gender_equality_in_Nicaragua->setTitleText('Gender equality in Nicaragua');
$div_wikipedia_Gender_equality_in_Nicaragua->setTitleLink('https://en.wikipedia.org/wiki/Gender_equality_in_Nicaragua');
$div_wikipedia_Gender_equality_in_Nicaragua->content = <<<HTML
	<p>When it comes to gender equality in Latin America, Nicaragua ranks high amongst the other countries in the region.
	When it came to global rankings regarding gender equality, the World Economic Forum ranked Nicaragua at number twelve in 2015,
	while in 2016 it ranked tenth, and in 2017 the country ranked sixth.</p>
	HTML;


$page->parent('world.html');
$page->template("stub");
$page->body($div_introduction);
$page->body('Country indices');

$page->body($div_wikipedia_Nicaragua);
$page->body($div_wikipedia_Politics_of_Nicaragua);
$page->body($div_wikipedia_Gender_equality_in_Nicaragua);
