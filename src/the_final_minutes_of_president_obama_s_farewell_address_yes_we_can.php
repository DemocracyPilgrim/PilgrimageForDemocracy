<?php
$page = new VideoPage();
$page->h1("The Final Minutes of President Obama's Farewell Address: Yes, we can.");
$page->tags("Video: Speech", "Barack Obama", "Teamwork", "President Saviour Syndrome");
$page->keywords("The Final Minutes of President Obama's Farewell Address: Yes, we can.");
$page->stars(3);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Excerpt from ${"Barack Obama"}'s final speech as President of the United States.
	These final sentences emphasize the important of $teamwork in building $democracy,
	and epitomize the very opposite of the ${"President as Saviour Syndrome"}.</p>
	HTML;



$div_youtube_The_Final_Minutes_of_President_Obama_s_Farewell_Address_Yes_we_can = new YoutubeContentSection();
$div_youtube_The_Final_Minutes_of_President_Obama_s_Farewell_Address_Yes_we_can->setTitleText("The Final Minutes of President Obama&#39;s Farewell Address: Yes, we can.");
$div_youtube_The_Final_Minutes_of_President_Obama_s_Farewell_Address_Yes_we_can->setTitleLink("https://www.youtube.com/watch?v=uWRmBjFxttc");
$div_youtube_The_Final_Minutes_of_President_Obama_s_Farewell_Address_Yes_we_can->content = <<<HTML
	<p>
	</p>
	HTML;

$div_Quotes = new ContentSection();
$div_Quotes->content = <<<HTML

	<blockquote><strong>Barack Obama</strong>:
	It has been the honor of my life to serve you.
	I won't stop.
	In fact, I will be right there with you as a citizen for all my remaining days.
	But for now, whether you are young or whether you're young at heart, I do have one final ask of you as your president,
	the same thing I asked when you took a chance on me eight years ago.
	<strong>I'm asking you to believe not in my ability to bring about change but in yours.</strong>
	</blockquote>

	<blockquote><strong>Barack Obama</strong>:
	I'm asking you to hold fast to that faith written into our founding documents,
	that idea whispered my slaves and abolitionists,
	that spirit sung by immigrants and homesteaders and those who March for justice,
	that Creed reaffirmed by those who planted flags from foreign battlefields to the surface of the Moon,
	a Creed at the core of every American whose story is not yet written:
	Yes, we can!
	</blockquote>
	HTML;



$page->parent('list_of_videos.html');

$page->body($div_introduction);
$page->body($div_youtube_The_Final_Minutes_of_President_Obama_s_Farewell_Address_Yes_we_can);

$page->body($div_Quotes);


