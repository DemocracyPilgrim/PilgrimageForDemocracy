<?php
$page = new OrganisationPage();
$page->h1("Campaign Legal Center");
$page->tags("Organisation", "Elections");
$page->keywords("Campaign Legal Center", "CLC");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

$r1 = $page->ref("https://campaignlegal.org/about", "About CLC");
// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;



$div_Campaign_Legal_Center_website = new WebsiteContentSection();
$div_Campaign_Legal_Center_website->setTitleText("Campaign Legal Center website ");
$div_Campaign_Legal_Center_website->setTitleLink("https://campaignlegal.org/");
$div_Campaign_Legal_Center_website->content = <<<HTML
	<p>Campaign Legal Center (CLC) is a nonpartisan organization that advocates for every eligible voter to meaningfully participate in the democratic process – no matter where they live, the color of their skin, or how much money they make. We use tactics such as litigation, policy advocacy and communications to make systemic impact at all levels of government.</p>

	<p>We are guided by the following principles:</p>

	<ul>
	<li>Our commitment will always be to democracy, not to political parties or electoral results.</li>
	<li>We respect the American people and their freedom to vote. We are fighting for every American to participate in and affect the political process regardless of race, economic status, or political affiliation. We advocate for every eligible voter, while recognizing that Black Americans and communities of color, in particular, have historically been and continue to be excluded from participating in the democratic process.</li>
	<li>We practice excellence and prioritize accuracy. We are thoughtful and proceed with care.</li>
	<li>We are committed to acting with respect and empathy both in our work and within our organization.</li>
	<li>We strive to be good partners and collaborators while staying true to our nonpartisan principles.</li>
	<li>We think about systemic impact when choosing what work to take. We work at any level of government when it introduces an innovative idea, could drive significant change at scale or sets an important precedent.</li>
	</ul>

	<p>Campaign Legal Center was founded in 2002 by its current president, Trevor Potter, a Republican former Commissioner of the Federal Election Commission.</p>


	</p>
	HTML;



$div_wikipedia_Campaign_Legal_Center = new WikipediaContentSection();
$div_wikipedia_Campaign_Legal_Center->setTitleText("Campaign Legal Center");
$div_wikipedia_Campaign_Legal_Center->setTitleLink("https://en.wikipedia.org/wiki/Campaign_Legal_Center");
$div_wikipedia_Campaign_Legal_Center->content = <<<HTML
	<p>Campaign Legal Center (CLC) is a nonprofit 501(c)(3) government watchdog group in the United States. CLC supports strong enforcement of United States campaign finance laws. Trevor Potter, former Republican chairman of the Federal Election Commission, is CLC's founding president.</p>
	HTML;


$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);



$page->body($div_Campaign_Legal_Center_website);
$page->related_tag("Campaign Legal Center");
$page->body($div_wikipedia_Campaign_Legal_Center);
