<?php
require_once 'section/duverger.php';

$page = new Page();
$page->h1('Duverger Syndrome');
$page->stars(1);
$page->tags("Elections: Duverger Syndrome", "Electoral System");
$page->keywords('Duverger Syndrome', 'Duverger', 'Duverger symptoms');

$page->preview( <<<HTML
	<p>The Duverger Syndrome is democracies' most critical illness.
	Both the causes and the fixes are known.
	Solutions must be applied as a matter of priority.</p>
	HTML );

$page->snp('description', "The Duverger Syndrome is democracies' most critical illness.");
//$page->snp('image', "/copyrighted/");


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>The Duverger Syndrome is democracies' most critical illness.
	Both the causes and the fixes are known.
	Solutions must be applied as a matter of priority.</p>

	<p>Most, if not all, of the symptoms of the Duverger Syndrome will be familiar to anybody reading these pages.
	What we need to realise is that these symptoms are intimately linked to known root causes.
	The proposed solutions will not fix everything, but they will help in making a noticeable difference.</p>

	<p>Read on, as we continue researching and developing this section of the website,
	about the symptoms, the causes as well as some historical examples in different countries of the Duverger Syndrome.</p>
	HTML;


$page->parent('electoral_system.html');

$page->body($div_introduction);

$page->body($div_comment_on_codeberg);



duverger_syndrome_article_menu($page);
