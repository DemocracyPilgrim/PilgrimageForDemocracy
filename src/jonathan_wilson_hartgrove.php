<?php
$page = new Page();
$page->h1("Jonathan Wilson-Hartgrove");
$page->alpha_sort("Wilson-Hartgrove, Jonathan");
$page->tags("Person");
$page->keywords("Jonathan Wilson-Hartgrove");
$page->stars(0);

$page->snp("description", "Christian writer and social activist.");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref('', '');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Jonathan Wilson-Hartgrove is a Christian writer, social justice activist and peace activist.
	He co-authored the book: "White Poverty: How Exposing Myths About Race and Class Can Reconstruct American Democracy".</p>
	HTML;

$div_wikipedia_Jonathan_Wilson_Hartgrove = new WikipediaContentSection();
$div_wikipedia_Jonathan_Wilson_Hartgrove->setTitleText("Jonathan Wilson-Hartgrove");
$div_wikipedia_Jonathan_Wilson_Hartgrove->setTitleLink("https://en.wikipedia.org/wiki/Jonathan_Wilson-Hartgrove");
$div_wikipedia_Jonathan_Wilson_Hartgrove->content = <<<HTML
	<p>Jonathan Wilson-Hartgrove is a Christian writer and preacher who has graduated both from Eastern University and Duke Divinity School.
	He associates himself with New Monasticism.
	Immediately prior to the 2003 invasion of Iraq, he and his wife, Leah, were members of a Christian peacemaking team
	that traveled to Iraq to communicate their message to Iraqis that not all American Christians were in favour of the coming Iraq War.</p>
	HTML;


$page->parent('list_of_people.html');
$page->template("stub");
$page->body($div_introduction);

$page->body('white_poverty_how_exposing_myths_about_race_and_class_can_reconstruct_american_democracy.html');

$page->body($div_wikipedia_Jonathan_Wilson_Hartgrove);
