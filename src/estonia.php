<?php
$page = new CountryPage('Estonia');
$page->h1('Estonia');
$page->tags("Country");
$page->keywords('Estonia');
$page->stars(0);

$page->snp('description', '1.37 million inhabitants.');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Estonia = new WikipediaContentSection();
$div_wikipedia_Estonia->setTitleText('Estonia');
$div_wikipedia_Estonia->setTitleLink('https://en.wikipedia.org/wiki/Estonia');
$div_wikipedia_Estonia->content = <<<HTML
	<p>The Republic of Estonia, is a country by the Baltic Sea in Northern Europe.
	It is bordered to the north by the Gulf of Finland across from Finland,
	to the west by the sea across from Sweden, to the south by Latvia, and to the east by Lake Peipus and Russia.</p>
	HTML;

$div_wikipedia_Constitution_of_Estonia = new WikipediaContentSection();
$div_wikipedia_Constitution_of_Estonia->setTitleText('Constitution of Estonia');
$div_wikipedia_Constitution_of_Estonia->setTitleLink('https://en.wikipedia.org/wiki/Constitution_of_Estonia');
$div_wikipedia_Constitution_of_Estonia->content = <<<HTML
	<p>The $constitution of Estonia is the fundamental law of the Republic of Estonia
	and establishes the state order as that of a democratic republic where the supreme power is vested in its citizens.
	The third Constitution of the Republic of Estonia was adopted by referendum in 1992.</p>
	HTML;

$div_wikipedia_Politics_of_Estonia = new WikipediaContentSection();
$div_wikipedia_Politics_of_Estonia->setTitleText('Politics of Estonia');
$div_wikipedia_Politics_of_Estonia->setTitleLink('https://en.wikipedia.org/wiki/Politics_of_Estonia');
$div_wikipedia_Politics_of_Estonia->content = <<<HTML
	<p>Politics in Estonia takes place in a framework of a parliamentary representative democratic republic,
	whereby the Prime Minister of Estonia is the head of government, and of a multi-party system.</p>
	HTML;


$page->parent('world.html');
$page->template("stub");
$page->body($div_introduction);
$page->body('Country indices');

$page->body($div_wikipedia_Estonia);
$page->body($div_wikipedia_Constitution_of_Estonia);
$page->body($div_wikipedia_Politics_of_Estonia);
