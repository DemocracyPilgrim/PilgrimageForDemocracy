<?php
$page = new CountryPage('El Salvador');
$page->h1('El Salvador');
$page->tags("Country");
$page->keywords('El Salvador');
$page->stars(0);

$page->snp('description', '6.6 million inhabitants.');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_El_Salvador = new WikipediaContentSection();
$div_wikipedia_El_Salvador->setTitleText('El Salvador');
$div_wikipedia_El_Salvador->setTitleLink('https://en.wikipedia.org/wiki/El_Salvador');
$div_wikipedia_El_Salvador->content = <<<HTML
	<p>The Republic of El Salvador, is a country in Central America.
	It is bordered on the northeast by Honduras, on the northwest by Guatemala, and on the south by the Pacific Ocean.
	El Salvador's capital and largest city is San Salvador.</p>
	HTML;

$div_wikipedia_Politics_of_El_Salvador = new WikipediaContentSection();
$div_wikipedia_Politics_of_El_Salvador->setTitleText('Politics of El Salvador');
$div_wikipedia_Politics_of_El_Salvador->setTitleLink('https://en.wikipedia.org/wiki/Politics_of_El_Salvador');
$div_wikipedia_Politics_of_El_Salvador->content = <<<HTML
	<p>Politics of El Salvador takes place in a framework of a presidential representative democratic republic,
	whereby the President of El Salvador is both head of state and head of government,
	and of an executive power is exercised by the government.</p>
	HTML;

$div_wikipedia_Human_rights_in_El_Salvador = new WikipediaContentSection();
$div_wikipedia_Human_rights_in_El_Salvador->setTitleText('Human rights in El Salvador');
$div_wikipedia_Human_rights_in_El_Salvador->setTitleLink('https://en.wikipedia.org/wiki/Human_rights_in_El_Salvador');
$div_wikipedia_Human_rights_in_El_Salvador->content = <<<HTML
	<p>There have been persistent concerns over human rights in El Salvador.
	Some of these date from the civil war of 1980–92.
	More recent concerns have been raised by Amnesty International and Human Rights Watch.
	They include women's rights, child labor, and unlawful killings and harassment of labor union members and other social activists.</p>
	HTML;


$page->parent('world.html');
$page->template("stub");
$page->body($div_introduction);
$page->body('Country indices');

$page->body($div_wikipedia_El_Salvador);
$page->body($div_wikipedia_Politics_of_El_Salvador);
$page->body($div_wikipedia_Human_rights_in_El_Salvador);
