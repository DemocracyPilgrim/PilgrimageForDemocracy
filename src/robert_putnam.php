<?php
$page = new PersonPage();
$page->h1("Robert Putnam");
$page->alpha_sort("Putnam, Putnam");
$page->tags("Person", "Society", "Society");
$page->keywords("Robert Putnam");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Robert_D_Putnam = new WikipediaContentSection();
$div_wikipedia_Robert_D_Putnam->setTitleText("Robert D Putnam");
$div_wikipedia_Robert_D_Putnam->setTitleLink("https://en.wikipedia.org/wiki/Robert_D._Putnam");
$div_wikipedia_Robert_D_Putnam->content = <<<HTML
	<p>Robert David Putnam (born January 9, 1941) is an American political scientist specializing in comparative politics. He is the Peter and Isabel Malkin Professor of Public Policy at the Harvard University John F. Kennedy School of Government.</p>
	HTML;


$page->parent('list_of_people.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Robert Putnam");
$page->body($div_wikipedia_Robert_D_Putnam);
