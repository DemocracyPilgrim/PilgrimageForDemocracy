<?php
$page = new CountryPage('South Africa');
$page->h1('South Africa');
$page->tags("Country");
$page->keywords('South Africa');
$page->stars(0);

$page->snp('description', '58 million inhabitants.');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>South Africa is a member of $BRICS.
	${'Nelson Mandela'} was the first president of South Africa, and the country's first black head of state.</p>
	HTML;

$div_wikipedia_South_Africa = new WikipediaContentSection();
$div_wikipedia_South_Africa->setTitleText('South Africa');
$div_wikipedia_South_Africa->setTitleLink('https://en.wikipedia.org/wiki/South_Africa');
$div_wikipedia_South_Africa->content = <<<HTML
	<p>South Africa is a middle power in international affairs;
	it maintains significant regional influence and is a member of both the Commonwealth of Nations and the G20.
	It is a developing country, ranking 109th on the Human Development Index, the 7th highest on the continent.</p>
	HTML;

$div_wikipedia_Freedom_Charter = new WikipediaContentSection();
$div_wikipedia_Freedom_Charter->setTitleText('Freedom Charter');
$div_wikipedia_Freedom_Charter->setTitleLink('https://en.wikipedia.org/wiki/Freedom_Charter');
$div_wikipedia_Freedom_Charter->content = <<<HTML
	<p>The Freedom Charter was the statement of core principles of the South African Congress Alliance,
	which consisted of the African National Congress (ANC) and its allies:
	the South African Indian Congress, the South African Congress of Democrats and the Coloured People's Congress.
	It is characterised by its opening demand, "The People Shall Govern!"</p>
	HTML;

$div_wikipedia_Human_rights_in_South_Africa = new WikipediaContentSection();
$div_wikipedia_Human_rights_in_South_Africa->setTitleText('Human rights in South Africa');
$div_wikipedia_Human_rights_in_South_Africa->setTitleLink('https://en.wikipedia.org/wiki/Human_rights_in_South_Africa');
$div_wikipedia_Human_rights_in_South_Africa->content = <<<HTML
	<p>Human rights in South Africa are protected under the constitution.
	The 1998 Human Rights report by Myles Nadioo noted that the government generally respected the rights of the citizens;
	however, there were concerns over the use of force by law enforcement, legal proceedings and discrimination.
	The Human Rights Commission is mandated by the South African Constitution and the Human Rights Commission Act of 1994,
	to monitor, both pro-actively and by way of complaints brought before it,
	violations of human rights and seeking redress for such violations.
	It also has an educational role.</p>
	HTML;

$div_wikipedia_Politics_of_South_Africa = new WikipediaContentSection();
$div_wikipedia_Politics_of_South_Africa->setTitleText('Politics of South Africa');
$div_wikipedia_Politics_of_South_Africa->setTitleLink('https://en.wikipedia.org/wiki/Politics_of_South_Africa');
$div_wikipedia_Politics_of_South_Africa->content = <<<HTML
	<p>The Republic of South Africa is a unitary parliamentary democratic republic.
	The President of South Africa serves both as head of state and as head of government.
	The President is elected by the National Assembly (the lower house of the South African Parliament)
	and must retain the confidence of the Assembly in order to remain in office.
	South Africans also elect provincial legislatures which govern each of the country's nine provinces.</p>

	<p>Since the end of apartheid in 1994 the African National Congress (ANC) has dominated South Africa's politics.
	The ANC is the ruling party in the national legislature, as well as in eight of the nine provinces (Western Cape is governed by the Democratic Alliance).</p>
	HTML;


$page->parent('world.html');
$page->template("stub");
$page->body($div_introduction);
$page->body('Country indices');

$page->body($div_wikipedia_South_Africa);
$page->body($div_wikipedia_Freedom_Charter);
$page->body($div_wikipedia_Human_rights_in_South_Africa);
$page->body($div_wikipedia_Politics_of_South_Africa);
