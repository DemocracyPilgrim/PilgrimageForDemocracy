<?php
$page = new Page();
$page->h1('List of people');
$page->tags("Topic portal");
$page->keywords("Person");
$page->stars(1);
$page->viewport_background('/free/list_of_people.png');

$page->preview( <<<HTML
	<p>
	</p>
	HTML );



$page->snp('description', "People on Democracy and Social Justice");
$page->snp('image', "/free/list_of_people.1200-630.png");



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>For each person that shall be featured below,
	we include only the information that is directly relevant to the project.
	In particular, we shall not cover personal information, biography, controversies, etc.
	unless such information has particular significance for the topics we aim to cover.
	To the extent that a person is notable enough to have a wikipedia article,
	we shall link to it:
	interested readers shall find more information there.</p>
	HTML;

$list = ListOfPeoplePages::WithTags("Person");
$print_list = $list->print();

$div_list = new ContentSection();
$div_list->content = <<<HTML
	<h3>list</h3>

	<p>Listed in alphabetical order by surname.</p>

	$print_list
	HTML;



$page->parent('lists.html');

$page->body($div_introduction);
$page->body($div_list);
