<?php
$page = new Page();
$page->h1("American Bar Association");
$page->tags("Organisation", "Institutions: Judiciary", "Artificial Intelligence", "USA", "Poverty", "Homelessness");
$page->keywords("American Bar Association", "ABA");
$page->stars(1);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

$r1 = $page->ref("https://www.courthousenews.com/ai-and-the-future-of-democracy-key-concerns-for-american-bar-association-leader/", "AI and the future of democracy key concerns for American Bar Association leader");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>As president of the American Bar Association, ${'Mary Smith'} created the Task Force for American Democracy and the Task Force on Law and ${'Artificial Intelligence'}.$r1</p>
	HTML;



$div_American_Bar_Association = new WebsiteContentSection();
$div_American_Bar_Association->setTitleText("American Bar Association ");
$div_American_Bar_Association->setTitleLink("https://www.americanbar.org/");
$div_American_Bar_Association->content = <<<HTML
	<p>The ABA was founded in 1878 on a commitment to set the legal and ethical foundation for the American nation.
	Today, it exists as a membership organization and stands committed to its mission of defending liberty and pursuing justice.</p>
	HTML;




$div_ABA_Task_Force_on_Law_and_Artificial_Intelligence = new WebsiteContentSection();
$div_ABA_Task_Force_on_Law_and_Artificial_Intelligence->setTitleText("ABA: Task Force on Law and Artificial Intelligence ");
$div_ABA_Task_Force_on_Law_and_Artificial_Intelligence->setTitleLink("https://www.americanbar.org/groups/centers_commissions/center-for-innovation/artificial-intelligence/");
$div_ABA_Task_Force_on_Law_and_Artificial_Intelligence->content = <<<HTML
	<p>The mission of the AI Task Force is to: (1) address the impact of AI on the legal profession and the practice of law, and related ethical implications;
	(2) provide insights on developing and using AI in a trustworthy and responsible manner; and (3) identify ways to address AI risks.</p>
	HTML;



$div_ABA_Commission_on_Immigration = new WebsiteContentSection();
$div_ABA_Commission_on_Immigration->setTitleText("ABA: Commission on Immigration ");
$div_ABA_Commission_on_Immigration->setTitleLink("https://www.americanbar.org/groups/public_interest/immigration/");
$div_ABA_Commission_on_Immigration->content = <<<HTML
	<p>The Commission directs the Association’s efforts to ensure fair treatment and full due process $rights for $immigrants,
	asylum-seekers, and refugees within the ${'United States'}.
	See also: <a href="https://www.americanbar.org/groups/public_interest/immigration/policy/">ABA: Immigration Law and Policies</a>.</p>
	HTML;



$div_ABA_Commission_on_Homelessness_Poverty = new WebsiteContentSection();
$div_ABA_Commission_on_Homelessness_Poverty->setTitleText("ABA: Commission on Homelessness & Poverty");
$div_ABA_Commission_on_Homelessness_Poverty->setTitleLink("https://www.americanbar.org/groups/public_interest/homelessness_poverty/");
$div_ABA_Commission_on_Homelessness_Poverty->content = <<<HTML
	<p>The Commission is committed to educating the bar and the public about homelessness and poverty
	and how the legal community and advocates can assist those in need.
	See also: <a href="https://www.americanbar.org/groups/public_interest/homelessness_poverty/ABAPolicyPositions/">ABA: Policy on Homelessness and Poverty</a>.</p>
	HTML;



$list_American_Bar_Association = ListOfPeoplePages::WithTags("American Bar Association");
$print_list_American_Bar_Association = $list_American_Bar_Association->print();

$div_list_American_Bar_Association = new ContentSection();
$div_list_American_Bar_Association->content = <<<HTML
	<h3>Related content</h3>

	$print_list_American_Bar_Association
	HTML;





$div_wikipedia_American_Bar_Association = new WikipediaContentSection();
$div_wikipedia_American_Bar_Association->setTitleText("American Bar Association");
$div_wikipedia_American_Bar_Association->setTitleLink("https://en.wikipedia.org/wiki/American_Bar_Association");
$div_wikipedia_American_Bar_Association->content = <<<HTML
	<p>The American Bar Association (ABA) is a voluntary bar association of lawyers and law students;
	it is not specific to any jurisdiction in the United States.
	Founded in 1878, the ABA's stated activities are the setting of academic standards for law schools,
	and the formulation of model ethical codes related to the legal profession.
	As of fiscal year 2017, the ABA had 194,000 dues-paying members, constituting approximately 14.4% of American attorneys.
	In 1979, half of all lawyers in the U.S. were members of the ABA.</p>
	HTML;


$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);

$page->body($div_American_Bar_Association);
$page->body($div_ABA_Task_Force_on_Law_and_Artificial_Intelligence);
$page->body($div_ABA_Commission_on_Immigration);
$page->body($div_ABA_Commission_on_Homelessness_Poverty);

$page->body($div_list_American_Bar_Association);

$page->body($div_wikipedia_American_Bar_Association);
