<?php
$page = new Page();
$page->h1("Trump v. United States (SCOTUS, 2024)");
$page->tags("Institutions: Judiciary", "SCOTUS Landmark Decision", "USA", "Elections", "Donald Trump", "Immunity");
$page->keywords("Trump v. United States");
$page->stars(2);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>"Trump v. United States" is a 2024 landmark decision by the ${'Supreme Court of the United States'}.</p>

	<p>The 2024 Supreme Court decision in Trump v. United States has been widely criticized
	for granting the President of the United States broad immunity for "official acts".
	The ruling prevents the judiciary from using presidential conversations or actions as evidence in indictments for unofficial criminal acts.
	This expansive immunity elevates the President to a position akin to an absolute monarch, effectively shielding them from accountability for any wrongdoing.
	The decision is on par with the infamous ${'Plessy v. Ferguson'} ruling,
	which was later overturned by ${'Brown v. Board of Education'} for its fundamental flaws.</p>
	HTML;


$div_Overturning_the_decision = new ContentSection();
$div_Overturning_the_decision->content = <<<HTML
	<h3>Overcoming <em>Trump v. United States</em>: A Difficult Path to Reversing Presidential Immunity</h3>


	<p>The 2024 Supreme Court decision in <em>Trump v. United States</em>, granting broad immunity to the President for "official acts,"
	has sparked significant debate about its implications for the balance of power and the rule of law.
	This decision, which effectively shields the President from accountability for potential misconduct,
	raises the question: how can such a precedent be reversed?</p>

	<p>Several pathways exist for potentially reversing the decision, but each presents significant challenges:</p>

	<p><strong>Constitutional Amendment</strong>: A constitutional amendment is the most direct route, but it faces a high hurdle.
	The process requires a two-thirds vote in both houses of Congress and ratification by three-quarters of the states.
	This path is extremely difficult, demanding a broad consensus that may be hard to achieve.</p>

	<p><strong>New Supreme Court Case</strong>: A new case could potentially overturn or limit the 2024 ruling.
	However, the Supreme Court must agree to hear the case (grant certiorari),
	a decision they are unlikely to make given their recent stance on presidential immunity.
	Furthermore, a new case might not directly overturn the previous ruling;
	instead, it could potentially limit its scope or apply it differently in a specific context.
	This approach would likely require a series of cases over time,
	similar to how ${'Plessy v. Ferguson'} was ultimately overturned in stages,
	by ${'Brown v. Board of Education'} in the domain of education,
	and by other cases in other domains like transportation, public accommodations, etc.</p>

	<p><strong>Congressional Legislation</strong>: Congress could attempt to pass legislation limiting presidential immunity.
	However, this route faces obstacles in the current political climate, as it requires a majority vote in both the House and the Senate,
	a feat potentially hindered by partisan divisions.
	Moreover, even if passed, such legislation would likely face legal challenges and ultimately end up before the Supreme Court,
	potentially leading to the same outcome.</p>

	<p>President Biden, recognizing the significant challenges, has reportedly opted for the constitutional amendment route as the most feasible path.
	The alternative pathways, whether through new litigation or legislation, would require either extensive time or significant changes in the political landscape,
	including potential reforms to the filibuster or the composition of the Supreme Court.</p>

	<p>The road to reversing <em>Trump v. United States</em> is a long and arduous one.
	It requires navigating a complex legal and political landscape,
	emphasizing the crucial need for sustained public dialogue and engagement to ensure the accountability of the highest office in the land.</p>
	HTML;




$div_Trump_v_United_States_Does_Former_President_Trump_Enjoy_Presidential_Immunity = new WebsiteContentSection();
$div_Trump_v_United_States_Does_Former_President_Trump_Enjoy_Presidential_Immunity->setTitleText("Trump v. United States: Does Former President Trump Enjoy Presidential Immunity from Criminal Prosecution for Conduct Alleged to Involve Official Acts During his Tenure in Office?");
$div_Trump_v_United_States_Does_Former_President_Trump_Enjoy_Presidential_Immunity->setTitleLink("https://constitution.congress.gov/browse/essay/intro.9-2-12/ALDE_00013898/");
$div_Trump_v_United_States_Does_Former_President_Trump_Enjoy_Presidential_Immunity->content = <<<HTML
	<p>Trump v. United States addressed, whether former President Donald J. Trump is immune from criminal prosecution
	for allegedly attempting to overturn the results of the 2020 election.
	A federal grand jury indicted President Trump on four criminal counts:
	(1) conspiracy to defraud the United States by overturning the election results,
	(2) conspiracy to obstruct an official proceeding (Congress’s certification of the electoral vote);
	(3) obstruction of, and attempt to obstruct, the certification of the electoral vote; and
	(4) conspiracy against the rights of persons to vote and to have their votes counted.
	President Trump asserted that the charges must be dismissed because he enjoys absolute immunity from criminal prosecution
	for all official acts he took as President—a category, he contended, that includes all acts alleged in the indictment.</p>

	<p>President Trump’s immunity claim presented an issue of first impression because no former President of the United States has been criminally charged,
	and thus no court has been required to address presidential immunity in the criminal context.
	The Supreme Court has held that sitting and former Presidents are immune from civil suits based on their official acts while in office,
	but that Presidents (sitting or former) do not enjoy civil immunity for private or unofficial conduct.
	Presidential immunity from civil suits for official acts does not have an express textual basis in the Constitution;
	it is derived from, among other things, concerns that civil suits would distract Presidents’ attention and, impede government functioning.</p>
	HTML;



$div_wikipedia_Trump_v_United_States_2024 = new WikipediaContentSection();
$div_wikipedia_Trump_v_United_States_2024->setTitleText("Trump v United States 2024");
$div_wikipedia_Trump_v_United_States_2024->setTitleLink("https://en.wikipedia.org/wiki/Trump_v._United_States_(2024)");
$div_wikipedia_Trump_v_United_States_2024->content = <<<HTML
	<p>Trump v. United States, 603 U.S., is a landmark decision of the Supreme Court of the United States in which the Court determined that
	presidential immunity from criminal prosecution presumptively extends to all of a president's "official acts"
	– with absolute immunity for official acts within an exclusive presidential authority that Congress cannot regulate
	such as the pardon, command of the military, execution of laws, or control of the executive branch.
	The case extends from an ongoing federal trial to determine whether Donald Trump, president at the time,
	and others engaged in election interference during the 2020 election, including events during the January 6, 2021, attack on the U.S. Capitol.
	It is the first time a case concerning criminal prosecution for alleged official acts of a president was brought before the Supreme Court.</p>

	<p>On July 1, 2024, the Court ruled in a 6–3 decision, that Trump had absolute immunity for acts he committed as president within his core constitutional purview,
	at least presumptive immunity for official acts within the outer perimeter of his official responsibility, and no immunity for unofficial acts.
	The decision also provides the same immunity to all presidents, including incumbent President Joe Biden.
	It declined to rule on the scope of immunity for some of Trump's acts alleged in his indictment,
	instead vacating the appellate decision and remanding the case to the district court for further proceedings.</p>
	HTML;


$page->parent('supreme_court_of_the_united_states.html');
$page->template("stub");
$page->body($div_introduction);
$page->body($div_Overturning_the_decision);

$page->body($div_Trump_v_United_States_Does_Former_President_Trump_Enjoy_Presidential_Immunity);

$page->body($div_wikipedia_Trump_v_United_States_2024);
