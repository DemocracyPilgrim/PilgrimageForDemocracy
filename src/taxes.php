<?php
$page = new Page();
$page->h1('Addendum E: Taxes');
$page->viewport_background('/free/taxes.png');
$page->keywords('Taxes', 'Tax');
$page->stars(1);
$page->tags("Living", "Institutions", "Topic portal");

$page->preview( <<<HTML
	<p></p>
	HTML );

$page->snp('description', "A necessary part of life");
$page->snp('image', "/free/taxes.1200-630.png");


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>It is said that the two things that one cannot avoid in life are: death, and taxes.</p>

	<p>In this section, we shall explore:
	the definition of taxes,
	the necessity of taxes,
	the amount of taxes,
	and the nature, good or bad, of taxes.</p>

	<p>The government needs the financial resources to perform its duties. Taxes are obviously necessary. However, not all taxes are created equal.</p>

	<p>The idea of fair taxes cannot be discussed without making reference to ${'public property'} and ${'private property'}.</p>

	<p>What criteria can we use to evaluate the usefulness of a tax,
	and its contribution to running a balanced society?
	What taxes are harmful and should be abolished?
	What taxes would, overall, be more beneficial?
	What proportions do harmful taxes take in a government's budget, as compared to more benign taxes?</p>

	<p>${'Mo Gawdat'}, former chief business officer for Google X, suggested to tax
	${'artificial intelligence'} businesses at 98% to support the people who are going to be displaced by AI,
	and to deal with the economic and social consequences of the technology.</p>
	HTML;


$div_Outline = new ContentSection();
$div_Outline->content = <<<HTML
	<h3>Outline</h3>

	<p>Besides defining $democracy, exploring taxes shall become another big part of the $Pilgrimage.
	Here are a few preliminary notes of the topics we plan to explore:</p>

	<ul>
		<li>Effect of taxes: depressing products and trades being taxed.</li>
		<li>Taxes and justice (ultra-wealthy people being taxed less than salaried people, notably in the $US).</li>
		<li>Tax acceptance: taxes perceived as fair would be better accepted by the population.</li>
		<li>Different types of taxes: taxes on labor and organic taxes.</li>
		<li>Taxing negative trades and activities: taxing pollution, taxing $advertising, etc.</li>
	</ul>
	HTML;


$div_Taxes_in_a_global_economy = new ContentSection();
$div_Taxes_in_a_global_economy->content = <<<HTML
	<h3>Taxes in a global economy</h3>

	<p>The ${'World Inequality Database'} is publishing the following article:</p>
	HTML;



$div_Paris_Conference_Calls_for_UN_Tax_Convention_to_Combat_Inequality = new WebsiteContentSection();
$div_Paris_Conference_Calls_for_UN_Tax_Convention_to_Combat_Inequality->setTitleText("Paris Conference Calls for UN Tax Convention to Combat Inequality");
$div_Paris_Conference_Calls_for_UN_Tax_Convention_to_Combat_Inequality->setTitleLink("https://wid.world/news-article/paris-conference-calls-for-un-tax-convention-to-combat-inequality/");
$div_Paris_Conference_Calls_for_UN_Tax_Convention_to_Combat_Inequality->content = <<<HTML
	<p>In November last year, countries at the UN have adopted by a landslide a resolution
	to begin the process of establishing a UN framework convention on tax,
	which has the potential to be the biggest shake-up of the international tax system in history.
	The framework convention would require global tax rules to be decided at the UN instead of the OECD,
	where these have been decided by a small club of rich countries for over sixty years.</p>
	HTML;



$div_rich_world_privilege = new WebsiteContentSection();
$div_rich_world_privilege->setTitleText("Rich world privilege: new study calls for a reform of the international monetary system");
$div_rich_world_privilege->setTitleLink("https://wid.world/news-article/the-us-exorbitant-privilege-has-become-a-rich-world-privilege-new-study-calls-for-a-reform-of-the-international-monetary-system/");
$div_rich_world_privilege->content = <<<HTML
	<p>Over the past decades the world has experienced a process of financial integration and capital liberalisation
	that has permitted an increase in foreign capital accumulation, especially since the 1990s.
	Gross foreign assets and liabilities have become larger almost everywhere,
	but particularly in rich countries, and foreign wealth has reached around 2 times the size of the global GDP.
	The unequal distribution of this external wealth, with the top 20 % richest countries
	capturing more than 90% of total foreign wealth, poses constraints on the poorest countries.</p>
	HTML;




$div_Tax_Justice_Now = new WebsiteContentSection();
$div_Tax_Justice_Now->setTitleText("Tax Justice Now ");
$div_Tax_Justice_Now->setTitleLink("https://www.taxjusticenow.org/");
$div_Tax_Justice_Now->content = <<<HTML
	<p>The goal of taxjusticenow.org (a ${'World Inequality Lab'} project) is to help citizens understand and participate in the tax debate.
	We see it as an essential companion to our 2019 book The Triumph of Injustice.
	On taxjusticenow.org, you can visualize how much each income group pays in taxes when we include all taxes
	(income taxes, corporate taxes, payroll taxes, consumption taxes, etc.)
	at all levels of government (federal, state, and local).
	Plus you can explore how changing existing taxes—such as increasing individual income tax rates—or creating
	new taxes—such as a progressive wealth tax or a value added tax—would affect tax revenue, tax progressivity, and inequality.
	We also model the tax plans of the main presidential candidates
	(and will update them as new or refined proposals come in).
	But you don't have to wait and can immediately build up your favorite tax plan
	and become an active participant in the debate.</p>
	HTML;





$page->parent('democracy_taxes.html');
$page->previous("education.html");
$page->next("environment.html");

$page->template("stub");
$page->body($div_introduction);
$page->body($div_Outline);

$page->related_tag("Tax");
$page->related_tag("Taxes");


$page->body($div_Taxes_in_a_global_economy);
$page->body($div_Paris_Conference_Calls_for_UN_Tax_Convention_to_Combat_Inequality);
$page->body($div_rich_world_privilege);
$page->body($div_Tax_Justice_Now);

$page->body('taxation_of_labor_vs_taxation_of_capital.html');
