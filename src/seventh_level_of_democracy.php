<?php
$page = new Page();
$page->h1("7: Seventh level of democracy — We, the Earthlings");
$page->stars(2);
$page->keywords('seventh level of democracy', 'seventh level');

$page->snp("description", "No country is located on another planet... Democracy and justice must be worldwide.");
$page->snp("image",       "/free/seventh_level_of_democracy.1200-630.png");
$page->viewport_background('/free/seventh_level_of_democracy.png');

$page->preview( <<<HTML
	<p>Democracy cannot last without a more peaceful and equitable world.
	Sustainable democracy demands a global vision.
	This level explores the responsibility of nations to foster fair, equitable and mutually supportive diplomatic relationships,
	solving economic colonialism and migration issues,
	while actively confronting threats from authoritarian regimes.</p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>$Democracy is defined not only by internal structures
	but also by the relationships $countries have with one another—both among democracies and between democracies and authoritarian regimes.</p>

	<p>Because $humanity remains bound to planet Earth, our shared existence as Earthlings transcends national boundaries.
	It is shortsighted to view any $country's $democratic (or non-democratic) regime in isolation from the global community.</p>

	<p>Just as the ${'Second Level of Democracy'} considers the relationship between individuals,
	so too must we consider the crucial relationships between countries.</p>
	HTML;




$div_Economic_colonialism = new ContentSection();
$div_Economic_colonialism->content = <<<HTML
	<h3>Economic colonialism</h3>

	<p>Economic colonialism, a modern manifestation of historical exploitative practices,
	occurs when powerful nations leverage their economic might to exert undue influence and control over less developed countries.
	This can take the form of unfair trade agreements, exploitative resource extraction, and the imposition of unfavorable economic policies.
	Trade between countries should adhere to democratic principles of fairness, including equitable terms of exchange, transparency in negotiations,
	and respect for the sovereignty of each nation.
	Genuine free trade requires mutual benefit, not the dominance of one nation over another.
	Failing to uphold these principles perpetuates economic inequalities
	and undermines the potential for genuine cooperation and mutual prosperity.</p>
	HTML;


$div_Immigration = new ContentSection();
$div_Immigration->content = <<<HTML
	<h3>Immigration</h3>

	<p>Addressing the global $migration crisis and its political ramifications requires a fundamental shift towards a more just and equitable $international order.
	This necessitates tackling the root causes—global inequality, conflict,
	and climate change—while simultaneously strengthening international cooperation to manage migration effectively and humanely.
	Such an order would empower nations to achieve economic and institutional self-sufficiency,
	reducing the pressures that drive mass migration and mitigating the humanitarian consequences,
	while establishing robust and humane border management systems.</p>
	HTML;


$div_Electoral_Achille_s_heel = new ContentSection();
$div_Electoral_Achille_s_heel->content = <<<HTML
	<h3>Electoral Achille's heel</h3>

	<p>Authoritarian regimes, such as $Russia and the ${"People's Republic of China"}, exploit weaknesses in democratic $electoral systems.
	This influence manifests in various ways.</p>

	<p>For example, electoral interference, often leveraging ${'social media'} and its largely unregulated $speech, is a significant concern.</p>

	<p>Another example is the way the $PRC waits for electoral alternance from one governing party to another
	in order to poach one of the few diplomatic partners that remain to the ${'Republic of China'}, $Taiwan.</p>

	<p>Strengthening electoral systems, the focus of the ${'fifth level of democracy'},
	is a critical countermeasure to such aggressive foreign influence.</p>
	HTML;





$div_social_networks = new ContentSection();
$div_social_networks->content = <<<HTML
	<h3>Social networks</h3>

	<p>Democracies face a significant disadvantage against autocracies in the realm of social media.
	in the name of ${'freedom of speech'},
	entities backed by authoritarian governments are free to operate within democracies,
	occupy space within social networks and operate their own social networks.
	The problem is that the converse is not true.</p>

	<p>The April 2024 US bill mandating TikTok divestment, while considered radical, highlights this asymmetry.
	The ${"People's Republic of China"}, in contrast, has long banned Western social media platforms
	(Facebook since 2009, subsequently Twitter, and then Instagram in2014).</p>

	<p>This situation directly relates to the ${'fifth level of democracy'} and the need for accurate, truthful information.
	These democratic principles can be leveraged to monitor and counter propaganda disseminated by authoritarian regimes within our media landscapes.</p>
	HTML;


$div_Taiwan = new ContentSection();
$div_Taiwan->content = <<<HTML
	<h3>The case of Taiwan</h3>

	<p>$Taiwan, officially the ${'Republic of China'}, is certainly the most isolated $democracy in the world.
	What does it say that $world democracies are prevented from having diplomatic relationships with the world's most impressive democratic success story?
	Despite push backs from the ${"People's Republic of China"},
	world democracies have taken notice.</p>
	HTML;



$list_all_related_topics = new ListOfPages();
$list_all_related_topics->add('international.html');
$print_list_all_related_topics = $list_all_related_topics->print();

$div_list_all_related_topics = new ContentSection();
$div_list_all_related_topics->content = <<<HTML
	$print_list_all_related_topics
	HTML;



$page->parent('democracy.html');
$page->previous('sixth_level_of_democracy.html');
$page->next('eighth_level_of_democracy.html');

$page->body($div_introduction);

$page->body($div_Economic_colonialism);
$page->body($div_Immigration);
$page->body($div_Electoral_Achille_s_heel);
$page->body($div_social_networks);
$page->body($div_Taiwan);

$page->body($div_list_all_related_topics);

