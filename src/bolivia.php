<?php
$page = new CountryPage('Bolivia');
$page->h1('Bolivia');
$page->tags("Country");
$page->keywords('Bolivia');
$page->stars(0);

$page->snp('description', '12 million inhabitants.');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Bolivia = new WikipediaContentSection();
$div_wikipedia_Bolivia->setTitleText('Bolivia');
$div_wikipedia_Bolivia->setTitleLink('https://en.wikipedia.org/wiki/Bolivia');
$div_wikipedia_Bolivia->content = <<<HTML
	<p>The sovereign state of Bolivia is a constitutionally unitary state, divided into nine departments.</p>
	HTML;

$div_wikipedia_Politics_of_Bolivia = new WikipediaContentSection();
$div_wikipedia_Politics_of_Bolivia->setTitleText('Politics of Bolivia');
$div_wikipedia_Politics_of_Bolivia->setTitleLink('https://en.wikipedia.org/wiki/Politics_of_Bolivia');
$div_wikipedia_Politics_of_Bolivia->content = <<<HTML
	<p>The politics of Bolivia takes place in a tree of a presidential representative democratic republic,
	whereby the president is head of state, head of government and head of a diverse multi-party system.</p>
	HTML;


$page->parent('world.html');
$page->template("stub");
$page->body($div_introduction);
$page->body('Country indices');

$page->body($div_wikipedia_Bolivia);
$page->body($div_wikipedia_Politics_of_Bolivia);
