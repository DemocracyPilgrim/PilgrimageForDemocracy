<?php
$page = new DocumentaryPage();
$page->h1("Defending Democracy Podcast");
$page->tags("Podcast", "Marc Elias", "Paige Moskowitz", "Democracy Docket");
$page->keywords("Defending Democracy Podcast");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;



$div_Defending_Democracy_Podcast = new WebsiteContentSection();
$div_Defending_Democracy_Podcast->setTitleText(" Defending Democracy Podcast");
$div_Defending_Democracy_Podcast->setTitleLink("https://www.democracydocket.com/defendingdemocracy/");
$div_Defending_Democracy_Podcast->content = <<<HTML
	<p>Marc Elias and Paige Moskowitz cover the latest in voting rights, redistricting and democracy.
	With help from special guests, Defending Democracy brings you the info you need to know this election year.
	Listen wherever you get your podcasts and on YouTube. New episodes drop every Friday.</p>
	HTML;



$div_youtube_Defending_Democracy_Complete_Podcast = new YoutubeContentSection();
$div_youtube_Defending_Democracy_Complete_Podcast->setTitleText("Defending Democracy — Complete Podcast");
$div_youtube_Defending_Democracy_Complete_Podcast->setTitleLink("https://www.youtube.com/playlist?list=PLyWBtUNDtcR3B58aThpuHxgtsmDJNETv1");
$div_youtube_Defending_Democracy_Complete_Podcast->content = <<<HTML
	<p>
	</p>
	HTML;



$page->parent('list_of_documentaries.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_Defending_Democracy_Podcast);
$page->body($div_youtube_Defending_Democracy_Complete_Podcast);

$page->related_tag("Defending Democracy Podcast");
