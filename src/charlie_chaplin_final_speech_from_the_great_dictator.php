<?php
$page = new VideoPage();
$page->h1("Charlie Chaplin: Final Speech from The Great Dictator");
$page->tags("Video: Speech", "Charlie Chaplin", "Humanity");
$page->stars(1);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;



$div_youtube_Charlie_Chaplin_Final_Speech_from_The_Great_Dictator = new YoutubeContentSection();
$div_youtube_Charlie_Chaplin_Final_Speech_from_The_Great_Dictator->setTitleText("Charlie Chaplin - Final Speech from The Great Dictator");
$div_youtube_Charlie_Chaplin_Final_Speech_from_The_Great_Dictator->setTitleLink("https://www.youtube.com/watch?v=J7GY1Xg6X20");
$div_youtube_Charlie_Chaplin_Final_Speech_from_The_Great_Dictator->content = <<<HTML
	<p>Original version.</p>
	HTML;




$div_youtube_A_Message_For_All_Of_Humanity_Charlie_Chaplin = new YoutubeContentSection();
$div_youtube_A_Message_For_All_Of_Humanity_Charlie_Chaplin->setTitleText("A Message For All Of Humanity - Charlie Chaplin");
$div_youtube_A_Message_For_All_Of_Humanity_Charlie_Chaplin->setTitleLink("https://www.youtube.com/watch?v=CsgaFKwUA6g");
$div_youtube_A_Message_For_All_Of_Humanity_Charlie_Chaplin->content = <<<HTML
	<p>Enhanced video montage of the same speech.</p>
	HTML;


$div_Quotes = new ContentSection();
$div_Quotes->content = <<<HTML
	<h3>Quotes</h3>

	<blockquote>
	In the 17th Chapter of St Luke it is written: “the Kingdom of God is within man” - not one man nor a group of men, but in all men! In you! You, the people have the power - the power to create machines. The power to create happiness! You, the people, have the power to make this life free and beautiful, to make this life a wonderful adventure.
	Then - in the name of democracy - let us use that power - let us all unite. Let us fight for a new world - a decent world that will give men a chance to work - that will give youth a future and old age a security.
	<blockquote>
	HTML;



$page->parent('list_of_videos.html');
$page->template("stub");
$page->body($div_introduction);

$page->body($div_youtube_Charlie_Chaplin_Final_Speech_from_The_Great_Dictator);
$page->body($div_youtube_A_Message_For_All_Of_Humanity_Charlie_Chaplin);
$page->body($div_Quotes);
$page->body('the_great_dictator.html');
