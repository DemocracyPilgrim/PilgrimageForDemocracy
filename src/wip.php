<?php
$page = new Page();
$page->h1("Addendum A: WIP");
$page->tags("Topic portal", "Society", "Institutions");
$page->keywords("WIP", "Democracy: WIP");
$page->stars(0);
$page->viewport_background("");

$page->snp("description", "A work in progress");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;


$div_Democracy_WIP = new ContentSection();
$div_Democracy_WIP->content = <<<HTML
	<h3>Democracy: WIP</h3>

	<p>Almost by definition, a $democracy is always a Work in Progress.</p>
	HTML;


$div_WIP = new ContentSection();
$div_WIP->content = <<<HTML
	<h3>WIP</h3>

	<p>Other Work in Progress material.</p>
	HTML;



$page->parent('democracy_wip.html');
$page->previous("humanity.html");
$page->next("teamwork.html");

$page->body($div_introduction);



$page->related_tag("Democracy: WIP", $div_Democracy_WIP);
$page->related_tag("WIP", $div_WIP);
