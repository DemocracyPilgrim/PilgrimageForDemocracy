<?php
$page = new Page();
$page->h1("Funding for elections");
$page->tags("Elections", "Electoral System");
$page->keywords("funding for elections");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

$r1 = $page->ref("https://electioninnovation.org/research/overview-of-private-funding-bans/", "Restrictions on Private Funding for Elections");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>This article is about the funding of the electoral process itself.
	It is different from ${'campaign finance'}.</p>

	<p>The ${'Center for Election Innovation & Research'} in conducting a research on the different states in the $USA
	which have legislation restricting private funding of elections. $r1</p>
	HTML;




$div_Restrictions_on_Private_Funding_for_Elections = new WebsiteContentSection();
$div_Restrictions_on_Private_Funding_for_Elections->setTitleText("Restrictions on Private Funding for Elections");
$div_Restrictions_on_Private_Funding_for_Elections->setTitleLink("https://electioninnovation.org/research/overview-of-private-funding-bans/");
$div_Restrictions_on_Private_Funding_for_Elections->content = <<<HTML
	<p>In 2020, state and local election officials were tasked with administering a federal election in the face of a global pandemic and record voter turnout.
	Typically, U.S. elections are funded through state and local budgets, with additional appropriations occasionally provided by the federal government.
	However, as election administration is chronically underfunded, the unprecedented circumstances of the pandemic required a significant investment of capital.
	To address this extraordinary need, several nonpartisan nonprofit organizations made funding available through private philanthropy for state and local election offices.</p>

	<p>Since the 2020 election, at least 28 states have enacted prohibitions or restrictions limiting certain agencies, officials, or government entities
	from receiving or spending private funding for the purpose of conducting elections.
	Each state has enacted such restrictions through statute except for Louisiana and Wisconsin, where voters approved new amendments to their state constitutions.
	At least three other states—Illinois, Minnesota, and New Jersey—currently have pending legislation that would establish private funding restrictions.
	Additionally, at least two other states—Kansas and Oklahoma—currently have pending legislation that would amend existing restrictions.</p>
	HTML;



$page->parent('electoral_system.html');
$page->template("stub");
$page->body($div_introduction);

$page->body($div_Restrictions_on_Private_Funding_for_Elections);
