<?php
$page = new Page();
$page->h1("Addendum B: Democracy — the Soloist and the Choir");
$page->stars(0);

//$page->snp("description", "");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p>Democracy needs both the visionary soloist and the harmonious choir.
	While individual brilliance can inspire, democracy thrives on collective action.
	We examine how the greatest achievements are often the result of teamwork and mutual support, rather than singular accomplishments.</p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Throughout history, particularly talented individuals, geniuses, great leaders or saints have inspired us, the masses.
	We look up to them, in awe. Sometimes we even follow them, try to learn from them and strive to emulate them.</p>

	<p>At the same time, most of the greatest achievements in human history were the fruit of collective endeavour.
	A Beethoven or a Victor Hugo might have singlehandedly written a great symphony or a great novel,
	but a Pharaoh might have been the instigator and the inspiration for a great pyramid being built,
	but he certainly did not build it on his own!</p>

	<p>$Democracy, by very definition, is a collective endeavour.
	Individual leaders or activist might inspire us by their courage, their profound insight or by their personal accomplishments.
	However, it is incumbent upon all of us, collectively, to create the best possible society.</p>
	HTML;


$div_individual_talent = new ContentSection();
$div_individual_talent->content = <<<HTML
	<h3>individual talent</h3>

	<p>The ${'first level of democracy'} features the individual: the rights and liberties of each and every one of us.
	Those individual rights are the most cherished aspects of democracies the world over.
	Indeed, our democratic liberal societies worship individuals.
	Popular talent shows like the highly successful "Got Talent" television franchise routinely put on a pedestal
	contestants who are individually talented.</p>

	<p>Arts is the main domain where a gifted person can shine.
	Great authors have written the greatest novels in world literature.
	Great composers have composed the most revered symphonies.</p>

	<p>At the same time, even the greatest of film director must rely on the talents of actors, costume designers,
	set designers, music composers, etc, etc.</p>
	HTML;


$div_Collective_achievements = new ContentSection();
$div_Collective_achievements->content = <<<HTML
	<h3>Collective achievements</h3>

	<p>The greatest of humanity's achievements have been the fruit of collective endeavour.
	A ruler might have ordered the construction of great monuments, like pyramids or the Great Wall of China,
	and yet they were build by teams of architects, engineers and countless workers.</p>

	<p>There has been many great scientists and researchers,
	but again the greatest technological achievements were the result of incredible teamwork,
	like all space missions (the Apollo Program...) or CERN's particle accelerators.</p>
	HTML;


$div_democracy_and_humanity = new ContentSection();
$div_democracy_and_humanity->content = <<<HTML
	<h3>Democracy and humanity</h3>

	<p>Democracy is necessarily a collective endeavour.
	Democratic successes can only be the result of a whole society's teamwork.</p>

	<p>If we want to solve some of humanity's most pressing challenges, like $hunger, $poverty, injustice and even wars,
	we need to set our minds on building on each other's contributions,
	and stand on each other's shoulders so that we can accomplish more, reach higher.</p>
	HTML;

$list_all_related_topics = new ListOfPages();
$list_all_related_topics->add('teamwork.html');
$print_list_all_related_topics = $list_all_related_topics->print();

$div_list_all_related_topics = new ContentSection();
$div_list_all_related_topics->content = <<<HTML
	$print_list_all_related_topics
	HTML;



$page->parent('democracy.html');
$page->previous('democracy_wip.html');
$page->next('saints_and_little_devils.html');

$page->template("stub");
$page->body($div_introduction);

$page->body($div_individual_talent);
$page->body($div_Collective_achievements);
$page->body('project/participate.html');

$page->body($div_democracy_and_humanity);

$page->body($div_list_all_related_topics);
