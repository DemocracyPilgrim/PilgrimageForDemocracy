<?php
$page = new PersonPage();
$page->h1("Yuval Noah Harari");
$page->alpha_sort("Harari, Yuval Noah");
$page->tags("Person", "Israel", "Information: Discourse", "Social Networks");
$page->keywords("Yuval Noah Harari");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Yuval Noah Harari is an Israeli historian and writer.</p>

	<p>He is the author of ${"Nexus: A Brief History of Information Networks from the Stone Age to AI"}.</p>
	HTML;



$div_uval_Noah_Harari_website = new WebsiteContentSection();
$div_uval_Noah_Harari_website->setTitleText("uval Noah Harari website ");
$div_uval_Noah_Harari_website->setTitleLink("https://www.ynharari.com/");
$div_uval_Noah_Harari_website->content = <<<HTML
	<p>Prof. Yuval Noah Harari is a historian, philosopher, and the bestselling author of Sapiens: A Brief History of Humankind, Homo Deus: A Brief History of Tomorrow, 21 Lessons for the 21st Century, the series Sapiens: A Graphic History and Unstoppable Us, and the NEXUS: A Brief History of Information Networks from the Stone Age to AI. His books have sold over 45 Million copies in 65 languages, and he is considered one of the world’s most influential public intellectuals today.</p>
	HTML;



$div_wikipedia_Yuval_Noah_Harari = new WikipediaContentSection();
$div_wikipedia_Yuval_Noah_Harari->setTitleText("Yuval Noah Harari");
$div_wikipedia_Yuval_Noah_Harari->setTitleLink("https://en.wikipedia.org/wiki/Yuval_Noah_Harari");
$div_wikipedia_Yuval_Noah_Harari->content = <<<HTML
	<p>Yuval Noah Harari is an Israeli medievalist, military historian, public intellectual, and writer. He currently serves as professor in the Department of History at the Hebrew University of Jerusalem. He is the author of the popular science bestsellers Sapiens: A Brief History of Humankind (2011), Homo Deus: A Brief History of Tomorrow (2016), and 21 Lessons for the 21st Century (2018). His writings examine free will, consciousness, intelligence, happiness, and suffering.</p>
	HTML;


$page->parent('list_of_people.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Yuval Noah Harari");

$page->body($div_uval_Noah_Harari_website);
$page->body($div_wikipedia_Yuval_Noah_Harari);
