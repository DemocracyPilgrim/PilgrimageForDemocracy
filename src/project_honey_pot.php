<?php
$page = new OrganisationPage();
$page->h1("Project Honey Pot");
$page->keywords("Project Honey Pot");
$page->viewport_background("");
$page->stars(0);
$page->tags("Organisation", "The Web Defenders", "Spam", "Justice");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;




$div_Project_Honey_Pot = new WebsiteContentSection();
$div_Project_Honey_Pot->setTitleText("Project Honey Pot ");
$div_Project_Honey_Pot->setTitleLink("https://www.projecthoneypot.org/");
$div_Project_Honey_Pot->content = <<<HTML
	<p>Project Honey Pot is the first and only distributed system for identifying spammers and the spambots they use to scrape addresses from your website. Using the Project Honey Pot system you can install addresses that are custom-tagged to the time and IP address of a visitor to your site. If one of these addresses begins receiving email we not only can tell that the messages are spam, but also the exact moment when the address was harvested and the IP address that gathered it.</p>
	HTML;



$div_wikipedia_Project_Honey_Pot = new WikipediaContentSection();
$div_wikipedia_Project_Honey_Pot->setTitleText("Project Honey Pot");
$div_wikipedia_Project_Honey_Pot->setTitleLink("https://en.wikipedia.org/wiki/Project_Honey_Pot");
$div_wikipedia_Project_Honey_Pot->content = <<<HTML
	<p>Project Honey Pot is a web-based honeypot network operated by Unspam Technologies, Inc. It uses software embedded in web sites. It collects information about the IP addresses used when harvesting e-mail addresses in spam, bulk mailing, and other e-mail fraud. The project solicits the donation of unused MX entries from domain owners.</p>
	HTML;


$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Project Honey Pot");
$page->body($div_Project_Honey_Pot);
$page->body($div_wikipedia_Project_Honey_Pot);
