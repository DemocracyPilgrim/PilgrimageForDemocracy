<?php
$page = new Page();
$page->h1("Hope");
$page->keywords("Hope");
$page->stars(0);

//$page->snp("description", "");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>How to share hope and love, for the world and for future generations?</p>
	HTML;


$page->parent('menu.html');
$page->template("stub");
$page->body($div_introduction);
