<?php
$page = new Page();
$page->h1("National Coalition Against Censorship");
$page->keywords("National Coalition Against Censorship", "NCAC");
$page->tags("Organisation", "Individual: Liberties");
$page->stars(0);

$page->snp("description", "Alliance of 50 American non-profit organizations against censorship");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );

$r1 = $page->ref('https://ncac.org/project/what-we-do', 'NCAC: What we do');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>The National Coalition Against Censorship a nonpartisan alliance of more than 50 $organisations dedicated to protecting ${'free speech'}.</p>

	<p>The NCAC has an initiative called "Kids right to read network"
	which fights book bans at the local level by providing legal support and resources to authors libraries or public schools.</p>
	HTML;


$div_NCAC_website = new WebsiteContentSection();
$div_NCAC_website->setTitleText("NCAC website ");
$div_NCAC_website->setTitleLink("https://ncac.org/");
$div_NCAC_website->content = <<<HTML
	<p>We envision an American society that understands, values, defends,
	and vigorously exercises free expression in a just, egalitarian, diverse, and inclusive democracy.</p>

	<p>Every generation of Americans faces new and significant challenges to free expression.
	For almost 50 years, NCAC has acted as a first responder to protect this freedom,
	which is both a fundamental human right and a keystone of democracy in the ever-changing American nation.
	We promote freedom of thought and inquiry and oppose censorship.
	When controversy occurs, we encourage and facilitate dialogue between divergent voices and perspectives,
	including those that have historically been silenced.$r1</p>
	HTML;



$div_wikipedia_National_Coalition_Against_Censorship = new WikipediaContentSection();
$div_wikipedia_National_Coalition_Against_Censorship->setTitleText("National Coalition Against Censorship");
$div_wikipedia_National_Coalition_Against_Censorship->setTitleLink("https://en.wikipedia.org/wiki/National_Coalition_Against_Censorship");
$div_wikipedia_National_Coalition_Against_Censorship->content = <<<HTML
	<p>The National Coalition Against Censorship (NCAC), founded in 1974,
	is an alliance of 50 American non-profit organizations, including literary, artistic, religious, educational, professional, labor, and civil liberties groups.
	NCAC is a New York–based organization with official 501(c)(3) status in the United States.
	The coalition seeks to defend freedom of thought, inquiry, and expression from censorship and threats of censorship
	through $education and outreach, and direct advocacy.
	NCAC assists individuals, community groups, and institutions with strategies and resources
	for resisting censorship and creating a climate hospitable to free expression.
	It also encourages the publicizing of cases of censorship and has a place to report instances of censorship on the organization's website.</p>
	HTML;


$page->parent('list_of_organisations.html');
$page->parent('censorship.html');
$page->template("stub");
$page->body($div_introduction);

$page->body($div_NCAC_website);

$page->body($div_wikipedia_National_Coalition_Against_Censorship);
