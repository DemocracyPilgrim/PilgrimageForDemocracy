<?php
$page = new Page();
$page->h1("Free Law Project");
$page->keywords("Free Law Project");
$page->tags("Institutions: Judiciary", "Organisation");
$page->stars(0);

$page->snp("description", "Nonprofit facilitating access to primary legal material.");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p>Nonprofit facilitating access to primary legal material.</p>
	HTML );

// $r1 = $page->ref('', '');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Free Law Project is a nonprofit facilitating access to primary legal material.</p>

	<p>(See also: $corruption, $justice, $institutions).
	HTML;



$div_Free_Law_Project_website = new WebsiteContentSection();
$div_Free_Law_Project_website->setTitleText("Free Law Project website ");
$div_Free_Law_Project_website->setTitleLink("https://free.law/");
$div_Free_Law_Project_website->content = <<<HTML
	<p>Started in 2010, Free Law Project is the leading 501(c)(3) nonprofit
	using technology, data, and advocacy to make the legal ecosystem more equitable and competitive.</p>

	<p>We do this by:</p>

	<ul>
		<li>Curating and providing free, public, and permanent access to primary legal materials</li>
		<li>Developing technology useful for legal research and innovation</li>
		<li>Fostering and supporting an open ecosystem for legal research</li>
		<li>Supporting academic research in the legal sector</li>
	<ul>
	</p>
	HTML;



$div_wikipedia_Free_Law_Project = new WikipediaContentSection();
$div_wikipedia_Free_Law_Project->setTitleText("Free Law Project");
$div_wikipedia_Free_Law_Project->setTitleLink("https://en.wikipedia.org/wiki/Free_Law_Project");
$div_wikipedia_Free_Law_Project->content = <<<HTML
	<p>Free Law Project is a United States federal 501(c)(3) Oakland-based nonprofit
	that provides free access to primary legal materials, develops legal research tools,
	and supports academic research on legal corpora.
	Free Law Project has several initiatives that collect and share legal information,
	including the largest collection of American oral argument audio,
	daily collection of new legal opinions from 200 United States courts and administrative bodies,
	the RECAP Project, which collects documents from PACER,
	and user-generated Supreme Court citation visualizations.
	Their data helped The Wall Street Journal expose 138 cases of conflict of interest cases regarding violations by federal judges.</p>
	HTML;


$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);

$page->body($div_Free_Law_Project_website);
$page->body('pacer_public_access_to_court_electronic_records.html');
$page->body('courtlistener.html');

$page->body($div_wikipedia_Free_Law_Project);
