<?php
$page = new Page();
$page->h1('Quantity of information');
$page->stars(0);
$page->tags("Media", "Information", "Social Networks");

//$page->snp('description', '');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Every single second, humanity produce online a staggering amount of information,
	in comments, likes, social media posts, pictures or videos.</p>

	<p>With the advent of ${'artificial intelligence'}, the trend will continue to accelerate.</p>

	<p>This is a problem because quantity outweighs quality by several orders of magnitude,
	even before we start worrying about orchestrated disinformation campaigns...</p>
	HTML;


$page->parent('media.html');
$page->body('information_overload.html');
$page->template("stub");
$page->body($div_introduction);


