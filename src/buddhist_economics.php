<?php
$page = new Page();
$page->h1("Buddhist economics");
$page->keywords("Buddhist economics");
$page->stars(0);
$page->tags("Fair Share");

//$page->snp("description", "");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Buddhist_economics = new WikipediaContentSection();
$div_wikipedia_Buddhist_economics->setTitleText("Buddhist economics");
$div_wikipedia_Buddhist_economics->setTitleLink("https://en.wikipedia.org/wiki/Buddhist_economics");
$div_wikipedia_Buddhist_economics->content = <<<HTML
	<p>Buddhist economics is a spiritual and philosophical approach to the study of economics.
	It examines the psychology of the human mind and the emotions that direct economic activity,
	in particular concepts such as anxiety, aspirations and self-actualization principles.
	In the view of its proponents, Buddhist economics aims to clear the confusion about what is harmful and what is beneficial
	in the range of human activities involving the production and consumption of goods and services,
	ultimately trying to make human beings ethically mature.
	The ideology's stated purpose is to "find a middle way between a purely mundane society and an immobile, conventional society."</p>
	HTML;


$page->parent('fair_share.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_wikipedia_Buddhist_economics);
