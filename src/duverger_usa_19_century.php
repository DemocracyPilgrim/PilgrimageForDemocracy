<?php
$page = new Page();
$page->h1('Duverger example: USA, 19th century');
$page->tags("USA", "Elections: Duverger Syndrome");
$page->stars(1);

//$page->snp('description', "");
//$page->snp('image', "/copyrighted/");

$page->preview( <<<HTML
	<p>The United States has not always had a two-party political system.
	It started during the 1836 presidential elections.</p>
	HTML );


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>The United States has not always had a two-party political system.
	It started during the 1836 presidential elections.</p>
	HTML;


$div_At_the_beginning = new ContentSection();
$div_At_the_beginning->content = <<<HTML
	<h3>At the beginning...</h3>

	<p>The first U.S. presidential  election in 1789 was very different
	from the way Americans do presidential elections today.
	In 1789 there were no official candidates.
	There  was no campaigning for the office.
	There were no political parties, no nominating conventions, nor primary elections.
	The entire election season was very short.
	The Electoral College was taken seriously rather than being treated as an antiquated formality.</p>

	<p>Also, there were no declared candidates.
	The generation of the Founding Fathers felt that running for office was uncouth
	and that acting like you wanted the  office was inappropriate.
	It was thought that the Electors would simply come together and  vote for who they thought was the best man for the job.
	Nobody would put their name forward and say, "I want to be president!"</p>

	<p>In the early 1790s, political parties were not political parties in the modern sense.
	They were  really just informal factions or schools of thought.
	Politicians may have been described as "Republicans" or a "Federalists",
	not because they formally joined that party.
	They simply were part of a community of like-minded people.</p>

	<p>During the 1792 presidential elections,
	George Washington did not really want to be elected for a second term,
	but he was finally compelled to stay.
	There were however some concerns about who would become his vice president.
	Thus, on October 16 1792, some prominent political figures from various states met in Philadelphia
	to discuss their choice for candidate for the vice presidency.
	That meeting was symbolic as it can be said it was the first caucus in American history.</p>
	HTML;


$div_1836_Presidential_Election = new ContentSection();
$div_1836_Presidential_Election->content = <<<HTML
	<h3>1836 Presidential Election</h3>

	<p>In contrast to the first few presidential elections,
	by 1836 a few organized factions and political parties had started to take shape.
	Until then, people who had been elected into the Electoral College would meet and debate among themselves,
	and then cast their votes for various political figures, with many "candidates" from the same party receiving votes from Electors.</p>

	<p>However, the 1836 election would change things forever.
	One political party, the Democratic Party, decided to conduct a form or primary,
	and put forward a single candidate, representing the whole Democratic faction.
	Meanwhile, their opponents, the Whig Party, a disorganized new faction,
	allowed their votes to be scattered amongst several of their representatives,
	with Whig electors often supporting a different candidate according to states.
	The result was a clear victory for the Democrats:</p>

	<table class="voting method">
		<tr>
			<th>Candidate</th>
			<th>Party</th>
			<th>Result</th>
			<th>Winner</th>
		</tr>
		<tr class="democratic">
			<td>Martin Van Buren</td>
			<td>Democratic Party</td>
			<td>50.83%</td>
			<td class="winner">&check;</td>
		</tr>
		<tr class="whig">
			<td>William Henry Harrison</td>
			<td>Whig Party</td>
			<td>36.63%</td>
			<td class="winner"></td>
		</tr>
		<tr class="whig">
			<td>Hugh Lawson White</td>
			<td>Whig Party</td>
			<td>9.72%</td>
			<td class="winner"></td>
		</tr>
		<tr class="whig">
			<td>Daniel Webster</td>
			<td>Whig Party</td>
			<td>2.74%</td>
			<td class="winner"></td>
		</tr>
		<tr class="whig">
			<td>Willie Person Mangum</td>
			<td>Whig Party</td>
			<td>0%</td>
			<td class="winner"></td>
		</tr>
	</table>
	HTML;

$div_1840_Presidential_Election = new ContentSection();
$div_1840_Presidential_Election->content = <<<HTML
	<h3>1840 Presidential Election</h3>

	<p>The take away lesson from the 1836 election was clear.
	Although it was not formulated as such back then, the use of ${'plurality voting'}
	in a single seat election (the presidency), meant that there could only be two viable candidates.
	The Whigs wanted their revenge from the previous presidential election,
	and this time, they had time to organise a national convention,
	which took place in Harrisburg, Pennsylvania, in December 1839.
	The party united all of their forces behind one single candidate: William Henry Harrison.</p>

	<p>The result speaks for itself:</p>

	<table class="voting method">
		<tr>
			<th>Candidate</th>
			<th>Party</th>
			<th>Result</th>
			<th>Winner</th>
		</tr>
		<tr class="whig">
			<td>William Henry Harrison</td>
			<td>Whig Party</td>
			<td>52.87%</td>
			<td class="winner">&check;</td>
		</tr>
		<tr class="democratic">
			<td>Martin Van Buren</td>
			<td>Democratic Party</td>
			<td>46.82%</td>
			<td class="winner"></td>
		</tr>
	</table>

	<p>Henceforth, all presidential elections in the US were mostly a choice between two opposing forces,
	two main parties.</p>

	<p>After the American Civil War, the political divide settled around the two party that will still know today:
	the Republican Party and the Democratic Party.</p>
	HTML;




$div_The_Rise_of_the_Two_Party_System = new WebsiteContentSection();
$div_The_Rise_of_the_Two_Party_System->setTitleText("US National Archives: The Rise of the Two-Party System: A Revolution in American Politics, 1824-1840");
$div_The_Rise_of_the_Two_Party_System->setTitleLink("https://www.archives.gov/legislative/resources/education/rise-of-the-two-party-system");
$div_The_Rise_of_the_Two_Party_System->content = <<<HTML
	<p>With these educational resources, students study the emergence of the two-party system in the United States between 1824 and 1840.
	The students work in collaborative groups to explore one of the most important,
	yet least understood, political changes in American history.
	The lesson draws information from the free eBook The Two-Party System: A Revolution in American Politics, 1824–1840.</p>
	HTML;





$div_wikipedia_1836_United_States_presidential_election = new WikipediaContentSection();
$div_wikipedia_1836_United_States_presidential_election->setTitleText("1836 United States presidential election");
$div_wikipedia_1836_United_States_presidential_election->setTitleLink("https://en.wikipedia.org/wiki/1836_United_States_presidential_election");
$div_wikipedia_1836_United_States_presidential_election->content = <<<HTML
	<p>The 1836 United States presidential election was the 13th quadrennial presidential election,
	held from Thursday, November 3 to Wednesday, December 7, 1836.
	In the third consecutive election victory for the Democratic Party,
	incumbent Vice President Martin Van Buren defeated four candidates fielded by the nascent Whig Party.</p>
	HTML;



$div_wikipedia_1840_United_States_presidential_election = new WikipediaContentSection();
$div_wikipedia_1840_United_States_presidential_election->setTitleText("1840 United States presidential election");
$div_wikipedia_1840_United_States_presidential_election->setTitleLink("https://en.wikipedia.org/wiki/1840_United_States_presidential_election");
$div_wikipedia_1840_United_States_presidential_election->content = <<<HTML
	<p>The 1840 United States presidential election was the 14th quadrennial presidential election,
	held from Friday, October 30 to Wednesday, December 2, 1840.
	Economic recovery from the Panic of 1837 was incomplete,
	and Whig nominee William Henry Harrison defeated incumbent President Martin Van Buren of the Democratic Party.
	The election marked the first of two Whig victories in presidential elections,
	but was the only one where they won a majority of the popular vote.
	This was the third rematch in American history, which would not occur again until 1892.</p>
	HTML;



$div_wikipedia_United_States_presidential_election = new WikipediaContentSection();
$div_wikipedia_United_States_presidential_election->setTitleText("United States presidential election");
$div_wikipedia_United_States_presidential_election->setTitleLink("https://en.wikipedia.org/wiki/United_States_presidential_election");
$div_wikipedia_United_States_presidential_election->content = <<<HTML
	<p>The election of the president and the vice president of the United States is an indirect election in which citizens of the United States
	who are registered to vote in one of the fifty U.S. states or in Washington, D.C.,
	cast ballots not directly for those offices, but instead for members of the Electoral College.</p>
	HTML;


$div_wikipedia_United_States_presidential_nominating_convention = new WikipediaContentSection();
$div_wikipedia_United_States_presidential_nominating_convention->setTitleText("United States presidential nominating convention");
$div_wikipedia_United_States_presidential_nominating_convention->setTitleLink("https://en.wikipedia.org/wiki/United_States_presidential_nominating_convention");
$div_wikipedia_United_States_presidential_nominating_convention->content = <<<HTML
	<p>A United States presidential nominating convention is a political convention held every four years in the United States
	by most of the political parties who will be fielding nominees in the upcoming U.S. presidential election.
	The formal purpose of such a convention is to select the party's nominee for popular election as President,
	as well as to adopt a statement of party principles and goals known as the party platform and adopt the rules for the party's activities,
	including the presidential nominating process for the next election cycle.</p>
	HTML;



$div_wikipedia_Political_parties_in_the_United_States = new WikipediaContentSection();
$div_wikipedia_Political_parties_in_the_United_States->setTitleText("Political parties in the United States");
$div_wikipedia_Political_parties_in_the_United_States->setTitleLink("https://en.wikipedia.org/wiki/Political_parties_in_the_United_States");
$div_wikipedia_Political_parties_in_the_United_States->content = <<<HTML
	<p>American electoral politics have been dominated by successive pairs of major political parties
	since shortly after the founding of the republic of the United States.
	Since the 1850s, the two largest political parties have been
	the Democratic Party and the Republican Party—which together have won every United States presidential election since 1852
	and controlled the United States Congress since at least 1856.</p>
	HTML;


$page->parent('duverger_syndrome.html');
$page->template("stub");
$page->body($div_introduction);

$page->body($div_At_the_beginning);

$page->body($div_1836_Presidential_Election);

$page->body($div_1840_Presidential_Election);

$page->body('duverger_syndrome.html');




$page->body($div_The_Rise_of_the_Two_Party_System);

$page->body($div_wikipedia_1836_United_States_presidential_election);
$page->body($div_wikipedia_1840_United_States_presidential_election);
$page->body($div_wikipedia_Political_parties_in_the_United_States);
$page->body($div_wikipedia_United_States_presidential_nominating_convention);
$page->body($div_wikipedia_United_States_presidential_election);
