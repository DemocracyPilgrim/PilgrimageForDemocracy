<?php
$page = new Page();
$page->h1("Social Capital");
$page->tags("Society");
$page->keywords("Social Capital");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Social_capital = new WikipediaContentSection();
$div_wikipedia_Social_capital->setTitleText("Social capital");
$div_wikipedia_Social_capital->setTitleLink("https://en.wikipedia.org/wiki/Social_capital");
$div_wikipedia_Social_capital->content = <<<HTML
	<p>
	</p>
	HTML;


$page->parent('society.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Social Capital");
$page->body($div_wikipedia_Social_capital);
