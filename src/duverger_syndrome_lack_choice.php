<?php
$page = new Page();
$page->h1('Duverger symptom 5: lack of choice, lack of good candidates');
$page->stars(0);
$page->tags("Elections: Duverger Syndrome");

$page->preview( <<<HTML
	<p>Why our electoral choice always seems to be limited to two less-than-ideal candidates...</p>
	HTML );

$r1 = $page->ref('https://thehill.com/homenews/senate/4124078-senate-gop-romney-winnowing-anti-trump-field/', 'Senate GOP rallies behind Romney call for winnowing anti-Trump field');

//$page->snp('description', "");
//$page->snp('image', "/copyrighted/");


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Many times, voters never get to vote for their favourite candidate,
	who, more often than not, drops out of the race for one reason or another before the general election.</p>

	<p>Because of the systemic failure of the electoral process, political parties must use strategy to remain competitive at the ballot box.
	It happens in every democratic countries at every election cycles.</p>

	<p>For the 2024 $US presidential election, the anti-Trump GOP establishment are already trying to prevent
	a multitude of anti-Trump republican candidates from running in the Republican primary
	in order to concentrate the party's efforts on one single candidates
	who, in their view, would have a better chance of winning the race against Joe Biden, the Democratic incumbent. $r1</p>

	<p>Later in that same race, one both $Trump and Biden had each locked in their primary nomination process,
	we saw the emergence of the "Double Haters", the part of the electorate who found both choices distasteful...</p>

	<p>In $Taiwan, the 2024 presidential election race started as a three-way race,
	between a DPP candidate, a KMT candidate and a centrist, ex-KMT candidate.
	In order for the KMT camp not to lose the election by splitting the vote, as it did in the year 2000,
	the KMT and the centrist candidate have entered into talks in order to unify their ticket,
	increasing the chance of winning, leaving the votes, again, with only two choices: DPP or KMT.</p>
	HTML;

$div_The_two_bad_apples_syndrome = new ContentSection();
$div_The_two_bad_apples_syndrome->content = <<<HTML
	<h3>The two bad apples syndrome</h3>

	<p>Elections after elections, for many voters it feels that they can only chose the least rotten of two bad apples.</p>

	<p>While many chose to vote for a third party candidate, for most it feels like a wasted vote.</p>

	<p>Certainly there are many people who could potentially be better candidates, true public servants who could unify the country...</p>

	<p>The reason why our choice always seems to be reduced to a couple of less than ideal candidates is systemic,
	as we shall see in ${"Duverger's Law"}.</p>
	HTML;


$page->parent('duverger_syndrome.html');
$page->parent('duverger_syndrome_negative_campaigning.html');

$page->template("stub");
$page->body($div_introduction);
$page->body($div_The_two_bad_apples_syndrome);

$page->body('duverger_syndrome_extremism.html');
