<?php
$page = new CountryPage('Israel');
$page->h1('Israel');
$page->tags("Country");
$page->keywords('Israel');
$page->stars(0);

$page->snp('description', '9.8 million inhabitants.');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Israel = new WikipediaContentSection();
$div_wikipedia_Israel->setTitleText('Israel');
$div_wikipedia_Israel->setTitleLink('https://en.wikipedia.org/wiki/Israel');
$div_wikipedia_Israel->content = <<<HTML
	<p>Israel is a country in West Asia.
	It is bordered by Lebanon to the north, by Syria to the northeast, by Jordan to the east,
	by the Red Sea to the south, by Egypt to the southwest, by the Mediterranean Sea to the west,
	and by the Palestinian territories – the West Bank along the east and the Gaza Strip along the southwest.</p>
	HTML;

$div_wikipedia_Basic_Law_Human_Dignity_and_Liberty = new WikipediaContentSection();
$div_wikipedia_Basic_Law_Human_Dignity_and_Liberty->setTitleText('Basic Law Human Dignity and Liberty');
$div_wikipedia_Basic_Law_Human_Dignity_and_Liberty->setTitleLink('https://en.wikipedia.org/wiki/Basic_Law:_Human_Dignity_and_Liberty');
$div_wikipedia_Basic_Law_Human_Dignity_and_Liberty->content = <<<HTML
	<p>Basic Law: Human Dignity and Liberty is a Basic Law in the State of Israel, enacted to protect the country's main human rights.
	It enjoys super-legal status, giving the Supreme Court the authority to disqualify any law contradicting it,
	as well as protection from Emergency Regulations.</p>
	HTML;

$div_wikipedia_Human_rights_in_Israel = new WikipediaContentSection();
$div_wikipedia_Human_rights_in_Israel->setTitleText('Human rights in Israel');
$div_wikipedia_Human_rights_in_Israel->setTitleLink('https://en.wikipedia.org/wiki/Human_rights_in_Israel');
$div_wikipedia_Human_rights_in_Israel->content = <<<HTML
	<p>Human rights in the State of Israel, both legally and in practice,
	have been evaluated by intergovernmental organizations, non-governmental organizations (NGOs) and human rights activists,
	often in the context of the Israeli–Palestinian conflict, the wider Arab–Israeli conflict and Israel internal politics.</p>
	HTML;


$page->parent('world.html');
$page->template("stub");
$page->body($div_introduction);
$page->body('Country indices');
$page->body('israel_palestine.html');

$page->related_tag("Israel");

$page->body($div_wikipedia_Israel);
$page->body($div_wikipedia_Basic_Law_Human_Dignity_and_Liberty);
$page->body($div_wikipedia_Human_rights_in_Israel);
