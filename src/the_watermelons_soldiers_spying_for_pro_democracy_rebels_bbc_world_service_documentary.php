<?php
$page = new DocumentaryPage();
$page->h1("The Watermelons: Soldiers spying for pro-democracy rebels - BBC World Service Documentary");
$page->viewport_background("");
$page->keywords("The Watermelons: Soldiers spying for pro-democracy rebels");
$page->stars(0);
$page->tags("Documentary", "Myanmar", "BBC World Service");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;





$div_The_Watermelons_Myanmar_s_military_moles = new WebsiteContentSection();
$div_The_Watermelons_Myanmar_s_military_moles->setTitleText("The Watermelons: Myanmar's military moles");
$div_The_Watermelons_Myanmar_s_military_moles->setTitleLink("https://www.bbc.co.uk/programmes/w3ct7hyq");
$div_The_Watermelons_Myanmar_s_military_moles->content = <<<HTML
	<p>BBC Eye reveals why military spies are prepared to risk everything. This program contains distressing content. The voices of all active Watermelons have been changed for security.</p>
	HTML;



$div_youtube_The_Watermelons_Soldiers_spying_for_pro_democracy_rebels_BBC_World_Service_Documentary = new YoutubeContentSection();
$div_youtube_The_Watermelons_Soldiers_spying_for_pro_democracy_rebels_BBC_World_Service_Documentary->setTitleText("The Watermelons: Soldiers spying for pro-democracy rebels - BBC World Service Documentary");
$div_youtube_The_Watermelons_Soldiers_spying_for_pro_democracy_rebels_BBC_World_Service_Documentary->setTitleLink("https://www.youtube.com/watch?v=w8cDs0FgHhc");
$div_youtube_The_Watermelons_Soldiers_spying_for_pro_democracy_rebels_BBC_World_Service_Documentary->content = <<<HTML
	<p>
	</p>
	HTML;



$page->parent('list_of_documentaries.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("The Watermelons: Soldiers spying for pro-democracy rebels - BBC World Service Documentary");
$page->body($div_The_Watermelons_Myanmar_s_military_moles);
$page->body($div_youtube_The_Watermelons_Soldiers_spying_for_pro_democracy_rebels_BBC_World_Service_Documentary);
