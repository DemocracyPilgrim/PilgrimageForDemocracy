<?php
$page = new CountryPage('Panama');
$page->h1('Panama');
$page->tags("Country");
$page->keywords('Panama');
$page->stars(0);

$page->snp('description', '4.3 million inhabitants.');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Panama = new WikipediaContentSection();
$div_wikipedia_Panama->setTitleText('Panama');
$div_wikipedia_Panama->setTitleLink('https://en.wikipedia.org/wiki/Panama');
$div_wikipedia_Panama->content = <<<HTML
	<p>Panama, officially the Republic of Panama, is a transcontinental country in Central America,
	spanning the southern tip of North America into the northern part of South America.
	It is bordered by Costa Rica to the west, Colombia to the southeast, the Caribbean Sea to the north,
	and the Pacific Ocean to the south.</p>
	HTML;

$div_wikipedia_Politics_of_Panama = new WikipediaContentSection();
$div_wikipedia_Politics_of_Panama->setTitleText('Politics of Panama');
$div_wikipedia_Politics_of_Panama->setTitleLink('https://en.wikipedia.org/wiki/Politics_of_Panama');
$div_wikipedia_Politics_of_Panama->content = <<<HTML
	<p>The politics of Panama take place in a framework of a presidential representative democratic republic with multi-party system,
	whereby the President of Panama is both head of state and head of government.
	The branches are according to Panama's Political Constitution of 1972,
	reformed by the Actos Reformatorios of 1978 and the Acto Constitucional of 1983,
	united in cooperation and limited through a system of checks and balances.</p>
	HTML;


$page->parent('world.html');
$page->template("stub");
$page->body($div_introduction);
$page->body('Country indices');

$page->body($div_wikipedia_Panama);
$page->body($div_wikipedia_Politics_of_Panama);
