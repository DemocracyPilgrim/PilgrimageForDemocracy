<?php
$page = new Page();
$page->h1("1: Individuals");
$page->tags("Topic portal");
$page->keywords("Individual", "individual", "individuals", "Individual: Rights", "Individual: Liberties", "Individual: Development");
$page->stars(0);
$page->viewport_background("/free/individuals.png");

$page->snp("description", "Individual Rights, Liberties, and personal development.");
$page->snp("image",       "/free/individuals.1200-630.png");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;



$list_Individual_Rights = ListOfPeoplePages::WithTags("Individual: Rights");
$print_list_Individual_Rights = $list_Individual_Rights->print();

$div_list_Individual_Rights = new ContentSection();
$div_list_Individual_Rights->content = <<<HTML
	<h3>Individual Rights</h3>

	$print_list_Individual_Rights
	HTML;


$list_Individual_Liberties = ListOfPeoplePages::WithTags("Individual: Liberties");
$print_list_Individual_Liberties = $list_Individual_Liberties->print();

$div_list_Individual_Liberties = new ContentSection();
$div_list_Individual_Liberties->content = <<<HTML
	<h3>Individual Liberties</h3>

	$print_list_Individual_Liberties
	HTML;


$list_Individual_Development = ListOfPeoplePages::WithTags("Individual: Development");
$print_list_Individual_Development = $list_Individual_Development->print();

$div_list_Individual_Development = new ContentSection();
$div_list_Individual_Development->content = <<<HTML
	<h3>Individual Development</h3>

	$print_list_Individual_Development
	HTML;



$page->parent('first_level_of_democracy.html');
$page->previous("dangers.html");
$page->next("society.html");
$page->template("stub");
$page->body($div_introduction);

$page->body($div_list_Individual_Rights);
$page->body($div_list_Individual_Liberties);
$page->body($div_list_Individual_Development);
