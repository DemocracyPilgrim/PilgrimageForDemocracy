<?php
$page = new BookPage();
$page->h1("Lucky Loser: How Donald Trump Squandered His Father’s Fortune and Created the Illusion of Success");
$page->tags("Book", "USA", "Donald Trump", "Susanne Craig", "Russ Buettner");
$page->keywords("Lucky Loser: How Donald Trump Squandered His Father’s Fortune and Created the Illusion of Success");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>"Lucky Loser: How Donald Trump Squandered His Father’s Fortune and Created the Illusion of Success"
	is a book by Russ Buettner and Susanne Craig about ${'Donald Trump'}.</p>
	HTML;


$page->parent('list_of_books.html');
$page->template("stub");
$page->body($div_introduction);
