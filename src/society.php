<?php
$page = new Page();
$page->h1("2: Society");
$page->viewport_background("/free/society.png");
$page->tags("Topic portal");
$page->keywords("Society", "society", "societies");
$page->stars(0);

$page->snp("description", "Living peacefully together in society.");
$page->snp("image",       "/free/society.1200-630.png");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Living together, in peace and harmony, despite all our $individual difference,
	is the hallmark of the ${'Second Level of Democracy'}.</p>
	HTML;

$list_Society = ListOfPeoplePages::WithTags("Society");
$print_list_Society = $list_Society->print();

$div_list_Society = new ContentSection();
$div_list_Society->content = <<<HTML
	<h3>Related content</h3>

	$print_list_Society
	HTML;



$div_wikipedia_Society = new WikipediaContentSection();
$div_wikipedia_Society->setTitleText("Society");
$div_wikipedia_Society->setTitleLink("https://en.wikipedia.org/wiki/Society");
$div_wikipedia_Society->content = <<<HTML
	<p>A society is a group of individuals involved in persistent social interaction or a large social group
	sharing the same spatial or social territory, typically subject to the same political authority and dominant cultural expectations.
	Societies are characterized by patterns of relationships (social relations) between individuals who share a distinctive culture and institutions;
	a given society may be described as the sum total of such relationships among its constituent members.</p>
	HTML;


$page->parent('second_level_of_democracy.html');
$page->previous("individuals.html");
$page->next("institutions.html");

$page->body($div_introduction);
$page->body($div_list_Society);


$page->body($div_wikipedia_Society);
