<?php
$page = new Page();
$page->h1("Long Walk to Freedom");
$page->tags("Book", "South Africa");
$page->keywords("Long Walk to Freedom");
$page->stars(0);

$page->snp("description", "A biography of Nelson Mandela");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref('', '');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Long Walk to Freedom is an autobiography of ${'Nelson Mandela'}, the first president of ${'South Africa'},
	written in collaboration with ${'Richard Stengel'}.</p>
	HTML;

$div_wikipedia_Long_Walk_to_Freedom = new WikipediaContentSection();
$div_wikipedia_Long_Walk_to_Freedom->setTitleText("Long Walk to Freedom");
$div_wikipedia_Long_Walk_to_Freedom->setTitleLink("https://en.wikipedia.org/wiki/Long_Walk_to_Freedom");
$div_wikipedia_Long_Walk_to_Freedom->content = <<<HTML
	<p>Long Walk to Freedom is an autobiography by South Africa's first democratically elected President Nelson Mandela,
	and it was first published in 1994 by Little Brown & Co.
	The book profiles his early life, coming of age, education and 27 years spent in prison.
	Under the apartheid government, Mandela was regarded as a terrorist
	and jailed on Robben Island for his role as a leader of the then-outlawed African National Congress (ANC)
	and its armed wing the Umkhonto We Sizwe.
	He later achieved international recognition for his leadership as president in rebuilding the country's once segregationist society.
	The last chapters of the book describe his political ascension and his belief that the struggle still continued against apartheid in South Africa.</p>
	HTML;


$page->parent('list_of_books.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_wikipedia_Long_Walk_to_Freedom);
