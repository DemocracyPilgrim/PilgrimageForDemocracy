<?php
$page = new Page();
$page->h1("No Democracy Lasts Forever: How the Constitution Threatens the United States");
$page->tags("Book", "USA", "Constitution", "Institutions: Legislative");
$page->keywords("No Democracy Lasts Forever", "No Democracy Lasts Forever: How the Constitution Threatens the United States");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>"No Democracy Lasts Forever: How the Constitution Threatens the United States" is a book by ${'Erwin Chemerinsky'}.</p>
	HTML;


$page->parent('list_of_books.html');
$page->template("stub");
$page->body($div_introduction);


