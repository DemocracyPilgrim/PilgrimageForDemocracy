<?php
$page = new Page();
$page->h1("Death and taxes");
$page->viewport_background("");
$page->keywords("Death and taxes");
$page->stars(0);
$page->tags("Taxes", "Death", "Benjamin Franklin");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Death_and_taxes_idiom = new WikipediaContentSection();
$div_wikipedia_Death_and_taxes_idiom->setTitleText("Death and taxes idiom");
$div_wikipedia_Death_and_taxes_idiom->setTitleLink("https://en.wikipedia.org/wiki/Death_and_taxes_(idiom)");
$div_wikipedia_Death_and_taxes_idiom->content = <<<HTML
	<p>"Death and taxes" is a phrase commonly referencing a famous quotation written by American statesman Benjamin Franklin:
	"Our new Constitution is now established, and has an appearance that promises permanency; but in this world nothing can be said to be certain, except death and taxes."</p>
	HTML;


$page->parent('democracy_taxes.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Death and taxes");
$page->body($div_wikipedia_Death_and_taxes_idiom);
