<?php
$page = new Page();
$page->h1("Marriage");
$page->tags("Society");
$page->keywords("Marriage");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$list_Marriage = ListOfPeoplePages::WithTags("Marriage");
$print_list_Marriage = $list_Marriage->print();

$div_list_Marriage = new ContentSection();
$div_list_Marriage->content = <<<HTML
	<h3>Related content</h3>

	$print_list_Marriage
	HTML;



$div_wikipedia_Marriage = new WikipediaContentSection();
$div_wikipedia_Marriage->setTitleText("Marriage");
$div_wikipedia_Marriage->setTitleLink("https://en.wikipedia.org/wiki/Marriage");
$div_wikipedia_Marriage->content = <<<HTML
	<p>Marriage, also called matrimony or wedlock, is a culturally and often legally recognised union between people called spouses.
	It establishes rights and obligations between them, as well as between them and their children (if any), and between them and their in-laws.
	It is nearly a cultural universal, but the definition of marriage varies between cultures and religions, and over time.
	Typically, it is an institution in which interpersonal relationships, usually sexual, are acknowledged or sanctioned.
	In some cultures, marriage is recommended or considered to be compulsory before pursuing sexual activity.
	A marriage ceremony is called a wedding, while a private marriage is sometimes called an elopement.</p>
	HTML;


$page->parent('society.html');
$page->template("stub");
$page->body($div_introduction);
$page->body($div_list_Marriage);


$page->body($div_wikipedia_Marriage);
