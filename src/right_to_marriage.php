<?php
$page = new Page();
$page->h1("Right to Marriage");
$page->tags("Individual: Rights", "Society", "Rights");
$page->keywords("Right to Marriage");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;



$div_wikipedia_Right_to_family_life = new WikipediaContentSection();
$div_wikipedia_Right_to_family_life->setTitleText("Right to family life");
$div_wikipedia_Right_to_family_life->setTitleLink("https://en.wikipedia.org/wiki/Right_to_family_life");
$div_wikipedia_Right_to_family_life->content = <<<HTML
	<p>The right to family life is the right of all individuals to have their established family life respected, and to have and maintain family relationships.
	This right is recognised in a variety of international human rights instruments, including Article 16 of the Universal Declaration of Human Rights,
	Article 23 of the International Covenant on Civil and Political Rights, and Article 8 of the European Convention on Human Rights.</p>
	HTML;



$page->parent('society.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_wikipedia_Right_to_family_life);
