<?php
$page = new Page();
$page->h1('Insidious political repression');
$page->keywords('insidious political repression');
$page->stars(0);

$page->snp('description', 'Direct repression that does not say its name.');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p>Direct but unacknowledged form of repression, retribution for unstated "crimes".</p>
	HTML );

$r1 = $page->ref('https://www.aljazeera.com/news/2023/8/30/academic-decline-why-are-university-professors-being-expelled-in-iran',
		'‘Academic decline‘: Why are university professors being expelled in Iran?');


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Insidious ${'political repression'} is a direct but unacknowledged form of repression.
	It is a form of punition or retribution but the connection between the supposed "crime"
	and the retribution is never stated, least of all to the victim.</p>
	HTML;

$div_Repression_in_Iran = new ContentSection();
$div_Repression_in_Iran->content = <<<HTML
	<h3>Repression in Iran</h3>

	<p>Protests began in $Iran in September 2022 after 22-year-old Mahsa Amini died in police custody
	following her arrest by the morality police for alleged non-compliance with a mandatory dress code for women.</p>

	<p>The Iranian authorities cracked down on the protests which subsided within a few months,
	but the authorities did not stop there.
	Many professors from prestigious universities were expelled,
	leaving everyone guessing to what extent their firing is related to the protests that shook Iran a few months prior.</p>

	<p>Ali Sharifi Zarchi is a high-profile professor of bioinformatics and artificial intelligence
	at Iran’s top institution of higher education, the Sharif University of Technology.
	In August 2023, he made public his expulsion,
	providing documents that belied the official reason for his sacking.</p>

	<p>Local media reported that more than 50 academics have been let go since President Ebrahim Raisi took office two years ago.</p>

	<p>Neither the number of academics being let go, nor the reasons for terminating their tenure have been confirmed by the authorities.</p>


	HTML;


$page->parent('political_repression.html');
$page->template("stub");
$page->body($div_introduction);
$page->body($div_Repression_in_Iran);
