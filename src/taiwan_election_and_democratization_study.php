<?php
$page = new Page();
$page->h1("Taiwan Election and Democratization Study (台灣選舉與民主化調查)");
$page->keywords("Taiwan Election and Democratization Study");
$page->tags("Organisation", "Taiwan");
$page->stars(0);

$page->snp("description", "Large-scale survey research project.");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p>$Taiwan's Election and Democratization Study (TEDS) is a continual large-scale survey research project
	supported and funded by the Department of Humanities and Social Science of the National  Science and Technology Council.</p>
	HTML );

$r1 = $page->ref('http://teds.nccu.edu.tw/intro/super_pages.php?ID=intro1', 'About TEDS: Origin and Mission');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>$Taiwan's Election and Democratization Study (TEDS) is a continual large-scale survey research project
	supported and funded by the Department of Humanities and Social Science of the National  Science and Technology Council.</p>
	HTML;




$div_Taiwan_Election_and_Democratization_Study = new WebsiteContentSection();
$div_Taiwan_Election_and_Democratization_Study->setTitleText("Taiwan Election and Democratization Study ");
$div_Taiwan_Election_and_Democratization_Study->setTitleLink("http://teds.nccu.edu.tw/");
$div_Taiwan_Election_and_Democratization_Study->content = <<<HTML
	<p>Taiwan’s Election and Democratization Study (abbreviated as TEDS)
	is a continual large-scale survey research project supported and funded by the Department of Humanities and Social Science(hereafter, DHSS)
	of the National  Science and Technology Council.
	Its chief purpose is to integrate several election-oriented face-to-face surveys in Taiwan
	in order to utilize scarce resources more efficiently and to further elevate the level of survey research.
	In June 2000, the fifth Executive Committee of the Social Science Research Center (hereafter, SSRC)
	of the Department of Humanities and Social Sciences of the NSC passed the Rules of Planning and Advancement of the TEDS Project,
	which set a new standard for inter-university cooperation in the administration of large-scale face-to-face surveys
	in the discipline of political science in Taiwan.
	The goal of the Rules is to facilitate the integration of several election- and democratization-related large-scale survey projects
	so as to pen the participation channels to the academic community and share the collected data sets with the public."
	In October 2000, the TEDS Planning and Advancement Committee (hereafter, the TEDS Committee)
	was organized. The TEDS Committee soon announced the launch of its first ever project:
	the National Survey of the 2001 Legislative Yuan Election (abbreviated as TEDS 2001)
	and openly called for the submission of add-on question items to the core TEDS questionnaire.
	The TEDS 2001 project thus blazed the trail for inter-university cooperation in the administration
	of large-scale face-to-face surveys in the discipline of political science in Taiwan.</p>
	HTML;



$page->parent('list_of_organisations.html');
$page->parent('political_science_in_taiwan.html');
$page->template("stub");
$page->body($div_introduction);
$page->body($div_Taiwan_Election_and_Democratization_Study);
