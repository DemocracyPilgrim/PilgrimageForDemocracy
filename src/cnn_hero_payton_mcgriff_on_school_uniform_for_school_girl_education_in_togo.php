<?php
$page = new VideoPage();
$page->h1("CNN Hero: Payton McGriff on school uniform for school girl education in Togo");
$page->tags("Video: News Story", "Payton McGriff", "Togo", "Style Her Empowered", "Humanity", "Education", "CNN Hero");
$page->stars(1);
$page->viewport_background("/free/cnn_hero_payton_mcgriff_on_school_uniform_for_school_girl_education_in_togo.png");

$page->snp("description", "Talent and resilience and resourcefulness is so equally distributed worldwide, but opportunity is not.");
$page->snp("image",       "/free/cnn_hero_payton_mcgriff_on_school_uniform_for_school_girl_education_in_togo.1200-630.png");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;





$div_In_Togo_these_school_uniforms_are_at_the_center_of_a_movement_offering_girls = new WebsiteContentSection();
$div_In_Togo_these_school_uniforms_are_at_the_center_of_a_movement_offering_girls->setTitleText(" In Togo, these school uniforms are at the center of a movement offering girls and women a chance to build better lives");
$div_In_Togo_these_school_uniforms_are_at_the_center_of_a_movement_offering_girls->setTitleLink("https://edition.cnn.com/2024/07/19/world/school-uniforms-africa-education-cnnheroes/index.html");
$div_In_Togo_these_school_uniforms_are_at_the_center_of_a_movement_offering_girls->content = <<<HTML
	<p>“It made all of the stories that you read in the book so real,” McGriff said. “Talent and resilience and resourcefulness is so equally distributed worldwide, but opportunity is not.”</p>
	<p>McGriff interviewed groups of girls about obstacles that made attending school difficult. They mentioned a lack of money and support, but when she asked about uniforms, the reaction was immediate.</p>

	<p>“Every girl stood up and raised her hand so high and, not only that, told a very expressive story about how she had been shamed out of school because she didn’t have her uniform,” she said. “I realized, ‘Okay, this is a place to start.’”</p>
	HTML;




$div_youtube_A_school_uniform_that_grows_ensures_African_girls_can_continue_their_education_for_years_to_come = new YoutubeContentSection();
$div_youtube_A_school_uniform_that_grows_ensures_African_girls_can_continue_their_education_for_years_to_come->setTitleText("A school uniform that &#39;grows&#39; ensures African girls can continue their education for years to come");
$div_youtube_A_school_uniform_that_grows_ensures_African_girls_can_continue_their_education_for_years_to_come->setTitleLink("https://www.youtube.com/watch?v=sj1Ubb-emm4");
$div_youtube_A_school_uniform_that_grows_ensures_African_girls_can_continue_their_education_for_years_to_come->content = <<<HTML
	<p>
	</p>
	HTML;



$page->parent('list_of_videos.html');
$page->template("stub");
$page->body($div_introduction);




$page->body($div_In_Togo_these_school_uniforms_are_at_the_center_of_a_movement_offering_girls);
$page->body($div_youtube_A_school_uniform_that_grows_ensures_African_girls_can_continue_their_education_for_years_to_come);
