<?php
require_once 'section/tax_renaissance.php';

$page = new Page();
$page->h1("The Tax Renaissance: A New Era for Justice and Sustainability");
$page->viewport_background("/free/tax_renaissance.png");
$page->keywords("Tax Renaissance", 'tax', 'taxes', 'taxation' );
$page->stars(2);
$page->tags("Taxes");

$page->snp("description", "It’s time for a Renaissance in the way we think about taxes.");
$page->snp("image",       "/free/tax_renaissance.1200-630.png");

$page->preview( <<<HTML
	<p>The old ways of taxation are failing us, harming our planet and creating social inequalities by putting the burden on workers.
	Organic Taxes present a revolutionary solution: a shift from taxing labor to taxing harm, incentivizing sustainability and justice.
	This innovative and powerful approach aims to create an economy where sustainable practices are rewarded and harmful behaviors are discouraged,
	for the benefit of all, not just a privileged few.</p>
	HTML );




// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Our current tax system is broken, rewarding what harms us and penalizing what builds a healthy future.
	It’s time for a Renaissance in the way we think about taxes.
	We must create a movement towards a more sustainable and just economy,
	addressing the very root causes of social inequalities and environmental destruction.
	We must make taxes work for us, not against us,
	and create a sustainable world where social justice, environmental well-being, and economic prosperity are inextricably linked.</p>
	HTML;




$page->parent('democracy_taxes.html');

$page->body($div_introduction);
tax_renaissance_menu($page);
