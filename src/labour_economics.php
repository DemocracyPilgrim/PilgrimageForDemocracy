<?php
$page = new Page();
$page->h1("Labour economics");
$page->tags("Living: Economy", "Economy");
$page->keywords("Labour economics");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Labour_economics = new WikipediaContentSection();
$div_wikipedia_Labour_economics->setTitleText("Labour economics");
$div_wikipedia_Labour_economics->setTitleLink("https://en.wikipedia.org/wiki/Labour_economics");
$div_wikipedia_Labour_economics->content = <<<HTML
	<p>Labour economics, or labor economics, seeks to understand the functioning and dynamics of the markets for wage labour. Labour is a commodity that is supplied by labourers, usually in exchange for a wage paid by demanding firms. Because these labourers exist as parts of a social, institutional, or political system, labour economics must also account for social, cultural and political variables.</p>
	HTML;


$page->parent('economy.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Labour economics");
$page->body($div_wikipedia_Labour_economics);
