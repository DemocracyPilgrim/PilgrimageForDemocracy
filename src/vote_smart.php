<?php
$page = new Page();
$page->h1('Vote Smart');
$page->tags("Elections", "Organisation");
$page->keywords('Vote Smart');
$page->stars(0);

$page->snp('description', 'Collecting information on issue positions, voting records, etc. of elected officials.');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;


$div_Vote_Smart_website = new WebsiteContentSection();
$div_Vote_Smart_website->setTitleText('Vote Smart website ');
$div_Vote_Smart_website->setTitleLink('https://justfacts.votesmart.org/');
$div_Vote_Smart_website->content = <<<HTML
	<p>Vote Smart's mission is to provide free, factual, unbiased information on candidates and elected officials to ALL Americans.</p>
	HTML;


$div_wikipedia_Vote_Smart = new WikipediaContentSection();
$div_wikipedia_Vote_Smart->setTitleText('Vote Smart');
$div_wikipedia_Vote_Smart->setTitleLink('https://en.wikipedia.org/wiki/Vote_Smart');
$div_wikipedia_Vote_Smart->content = <<<HTML
	<p>Vote Smart is a non-profit, non-partisan research organization that
	collects and distributes information on candidates for public office in the United States.
	It covers candidates and elected officials in six basic areas:
	background information, issue positions (via the Political Courage Test), voting records, campaign finances,
	interest group ratings, and speeches and public statements.</p>
	HTML;


$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);

$page->body($div_Vote_Smart_website);
$page->body($div_wikipedia_Vote_Smart);
