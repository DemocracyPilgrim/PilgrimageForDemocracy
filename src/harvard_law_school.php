<?php
$page = new OrganisationPage();
$page->h1("Harvard Law School");
$page->viewport_background("");
$page->keywords("Harvard Law School");
$page->stars(0);
$page->tags("Organisation", "Education", "Justice");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;



$div_Harvard_Law_School = new WebsiteContentSection();
$div_Harvard_Law_School->setTitleText("Harvard Law School ");
$div_Harvard_Law_School->setTitleLink("https://hls.harvard.edu/");
$div_Harvard_Law_School->content = <<<HTML
	<p>
	</p>
	HTML;



$div_wikipedia_Harvard_Law_School = new WikipediaContentSection();
$div_wikipedia_Harvard_Law_School->setTitleText("Harvard Law School");
$div_wikipedia_Harvard_Law_School->setTitleLink("https://en.wikipedia.org/wiki/Harvard_Law_School");
$div_wikipedia_Harvard_Law_School->content = <<<HTML
	<p>Harvard Law School (HLS) is the law school of Harvard University, a private research university in Cambridge, Massachusetts. Founded in 1817, Harvard Law School is the oldest law school in continuous operation in the United States.</p>
	HTML;


$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Harvard Law School");
$page->body($div_Harvard_Law_School);
$page->body($div_wikipedia_Harvard_Law_School);
