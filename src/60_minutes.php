<?php
$page = new Page();
$page->h1("60 Minutes");
$page->tags("Organisation", "Information: Media");
$page->keywords("60 Minutes");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;



$div_nb60_Minutes = new WebsiteContentSection();
$div_nb60_Minutes->setTitleText("60 Minutes");
$div_nb60_Minutes->setTitleLink("https://www.cbsnews.com/60-minutes/");
$div_nb60_Minutes->content = <<<HTML
	<p>60 Minutes official websites with all the latest episodes.</p>
	HTML;




$div_wikipedia_60_Minutes = new WikipediaContentSection();
$div_wikipedia_60_Minutes->setTitleText("60 Minutes");
$div_wikipedia_60_Minutes->setTitleLink("https://en.wikipedia.org/wiki/60_Minutes");
$div_wikipedia_60_Minutes->content = <<<HTML
	<p>60 Minutes is an American television news magazine broadcast on the CBS television network.
	Debuting in 1968, the program was created by Don Hewitt and Bill Leonard, who distinguished it from other news programs
	by using a unique style of reporter-centered investigation.
	In 2002, 60 Minutes was ranked number six on TV Guide's list of the "50 Greatest TV Shows of All Time",
	and in 2013, it was ranked number 24 on the magazine's list of the "60 Best Series of All Time".
	In 2023, Variety ranked 60 Minutes as the twentieth-greatest TV show of all time.
	The New York Times has called it "one of the most esteemed news magazines on American television".</p>
	HTML;


$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_nb60_Minutes);

$page->related_tag("60 Minutes");
$page->body($div_wikipedia_60_Minutes);
