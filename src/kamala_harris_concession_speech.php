<?php
$page = new Page();
$page->h1("Kamala Harris Concession Speech");
$page->tags("Elections", "Kamala Harris", "Concession Speech", "USA");
$page->keywords("Kamala Harris Concession Speech");
$page->stars(3);
$page->viewport_background("/free/kamala_harris_concession_speech.png");

$page->snp("description", "Kamala Harris could have been a great leader for the USA.");
$page->snp("image",       "/free/kamala_harris_concession_speech.1200-630.png");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>After her loss in the 2024 presidential election, ${"Kamala Harris"} delivered the ${"concession speech"} below.</p>

	<p>Unlike what her opponent did after the 2020 presidential elections, Kamala Harris gracefully conceded the election.
	She also encouraged her supporters to continue fighting for democracy and for all the issues that fuelled her campaign.</p>
	HTML;


$div_youtube_Kamala_Harris_Concession_Speech = new YoutubeContentSection();
$div_youtube_Kamala_Harris_Concession_Speech->setTitleText("Kamala Harris Concession Speech ");
$div_youtube_Kamala_Harris_Concession_Speech->setTitleLink("https://www.youtube.com/watch?v=L8JYi0mGSx8");


$div_Transcript = new ContentSection();
$div_Transcript->content = <<<HTML
	<h3>Transcript</h3>

	<blockquote>
	<p>Good afternoon. Good afternoon. Good afternoon, everyone, good afternoon. Good afternoon. Good afternoon.</p>

	<p>Thank you all. Thank you, thank you. Thank you, thank you. So let me say, and I love you back, and I love you back. So let me say, my heart is full today. My heart is full today, full of gratitude for the trust you have placed in me, full of love for our country and full of resolve.</p>

	<p>The outcome of this election is not what we wanted, not what we fought for, not what we voted for, but hear me when I say, hear me when I say, the light of America’s promise will always burn bright, as long as we never give up and as long as we keep fighting.</p>

	<p>To my beloved Doug and our family, I love you so very much. To President Biden and Dr Biden, thank you for your faith and support. To Governor Walz and the Walz family, I know your service to our nation will continue.</p>

	<p>And to my extraordinary team, to the volunteers who gave so much of themselves, to the poll workers and the local election officials. I thank you. I thank you all.</p>

	<p>I am so proud of the race we ran and the way we ran it. Over the 107 days of this campaign, we have been intentional about building community and building coalitions, bringing people together from every walk of life and background, united by love of country, with enthusiasm and joy in our fight for America’s future and we did it with the knowledge that we all have so much more in common than what separates us.</p>

	<p>Now, I know folks are feeling and experiencing a range of emotions right now. I get it, but we must accept the results of this election. Earlier today, I spoke with President-elect Trump and congratulated him on his victory. I also told him that we will help him and his team with their transition and that we will engage in a peaceful transfer of power.</p>

	<p>A fundamental principle of American democracy is that when we lose an election, we accept the results.</p>

	<p>That principle, as much as any other, distinguishes democracy from monarchy or tyranny, and anyone who seeks the public trust must honor it. At the same time, in our nation, we owe loyalty not to a president or a party, but to the constitution of the United States and loyalty to our conscience and to our God.</p>

	<p>My allegiance to all three is why I am here to say, while I concede this election, I do not concede the fight that fueled this campaign. The fight – the fight for freedom, for opportunity, for fairness and the dignity of all people. A fight for the ideals at the heart of our nation, the ideals that reflect America at our best. That is a fight I will never give up.</p>

	<p>I will never give up the fight for a future where Americans can pursue their dreams, ambitions and aspirations, where the women of America have the freedom to make decisions about their own body and not have their government telling them what do.</p>

	<p>We will never give up the fight to protect our schools and our streets from gun violence, and, America, we will never give up the fight for our democracy, for the rule of law, for equal justice and for the sacred idea that every one of us, no matter who we are or where we start out, has certain fundamental rights and freedoms that must be respected and upheld.</p>

	<p>We will continue to wage this fight in the voting booth, in the courts and in the public square, and we will also wage it in quieter ways, in how we live our lives, by treating one another with kindness and respect, by looking in the face of a stranger and seeing a neighbor, by always using our strength to lift people up to fight for the dignity that all people deserve.</p>

	<p>The fight for our freedom will take hard work, but like I always say, we like hard work. Hard work is good work. Hard work can be joyful work. And the fight for our country is always worth it. It is always worth it.</p>

	<p>To the young people who are watching, it is OK to feel sad and disappointed, but please know it’s going to be OK. On the campaign, I would often say, when we fight, we win.</p>

	<p>But here’s the thing – sometimes the fight takes a while. That doesn’t mean we won’t win, that doesn’t mean we won’t win. The important thing is, don’t ever give up. Don’t ever give up. Don’t ever stop trying to make the world a better place. You have power. You have power. And don’t you ever listen when anyone tells you something is impossible because it has never been done before.</p>

	<p>You have the capacity to do extraordinary good in the world.</p>

	<p>And so to everyone who is watching, do not despair. This is not a time to throw up our hands. This is a time to roll up our sleeves. This is a time to organize, to mobilize and to stay engaged for the sake of freedom and justice and the future that we all know we can build together.</p>

	<p>Look, many of you know, I started out as a prosecutor, and throughout my career, I saw people at some of the worst times in their lives, people who had suffered great harm and great pain, and yet found within themselves the strength and the courage and the resolve to take the stand, to take a stand, to fight for justice, to fight for themselves, to fight for others.</p>

	<p>So let their courage be our inspiration. Let their determination be our charge.</p>

	<p>And I’ll close with this: there’s an adage, an historian once called it a law of history, true of every society across the ages. The adage is: “Only when it is dark enough can you see the stars.”</p>

	<p>I know many people feel like we are entering a dark time, but for the benefit of us all, I hope that is not the case. But here’s the thing, America: if it is, let us fill the sky with the light of a brilliant, brilliant, billion of stars. The light, the light of optimism, of faith, of truth and service, and may that work guide us, even in the face of setbacks, toward the extraordinary promise of the United States of America.</p>

	<p>I thank you all, may God bless you and may God Bless the United States of America. I thank you all.</p>
	</blockquote>
	HTML;


$page->parent('elections.html');
$page->body($div_introduction);

$page->body($div_youtube_Kamala_Harris_Concession_Speech);
$page->body($div_Transcript);
