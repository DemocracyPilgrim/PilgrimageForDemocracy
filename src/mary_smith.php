<?php
$page = new Page();
$page->h1("Mary L. Smith");
$page->alpha_sort("Smith, Mary");
$page->tags("Person", "Lawyer", "Artificial Intelligence", "Justice", "USA");
$page->keywords("Mary Smith");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

$r1 = $page->ref("https://www.courthousenews.com/ai-and-the-future-of-democracy-key-concerns-for-american-bar-association-leader/", "AI and the future of democracy key concerns for American Bar Association leader");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Mary Smith is an American lawyer who served as the president of the ${'American Bar Association'},
	where she created the Task Force for American Democracy and the Task Force on Law and Artificial Intelligence. $r1</p>
	HTML;

$div_wikipedia_Mary_L_Smith = new WikipediaContentSection();
$div_wikipedia_Mary_L_Smith->setTitleText("Mary L Smith");
$div_wikipedia_Mary_L_Smith->setTitleLink("https://en.wikipedia.org/wiki/Mary_L._Smith");
$div_wikipedia_Mary_L_Smith->content = <<<HTML
	<p>Mary L. Smith (born August 28, 1962) is an American lawyer, senior executive, and civic leader in private and public sectors.
	She served as the CEO of the Indian Health Services from October 2015 to February 2017,
	a $6 billion national healthcare system with 15,000 employees, 26 hospitals and over 50 clinics.
	Prior to this, Smith served as Associate Counsel to the President and Associate Director of Policy Planning in the Clinton Administration,
	and as a senior trial attorney in the Department of Justice during the Obama Administration.
	In 2009, she was nominated by President Barack Obama to be the Assistant Attorney General for the Tax Division of the United States Department of Justice.
	However, she was never confirmed by the Senate, and the White House in 2010 decided not to renominate her to the post.
	In 2023, she was sworn in as president of the American Bar Association. She is the first Native American woman to serve in this role.</p>
	HTML;


$page->parent('list_of_people.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_wikipedia_Mary_L_Smith);
