<?php
$page = new CountryPage('Ethiopia');
$page->h1('Ethiopia');
$page->tags("Country");
$page->keywords('Ethiopia');
$page->stars(0);

$page->snp('description', '107 million inhabitants.');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Ethiopia may become a member of the $BRICS alliance.</p>
	HTML;

$div_wikipedia_Ethiopia = new WikipediaContentSection();
$div_wikipedia_Ethiopia->setTitleText('Ethiopia');
$div_wikipedia_Ethiopia->setTitleLink('https://en.wikipedia.org/wiki/Ethiopia');
$div_wikipedia_Ethiopia->content = <<<HTML
	<p>Ethiopia is a multi-ethnic state with over 80 different ethnic groups.
	Christianity is the most widely professed faith in the country,
	with significant minorities of the adherents of Islam and a small percentage to traditional faiths.</p>
	HTML;

$div_wikipedia_Politics_of_Ethiopia = new WikipediaContentSection();
$div_wikipedia_Politics_of_Ethiopia->setTitleText('Politics of Ethiopia');
$div_wikipedia_Politics_of_Ethiopia->setTitleLink('https://en.wikipedia.org/wiki/Politics_of_Ethiopia');
$div_wikipedia_Politics_of_Ethiopia->content = <<<HTML
	<p>The government is structured as a federal parliamentary republic with both a President and Prime Minister.
	The government is multicameralism with a house of representative and a council.</p>
	HTML;

$div_wikipedia_Human_rights_in_Ethiopia = new WikipediaContentSection();
$div_wikipedia_Human_rights_in_Ethiopia->setTitleText('Human rights in Ethiopia');
$div_wikipedia_Human_rights_in_Ethiopia->setTitleLink('https://en.wikipedia.org/wiki/Human_rights_in_Ethiopia');
$div_wikipedia_Human_rights_in_Ethiopia->content = <<<HTML
	<p>Reports of human rights violations within the country have been levied not only at the federal government of Ethiopa,
	but also by various militant groups and regional militias; including the Tigray People's Liberation Front.</p>
	HTML;

$div_wikipedia_War_crimes_in_the_Tigray_War = new WikipediaContentSection();
$div_wikipedia_War_crimes_in_the_Tigray_War->setTitleText('War crimes in the Tigray War');
$div_wikipedia_War_crimes_in_the_Tigray_War->setTitleLink('https://en.wikipedia.org/wiki/War_crimes_in_the_Tigray_War');
$div_wikipedia_War_crimes_in_the_Tigray_War->content = <<<HTML
	<p>All sides of the Tigray War have been repeatedly accused of committing war crimes since it began in November 2020.
	In particular, the Ethiopian federal government, the State of Eritrea, the Tigray People's Liberation Front (TPLF) and Amhara regional forces
	have been the subject of numerous reports of both war crimes and crimes against humanity.</p>
	HTML;


$page->parent('world.html');
$page->template("stub");
$page->body($div_introduction);
$page->body('Country indices');

$page->body($div_wikipedia_Ethiopia);
$page->body($div_wikipedia_Politics_of_Ethiopia);
$page->body($div_wikipedia_Human_rights_in_Ethiopia);
$page->body($div_wikipedia_War_crimes_in_the_Tigray_War);
