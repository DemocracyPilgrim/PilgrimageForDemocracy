<?php
$page = new Page();
$page->h1('Economic, social and cultural rights');
$page->keywords('civil rights', 'Civil Rights');
$page->tags("Individual: Rights", "Society", "Rights");
$page->stars(0);

//$page->snp('description', '');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;


$list_Rights = ListOfPeoplePages::WithTags("Rights");
$print_list_Rights = $list_Rights->print();

$div_list_Rights = new ContentSection();
$div_list_Rights->content = <<<HTML
	<h3>Related content</h3>

	$print_list_Rights
	HTML;



$div_wikipedia_Economic_social_and_cultural_rights = new WikipediaContentSection();
$div_wikipedia_Economic_social_and_cultural_rights->setTitleText('Economic social and cultural rights');
$div_wikipedia_Economic_social_and_cultural_rights->setTitleLink('https://en.wikipedia.org/wiki/Economic,_social_and_cultural_rights');
$div_wikipedia_Economic_social_and_cultural_rights->content = <<<HTML
	<p>Economic, social and cultural rights (ESCR) are socio-economic human rights,
	such as the right to education, right to housing, right to an adequate standard of living,
	right to health, victims' rights and the right to science and culture.</p>
	HTML;


$page->parent('individuals.html');
$page->template("stub");
$page->body($div_introduction);
$page->body($div_list_Rights);


$page->body($div_wikipedia_Economic_social_and_cultural_rights);
