<?php
$page = new VideoPage();
$page->h1("President Biden: Interview with Lawrence O'Donnell, January 2025");
$page->viewport_background("");
$page->keywords("President Biden: Interview with Lawrence O'Donnell, January 2025");
$page->stars(0);
$page->tags("Video: Interview", "Joe Biden", "Lawrence O'Donnell");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;



$div_youtube_Biden_shares_serious_concern_for_U_S_democracy_in_Oval_Office_interview_with_Lawrence_O_Donnell = new YoutubeContentSection();
$div_youtube_Biden_shares_serious_concern_for_U_S_democracy_in_Oval_Office_interview_with_Lawrence_O_Donnell->setTitleText("Biden shares &#39;serious concern&#39; for U.S. democracy in Oval Office interview with Lawrence O&#39;Donnell");
$div_youtube_Biden_shares_serious_concern_for_U_S_democracy_in_Oval_Office_interview_with_Lawrence_O_Donnell->setTitleLink("https://www.youtube.com/watch?v=uEBmJ-aAmBo");
$div_youtube_Biden_shares_serious_concern_for_U_S_democracy_in_Oval_Office_interview_with_Lawrence_O_Donnell->content = <<<HTML
	<p>
	</p>
	HTML;

$page->body($div_youtube_Biden_shares_serious_concern_for_U_S_democracy_in_Oval_Office_interview_with_Lawrence_O_Donnell);


$page->parent('list_of_videos.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("President Biden: Last interview with Lawrence O'Donnell");
