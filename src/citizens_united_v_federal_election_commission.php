<?php
$page = new Page();
$page->h1("Citizens United v. Federal Election Commission (2010)");
$page->tags("Institutions: Judiciary", "SCOTUS Landmark Decision", "Elections", "USA", "Electoral System");
$page->keywords("Citizens United v. Federal Election Commission");
$page->stars(1);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>"Citizens United v. Federal Election Commission" is a 2010 landmark decision by the ${'Supreme Court of the United States'}.</p>

	<p>This landmark decision removed restrictions on corporate and union spending in political campaigns,
	allowing unlimited spending by corporations and unions on elections.</p>

	<p>Critics argue that this decision has exacerbated the influence of money in politics,
	giving corporations and wealthy individuals disproportionate influence over elections, and further eroding the principle of "one person, one vote."</p>

	<p>Supporters of the decision argue that it protects free speech rights
	and ensures that corporations have the same rights as individuals to participate in political discourse.</p>

	<p>This decision remains in force, and its impact on the political landscape continues to be debated.
	It has given corporations and wealthy individuals a disproportionate influence on elections,
	undermining the principle of "one person, one vote" and contributing to political polarization.</p>
	HTML;

$div_wikipedia_Citizens_United_v_FEC = new WikipediaContentSection();
$div_wikipedia_Citizens_United_v_FEC->setTitleText("Citizens United v FEC");
$div_wikipedia_Citizens_United_v_FEC->setTitleLink("https://en.wikipedia.org/wiki/Citizens_United_v._FEC");
$div_wikipedia_Citizens_United_v_FEC->content = <<<HTML
	<p>Citizens United v. Federal Election Commission, 558 U.S. 310 (2010), is a landmark decision of the Supreme Court of the United States
	regarding campaign finance laws and free speech under the First Amendment to the U.S. Constitution.
	The court held 5–4 that the freedom of speech clause of the First Amendment prohibits the government
	from restricting independent expenditures for political campaigns by corporations, nonprofit organizations, labor unions, and other associations.</p>
	HTML;


$page->parent('supreme_court_of_the_united_states.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_wikipedia_Citizens_United_v_FEC);
