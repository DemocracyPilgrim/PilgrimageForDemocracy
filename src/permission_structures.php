<?php
$page = new Page();
$page->h1("Permission Structures");
$page->tags("Society", "Information: Discourse", "Barack Obama", "Donald Trump");
$page->keywords("Permission Structure", "permission structure");
$page->stars(2);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>A permission structure in politics refers to the informal and formal rules and norms that govern who gets to participate in political processes and decision-making.</p>
	HTML;


$div_Formal_institutions = new ContentSection();
$div_Formal_institutions->content = <<<HTML
	<h3>Formal Institutions</h3>

	<p> The legal framework that defines voting rights, eligibility for office, campaign finance laws, and how legislation is passed.</p>

	<p>For example, <strong>voter ID laws</strong> can be seen as a form of permission structure that potentially limits access to voting for certain groups of people.</p>

	<p><strong>Political parties</strong> act as permission structures, determining which candidates get nominated and how they are supported in elections.</p>
	HTML;



$div_Informal_Norms = new ContentSection();
$div_Informal_Norms->content = <<<HTML
	<h3>Informal Norms</h3>

	<p>Social expectations and unwritten rules that influence political behavior, such as the importance of consensus, the power of lobbyists,
	or the role of traditional political parties.</p>
	HTML;


$div_Access_to_Resources = new ContentSection();
$div_Access_to_Resources->content = <<<HTML
	<h3>Access to Resources</h3>

	<p>The distribution of wealth, education, and social capital, which can create disparities in political influence.</p>

	<p>For example, <strong>campaign finance laws</strong>:
	The rules governing campaign contributions and spending can create an uneven playing field for candidates,
	effectively granting more permission to those with greater resources.</p>

	<p>The ownership and control of <strong>media</strong> outlets can influence the information that reaches the public,
	creating a permission structure that favours certain narratives or perspectives.</p>
	HTML;



$div_Permission_to_vote_in_a_certain_way = new ContentSection();
$div_Permission_to_vote_in_a_certain_way->content = <<<HTML
	<h3>Permission to vote in a certain way</h3>

	<p>"The term 'permission structure' has been defined by a marketing executive as pushing 'the proper buttons that need to be pushed'
	to get people to purchase a product they otherwise would shun.</p>

	<p>By extension, it was used in politics, especially since Obama's 2008 presidential campaign, as a way to encourage people to vote a certain way
	by giving them the permission to do so.
	It was used during the 2008 election by the Obama Campaign to provide the moral permission for a segment of hesitant white voters
	to vote for the first African-American president.
	It is also applicable for legislators, to give them the proper excuses or political cover to vote for or against specific bills.</p>

	<p>During the 2024 US presidential campaign, extremely conservative Republicans like Dick Cheney openly expressing support for Kamala Harris
	provided a permission structure for a certain category of Republicans to vote for the Democratic candidate.</p>
	HTML;



$div_Negative_Permission_Structures = new ContentSection();
$div_Negative_Permission_Structures->content = <<<HTML
	<h3>Negative Permission Structures</h3>

	<p>Permission structures can work both ways.
	In a positive way, permission structures can encourage voters to align themselves with certain progressive movements for more social justice and inclusion.
	In a negative way, permission structures can provide the excuse for people to express their hidden demons,
	for example by making it socially acceptable to be openly racist, xenophobic, violent, self-centred, etc.</p>

	<p>The negative aspect of permission structures has most been obvious with ${'Donald Trump'}'s discourse and campaigning style.
	If a president can behave in such ways and proffer such insults and openly use xenophobic, racist or misogynistic rhetoric,
	it then makes it socially and politically acceptable for other candidates and their supporters to express similar traits.</p>
	HTML;


$div_Barack_Obama = new ContentSection();
$div_Barack_Obama->content = <<<HTML
	<h3>Barack Obama</h3>

	<p>Barack Obama often spoke about the need to "break down barriers" and "level the playing field" in areas like healthcare, education, and economic opportunity.
	This implicitly acknowledges the existence of power structures that limit access and opportunity for certain groups, and he advocated for changing those structures.</p>

	<p>He frequently emphasized the need to create "a more just and equitable society",
	empowering marginalized groups, by giving voice and power to those who have been historically marginalized.
	This speaks to the idea of expanding the permission structure to include more diverse voices and perspectives.</p>

	<p>He focused on policies that aimed to provide greater access to education, healthcare, and economic opportunity for underserved communities.
	This relates to the concept of changing the permission structure that governs access to resources and opportunities.</p>
	HTML;



$div_Donald_Trump = new ContentSection();
$div_Donald_Trump->content = <<<HTML
	<h3>Donald Trump</h3>

	<p>Challenging the existing power structure:
	Trump often used rhetoric that framed himself as an outsider challenging the "establishment" and "deep state."
	He argued that the existing political and bureaucratic structures were rigged against him and his supporters,
	implying a need to change the permission structure that governs power and influence.</p>

	<P>Trump often appealed to a sense of grievance and exclusion.
	He frequently spoke about "taking back our country" and "draining the swamp" of corruption in Washington.
	This resonated with voters who felt marginalized or excluded from the political process, suggesting that the existing permission structure was failing them.</p>

	<p>Trump embraced a populist approach.
	His rhetoric often focused on empowering "the people" against a perceived elite.
	This resonates with the idea of shifting the permission structure to give greater voice and power to ordinary citizens.</p>
	HTML;


$page->parent('society.html');
$page->body($div_introduction);
$page->body($div_Formal_institutions);
$page->body($div_Informal_Norms);
$page->body($div_Access_to_Resources);

$page->body($div_Permission_to_vote_in_a_certain_way);
$page->body($div_Negative_Permission_Structures);

$page->body($div_Barack_Obama);
$page->body($div_Donald_Trump);


$page->related_tag("Permission Structure");
