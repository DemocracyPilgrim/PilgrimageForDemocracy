<?php
$page = new PersonPage();
$page->h1("Robert Reich");
$page->alpha_sort("Reich, Robert");
$page->tags("Person", "USA", "Economy", "Living: Fair Income");
$page->keywords("Robert Reich");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;



$div_youtube_Robert_Reich = new YoutubeContentSection();
$div_youtube_Robert_Reich->setTitleText("Robert Reich YouTube channel");
$div_youtube_Robert_Reich->setTitleLink("https://www.youtube.com/@RBReich/featured");
$div_youtube_Robert_Reich->content = <<<HTML
	<p>
	</p>
	HTML;



$div_wikipedia_Robert_Reich = new WikipediaContentSection();
$div_wikipedia_Robert_Reich->setTitleText("Robert Reich");
$div_wikipedia_Robert_Reich->setTitleLink("https://en.wikipedia.org/wiki/Robert_Reich");
$div_wikipedia_Robert_Reich->content = <<<HTML
	<p>Robert Bernard Reich is an American professor, author, lawyer, and political commentator. He worked in the administrations of presidents Gerald Ford and Jimmy Carter, and served as Secretary of Labor from 1993 to 1997 in the cabinet of President Bill Clinton. He was also a member of President Barack Obama's economic transition advisory board.</p>
	HTML;


$page->parent('list_of_people.html');
$page->template("stub");
$page->body($div_introduction);

$page->body($div_youtube_Robert_Reich);


$page->related_tag("Robert Reich");
$page->body($div_wikipedia_Robert_Reich);
