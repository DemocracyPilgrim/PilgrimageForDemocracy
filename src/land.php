<?php
$page = new Page();
$page->h1("Land");
$page->viewport_background("");
$page->keywords("Land", "land");
$page->stars(0);
$page->tags("Environment");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Land = new WikipediaContentSection();
$div_wikipedia_Land->setTitleText("Land");
$div_wikipedia_Land->setTitleLink("https://en.wikipedia.org/wiki/Land");
$div_wikipedia_Land->content = <<<HTML
	<p>Land, also known as dry land, ground, or earth, is the solid terrestrial surface of Earth not submerged by the ocean or another body of water. It makes up 29.2% of Earth's surface and includes all continents and islands.</p>
	HTML;


$page->parent('environment.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Land");
$page->body($div_wikipedia_Land);
