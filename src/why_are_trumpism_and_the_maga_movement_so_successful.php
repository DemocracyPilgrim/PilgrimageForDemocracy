<?php
$page = new Page();
$page->h1("Why are Trumpism and the MAGA movement so successful?");
$page->tags("USA", "Donald Trump", "Donald Trump: Research", "WIP");
$page->keywords("Trumpism", "MAGA");
$page->stars(0);

$page->snp("description", "To what can we attribute the success of Trumpism and of the MAGA movement?");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>To most people who value $democracy, the rule of law as well as plain old human decency,
	the success of Donald $Trump in $American politics is baffling, to say the least.
	How could such a man as Trump win against Hillary Clinton in 2016?
	How could have the 2020 presidential race between Trump and Biden been so close?
	And, worst of all, how could the 2024 presidential rematch very well end up in Trump's return to the White House?
	How can there be so many polls showing Trump leading over Biden, given everything we know about the MAGA leader,
	his temperament, his lies, his crimes, his obvious malevolent narcissism?
	</p>
	HTML;


$div_Propagandist = new ContentSection();
$div_Propagandist->content = <<<HTML
	<h3>Propagandist</h3>

	<p>Donald Trump is an incredibly powerful messenger.
	He is a skilled propagandist.
	He picks a message and he repeats it over and over again:
	that's all it takes to be effective.
	It doesn't have to be smart or savvy or accurate.
	It just needs repetition.</p>
	HTML;


$list = new ListOfPages();
$list->add('donald_trump.html');
$list->add('the_despot_s_apprentice_donald_trump_s_attack_on_democracy.html');
$list->add('blowback_a_warning_to_save_democracy.html');
$list->add('duverger_usa_21_century.html');
$print_list = $list->print();

$div_list = new ContentSection();
$div_list->content = <<<HTML
	<h3>Related content</h3>

	$print_list
	HTML;

$div_wikipedia_Trumpism = new WikipediaContentSection();
$div_wikipedia_Trumpism->setTitleText("Trumpism");
$div_wikipedia_Trumpism->setTitleLink("https://en.wikipedia.org/wiki/Trumpism");
$div_wikipedia_Trumpism->content = <<<HTML
	<p>Trumpism is an authoritarian political movement that follows the political ideologies associated with Donald Trump and his political base.
	Scholars and historians describe $Trumpism as a movement that incorporates a wide range of right-wing ideologies
	such as right-wing populism, national conservatism, neo-nationalism, and neo-fascism.
	Trumpist rhetoric heavily features anti-immigrant, xenophobic, nativist, and racist attacks against minority groups.
	Other identified aspects include conspiracist, isolationist, Christian nationalist, protectionist, anti-feminist, and anti-LGBT beliefs.</p>
	HTML;



$page->parent('american_fascism.html');
$page->template("stub");
$page->body($div_introduction);

$page->body($div_Propagandist);

$page->related_tag("MAGA", $div_list);

$page->body($div_wikipedia_Trumpism);
