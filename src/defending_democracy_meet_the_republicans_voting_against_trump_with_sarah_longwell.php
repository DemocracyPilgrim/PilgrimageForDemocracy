<?php
$page = new VideoPage();
$page->h1("Defending Democracy: Meet the Republicans Voting Against Trump with Sarah Longwell");
$page->tags("Video: Interview", "Defending Democracy Podcast", "Sarah Longwell", "Marc Elias", "Republican Party Evolution", "Donald Trump");
$page->keywords("Defending Democracy: Meet the Republicans Voting Against Trump with Sarah Longwell");
$page->stars(1);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;


$div_youtube_Meet_the_Republicans_Voting_Against_Trump_with_Sarah_Longwell = new YoutubeContentSection();
$div_youtube_Meet_the_Republicans_Voting_Against_Trump_with_Sarah_Longwell->setTitleText("Meet the Republicans Voting Against Trump with Sarah Longwell");
$div_youtube_Meet_the_Republicans_Voting_Against_Trump_with_Sarah_Longwell->setTitleLink("https://www.youtube.com/watch?v=snx72dt8KZY");
$div_youtube_Meet_the_Republicans_Voting_Against_Trump_with_Sarah_Longwell->content = <<<HTML
	<p>Sarah Longwell, founder of Republican Voters Against Trump, wants you to know that not all Republicans support former President Donald Trump. She discusses with Marc Elias fractures in the GOP, how Trump is a threat to democracy and conservative ideals and what Democratic voters need to understand about modern Republican voters.</p>
	HTML;



$page->parent('list_of_videos.html');
$page->template("stub");
$page->body($div_introduction);

$page->body($div_youtube_Meet_the_Republicans_Voting_Against_Trump_with_Sarah_Longwell);
