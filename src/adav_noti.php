<?php
$page = new PersonPage();
$page->h1("Adav Noti");
$page->alpha_sort("Noti, Adav");
$page->tags("Person", "Elections", "Campaign Legal Center");
$page->keywords("Adav Noti");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

$r1 = $page->ref("https://campaignlegal.org/staff/adav-noti", "Adav Noti, CLC Executive Director");
// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Adav Noti is an $American $lawyer, and the executive director of the ${'Campaign Legal Center'}. $r1</p>
	HTML;




$page->parent('list_of_people.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Adav Noti");
