<?php
$page = new Page();
$page->h1('Denis Mukwege');
$page->alpha_sort("Mukwege, Denis");
$page->tags("Person");
$page->keywords('Denis Mukwege');
$page->stars(0);

//$page->snp('description', '');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Denis_Mukwege = new WikipediaContentSection();
$div_wikipedia_Denis_Mukwege->setTitleText('Denis Mukwege');
$div_wikipedia_Denis_Mukwege->setTitleLink('https://en.wikipedia.org/wiki/Denis_Mukwege');
$div_wikipedia_Denis_Mukwege->content = <<<HTML
	<p>Denis Mukwege is a Congolese gynecologist and Pentecostal pastor.
	He founded and works in Panzi Hospital in Bukavu,
	where he specializes in the treatment of women who have been raped by armed rebels.
	In 2018, Mukwege and Iraqi Yazidi human rights activist Nadia Murad were jointly awarded the Nobel Peace Prize
	for "their efforts to end the use of sexual violence as a weapon of war and armed conflict".</p>
	HTML;


$page->parent('list_of_people.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_wikipedia_Denis_Mukwege);
