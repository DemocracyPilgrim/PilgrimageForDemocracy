<?php
$page = new Page();
$page->h1("Social Media and Election Integrity");
$page->viewport_background("");
$page->keywords("Social Media and Election Integrity");
$page->stars(0);
$page->tags("Information: Social Networks", "Social Networks", "The Bad Web", "Algorithms", "Tech Policy Press");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>See the warning (article below) by ${'Tech Policy Press'}, regarding the ${'social networks'}' use of $algorithms.</p>
	HTML;



$div_We_Worked_On_Election_Integrity_At_Meta_The_EU_And_All_Democracies_Need_to = new WebsiteContentSection();
$div_We_Worked_On_Election_Integrity_At_Meta_The_EU_And_All_Democracies_Need_to->setTitleText("We Worked On Election Integrity At Meta. The EU – And All Democracies – Need to Fix the Feed Before It’s Too Late");
$div_We_Worked_On_Election_Integrity_At_Meta_The_EU_And_All_Democracies_Need_to->setTitleLink("https://www.techpolicy.press/we-worked-on-election-integrity-at-meta-the-eu-and-all-democracies-need-to-fix-the-feed-before-its-too-late/");
$div_We_Worked_On_Election_Integrity_At_Meta_The_EU_And_All_Democracies_Need_to->content = <<<HTML
	<p>The key driver of election disinformation – and the key aspect of social media platforms being exploited by bad actors – are the algorithms. Algorithmic systems are how social media platforms determine what content a user will see. The basic components of recommendation systems on large online platforms are similar – they shape the content that is recommended, what shows up in your feeds and searches, and how advertisements are delivered. In lay person's terms, you don’t control so much of what you see on your “page,” despite claims to the contrary. The social media company does.</p>
	HTML;




$div_wikipedia_Social_media_use_in_politics = new WikipediaContentSection();
$div_wikipedia_Social_media_use_in_politics->setTitleText("Social media use in politics");
$div_wikipedia_Social_media_use_in_politics->setTitleLink("https://en.wikipedia.org/wiki/Social_media_use_in_politics");
$div_wikipedia_Social_media_use_in_politics->content = <<<HTML
	<p>Social media use in politics refers to the use of online social media platforms in political processes and activities. Political processes and activities include all activities that pertain to the governance of a country or area. This includes political organization, global politics, political corruption, political parties, and political values. The media's primary duty is to present us with information and alert us when events occur. This information may affect what we think and the actions we take. The media can also place pressure on the government to act by signaling a need for intervention or showing that citizens want change.</p>
	HTML;


$page->parent('social_networks.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Social Media and Election Integrity");

$page->body($div_We_Worked_On_Election_Integrity_At_Meta_The_EU_And_All_Democracies_Need_to);

$page->body($div_wikipedia_Social_media_use_in_politics);
