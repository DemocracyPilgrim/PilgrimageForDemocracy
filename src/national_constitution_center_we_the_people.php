<?php
$page = new DocumentaryPage();
$page->h1("National Constitution Center: We the People");
$page->tags("Podcast", "USA", "National Constitution Center", "Information: Media");
$page->keywords("National Constitution Center: We the People", "Podcast: We the People");
$page->stars(0);
$page->viewport_background("/free/national_constitution_center_we_the_people.png");

$page->snp("description", "Podcast hosted by the National Constitution Center.");
$page->snp("image",       "/free/national_constitution_center_we_the_people.1200-630.png");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>"We the People" is a weekly podcast by the ${'National Constitution Center'};</p>
	HTML;



$div_We_the_People = new WebsiteContentSection();
$div_We_the_People->setTitleText("We the People");
$div_We_the_People->setTitleLink("https://constitutioncenter.org/news-debate/podcasts");
$div_We_the_People->content = <<<HTML
	<p>A weekly show of constitutional debate hosted by National Constitution Center President and CEO Jeffrey Rosen where listeners can hear the best arguments on all sides of the constitutional issues at the center of American life.</p>
	HTML;



$page->parent('list_of_documentaries.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_We_the_People);

$page->related_tag("Podcast: We the People");
