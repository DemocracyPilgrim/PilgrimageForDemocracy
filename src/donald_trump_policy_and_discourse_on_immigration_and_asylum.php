<?php
$page = new Page();
$page->h1("Donald Trump's policy and discourse on immigration and asylum");
$page->tags("Humanity", "Asylum Seeker", "Immigration", "Racism", "USA", "Donald Trump");
$page->keywords("Donald Trump policy and discourse on immigration and asylum policy");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

$r1 = $page->ref("https://www.aclu.org/press-releases/federal-court-blocks-new-trump-asylum-restrictions", "Federal Court Blocks New Trump Asylum Restrictions");
$r2 = $page->ref("https://afsc.org/news/how-trump-making-it-harder-asylum-seekers", "How Trump is making it harder for asylum seekers");
$r3 = $page->ref("https://www.hhrjournal.org/2020/08/eliminating-asylum-the-effects-of-trump-administration-policies/", "Eliminating Asylum: The Effects of Trump Administration Policies");
$r4 = $page->ref("https://www.amnesty.org/en/latest/news/2020/11/trumps-heritage-immigration-asylum-policy/", "Rebuilding from the ashes, Trump’s heritage on immigration and asylum policy");
$r5 = $page->ref("https://apnews.com/article/politics-mexico-costa-rica-donald-trump-1a5a89459fb0b61f04f8500789b9b221", "US to limit asylum to migrants who pass through a 3rd nation");
$r6 = $page->ref("https://www.cfr.org/backgrounder/how-does-us-refugee-system-work-trump-biden-afghanistan", "How Does the U.S. Refugee System Work?");
$r7 = $page->ref("https://www.hrw.org/news/2020/12/11/trump-administrations-final-insult-and-injury-refugees", "The Trump Administration’s Final Insult and Injury to Refugees");
$r8 = $page->ref("https://www.nbcnews.com/politics/supreme-court/supreme-court-ends-dispute-trump-era-asylum-seeker-policy-rcna77389", "Supreme Court ends dispute over Trump-era asylum-seeker policy");
$r9 = $page->ref("https://immigrationequality.org/legal/legal-help/asylum/important-changes-for-asylum-seekers-under-the-trump-administration/", "Important Changes for Asylum Seekers under the Trump Administration");
$r10 = $page->ref("https://immigrantjustice.org/timeline-trump-administrations-efforts-end-asylum", "A Timeline Of The Trump Administration’s Efforts To End Asylum");
$r11 = $page->ref("https://www.nytimes.com/2024/05/12/us/donald-trump-hannibal-lecter.html", "Trump, Bashing Migrants, Likens Them to Hannibal Lecter, Movie Cannibal");
$r12 = $page->ref("https://www.euronews.com/culture/2024/07/19/what-is-it-with-donald-trumps-obsession-with-hannibal-lecter", "What is it with Donald Trump’s obsession with Hannibal Lecter?");
$r13 = $page->ref("https://www.washingtonpost.com/politics/2024/08/14/why-trump-keeps-talking-about-fictional-serial-killer-hannibal-lecter/", "Why Trump keeps talking about fictional serial killer Hannibal Lecter");
$r14 = $page->ref("https://www.rollingstone.com/politics/politics-features/donald-trump-hannibal-lecter-timeline-1235070008/", "Why Is Trump So Obsessed With Hannibal Lecter?: A Complete Timeline");
$r15 = $page->ref("https://variety.com/2024/tv/news/donald-trump-hannibal-lecter-fascination-1236077926/", "Why Is Donald Trump So Fixated on Hannibal Lecter?");
$r16 = $page->ref("https://www.theguardian.com/culture/article/2024/jul/25/donald-trump-hannibal-lecter", "Why is Donald Trump so obsessed with Hannibal Lecter?");
$r17 = $page->ref("https://www.forbes.com/sites/siladityaray/2024/07/19/trump-likens-migrants-to-late-great-hannibal-lecter-in-rnc-speech-a-comparison-hes-used-many-times-before/", "Trump Likens Migrants To ‘Late Great Hannibal Lecter’ In RNC Speech—A Comparison He’s Used Before");



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>See references... </p>
	HTML;

$div_wikipedia_Immigration_policy_of_Donald_Trump = new WikipediaContentSection();
$div_wikipedia_Immigration_policy_of_Donald_Trump->setTitleText("Immigration policy of Donald Trump");
$div_wikipedia_Immigration_policy_of_Donald_Trump->setTitleLink("https://en.wikipedia.org/wiki/Immigration_policy_of_Donald_Trump");
$div_wikipedia_Immigration_policy_of_Donald_Trump->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Trump_administration_family_separation_policy = new WikipediaContentSection();
$div_wikipedia_Trump_administration_family_separation_policy->setTitleText("Trump administration family separation policy");
$div_wikipedia_Trump_administration_family_separation_policy->setTitleLink("https://en.wikipedia.org/wiki/Trump_administration_family_separation_policy");
$div_wikipedia_Trump_administration_family_separation_policy->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Racial_views_of_Donald_Trump = new WikipediaContentSection();
$div_wikipedia_Racial_views_of_Donald_Trump->setTitleText("Racial views of Donald Trump");
$div_wikipedia_Racial_views_of_Donald_Trump->setTitleLink("https://en.wikipedia.org/wiki/Racial_views_of_Donald_Trump");
$div_wikipedia_Racial_views_of_Donald_Trump->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Trump_travel_ban = new WikipediaContentSection();
$div_wikipedia_Trump_travel_ban->setTitleText("Trump travel ban");
$div_wikipedia_Trump_travel_ban->setTitleLink("https://en.wikipedia.org/wiki/Trump_travel_ban");
$div_wikipedia_Trump_travel_ban->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Legal_challenges_to_the_Trump_travel_ban = new WikipediaContentSection();
$div_wikipedia_Legal_challenges_to_the_Trump_travel_ban->setTitleText("Legal challenges to the Trump travel ban");
$div_wikipedia_Legal_challenges_to_the_Trump_travel_ban->setTitleLink("https://en.wikipedia.org/wiki/Legal_challenges_to_the_Trump_travel_ban");
$div_wikipedia_Legal_challenges_to_the_Trump_travel_ban->content = <<<HTML
	<p>
	</p>
	HTML;


$page->parent('immigration.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_wikipedia_Immigration_policy_of_Donald_Trump);
$page->body($div_wikipedia_Trump_administration_family_separation_policy);
$page->body($div_wikipedia_Racial_views_of_Donald_Trump);
$page->body($div_wikipedia_Trump_travel_ban);
$page->body($div_wikipedia_Legal_challenges_to_the_Trump_travel_ban);
