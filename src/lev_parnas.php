<?php
$page = new PersonPage();
$page->h1("Lev Parnas");
$page->alpha_sort("Parnas, Lev");
$page->tags("Person", "Ukraine", "Donald Trump");
$page->keywords("Lev Parnas");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Lev_Parnas = new WikipediaContentSection();
$div_wikipedia_Lev_Parnas->setTitleText("Lev Parnas");
$div_wikipedia_Lev_Parnas->setTitleLink("https://en.wikipedia.org/wiki/Lev_Parnas");
$div_wikipedia_Lev_Parnas->content = <<<HTML
	<p>Lev Parnas is a Soviet-born American businessman and former associate of Rudy Giuliani.
	Parnas, Giuliani, Igor Fruman, John Solomon, Yuriy Lutsenko, Dmytro Firtash and his allies, Victoria Toensing and Joe diGenova,
	were involved in creating the false Biden–Ukraine conspiracy theory, which is part of the Trump–Ukraine scandal's efforts to damage Joe Biden.
	As president, Donald Trump said he did not know Parnas nor what he was involved in; Parnas insisted Trump "knew exactly what was going on".</p>
	HTML;


$page->parent('list_of_people.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Lev Parnas");
$page->body($div_wikipedia_Lev_Parnas);
