<?php
$page = new BookPage();
$page->h1("The Cult of Trump: A Leading Cult Expert Explains How the President Uses Mind Control");
$page->tags("Book", "Steven Hassan", "USA", "MAGA", "Donald Trump", "Disinformation");
$page->keywords("The Cult of Trump: A Leading Cult Expert Explains How the President Uses Mind Control");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>"The Cult of $Trump: A Leading Cult Expert Explains How the President Uses Mind Control" is a book by ${'Steven Hassan'}.</p>
	HTML;




$div_youtube_David_Pakman_Show_Hassan_explains_the_cult_of_Trump = new YoutubeContentSection();
$div_youtube_David_Pakman_Show_Hassan_explains_the_cult_of_Trump->setTitleText("David Pakman Show: Hassan explains the cult of Trump ");
$div_youtube_David_Pakman_Show_Hassan_explains_the_cult_of_Trump->setTitleLink("https://www.youtube.com/watch?v=uMlLTtL80pI");





$div_The_Cult_of_TrumpThe_Cult_of_Trump = new WebsiteContentSection();
$div_The_Cult_of_TrumpThe_Cult_of_Trump->setTitleText("Publisher: The Cult of Trump");
$div_The_Cult_of_TrumpThe_Cult_of_Trump->setTitleLink("https://www.simonandschuster.com/books/The-Cult-of-Trump/Steven-Hassan/9781982127343");
$div_The_Cult_of_TrumpThe_Cult_of_Trump->content = <<<HTML
	<p>Since the 2016 election, Donald Trump’s behavior has become both more disturbing and yet increasingly familiar. He relies on phrases like, “fake news,” “build the wall,” and continues to spread the divisive mentality of us-vs.-them. He lies constantly, has no conscience, never admits when he is wrong, and projects all of his shortcomings on to others. He has become more authoritarian, more outrageous, and yet many of his followers remain blindly devoted. Scott Adams, the creator of Dilbert and a major Trump supporter, calls him one of the most persuasive people living. His need to squash alternate information and his insistence of constant ego stroking are all characteristics of other famous leaders—cult leaders.</p>

	<p>In The Cult of Trump, mind control and licensed mental health expert Steven Hassan draws parallels between our current president and people like Jim Jones, David Koresh, Ron Hubbard, and Sun Myung Moon, arguing that this presidency is in many ways like a destructive cult. He specifically details the ways in which people are influenced through an array of social psychology methods and how they become fiercely loyal and obedient. Hassan was a former “Moonie” himself, and he presents a “thoughtful and well-researched analysis of some of the most puzzling aspects of the current presidency, including the remarkable passivity of fellow Republicans [and] the gross pandering of many members of the press” (Thomas G. Gutheil, MD and professor of psychiatry, Harvard Medical School).</p>

	<p>The Cult of Trump is an accessible and in-depth analysis of the president, showing that under the right circumstances, even sane, rational, well-adjusted people can be persuaded to believe the most outrageous ideas. “This book is a must for anyone who wants to understand the current political climate” (Judith Stevens-Long, PhD and author of Living Well, Dying Well).</p>
	HTML;



$page->parent('list_of_books.html');
$page->template("stub");
$page->body($div_introduction);



$page->body($div_The_Cult_of_TrumpThe_Cult_of_Trump);

$page->body($div_youtube_David_Pakman_Show_Hassan_explains_the_cult_of_Trump);
