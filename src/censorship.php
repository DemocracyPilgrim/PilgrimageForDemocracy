<?php
$page = new Page();
$page->h1('Censorship');
$page->keywords('censorship');
$page->tags("Information: Government", "Information: Media", "Political Discourse", "Freedom of Speech", "Danger");
$page->stars(0);

$page->snp('description', 'Suppression of speech, public communication, or other information.');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Censorship is especially problematic in the following cases:</p>

	<ul>
		<li>The information being censored is factually true.</li>
		<li>Powerful entities (political or economic powers) censor the powerless (oppressed or poor people).</li>
		<li>Censorship is applied in order to prevent factual truth from being disseminated.</li>
		<li>Censorship is applied in order to prevent propaganda, falsehoods, $disinformation from being denounced for what they are.</li>
		<li>A sanctioned world view, narrative or lifestyle is being imposed onto a society
			and competing truthful, harmless world views, narratives and lifestyles are censored,
			all in violation of ${'freedom of conscience'}.</li>
	</ul>
	HTML;

$list = new ListOfPages();
$list->add('national_coalition_against_censorship.html');
$list->add('freedom_of_speech.html');
$print_list = $list->print();

$div_list = new ContentSection();
$div_list->content = <<<HTML
	<h3>Related content</h3>

	$print_list
	HTML;

$div_wikipedia_Censorship = new WikipediaContentSection();
$div_wikipedia_Censorship->setTitleText('Censorship');
$div_wikipedia_Censorship->setTitleLink('https://en.wikipedia.org/wiki/Censorship');
$div_wikipedia_Censorship->content = <<<HTML
	<p>Censorship is the suppression of speech, public communication, or other information.
	This may be done on the basis that such material is considered objectionable, harmful, sensitive, or "inconvenient".
	Censorship can be conducted by governments, private institutions and other controlling bodies.</p>

	<p>Direct censorship may or may not be legal, depending on the type, location, and content.
	Many countries provide strong protections against censorship by law, but none of these protections are absolute
	and frequently a claim of necessity to balance conflicting rights is made, in order to determine what could and could not be censored.</p>
	HTML;


$page->parent('political_discourse.html');
$page->template("stub");
$page->body($div_introduction);

$page->body($div_list);

$page->body($div_wikipedia_Censorship);
