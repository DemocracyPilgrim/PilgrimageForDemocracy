<?php
$page = new Page();
$page->h1("We Are the Leaders We Have Been Looking For");
$page->tags("Book");
$page->keywords("We Are the Leaders We Have Been Looking For");
$page->stars(0);

//$page->snp("description", "");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>"We Are the Leaders We Have Been Looking For" is a book by ${'Eddie Glaude'}.
	The book is related to the 6th level of Democracy.</p>
	HTML;


$page->parent('list_of_books.html');
$page->template("stub");
$page->body($div_introduction);

$page->body('eddie_glaude.html');
