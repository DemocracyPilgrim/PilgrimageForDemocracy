<?php
$page = new Page();
$page->h1('International Organization for Migration');
$page->keywords('International Organization for Migration', 'IOM');
$page->tags("Organisation", "Immigration", "International");
$page->stars(0);

//$page->snp('description', '');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>The International Organization for Migration (IOM) is part of the United Nations System
	as the leading inter-governmental organization promoting since 1951 humane and orderly migration
	for the benefit of all, with 175 member states and a presence in over 100 countries.</p>
	HTML;

$div_wikipedia_International_Organization_for_Migration = new WikipediaContentSection();
$div_wikipedia_International_Organization_for_Migration->setTitleText('International Organization for Migration');
$div_wikipedia_International_Organization_for_Migration->setTitleLink('https://en.wikipedia.org/wiki/International_Organization_for_Migration');
$div_wikipedia_International_Organization_for_Migration->content = <<<HTML
	<p>The International Organization for Migration (IOM) is a United Nations agency
	that provides services and advice concerning migration to governments and migrants,
	including internally displaced persons, refugees, and migrant workers.
	The IOM was established in 1951 as the Intergovernmental Committee for European Migration (ICEM) to help resettle people displaced by World War II.
	It became a United Nations agency in 2016.</p>
	HTML;


$page->parent('united_nations.html');
$page->parent('immigration.html');
$page->template("stub");
$page->body($div_introduction);
$page->body('global_migration_data_analysis_centre.html');
$page->body('missing_migrants_project.html');
$page->body('migration_law.html');


$page->body($div_wikipedia_International_Organization_for_Migration);
