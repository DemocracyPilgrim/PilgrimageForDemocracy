<?php
$page = new Page();
$page->h1('Sharon Brous');
$page->alpha_sort("Brous, Sharon");
$page->tags("Person");
$page->keywords('Sharon Brous');
$page->stars(1);

//$page->snp('description', '');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );


$r1 = $page->ref('https://ikar.org/sermons/lets-not-lose-our-minds-rabbi-sharon-brous/', 'IKAR: We’ve Lost So Much. Let’s Not Lose Our Damn Minds – Rabbi Sharon Brous');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Sharon Brous is an $American rabbi who currently serves as the senior rabbi of IKAR, a Jewish congregation in Los Angeles, which she founded.</p>
	HTML;

$div_reaction_after_hamas_attack = new ContentSection();
$div_reaction_after_hamas_attack->content = <<<HTML
	<h3>Reaction after Hamas attack</h3>


	<p>One week about the 2023 October 7th $Hamas attack on $Israel, rabbi Sharon Brous gave a sermon
	in which she detailed the trauma suffered by the whole Jewish community.
	She concluded her sermon with the following observations:$1</p>

	<blockquote>
	<p>This, I believe, is the great question of our lives:
	When the night comes, who will sit and weep by your side? Who shares your worry?
	Who will not be scared away by your grief, but will come closer?
	Who sees you? And who do you see?</p>

	<p>As we walk into this unknown future, full of grief and uncertainty, I thank this community for stepping closer,
	in the depths of our heartache this week.
	Please, let us continue to find our way to each other with tenderness.
	We need one another now.</p>

	<p>And I thank our friends and allies, who also came close, despite your own anguished hearts.</p>

	<p>And I ask us to promise that this feeling of isolation and loneliness, the yearning for solidarity,
	will remind us of the sacred responsibility to step closer, rather than hide, equivocate and retreat ourselves when another people is suffering.
	<strong>We, who have been excluded by the narrow scope of others’ moral concern,
	must not narrow the scope of our moral concern to exclude others.</strong>
	Do you understand what I’m saying?
	Just because others have lost their damn minds, we must not lose our damn minds.</p>

	<p>Sadly, I know that the days ahead will give us many opportunities
	to be the kinds of allies and friends we wish we had been embraced by this week.
	<strong>It is precisely my unremitting desire for my own pain to be validated
	that will guide me in validating and crying out with other human beings in suffering,
	including $Palestinian civilians in $Gaza, for whom the situation is already unbearable and becoming increasingly desperate.</strong> (...)</p>

	<p>Every single person in Gaza has a mother, or had a mother at some point. (...)</p>

	<p>I say this today: it will take generations for us to recover from the psychic wounds we have incurred this past week.
	I have a sense that <strong>our healing will come when treat each other, mother to mother, sister to sister, brother to brother,
	as though we, and each other’s children, are our shared responsibility.</strong></p>
	</blockquote>
	HTML;


$div_IKAR_website = new WebsiteContentSection();
$div_IKAR_website->setTitleText('IKAR website ');
$div_IKAR_website->setTitleLink('https://ikar.org/');
$div_IKAR_website->content = <<<HTML
	<p>IKAR’s mission is to reanimate Jewish life and develop a spiritual and moral foundation for a just and equitable society.</p>
	HTML;


$div_wikipedia_Sharon_Brous = new WikipediaContentSection();
$div_wikipedia_Sharon_Brous->setTitleText('Sharon Brous');
$div_wikipedia_Sharon_Brous->setTitleLink('https://en.wikipedia.org/wiki/Sharon_Brous');
$div_wikipedia_Sharon_Brous->content = <<<HTML
	<p>Sharon Brous (born 1973) is an American rabbi who currently serves as the senior rabbi of IKAR, a Jewish congregation in Los Angeles.</p>
	HTML;

$div_wikipedia_IKAR = new WikipediaContentSection();
$div_wikipedia_IKAR->setTitleText('IKAR');
$div_wikipedia_IKAR->setTitleLink('https://en.wikipedia.org/wiki/IKAR');
$div_wikipedia_IKAR->content = <<<HTML
	<p>IKAR is a post-denominational Jewish congregation and community founded in Los Angeles and led by Rabbi Sharon Brous.</p>
	HTML;


$page->parent('list_of_people.html');
$page->template("stub");
$page->body($div_introduction);
$page->body($div_reaction_after_hamas_attack);


$page->body($div_wikipedia_Sharon_Brous);
$page->body($div_wikipedia_IKAR);

$page->body($div_IKAR_website);
