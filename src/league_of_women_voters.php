<?php
$page = new OrganisationPage();
$page->h1("League of Women Voters");
$page->tags("Organisation", "Women's Rights", "Elections");
$page->keywords("League of Women Voters");
$page->stars(0);
$page->viewport_background("/free/league_of_women_voters.png");

$page->snp("description", "Empowering votes and defending democracy for over a century.");
$page->snp("image",       "/free/league_of_women_voters.1200-630.png");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;




$div_League_of_Women_Voters = new WebsiteContentSection();
$div_League_of_Women_Voters->setTitleText("League of Women Voters ");
$div_League_of_Women_Voters->setTitleLink("https://www.lwv.org/");
$div_League_of_Women_Voters->content = <<<HTML
	<p>The League is a political grassroots network and membership organization that believes the freedom to vote is a nonpartisan issue. For more than a century, we’ve worked to empower voters and defend democracy. As a women-led organization, we encourage everyone to take part in our democracy.</p>
	HTML;


$div_youtube_League_of_Women_Voters = new YoutubeContentSection();
$div_youtube_League_of_Women_Voters->setTitleText("League of Women Voters");
$div_youtube_League_of_Women_Voters->setTitleLink("https://www.youtube.com/@LeagueofWomenVotersUS");




$div_wikipedia_League_of_Women_Voters = new WikipediaContentSection();
$div_wikipedia_League_of_Women_Voters->setTitleText("League of Women Voters");
$div_wikipedia_League_of_Women_Voters->setTitleLink("https://en.wikipedia.org/wiki/League_of_Women_Voters");
$div_wikipedia_League_of_Women_Voters->content = <<<HTML
	<p>The League of Women Voters (LWV) is a nonpartisan American nonprofit political organization. Founded in 1920, its ongoing major activities include registering voters, providing voter information, boosting voter turnout and advocating for voting rights. In addition, the LWV works with partners for specific campaigns including support for campaign finance reform, women's rights, health care reform and gun control.</p>
	HTML;


$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);



$page->body($div_League_of_Women_Voters);
$page->related_tag("League of Women Voters");
$page->body($div_youtube_League_of_Women_Voters);
$page->body($div_wikipedia_League_of_Women_Voters);
