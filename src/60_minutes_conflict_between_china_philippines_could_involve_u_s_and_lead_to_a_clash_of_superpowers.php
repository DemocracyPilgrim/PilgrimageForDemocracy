<?php
$page = new VideoPage();
$page->h1("60 Minutes: Conflict between China, Philippines could involve U.S. and lead to a clash of superpowers");
$page->tags("Video: News Story", "60 Minutes", "China", "USA", "Philippines", "Chinese Expansionism");
$page->stars(2);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>The U.S. could be drawn into a conflict between China and the Philippines that's been roiling the South China Sea.</p>
	HTML;



$div_Conflict_between_China_Philippines_could_involve_U_S_and_lead_to_a_clash_of = new WebsiteContentSection();
$div_Conflict_between_China_Philippines_could_involve_U_S_and_lead_to_a_clash_of->setTitleText("Video: Conflict between China, Philippines could involve U.S...");
$div_Conflict_between_China_Philippines_could_involve_U_S_and_lead_to_a_clash_of->setTitleLink("https://www.cbsnews.com/news/china-philippines-conflict-could-involve-us-60-minutes-transcript/");
$div_Conflict_between_China_Philippines_could_involve_U_S_and_lead_to_a_clash_of->content = <<<HTML
	<p>If there's going to be a military conflict between the United States and China, the thinking in Washington goes, it will most likely happen if China tries to invade Taiwan. But lately tensions have escalated precariously in another part of the South China Sea-the waters off the western coast of the Philippines where an international tribunal ruled the Philippines has exclusive economic rights. But China claims almost all of the South China Sea, one of the world's most vital waterways through which more than $3 trillion in goods flow each year. To assert its claims, China has been using tactics just short of war -- leading to violent confrontations. The United States has a mutual defense treaty with the Philippines, which could mean American intervention. It's been called "the most dangerous conflict no one is talking about." And last month we saw for ourselves just how dangerous it can be…</p>
	HTML;



$div_60_Minutes_witnesses_international_incident_in_the_South_China_Sea = new WebsiteContentSection();
$div_60_Minutes_witnesses_international_incident_in_the_South_China_Sea->setTitleText("Video: 60 Minutes witnesses international incident in the South China Sea");
$div_60_Minutes_witnesses_international_incident_in_the_South_China_Sea->setTitleLink("https://www.cbsnews.com/news/60-minutes-witnesses-international-incident-in-the-south-china-sea/");
$div_60_Minutes_witnesses_international_incident_in_the_South_China_Sea->content = <<<HTML
	<p>For this week's season premiere of 60 Minutes, correspondent Cecilia Vega and a producing team intended to report on tensions between China and the Philippines in the South China Sea. They did not expect to end up in the middle of an international incident themselves, seeing China's intimidation tactics first-hand.</p>
	HTML;



$div_China_rams_Philippine_ship_while_60_Minutes_on_board_South_China_Sea_tensions = new WebsiteContentSection();
$div_China_rams_Philippine_ship_while_60_Minutes_on_board_South_China_Sea_tensions->setTitleText("Video: China rams Philippine ship while 60 Minutes on board; South China Sea tensions");
$div_China_rams_Philippine_ship_while_60_Minutes_on_board_South_China_Sea_tensions->setTitleLink("https://www.cbsnews.com/news/philippines-china-sea-conflict-us-role-60-minutes/");
$div_China_rams_Philippine_ship_while_60_Minutes_on_board_South_China_Sea_tensions->content = <<<HTML
	<p>An escalating series of clashes in the South China Sea between the Philippines and China   could draw the U.S., which has a mutual defense treaty with the Philippines, into the conflict.</p>
	HTML;






$page->parent('list_of_videos.html');
$page->template("stub");
$page->body($div_introduction);

$page->body($div_Conflict_between_China_Philippines_could_involve_U_S_and_lead_to_a_clash_of);
$page->body($div_60_Minutes_witnesses_international_incident_in_the_South_China_Sea);
$page->body($div_China_rams_Philippine_ship_while_60_Minutes_on_board_South_China_Sea_tensions);
