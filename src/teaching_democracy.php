<?php
$page = new Page();
$page->h1("Addendum D: Democracy — Teaching it with ethics");
$page->keywords("teaching democracy");
$page->stars(0);

//$page->snp("description", "");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p>Freedom is not inherited; it must be taught and cultivated.
	How can we teach democracy without indoctrination?
	We confront the ethical challenge of educating young minds in democratic values,
	respecting freedom of conscience and diverse perspectives,
	while equipping future generations to build a just society.</p>
	HTML );

$r1 = $page->ref("https://www.reaganlibrary.gov/archives/speech/january-5-1967-inaugural-address-public-ceremony", "Ronald Reagan, January 5, 1967: California Gubernatorial Inaugural Address");


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>How to teach $democracy to young students at school in an ethical way?</p>

	<p>Here is a conundrum:
	school $education is all about shaping the minds of the children of the nation.
	It may not be brainwashing, but it certainly is a form of "brain shaping".
	And yet, one of the most fundamental values of the ${'first level of democracy'} is
	${'freedom of conscience'}: we ought to respect each other's opinions and values.</p>

	<p>Certainly, any form of education can only be performed with the full ascent of the parents,
	the ultimate guardians of school age children.
	Unfortunately, discussing democracy involves discussing topics and value systems
	that can be perceived as highly political and highly divisive.
	Some parents might react negatively to some aspects of civics education.</p>

	<p>Another thing to consider is that the political climate and media landscape varies from $country to country.
	Apparently simple topics like ${'freedom of speech'} or $disinformation might be fully acceptable teaching material
	in some countries' schools, but highly controversial in other countries.</p>

	<p>This page is a call to teachers, educators, policy makers, school curriculum developers
	to help brainstorm and develop ethical, appropriate curriculum and teaching material
	to teach the features and values of $democracy, ${'social justice'} in various countries.
	Such teaching material could be integrated into civics classes, history classes, geography, maths, literature, English classes, etc. etc.</p>


	<blockquote>
		"The secret to freedom is in educating the people, whereas the secret to tyranny is keeping them ignorant."
		<br>~Robespierre~
	</blockquote>
	HTML;

$list = new ListOfPages();
$list->add('think_like_a_voter.html');
$print_list = $list->print();

$div_list = new ContentSection();
$div_list->content = <<<HTML
	<h3>Related content</h3>

	$print_list
	HTML;


$div_Quotes = new ContentSection();
$div_Quotes->content = <<<HTML
	<h3>Quotes</h3>

	<blockquote>
		“Freedom is never more than one generation away from extinction.
		We didn't pass it to our children in the bloodstream.
		It must be fought for, protected, and handed on for them to do the same,
		or one day we will spend our sunset years telling our children and our children's children
		what it was once like in the United States where men were free.”

		"Freedom is a fragile thing and it's never more than one generation away from extinction.
		It is not ours by way of inheritance;
		it must be fought for and defended constantly by each generation, for it comes only once to a people.
		And those in world history who have known freedom and then lost it have never known it again. $r1
		<br>― Ronald Reagan
	</blockquote>
	HTML;


$div_US_Archives_Educational_Resources = new WebsiteContentSection();
$div_US_Archives_Educational_Resources->setTitleText("US National archives: Educational Resources");
$div_US_Archives_Educational_Resources->setTitleLink("https://www.archives.gov/legislative/resources");
$div_US_Archives_Educational_Resources->content = <<<HTML
	<p>The Center for Legislative Archives—part of the US National Archives—maintains some of the most historically valuable documents
	created by the federal government: the records of the U.S. House of Representatives and the U.S. Senate.
	Educators can use these historical documents to teach about representative democracy, how Congress works,
	and the important role Congress has played throughout American history.</p>
	HTML;


$h2_Next = new h2HeaderContent("Next...");


$list_all_related_topics = new ListOfPages();
$list_all_related_topics->add('education.html');
$print_list_all_related_topics = $list_all_related_topics->print();

$div_list_all_related_topics = new ContentSection();
$div_list_all_related_topics->content = <<<HTML
	$print_list_all_related_topics
	HTML;



$page->parent('democracy.html');
$page->previous('saints_and_little_devils.html');
$page->next('democracy_taxes.html');
$page->template("stub");
$page->body($div_introduction);

$page->body($div_list);



$page->body($div_Quotes);
$page->body($div_US_Archives_Educational_Resources);

$page->body($div_list_all_related_topics);
$page->body($h2_Next);
