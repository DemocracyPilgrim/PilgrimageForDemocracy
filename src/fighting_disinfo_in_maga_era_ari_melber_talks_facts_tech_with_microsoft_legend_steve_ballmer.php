<?php
$page = new VideoPage();
$page->h1("Fighting disinfo in MAGA era: Ari Melber talks facts & tech with Microsoft legend Steve Ballmer");
$page->tags("Video: Interview", "Information: Facts", "Steve Ballmer", "Ari Melber", "Disinformation", "USAFacts");
$page->keywords("Fighting disinfo in MAGA era: Ari Melber talks facts & tech with Microsoft legend Steve Ballmer");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;


$div_youtube_Fighting_disinfo_in_MAGA_era_Ari_Melber_talks_facts_amp_tech_with_Microsoft_legend_Steve_Ballmer = new YoutubeContentSection();
$div_youtube_Fighting_disinfo_in_MAGA_era_Ari_Melber_talks_facts_amp_tech_with_Microsoft_legend_Steve_Ballmer->setTitleText("Fighting disinfo in MAGA era: Ari Melber talks facts &amp; tech with Microsoft legend Steve Ballmer");
$div_youtube_Fighting_disinfo_in_MAGA_era_Ari_Melber_talks_facts_amp_tech_with_Microsoft_legend_Steve_Ballmer->setTitleLink("https://www.youtube.com/watch?v=1cQhnm4K3qA");
$div_youtube_Fighting_disinfo_in_MAGA_era_Ari_Melber_talks_facts_amp_tech_with_Microsoft_legend_Steve_Ballmer->content = <<<HTML
	<p>
	</p>
	HTML;



$page->parent('list_of_videos.html');
$page->template("stub");
$page->body($div_introduction);
$page->body($div_youtube_Fighting_disinfo_in_MAGA_era_Ari_Melber_talks_facts_amp_tech_with_Microsoft_legend_Steve_Ballmer);
