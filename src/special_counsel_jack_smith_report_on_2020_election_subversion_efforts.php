<?php
$page = new ReportPage();
$page->h1("Special Counsel Jack Smith Report on 2020 Election Subversion Efforts");
$page->viewport_background("");
$page->keywords("Special Counsel Jack Smith Report on 2020 Election Subversion Efforts");
$page->stars(0);
$page->tags("Report", "Donald Trump", "Jack Smith");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>

	<div class="download-pdf">
		<a href="/archives/special-counsel-jack-smiths-report-on-2020-election-subversion-efforts.pdf">
			<div class="download"><i class="outline file pdf white large icon"></i>Download</div>
		</a>
	</div>
	HTML;



$div_Special_counsel_report_condemns_Trump_s_criminal_efforts_to_retain_power_in = new WebsiteContentSection();
$div_Special_counsel_report_condemns_Trump_s_criminal_efforts_to_retain_power_in->setTitleText(" Special counsel report condemns Trump’s ‘criminal efforts to retain power’ in");
$div_Special_counsel_report_condemns_Trump_s_criminal_efforts_to_retain_power_in->setTitleLink("https://edition.cnn.com/2025/01/14/politics/special-counsel-smith-report-trump-2020-election-subversion");
$div_Special_counsel_report_condemns_Trump_s_criminal_efforts_to_retain_power_in->content = <<<HTML
	<p> Attorney General Merrick Garland has publicly released special counsel Jack Smith’s report on his investigation into Donald Trump and efforts to overturn the 2020 election, detailing the president-elect’s “criminal efforts to retain power” and projecting confidence in the investigation.
	The more than 130-page report, which was submitted to Congress and released early Tuesday after a court hold blocking its release expired at midnight, spells out in extensive — if largely already known — detail how Trump tried to overturn the 2020 election. Smith’s team states in no uncertain terms that they believed Trump criminally attempted to subvert the will of the people and overturn the election results.</p>
	HTML;



$div_wikipedia_Federal_prosecution_of_Donald_Trump_election_obstruction_case = new WikipediaContentSection();
$div_wikipedia_Federal_prosecution_of_Donald_Trump_election_obstruction_case->setTitleText("Federal prosecution of Donald Trump election obstruction case");
$div_wikipedia_Federal_prosecution_of_Donald_Trump_election_obstruction_case->setTitleLink("https://en.wikipedia.org/wiki/Federal_prosecution_of_Donald_Trump_(election_obstruction_case)");
$div_wikipedia_Federal_prosecution_of_Donald_Trump_election_obstruction_case->content = <<<HTML
	<p>United States of America v. Donald J. Trump was a federal criminal case against Donald Trump, former president of the United States from 2017 to 2021 and president-elect, regarding his alleged participation in attempts to overturn the 2020 U.S. presidential election, including his involvement in the January 6 Capitol attack.</p>
	HTML;

$div_wikipedia_Smith_special_counsel_investigation = new WikipediaContentSection();
$div_wikipedia_Smith_special_counsel_investigation->setTitleText("Smith special counsel investigation");
$div_wikipedia_Smith_special_counsel_investigation->setTitleLink("https://en.wikipedia.org/wiki/Smith_special_counsel_investigation");
$div_wikipedia_Smith_special_counsel_investigation->content = <<<HTML
	<p>The Smith special counsel investigation was a special counsel investigation that was opened by U.S. Attorney General Merrick Garland on November 18, 2022, three days after Donald Trump announced his campaign for the 2024 United States presidential election. The investigation was created to continue two investigations initiated by the Justice Department (DOJ) regarding former U.S. President Donald Trump. Garland appointed Jack Smith, a longtime federal prosecutor, to lead the independent investigations. Smith was tasked with investigating Trump's role in the January 6 United States Capitol attack and Trump's mishandling of government records, including classified documents.</p>
	HTML;


$page->parent('list_of_research_and_reports.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Special Counsel Jack Smith Report on 2020 Election Subversion Efforts");
$page->body($div_Special_counsel_report_condemns_Trump_s_criminal_efforts_to_retain_power_in);
$page->body($div_wikipedia_Federal_prosecution_of_Donald_Trump_election_obstruction_case);
$page->body($div_wikipedia_Smith_special_counsel_investigation);
