<?php
$page = new OrganisationPage();
$page->h1("Integrity Institute");
$page->viewport_background("");
$page->keywords("Integrity Institute");
$page->stars(0);
$page->tags("Organisation", "The Web Defenders", "Disinformation", "Social Networks");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>The Integrity Institute researches and produces reports on the following topics: Child Safety Online, Analyses of Facebook, Misinformation Amplification Analyses, Elections Integrity Program.</p>
	HTML;




$div_Misinformation_Amplification_Analysis_and_Tracking_Dashboard = new WebsiteContentSection();
$div_Misinformation_Amplification_Analysis_and_Tracking_Dashboard->setTitleText("Misinformation Amplification Analysis and Tracking Dashboard");
$div_Misinformation_Amplification_Analysis_and_Tracking_Dashboard->setTitleLink("https://integrityinstitute.org/blog/misinformation-amplification-tracking-dashboard");
$div_Misinformation_Amplification_Analysis_and_Tracking_Dashboard->content = <<<HTML
	<p>Tracking how social media platforms respond to misinformation is crucial to understanding the risks and dangers that platforms can pose to democratic elections. At the Integrity Institute, we are tracking how misinformation performs on platforms to measure the extent to which platforms are amplifying misinformation, and the extent to which they are creating an incentive structure that rewards lies and misinformation online.
	How platforms respond to misinformation can change. Amplification of misinformation can rise around critical events if misinformation narratives take hold. It can also fall, if platforms implement design changes around the event that reduce the spread of misinformation. We will be tracking how misinformation is amplified on the platforms, updated weekly, heading into and after the US midterm elections, to see if the large platforms are taking responsible actions in response to misinformation.</p>
	HTML;




$div_How_Social_Media_Amplifies_Misinformation_More_Than_Information = new WebsiteContentSection();
$div_How_Social_Media_Amplifies_Misinformation_More_Than_Information->setTitleText("How Social Media Amplifies Misinformation More Than Information");
$div_How_Social_Media_Amplifies_Misinformation_More_Than_Information->setTitleLink("https://www.nytimes.com/2022/10/13/technology/misinformation-integrity-institute-report.html");
$div_How_Social_Media_Amplifies_Misinformation_More_Than_Information->content = <<<HTML
	<p>A new analysis found that algorithms and some features of social media sites help false posts go viral.</p>
	HTML;





$div_Integrity_Institute = new WebsiteContentSection();
$div_Integrity_Institute->setTitleText("Integrity Institute ");
$div_Integrity_Institute->setTitleLink("https://integrityinstitute.org/");
$div_Integrity_Institute->content = <<<HTML
	<p>We protect the social internet.</p>
	HTML;



$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Integrity Institute");

$page->body($div_Integrity_Institute);
$page->body($div_Misinformation_Amplification_Analysis_and_Tracking_Dashboard);
$page->body($div_How_Social_Media_Amplifies_Misinformation_More_Than_Information);
