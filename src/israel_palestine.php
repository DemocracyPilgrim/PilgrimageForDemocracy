<?php
$page = new Page();
$page->h1('Israel and Palestine');
$page->stars(0);
$page->keywords('Hamas');

//$page->snp('description', '');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );

$r1 = $page->ref('https://www.youtube.com/watch?v=FiCUoVrqVN4', 'Our hearts are big enough to hold the humanity and heartache of Jews and Palestinians: Rabbi Brous');


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>After the 2023 October 7th attack by Hamas on Israel, rabbi ${'Sharon Brous'} was asked about the loss of 1400 Israeli people,
	murdered by Hamas, and about the loss of life of Palestinian civilians, following Israel's retaliatory attacks on Gaza.
	She said: $r1</p>

	<blockquote>
	<p><strong>Our hearts are capacious enough to hold both.</strong></p>

	<p>If we really care about humanity, then we care not only about the Jewish babies that were massacred in these towns on the south.
	But I'm also deeply concerned about the Palestinian children in Gaza who have nothing to do with this war
	and who deserve to live to live a free life, a life of dignity and a life of peace.</p>

	<p>What I actually am asking of us is that we dare
	to hold <strong>the humanity and the heartache and the trauma and the need for security and safety of the Jewish people</strong>
	while also holding <strong>the humanity and the heartache and the dignity and the need for justice for the Palestinian people</strong>.
	These are not binaries.
	These are things that we can actually and must actually seek out and achieve together.
	</p>
	</blockquote>

	<p>The decades old Israeli-Palestinian conflict is one of the most complex there is in this world.
	There are no simple nor quick solutions for lasting peace.
	The purpose of this page is to provide some insights and information,
	in the spirit of what Sharon Brous stated above.</p>
	HTML;


$div_wikipedia_Israeli_occupied_territories = new WikipediaContentSection();
$div_wikipedia_Israeli_occupied_territories->setTitleText('Israeli occupied territories');
$div_wikipedia_Israeli_occupied_territories->setTitleLink('https://en.wikipedia.org/wiki/Israeli-occupied_territories');
$div_wikipedia_Israeli_occupied_territories->content = <<<HTML
	<p>Israeli-occupied territories are the lands that were captured and occupied by Israel during the Six-Day War of 1967.
	While the term is currently applied to the Palestinian territories and the Golan Heights,
	it has also been used to refer to areas that were formerly occupied by Israel,
	namely the Sinai Peninsula and southern Lebanon.</p>
	HTML;

$div_wikipedia_Palestinian_citizens_of_Israel = new WikipediaContentSection();
$div_wikipedia_Palestinian_citizens_of_Israel->setTitleText('Palestinian citizens of Israel');
$div_wikipedia_Palestinian_citizens_of_Israel->setTitleLink('https://en.wikipedia.org/wiki/Palestinian_citizens_of_Israel');
$div_wikipedia_Palestinian_citizens_of_Israel->content = <<<HTML
	<p>Palestinian citizens of Israel, also known as 48-Palestinians are Arab citizens of Israel that self-identify as Palestinian.
	According to Israel's Central Bureau of Statistics, the Arab population in 2019 was estimated at 1,890,000,
	representing 20.95% of the country's population.</p>
	HTML;

$div_wikipedia_Israel_and_apartheid = new WikipediaContentSection();
$div_wikipedia_Israel_and_apartheid->setTitleText('Israel and apartheid');
$div_wikipedia_Israel_and_apartheid->setTitleLink('https://en.wikipedia.org/wiki/Israel_and_apartheid');
$div_wikipedia_Israel_and_apartheid->content = <<<HTML
	<p>Israel's policies and actions in its ongoing occupation of the Palestinian territories have drawn accusations
	that it is committing the crime of apartheid.
	Leading Palestinian, Israeli and international human rights groups have said that the totality and severity of the human rights violations
	against the Palestinian population in the occupied territories, and by some in Israel proper,
	amount to the crime against humanity of apartheid.
	Israel and some of its Western allies have rejected the accusation, with the former often labeling the charge antisemitic.</p>
	HTML;


$div_wikipedia_2023_Israel_Hamas_war = new WikipediaContentSection();
$div_wikipedia_2023_Israel_Hamas_war->setTitleText('2023 Israel–Hamas war');
$div_wikipedia_2023_Israel_Hamas_war->setTitleLink('https://en.wikipedia.org/wiki/2023_Israel%E2%80%93Hamas_war');
$div_wikipedia_2023_Israel_Hamas_war->content = <<<HTML
	<p>On 7 October 2023, an armed conflict broke out between Israel and Hamas-led Palestinian militants from the Gaza Strip
	after the latter launched a multi-pronged invasion of southern Israel.
	The Israeli military retaliated by conducting an extensive aerial bombardment campaign on Gazan targets
	and followed up with a large-scale ground invasion of Gaza.
	By the end of October, more than 1,500 Israelis, mostly civilians, were killed in the initial attacks by Hamas,
	and more than 9,700 Palestinians had been killed in the fighting.
	Over 230 Israelis and foreign nationals in Israel were captured or abducted by Hamas during the attack
	and were taken into Gaza as hostages.</p>
	HTML;



$page->parent('world.html');
$page->template("stub");
$page->body($div_introduction);
$page->body('israel.html');
$page->body('palestine.html');


$page->body($div_wikipedia_Israeli_occupied_territories);
$page->body($div_wikipedia_Palestinian_citizens_of_Israel);
$page->body($div_wikipedia_Israel_and_apartheid);
$page->body($div_wikipedia_2023_Israel_Hamas_war);
