<?php
$page = new Page();
$page->h1("History of Democracy and Social Justice in the United States");
$page->tags("International", "USA", "History", "Democracy", "Social Justice");
$page->keywords("History of Democracy and Social Justice in the United States", "History of the United States");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_History_of_the_United_States = new WikipediaContentSection();
$div_wikipedia_History_of_the_United_States->setTitleText("History of the United States");
$div_wikipedia_History_of_the_United_States->setTitleLink("https://en.wikipedia.org/wiki/History_of_the_United_States");
$div_wikipedia_History_of_the_United_States->content = <<<HTML
	<p>
	</p>
	HTML;


$page->parent('united_states.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("History of the United States");
$page->body($div_wikipedia_History_of_the_United_States);
