<?php
$page = new Page();
$page->h1("Migration law");
$page->keywords("migration law");
$page->tags("Immigration", "International");
$page->stars(0);

//$page->snp("description", "");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>What $laws and $institutions govern $immigration in each $country?
	What are the best practices?</p>
	HTML;


$div_Migration_Law_Database = new WebsiteContentSection();
$div_Migration_Law_Database->setTitleText("Migration Law Database ");
$div_Migration_Law_Database->setTitleLink("https://imldb.iom.int/");
$div_Migration_Law_Database->content = <<<HTML
	<p>Since there is no single instrument or norm covering all the relevant rights and duties of migrants,
	it is important to collect information on international migration law and frame it in an accessible and comprehensible way.</p>

	<p>$IOM seeks to consolidate this information and make it more easily accessible through, inter alia, its online migration law database.</p>

	<p>The online migration law database draws together migration related instruments and relevant norms regulating migration
	at the international and regional levels.
	The following sources of information are being included:
	relevant international, regional and bilateral treaties; international and regional resolutions, declarations and other instruments.
	The database will soon also contain relevant and prominent case law.</p>
	HTML;



$div_wikipedia_Discrimination_based_on_nationality = new WikipediaContentSection();
$div_wikipedia_Discrimination_based_on_nationality->setTitleText("Discrimination based on nationality #Migration law");
$div_wikipedia_Discrimination_based_on_nationality->setTitleLink("https://en.wikipedia.org/wiki/Discrimination_based_on_nationality#Migration_law");
$div_wikipedia_Discrimination_based_on_nationality->content = <<<HTML
	<p>Many states have travel and immigration laws based on nationality, for example offering visa-free travel to nationals of certain states but not others.</p>

	<p>A well-known example of discrimination on the basis of nationality is the Executive Order 13769 ("Muslim ban")
	in which nationals of several Muslim-majority states were prohibited from traveling to the United States.</p>
	HTML;



$page->parent('immigration.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_Migration_Law_Database);
$page->body($div_wikipedia_Discrimination_based_on_nationality);
