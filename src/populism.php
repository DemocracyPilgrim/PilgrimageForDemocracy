<?php
$page = new Page();
$page->h1("Populism");
$page->keywords("populism", "populist");
$page->tags("Information: Discourse", "Political Discourse");
$page->stars(0);

//$page->snp("description", "");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref('', '');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Populism = new WikipediaContentSection();
$div_wikipedia_Populism->setTitleText("Populism");
$div_wikipedia_Populism->setTitleLink("https://en.wikipedia.org/wiki/Populism");
$div_wikipedia_Populism->content = <<<HTML
	<p>Populism is a range of political stances that emphasize the idea of "the people" and often juxtapose this group with "the elite".
	It is frequently associated with anti-establishment and anti-political sentiment.
	The term developed in the late 19th century
	and has been applied to various politicians, parties and movements since that time, often as a pejorative.
	Within political science and other social sciences,
	several different definitions of populism have been employed, with some scholars proposing that the term be rejected altogether.</p>
	HTML;


$page->parent('political_discourse.html');
$page->template("stub");
$page->body($div_introduction);

$page->body('what_is_populism.html');

$page->body($div_wikipedia_Populism);
