<?php
$page = new Page();
$page->h1("Health care");
$page->tags("Individual: Rights", "Society");
$page->keywords("Healthcare", "healthcare", "health care");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;


$list_Healthcare = ListOfPeoplePages::WithTags("Healthcare");
$print_list_Healthcare = $list_Healthcare->print();

$div_list_Healthcare = new ContentSection();
$div_list_Healthcare->content = <<<HTML
	<h3>Related content</h3>

	$print_list_Healthcare
	HTML;



$div_wikipedia_Health_care = new WikipediaContentSection();
$div_wikipedia_Health_care->setTitleText("Health care");
$div_wikipedia_Health_care->setTitleLink("https://en.wikipedia.org/wiki/Health_care");
$div_wikipedia_Health_care->content = <<<HTML
	<p>Health care, or healthcare, is the improvement of health via the prevention, diagnosis, treatment,
	amelioration or cure of disease, illness, injury, and other physical and mental impairments in people.
	Health care is delivered by health professionals and allied health fields.
	Medicine, dentistry, pharmacy, midwifery, nursing, optometry, audiology, psychology, occupational therapy, physical therapy, athletic training,
	and other health professions all constitute health care.
	The term includes work done in providing primary care, secondary care, tertiary care, and public health.</p>
	HTML;


$page->parent('individuals.html');
$page->template("stub");
$page->body($div_introduction);
$page->body($div_list_Healthcare);


$page->body($div_wikipedia_Health_care);
