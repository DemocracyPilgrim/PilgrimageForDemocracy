<?php
$page = new Page();
$page->h1("Australian Strategic Policy Institute");
$page->keywords("Australian Strategic Policy Institute", "ASPI");
$page->stars(0);

$page->snp("description", "Australian think tank");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );

$r1 = $page->ref('https://www.aspi.org.au/our-work', 'ASPI: Our work');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>The Australian Strategic Policy Institute (ASPI) is a defence and strategic policy think tank.</p>

	<p>It produces reports about $cyberwarfare, national security, $China, intelligence, climate change. $r1</p>
	HTML;

$div_wikipedia_Australian_Strategic_Policy_Institute = new WikipediaContentSection();
$div_wikipedia_Australian_Strategic_Policy_Institute->setTitleText("Australian Strategic Policy Institute");
$div_wikipedia_Australian_Strategic_Policy_Institute->setTitleLink("https://en.wikipedia.org/wiki/Australian_Strategic_Policy_Institute");
$div_wikipedia_Australian_Strategic_Policy_Institute->content = <<<HTML
	<p>The Australian Strategic Policy Institute (ASPI) is a defence and strategic policy think tank
	based in Canberra, Australian Capital Territory,
	founded by the Australian government,
	and funded by the Australian Department of Defence along with overseas governments, and defence and technology companies.</p>
	HTML;


$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);


$page->body('truth_and_reality_with_chinese_characteristics.html');
$page->body($div_wikipedia_Australian_Strategic_Policy_Institute);
