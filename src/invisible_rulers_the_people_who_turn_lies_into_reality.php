<?php
$page = new Page();
$page->h1("Invisible Rulers: The People Who Turn Lies into Reality");
$page->tags("Book", "Information: Social Networks", "Social Networks", "Disinformation", "Artificial Intelligence");
$page->keywords("Invisible Rulers: The People Who Turn Lies into Reality");
$page->stars(1);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>"Invisible Rulers: The People Who Turn Lies into Reality" is a book by ${"Renée DiResta"}.</p>
	HTML;




$div_Invisible_Rulers_The_People_Who_Turn_Lies_into_Reality = new WebsiteContentSection();
$div_Invisible_Rulers_The_People_Who_Turn_Lies_into_Reality->setTitleText("Author: Invisible Rulers: The People Who Turn Lies into Reality ");
$div_Invisible_Rulers_The_People_Who_Turn_Lies_into_Reality->setTitleLink("https://www.reneediresta.com/books/");
$div_Invisible_Rulers_The_People_Who_Turn_Lies_into_Reality->content = <<<HTML
	<p>An “essential and riveting” (Jonathan Haidt) analysis of the radical shift in the dynamics of power and influence,
	revealing how the machinery that powered the Big Lie works to create bespoke realities revolutionizing politics, culture, and society.</p>

	<p>Renée DiResta’s powerful, original investigation into the way power and influence have been profoundly transformed
	reveals how a virtual rumor mill of niche propagandists increasingly shapes public opinion.
	While propagandists position themselves as trustworthy Davids, their reach, influence, and economics
	make them classic Goliaths—invisible rulers who create bespoke realities to revolutionize politics, culture, and society.
	Their work is driven by a simple maxim: if you make it trend, you make it true.</p>

	<p>By revealing the machinery and dynamics of the interplay between influencers, algorithms, and online crowds,
	DiResta vividly illustrates the way propagandists deliberately undermine belief in the fundamental legitimacy of institutions that make society work.
	This alternate system for shaping public opinion, unexamined until now, is rewriting the relationship between the people and their government in profound ways.
	It has become a force so shockingly effective that its destructive power seems limitless.
	Scientific proof is powerless in front of it. Democratic validity is bulldozed by it.
	Leaders are humiliated by it. But they need not be.</p>

	<p>With its deep insight into the power of propagandists to drive online crowds
	into battle—while bearing no responsibility for the consequences—Invisible Rulers not only predicts those consequences
	but offers ways for leaders to rapidly adapt and fight back.</p>
	HTML;


$div_Publisher_Invisible_Rulers_The_People_Who_Turn_Lies_into_Reality = new WebsiteContentSection();
$div_Publisher_Invisible_Rulers_The_People_Who_Turn_Lies_into_Reality->setTitleText("Publisher: Invisible Rulers: The People Who Turn Lies into Reality ");
$div_Publisher_Invisible_Rulers_The_People_Who_Turn_Lies_into_Reality->setTitleLink("https://www.hachettebookgroup.com/titles/renee-diresta/invisible-rulers/9781541703377/");





$div_youtube_Renee_DiResta_The_Invisible_Rulers_Turning_Lies_Into_Reality = new YoutubeContentSection();
$div_youtube_Renee_DiResta_The_Invisible_Rulers_Turning_Lies_Into_Reality->setTitleText("Renée DiResta | The Invisible Rulers Turning Lies Into Reality");
$div_youtube_Renee_DiResta_The_Invisible_Rulers_Turning_Lies_Into_Reality->setTitleLink("https://www.youtube.com/watch?v=Ad2gjdN_k5Y");
$div_youtube_Renee_DiResta_The_Invisible_Rulers_Turning_Lies_Into_Reality->content = <<<HTML
	<p>Just what is the machinery that powers hugely influential propaganda?
	How does it work? Who’s behind it? And what can people do about it?
	Renée DiResta, a writer and former researcher with Stanford’s Internet Observatory, comes to Commonwealth Club World Affairs
	to share her research into the way power and influence have been profoundly transformed,
	how a virtual rumor mill of niche propagandists increasingly shapes public opinion.
	She says that while propagandists position themselves as trustworthy Davids, their reach, influence, and economics
	make them classic Goliaths—invisible rulers who create bespoke realities to revolutionize politics, culture, and society.
	Their work is driven by a simple maxim: if you make it trend, you make it true.
	By revealing the machinery and dynamics of the interplay between influencers, algorithms, and online crowds,
	DiResta vividly illustrates the way propagandists deliberately undermine belief in the fundamental legitimacy of institutions that make society work.
	This alternate system for shaping public opinion, unexamined until now, is rewriting the relationship between the people and their government in profound ways.
	It has become a force so shockingly effective that its destructive power can seem limitless.
	Scientific proof is often powerless in front of it. Democratic validity is bulldozed by it.
	Leaders are humiliated by it.
	But they need not be. Join us as DiResta not only predicts the consequences of these online propagandists
	but offers ways for leaders to rapidly adapt and fight back.</p>
	HTML;



$page->parent('list_of_books.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_Invisible_Rulers_The_People_Who_Turn_Lies_into_Reality);
$page->body($div_Publisher_Invisible_Rulers_The_People_Who_Turn_Lies_into_Reality);
$page->body($div_youtube_Renee_DiResta_The_Invisible_Rulers_Turning_Lies_Into_Reality);
