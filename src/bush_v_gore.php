<?php
$page = new Page();
$page->h1("Bush v. Gore (2000)");
$page->tags("Institutions: Judiciary", "SCOTUS Landmark Decision", "Elections", "USA", "Electoral System");
$page->keywords("Bush v. Gore");
$page->stars(1);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>"Bush v. Gore" is a 2000 landmark decision by the ${'Supreme Court of the United States'}.</p>

	<p>This controversial decision halted a recount of votes in Florida,
	effectively deciding the 2000 presidential election in favor of George W. Bush.</p>

	<p>The decision undermined the democratic process, creating a sense of legitimacy and fairness around the election.
	The decision further raised concerns about the politicization of the Supreme Court and its role in deciding elections.</p>

	<p>Supporters of the decision argue that it was necessary to provide legal clarity
	and prevent the recount from being conducted in a manner that violated the equal protection clause of the Fourteenth Amendment.</p>
	HTML;

$div_wikipedia_Bush_v_Gore = new WikipediaContentSection();
$div_wikipedia_Bush_v_Gore->setTitleText("Bush v Gore");
$div_wikipedia_Bush_v_Gore->setTitleLink("https://en.wikipedia.org/wiki/Bush_v._Gore");
$div_wikipedia_Bush_v_Gore->content = <<<HTML
	<p>Bush v. Gore, 531 U.S. 98 (2000), was a landmark decision of the United States Supreme Court on December 12, 2000,
	that settled a recount dispute in Florida's 2000 presidential election between George W. Bush and Al Gore.
	On December 8, the Florida Supreme Court had ordered a statewide recount of all undervotes, over 61,000 ballots that the vote tabulation machines had missed.
	The Bush campaign immediately asked the U.S. Supreme Court to stay the decision and halt the recount.</p>

	<p>In a 5–4 per curiam decision, the Court ruled, strictly on equal protection grounds, that the recount be stopped.
	Specifically, it held that the use of different standards of counting in different counties violated the Equal Protection Clause of the U.S. Constitution;
	the case had also been argued on Article II jurisdictional grounds, which found favor with only Justices Antonin Scalia, Clarence Thomas, and William Rehnquist.</p>

	<p>The Supreme Court's decision in Bush v. Gore was among the most controversial in U.S. history,
	as it allowed Florida Secretary of State Katherine Harris's vote certification to stand, giving Bush Florida's 25 electoral votes.
	Florida's votes gave Bush, the Republican nominee, 271 electoral votes, one more than the 270 required to win the Electoral College.
	This meant the defeat of Democratic candidate Al Gore, who won 267 electoral votes but received 266, as a "faithless elector" from the District of Columbia abstained from voting.
	Media organizations later analyzed the ballots and found that, under specified criteria, the original, limited recount of undervotes of several large counties
	would have resulted in a Bush victory, though a statewide recount would have shown that Gore received the most votes, according to the Florida Ballot Project.
	Florida later retired the punch-card voting machines that produced the ballots disputed in the case.</p>
	HTML;


$page->parent('supreme_court_of_the_united_states.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_wikipedia_Bush_v_Gore);
