<?php
$page = new Page();
$page->h1("Approval Voting: A Simple Path to Stronger Democracies");
$page->viewport_background("/free/approval_voting.png");
$page->tags("Elections: Solution", "Voting Method");
$page->keywords("Approval Voting", "approval voting");
$page->stars(3);

//$page->snp("description", "");
$page->snp("image",       "/free/approval_voting.1200-630.png");

$page->preview( <<<HTML
	<p>Tired of choosing the "lesser of two evils"?
	Approval Voting offers a refreshingly simple way to empower voters and elect candidates who truly represent their values.
	This powerful alternative can strengthen our democracies.</p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Are our $elections truly representing the will of the people?
	Are we often stuck choosing between the "lesser of two evils,"
	feeling like our vote is a compromise rather than a genuine expression of our preferences?
	At the heart of our project is the belief that everyone deserves a voice,
	and Approval Voting is a key step towards achieving that goal.
	Approval Voting offers a way to break free from the constraints of single-choice voting
	and empower voters to support the candidates they truly believe in.
	It’s a simple, yet profound, shift that can lead to more representative and effective government.</p>
	HTML;


$div_What_is_Approval_Voting = new ContentSection();
$div_What_is_Approval_Voting->content = <<<HTML
	<h3>What is Approval Voting?</h3>

	<p>Approval Voting is a voting system where voters can select (approve of) as many candidates as they wish on the ballot.
	The candidate with the most "approval" votes wins.
	It is strikingly different from the more common "choose-one" voting methods where voters can only select a single candidate.
	Approval Voting empowers voters to express their true feelings
	and preferences without the fear of "wasting" their vote.</p>

	<p>Here's how it works:</p>

	<ol>
	<li>Voters receive a ballot listing all candidates.</li>

	<li>Voters mark each candidate they approve of (e.g., with a checkmark, an 'X', or by filling in a bubble).
	There is no ranking involved.</li>

	<li>Election officials count the votes. The candidate with the most approvals wins.</li>

	</ol>
	HTML;


$div_Approval_Voting_has_some_very_compelling_benefits = new ContentSection();
$div_Approval_Voting_has_some_very_compelling_benefits->content = <<<HTML
	<h3>Approval Voting has some very compelling benefits</h3>

	<p><strong>•  Simplicity:</strong>
	It's easy to understand and use for both voters and election administrators. A simple X or checkmark is all it takes.</p>

	<p><strong>•  Flexibility:</strong>
	Voters can express support for multiple candidates without feeling like they are wasting their vote on someone without a chance.
	This leads to greater voter satisfaction.</p>

	<p><strong>•  Focus on Positive Choice:</strong>
	It encourages voters to choose candidates they support, not just those they dislike the least.
	This results in less negative campaigning and a more positive outcome.</p>

	HTML;



$div_Approval_Voting_in_Multi_Seat_Districts = new ContentSection();
$div_Approval_Voting_in_Multi_Seat_Districts->content = <<<HTML
	<h3>Approval Voting in Multi-Seat Districts</h3>

	<p>Approval Voting isn't limited to single-winner elections.
	It can be adapted for districts with multiple representatives, enabling a system where groups of people are represented fairly.</p>

	<p>In a multi-seat election, voters still approve of as many candidates as they like.
	The candidates with the most approval votes win the available seats.
	Imagine a city council election with five open seats;
	voters simply mark all the candidates they find acceptable, and the five candidates with the most votes are elected.</p>

	<p>This approach can lead to greater representation of diverse viewpoints and reduces the risk of a single party dominating all seats.
	This is an easy path to more proportional representation.</p>
	HTML;



$div_Implementation_Considerations = new ContentSection();
$div_Implementation_Considerations->content = <<<HTML
	<h3>Implementation Considerations</h3>

	<p>Implementing Approval Voting is surprisingly straightforward.</p>

	<p><strong>•  Ballot Design:</strong>
	Approval Voting ballots are simple and intuitive.
	They list all candidates with a space to mark approval (e.g., a checkbox or bubble) next to each name.</p>

	<p><strong>•  Counting Methods:</strong>
	Tabulation is simple: just count the number of approvals for each candidate.
	Existing election equipment can be easily adapted to accommodate approval voting.</p>

	<p><strong>•  Technology:</strong>
	Approval Voting can be implemented using existing voting machines or with paper ballots.
	This provides a low barrier to entry and can be adopted quickly.</p>

	<p><strong>•  Voter Education:</strong>
	As with any electoral system, clear and concise voter education materials
	are essential to ensure voters understand how to use the system.
	This ensures voter confidence and reduces errors.</p>
	HTML;



$div_Examples_of_Approval_Voting = new ContentSection();
$div_Examples_of_Approval_Voting->content = <<<HTML
	<h3>Examples of Approval Voting</h3>

	<p>While Approval Voting may seem radical, it has been successfully implemented in various contexts.</p>

	<h4>Real-World Examples</h4>

	<p>The city of Fargo, North Dakota, has been using Approval Voting since 2018 and reports high voter satisfaction.</p>

	<h4>Hypothetical Election Scenarios</h4>

	<p>Imagine an election with three candidates: A, B, and C.
	In a "choose-one" system, voters might feel forced to vote strategically, rather than for their true preference.
	With Approval Voting, they can support both A and B, or even all three, if they find them acceptable.
	This leads to more representative results.</p>

	<p>In a multi-seat election, Approval Voting allows voters to support candidates from different parties or factions,
	ensuring that a wider range of voices are represented.
	It is no longer an "all-or-nothing" affair.</p>

	<p>Consider an election where a popular candidate is also divisive.
	Approval Voting could allow a more moderate, consensus-oriented candidate to emerge as the winner.
	This can result in more stable governance.</p>
	HTML;



$div_Benefits_of_Approval_Voting_for_Democracies = new ContentSection();
$div_Benefits_of_Approval_Voting_for_Democracies->content = <<<HTML
	<h3>Benefits of Approval Voting for Democracies</h3>

	<p>Approval Voting offers a wide array of benefits to democracies:</p>

	<p><strong>•  Mitigating the ${'Duverger Syndrome'}:</strong>
	Approval Voting directly addresses the problems of two-party dominance by reducing spoiler effects
	and encouraging candidates to appeal to a broader base.
	It helps break the cycle of divisive politics.</p>

	<p><strong>•  Promoting Consensus:</strong>
	It encourages candidates to seek common ground and build coalitions, rather than focusing on divisive rhetoric.
	Candidates need to appeal to a broader spectrum of voters, and must listen and be open to compromise.</p>

	<p><strong>•  Increasing Voter Satisfaction:</strong>
	Voters are more likely to feel their vote counts when they can express support for multiple candidates.
	This results in increased voter turnout and participation.</p>

	<p><strong>•  Strengthening Democracy:</strong>
	By empowering voters and promoting more representative outcomes,
	Approval Voting strengthens the foundations of democracy and ensures that governments are more accountable to the people.</p>

	<p>Approval Voting perfectly aligns with our framework for a better democracy:
	Individual freedom, social responsibility and the need to make our societies as democratic as possible.
	It is an excellent step in the right direction.</p>
	HTML;




$div_Addressing_Potential_Concerns = new ContentSection();
$div_Addressing_Potential_Concerns->content = <<<HTML
	<h3>Addressing Potential Concerns</h3>

	<p>As with any electoral system, Approval Voting is not without potential concerns.</p>

	<p><strong>•  Strategic Voting:</strong>
	While strategic voting is possible with any system, Approval Voting minimizes the incentive to vote strategically rather than honestly.
	Voters can still express their preferences for candidates they genuinely support.</p>

	<p><strong>•  "Compromise Candidate" Wins:</strong>
	While a broadly acceptable candidate might win, this can be a positive outcome if it leads to more stable and effective governance.
	It can encourage candidates to work together for the common good.</p>

	<p><strong>•  Voter Education:</strong>
	As with any change to the electoral system, effective voter education is crucial to address potential confusion
	and ensure that voters understand how to use the system.
	Informed voters are empowered voters.</p>
	HTML;


$div_Conclusion = new ContentSection();
$div_Conclusion->content = <<<HTML
	<h3>Conclusion</h3>

	<p>Approval Voting offers a refreshingly simple, yet profoundly effective, path to stronger democracies.
	It empowers voters, promotes consensus, and mitigates the pitfalls of traditional voting systems.
	As part of our $Pilgrimage, we believe that exploring and implementing innovative solutions like Approval Voting
	is essential for building a more just and representative world.</p>

	<p>We encourage you to learn more about Approval Voting, get involved in advocating for electoral reform,
	and help us build a stronger democracy for all.
	Please contribute to this project with new ideas and information.</p>
	HTML;




$div_wikipedia_Approval_voting = new WikipediaContentSection();
$div_wikipedia_Approval_voting->setTitleText("Approval voting");
$div_wikipedia_Approval_voting->setTitleLink("https://en.wikipedia.org/wiki/Approval_voting");
$div_wikipedia_Approval_voting->content = <<<HTML
	<p>Approval voting is a single-winner electoral system in which voters mark all the candidates they support, instead of just choosing one.
	The candidate with the highest approval rating is elected.</p>
	HTML;


$page->parent('voting_methods.html');
$page->next('score_voting.html');

$page->body($div_introduction);
$page->body($div_What_is_Approval_Voting);
$page->body($div_Approval_Voting_has_some_very_compelling_benefits);
$page->body($div_Approval_Voting_in_Multi_Seat_Districts);
$page->body($div_Implementation_Considerations);
$page->body($div_Examples_of_Approval_Voting);
$page->body($div_Benefits_of_Approval_Voting_for_Democracies);
$page->body($div_Addressing_Potential_Concerns);
$page->body($div_Conclusion);

$page->related_tag("Approval Voting");

$page->body($div_wikipedia_Approval_voting);
