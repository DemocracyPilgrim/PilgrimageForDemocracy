<?php
$page = new OrganisationPage();
$page->h1("Systemic Justice Project");
$page->viewport_background("");
$page->keywords("Systemic Justice Project");
$page->stars(0);
$page->tags("Organisation", "Harvard Law School", "Institutions: Judiciary", "Justice");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

$r1 = $page->ref("https://systemicjustice.org/about-us/", "About The Systemic Justice Project");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;




$div_Systemic_Justice_Project = new WebsiteContentSection();
$div_Systemic_Justice_Project->setTitleText("Systemic Justice Project ");
$div_Systemic_Justice_Project->setTitleLink("https://systemicjustice.org/");
$div_Systemic_Justice_Project->content = <<<HTML
	<p>The Systemic Justice Project (SJP) is designed as an alternative to the traditional legal-educational model. It takes a problem-centric, not law-centric approach, and works with organizations, lawyers, law students, organizers, activists, journalists, and artists around the U.S. Through its courses, initiatives, collaborations, and student-created documentaries, podcasts, and publications, provides a unique alternative to the conventional means and ends of legal education. $r1</p>
	HTML;



$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Systemic Justice Project");

$page->body($div_Systemic_Justice_Project);
