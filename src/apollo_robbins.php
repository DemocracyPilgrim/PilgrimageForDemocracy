<?php
$page = new PersonPage();
$page->h1("Apollo Robbins");
$page->alpha_sort("Robbins, Apollo");
$page->tags("Person");
$page->keywords("Apollo Robbins");
$page->stars(0);
$page->viewport_background("/free/apollo_robbins.png");

$page->snp("description", "An honest thief and ethical trickster.");
$page->snp("image",       "/free/apollo_robbins.1200-630.png");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Apollo Robbins is an honest thief and ethical trickster from $America.</p>

	<p>His ${"TED Talk: The art of misdirection"} is featured in the introduction to the ${'fifth level of democracy'},
	to illustrate the power of the $media and ${'social networks'} over our $social and $political awareness.</p>
	HTML;

$div_wikipedia_Apollo_Robbins = new WikipediaContentSection();
$div_wikipedia_Apollo_Robbins->setTitleText("Apollo Robbins");
$div_wikipedia_Apollo_Robbins->setTitleLink("https://en.wikipedia.org/wiki/Apollo_Robbins");
$div_wikipedia_Apollo_Robbins->content = <<<HTML
	<p>Apollo Robbins is an American sleight-of-hand artist, security consultant, self-described gentleman thief and deception specialist. Forbes has called him "an artful manipulator of awareness".</p>
	HTML;

$div_youtube_Apollo_Robbins_Demonstrates_the_Technique_of_a_Master_Pickpocket_The_New_Yorker = new YoutubeContentSection();
$div_youtube_Apollo_Robbins_Demonstrates_the_Technique_of_a_Master_Pickpocket_The_New_Yorker->setTitleText("Apollo Robbins Demonstrates the Technique of a Master Pickpocket | The New Yorker");
$div_youtube_Apollo_Robbins_Demonstrates_the_Technique_of_a_Master_Pickpocket_The_New_Yorker->setTitleLink("https://www.youtube.com/watch?v=LoUSO_Mj1TQ");



$div_youtube_Apollo_Robbins_Great_pickpocketing_routine = new YoutubeContentSection();
$div_youtube_Apollo_Robbins_Great_pickpocketing_routine->setTitleText("Apollo Robbins - Great pickpocketing routine in the street");
$div_youtube_Apollo_Robbins_Great_pickpocketing_routine->setTitleLink("https://www.youtube.com/watch?v=VKZlesut-Pk");


$page->parent('list_of_people.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Apollo Robbins");
$page->body($div_youtube_Apollo_Robbins_Demonstrates_the_Technique_of_a_Master_Pickpocket_The_New_Yorker);
$page->body($div_youtube_Apollo_Robbins_Great_pickpocketing_routine);
$page->body($div_wikipedia_Apollo_Robbins);
