<?php
$page = new Page();
$page->h1('Global Issues');
$page->stars(0);
$page->tags("International");

$page->preview( <<<HTML
	<p>We live in an interconnected world, with issues affecting the whole of humanity.</p>
	HTML );

$page->snp('description', "We live in an interconnected world, with issues affecting the whole of humanity.");
$page->snp('image', "/copyrighted/ben-white-gEKMstKfZ6w.1200-630.jpg");


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	We live in an interconnected world, with issues affecting the whole of humanity.
	HTML;

$list = new ListOfPages();
$list->add('counterinsurgency_mathematics.html');
$list->add('education.html');
$list->add('hunger.html');
$list->add('global_natural_resources.html');
$list->add('peaceful_resistance_in_times_of_war.html');
$list->add('living_together.html');
$list->add('second_level_of_democracy.html');
$print_list = $list->print();

$div_list = new ContentSection();
$div_list->content = <<<HTML
	<h3>Related content</h3>

	$print_list
	HTML;


$page->parent('international.html');
$page->template("stub");
$page->body($div_introduction);

$page->body($div_list);
