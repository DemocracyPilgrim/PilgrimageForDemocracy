<?php
$page = new Page();
$page->h1("U.S. states");
$page->tags("International", "USA");
$page->keywords("US states", "US State");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_U_S_state = new WikipediaContentSection();
$div_wikipedia_U_S_state->setTitleText("U S state");
$div_wikipedia_U_S_state->setTitleLink("https://en.wikipedia.org/wiki/U.S._state");
$div_wikipedia_U_S_state->content = <<<HTML
	<p>In the United States, a state is a constituent political entity, of which there are 50. Bound together in a political union, each state holds governmental jurisdiction over a separate and defined geographic territory where it shares its sovereignty with the federal government. Due to this shared sovereignty, Americans are citizens both of the federal republic and of the state in which they reside.</p>
	HTML;


$page->parent('united_states.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("US State");
$page->body($div_wikipedia_U_S_state);
