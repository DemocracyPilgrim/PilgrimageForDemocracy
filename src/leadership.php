<?php
$page = new Page();
$page->h1("Leadership");
$page->viewport_background("");
$page->keywords("Leadership");
$page->stars(0);
$page->tags("Elections");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Leadership = new WikipediaContentSection();
$div_wikipedia_Leadership->setTitleText("Leadership");
$div_wikipedia_Leadership->setTitleLink("https://en.wikipedia.org/wiki/Leadership");
$div_wikipedia_Leadership->content = <<<HTML
	<p>Leadership, is defined as the ability of an individual, group, or organization
	to "lead", influence, or guide other individuals, teams, or organizations.</p>
	HTML;


$page->parent('elections.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Leadership");
$page->body($div_wikipedia_Leadership);
