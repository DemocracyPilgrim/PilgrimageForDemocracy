<?php
$page = new OrganisationPage();
$page->h1("Berkman Klein Center for Internet & Society");
$page->viewport_background("");
$page->keywords("Berkman Klein Center for Internet & Society");
$page->stars(0);
$page->tags("Organisation", "The Web Defenders", "Social Networks", "Harvard Law School");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>The Berkman Klein Center for Internet & Society is a research center at ${"Harvard Law School"}.</p>
	HTML;



$div_Berkman_Klein_Center_for_Internet_and_Society = new WebsiteContentSection();
$div_Berkman_Klein_Center_for_Internet_and_Society->setTitleText("Berkman Klein Center for Internet & Society ");
$div_Berkman_Klein_Center_for_Internet_and_Society->setTitleLink("https://cyber.harvard.edu/");
$div_Berkman_Klein_Center_for_Internet_and_Society->content = <<<HTML
	<p>The Berkman Klein Center's mission is to explore and understand cyberspace; to study its development, dynamics, norms, and standards; and to assess the need or lack thereof for laws and sanctions.
	We are a research center, premised on the observation that what we seek to learn is not already recorded. Our method is to build out into cyberspace, record data as we go, self-study, and share. Our mode is entrepreneurial nonprofit.</p>
	HTML;




$div_wikipedia_Berkman_Klein_Center_for_Internet_Society = new WikipediaContentSection();
$div_wikipedia_Berkman_Klein_Center_for_Internet_Society->setTitleText("Berkman Klein Center for Internet Society");
$div_wikipedia_Berkman_Klein_Center_for_Internet_Society->setTitleLink("https://en.wikipedia.org/wiki/Berkman_Klein_Center_for_Internet_&_Society");
$div_wikipedia_Berkman_Klein_Center_for_Internet_Society->content = <<<HTML
	<p>The Berkman Klein Center for Internet & Society is a research center at Harvard University that focuses on the study of cyberspace. Founded at Harvard Law School, the center traditionally focused on internet-related legal issues. On May 15, 2008, the center was elevated to an interfaculty initiative of Harvard University as a whole. It is named after the Berkman family. On July 5, 2016, the center added "Klein" to its name following a gift of $15 million from Michael R. Klein.</p>
	HTML;


$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Berkman Klein Center for Internet & Society");

$page->body($div_Berkman_Klein_Center_for_Internet_and_Society);
$page->body($div_wikipedia_Berkman_Klein_Center_for_Internet_Society);
