<?php
$page = new Page();
$page->h1("Nobel Peace Prize");
$page->keywords("Nobel Peace Prize");
$page->tags("Award", "Peace", "International", "Humanity", "Society");
$page->stars(0);

//$page->snp("description", "");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$list = new ListOfPages();
$list->add('narges_mohammadi.html');
$print_list = $list->print();

$div_list = new ContentSection();
$div_list->content = <<<HTML
	<h3>Related content</h3>

	$print_list
	HTML;

$div_wikipedia_Nobel_Peace_Prize = new WikipediaContentSection();
$div_wikipedia_Nobel_Peace_Prize->setTitleText("Nobel Peace Prize");
$div_wikipedia_Nobel_Peace_Prize->setTitleLink("https://en.wikipedia.org/wiki/Nobel_Peace_Prize");
$div_wikipedia_Nobel_Peace_Prize->content = <<<HTML
	<p>The Nobel Peace Prize is one of the five Nobel Prizes established
	by the will of Swedish industrialist, inventor and armaments manufacturer Alfred Nobel,
	along with the prizes in Chemistry, Physics, Physiology or Medicine and Literature.</p>
	HTML;

$div_wikipedia_List_of_Nobel_Peace_Prize_laureates = new WikipediaContentSection();
$div_wikipedia_List_of_Nobel_Peace_Prize_laureates->setTitleText("List of Nobel Peace Prize laureates");
$div_wikipedia_List_of_Nobel_Peace_Prize_laureates->setTitleLink("https://en.wikipedia.org/wiki/List_of_Nobel_Peace_Prize_laureates");
$div_wikipedia_List_of_Nobel_Peace_Prize_laureates->content = <<<HTML
	<p>The Norwegian Nobel Committee awards the Nobel Peace Prize annually
	"to the person who shall have done the most or the best work for fraternity between nations,
	for the abolition or reduction of standing armies and for the holding and promotion of peace congresses".</p>
	HTML;


$page->parent('list_of_awards.html');
$page->template("stub");
$page->body($div_introduction);

$page->body($div_list);

$page->body($div_wikipedia_Nobel_Peace_Prize);
$page->body($div_wikipedia_List_of_Nobel_Peace_Prize_laureates);
