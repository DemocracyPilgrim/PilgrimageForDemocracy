<?php
$page = new Page();
$page->h1("Election official (poll worker)");
$page->tags("Elections", "Electoral System");
$page->keywords("election official", "poll worker");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

$r1 = $page->ref("https://www.americanbar.org/news/abanews/aba-news-archives/2024/09/aba-recruiting-lawyer-poll-workers/", "ABA recruiting lawyers to volunteer as poll workers");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>In a difficult electoral context in the $USA, the ${'American Bar Association'}, for the third consecutive election cycle,
	have issued a call aimed at mobilizing lawyers to assist as poll workers for the upcoming election.$r1</p>
	HTML;

$div_wikipedia_Election_official = new WikipediaContentSection();
$div_wikipedia_Election_official->setTitleText("Election official");
$div_wikipedia_Election_official->setTitleLink("https://en.wikipedia.org/wiki/Election_official");
$div_wikipedia_Election_official->content = <<<HTML
	<p>An election official, election officer, election judge, election clerk, or poll worker is an official responsible for the proper and orderly voting at polling stations.
	Depending on the country or jurisdiction, election officials may be identified as members of a political party or non-partisan.
	They are generally volunteers or paid a small stipend for their work.
	Each polling station is staffed with multiple officials.
	The duties include signing in registered voters, explaining voting procedure and use of voting equipment, providing ballots, and monitoring the conduct of the election.</p>
	HTML;




$div_Poll_Worker_Esq = new WebsiteContentSection();
$div_Poll_Worker_Esq->setTitleText("American Bar Association: Poll Worker, Esq. ");
$div_Poll_Worker_Esq->setTitleLink("https://www.americanbar.org/groups/public_interest/election_law/poll-worker-esq/");
$div_Poll_Worker_Esq->content = <<<HTML
	<p>The Standing Committee on Election Law is thankful to all of the poll workers across the country who make elections safe and fair.
	The poll worker’s job is never easy, but recent years have been particularly challenging.
	We owe those who volunteer a special debt of gratitude.
	Among them are many ABA members who signed up through our Poll Worker, Esq. program.
	But whether they signed up through the ABA or not, poll workers are the unsung heroes of elections.
	Please join the effort by volunteering to serve as a poll worker in your community’s next election.</p>
	HTML;



$page->parent('electoral_system.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_Poll_Worker_Esq);
$page->body($div_wikipedia_Election_official);
