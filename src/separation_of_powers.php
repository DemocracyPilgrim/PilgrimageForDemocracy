<?php
$page = new Page();
$page->h1('Separation of powers');
$page->keywords('Separation of Powers', 'separation of powers');
$page->stars(1);
$page->viewport_background('/free/separation_of_powers.png');

$page->preview( <<<HTML
	<p></p>
	HTML );

$page->snp('description', 'A cornerstone of democratic institutions.');
$page->snp('image',       '/free/separation_of_powers.1200-630.png');



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<h3>Separation of Powers: Safeguarding Democracy through Institutional Balance</h3>

	<p>Separation of powers is a cornerstone of any robust $democratic system,
	designed to prevent the concentration of power in the hands of any one individual or ${'branch of government'}.
	This vital principle ensures a balance of authority and prevents tyranny by dividing governmental functions into distinct, yet interdependent, branches:
	the $legislative, executive, and $judicial.</p>

	<p>In practical terms, this separation is institutionalized through a system of checks and balances.</p>

	<ul>
		<li><strong>The legislative branch</strong>, typically a parliament or congress, is responsible for making laws.</li>
		<li><strong>The executive branch</strong>, led by the president or prime minister, enforces those laws.</li>
		<li><strong>The judicial branch</strong>, comprised of courts and judges, interprets the laws and ensures their fair application. </li>
	</ul>

	<p>Each branch possesses specific powers and limitations, preventing any one branch from dominating the others.
	The legislative branch can propose laws, but the executive branch has the power to veto them.
	The judiciary can strike down laws deemed unconstitutional, while the executive branch appoints judges.</p>

	<p>This intricate system of checks and balances prevents any one branch from becoming too powerful
	and ensures that each branch remains accountable to the others.
	This interconnectedness ensures that the government operates within a framework of shared responsibility and accountability,
	safeguarding the core principles of democracy and protecting individual liberties.</p>

	<p>In the ${'Republic of China'} ($Taiwan), there exist two additional ${'branches of government'} (the Control Yuan and the Examination Yuan),
	which provide further checks within the government institutions.</p>

	<p>We ought to pay special attention of attempts at ${'obstruction of justice'}
	by one branch (legislative or executive) over the $judiciary.</p>
	HTML;


$list_Separation_of_Powers = ListOfPeoplePages::WithTags("Separation of Powers");
$print_list_Separation_of_Powers = $list_Separation_of_Powers->print();

$div_list_Separation_of_Powers = new ContentSection();
$div_list_Separation_of_Powers->content = <<<HTML
	<h3>Related content</h3>

	$print_list_Separation_of_Powers
	HTML;




$div_wikipedia_Separation_of_powers = new WikipediaContentSection();
$div_wikipedia_Separation_of_powers->setTitleText('Separation of powers');
$div_wikipedia_Separation_of_powers->setTitleLink('https://en.wikipedia.org/wiki/Separation_of_powers');
$div_wikipedia_Separation_of_powers->content = <<<HTML
	<p>Separation of powers refers to the division of a state's government into "branches",
	each with separate, independent powers and responsibilities,
	so that the powers of one branch are not in conflict with those of the other branches.</p>
	HTML;


$page->parent('institutions.html');

$page->template("stub");
$page->body($div_introduction);
$page->body($div_list_Separation_of_Powers);

$page->body($div_wikipedia_Separation_of_powers);
