<?php
$page = new Page();
$page->h1("Advance Democracy");
$page->keywords("Advance Democracy");
$page->tags("Organisation");
$page->stars(0);

//$page->snp("description", "");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref('', '');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;



$div_Advance_Democracy_website = new WebsiteContentSection();
$div_Advance_Democracy_website->setTitleText("Advance Democracy website ");
$div_Advance_Democracy_website->setTitleLink("https://advancedemocracy.org/");
$div_Advance_Democracy_website->content = <<<HTML
	<p><strong>The Climate & Environment Program:</strong>
	Advance Democracy, Inc. investigates entities undermining the global consensus on climate change
	through financial corruption, clandestine front-groups, social media influence operations, disinformation,
	vexatious litigation, lobbying, and front-facing public relations activity.</p>

	<p><strong>The Government Accountability & Corruption Program:</strong>
	Advance Democracy, Inc. uses our global network of researchers to uncover and document corrupt actions of governments,
	elected officials, and political entities.
	Outputs of this research are shared with government oversight bodies, not-for-profit accountability groups, and prominent media organizations.
	A component of this program are the ADI Accountability Dashboards, which help the media, researchers, and citizens
	track political donations through the U.S. campaign finance system.</p>

	<p><strong>The Democracy Program:</strong>
	Advance Democracy, Inc. uncovers and details efforts to undermine democratic institutions and the democratic process.
	We focus our research on voter suppression efforts, the spread of political disinformation,
	financial corruption, press freedoms, and human rights.</strong>

	<p><strong>The Global Extremism Program:</strong>
	Advance Democracy, Inc. uses our global investigative capabilities to map and expose the support networks behind extremist groups
	threatening the United States and its allies.
	We work to identify and document the growth, reach, and influence of violent extremist groups,
	as well as political rhetoric in the United States and Europe targeting communities of color, immigrants, the LBGTQ population, and religious minorities
	that has contributed to a surge in global hate crimes.
	Our research also includes the documentation of gender-based discrimination, anti-Asian hate crimes, and antisemitic incidents.</p>

	<p><strong>The Public Health Program:</strong>
	Disinformation and misinformation about public health matters are rampant and deadly.
	Advance Democracy, Inc. maps and exposes the network of state actors, fringe media outlets, businesses, organizations, and individuals
	that undermine efforts to respond to public health emergencies, including the COVID-19 pandemic and the opioid epidemic.</p>
	HTML;




$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);

$page->body($div_Advance_Democracy_website);
