<?php
$page = new CountryPage('Malaysia');
$page->h1('Malaysia');
$page->tags("Country");
$page->keywords('Malaysia');
$page->stars(0);

$page->snp('description', '33,2 million inhabitants.');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>In 2023, Malaysia rejected the latest edition of the "standard map of $China"
	that lays claim to almost the entire South China Sea,
	including areas lying off the coast of Malaysian Borneo.</p>

	<p>As Malaysia continues to explore for oil and gas off Borneo,
	it must contend with Chinese threats and intimidation,
	as well as incursion of Chinese vessels into its exclusive economic zone.</p>

	<p>See: ${'Chinese expansionism'}.</p>
	HTML;

$div_wikipedia_Malaysia = new WikipediaContentSection();
$div_wikipedia_Malaysia->setTitleText('Malaysia');
$div_wikipedia_Malaysia->setTitleLink('https://en.wikipedia.org/wiki/Malaysia');
$div_wikipedia_Malaysia->content = <<<HTML
	<p>Malaysia is a country in Southeast Asia.
	The federal constitutional monarchy consists of thirteen states and three federal territories,
	separated by the South China Sea into two regions: Peninsular Malaysia and Borneo's East Malaysia.</p>
	HTML;

$div_wikipedia_Human_rights_in_Malaysia = new WikipediaContentSection();
$div_wikipedia_Human_rights_in_Malaysia->setTitleText('Human rights in Malaysia');
$div_wikipedia_Human_rights_in_Malaysia->setTitleLink('https://en.wikipedia.org/wiki/Human_rights_in_Malaysia');
$div_wikipedia_Human_rights_in_Malaysia->content = <<<HTML
	<p>The protection of basic human rights is enshrined in Constitution of Malaysia.
	These include liberty of the person (Article 5) and prohibition of slavery and forced labour (Article 6).
	At the national level, legislative measures that exist to prevent human rights violations and abuses
	can be found in acts and laws on issues that either have a human rights component or relate to certain groups of society
	whose rights may be at risk of being violated.
	Human rights groups are generally critical of the Malaysian government and the Royal Malaysia Police.
	Preventive detention laws such as the Internal Security Act and the Emergency (Public Order and Prevention of Crime) Ordinance 1969
	allow for detention without trial or charge and as such are a source of concern for human rights organizations like Suara Rakyat Malaysia.</p>
	HTML;


$page->parent('world.html');
$page->template("stub");
$page->body($div_introduction);
$page->body('Country indices');

$page->body($div_wikipedia_Malaysia);
$page->body($div_wikipedia_Human_rights_in_Malaysia);
