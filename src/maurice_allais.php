<?php
$page = new PersonPage();
$page->h1('Maurice Allais');
$page->alpha_sort("Allais, Maurice");
$page->keywords('Maurice Allais');
$page->stars(0);
$page->tags("Person", "Fair Share");

$page->snp('description', 'French economist.');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p>Maurice Allais was a French physicist and economist, 1988 winner of the Nobel Memorial Prize in Economic Sciences.</p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Maurice Allais was a French physicist and economist, 1988 winner of the Nobel Memorial Prize in Economic Sciences.</p>

	<blockquote>
	"Without any exaggeration, the current mechanism of money creation through credit
	is certainly the 'cancer' that's irretrievably eroding market economies of private property."
	</blockquote>
	HTML;

$div_wikipedia_Maurice_Allais = new WikipediaContentSection();
$div_wikipedia_Maurice_Allais->setTitleText('Maurice Allais');
$div_wikipedia_Maurice_Allais->setTitleLink('https://en.wikipedia.org/wiki/Maurice_Allais');
$div_wikipedia_Maurice_Allais->content = <<<HTML
	<p>Maurice Allais was a French physicist and economist,
	the 1988 winner of the Nobel Memorial Prize in Economic Sciences "for his pioneering contributions to the theory of markets and efficient utilization of resources",
	along with John Hicks (Value and Capital, 1939)
	and Paul Samuelson (The Foundations of Economic Analysis, 1947), to neoclassical synthesis.
	They formalize the self-regulation of markets, that Keynes refuted, while reiterating some of his ideas.</p>
	HTML;


$page->parent('list_of_people.html');
$page->template("stub");
$page->body($div_introduction);

$page->body('fair_share.html');

$page->body($div_wikipedia_Maurice_Allais);
