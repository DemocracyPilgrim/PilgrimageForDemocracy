<?php
$page = new Page();
$page->h1("Roe v. Wade (1973)");
$page->tags("Institutions: Judiciary", "SCOTUS Landmark Decision", "USA", "Elections", "Women's Rights");
$page->keywords("Roe v. Wade");
$page->stars(1);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>"Roe v. Wade" is a 1973 landmark decision by the ${'Supreme Court of the United States'}.</p>

	<p>This landmark case established a constitutional right to abortion, recognizing a woman's right to privacy in making decisions about her own body.</p>

	<p>This decision sparked a major national debate about abortion rights, but it remains a cornerstone of reproductive rights for women.
	It has had a profound impact on women's autonomy and reproductive healthcare.</p>
	HTML;

$div_wikipedia_Roe_v_Wade = new WikipediaContentSection();
$div_wikipedia_Roe_v_Wade->setTitleText("Roe v Wade");
$div_wikipedia_Roe_v_Wade->setTitleLink("https://en.wikipedia.org/wiki/Roe_v._Wade");
$div_wikipedia_Roe_v_Wade->content = <<<HTML
	<p>Roe v. Wade, 410 U.S. 113 (1973), was a landmark decision of the U.S. Supreme Court
	in which the Court ruled that the Constitution of the United States generally protected a right to have an abortion.
	The decision struck down many abortion laws, and caused an ongoing abortion debate in the United States about whether, or to what extent, abortion should be legal,
	who should decide the legality of abortion, and what the role of moral and religious views in the political sphere should be.
	The decision also shaped debate concerning which methods the Supreme Court should use in constitutional adjudication.
	The Supreme Court overruled Roe in 2022, ending the constitutional right to abortion.</p>
	HTML;


$page->parent('supreme_court_of_the_united_states.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_wikipedia_Roe_v_Wade);
