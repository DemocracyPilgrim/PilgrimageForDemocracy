<?php
$page = new Page();
$page->h1("American Friends Service Committee");
$page->tags("Organisation", "Humanity", "Immigration", "Society", "Living");
$page->keywords("American Friends Service Committee", "AFSC");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

$r1 = $page->ref("https://afsc.org/about", "About AFSC");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;



$div_American_Friends_Service_Committee = new WebsiteContentSection();
$div_American_Friends_Service_Committee->setTitleText("American Friends Service Committee ");
$div_American_Friends_Service_Committee->setTitleLink("https://afsc.org/");
$div_American_Friends_Service_Committee->content = <<<HTML
	<p>AFSC works for a just, peaceful, and sustainable world free of violence, inequality, and oppression.
	We join with people and partners worldwide to meet urgent community needs, challenge injustice, and build peace.</p>

	<p>Often found on the forefront of social change movements, AFSC pursues systemic change.
	In our work for a better future, we focus on three strategic goals - just and sustainable peace, just economies, and just responses to migration.</p>

	<p>Guided by the Quaker belief in the divine light of each person,
	AFSC works with people of all faiths and backgrounds to challenge unjust systems and promote lasting peace.$r1</p>
	HTML;



$div_wikipedia_American_Friends_Service_Committee = new WikipediaContentSection();
$div_wikipedia_American_Friends_Service_Committee->setTitleText("American Friends Service Committee");
$div_wikipedia_American_Friends_Service_Committee->setTitleLink("https://en.wikipedia.org/wiki/American_Friends_Service_Committee");
$div_wikipedia_American_Friends_Service_Committee->content = <<<HTML
	<p>The American Friends Service Committee (AFSC) is a Religious Society of Friends (Quaker)-founded organization
	working for peace and social justice in the United States and around the world.
	AFSC was founded in 1917 as a combined effort by American members of the Religious Society of Friends to assist civilian victims of World War I.
	It continued to engage in relief action in Europe and the Soviet Union after the Armistice of 1918.
	By the mid-1920s, AFSC focused on improving racial relations, immigration policy, and labor conditions in the U.S.,
	as well as exploring ways to prevent the outbreak of another conflict before and after World War II.
	As the Cold War developed, the organization began to employ more professionals rather than Quaker volunteers.
	Over time, it broadened its appeal and began to respond more forcefully to racial injustice, international peacebuilding,
	migration and refugee issues, women's issues, and the demands of sexual minorities for equal treatment.
	Currently, the organization's three priorities include work on peacebuilding, a focus on just economies, and humane responses to the global migration crisis.</p>
	HTML;


$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);

$page->body($div_American_Friends_Service_Committee);

$page->body($div_wikipedia_American_Friends_Service_Committee);
