<?php
$page = new Page();
$page->h1("Echo chamber");
$page->keywords("echo chamber");
$page->tags("Information: Discourse", "Political Discourse");
$page->stars(0);

//$page->snp("description", "");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref('', '');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>We need to get out of our echo chambers. Our democracies depend on it.</p>

	<p>How to get out of our echo chambers?</p>
	HTML;


$div_How_to_get_out_of_our_own_echo_chambers = new ContentSection();
$div_How_to_get_out_of_our_own_echo_chambers->content = <<<HTML
	<h3>How to get out of our own echo chambers?</h3>

	<p>The following list is only a starting point...</p>

	<ul>
		<li>Maybe we can start by recognizing that we may be as much within an echo chamber as others.</li>
		<li>Explicitly seek out other opinions.</li>
		<li>In social networks, stop using the downvote button as "I don't like this" or "I disagree with this".</li>
		<li>Stop reading / watching content you agree with (especially because it's from a source you generally agree with).</li>
		<li>Engage in actual, thoughtful discourse.</li>
	</p>
	HTML;



$div_wikipedia_Echo_chamber_media = new WikipediaContentSection();
$div_wikipedia_Echo_chamber_media->setTitleText("Echo chamber media");
$div_wikipedia_Echo_chamber_media->setTitleLink("https://en.wikipedia.org/wiki/Echo_chamber_(media)");
$div_wikipedia_Echo_chamber_media->content = <<<HTML
	<p>In news media and social media, an echo chamber is an environment or ecosystem
	in which participants encounter beliefs that amplify or reinforce their preexisting beliefs by communication and repetition
	inside a closed system and insulated from rebuttal.
	An echo chamber circulates existing views without encountering opposing views,
	potentially resulting in confirmation bias.
	Echo chambers may increase social and political polarization and extremism.
	On social media, it is thought that echo chambers limit exposure to diverse perspectives,
	and favor and reinforce presupposed narratives and ideologies.</p>
	HTML;


$page->parent('political_discourse.html');
$page->template("stub");
$page->body($div_introduction);

$page->body($div_How_to_get_out_of_our_own_echo_chambers);

$page->body($div_wikipedia_Echo_chamber_media);
