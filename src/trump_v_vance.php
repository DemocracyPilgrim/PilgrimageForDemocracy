<?php
$page = new Page();
$page->h1("Trump v. Vance (2020)");
$page->tags("Institutions: Judiciary", "SCOTUS Landmark Decision", "USA", "Donald Trump", "Immunity", "Separation of Powers");
$page->keywords("Trump v. Vance");
$page->stars(1);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>"Trump v. Vance" is a 2020 landmark decision by the ${'Supreme Court of the United States'}.</p>

	<p>The case involved a subpoena for Trump's financial records.
	It set a legal precedent related to executive privilege and presidential immunity.</p>

	<p>The Manhattan District Attorney's office subpoenaed Trump's accounting firm, Mazars USA,
	for financial records as part of an investigation into possible financial crimes.
	Trump claimed that the subpoena was an overreach of power and violated executive privilege, arguing that the records were protected from disclosure.</p>

	<p>The Supreme Court ruled against Trump, holding that the executive privilege doctrine did not shield him from complying with the subpoena.
	The Court affirmed that a sitting president does have limited immunity from criminal investigation and prosecution, but that this immunity is not absolute.
	The Court emphasized the importance of balancing the President's need for confidentiality with the need for transparency and accountability in the judicial process.</p>

	<p>The decision established an important precedent for future cases involving presidential immunity and the scope of executive privilege.
	It gave prosecutors more leeway in seeking evidence from presidents and their associates, potentially leading to more investigations into presidential misconduct.
	The decision was highly contentious, with some arguing that it undermined the separation of powers and others claiming that it was necessary to ensure accountability.</p>
	HTML;




$div_Oyez_Trump_v_Vance = new WebsiteContentSection();
$div_Oyez_Trump_v_Vance->setTitleText("Oyez: Trump v Vance ");
$div_Oyez_Trump_v_Vance->setTitleLink("https://www.oyez.org/cases/2019/19-635");
$div_Oyez_Trump_v_Vance->content = <<<HTML
	<p>Does the Constitution permit a county prosecutor to subpoena a third-party custodian for the financial and tax records of a sitting president,
	over which the president has no claim of executive privilege?</p>
	HTML;



$div_Oyez_Trump_v_Mazars_USA_LLP = new WebsiteContentSection();
$div_Oyez_Trump_v_Mazars_USA_LLP->setTitleText("Oyez: Trump v. Mazars USA, LLP ");
$div_Oyez_Trump_v_Mazars_USA_LLP->setTitleLink("https://www.oyez.org/cases/2019/19-715");
$div_Oyez_Trump_v_Mazars_USA_LLP->content = <<<HTML
	<p>Does the Constitution prohibit subpoenas issued to Donald Trump’s accounting firm requiring it to provide non-privileged financial records
	relating to Trump (as a private citizen) and some of his businesses?</p>
	HTML;



$div_wikipedia_Trump_v_Vance = new WikipediaContentSection();
$div_wikipedia_Trump_v_Vance->setTitleText("Trump v Vance");
$div_wikipedia_Trump_v_Vance->setTitleLink("https://en.wikipedia.org/wiki/Trump_v._Vance");
$div_wikipedia_Trump_v_Vance->content = <<<HTML
	<p>Trump v. Vance, 591 U.S. ___ (2020), was a landmark US Supreme Court case arising from a subpoena issued in August 2019
	by Manhattan District Attorney Cyrus Vance Jr. against Mazars, then-President Donald Trump's accounting firm,
	for Trump's tax records and related documents, as part of his ongoing investigation into the Stormy Daniels scandal.
	Trump commenced legal proceedings to prevent their release.</p>

	<p>The Court held that Article II and the Supremacy Clause of the US Constitution do not categorically preclude or require a heightened standard
	for the issuance of a state criminal subpoena to a sitting president.
	The 7–2 decision was issued in July 2020, with Justices Samuel Alito and Clarence Thomas dissenting.</p>
	HTML;


$page->parent('supreme_court_of_the_united_states.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_Oyez_Trump_v_Vance);
$page->body($div_Oyez_Trump_v_Mazars_USA_LLP);

$page->body($div_wikipedia_Trump_v_Vance);
