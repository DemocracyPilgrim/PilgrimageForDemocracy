<?php
$page = new PersonPage();
$page->h1("Ferdinand Marcos");
$page->alpha_sort("Marcos, Ferdinand");
$page->tags("International", "Philippines", "Danger", "History", "President");
$page->keywords("Ferdinand Marcos");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Ferdinand_Marcos = new WikipediaContentSection();
$div_wikipedia_Ferdinand_Marcos->setTitleText("Ferdinand Marcos");
$div_wikipedia_Ferdinand_Marcos->setTitleLink("https://en.wikipedia.org/wiki/Ferdinand_Marcos");
$div_wikipedia_Ferdinand_Marcos->content = <<<HTML
	<p>Ferdinand Emmanuel Edralin Marcos Sr. (1917 – 1989) was a Filipino lawyer, politician, dictator, and kleptocrat who was the tenth president of the Philippines, ruling from 1965 to 1986. Marcos ruled the country under martial law from 1972 to 1981. He enjoyed expanded powers under the 1973 Constitution. He was deposed by a nonviolent revolution in 1986. Marcos described his philosophy as "constitutional authoritarianism" under his Kilusang Bagong Lipunan (New Society Movement). The most controversial figure in Filipino history, Marcos's regime was infamous for corruption, extravagance, and brutality.</p>
	HTML;


$page->parent('list_of_people.html');
$page->parent('philippines.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Ferdinand Marcos");
$page->body($div_wikipedia_Ferdinand_Marcos);
