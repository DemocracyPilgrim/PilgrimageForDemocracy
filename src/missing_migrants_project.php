<?php
$page = new Page();
$page->h1("Missing Migrants Project");
$page->keywords("Missing Migrants Project", 'MMP');
$page->tags("Organisation", "Humanity", "Immigration", "International");
$page->stars(1);

$page->snp("description", "An International Organization for Migration initiative.");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p>Missing Migrants Project is an initiative of the ${'International Organization for Migration'} (IOM)
	to document deaths and disappearances of migrants.</p>
	HTML );


$r1 = $page->ref('https://missingmigrants.iom.int/project', 'About the Missing Migrant Project');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Missing Migrants Project is an initiative implemented since 2014 by the ${'International Organization for Migration'} (IOM)
	to document deaths and disappearances of people in the process of $migration towards an international destination.</p>
	HTML;



$div_Missing_Migrants_Project_website = new WebsiteContentSection();
$div_Missing_Migrants_Project_website->setTitleText("Missing Migrants Project website ");
$div_Missing_Migrants_Project_website->setTitleLink("https://missingmigrants.iom.int/");
$div_Missing_Migrants_Project_website->content = <<<HTML
	<p>The International Organization for Migration (IOM)’s Missing Migrants Project records incidents in which migrants,
	including refugees and asylum-seekers, have died at state borders or in the process of migrating to an international destination.
	It was developed in response to disparate reports of people dying or disappearing along migratory routes around the world,
	and particularly in the wake of two shipwrecks in October 2013, when at least 368 people died near the Italian island of Lampedusa.
	The Project hosts the only existing open-access database of records of deaths during migration on the global level.
	These data are used to inform the Sustainable Development Goals Indicator 10.7.3 on
	the “[n]umber of people who died or disappeared in the process of migration towards an international destination.”
	Missing Migrants Project is also a concerted effort towards informing the Global Compact on Migration’s Objective 8,
	which commits signatory states to “save lives and establish coordinated international efforts on missing migrants.” $r1</p>
	HTML;




$div_Missing_Migrants_Project = new WebsiteContentSection();
$div_Missing_Migrants_Project->setTitleText("Global Migration Data Analysis Centre: Missing Migrants Project");
$div_Missing_Migrants_Project->setTitleLink("https://gmdac.iom.int/missing-migrants-project");
$div_Missing_Migrants_Project->content = <<<HTML
	<p>More than 44,000 migrants around the world have lost their lives since the project began in 2014.
	More than half of these deaths were recorded in the Mediterranean Sea, which has seen a devastating spike in deaths in recent years.
	The Missing Migrants Project collaborates with various government and non-government entities to collect data,
	and also draws on media and other available sources of information.</p>

	<p>The outcomes of the Missing Migrant Project include:</p>

	<ul>
	<li>The Missing Migrants database and website, with records of migrant deaths and disappearances since 2014,
	research and analysis and resources for families searching for missing loved ones</li>
	<li>Regular infographics, policy briefs and “Fatal Journeys” report series on migrant deaths</li>
	<li>Expert seminars on issues of concern, including on identification and tracing of dead and missing migrants</li>
	<li>Network-building to collect better quality data and to advocate for better management of the dead,
	improved identification of the dead, and support to families of the missing</li>
	</ul>
	HTML;



$page->parent('international_organization_for_migration.html');
$page->template("stub");
$page->body($div_introduction);

$page->body($div_Missing_Migrants_Project_website);
$page->body($div_Missing_Migrants_Project);
$page->body('deaths_of_migrants.html');
