<?php
$page = new CountryPage('Tanzania');
$page->h1("Tanzania");
//$page->alpha_sort("Tanzania");
$page->tags("Country");
$page->keywords("Tanzania", "Tanzanian");
$page->stars(1);
$page->viewport_background("/free/tanzania.png");

$page->snp("description", "67 million inhabitants.");
$page->snp("image",       "/free/tanzania.1200-630.png");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$list_Tanzania = ListOfPeoplePages::WithTags("Tanzania");
$print_list_Tanzania = $list_Tanzania->print();

$div_list_Tanzania = new ContentSection();
$div_list_Tanzania->content = <<<HTML
	<h3>Related content</h3>

	$print_list_Tanzania
	HTML;



$div_wikipedia_Tanzania = new WikipediaContentSection();
$div_wikipedia_Tanzania->setTitleText("Tanzania");
$div_wikipedia_Tanzania->setTitleLink("https://en.wikipedia.org/wiki/Tanzania");
$div_wikipedia_Tanzania->content = <<<HTML
	<p>Tanzania, officially the United Republic of Tanzania, is a country in East Africa within the African Great Lakes region.
	It is bordered by Uganda to the northwest; Kenya to the northeast; the Indian Ocean to the east; Mozambique and Malawi to the south;
	Zambia to the southwest; and Rwanda, Burundi, and the Democratic Republic of the Congo to the west.
	Mount Kilimanjaro, Africa's highest mountain, is in northeastern Tanzania.</p>
	HTML;

$div_wikipedia_Constitution_of_Tanzania = new WikipediaContentSection();
$div_wikipedia_Constitution_of_Tanzania->setTitleText("Constitution of Tanzania");
$div_wikipedia_Constitution_of_Tanzania->setTitleLink("https://en.wikipedia.org/wiki/Constitution_of_Tanzania");
$div_wikipedia_Constitution_of_Tanzania->content = <<<HTML
	<p>The Constitution of the United Republic of Tanzania, also known as the Permanent Constitution, was ratified in 16 March 1977.
	Before the current establishment, Tanzania has had three constitutions:
	the Independence Constitution (1961),
	the Republican Constitution (1962),
	and the Interim Constitution of the United Republic of Tanganyika and Zanzibar (1964).</p>
	HTML;

$div_wikipedia_Politics_of_Tanzania = new WikipediaContentSection();
$div_wikipedia_Politics_of_Tanzania->setTitleText("Politics of Tanzania");
$div_wikipedia_Politics_of_Tanzania->setTitleLink("https://en.wikipedia.org/wiki/Politics_of_Tanzania");
$div_wikipedia_Politics_of_Tanzania->content = <<<HTML
	<p>The politics of Tanzania takes place in a framework of a unitary presidential democratic republic,
	whereby the President of Tanzania is both head of state and head of government, and of a multi-party system.
	Executive power is exercised by the government. Legislative power is vested in both the government and parliament.
	The party system is dominated by the Chama Cha Mapinduzi (Revolutionary State Party).
	The Judiciary is independent of the executive and the legislature.</p>
	HTML;


$page->parent('world.html');
$page->template("stub");
//$page->body($div_introduction);
$page->body($div_list_Tanzania);
$page->body('Country indices');

$page->body($div_wikipedia_Tanzania);
$page->body($div_wikipedia_Constitution_of_Tanzania);
$page->body($div_wikipedia_Politics_of_Tanzania);
