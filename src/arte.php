<?php
$page = new OrganisationPage();
$page->h1("Arte");
$page->viewport_background("");
$page->keywords("Arte");
$page->stars(0);
$page->tags("Organisation", "Information: Media", "Broadcaster", "France", "Germany");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;




$div_ARTE_the_European_culture_TV_channel_free_and_on_demand = new WebsiteContentSection();
$div_ARTE_the_European_culture_TV_channel_free_and_on_demand->setTitleText("ARTE, the European culture TV channel, free and on demand");
$div_ARTE_the_European_culture_TV_channel_free_and_on_demand->setTitleLink("https://www.arte.tv/en/");
$div_ARTE_the_European_culture_TV_channel_free_and_on_demand->content = <<<HTML
	<p>
	</p>
	HTML;




$div_youtube_ARTE_tv_Documentary = new YoutubeContentSection();
$div_youtube_ARTE_tv_Documentary->setTitleText("ARTE.tv Documentary");
$div_youtube_ARTE_tv_Documentary->setTitleLink("https://www.youtube.com/@artetvdocumentary");
$div_youtube_ARTE_tv_Documentary->content = <<<HTML
	<p>
	</p>
	HTML;




$div_wikipedia_Arte = new WikipediaContentSection();
$div_wikipedia_Arte->setTitleText("Arte");
$div_wikipedia_Arte->setTitleLink("https://en.wikipedia.org/wiki/Arte");
$div_wikipedia_Arte->content = <<<HTML
	<p>Arte is a European public service channel dedicated to culture. It is made up of three separate companies: the Strasbourg-based European Economic Interest Grouping (EEIG) ARTE, plus two member companies acting as editorial and programme production centres, ARTE France in Paris (formerly known as La Sept) and ARTE Deutschland in Baden-Baden (a subsidiary of the two main public German TV networks ARD and ZDF).</p>
	HTML;


$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Arte");
$page->body($div_ARTE_the_European_culture_TV_channel_free_and_on_demand);
$page->body($div_youtube_ARTE_tv_Documentary);
$page->body($div_wikipedia_Arte);
