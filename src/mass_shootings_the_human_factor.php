<?php
$page = new Page();
$page->h1('Mass shootings: the human factor');
$page->keywords('mass shootings');
$page->tags("Institutions: Legislative", "USA", "Gun Legislation", "Society");
$page->stars(3);

$page->snp('description', 'Analysing the human and institutional factors behind mass shootings.');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p>On October 25 2023, the United States House of Representatives voted, 220–209,
	to elect Mike Johnson as the 56th Speaker of the United States House of Representatives.</p>

	<p>On October 25, 2023, a gunman killed 18 people and injured 13 others during a shooting spree
	at two locations in Lewiston, Maine, United States.</p>

	<p>What is the connection between these two seemingly completely unrelated events which happened on the same day, only a few hours apart?</p>
	HTML );


$r1 = $page->ref('https://en.wikipedia.org/wiki/Mike_Johnson', 'Wikipedia: Mike Johnson');
$r2 = $page->ref('https://en.wikipedia.org/wiki/2023_Lewiston_shootings', 'Wikipedia: 2023 Lewiston shootings');
$r3 = $page->ref('https://edition.cnn.com/politics/live-news/house-speaker-vote-10-25-23/index.html', 'CNN timeline: Rep. Mike Johnson voted new House speaker');
$r4 = $page->ref('https://www.youtube.com/watch?v=kQFsM6b0IS8', 'Fox News: Newly-minted House Speaker speaks out on key issues');
$r5 = $page->ref('https://en.wikipedia.org/wiki/Vehicle-ramming_attack', 'Vehicle-ramming attack');
$r6 = $page->ref('https://justfacts.votesmart.org/candidate/evaluations/156097/mike-johnson', 'Vote Smart: Mike Johnson\'s Ratings and Endorsements');
$r7 = $page->ref('https://www.americas1stfreedom.org/content/the-house-has-a-new-speaker/', 'NRA: The House Has a New Speaker');
$r8 = $page->ref('https://en.wikipedia.org/wiki/2016_Nice_truck_attack', 'Wikipedia: 2016 Nice truck attack');
$r9 = $page->ref('https://en.wikipedia.org/wiki/Mass_shootings_in_the_United_States', 'Mass shootings in the United States');
$r10 = $page->ref('https://hakeemjeffries.com/issues/', 'Hakeem Jeffries: Issues');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>On October 25 2023, the United States House of Representatives voted, 220–209,
	to elect Mike Johnson as the 56th Speaker of the United States House of Representatives. $r1</p>

	<p>On October 25, 2023, a gunman killed 18 people and injured 13 others during a shooting spree
	at two locations in Lewiston, Maine, United States. $r2</p>

	<p>These two completely unrelated events happened on the same day, only a few hours apart:
	Representative Mike Johnson was voted new House speaker at 2:10pm.
	He was officially sworn in as House speaker at 2:53pm. $r3
	The mass shooting in Lewiston, Maine, started at 6:56pm.</p>
	HTML;


$div_first_issue_for_speaker_johnson = new ContentSection();
$div_first_issue_for_speaker_johnson->content = <<<HTML
	<p>Thus, one of the first issues that just sworn-in Speaker Johnson was asked about
	was that of gun control legislation.
	On the evening of the 25th, there was an impromptu press conference in the halls of the House of Representative,
	during which Johnson offered his hopes and prayers for the victims and their families.
	He did not address gun legislation.
	Two days later, Johnson gave is first TV interview as a Speaker, on Fox News.
	In the hour-long interview, the Lewinston shooting was the first topic that was addressed.
	Johnson said: $r4</p>

	<blockquote>
	<p><strong>Speaker Johnson</strong>:
	I've been in the job about 48 hours now. (...)
	I just want to say at the outset here:
	God be with the law enforcement officers that are handling that situation in Maine our our prayers have been with the families of that tragedy. (...)</p>

	<p><strong>Fox News host</strong>:
	Already though, you're immersed in Democrats - and this happens with almost every shooting incident -
	the immediate call by the left in this country: "We need more gun laws. We need more legislation."
	What's your answer to that?</p>

	<p><strong>Speaker Johnson</strong>:
	At the end of the day, it's the problem is the human heart.
	It's not guns. It's not the weapons.
	At the end of the day we have to protect the right of the citizens to protect themselves.
	That's the second amendment. (...)
	This is not the time to be talking about legislation:
	we're in the middle of that crisis right now.
	But I just want you to know and I want the American people to know that
	all the members of the house here are deeply concerned about the families involved.
	We pray for the law enforcement officers that are doing that hard job tonight that most people do not have the bravery to do.</p>

	<p><strong>Fox News host</strong>:
	You talk about the human heart if somebody really wants to kill innocent people.
	There's a lot of ways they can do it beyond using a gun. (...)
	Is there any any specific gun law that you would look at, or any new legislation you would look at?</p>

	<p><strong>Speaker Johnson</strong>:
	There'll be lots of discussion as there are after these heartbreaking tragedies. (...)
	You know in Europe and in other places, they use vehicles to mow down crowds at parades.
	They've done that here in the United States.
	It's not the weapon, it's the underlying problem.
	I believe we have to address the root problems of these things,
	and mental health obviously as in this case is a big issue.
	We've got to seriously address that as a society and as a government and and there's lots of measures pending on that as well.</p>
	</blockquote>
	HTML;

$div_the_human_heart = new ContentSection();
$div_the_human_heart->content = <<<HTML
	<p>To begin with, we agree with Speaker Johnson:
	the human heart, <strong>our human nature is an important underlying problem</strong>.
	We, collectively, have the society we deserve, the democracies we deserve.
	It is obvious that people wilfully committing mass murder have deep emotional and psychological issues.</p>

	<p>We can extend this observation to any of the ills that plague our society:
	all types of violence, all the hatred, all the thievery and lawbreaking,
	all the environmental destruction, the pollution, the economic exploitation, etc.
	All of these problems and many more are caused by lack of empathy, lack of love, by selfish uncaring impulses, etc.</p>

	<p>If our human hearts, as Mike Johnson puts it, were different, more loving, more enlightened,
	then our societal need for government would be much reduced.
	The need for legislation, regulations, and for a criminal justice system would be greatly attenuated.</p>

	<p>Unfortunately, precisely because our human hearts are less than perfect,
	we do need to take this into account and put institutions in place to prevent the worse, like the Livinston mass shooting, from happening.</p>
	HTML;


$div_vehicle_ramming_attacks = new ContentSection();
$div_vehicle_ramming_attacks->content = <<<HTML
	<p>During the interview, Speaker Johnson alluded to the <strong>vehicle-ramming attacks</strong>$r5 that occurred in Europe and in the United States.
	These attacks are a new type of terrorist threat.
	The most notable occurrence probably was the 2016 truck attack in Nice, $France, during which a terrorist killed 86 people by driving a truck into a crowd.$r8
	Terrorists perpetrating such heinous, murderous acts indeed have some of the most evil hearts amongst all human beings.</p>

	<p>In raising the issue of vehicle-ramming attacks, Johnson fails to mention two critical issues, though.</p>

	<p>First, when terrorists increasingly started using cars to kill innocent pedestrians,
	European governments did not remain idle, blaming the crimes on evil hearts and left it at that.
	Actions were taken, legislations were voted, and preventive measures were put in place,
	all of which in order to prevent and limit such crimes.</p>

	<p>Also Johnson fails to mention the relative frequency of occurrence and number of victims between
	mass shootings in the United States and vehicle-ramming attacks worldwide.
	The number of car or truck attacks, and the number of fatalities are very far from reaching
	the levels achieved by gun-related crimes.
	There are hundreds of mass shooting events occurring in the United States every year,$r9
	with over a thousand fatalities, in addition to all the crimes and murders committed with firearms.</p>

	<p>Thus it is all the more important for the US government and legislators to take the necessary measures
	to limit and prevent mass shootings by introducing appropriate and common sense legislation on gun ownership.</p>
	HTML;


$div_Johnson_and_NRA = new ContentSection();
$div_Johnson_and_NRA->content = <<<HTML
	<p>It should be noted that Mike Johnson has a 92% approval rating by the <strong>National Rifle Association (NRA)</strong>,
	100% approval rating by the Gun Owners of America,
	and 100% approval rating by the National Shooting Sports Foundation. $r6
	When Johnson was elected speaker, the NRA welcomed the news $r7, noting Johnson's efforts to
	"<em>protect second amendment freedom</em>".</p>

	<p>The NRA welcomed the fact that Democrat Hakeem Jeffries lost the speakership vote.
	Strangely, the NRA even directly quotes from Jeffries's campaign website which states:
	"<em>In Congress, Hakeem has been a leader on gun violence prevention legislation,
	pushing for commonsense measures like universal background checks for firearm purchases
	and a ban on assault weapons</em>". $r10</p>

	<p>In conclusion, we agree with Mike Johnson that mass shootings
	is primarily a human heart issue.
	At the same time, we agree with Hakeem Jeffries that the most appropriate way
	to address this specific type of crime is to introduce commonsense gun violence prevention legislation.</p>
	HTML;


$div_gun_legislation = new ContentSection();
$div_gun_legislation->content = <<<HTML
	<p>In the same way as there are <strong>institutional factors</strong> and <strong>human factors</strong>
	that affect the quality and the nature of our $democracies,
	there are also institutional factors and human factors that play critical roles
	in the prevalence of mass shootings in the United States.</p>

	<p>Among the institutional factors is the lack of proper gun violence prevention legislation.
	We also count as institutional factors the fact that gun lobbies such as the NRA
	can play such a decisive role in US politics,
	in getting their favourite candidates elected and in shaping public opinion on the issue of gun control.</p>

	<p>Among the human factors, there is obviously the obviously diseased mental states of the perpetrators of mass shootings.
	There are also all the politicians who are easily bought off by gun lobbies,
	and who fail to exercise due diligence by passing appropriate legislation,
	disregarding the human and social cost, the deaths of innocent civilians,
	in order to secure donations to fill in their campaign re-election fund.</p>
	HTML;

$div_wikipedia_Mass_shootings_in_the_United_States = new WikipediaContentSection();
$div_wikipedia_Mass_shootings_in_the_United_States->setTitleText('Mass shootings in the United States');
$div_wikipedia_Mass_shootings_in_the_United_States->setTitleLink('https://en.wikipedia.org/wiki/Mass_shootings_in_the_United_States');
$div_wikipedia_Mass_shootings_in_the_United_States->content = <<<HTML
	<p>Mass shootings are incidents involving multiple victims of firearm related violence.
	Definitions vary, with no single, broadly accepted definition.
	One definition is an act of public firearm violence—excluding gang killings, domestic violence,
	or terrorist acts sponsored by an organization—in which a shooter kills at least four victims.
	Using this definition, a 2016 study found that nearly one-third of the world's public mass shootings
	between 1966 and 2012 (90 of 292 incidents) occurred in the United States.</p>
	HTML;

$div_wikipedia_List_of_mass_shootings_in_the_United_States = new WikipediaContentSection();
$div_wikipedia_List_of_mass_shootings_in_the_United_States->setTitleText('List of mass shootings in the United States');
$div_wikipedia_List_of_mass_shootings_in_the_United_States->setTitleLink('https://en.wikipedia.org/wiki/List_of_mass_shootings_in_the_United_States');
$div_wikipedia_List_of_mass_shootings_in_the_United_States->content = <<<HTML
	<p>This is a list of the most notable mass shootings in the United States that have occurred since 1920.</p>
	HTML;

$div_wikipedia_2023_Lewiston_shootings = new WikipediaContentSection();
$div_wikipedia_2023_Lewiston_shootings->setTitleText('2023 Lewiston shootings');
$div_wikipedia_2023_Lewiston_shootings->setTitleLink('https://en.wikipedia.org/wiki/2023_Lewiston_shootings');
$div_wikipedia_2023_Lewiston_shootings->content = <<<HTML
	<p>On October 25, 2023, a gunman killed 18 people and injured 13 others during a shooting spree at two locations in Lewiston, Maine, United States.
	The first mass shooting was at the Just-in-Time Recreation bowling alley, during a youth league event,
	and the second occurred minutes later at the Schemengees Bar & Grille Restaurant.</p>
	HTML;


$page->parent('institutional_vs_human_factors_in_democracy.html');
$page->body($div_introduction);

$page->body($div_first_issue_for_speaker_johnson);
$page->body($div_the_human_heart);
$page->body($div_vehicle_ramming_attacks);
$page->body($div_Johnson_and_NRA);
$page->body($div_gun_legislation);

$page->body('saints_and_little_devils.html');

$page->body($div_wikipedia_Mass_shootings_in_the_United_States);
$page->body($div_wikipedia_List_of_mass_shootings_in_the_United_States);
$page->body($div_wikipedia_2023_Lewiston_shootings);
