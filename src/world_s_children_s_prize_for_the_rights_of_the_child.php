<?php
$page = new AwardPage();
$page->h1("World's Children's Prize for the Rights of the Child");
$page->tags("Award", "Children's Rights", "Education");
$page->keywords("World's Children's Prize for the Rights of the Child");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;



$div_Worlds_Childrens_Prize_for_the_Rights_of_the_Child = new WebsiteContentSection();
$div_Worlds_Childrens_Prize_for_the_Rights_of_the_Child->setTitleText("Worlds Childrens Prize for the Rights of the Child ");
$div_Worlds_Childrens_Prize_for_the_Rights_of_the_Child->setTitleLink("https://worldschildrensprize.org/");
$div_Worlds_Childrens_Prize_for_the_Rights_of_the_Child->content = <<<HTML
	<p>The World’s Children’s Prize program is one of the the world's largest annual education programs empowering children to become changemakers who can stand up for the equal value of all people, the Rights of the Child, democracy and sustainable development.</p>
	HTML;




$div_wikipedia_World_s_Children_s_Prize_for_the_Rights_of_the_Child = new WikipediaContentSection();
$div_wikipedia_World_s_Children_s_Prize_for_the_Rights_of_the_Child->setTitleText("World s Children s Prize for the Rights of the Child");
$div_wikipedia_World_s_Children_s_Prize_for_the_Rights_of_the_Child->setTitleLink("https://en.wikipedia.org/wiki/World's_Children's_Prize_for_the_Rights_of_the_Child");
$div_wikipedia_World_s_Children_s_Prize_for_the_Rights_of_the_Child->content = <<<HTML
	<p>The World’s Children’s Prize for the Rights of the Child was founded in 2000 and is run by the World's Children's Prize Foundation (WCPF), based in Mariefred, Sweden.
	The WCPF is a non-profit organisation, independent of all political and religious affiliation, and run with support from bodies including the Swedish Postcode Lottery and the Swedish International Development Cooperation Agency.</p>
	HTML;


$page->parent('list_of_awards.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("World's Children's Prize for the Rights of the Child");

$page->body($div_Worlds_Childrens_Prize_for_the_Rights_of_the_Child);
$page->body($div_wikipedia_World_s_Children_s_Prize_for_the_Rights_of_the_Child);
