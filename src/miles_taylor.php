<?php
$page = new Page();
$page->h1("Miles Taylor");
$page->alpha_sort("Taylor, Miles");
$page->tags("Person");
$page->keywords("Miles Taylor");
$page->stars(0);

//$page->snp("description", "");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p>Miles Taylor is an American government official who served in the administrations of George W. Bush and Donald Trump.</p>
	HTML );

// $r1 = $page->ref('', '');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Miles Taylor was chief of staff of the ${'United States'} Department of Homeland Security (DHS) from 2017 to 2019
	within ${'Donald Trump'}'s administration.</p>

	<p>Miles Tailor is the author of the book: ${"Blowback: A Warning to Save Democracy from the next Trump"}.</p>
	HTML;

$div_wikipedia_Miles_Taylor_security_expert = new WikipediaContentSection();
$div_wikipedia_Miles_Taylor_security_expert->setTitleText("Miles Taylor security expert");
$div_wikipedia_Miles_Taylor_security_expert->setTitleLink("https://en.wikipedia.org/wiki/Miles_Taylor_(security_expert)");
$div_wikipedia_Miles_Taylor_security_expert->content = <<<HTML
	<p>Miles Taylor is an American government official who served in the administrations of George W. Bush and Donald Trump.
	In the administration of the latter,
	he was an appointee who served in the United States Department of Homeland Security (DHS) from 2017 to 2019,
	including as chief of staff of the DHS.</p>

	<p>In 2018, Taylor wrote an op-ed in The New York Times under the pen-name "Anonymous" that was titled,
	"I Am Part of the Resistance Inside the Trump Administration", which drew widespread attention for its criticism of Trump.
	Several months after quitting the administration, again under the pen name "Anonymous,"
	he published a book in November 2019 titled, A Warning.
	In October 2020, he revealed that he was "Anonymous" while campaigning against Trump's reelection.
	He was the first former Trump administration official to endorse Joe Biden
	and launched a group of ex-officials to oppose Trump's re-election.</p>
	HTML;


$page->parent('list_of_people.html');
$page->template("stub");
$page->body($div_introduction);


$page->body('blowback_a_warning_to_save_democracy.html');
$page->body($div_wikipedia_Miles_Taylor_security_expert);
