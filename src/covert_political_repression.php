<?php
$page = new Page();
$page->h1('Covert political repression');
$page->keywords('covert political repression');
$page->stars(0);

$page->snp('description', 'Surveillance, wiretapping and other indirect forms of repression.');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p>Covert surveillance, wiretapping and other methods which do not bring any direct harm to the targeted individuals.</p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Covert ${'political repression'} is an indirect form of repression including:
	surveillance, wiretapping, etc.
	where the targeted individuals may or may not know they are being targeted.
	Political opponents would be aware of the likelihood of such surveillance
	and may self-censor and curtail their own communications with other individuals.</p>
	HTML;


$page->parent('political_repression.html');
$page->template("stub");
$page->body($div_introduction);


