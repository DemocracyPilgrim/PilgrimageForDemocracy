<?php
$page = new Page();
$page->h1("Duverger's Law");
$page->stars(1);
$page->tags("Elections: Duverger Syndrome");
$page->keywords("Duverger's Law");

//$page->snp('description', "");
//$page->snp('image', "/copyrighted/");

$page->preview( <<<HTML
	<p>On single winner electoral systems and the tendency for a two party system.</p>
	HTML );


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Duverger's Law perfectly explains why in any society where there is a multitude of opinions,
	only two big parties or two broad camps tend to emerge.</p>

	<p>Duverger's Law is the most important theory to understand for any concerned citizen,
	because its effects are all the symptoms of the ${'Duverger Syndrome'}
	that we can all observe today and that plague our political environment.</p>

	<p>What is now known as Duverger's Law has first been described in the 1950s,
	by ${'Maurice Duverger'}, a $French political scientist.
	However the lessons have yet to be learned in the 2020s.
	Fixing our electoral system by adopting a better ${'voting method'}
	is probably the most critical issue for our $democracies.</p>
	HTML;


$div_Simple_example = new ContentSection();
$div_Simple_example->content = <<<HTML
	<h3>A simple example</h3>

	<p>Before we venture into specific definitions and more technical considerations,
	let's consider a very simple example...</p>

	<p>Imagine that we have a presidential election with three candidates:
	one progressive, and two conservatives.
	Further, imagine that the opinion polls for each candidate are as follows:</p>

	<table class="voting method">
		<tr>
			<th>Candidate</th>
			<th>Political affiliation</th>
			<th>Opinion polls</th>
			<th>Winner</th>
		</tr>
		<tr class="progressive">
			<td>A. Attaboy</td>
			<td>Progressive (Left)</td>
			<td>40%</td>
			<td class="winner">&check;</td>
		</tr>
		<tr class="conservative">
			<td>B. Bernard</td>
			<td>Conservative (centre right)</td>
			<td>25%</td>
			<td></td>
		</tr>
		<tr class="very conservative">
			<td>C. Curtis</td>
			<td>Ultra conservative (right)</td>
			<td>35%</td>
			<td></td>
		</tr>
	</table>

	<p>It is obvious that if on election day, people voted more or less as predicted in the opinion polls,
	candidate A, the progressive, would win, not with a majority of the vote (over 50%)
	but with a plurality of the vote (40%, which is less that 50%, but more than any other candidate).</p>

	<p>It is equally obvious that conservative voters split their vote between two candidates.
	If we imagine that the second choice of the people who voted for centre right candidate is the candidate on the right,
	and vice versa, then it stands to reason that conservatives would stand a better chance to win
	by holding a primary and be united behind one single candidate in the general election.</p>

	<table class="voting method">
		<tr>
			<th>Candidate</th>
			<th>Political affiliation</th>
			<th>Election results</th>
			<th>Winner</th>
		</tr>
		<tr class="progressive">
			<td>A. Attaboy</td>
			<td>Progressive (Left)</td>
			<td>40%</td>
			<td></td>
		</tr>
		<tr class="very conservative">
			<td>C. Curtis</td>
			<td>Ultra conservative (right)</td>
			<td>60%</td>
			<td class="winner">&check;</td>
		</tr>
	</table>

	<p>Now, imagine that candidate B, having lost the primary, goes into retirement.
	The next election, another moderate conservative, candidate D, foregoes the primary,
	and presents herself at the general election.
	She may represent 25~30% percent of the electorate, but voters are afraid to split the vote,
	and consider her to have no chance to win the general election.
	Thus, even though she might have been their first choice,
	they decide to ditch her and vote for the ultra conservative candidate,
	to ensure that the election is not won by a progressive candidate.</p>

	<table class="voting method">
		<tr>
			<th>Candidate</th>
			<th>Political affiliation</th>
			<th>Opinion polls</th>
			<th>Winner</th>
		</tr>
		<tr class="progressive">
			<td>A. Attaboy</td>
			<td>Progressive (Left)</td>
			<td>40%</td>
			<td></td>
		</tr>
		<tr class="conservative">
			<td>D. Durian</td>
			<td>Conservative (centre right)</td>
			<td>5%</td>
			<td></td>
		</tr>
		<tr class="very conservative">
			<td>C. Curtis</td>
			<td>Ultra conservative (right)</td>
			<td>55%</td>
			<td class="winner">&check;</td>
		</tr>
	</table>

	HTML;


$div_Duverger_Law = new ContentSection();
$div_Duverger_Law->content = <<<HTML
	<h3>Duverger's Law</h3>

	<p><strong>Single winner election</strong>:
	Duverger's Law applies in elections where there is only one winner,
	like a presidential election or legislative elections where there is only one elected lawmaker per electoral district,
	However, given the same plurality voting system, the effects of Duverger's Law can also be observed
	in multi-seats electoral district or even when there is some form of proportional representation.
	Some recent research has shown that Duverger's Law also applies in multi-seat electoral districts.</p>

	<p><strong>${'Plurality voting'}</strong>:
	There are two aspects to any voting method: casting the ballots, and tallying the ballots.
	Casting the ballot is when the voters are asked about their preference of candidates.
	It concerns the design of the ballot itself, and what the voters are allowed or not allowed to do to express their choices.
	Tallying the ballot typically occurs after the polling stations have closed:
	it is the simple mathematical operation involved in counting the ballots,
	making simple additions, and determining the winner.</p>

	<p>This distinction between casting the ballots and tallying the ballots appears obvious.
	It is however a very important one to make.</p>

	<p>The traditional definition of plurality voting focuses on the tallying part,
	and relies on the direct interpretation of the word "<em>plurality</em>":
	after counting the votes for each candidate,
	the elected candidate is the one who wins a <strong>plurality</strong> of the votes,
	i.e. more votes than anyone else,
	even if they have not won a <strong>majority</strong> of the votes,
	i.e. more than 50%.</p>

	<p>The first aspect, casting the ballot, is often overlooked.
	Indeed, given any number of candidates, the voter is asked to make a single choice, to vote for one single candidate or party.
	And <em>this</em> is the real root cause of all the symptoms of the ${"Duverger Syndrome"}.</p>
	HTML;



$div_wikipedia_Duverger_s_law = new WikipediaContentSection();
$div_wikipedia_Duverger_s_law->setTitleText("Duverger's law");
$div_wikipedia_Duverger_s_law->setTitleLink("https://en.wikipedia.org/wiki/Duverger%27s_law");
$div_wikipedia_Duverger_s_law->content = <<<HTML
	<p>In political science, Duverger's law holds that in political systems with only one winner (as in the U.S.),
	two main parties tend to emerge with minor parties typically splitting votes away from the most similar major party.</p>
	HTML;



$page->parent('duverger_syndrome.html');
$page->body($div_introduction);

$page->body($div_Simple_example);
$page->body($div_Duverger_Law);

$page->body($div_wikipedia_Duverger_s_law);

$page->body('duverger_syndrome.html');
