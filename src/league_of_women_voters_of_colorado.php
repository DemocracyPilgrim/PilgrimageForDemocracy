<?php
$page = new OrganisationPage();
$page->h1("League of Women Voters of Colorado");
$page->viewport_background("/free/league_of_women_voters_of_colorado.png");
$page->keywords("League of Women Voters of Colorado ");
$page->stars(0);
$page->tags("Organisation", "Colorado", "League of Women Voters", "Elections", "Women's Rights", "Voting Methods", "Approval Voting");

//$page->snp("description", "");
$page->snp("image",       "/free/league_of_women_voters_of_colorado.1200-630.png");

$page->preview( <<<HTML
	<p></p>
	HTML );

$r1 = $page->ref("https://www.lwvcolorado.org/content.aspx?page_id=22&club_id=314195&module_id=540498", "LWV-Colorado: Alternative Voting Methods");
$r2 = $page->ref("https://www.youtube.com/@lwvcolorado/search?query=AVM%20Task%20Force", "LWV-Colorado: AVM Task Force Meeting");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;



$div_Alternative_Voting_Method_Task_Force = new ContentSection();
$div_Alternative_Voting_Method_Task_Force->content = <<<HTML
	<h3>Alternative Voting Method Task Force</h3>

	<p>The League of Women Voters of Colorado has an Alternative Voting Method Task Force $1.
	The task force meetings are published on their YouTube channel $r2.</p>
	HTML;



$div_League_of_Women_Voters_of_Colorado = new WebsiteContentSection();
$div_League_of_Women_Voters_of_Colorado->setTitleText("League of Women Voters of Colorado ");
$div_League_of_Women_Voters_of_Colorado->setTitleLink("https://lwvcolorado.org/");
$div_League_of_Women_Voters_of_Colorado->content = <<<HTML
	<p>In Colorado, the League of Women Voters (LWVCO) was organized in 1928. Throughout its history, members have researched, studied, discussed, and reached consensus on many controversial issues. The League in Colorado has had an impact in many areas of government.</p>

	<p>Our Mission: Empowering Voters. Defending Democracy.</p>
	HTML;



$div_youtube_League_of_Women_Voters_of_Colorado = new YoutubeContentSection();
$div_youtube_League_of_Women_Voters_of_Colorado->setTitleText("League of Women Voters of Colorado");
$div_youtube_League_of_Women_Voters_of_Colorado->setTitleLink("https://www.youtube.com/@lwvcolorado");
$div_youtube_League_of_Women_Voters_of_Colorado->content = <<<HTML
	<p>The League of Women Voters of Colorado YouTube channel.</p>
	HTML;




$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);




$page->body($div_Alternative_Voting_Method_Task_Force);


$page->related_tag("League of Women Voters of Colorado");
$page->body($div_League_of_Women_Voters_of_Colorado);
$page->body($div_youtube_League_of_Women_Voters_of_Colorado);
