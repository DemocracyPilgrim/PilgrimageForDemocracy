<?php
$page = new Page();
$page->h1("Andrew Weissmann");
$page->alpha_sort("Weissmann, Andrew");
$page->tags("Person", "Institutions: Judiciary", "USA", "Lawyer");
$page->keywords("Andrew Weissmann");
$page->stars(0);
$page->viewport_background("/free/andrew_weissmann.png");

$page->snp("description", "American attorney and professor");
$page->snp("image",       "/free/andrew_weissmann.1200-630.png");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Andrew Weissmann is an American attorney and professor, and a former prosecutor.
	Together with ${'Mary McCord'}, he is the host the award-winning podcast ${"Prosecuting Donald Trump"}.</p>

	<p>He is the author of ${"Where Law Ends: Inside the Mueller Investigation"},
	an insider view of the investigation of the $Russian interference in the 2016 $US presidential election.</p>
	HTML;





$div_wikipedia_Andrew_Weissmann = new WikipediaContentSection();
$div_wikipedia_Andrew_Weissmann->setTitleText("Andrew Weissmann");
$div_wikipedia_Andrew_Weissmann->setTitleLink("https://en.wikipedia.org/wiki/Andrew_Weissmann");
$div_wikipedia_Andrew_Weissmann->content = <<<HTML
	<p>Andrew A. Weissmann (born March 17, 1958) is an American attorney and professor.
	He was an Assistant United States Attorney from 1991 to 2002, when he prosecuted high-profile organized crime cases.
	He served as a lead prosecutor in Robert S. Mueller's Special Counsel's Office (2017–2019),
	as Chief of the Fraud Section in the Department of Justice (2015–2017)
	and is currently a professor at NYU Law School.</p>
	HTML;



$div_Andrew_Weissmann_on_CSPAN = new WebsiteContentSection();
$div_Andrew_Weissmann_on_CSPAN->setTitleText("Andrew Weissmann on C-SPAN");
$div_Andrew_Weissmann_on_CSPAN->setTitleLink("https://www.c-span.org/person/andrew-weissmann/9267911/");
$div_Andrew_Weissmann_on_CSPAN->content = <<<HTML
	<p>Andrew Weissmann was a General Counsel for the Federal Bureau of Investigation with six videos in the C-SPAN Video Library;
	the first appearance was a 2010 Senate Committee as a Chief for the Criminal Division in the U.S. Attorney's Office, Eastern District of New York.</p>
	HTML;




$page->parent('list_of_people.html');
$page->template("stub");
$page->body($div_introduction);

$page->related_tag("Andrew Weissmann");
$page->body($div_Andrew_Weissmann_on_CSPAN);

$page->body($div_wikipedia_Andrew_Weissmann);
