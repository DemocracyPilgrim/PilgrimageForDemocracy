<?php
$page = new Page();
$page->h1("Conservatism");
$page->keywords("conservatism");
$page->tags("Information: Discourse", "Politics", "Society");
$page->stars(0);

//$page->snp("description", "");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;


$div_codeberg_How_to_define_conservatism_and_what_to_make_of_the_conservative_movement_in_the_USA = new CodebergContentSection();
$div_codeberg_How_to_define_conservatism_and_what_to_make_of_the_conservative_movement_in_the_USA->setTitleText('How to define conservatism, and what to make of the "conservative" movement in the USA?');
$div_codeberg_How_to_define_conservatism_and_what_to_make_of_the_conservative_movement_in_the_USA->setTitleLink("https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy/issues/56");
$div_codeberg_How_to_define_conservatism_and_what_to_make_of_the_conservative_movement_in_the_USA->content = <<<HTML
	<p>There is nothing wrong in being a true conservative, but should properly define the term, cover its different facets,
	and investigate how conservatism would look like under the prism of democracy and social justice.</p>
	HTML;


$div_wikipedia_Conservatism = new WikipediaContentSection();
$div_wikipedia_Conservatism->setTitleText("Conservatism");
$div_wikipedia_Conservatism->setTitleLink("https://en.wikipedia.org/wiki/Conservatism");
$div_wikipedia_Conservatism->content = <<<HTML
	<p>Conservatism is a cultural, social, and political philosophy that seeks to promote and to preserve traditional institutions, customs, and values.
	The central tenets of conservatism may vary in relation to the culture and civilization in which it appears.
	In Western culture, depending on the particular nation, conservatives seek to promote a range of social institutions,
	such as the nuclear family, organized religion, the military, the nation-state, property rights, rule of law, aristocracy, and monarchy.
	Conservatives tend to favour institutions and practices that guarantee social order and historical continuity.</p>
	HTML;


$page->parent('political_discourse.html');
$page->template("stub");
$page->body($div_introduction);

$page->body($div_codeberg_How_to_define_conservatism_and_what_to_make_of_the_conservative_movement_in_the_USA);

$page->body($div_wikipedia_Conservatism);
