<?php
$page = new Page();
$page->h1("Council on Foreign Relations");
$page->tags("Organisation", "International");
$page->keywords("Council on Foreign Relations");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;




$div_Council_on_Foreign_Relations = new WebsiteContentSection();
$div_Council_on_Foreign_Relations->setTitleText("Council on Foreign Relations ");
$div_Council_on_Foreign_Relations->setTitleLink("https://www.cfr.org/");
$div_Council_on_Foreign_Relations->content = <<<HTML
	<p>The Council on Foreign Relations (CFR) is an independent, nonpartisan membership organization, think tank, and publisher
	dedicated to being a resource for its members, government officials, business executives, journalists, educators and students,
	civic and religious leaders, and other interested citizens in order to help them better understand the world and the foreign policy choices
	facing the United States and other countries. Founded in 1921, CFR takes no institutional positions on matters of policy.</p>
	HTML;



$div_wikipedia_Council_on_Foreign_Relations = new WikipediaContentSection();
$div_wikipedia_Council_on_Foreign_Relations->setTitleText("Council on Foreign Relations");
$div_wikipedia_Council_on_Foreign_Relations->setTitleLink("https://en.wikipedia.org/wiki/Council_on_Foreign_Relations");
$div_wikipedia_Council_on_Foreign_Relations->content = <<<HTML
	<p>The Council on Foreign Relations (CFR) is an American think tank specializing in U.S. foreign policy and international relations.
	Founded in 1921, it is an independent and nonpartisan 501(c)(3) nonprofit organization.
	CFR is based in New York City, with an additional office in Washington, D.C.
	Its membership has included senior politicians, secretaries of state, CIA directors, bankers, lawyers, professors,
	corporate directors, CEOs, and prominent media figures.</p>
	HTML;


$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_Council_on_Foreign_Relations);
$page->body($div_wikipedia_Council_on_Foreign_Relations);
