<?php
$page = new PersonPage();
$page->h1("Sean Penn");
$page->alpha_sort("Penn, Sean");
$page->viewport_background("/free/sean_penn.png");
$page->keywords("Sean Penn");
$page->stars(0);
$page->tags("Person");

$page->snp("description", "American actor and social activist");
$page->snp("image",       "/free/sean_penn.1200-630.png");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Sean Pean is an $American actor. Is is a co-founder of ${'Community Organized Relief Effort'}.</p>
	HTML;

$div_wikipedia_Sean_Penn = new WikipediaContentSection();
$div_wikipedia_Sean_Penn->setTitleText("Sean Penn");
$div_wikipedia_Sean_Penn->setTitleLink("https://en.wikipedia.org/wiki/Sean_Penn");
$div_wikipedia_Sean_Penn->content = <<<HTML
	<p>Sean Justin Penn (born August 17, 1960) is an American actor and film director. He is known for his intense leading man roles in film. Over his career, he has earned numerous accolades including two Academy Awards, a Golden Globe Award, and a Screen Actors Guild Award, as well as nominations for three BAFTA Film Awards. Penn received an Honorary César in 2015.
	In addition to his film work, Penn has also engaged in political and social activism. This includes his criticism of the George W. Bush administration, his contact with the presidents of Cuba and Venezuela, and his humanitarian work in the aftermath of Hurricane Katrina in 2005 and the 2010 Haiti earthquake, as well as his support for Ukraine President Volodymyr Zelenskyy amidst the Russian-Ukrainian War.</p>
	HTML;


$page->parent('list_of_people.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Sean Penn");
$page->body($div_wikipedia_Sean_Penn);
