<?php
$page = new OrganisationPage();
$page->h1('South Mountain Company');
$page->keywords('South Mountain');
$page->stars(0);
$page->tags("Organisation", "Fair Share");

$page->snp('description', 'Architecture, B Corp rated company founded by John Abrams.');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p>South Mountain company is an architecture, B Corp rated company founded by John Abrams.</p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>South Mountain company is an architecture, ${'B Corp'} rated company founded by ${'John Abrams'}.</p>
	HTML;



$div_South_Mountain_Company = new WebsiteContentSection();
$div_South_Mountain_Company->setTitleText('South Mountain Company website');
$div_South_Mountain_Company->setTitleLink('https://www.southmountain.com/');
$div_South_Mountain_Company->content = <<<HTML
	<p>South Mountain Company is a fully integrated architecture & engineering, building, interiors and renewable energy firm.
	That’s what we do.
	But to us, how we do it is just as important.
	We are an impact-driven organization guided by core principles.
	We pay as much attention to the craft of business as we do to our design, engineering and building.</p>

	<p>South Mountain Company is committed to a Triple Bottom Line,
	which means that people, planet, and profit are all equally important to us – balancing profits
	with environmental restoration, social justice, and community engagement. We are a certified B Corp and worker-owned cooperative.</p>
	HTML;



$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);
$page->body('john_abrams.html');
$page->body('b_lab.html');
$page->body($div_South_Mountain_Company);


