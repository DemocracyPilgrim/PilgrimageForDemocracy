<?php
$page = new Page();
$page->h1("Kelo v. City of New London (2005)");
$page->tags("Institutions: Judiciary", "SCOTUS Landmark Decision", "USA", "Eminent Domain");
$page->keywords("Kelo v. City of New London");
$page->stars(1);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>"Kelo v. City of New London" is a 2005 landmark decision by the ${'Supreme Court of the United States'}.</p>

	<p>This ruling upheld the government's use of eminent domain to seize private property for economic development projects, even when the property was not blighted.
	This decision has been criticized for weakening property rights and enabling government overreach in economic development.</p>
	HTML;

$div_wikipedia_Kelo_v_City_of_New_London = new WikipediaContentSection();
$div_wikipedia_Kelo_v_City_of_New_London->setTitleText("Kelo v City of New London");
$div_wikipedia_Kelo_v_City_of_New_London->setTitleLink("https://en.wikipedia.org/wiki/Kelo_v._City_of_New_London");
$div_wikipedia_Kelo_v_City_of_New_London->content = <<<HTML
	<p>Kelo v. City of New London, 545 U.S. 469 (2005), was a landmark decision by the Supreme Court of the United States
	in which the Court held, 5–4, that the use of eminent domain to transfer land from one private owner to another private owner
	to further economic development does not violate the Takings Clause of the Fifth Amendment.
	In the case, plaintiff Susette Kelo sued the city of New London, Connecticut, for violating her civil rights
	after the city tried to acquire her house's property through eminent domain so that the land could be used as part of a "comprehensive redevelopment plan".
	Justice John Paul Stevens wrote for the five-justice majority that the city's use of eminent domain was permissible under the Takings Clause,
	because the general benefits the community would enjoy from economic growth qualified as "public use".</p>
	HTML;


$page->parent('supreme_court_of_the_united_states.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_wikipedia_Kelo_v_City_of_New_London);
