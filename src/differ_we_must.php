<?php
$page = new Page();
$page->h1('Differ We Must: How Lincoln Succeeded in a Divided America');
$page->tags("Book", "USA", "Information: Discourse", "Political Discourse");
$page->stars(0);

//$page->snp('description', '');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Steve_Inskeep = new WikipediaContentSection();
$div_wikipedia_Steve_Inskeep->setTitleText('Steve Inskeep');
$div_wikipedia_Steve_Inskeep->setTitleLink('https://en.wikipedia.org/wiki/Steve_Inskeep');
$div_wikipedia_Steve_Inskeep->content = <<<HTML
	<p>Inskeep is the author of four books:
	Instant City: Life and Death in Karachi (2011),
	Jacksonland: President Andrew Jackson, Cherokee Chief John Ross, and a Great American Land Grab (2015),
	Imperfect Union: How Jessie and John Fremont Mapped the West, Invented Celebrity, and Helped Cause the Civil War (2020),
	and Differ We Must: How Lincoln Succeeded in a Divided America (2023).</p>
	HTML;


$page->parent('list_of_books.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_wikipedia_Steve_Inskeep);
