<?php
$page = new Page();
$page->h1("Development Policy Centre");
$page->keywords("Development Policy Centre");
$page->tags("Organisation");
$page->stars(0);

//$page->snp("description", "");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;



$div_Development_Policy_Centre_Crawford_School_of_Public_Policy = new WebsiteContentSection();
$div_Development_Policy_Centre_Crawford_School_of_Public_Policy->setTitleText("Development Policy Centre, Crawford School of Public Policy");
$div_Development_Policy_Centre_Crawford_School_of_Public_Policy->setTitleLink("https://devpolicy.crawford.anu.edu.au/");
$div_Development_Policy_Centre_Crawford_School_of_Public_Policy->content = <<<HTML
	<p>The Development Policy Centre (Devpol) is a think tank for aid and development serving Australia, the region, and the global development community.
	We undertake independent research and promote practical initiatives to improve the effectiveness of Australian aid,
	to support the development of Papua New Guinea and the Pacific island region, and to contribute to better global development policy.
	Our discussion papers, policy briefs and reports make our research available for all.
	Our events are fora for the dissemination of findings and the exchange of information and ideas.
	The Devpolicy Blog is our platform for analysis, discussion and debate.
	We are based at Crawford School of Public Policy in the College of Asia and the Pacific at The Australian National University.</p>
	HTML;



$div_Devpolicy_Blog = new WebsiteContentSection();
$div_Devpolicy_Blog->setTitleText("Devpolicy Blog ");
$div_Devpolicy_Blog->setTitleLink("https://devpolicy.org/");
$div_Devpolicy_Blog->content = <<<HTML
	<p>Established in September 2010, the Devpolicy Blog provides a platform for the best in aid and development analysis, research and policy comment,
	with global coverage and a focus on Australia, the Pacific and Papua New Guinea.
	As of June 2022, Devpolicy has published more than 3,900 posts from more than 1,300 contributors.</p>

	<p>The blog is run out of the Development Policy Centre housed in the Crawford School of Public Policy in the ANU College of Asia
	and the Pacific at The Australian National University.</p>
	HTML;



$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);

$page->body($div_Development_Policy_Centre_Crawford_School_of_Public_Policy);
$page->body($div_Devpolicy_Blog);
