<?php
$page = new DocumentaryPage();
$page->h1("Myanmar: The Rebel Army | ARTE.tv Documentary");
$page->viewport_background("");
$page->keywords("Myanmar: The Rebel Army");
$page->stars(0);
$page->tags("Documentary", "Myanmar", "Arte");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;




$div_Myanmar_The_Rebel_Army = new WebsiteContentSection();
$div_Myanmar_The_Rebel_Army->setTitleText("Myanmar: The Rebel Army ");
$div_Myanmar_The_Rebel_Army->setTitleLink("https://www.arte.tv/en/videos/118107-000-A/arte-reportage/");
$div_Myanmar_The_Rebel_Army->content = <<<HTML
	<p>
	</p>
	HTML;




$div_youtube_Myanmar_The_Rebel_Army_ARTE_tv_Documentary = new YoutubeContentSection();
$div_youtube_Myanmar_The_Rebel_Army_ARTE_tv_Documentary->setTitleText("Myanmar: The Rebel Army | ARTE.tv Documentary");
$div_youtube_Myanmar_The_Rebel_Army_ARTE_tv_Documentary->setTitleLink("https://www.youtube.com/watch?v=bmLBNCfXC34");
$div_youtube_Myanmar_The_Rebel_Army_ARTE_tv_Documentary->content = <<<HTML
	<p>
	</p>
	HTML;




$page->parent('list_of_documentaries.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Myanmar: The Rebel Army");

$page->body($div_Myanmar_The_Rebel_Army);
$page->body($div_youtube_Myanmar_The_Rebel_Army_ARTE_tv_Documentary);
