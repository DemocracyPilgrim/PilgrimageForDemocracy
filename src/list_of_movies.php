<?php
$page = new Page();
$page->h1('List of movies');
$page->stars(0);
$page->keywords('movies', 'list of movies');

$page->preview( <<<HTML
	<p>Movies related to peace, democracy or social justice.</p>
	HTML );


$page->snp('description', "Some movies worth watching...");
//$page->snp('image', "/copyrighted/");


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Some movies which are related to the themes discussed in the $Pilgrimage.</p>
	HTML;


$div_codeberg_List_of_documentaries_and_movies = new CodebergContentSection();
$div_codeberg_List_of_documentaries_and_movies->setTitleText('List of documentaries and movies');
$div_codeberg_List_of_documentaries_and_movies->setTitleLink('https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy/issues/3');
$div_codeberg_List_of_documentaries_and_movies->content = <<<HTML
	<p>list of relevant documentaries and movies.</p>
	HTML;


$page->parent('lists.html');

$page->body($div_introduction);
$page->related_tag("Movie");

// Featured
$page->body('the_remains_of_the_day.html');
$page->body('list_of_documentaries.html');
$page->body($div_codeberg_List_of_documentaries_and_movies);
