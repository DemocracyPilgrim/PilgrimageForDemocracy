<?php
$page = new Page();
$page->h1("Ken Burns");
$page->alpha_sort("Burns, Ken");
$page->tags("Person", "Historian", "Information: Media", "Liberty Medal Recipient");
$page->keywords("Ken Burns");
$page->stars(0);

$page->snp("description", "American documentary filmmaker.");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref('', '');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Ken Burns is an $American history documentary filmmaker.</p>

	<p>Burn gave the keynote address at Brandeis University at the 2024 graduation ceremony, where he gave a powerful defense of democracy,
	quoting ${'Louis Brandeis'}, US Supreme Court justice, after whom the university is named.</p>

	<blockquote>
		The most important political office is that of private citizen.
		<br>
		-- ${'Louis Brandeis'}
	</blockquote>

	<p>In 2024, Burns was awarded the ${'Liberty Medal'}.</p>
	HTML;



$div_Brandeis_Alumni_Family_and_Friends = new WebsiteContentSection();
$div_Brandeis_Alumni_Family_and_Friends->setTitleText("Brandeis Alumni: Transcript of Ken Burns Keynote Address to Brandeis University's 2024 Graduates");
$div_Brandeis_Alumni_Family_and_Friends->setTitleLink("https://alumni.brandeis.edu/news/2024/commencement/transcripts/ken-burns-remarks.html");
$div_Brandeis_Alumni_Family_and_Friends->content = <<<HTML
	<p>Opening statement:</p>

	<blockquote>Listen, I am in the business of history.
	It is not always a happy subject on college campuses these days,
	particularly when forces seem determined to eliminate or water down difficult parts of our past,
	particularly when the subject may seem to sum an anachronistic and irrelevant pursuit, and
	particularly with the ferocious urgency this moment seems to exert on us.
	It is my job, however, to remind people of the power our past also exerts,
	to help us better understand what's going on now with compelling story, memory, and anecdote.
	It is my job to try to discern patterns and themes from history to enable us to interpret our dizzying and sometimes dismaying present.
	<br>
	<br>(...)
	<blockquote>
	HTML;




$div_wikipedia_Ken_Burns = new WikipediaContentSection();
$div_wikipedia_Ken_Burns->setTitleText("Ken Burns");
$div_wikipedia_Ken_Burns->setTitleLink("https://en.wikipedia.org/wiki/Ken_Burns");
$div_wikipedia_Ken_Burns->content = <<<HTML
	<p>Kenneth Lauren Burns (born July 29, 1953) is an American filmmaker known for his documentary films and television series,
	many of which chronicle American history and culture.
	His work is often produced in association with WETA-TV and/or the National Endowment for the Humanities and distributed by PBS.</p>
	HTML;


$page->parent('list_of_people.html');
$page->template("stub");
$page->body($div_introduction);
$page->related_tag("Ken Burns");

$page->body($div_Brandeis_Alumni_Family_and_Friends);

$page->body($div_wikipedia_Ken_Burns);
