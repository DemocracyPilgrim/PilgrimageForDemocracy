<?php
$page = new VideoPage();
$page->h1("Robert Reich: The Republican Party Is Over");
$page->tags("Video: News Story", "Robert Reich", "Republican Party", "USA", "Donald Trump");
$page->keywords("Robert Reich: The Republican Party Is Over");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;



$div_youtube_The_Republican_Party_Is_Over_Robert_Reich = new YoutubeContentSection();
$div_youtube_The_Republican_Party_Is_Over_Robert_Reich->setTitleText("The Republican Party Is Over | Robert Reich");
$div_youtube_The_Republican_Party_Is_Over_Robert_Reich->setTitleLink("https://www.youtube.com/watch?v=S-3Wv8s3NAA");
$div_youtube_The_Republican_Party_Is_Over_Robert_Reich->content = <<<HTML
	<p>
	</p>
	HTML;




$page->parent('list_of_videos.html');
$page->template("stub");
$page->body($div_introduction);
$page->body($div_youtube_The_Republican_Party_Is_Over_Robert_Reich);
