<?php
$page = new Page();
$page->h1("Democratic backsliding in the United States");
$page->tags("Elections", "USA");
$page->keywords("Democratic backsliding in the United States");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Democratic_backsliding_in_the_United_States = new WikipediaContentSection();
$div_wikipedia_Democratic_backsliding_in_the_United_States->setTitleText("Democratic backsliding in the United States");
$div_wikipedia_Democratic_backsliding_in_the_United_States->setTitleLink("https://en.wikipedia.org/wiki/Democratic_backsliding_in_the_United_States");
$div_wikipedia_Democratic_backsliding_in_the_United_States->content = <<<HTML
	<p>Democratic backsliding in the United States has been identified as a trend at the state and national levels in various indices and analyses.
	Democratic backsliding is "a process of regime change towards autocracy that makes the exercise of political power more arbitrary and repressive
	and that restricts the space for public contestation and political participation in the process of government selection".</p>
	HTML;


$page->parent('elections.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_wikipedia_Democratic_backsliding_in_the_United_States);
