<?php
$page = new PersonPage();
$page->h1("Richard Daynard");
$page->alpha_sort("Daynard, Richard");
$page->viewport_background("/free/richard_daynard.png");
$page->keywords("Richard Daynard");
$page->stars(2);
$page->tags("Person", "Lawyer", "Tobacco", "Gambling", "Organic Taxes", "Personal Responsibility Rhetoric");

$page->snp("description", "American legal crusader for public health and safety.");
$page->snp("image",       "/free/richard_daynard.1200-630.png");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<h3>Richard Daynard: A Legal Crusader for Public Health and Safety</h3>

	<p>Richard Daynard is an American legal scholar renowned for his pioneering work in using litigation to combat public health threats,
	particularly those posed by the tobacco industry.
	His career demonstrates a relentless commitment to holding powerful corporations accountable for the harm they inflict on society.</p>

	<p>Daynard's impact stems from his innovative legal strategies.
	As a professor at Northeastern University School of Law, he founded and directs the Public Health Advocacy Institute (PHAI).
	This institute is dedicated to using legal and advocacy tools to address a wide range of public health issues.</p>

	<p>Daynard is best known for his crucial role in the battle against tobacco companies.
	He recognized the immense legal challenges in directly suing tobacco companies for the health consequences of smoking.
	However, Daynard pioneered a different approach:
	He spearheaded litigation using the Racketeer Influenced and Corrupt Organizations Act (RICO)
	to expose the decades of deception and conspiracy by Big Tobacco to suppress the truth about the dangers of their products.</p>

	<p>His work, and that of his team at PHAI, contributed significantly to the landmark 1998 Master Settlement Agreement (MSA).
	This agreement, reached between 46 U.S. states and the major tobacco companies,
	resulted in billions of dollars being paid to states to cover healthcare costs associated with smoking
	and imposed significant restrictions on tobacco advertising and marketing.</p>

	<p>Daynard's efforts extended beyond this victory.
	He has continued to analyze and expose tobacco industry practices, including their targeted marketing toward vulnerable populations, like youth.
	He remains an outspoken advocate for strong tobacco control policies,
	including increased taxation, smoke-free public places, and counter-advertising campaigns.</p>

	<p>While his work on tobacco is his most recognized contribution, Daynard has also expanded his focus to other public health issues.
	He has explored the use of litigation to combat gun violence,
	promoting laws to hold gun manufacturers and retailers accountable for the harm they inflict.
	He has also taken on the predatory practices of the payday loan industry and the dangers of certain food products.</p>
	HTML;

$div_Engagement_Against_Betting_Markets = new ContentSection();
$div_Engagement_Against_Betting_Markets->content = <<<HTML
	<h3>Engagement Against Betting Markets</h3>

	<p>Daynard's commitment to protecting the public from harmful industries also extends to the world of betting markets.
	He has expressed concerns about the potential for market manipulation and the negative social consequences associated with widespread gambling,
	particularly in its increasingly digital formats.
	This engagement indicates a broader awareness of the systemic risks that can arise when profit motives outweigh ethical considerations.</p>

	<p>Richard Daynard is more than just a legal scholar;
	he is a social justice advocate who has fundamentally changed how the law can be used to promote public health and safety.
	His legacy is a powerful reminder that even the most entrenched corporate power can be challenged and held accountable
	through innovative legal strategies and unwavering dedication.</p>
	HTML;

$div_wikipedia_Richard_Daynard = new WikipediaContentSection();
$div_wikipedia_Richard_Daynard->setTitleText("Richard Daynard");
$div_wikipedia_Richard_Daynard->setTitleLink("https://en.wikipedia.org/wiki/Richard_Daynard");
$div_wikipedia_Richard_Daynard->content = <<<HTML
	<p>Richard A. Daynard (born 1944) is an American legal scholar. He is a University Distinguished Professor at the Northeastern University School of Law.</p>
	HTML;


$page->parent('list_of_people.html');

$page->body($div_introduction);
$page->body($div_Engagement_Against_Betting_Markets);



$page->related_tag("Richard Daynard");
$page->body($div_wikipedia_Richard_Daynard);
