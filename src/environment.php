<?php
$page = new Page();
$page->h1('Addendum F: Environment');
$page->viewport_background('/free/environment.png');
$page->keywords("Environment", "environment", "environmental", "Environmental");
$page->stars(0);
$page->tags("Topic portal");

$page->snp('description', 'For our generation and future generations.');
$page->snp('image',       '/free/environment.1200-630.png');

$page->preview( <<<HTML
	<p>Our natural environment, and its protection, is one of the major critical topics for our generation and for future generations.</p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Besides $democracy and ${'social justice'},
	our natural environment and its protection is one of the major critical topics of our time.</p>
	HTML;


$page->parent('environmental_stewardship.html');
$page->previous("taxes.html");
$page->next("web.html");

$page->body($div_introduction);
$page->related_tag("Environment");
