<?php
$page = new Page();
$page->h1("Labour");
$page->viewport_background("");
$page->keywords("Labour", "Labor", "labour", "labor");
$page->stars(0);
$page->tags("Living");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Work_human_activity = new WikipediaContentSection();
$div_wikipedia_Work_human_activity->setTitleText("Work human activity");
$div_wikipedia_Work_human_activity->setTitleLink("https://en.wikipedia.org/wiki/Work_(human_activity)");
$div_wikipedia_Work_human_activity->content = <<<HTML
	<p>Work or labor (labour in Commonwealth English) is the intentional activity people perform to support the needs and desires of themselves, other people, or organizations. In the context of economics, work can be viewed as the human activity that contributes (along with other factors of production) towards the goods and services within an economy.</p>
	HTML;

$div_wikipedia_Wage_labour = new WikipediaContentSection();
$div_wikipedia_Wage_labour->setTitleText("Wage labour");
$div_wikipedia_Wage_labour->setTitleLink("https://en.wikipedia.org/wiki/Wage_labour");
$div_wikipedia_Wage_labour->content = <<<HTML
	<p>Wage labour (also wage labor in American English), usually referred to as paid work, paid employment, or paid labour, refers to the socioeconomic relationship between a worker and an employer in which the worker sells their labour power under a formal or informal employment contract. These transactions usually occur in a labour market where wages or salaries are market-determined.</p>
	HTML;


$page->parent('living.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag(array("Labor", "Labour"));
$page->body($div_wikipedia_Work_human_activity);
$page->body($div_wikipedia_Wage_labour);
