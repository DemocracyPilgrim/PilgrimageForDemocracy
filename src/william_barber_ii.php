<?php
$page = new Page();
$page->h1("William Barber II");
$page->alpha_sort("Barber, William");
$page->tags("Person");
$page->keywords("William Barber II");
$page->stars(0);

$page->snp("description", "American protestant minister and anti-poverty social activist.");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref('', '');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>William J. Barker II is an $American protestant minister and anti-$poverty social activist.
	He is the co-author of the book: "White Poverty: How Exposing Myths About Race and Class Can Reconstruct American Democracy".</p>

	<p>He is a leader of the ${"Poor People's Campaign: A National Call for a Moral Revival"}.</p>
	HTML;

$div_wikipedia_William_Barber_II = new WikipediaContentSection();
$div_wikipedia_William_Barber_II->setTitleText("William Barber II");
$div_wikipedia_William_Barber_II->setTitleLink("https://en.wikipedia.org/wiki/William_Barber_II");
$div_wikipedia_William_Barber_II->content = <<<HTML
	<p>William J. Barber II (born August 30, 1963) is an American Protestant minister, social activist,
	professor in the Practice of Public Theology and Public Policy and founding director of the Center for Public Theology & Public Policy at Yale Divinity School.
	He is the president and senior lecturer at Repairers of the Breach and co-chair of the Poor People's Campaign: A National Call for a Moral Revival.
	He also serves as a member of the national board of the National Association for the Advancement of Colored People (NAACP)
	and is the chair of its legislative political action committee.</p>
	HTML;


$page->parent('list_of_people.html');
$page->template("stub");
$page->body($div_introduction);

$page->body('white_poverty_how_exposing_myths_about_race_and_class_can_reconstruct_american_democracy.html');

$page->body($div_wikipedia_William_Barber_II);
