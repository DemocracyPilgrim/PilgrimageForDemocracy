<?php
$page = new Page();
$page->h1('Institutional vs Human Factors in Democracy');
$page->stars(1);

$page->snp('description', 'Institutional and human factors influencing the quality of a democracy.');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p>The quality of a democracy is affected by a huge number of factors,
	which could broadly be sorted into two categories:
	the human factors and the institutional factors
	which influence each other.
	The institutions are a reflection of the human who created them,
	and human behaviour is often directly influenced by the institutions.</p>
	HTML );


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>The quality of a democracy is affected by a huge number of factors,
	which could broadly be sorted into two categories:
	the human factors and the institutional factors
	which influence each other.
	The institutions are a reflection of the human who created them,
	and human behaviour is often directly influenced by the institutions.
	In order to improve our democracies, it can be useful to distinguish the human versus the institutional factors,
	and the ways they interact with each other.</p>
	HTML;

$h2_Examples = new h2HeaderContent('Examples');


$div_Election_methods = new ContentSection();
$div_Election_methods->content = <<<HTML
	<h3>Election methods</h3>

	<p>
	</p>
	HTML;


$h2_Institutional_factors = new h2HeaderContent('Institutional factors');

$div_institutional_factors = new ContentSection();
$div_institutional_factors->content = <<<HTML
	<p>The main  institutional factors are:</p>
	<ul>
		<li>Separation of powers</li>
		<li>Justice and accountability</li>
		<li>Election method</li>
		<li>Media</li>
	</ul>
	HTML;


$h2_Human_factors = new h2HeaderContent('Human factors');

$div_Human_factors = new ContentSection();
$div_Human_factors->content = <<<HTML
	<p>In order of (arguably) decreasing importance, the main human factors are:</p>
	<ul>
		<li>Perpetrators of crimes.</li>
		<li>The human quality of elected officials</li>
		<li>The quality of the lobbies</li>
		<li>Citizens, as voters and as members of society.</li>
	</ul>
	HTML;



$page->parent('institutions.html');
$page->template("stub");

$page->body($div_introduction);

$page->body($h2_Examples);
$page->body('mass_shootings_the_human_factor.html');
$page->body($div_Election_methods);

$page->body($h2_Institutional_factors);
$page->body($div_institutional_factors);

$page->body($h2_Human_factors);
$page->body($div_Human_factors);

$page->body('saints_and_little_devils.html');
