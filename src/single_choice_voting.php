<?php
$page = new Page();
$page->h1("Single Choice Voting");
$page->keywords("Single Choice Voting");
$page->tags("Elections");
$page->stars(0);

//$page->snp("description", "");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref('', '');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>See: ${'plurality voting'}</p>
	HTML;


$page->parent('voting_methods.html');
$page->template("stub");
$page->body($div_introduction);
