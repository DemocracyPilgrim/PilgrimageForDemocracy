<?php
$page = new CountryPage('Dominican Republic');
$page->h1('Dominican Republic');
$page->tags("Country");
$page->keywords('Dominican Republic');
$page->stars(0);

$page->snp('description', '10,8 million inhabitants.');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Dominican_Republic = new WikipediaContentSection();
$div_wikipedia_Dominican_Republic->setTitleText('Dominican Republic');
$div_wikipedia_Dominican_Republic->setTitleLink('https://en.wikipedia.org/wiki/Dominican_Republic');
$div_wikipedia_Dominican_Republic->content = <<<HTML
	<p>The Dominican Republic is a country located on the island of Hispaniola in the Greater Antilles archipelago of the Caribbean region.
	It occupies the eastern five-eighths of the island, which it shares with $Haiti.</p>
	HTML;

$div_wikipedia_Politics_of_the_Dominican_Republic = new WikipediaContentSection();
$div_wikipedia_Politics_of_the_Dominican_Republic->setTitleText('Politics of the Dominican Republic');
$div_wikipedia_Politics_of_the_Dominican_Republic->setTitleLink('https://en.wikipedia.org/wiki/Politics_of_the_Dominican_Republic');
$div_wikipedia_Politics_of_the_Dominican_Republic->content = <<<HTML
	<p>The Dominican Republic is a representative democracy,
	where the President of the Dominican Republic functions as both the head of the government and head of the multi-party system.</p>
	HTML;

$div_wikipedia_Racism_in_the_Dominican_Republic = new WikipediaContentSection();
$div_wikipedia_Racism_in_the_Dominican_Republic->setTitleText('Racism in the Dominican Republic');
$div_wikipedia_Racism_in_the_Dominican_Republic->setTitleLink('https://en.wikipedia.org/wiki/Racism_in_the_Dominican_Republic');
$div_wikipedia_Racism_in_the_Dominican_Republic->content = <<<HTML
	<p>Racism in the Dominican Republic exists due to the after-effects of African slavery and the subjugation of black people throughout history.
	In the Dominican Republic, "blackness" is often associated with Haitian migrants and a lower class status.
	Those who possess more African-like phenotypic features are often victims of discrimination, and are seen as foreigners.</p>
	HTML;

$div_wikipedia_Haitians_in_the_Dominican_Republic = new WikipediaContentSection();
$div_wikipedia_Haitians_in_the_Dominican_Republic->setTitleText('Haitians in the Dominican Republic');
$div_wikipedia_Haitians_in_the_Dominican_Republic->setTitleLink('https://en.wikipedia.org/wiki/Haitians_in_the_Dominican_Republic');
$div_wikipedia_Haitians_in_the_Dominican_Republic->content = <<<HTML
	<p>The Haitian minority of the Dominican Republic is the largest ethnic minority in the Dominican Republic since the early 20th century.</p>
	HTML;


$page->parent('world.html');
$page->template("stub");
$page->body($div_introduction);
$page->body('Country indices');

$page->body($div_wikipedia_Dominican_Republic);
$page->body($div_wikipedia_Politics_of_the_Dominican_Republic);
$page->body($div_wikipedia_Racism_in_the_Dominican_Republic);
$page->body($div_wikipedia_Haitians_in_the_Dominican_Republic);
