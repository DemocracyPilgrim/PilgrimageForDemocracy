<?php
$page = new Page();
$page->h1("World Central Kitchen");
$page->keywords("World Central Kitchen");
$page->tags("Organisation", "Humanity");
$page->stars(0);

//$page->snp("description", "");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;



$div_World_Central_Kitchen_website = new WebsiteContentSection();
$div_World_Central_Kitchen_website->setTitleText("World Central Kitchen website ");
$div_World_Central_Kitchen_website->setTitleLink("https://wck.org/");
$div_World_Central_Kitchen_website->content = <<<HTML
	<p>WCK is first to the frontlines, providing meals in response to humanitarian, climate, and community crises.</p>
	HTML;



$div_wikipedia_World_Central_Kitchen = new WikipediaContentSection();
$div_wikipedia_World_Central_Kitchen->setTitleText("World Central Kitchen");
$div_wikipedia_World_Central_Kitchen->setTitleLink("https://en.wikipedia.org/wiki/World_Central_Kitchen");
$div_wikipedia_World_Central_Kitchen->content = <<<HTML
	<p>World Central Kitchen (WCK) is a not-for-profit, non-governmental organization that provides food relief.
	It was founded in 2010 by Spanish American chef and restaurateur José Andrés following the earthquake in Haiti,
	and has subsequently responded to Hurricane Harvey, the 2018 lower Puna eruption,
	2023 Turkey–Syria earthquakes, and the ongoing Gaza humanitarian crisis.</p>
	HTML;



$div_wikipedia_We_Feed_People = new WikipediaContentSection();
$div_wikipedia_We_Feed_People->setTitleText("We Feed People ");
$div_wikipedia_We_Feed_People->setTitleLink("https://en.wikipedia.org/wiki/We_Feed_People");
$div_wikipedia_We_Feed_People->content = <<<HTML
	<p>We Feed People is a 2022 American documentary film directed by Ron Howard
	that chronicles how chef José Andrés and his nonprofit World Central Kitchen rebuild nations in the wake of disaster,
	providing food to those affected.</p>
	HTML;




$div_They_Had_Just_Delivered_Tons_of_Food_Then_Their_Convoy_Was_Hit = new WebsiteContentSection();
$div_They_Had_Just_Delivered_Tons_of_Food_Then_Their_Convoy_Was_Hit->setTitleText("They Had Just Delivered Tons of Food. Then Their Convoy Was Hit.");
$div_They_Had_Just_Delivered_Tons_of_Food_Then_Their_Convoy_Was_Hit->setTitleLink("https://www.nytimes.com/2024/04/03/world/middleeast/israel-world-central-kitchen-gaza.html");
$div_They_Had_Just_Delivered_Tons_of_Food_Then_Their_Convoy_Was_Hit->content = <<<HTML
	<p>The deaths of World Central Kitchen workers pushed the number of aid employees
	killed during the war in Gaza to at least 196, according to the U.N. secretary general, António Guterres.</p>
	HTML;




$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_World_Central_Kitchen_website);

$page->body($div_wikipedia_World_Central_Kitchen);
$page->body($div_wikipedia_We_Feed_People);

$page->body($div_They_Had_Just_Delivered_Tons_of_Food_Then_Their_Convoy_Was_Hit);
