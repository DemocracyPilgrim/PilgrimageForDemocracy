<?php
$page = new Page();
$page->h1('Duverger example: Taiwan 2000 and 2004 presidential elections');
$page->stars(0);
$page->tags("Elections: Duverger Syndrome", "Taiwan");

//$page->snp('description', "");
//$page->snp('image', "/copyrighted/");

$page->preview( <<<HTML
	<p>The 2000 and 2004 presidential elections in the Republic of China, Taiwan
	are the best historical illustration of Duverger's Law,
	and mimic quite closely what occurred almost two centuries earlier in the United States
	during their 1836 and 1840 elections.</p>
	HTML );

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>The 2000 and 2004 presidential elections in the ${'Republic of China'}, $Taiwan
	are the best historical illustration of ${"Duverger's Law"},
	and mimic quite closely what occurred almost two centuries earlier in the ${'United States'}
	during their 1836 and 1840 elections.</p>

	<p>These two elections in Taiwan perfectly illustrate the mechanism of Duverger's Law,
	due to the use of ${"plurality voting"}, and the core political problems that arise from it.
	It is critical for Taiwan and other $democracies to adopt a better ${'voting method'}.</p>

	<p>The 2000 election was only the second free popular presidential election,
	and the one that would see the end of the KMT rule over the country,
	and that would prove that Taiwan had indeed become a democracy.</p>

	<p>However, opposition candidate Chen Shui-bian, although elected,
	would not win with a majority of the votes.</p>
	HTML;


$div_2000_Presidential_Election = new ContentSection();
$div_2000_Presidential_Election->content = <<<HTML
	<h3>2000 Presidential Election</h3>

	<p>
	</p>

	<style>
		table.voting.method tr td strong {
			text-decoration: underline;
		}
	</style>

	<table class="voting method">
		<tr>
			<th>Candidate</th>
			<th>Party</th>
			<th>Result</th>
			<th>Winner</th>
		</tr>
		<tr class="dpp">
			<td><strong>Chen Shui-bian 陳水扁</strong>
			    <br><strong>Annette Lu 呂秀蓮</strong>
			</td>
			<td>Democratic Progressive Party 民主進步黨</td>
			<td>39.30%</td>
			<td class="winner" style="color: white;">&check;</td>
		</tr>
		<tr class="tw-people-first-party">
			<td><strong>James Soong 宋楚瑜</strong>
			    <br>Chang Chau-hsiung 張昭雄
			</td>
			<td>Independent 無黨籍</td>
			<td>36.84%</td>
			<td></td>
		</tr>
		<tr class="kmt">
			<td><strong>Lien Chan 連戰</strong>
			    <br>Vincent Siew 蕭萬長
			</td>
			<td>Kuomintang 中國國民黨</td>
			<td>23.10%</td>
			<td></td>
		</tr>
		<tr class="independent">
			<td>Hsu Hsin-liang 許信良
			    <br>Josephine Chu 朱惠良
			</td>
			<td>Independent 無黨籍</td>
			<td>0.63%</td>
			<td></td>
		</tr>
		<tr class="tw-new-party">
			<td>Li Ao 李敖
			    <br>Elmer Fung 馮滬祥
			</td>
			<td>New Party 新黨</td>
			<td>0.13%</td>
			<td></td>
		</tr>
	</table>
	HTML;

$div_2004_Presidential_Election = new ContentSection();
$div_2004_Presidential_Election->content = <<<HTML
	<h3>2004 Presidential Election</h3>

	<p>
	</p>
	<table class="voting method">
		<tr>
			<th>Candidate</th>
			<th>Party</th>
			<th>Result</th>
			<th>Winner</th>
		</tr>
		<tr class="dpp">
			<td>Chen Shui-bian 陳水扁
			    <br>Annette Lu 呂秀蓮
			</td>
			<td>Democratic Progressive Party 民主進步黨</td>
			<td>50.11%</td>
			<td class="winner" style="color: white;">&check;</td>
		</tr>
		<tr class="kmt">
			<td>Lien Chan 連戰
			    <br>James Soong 宋楚瑜
			</td>
			<td>Kuomintang 中國國民黨</td>
			<td>49.89%</td>
			<td></td>
		</tr>
	</table>
	HTML;



$div_wikipedia_2000_Taiwanese_presidential_election = new WikipediaContentSection();
$div_wikipedia_2000_Taiwanese_presidential_election->setTitleText("2000 Taiwanese presidential election");
$div_wikipedia_2000_Taiwanese_presidential_election->setTitleLink("https://en.wikipedia.org/wiki/2000_Taiwanese_presidential_election");
$div_wikipedia_2000_Taiwanese_presidential_election->content = <<<HTML
	<p>Presidential elections were held in Taiwan on 18 March 2000 to elect the president and vice president.
	With a voter turnout of 83%, Chen Shui-bian and Annette Lu of the Democratic Progressive Party (DPP)
	were elected president and vice president respectively with a slight plurality.</p>
	HTML;



$div_wikipedia_2004_Taiwanese_presidential_election = new WikipediaContentSection();
$div_wikipedia_2004_Taiwanese_presidential_election->setTitleText("2004 Taiwanese presidential election");
$div_wikipedia_2004_Taiwanese_presidential_election->setTitleLink("https://en.wikipedia.org/wiki/2004_Taiwanese_presidential_election");
$div_wikipedia_2004_Taiwanese_presidential_election->content = <<<HTML
	<p>Presidential elections were held in Taiwan on 20 March 2004.
	A consultative referendum took place on the same day regarding relations with the People's Republic of China.</p>

	<p>President Chen Shui-bian and Vice President Annette Lu of the Democratic Progressive Party
	were re-elected by a narrow margin of 0.22%
	over a combined opposition ticket of Kuomintang (KMT) Chairman Lien Chan and People First Party Chairman James Soong.
	Lien and Soong refused to concede and unsuccessfully challenged the results.</p>
	HTML;

$page->parent('duverger_syndrome.html');
$page->body($div_introduction);

$page->body($div_2000_Presidential_Election);
$page->body($div_2004_Presidential_Election);

$page->body($div_wikipedia_2000_Taiwanese_presidential_election);
$page->body($div_wikipedia_2004_Taiwanese_presidential_election);
