<?php
$page = new Page();
$page->h1("International Democracy Community");
$page->keywords("International Democracy Community");
$page->tags("Organisation");
$page->stars(0);

//$page->snp("description", "");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>This website is dedicated to those active on issues of direct democracy, participation and citizens' rights around the world.
	Countless people and organisations around the world work daily
	to make the way we make decisions more inclusive and to improve the societies we live in.</p>
	HTML;


$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);
