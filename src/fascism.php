<?php
$page = new Page();
$page->h1('Fascism');
$page->keywords('Fascism', 'fascism');
$page->stars(0);

//$page->snp('description', '');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Fascism = new WikipediaContentSection();
$div_wikipedia_Fascism->setTitleText('Fascism');
$div_wikipedia_Fascism->setTitleLink('https://en.wikipedia.org/wiki/Fascism');
$div_wikipedia_Fascism->content = <<<HTML
	<p>Fascism is a far-right, authoritarian, ultranationalist political ideology and movement, characterized by a dictatorial leader,
	centralized autocracy, militarism, forcible suppression of opposition, belief in a natural social hierarchy,
	subordination of individual interests for the perceived good of the nation or race, and strong regimentation of society and the economy.</p>
	HTML;


$page->parent('authoritarianism.html');
$page->template("stub");
$page->body($div_introduction);
$page->body('american_fascism.html');
$page->related_tag("Fascism");


$page->body($div_wikipedia_Fascism);
