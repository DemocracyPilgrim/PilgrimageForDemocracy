<?php
$page = new Page();
$page->h1("Erwin Chemerinsky");
$page->alpha_sort("Chemerinsky, Erwin");
$page->tags("Person", "Lawyer", "USA", "Institutions: Judiciary", "Constitution");
$page->keywords("Erwin Chemerinsky");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

$r1 = $page->ref("https://www.theguardian.com/books/article/2024/sep/01/erwin-chemerinsky-no-democracy-lasts-forever", "Erwin Chemerinsky on the need for a new US constitution: ‘Our democracy is at grave risk’");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Erwin Chemerinsky is an American $constitutional legal scholar.</p>

	<p>He wrote: "${'No Democracy Lasts Forever: How the Constitution Threatens the United States'}". $r1</p>
	HTML;



$div_youtube_Erwin_Chemerinsky_No_Democracy_Lasts_Forever = new YoutubeContentSection();
$div_youtube_Erwin_Chemerinsky_No_Democracy_Lasts_Forever->setTitleText("Erwin Chemerinsky | No Democracy Lasts Forever");
$div_youtube_Erwin_Chemerinsky_No_Democracy_Lasts_Forever->setTitleLink("https://www.youtube.com/watch?v=4DWxA2Fxl4E");
$div_youtube_Erwin_Chemerinsky_No_Democracy_Lasts_Forever->content = <<<HTML
	<p>Has the U.S. Constitution become a threat to American democracy?
	Does it need to be dramatically changed or replaced if secession is to be avoided?
	Join us in-person or online as Erwin Chemerinsky returns to Commonwealth Club World Affairs to share his deeply troubled thoughts of the Constitution’s inherent flaws.
	The dean of the UC Berkeley law school came to the sobering conclusion that our nearly 250-year-old founding document is responsible for the crisis now facing American democracy.
	Chemerinsky points out that just 15 of the 11,848 amendments proposed since 1789 have passed,
	and he contends that the very nature of our polarization results from the Constitution’s “bad bones,”
	which have created a government that no longer works or has the confidence of the public.
	Yet he says political Armageddon can still be avoided if a new constitutional convention is empowered to replace the Constitution of 1787,
	much as the Founding Fathers replaced the outdated Articles of Confederation.
	And if that’s not possible? He has an even more radical proposal:
	That Americans must give serious thought to forms of secession―including a United States structured like the European Union―based on a recognition
	that what divides us as a country is, in fact, greater than what unites us.</p>
	HTML;



$div_wikipedia_Erwin_Chemerinsky = new WikipediaContentSection();
$div_wikipedia_Erwin_Chemerinsky->setTitleText("Erwin Chemerinsky");
$div_wikipedia_Erwin_Chemerinsky->setTitleLink("https://en.wikipedia.org/wiki/Erwin_Chemerinsky");
$div_wikipedia_Erwin_Chemerinsky->content = <<<HTML
	<p>Erwin Chemerinsky (born May 14, 1953) is an American legal scholar known for his studies of constitutional law and federal civil procedure.
	Since 2017, Chemerinsky has been the dean of the UC Berkeley School of Law.
	Previously, he was the inaugural dean of the University of California, Irvine School of Law from 2008 to 2017.</p>

	<p>Chemerinsky was named a fellow of the American Academy of Arts and Sciences in 2016.
	The National Jurist magazine named him the most influential person in legal education in the United States in 2017.
	In 2021 Chemerinsky was named President-elect of the Association of American Law Schools.</p>
	HTML;


$page->parent('list_of_people.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_youtube_Erwin_Chemerinsky_No_Democracy_Lasts_Forever);
$page->body($div_wikipedia_Erwin_Chemerinsky);
