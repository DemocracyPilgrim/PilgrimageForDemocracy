<?php
$page = new Page();
$page->h1("Profitable Factories Shutting Down");
$page->viewport_background("");
$page->keywords("Profitable Factories Shutting Down");
$page->stars(0);
$page->tags("Living: Economy", "Economy", "Fair Share");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;


$page->parent('fair_share.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Profitable Factories Shutting Down");
