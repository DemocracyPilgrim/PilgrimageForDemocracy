<?php
$page = new Page();
$page->h1('3: Third level of democracy — We, the Professionals');
$page->stars(4);
$page->keywords('third level of democracy', 'third level', 'Third Level of Democracy');
$page->viewport_background('/free/third_level_of_democracy.png');

$page->preview( <<<HTML
	<p>Beyond theory, democracy demands effective action.
	This level examines the vital institutions, laws, and expertise that translate ideals into a functioning and fair society.</p>
	HTML );


$page->snp('description', "Making living together work.");
$page->snp('image', "/free/third_level_of_democracy.1200-630.png");


$r1 = $page->ref('http://news.bbc.co.uk/2/hi/8239048.stm', 'BBC: Could the UK drive on the right?');
$r2 = $page->ref('https://en.wikipedia.org/wiki/London_Bridge#Old_London_Bridge_(1209%E2%80%931831)', 'Old London Bridge (1209–1831)');
$r3 = $page->ref('https://en.wikipedia.org/wiki/Architecture_of_Turkey#Earthquakes', 'See for example the 2023 Turkey earthquake, where the very high casualty count was directly linked to shoddy building practices.');


$div_qualities = new ContentSection();
$div_qualities->content = <<<HTML
	<p>The flaws of the third level of democracy are:
		<span class="democracy flaw">incompetence</span>,
		<span class="democracy flaw">corruption</span>,
		<span class="democracy flaw">cronyism</span>.</p>
	<p>The virtues of the third level of democracy are:
		<span class="democracy virtue">competence</span>,
		<span class="democracy virtue">professionalism</span>,
		<span class="democracy virtue">integrity</span>,
		<span class="democracy virtue">public service</span>,
		<span class="democracy virtue">self regulation</span>.</p>
	<p>
	HTML;


$h2_Making_it_work = new h2HeaderContent('Making it work');


$div_What_side_to_drive_in_Britain = new ContentSection();
$div_What_side_to_drive_in_Britain->content = <<<HTML
	<h3>Which side of the road should British people drive?</h3>

	<p>It is well known that in Great Britain, people drive their cars on the left-hand side of the road,
	while the whole of continental Europe drives on the right-hand side.
	What is less known, is that the UK almost changed the side they drove.</p>

	<p>Here is the story.</p>

	<p>Back in 2012, a mere four years before the fateful 2016 European Union referendum in the United Kingdom
	that eventually led to Brexit, Britain's exit from the European Union,
	the British authorities were still considering changing the side motor vehicles drive
	so that driving in the UK conforms to the driving customs in mainland Europe.</p>

	<p>Consider this newspaper article published that year:</p>

	<blockquote>
		<p><strong>Whiteball, London, Sunday March 32nd, 2012.</strong><br>
		The Rt Hon. Justin Redding MP, Secretary of State for Transport,
		has announced that
		in view of conforming with driving practice all over continental Europe,
		Her Majesty's Government has decided to let the UK switch the side of the road Her Royal subjects drive on.
		After consultation with European partners,
		and following high level meetings with their French counterparts,
		Ministry officials confirmed that preparations are already underway so that by July 2016,
		all vehicles in Great Britain would be driving on the right-hand side of the road.</p>

		<p>The government is expected to draw from the experience of countries who have made the switch in the past,
		like Sweden in 1967 and more recently like Samoa,
		as well as the dozen other countries which made the switch to right-hand traffic in the 1970s,
		like Sudan in 1973 and East Timor in 1976.</p>

		<p>The experience and feedback will be invaluable in ensuring that the switch in Britain happens smoothly,
		especially in a country prone to Elizabethan road rage and brimming with fish & chips.</p>

		<p>Her Majesty's Government wants to reassure Her loyal subjects that
		in order to ensure their safety, the switch will proceed in a progressive and orderly fashion.</p>

		<p>In order to prevent road accidents and complications due to a confused public or, worse, to elderly Sunday drivers,
		it was estimated that the safest way to go forward was to let professional drivers switch sides first.
		Indeed, people who make a living by driving vehicles can easily be contacted
		to receive a full briefing and appropriate training.</p>

		<p>The proposed timetable for the progressive switch would be as follows:</p>

		<ul>
			<li><strong>1st January 2016</strong>: only lorries are to switch sides.
			From that day on, they shall start driving on the right-hand side of the road,
			while all other vehicles continue driving on the left.</li>

			<li><strong>2nd April 2016</strong>: all other professional vehicles (taxis, ambulances, police cars, etc.) will follow suit and switch sides.
			(note: the original study had proposed the date of April 1st, but the Government decided to postpone this stage to the next day,
			for fear that <em>fools</em> would mistakenly believe that it is only a joke!)</li>

			<li><strong>1st July 2016</strong>: if everything proceeds smoothly in the previous stages, all private vehicles will finally switch sides too,
			thereby completing an operation which everybody anticipates will be successful.</li>
		</ul>

		<p>It is planned that <strong>on the 23rd June 2016</strong>, a final cabinet-level meeting would be held
		to make the final go/no-go decision on <strong>perfecting the UK's integration within the European Union</strong>,
		by finally allowing private vehicles to drive on the right side of the road, rather than on the wrong side.</p>
	</blockquote>

	<p>This story is not as fabricated as it may seem...
	Although the Department for Transport says it has no plans to change the driving side,
	it did in fact examine such a plan in the late 1960s,
	two years after Sweden successfully switched to driving on the right.
	But after seriously considering the matter, the government decided against it,
	mostly because of the prohibitive cost, and the practical impossibility of upgrading all the infrastructure
	specifically build for left-side driving. $r1</p>

	<p>One of the very first attempts to regulate traffic and enforce a driving side
	was made on the Old London Bridge where, starting 1670,
	attempts were made to keep traffic in each direction to one side,
	at first through a keep-right policy and from 1722, through a keep-left policy. $r2</p>

	<p>The fictitious story above illustrates important points.
	With regard to traffic, there is not a right side and a wrong side,
	but, right or left, it does make sense that everybody agrees to drive on the same side.
	This idea can be generalized to almost any aspect to civilized, communal life.
	A society functions much more smoothly if people agree to adhere to sensible common rules.
	How intrusive can those rules be, and what areas of public and private life can they be applied to
	are the constant subject of respectful democratic debates.</p>

	<p>Although in a democratic society people have individual freedoms
	(see <a href="/first_level_of_democracy.html")>First level of democracy — We, the Individuals</a>),
	we do live in society, together with millions of other people
	(see <a href="/second_level_of_democracy.html">Second level of democracy — We, the Society</a>),
	and <strong>some common rules</strong> for living and operating in this society are necessary.
	Not only that, regardless of our position in society, we each have a role to play in it,
	as participants and professionals.</p>
	HTML;





$h2_institutions = new h2HeaderContent('Institutions');

$div_institutions = new ContentSection();
$div_institutions->content = <<<HTML
	<p>Human being being inherently flawed, a functioning $democracy requires strong $institutions to ensure order and fairness.</p>

	<p>Ideally, a society where individuals consistently act with the common good in mind would need fewer institutions to govern itself.
	However, the reality is that self-interest and $criminal behaviour necessitate robust democratic institutions to maintain peace and order.
	These institutions serve as a framework for resolving conflict, protecting individual $rights, and promoting social progress.
	The required strength and intrusiveness of these institutions
	is inversely proportional to the level of civic responsibility and ethical conduct of all the individuals within a society.</p>

	<p>In other words, the more all individuals behave with the common good at heart, the less institutions would be needed.
	After all, who would need a police force in a world without abusers, criminals or lawbreakers?</p>
	HTML;


$div_laws_and_legislature = new ContentSection();
$div_laws_and_legislature->content = <<<HTML
	<h3>The Rules We Live By: Laws, Legislatures, and the Social Contract</h3>


	<p>A society cannot function effectively if each individual operates by their own set of rules.
	Common sense and the pursuit of the collective good necessitate a shared framework of rules and regulations.
	Just as we all agree to drive on the same side of the road, countless aspects of daily life require standardized procedures and shared expectations.
	Certain activities, particularly those with potential for harm, destruction, or pollution, require careful regulation.
	This necessitates the creation and enforcement of $laws, a task that falls to a legislative body.
	A legislature, or parliament, must be established through democratic processes – a topic we will explore in more detail later –
	and given the responsibility of $lawmaking,
	ensuring that the needs and aspirations of the citizenry are reflected in the legal framework that governs society.</p>
	HTML;


$div_Constitution = new ContentSection();
$div_Constitution->content = <<<HTML
	<h3>The Constitution: A Foundation for Governance and Rights</h3>

	<p>A $nation's $constitution serves as the bedrock upon which its governance and the rights of its citizens are built.
	It outlines the fundamental principles and structures that govern the nation, defining the relationship between the state and its people.
	The effectiveness of a constitution hinges on its ability to strike a delicate balance between stability and adaptability.</p>

	<p>A good constitution safeguards the fundamental $rights discussed in the ${'Second Level of Democracy'},
	ensuring that certain individual $freedoms are inviolable and protected from arbitrary infringement.
	These unalienable rights are enshrined in the constitution to ensure their long-term preservation,
	acting as a bulwark against tyranny and injustice.</p>

	<p>While the constitution must be amendable to reflect societal changes and incorporate new insights, this process should not be overly easy.
	A constitution that is too easily amended risks being manipulated by those seeking to undermine its core principles,
	particularly by populist leaders or in illiberal democracies.
	History is replete with examples of such leaders who have exploited constitutional amendments to consolidate power,
	extend their terms, and erode democratic norms, often under the guise of popular support.
	This underscores the importance of robust safeguards and procedures for constitutional amendment,
	ensuring that changes reflect genuine societal needs and not the whims of power-hungry individuals.</p>
	HTML;

$div_Policing = new ContentSection();
$div_Policing->content = <<<HTML
	<h3>Policing: A Framework for Law Enforcement and Protection</h3>

	<p>Ideally, a society where everyone abided by the law and acted with the common good in mind would render a police force unnecessary.
	However, the reality is that human behaviour often deviates from this ideal,
	requiring a system of law enforcement to maintain order and protect citizens.</p>

	<p>A healthy $democracy must navigate a careful balance in its approach to policing.
	Excessive police brutality, fuelled by a culture of impunity and a disregard for ${'human rights'}, is unacceptable and erodes public trust.
	Conversely, calls to defund the police, while driven by concerns about police misconduct,
	often fail to address the fundamental need for law enforcement and public safety.</p>

	<p>The challenge lies in finding a model of policing that effectively enforces the law, protects citizens, and upholds the principles of justice and fairness.
	This requires a focus on community policing, where officers build relationships with the communities they serve, fostering trust and understanding.
	It also necessitates investment in preventative measures, such as social programs, $education, and mental health services,
	that address the root causes of $crime and reduce the need for police intervention in the first place.</p>

	<p>Ultimately, the goal is to create a system of policing that is both effective and accountable,
	one that balances the need for security with the protection of individual rights and the pursuit of a just and equitable society.</p>
	HTML;



$div_Justice_and_the_judiciary = new ContentSection();
$div_Justice_and_the_judiciary->content = <<<HTML
	<h3>The Judiciary: Upholding the Rule of Law with Integrity</h3>


	<p>In any society, human imperfections inevitably lead to instances of $crime and legal transgression.
	This necessitates a robust system of $justice, where laws are enforced, and wrongdoings are addressed.
	The judiciary, tasked with upholding the law and ensuring fairness, plays a vital role in this process.</p>

	<p>However, a truly just system requires a delicate balance. The judiciary must be independent,
	free from undue influence or pressure from political or other external forces.
	Judges must possess integrity, upholding the highest ethical standards and dispensing justice impartially.
	This independence and integrity are essential to ensure that the judicial system remains a bulwark against corruption and bias,
	safeguarding the rights of all citizens and upholding the rule of law.</p>
	HTML;


$div_Separation_of_powers = new ContentSection();
$div_Separation_of_powers->content = <<<HTML
	<h3>Separation of Powers: a Delicate Balance</h3>

	<p>A cornerstone of democratic governance is the principle of ${'separation of powers'},
	dividing governmental authority among distinct branches to prevent any one entity from becoming too powerful.
	This system, often embodied in a structure of ${'three branches'}, aims to ensure accountability and a balance of influence.
	While this model is widely adopted, some nations, such as $Taiwan, have implemented unique systems with additional $branches
	designed to enhance oversight and accountability, showcasing the ongoing evolution of democratic institutions around the world.</p>
	HTML;




$h2_professionals = new h2HeaderContent('We, the Professionals');

$div_real_expertise = new ContentSection();
$div_real_expertise->content = <<<HTML
	<h3>The Rise of Expertise in a Digital Age</h3>

	<p>The internet has ushered in a new era, democratizing access to information and empowering individuals with knowledge once reserved for a select few.
	This decentralization of power and knowledge has fostered a surge in citizen participation,
	challenging traditional hierarchies and promoting greater transparency.</p>

	<p>However, this trend, when taken to extremes, can have unintended consequences.
	While readily available information empowers individuals, it is crucial to distinguish between genuine expertise and superficial knowledge.
	Just as one would seek a qualified surgeon for open heart surgery
	&mdash; One wouldn't trust one's life to a "doctor" who watched a youtube video on how to conduct open heart surgery! &mdash;
	so too must we approach complex issues with discernment.
	Trusting the advice of self-proclaimed "experts" who lack the necessary training and experience can lead to dangerous outcomes.</p>

	<p>Navigating a world of readily accessible information requires a critical eye,
	a commitment to seeking out credible sources,
	and a recognition that true expertise is built upon rigorous study, experience, and evidence-based practice.</p>
	HTML;

$div_Our_roles_in_society = new ContentSection();
$div_Our_roles_in_society->content = <<<HTML
	<h3>The Fabric of Society: Our Roles as Citizens and Professionals</h3>

	<p>Democratic institutions are brought to life by the dedicated individuals who serve within them, whether as public servants or professionals.
	But a thriving society extends far beyond the realm of government.
	Every citizen, from employees and business owners to those working in various sectors of the economy,
	plays a crucial role in shaping the fabric of their community.</p>

	<p>As professionals, we have the power to influence the world around us,
	contributing to the well-being of our communities and the advancement of society.
	Ideally, each individual finds their unique purpose and their own constructive place within the greater whole.
	Feeling valued and contributing to a shared endeavour creates a sense of belonging and purpose,
	fostering a sense of shared responsibility for the betterment of society.</p>
	HTML;

$div_Constructive_trades_and_destructive_trades = new ContentSection();
$div_Constructive_trades_and_destructive_trades->content = <<<HTML
	<h3>The Spectrum of Trade: From Positive Contribution to Societal Harm</h3>

	<p>Even within a thriving economy, not all industries or professions hold equal value to society.
	Some contribute positively, enriching lives and promoting well-being, while others may have a detrimental impact,
	generating profits at the expense of public health or environmental sustainability.</p>

	<p>For instance, the tobacco industry exemplifies a sector that prioritizes profit over societal well-being.
	Its primary aim is to generate revenue through the addictive nature of its products,
	often with severe consequences for public health.
	This raises a crucial question:
	how can such industries be regulated, perhaps through $taxation, to mitigate their negative impact?</p>

	<p>In contrast, industries that empower people, foster creativity, or provide essential services contribute demonstrably to societal well-being.
	These sectors strive to enhance individual potential, create opportunities, and contribute to a more equitable and prosperous society.</p>

	<p>The distinction between "constructive" and "destructive" trades often involves a degree of subjective judgement.
	However, assuming an industry is legal and not overtly harmful enough to warrant outright bans, a nuanced approach to regulation and $taxation becomes essential.
	Countries around the world have implemented various strategies to address the potential harms of specific industries,
	from tobacco taxes to carbon emissions regulations.
	This ongoing dialogue underscores the need for a comprehensive and ethical framework
	for evaluating the impact of different industries on society and for implementing policies that promote the common good.</p>
	HTML;


$div_trust_accountability_professionalism = new ContentSection();
$div_trust_accountability_professionalism->content = <<<HTML
	<h3>Trust and Accountability: the Need for Professionalism</h3>

	<p>In our daily lives, we place immense trust in professionals, relying on their expertise and ethical conduct
	to ensure the safety, well-being, and proper functioning of society.
	From doctors providing healthcare to architects designing buildings, we expect a high degree of competence and integrity.</p>

	<p>However, this trust is not always justified.
	While some professionals excel in their fields, others exhibit incompetence, negligence, or even engage in criminal activities.
	The tragic consequences of these failures can range from compromised healthcare to catastrophic building collapses$r3,
	highlighting the critical need for oversight and regulation.</p>

	<p>History is littered with examples of professional misconduct,
	such as the widespread recommendation of smoking in the 1960s, a practice now widely recognized as harmful.
	Such instances underscore the importance of continuous vigilance, rigorous training standards,
	and robust mechanisms to hold professionals accountable for their actions.
	We must strive for a system that prioritizes public safety, ethical conduct,
	and the ongoing pursuit of excellence within every profession.</p>
	HTML;

$div_Taxes = new ContentSection();
$div_Taxes->content = <<<HTML
	<h3>The Bridge Between Public and Private: The Role of Taxation</h3>

	<p>A fundamental aspect of third level of democracy is the intricate relationship between its public institutions and its private economy.
	These two domains, while distinct, are inextricably intertwined, with professional individuals working within both.
	At the heart of this relationship lies $taxation, a critical mechanism that bridges the public and private sectors of the economy.</p>

	<p>To understand the complexities of taxation,
	a clear distinction must be made between what constitutes the public domain and what belongs to the private realm.
	This distinction is essential for establishing a fair and equitable system
	where public resources are used effectively to meet collective needs while respecting individual rights and property.
	A thorough investigation into the principles and practices of taxation is necessary
	to ensure that it serves as a tool for achieving social justice, promoting economic stability, and fostering a thriving society.</p>
	HTML;


$div_All_labor_has_dignity = new ContentSection();
$div_All_labor_has_dignity->content = <<<HTML
	<h3>All Labor has Dignity</h3>

	<p>The success of a nation relies not solely on the contributions of doctors, professors, lawyers, and other highly esteemed professionals.
	It is equally dependent on the tireless work of garbage collectors, street cleaners, factory workers,
	and all those who contribute to the essential functions of society.
	To truly function as a just and thriving community, we must acknowledge the equal worth and dignity of all labour,
	regardless of its perceived status or position on the social ladder.
	Every individual, from the most celebrated scientist to the most humble labourer,
	plays a vital role in the complex tapestry of a functioning society.</p>

	<blockquote>
	You are doing many things here in this struggle.
	You are demanding that this city will respect the dignity of labor.
	So often we overlook the work and the significance of those who are not in professional jobs, of those who are not in the so-called big jobs.
	But let me say to you tonight, that whenever you are engaged in work that serves humanity and is for the building of humanity,
	it has dignity, and it has worth.
	One day our society must come to see this.
	One day our society will come to respect the sanitation worker if it is to survive,
	for the person who picks up our garbage, in the final analysis,
	is as significant as the physician, for if he doesn't do his job, diseases are rampant.
	All labor has dignity.
	<br>
	<br>
	-- Dr. Martin Luther King, Jr., March 18, 1968, Memphis, TN.
	</blockquote>
	HTML;


$div_the_Non_Profit_sector = new ContentSection();
$div_the_Non_Profit_sector->content = <<<HTML
	<h3>The Vital Role of the Non-Profit Sector</h3>

	<p>No examination of democratic institutions would be complete without acknowledging the crucial role played by the non-profit sector.
	Organizations like NGOs, charities, and think tanks often fill essential gaps in society,
	providing vital services to communities overlooked or underserved by both government and private businesses.</p>

	<p>These organizations frequently focus on addressing pressing social needs, advocating for marginalized groups,
	promoting research and innovation, and fostering a more equitable and just society.
	Their tireless efforts often serve as a catalyst for positive change, bringing attention to critical issues and inspiring collective action.
	The non-profit sector stands as a testament to the power of individual initiative, collective purpose,
	and the unwavering pursuit of a more just and compassionate world.</p>
	HTML;

$div_Education = new ContentSection();
$div_Education->content = <<<HTML
	<h3>Investing in the Future: Education and Professional Training</h3>

	<p>A vibrant democracy thrives on an educated and capable citizenry.
	This necessitates a robust system of education that equips individuals
	with the knowledge, skills, and critical thinking abilities necessary for active participation in a democratic society.
	Beyond formal education, ongoing professional training and development are essential
	for ensuring that individuals maintain relevant skills, adapt to evolving demands, and contribute to a dynamic and innovative workforce.
	Investing in education and training not only empowers individuals
	but also strengthens the fabric of society, fostering a more engaged and productive citizenry.</p>
	HTML;



$list_all_related_topics = new ListOfPages();
$list_all_related_topics->add('institutions.html');
$print_list_all_related_topics = $list_all_related_topics->print();

$div_list_all_related_topics = new ContentSection();
$div_list_all_related_topics->content = <<<HTML
	$print_list_all_related_topics
	HTML;



$page->parent('democracy.html');
$page->previous('second_level_of_democracy.html');
$page->next('fourth_level_of_democracy.html');

$page->body($div_qualities);

$page->body($h2_Making_it_work);
$page->body($div_What_side_to_drive_in_Britain);


$page->body($h2_institutions);
$page->body($div_institutions);
$page->body($div_Constitution);
$page->body($div_laws_and_legislature);
$page->body($div_Justice_and_the_judiciary);
$page->body($div_Policing);
$page->body($div_Separation_of_powers);

$page->body($h2_professionals);
$page->body($div_Our_roles_in_society);
$page->body($div_All_labor_has_dignity);
$page->body($div_Constructive_trades_and_destructive_trades);
$page->body($div_Taxes);
$page->body($div_trust_accountability_professionalism);
$page->body($div_Education);
$page->body($div_real_expertise);
$page->body($div_the_Non_Profit_sector);

$page->body($div_list_all_related_topics);
