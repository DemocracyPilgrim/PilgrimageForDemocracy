<?php
$page = new Page();
$page->h1("Political polarisation");
$page->tags("Elections", "Party Politics");
$page->keywords("political polarisation");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Political_polarization = new WikipediaContentSection();
$div_wikipedia_Political_polarization->setTitleText("Political polarization");
$div_wikipedia_Political_polarization->setTitleLink("https://en.wikipedia.org/wiki/Political_polarization");
$div_wikipedia_Political_polarization->content = <<<HTML
	<p>Political polarization is the divergence of political attitudes away from the center, towards ideological extremes.
	Scholars distinguish between ideological polarization (differences between the policy positions)
	and affective polarization (an emotional dislike and distrust of political out-groups).</p>
	HTML;

$div_wikipedia_Political_polarization_in_the_United_States = new WikipediaContentSection();
$div_wikipedia_Political_polarization_in_the_United_States->setTitleText("Political polarization in the United States");
$div_wikipedia_Political_polarization_in_the_United_States->setTitleLink("https://en.wikipedia.org/wiki/Political_polarization_in_the_United_States");
$div_wikipedia_Political_polarization_in_the_United_States->content = <<<HTML
	<p>Political polarization is a prominent component of politics in the United States.
	Scholars distinguish between ideological polarization (differences between the policy positions)
	and affective polarization (a dislike and distrust of political out-groups), both of which are apparent in the United States.
	In the last few decades, the U.S. has experienced a greater surge in ideological polarization and affective polarization than comparable democracies.</p>
	HTML;


$page->parent('duverger_syndrome_party_politics.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_wikipedia_Political_polarization);
$page->body($div_wikipedia_Political_polarization_in_the_United_States);
