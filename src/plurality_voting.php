<?php
$page = new Page();
$page->h1("Plurality voting");
$page->tags("Elections");
$page->keywords("Plurality voting", "plurality voting", "Plurality Voting");
$page->stars(0);

//$page->snp("description", "");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p>Plurality voting is a very common voting method, but one with many severe flaws.</p>
	HTML );

// $r1 = $page->ref('', '');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Plurality voting is a very common ${'voting method'}, but one with many severe flaws.</p>

	<p>There are two main aspects to plurality voting:</p>

	<ul>
		<li><strong>Voting method</strong>: given the choice of a multitude of candidates or parties (sometimes only two, sometimes many more),
		the voter is only allowed to <strong>select (or vote for) one single candidate</strong> or party.</li>
		<li><strong>Tallying method</strong>: tallying the ballots is a simple matter of counting how many votes each candidate or party got.
		The one who got more votes than all others (i.e. the one who got a <em>plurality</em> of the votes) is elected.</li>
	</ul>
	HTML;


$div_Only_one = new ContentSection();
$div_Only_one->content = <<<HTML
	<h3>Only one!</h3>

	<p>The traditional definition of plurality voting insists on the tallying aspect of this ${'voting method'},
	and on the fact that the candidate who got a plurality of the votes is elected.
	Tallying the votes is indeed extremely simple and straightforward.</p>

	<p>The most critical aspect, however, and the one with the most nefarious consequences, is the actual ballot,
	and how the voters are instructed to use it to express their opinions.
	Most critically, among a multitude of candidates,
	<strong>the voters are only allowed to select one</strong> and one only.
	One major problem is that voters are not given the opportunity to express their opinions on each and every candidates.
	Voters may very well like several of the competing candidates.
	They may even be forced to abandon their favourite candidate, and they often do, in order to vote for a much less liked candidates,
	but who has more chances to get elected, against another much despised candidate.
	This is the lesser-of-two-evils effect.</p>
	HTML;



$div_Consequences = new ContentSection();
$div_Consequences->content = <<<HTML
	<h3>Consequences</h3>

	<p>As explained in ${"Duverger's Law"}, the effect of plurality voting is to split the electorate into two broad camps.
	It results in negative campaigning, more extreme candidates and a much deteriorated political climate,
	which is all covered in the ${"Duverger Syndrome"}.</p>
	HTML;



$div_wikipedia_Plurality_voting = new WikipediaContentSection();
$div_wikipedia_Plurality_voting->setTitleText("Plurality voting");
$div_wikipedia_Plurality_voting->setTitleLink("https://en.wikipedia.org/wiki/Plurality_voting");
$div_wikipedia_Plurality_voting->content = <<<HTML
	<p>Plurality voting refers to electoral systems in which a candidate in an electoral district
	who polls more than any other (that is, receives a plurality) is elected.</p>
	HTML;


$page->parent('voting_methods.html');
$page->template("stub");
$page->body($div_introduction);

$page->body($div_Only_one);

$page->body($div_Consequences);
$page->body('duverger_syndrome.html');

$page->body($div_wikipedia_Plurality_voting);
