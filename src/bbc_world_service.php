<?php
$page = new OrganisationPage();
$page->h1("BBC World Service");
$page->viewport_background("");
$page->keywords("BBC World Service");
$page->stars(0);
$page->tags("Organisation", "Information: Media", "Broadcaster", "BBC", "UK");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;



$div_World_Service_Listen_Live_BBC_SoundsWorld_Service_Listen_Live_BBC = new WebsiteContentSection();
$div_World_Service_Listen_Live_BBC_SoundsWorld_Service_Listen_Live_BBC->setTitleText("World Service - Listen Live - BBC Sounds");
$div_World_Service_Listen_Live_BBC_SoundsWorld_Service_Listen_Live_BBC->setTitleLink("https://www.bbc.co.uk/sounds/play/live:bbc_world_service");
$div_World_Service_Listen_Live_BBC_SoundsWorld_Service_Listen_Live_BBC->content = <<<HTML
	<p>
	</p>
	HTML;



$div_youtube_BBC_World_Service = new YoutubeContentSection();
$div_youtube_BBC_World_Service->setTitleText("BBC World Service");
$div_youtube_BBC_World_Service->setTitleLink("https://www.youtube.com/@BBCWorldService/featured");
$div_youtube_BBC_World_Service->content = <<<HTML
	<p>
	</p>
	HTML;




$div_wikipedia_BBC_World_Service = new WikipediaContentSection();
$div_wikipedia_BBC_World_Service->setTitleText("BBC World Service");
$div_wikipedia_BBC_World_Service->setTitleLink("https://en.wikipedia.org/wiki/BBC_World_Service");
$div_wikipedia_BBC_World_Service->content = <<<HTML
	<p>The BBC World Service is an international broadcaster owned and operated by the BBC. It is the world's largest external broadcaster in terms of reception area, language selection and audience reach. It broadcasts radio news, speech and discussions in more than 40 languages to many parts of the world on analogue and digital shortwave platforms, internet streaming, podcasting, satellite, DAB, FM, LW and MW relays. In 2024, the World Service reached an average of 450 million people a week (via TV, radio and online). In November 2016, the BBC announced that it would start broadcasting in additional languages including Amharic and Igbo, in its biggest expansion since the 1940s.</p>
	HTML;


$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("BBC World Service");
$page->body($div_World_Service_Listen_Live_BBC_SoundsWorld_Service_Listen_Live_BBC);
$page->body($div_youtube_BBC_World_Service);
$page->body($div_wikipedia_BBC_World_Service);
