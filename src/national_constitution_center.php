<?php
$page = new Page();
$page->h1("National Constitution Center");
$page->tags("Organisation", "USA", "Constitution", "Institutions: Legislative", "Liberty Medal");
$page->keywords("National Constitution Center");
$page->stars(0);
$page->viewport_background("/free/national_constitution_center.png");

$page->snp("description", "Studying and teaching the Constitution of the United States.");
$page->snp("image",       "/free/national_constitution_center.1200-630.png");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;



$div_National_Constitution_Center = new WebsiteContentSection();
$div_National_Constitution_Center->setTitleText("National Constitution Center ");
$div_National_Constitution_Center->setTitleLink("https://constitutioncenter.org/");
$div_National_Constitution_Center->content = <<<HTML
	<p>The National Constitution Center brings together people of all ages and perspectives, across America and around the world,
	to learn about, debate, and celebrate the greatest vision of human freedom in history, the U.S. Constitution.</p>
	HTML;



$div_wikipedia_National_Constitution_Center = new WikipediaContentSection();
$div_wikipedia_National_Constitution_Center->setTitleText("National Constitution Center");
$div_wikipedia_National_Constitution_Center->setTitleLink("https://en.wikipedia.org/wiki/National_Constitution_Center");
$div_wikipedia_National_Constitution_Center->content = <<<HTML
	<p>The National Constitution Center is a non-profit institution that is devoted to the study of the Constitution of the United States.
	Located at the Independence Mall in Philadelphia, Pennsylvania, the center is an interactive museum which serves as a national town hall,
	hosting government leaders, journalists, scholars, and celebrities who engage in public discussions, including Constitution-related events and presidential debates.</p>
	HTML;


$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);
$page->related_tag("National Constitution Center");

$page->body($div_National_Constitution_Center);

$page->body($div_wikipedia_National_Constitution_Center);
