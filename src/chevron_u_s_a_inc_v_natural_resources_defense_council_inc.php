<?php
$page = new Page();
$page->h1("Chevron U.S.A., Inc. v. Natural Resources Defense Council, Inc. (1984)");
$page->tags("Institutions: Judiciary", "SCOTUS Landmark Decision", "USA", "Institutions: Executive", "Pollution");
$page->keywords("Chevron U.S.A., Inc. v. Natural Resources Defense Council, Inc.", "Chevron Doctrine", "Chevron Deference");
$page->stars(2);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>"Chevron U.S.A., Inc. v. Natural Resources Defense Council, Inc." is a 1984 landmark decision by the ${'Supreme Court of the United States'}.</p>

	<p>The case revolved around the Clean Air Act and the Environmental Protection Agency (EPA).
	The EPA had interpreted the Act's language to allow for "bubble" regulations,
	where a company could offset pollution from one source by reducing it at another source within the same facility.
	The Natural Resources Defense Council (NRDC) challenged the EPA's interpretation, arguing it was inconsistent with the plain meaning of the statute.</p>

	<p>The Supreme Court established the "Chevron deference" doctrine,
	holding that courts should defer to an agency's interpretation of an ambiguous statute if:</p>
	<ol>
	<li>Congress has not directly spoken to the issue at hand: Meaning the statute is unclear or silent on the specific issue.</li>
	<li>The agency's interpretation is reasonable:
	Even if the agency's interpretation is not the only possible reading, it must be reasonable in light of the statute's language and legislative intent.</li>
	</ol>

	<p>The Court upheld the EPA's interpretation of the Clean Air Act, deferring to the agency's expertise in environmental regulation.</p>

	<p>The decision was overruled 40 years later by ${'Loper Bright Enterprises v. Raimondo'},
	on the ground that it conflicts with the Administrative Procedure Act.</p>
	HTML;


$div_Impact = new ContentSection();
$div_Impact->content = <<<HTML
	<h3>Impact</h3>

	<p>Chevron established a principle of deference to agency expertise in interpreting ambiguous statutes within their area of responsibility.
	It acknowledges that agencies possess specialized knowledge and experience that courts may lack.</p>

	<p>The doctrine promotes administrative efficiency by preventing constant litigation over agency interpretations.
	It encourages agencies to develop and implement regulations without unnecessary legal challenges.</p>

	<p>Chevron emphasizes the importance of congressional intent.
	If Congress is clear in its language, the agency cannot deviate from that intent.
	However, when a statute is ambiguous, the agency has more latitude in interpreting the law.</p>

	<p>Chevron had significant implications for agency policymaking.
	It empowered agencies to fill in gaps in legislation and develop regulations, even in areas where Congress has been less specific.</p>

	<p>The decision has generated a lot of debates:</p>


	<p>Critics argue that Chevron undermines Congress's role in lawmaking by giving agencies too much power to interpret statutes.
	Some argue that Chevron limits judicial review, as courts are less likely to question an agency's interpretation.
	Concerns exist that the doctrine might encourage agencies to interpret statutes in ways that benefit their own interests or policies rather than the public interest.</p>


	<p>Chevron deference was a significant legal doctrine, influencing judicial review of agency actions across various areas,
	from environmental regulations to healthcare to immigration.
	While it has been both praised and criticized, its impact on the balance of power between the executive and judicial branches continues to be debated.</p>
	HTML;







$div_wikipedia_Chevron_U_S_A_Inc_v_Natural_Resources_Defense_Council_Inc = new WikipediaContentSection();
$div_wikipedia_Chevron_U_S_A_Inc_v_Natural_Resources_Defense_Council_Inc->setTitleText("Chevron U S A Inc v Natural Resources Defense Council Inc");
$div_wikipedia_Chevron_U_S_A_Inc_v_Natural_Resources_Defense_Council_Inc->setTitleLink("https://en.wikipedia.org/wiki/Chevron_U.S.A.,_Inc._v._Natural_Resources_Defense_Council,_Inc.");
$div_wikipedia_Chevron_U_S_A_Inc_v_Natural_Resources_Defense_Council_Inc->content = <<<HTML
	<p>Chevron U.S.A., Inc. v. Natural Resources Defense Council, Inc., 467 U.S. 837 (1984),
	was a landmark decision of the United States Supreme Court that set forth the legal test
	used when U.S. federal courts must defer to a government agency's interpretation of a law or statute.
	The decision articulated a doctrine known as "Chevron deference".</p>
	HTML;


$page->parent('supreme_court_of_the_united_states.html');
$page->body($div_introduction);
$page->body($div_Impact);


$page->body($div_wikipedia_Chevron_U_S_A_Inc_v_Natural_Resources_Defense_Council_Inc);
