<?php
$page = new CountryPage("The Democratic Republic of the Congo");
$page->h1("Democratic Republic of the Congo");
$page->viewport_background("/free/democratic_republic_of_the_congo.png");
$page->keywords("Democratic Republic of the Congo");
$page->stars(2);
$page->tags("Country");

$page->snp("description", "111 million inhabitants.");
$page->snp("image",       "/free/democratic_republic_of_the_congo.1200-630.png");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>The Democratic Republic of the Congo (DRC), a vast and resource-rich nation in the heart of Africa, is a country of stark contrasts.
	While its name suggests a commitment to democracy,
	the reality on the ground is a complex and often challenging struggle for genuine political freedom, social justice, and equitable development.</p>

	<p>The DRC's history is marked by colonialism, conflict, and instability, creating a deeply fractured society.
	While the country has held elections, the democratic process often faces significant hurdles.
	Challenges include limited transparency, allegations of electoral fraud, and a history of disputed results.
	The political landscape remains fragile, with concerns about the influence of powerful individuals and groups, and the exclusion of marginalized voices.</p>

	<p>Freedom of expression and assembly, while legally enshrined, are frequently curtailed in practice.
	Journalists, activists, and human rights defenders face intimidation, harassment, and even violence.
	This creates a climate of fear that limits genuine public discourse and the ability of citizens to hold their leaders accountable.</p>

	<p>Social justice also remains elusive. The DRC possesses immense mineral wealth, yet a significant portion of the population lives in poverty.
	Corruption, inequality, and lack of access to basic services like healthcare and education are pervasive issues.
	The exploitation of natural resources, often fueled by conflict and human rights abuses, further undermines efforts to achieve a more just society.
	Armed groups continue to operate in parts of the country, displacing communities and perpetuating cycles of violence and insecurity.</p>

	<p>In conclusion, the Democratic Republic of the Congo is a nation navigating a difficult path towards genuine democracy, freedom, and social justice.
	While progress has been made in certain areas, the country continues to face significant systemic challenges.
	Understanding these complexities is crucial to appreciating the ongoing struggle of the Congolese people for a more equitable and peaceful future.
	The DRC's journey is a stark reminder that the mere existence of democratic institutions does not guarantee a truly democratic, free, and just society.</p>
	HTML;

$div_wikipedia_Democratic_Republic_of_the_Congo = new WikipediaContentSection();
$div_wikipedia_Democratic_Republic_of_the_Congo->setTitleText("Democratic Republic of the Congo");
$div_wikipedia_Democratic_Republic_of_the_Congo->setTitleLink("https://en.wikipedia.org/wiki/Democratic_Republic_of_the_Congo");
$div_wikipedia_Democratic_Republic_of_the_Congo->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Human_rights_in_the_Democratic_Republic_of_the_Congo = new WikipediaContentSection();
$div_wikipedia_Human_rights_in_the_Democratic_Republic_of_the_Congo->setTitleText("Human rights in the Democratic Republic of the Congo");
$div_wikipedia_Human_rights_in_the_Democratic_Republic_of_the_Congo->setTitleLink("https://en.wikipedia.org/wiki/Human_rights_in_the_Democratic_Republic_of_the_Congo");
$div_wikipedia_Human_rights_in_the_Democratic_Republic_of_the_Congo->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Sexual_violence_in_the_Democratic_Republic_of_the_Congo = new WikipediaContentSection();
$div_wikipedia_Sexual_violence_in_the_Democratic_Republic_of_the_Congo->setTitleText("Sexual violence in the Democratic Republic of the Congo");
$div_wikipedia_Sexual_violence_in_the_Democratic_Republic_of_the_Congo->setTitleLink("https://en.wikipedia.org/wiki/Sexual_violence_in_the_Democratic_Republic_of_the_Congo");
$div_wikipedia_Sexual_violence_in_the_Democratic_Republic_of_the_Congo->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Child_soldiers_in_the_Democratic_Republic_of_the_Congo = new WikipediaContentSection();
$div_wikipedia_Child_soldiers_in_the_Democratic_Republic_of_the_Congo->setTitleText("Child soldiers in the Democratic Republic of the Congo");
$div_wikipedia_Child_soldiers_in_the_Democratic_Republic_of_the_Congo->setTitleLink("https://en.wikipedia.org/wiki/Child_soldiers_in_the_Democratic_Republic_of_the_Congo");
$div_wikipedia_Child_soldiers_in_the_Democratic_Republic_of_the_Congo->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_International_Criminal_Court_investigation_in_the_Democratic_Republic_of_the_Congo = new WikipediaContentSection();
$div_wikipedia_International_Criminal_Court_investigation_in_the_Democratic_Republic_of_the_Congo->setTitleText("International Criminal Court investigation in the Democratic Republic of the Congo");
$div_wikipedia_International_Criminal_Court_investigation_in_the_Democratic_Republic_of_the_Congo->setTitleLink("https://en.wikipedia.org/wiki/International_Criminal_Court_investigation_in_the_Democratic_Republic_of_the_Congo");
$div_wikipedia_International_Criminal_Court_investigation_in_the_Democratic_Republic_of_the_Congo->content = <<<HTML
	<p>
	</p>
	HTML;


$page->parent('world.html');
$page->body($div_introduction);



$page->related_tag("Democratic Republic of the Congo");
$page->country_indices();
$page->body($div_wikipedia_Democratic_Republic_of_the_Congo);
$page->body($div_wikipedia_Human_rights_in_the_Democratic_Republic_of_the_Congo);
$page->body($div_wikipedia_Sexual_violence_in_the_Democratic_Republic_of_the_Congo);
$page->body($div_wikipedia_Child_soldiers_in_the_Democratic_Republic_of_the_Congo);
$page->body($div_wikipedia_International_Criminal_Court_investigation_in_the_Democratic_Republic_of_the_Congo);
