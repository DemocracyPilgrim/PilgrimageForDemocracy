<?php
$page = new Page();
$page->h1('RESULTS (organization)');
$page->keywords('RESULTS');
$page->tags("Organisation");
$page->stars(0);

$page->snp('description', 'Finding solutions to poverty and hunger.');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;


$div_RESULTS_website = new WebsiteContentSection();
$div_RESULTS_website->setTitleText('RESULTS website ');
$div_RESULTS_website->setTitleLink('https://results.org/');
$div_RESULTS_website->content = <<<HTML
	<p>RESULTS is a movement of passionate, committed everyday people
	who use their voices to influence political decisions that will bring an end to poverty.</p>
	HTML;


$div_wikipedia_Results_organization = new WikipediaContentSection();
$div_wikipedia_Results_organization->setTitleText('Results organization');
$div_wikipedia_Results_organization->setTitleLink('https://en.wikipedia.org/wiki/Results_(organization)');
$div_wikipedia_Results_organization->content = <<<HTML
	<p>RESULTS is a US non-partisan citizens' advocacy organization founded in 1980.
	The organization aims to find long-term solutions to poverty by focusing on its root causes.
	It lobbies public officials, does research, and works with the media and the public to fight hunger and poverty.
	RESULTS has 100 U.S. local chapters and works in six other countries.</p>
	HTML;


$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_RESULTS_website);
$page->body($div_wikipedia_Results_organization);
