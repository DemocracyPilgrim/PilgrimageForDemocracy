<?php
$page = new Page();
$page->h1("Texas");
$page->viewport_background("");
$page->keywords("Texas");
$page->stars(0);
$page->tags("International", "USA", "US State");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Texas = new WikipediaContentSection();
$div_wikipedia_Texas->setTitleText("Texas");
$div_wikipedia_Texas->setTitleLink("https://en.wikipedia.org/wiki/Texas");
$div_wikipedia_Texas->content = <<<HTML
	<p>Texas is the most populous state in the South Central region of the United States.</p>
	HTML;


$page->parent('united_states.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Texas");
$page->body($div_wikipedia_Texas);
