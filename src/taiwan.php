<?php
$page = new CountryPage('Taiwan');
$page->h1('Taiwan (Republic of China)');
$page->tags("Country");
$page->stars(1);
$page->keywords('Taiwan', 'Republic of China', 'ROC', 'Taiwanese');

$page->preview( <<<HTML
	<p>
	</p>
	HTML );


$page->snp('description', "24 million inhabitants.");
//$page->snp('image', "/copyrighted/");



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Taiwan is one of the top-rated democracies in the world, and the first democracy is Asia.</p>
	HTML;


$list_Taiwan = ListOfPeoplePages::WithTags("Taiwan");
$print_list_Taiwan = $list_Taiwan->print();

$div_list_Taiwan = new ContentSection();
$div_list_Taiwan->content = <<<HTML
	<h3>Related content</h3>

	$print_list_Taiwan
	HTML;




$div_wikipedia_Taiwan = new WikipediaContentSection();
$div_wikipedia_Taiwan->setTitleText('Taiwan');
$div_wikipedia_Taiwan->setTitleLink('https://en.wikipedia.org/wiki/Taiwan');
$div_wikipedia_Taiwan->content = <<<HTML
	<p>Taiwan, officially the Republic of China (ROC), is a country in East Asia.</p>
	HTML;

$div_wikipedia_Politics_of_the_Republic_of_China = new WikipediaContentSection();
$div_wikipedia_Politics_of_the_Republic_of_China->setTitleText('Politics of the Republic of China');
$div_wikipedia_Politics_of_the_Republic_of_China->setTitleLink('https://en.wikipedia.org/wiki/Politics_of_the_Republic_of_China');
$div_wikipedia_Politics_of_the_Republic_of_China->content = <<<HTML
	<p>The Republic of China (ROC), commonly known as Taiwan,
	is governed in a framework of a representative democratic republic under a five-power system first envisioned by Sun Yat-sen in 1906,
	whereby under the constitutional amendments, the President is head of state
	and the Premier (President of the Executive Yuan) is head of government, and of a multi-party system.</p>
	HTML;

$div_wikipedia_Political_status_of_Taiwan = new WikipediaContentSection();
$div_wikipedia_Political_status_of_Taiwan->setTitleText('Political status of Taiwan');
$div_wikipedia_Political_status_of_Taiwan->setTitleLink('https://en.wikipedia.org/wiki/Political_status_of_Taiwan');
$div_wikipedia_Political_status_of_Taiwan->content = <<<HTML
	<p>The controversy surrounding the political status of Taiwan or the Taiwan issue
	is a result of World War II, the second phase of the Chinese Civil War (1945–1949), and the Cold War.</p>
	HTML;

$div_wikipedia_Taiwan_China = new WikipediaContentSection();
$div_wikipedia_Taiwan_China->setTitleText('"Taiwan, China"');
$div_wikipedia_Taiwan_China->setTitleLink('https://en.wikipedia.org/wiki/Taiwan,_China');
$div_wikipedia_Taiwan_China->content = <<<HTML
	<p>"Taiwan, China", "Taiwan, Province of China", and "Taipei, China"
	are controversial political terms that claim Taiwan and its associated territories as a province or territory of "China".</p>
	HTML;

$div_wikipedia_Human_rights_in_Taiwan = new WikipediaContentSection();
$div_wikipedia_Human_rights_in_Taiwan->setTitleText('Human rights in Taiwan');
$div_wikipedia_Human_rights_in_Taiwan->setTitleLink('https://en.wikipedia.org/wiki/Human_rights_in_Taiwan');
$div_wikipedia_Human_rights_in_Taiwan->content = <<<HTML
	<p>The human rights record in Taiwan is generally held to have experienced significant positive transformation since the 1990s.</p>
	HTML;

$div_wikipedia_Capital_punishment_in_Taiwan = new WikipediaContentSection();
$div_wikipedia_Capital_punishment_in_Taiwan->setTitleText('Capital punishment in Taiwan');
$div_wikipedia_Capital_punishment_in_Taiwan->setTitleLink('https://en.wikipedia.org/wiki/Capital_punishment_in_Taiwan');
$div_wikipedia_Capital_punishment_in_Taiwan->content = <<<HTML
	<p>Capital punishment is a legal penalty in Taiwan.
	The death penalty can be imposed for murder, treason, drug trafficking, piracy, terrorism,
	and especially serious cases of robbery, rape, and kidnapping, as well as for military offences, such as desertion during war time.
	In practice, however, all executions in Taiwan since the early 2000s have been for murder.</p>
	HTML;

$div_wikipedia_National_Human_Rights_Commission_Taiwan = new WikipediaContentSection();
$div_wikipedia_National_Human_Rights_Commission_Taiwan->setTitleText('National Human Rights Commission Taiwan');
$div_wikipedia_National_Human_Rights_Commission_Taiwan->setTitleLink('https://en.wikipedia.org/wiki/National_Human_Rights_Commission_(Taiwan)');
$div_wikipedia_National_Human_Rights_Commission_Taiwan->content = <<<HTML
	<p>The National Human Rights Commission of Taiwan was founded on August 1, 2020 as Taiwan’s national human rights institution.
	The commission is designed to promote and protect human rights in Taiwan and fulfill the government's commitment to meet the Paris Principles.</p>
	HTML;



$page->parent('world.html');
$page->template("stub");
$page->body($div_introduction);
$page->body('project/taiwan_democracy.html');
$page->body('china_and_taiwan.html');
$page->body('one_country_two_systems.html');
$page->body('taxes_in_taiwan.html');
$page->body('political_science_in_taiwan.html');

$page->body($div_list_Taiwan);

$page->body('Country indices');


$page->body($div_wikipedia_Taiwan);
$page->body($div_wikipedia_Politics_of_the_Republic_of_China);
$page->body($div_wikipedia_Political_status_of_Taiwan);
$page->body($div_wikipedia_Human_rights_in_Taiwan);
$page->body($div_wikipedia_National_Human_Rights_Commission_Taiwan);
$page->body($div_wikipedia_Capital_punishment_in_Taiwan);
$page->body($div_wikipedia_Taiwan_China);
