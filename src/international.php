<?php
$page = new Page();
$page->h1("7: International");
$page->tags("Topic portal");
$page->keywords("International", "international");
$page->stars(1);
$page->viewport_background("/free/international.png");

$page->snp("description", "The international aspects of democracy and social justice.");
$page->snp("image",       "/free/international.1200-630.png");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Just like no man is an island and no individual is completely isolated from the surrounding society,
	no country can ever be fully independent from other countries,
	and each democratic country is affecting and affected by other countries around the world.
	Democracy and Social Justice are truly global issues.</p>
	HTML;



$page->parent('seventh_level_of_democracy.html');
$page->previous("living.html");
$page->next("humanity.html");

$page->body($div_introduction);
$page->related_tag("International");
