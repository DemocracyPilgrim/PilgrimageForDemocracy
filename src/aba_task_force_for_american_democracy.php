<?php
$page = new Page();
$page->h1("ABA Task Force for American Democracy");
$page->tags("Organisation", "USA", "Democracy", "Institutions: Judiciary", "American Bar Association", "Electoral System");
$page->keywords("ABA Task Force for American Democracy", "Task Force for American Democracy");
$page->stars(1);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

$r1 = $page->ref("https://www.americanbar.org/groups/public_interest/election_law/american-democracy/resources/", "Task Force for American Democracy: resources");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>The Task Force for American Democracy is a task force of the ${"American Bar Association"},
	created by ${'Mary Smith'}, and where judge ${'Michael Luttig'} is a co-chair.</p>

	<p>It bestows the ${"Unsung Heroes of Democracy"} $Award.</p>

	<p>The Task Force has working papers on the following topics $r1:</p>

	<ul>
		<li>Political Reforms to Combat Extremism</li>
		<li>Increasing Trust in Our Elections</li>
		<li>The State of Civics Education in the General Populace</li>
		<li>Deepfakes and American Elections</li>
		<li>Addressing Baseless Election Related Lawsuits</li>
		<li>Improving Participation in Democratic Processes</li>
		<li>Reinvigorating American Democracy – A Youth Perspective</li>
		<li>Improving Access to Voting</li>
		<li>The State of Democracy Education in Law Schools</li>
		<li>Keeping the Administration of Our  Elections from Becoming Politicized</li>
		<li>Decreasing the Political Polarization of the American Public</li>
		<li>Addressing Negative Partisanship with Mobile Voting</li>
		<li>Reviving the American Tradition of Fusion Voting</li>
		<li>Proportional Representation</li>
		<li>Why Democracy?</li>
		<li>How Does the First Amendment Right  of ${'Free Speech'} Intersect with $Democracy?</li>
		<li>Democracy:  The Rule of Law and Human Rights</li>
		<li>(Dis)Trust in Elections: Identifying Who Distrusts the Election Process and Why</li>
		<li>24 for ’24: Urgent Recommendations in Law, Media, Politics, and Tech for Fair and Legitimate 2024 U.S. Elections</li>
		<li>Building Trust: Election Administration and the Role of Higher Education</li>
		<li>Communicating with Voters to Build Trust in the U.S. Election System</li>
		<li>Democracy Depends On All of Us | Brennan Center for Justice</li>
		<li>Deterring Threats to Election Workers</li>
		<li>Guns and Voting: How to Protect Elections After Bruen</li>
		<li>How Federalism Plus Polarization Create Distrust in American Elections (and Experimental Evidence for a Potential Solution)</li>
		<li>Improving the Voting Experience After 2020</li>
		<li>New Models for Keeping Partisans out of Election Administration</li>
		<li>New Models for Keeping Partisans out of Election Administration</li>
		<li>Securing 2024: Defending US Elections Through Investment and Reform</li>
		<li>The 2024 Election: Rebuilding Trust</li>
		<li>The Financial and Economic Dangers of Democratic Backsliding</li>
		<li>The Legal Ethics of Lying About American Democracy</li>
		<li>Election Administration as a Licensed Profession</li>
		<li>How to Host a Listening Tour in Your Community</li>
	</ul>
	HTML;




$div_ABA_Task_Force_for_American_Democracy = new WebsiteContentSection();
$div_ABA_Task_Force_for_American_Democracy->setTitleText("ABA Task Force for American Democracy ");
$div_ABA_Task_Force_for_American_Democracy->setTitleLink("https://www.americanbar.org/groups/public_interest/election_law/american-democracy/");
$div_ABA_Task_Force_for_American_Democracy->content = <<<HTML
	<p>The Task Force for American Democracy, co-chaired by former Federal Judge J. Michael Luttig
	and former Secretary of Homeland Security Jeh Charles Johnson,
	examines ways to ensure an enduring American democracy.
	R. William Ide III serves as vice chair.</p>
	HTML;




$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_ABA_Task_Force_for_American_Democracy);
