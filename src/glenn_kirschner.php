<?php
$page = new Page();
$page->h1('Glenn Kirschner');
$page->alpha_sort("Kirschner, Glenn");
$page->tags("Person", "Institutions: Judiciary", "USA", "Lawyer");
$page->keywords('Glenn Kirschner');
$page->stars(0);

$page->snp('description', 'American attorney.');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Glenn Kirschner is an $American attorney.
	He provides legal analysis on mainstream television media and on his own youtube channel
	about legal cases against ${'Donald Trump'}.</p>
	HTML;

$div_wikipedia_Glenn_Kirschner = new WikipediaContentSection();
$div_wikipedia_Glenn_Kirschner->setTitleText('Glenn Kirschner');
$div_wikipedia_Glenn_Kirschner->setTitleLink('https://en.wikipedia.org/wiki/Glenn_Kirschner');
$div_wikipedia_Glenn_Kirschner->content = <<<HTML
	<p>Glenn L. Kirschner is an American attorney, a former U.S. Army prosecutor, and an NBC News/MSNBC legal analyst.</p>
	HTML;


$page->parent('list_of_people.html');
$page->template("stub");
$page->body($div_introduction);
$page->body('american_fascism.html');


$page->body($div_wikipedia_Glenn_Kirschner);
