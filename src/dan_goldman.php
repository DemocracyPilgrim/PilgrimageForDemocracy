<?php
$page = new PersonPage();
$page->h1("Dan Goldman");
$page->alpha_sort("Goldman, Dan", "Democratic Party");
$page->tags("Person");
$page->keywords("Dan Goldman");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Dan_Goldman = new WikipediaContentSection();
$div_wikipedia_Dan_Goldman->setTitleText("Dan Goldman");
$div_wikipedia_Dan_Goldman->setTitleLink("https://en.wikipedia.org/wiki/Dan_Goldman");
$div_wikipedia_Dan_Goldman->content = <<<HTML
	<p>Daniel Sachs Goldman is an American attorney, politician, and heir; he is a member of the U.S. House of Representatives from New York's 10th congressional district. A politically progressive member of the Democratic Party, he previously served as the lead majority counsel in the first impeachment inquiry against Donald Trump and lead counsel to House Managers in Trump's impeachment trial which was also in 2019. Goldman is among the wealthiest members of Congress, with an estimated personal net worth of up to $253 million according to financial disclosure forms.</p>
	HTML;


$page->parent('list_of_people.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Dan Goldman");
$page->body($div_wikipedia_Dan_Goldman);
