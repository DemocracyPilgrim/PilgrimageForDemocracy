<?php
$page = new Page();
$page->h1('Kerry Washington');
$page->alpha_sort("Washington, Kerry");
$page->tags("Person");
$page->keywords('Kerry Washington');
$page->stars(0);

//$page->snp('description', '');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Kerry Washington is an American actress.
	She is engaged on civic issues, against voter apathy, for voting rights, and against violence against women and girls.</p>
	HTML;


$div_quotes = new ContentSection();
$div_quotes->content = <<<HTML
	<h3>Quotes</h3>

	<blockquote>
	Service is a way out of despair.
	</blockquote>

	<blockquote>
	We have this culture of hero worship, where we put the solution in other people's hands.
	</blockquote>

	<blockquote>
	It's not about Olivia Pope [a fictional character] fixing the election,
	it's about realizing how many people didn't vote in the election,
	and that every one of those people gave their power up.
	That's why voting rights is so important.
	I do get so overwhelmed by how much needs to be fixed:
	education, health care, the environment, reproductive rights, all of it...
	But at least if we're fighting for voting right, we're fighting for our voice.
	We're not giving up our voice in all of those issues.</p>
	HTML;



$div_youtube_You_are_the_fixer_Kerry_Washington_reveals_the_key_to_civic_engagement = new YoutubeContentSection();
$div_youtube_You_are_the_fixer_Kerry_Washington_reveals_the_key_to_civic_engagement->setTitleText('‘You are the fixer’: Kerry Washington reveals the key to civic engagement');
$div_youtube_You_are_the_fixer_Kerry_Washington_reveals_the_key_to_civic_engagement->setTitleLink('https://www.youtube.com/watch?v=RE5G3u-VIbU');
$div_youtube_You_are_the_fixer_Kerry_Washington_reveals_the_key_to_civic_engagement->content = <<<HTML
	HTML;


$div_wikipedia_Kerry_Washington = new WikipediaContentSection();
$div_wikipedia_Kerry_Washington->setTitleText('Kerry Washington');
$div_wikipedia_Kerry_Washington->setTitleLink('https://en.wikipedia.org/wiki/Kerry_Washington');
$div_wikipedia_Kerry_Washington->content = <<<HTML
	<p>Kerry Washington is an American actress.</p>
	HTML;


$page->parent('list_of_people.html');
$page->template("stub");
$page->body($div_introduction);
$page->body($div_quotes);


$page->body($div_youtube_You_are_the_fixer_Kerry_Washington_reveals_the_key_to_civic_engagement);
$page->body($div_wikipedia_Kerry_Washington);
