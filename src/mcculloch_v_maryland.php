<?php
$page = new Page();
$page->h1("McCulloch v. Maryland (1819)");
$page->tags("Institutions: Judiciary", "SCOTUS Landmark Decision", "USA", "Taxes", "Banking", "Subsidiarity");
$page->keywords("McCulloch v. Maryland");
$page->stars(1);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>"McCulloch v. Maryland" is a 1819 landmark decision by the ${'Supreme Court of the United States'}.</p>

	<p>This decision established the principle of implied powers,
	which holds that the federal government has powers beyond those explicitly listed in the Constitution.</p>

	<p>Critics argue that this decision has led to an expansion of federal power at the expense of states' rights,
	potentially undermining the balance of power between federal and state governments.</p>

	<p>Supporters of the decision argue that it is necessary to ensure that
	the federal government has the flexibility to address emerging issues and effectively carry out its responsibilities.</p>
	HTML;

$div_wikipedia_McCulloch_v_Maryland = new WikipediaContentSection();
$div_wikipedia_McCulloch_v_Maryland->setTitleText("McCulloch v Maryland");
$div_wikipedia_McCulloch_v_Maryland->setTitleLink("https://en.wikipedia.org/wiki/McCulloch_v._Maryland");
$div_wikipedia_McCulloch_v_Maryland->content = <<<HTML
	<p>McCulloch v. Maryland, 17 U.S. (4 Wheat.) 316 (1819), was a landmark U.S. Supreme Court decision
	that defined the scope of the U.S. Congress's legislative power and how it relates to the powers of American state legislatures.
	The dispute in McCulloch involved the legality of the national bank and a tax that the state of Maryland imposed on it.
	In its ruling, the Supreme Court established firstly that the "Necessary and Proper" Clause of the U.S. Constitution
	gives the U.S. federal government certain implied powers necessary and proper for the exercise of the powers enumerated explicitly in the Constitution,
	and secondly that the American federal government is supreme over the states, and so states' ability to interfere with the federal government is restricted.
	Since the legislature has the authority to tax and spend, the court held that
	it therefore has authority to establish a national bank, as being "necessary and proper" to that end.</p>

	<p>McCulloch has been described as "the most important Supreme Court decision in American history
	defining the scope of Congress's powers and delineating the relationship between the federal government and the states."
	The case established two important principles in constitutional law.
	First, the Constitution grants to Congress implied powers to implement the Constitution's express powers to create a functional national government.
	Prior to the Supreme Court's decision in McCulloch, the scope of the U.S. government's authority was unclear.
	Second, state action may not impede valid constitutional exercises of power by the federal government.</p>
	HTML;


$page->parent('supreme_court_of_the_united_states.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_wikipedia_McCulloch_v_Maryland);
