<?php
$page = new Page();
$page->h1("List of awards and prizes");
$page->keywords("awards", "prize", "Award", "Prize");
$page->stars(0);
$page->viewport_background('/free/list_of_awards.png');

$page->snp("description", "/free/list_of_awards.1200-630.png");
$page->snp("image",       "Recognizing achievements in the fields of democracy and social justice.");

$page->preview( <<<HTML
	<p>Awards and prizes related to peace, democracy or social justice.</p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Some awards and prizes which are related to the themes discussed in the $Pilgrimage.</p>

	<p>You can suggest any relevant awards to be added to this list.</p>
	HTML;




$page->parent('lists.html');
$page->template("stub");
$page->body($div_introduction);

$page->related_tag(array("Award", "Prize"));
