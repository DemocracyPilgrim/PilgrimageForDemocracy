<?php
$page = new PersonPage();
$page->h1("Charlie Chaplin");
$page->alpha_sort("Chaplin, Charlie");
$page->tags("Person");
$page->keywords("Charlie Chaplin");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Charlie_Chaplin = new WikipediaContentSection();
$div_wikipedia_Charlie_Chaplin->setTitleText("Charlie Chaplin");
$div_wikipedia_Charlie_Chaplin->setTitleLink("https://en.wikipedia.org/wiki/Charlie_Chaplin");
$div_wikipedia_Charlie_Chaplin->content = <<<HTML
	<p>Sir Charles Spencer Chaplin KBE (16 April 1889 – 25 December 1977) was an English comic actor, filmmaker, and composer who rose to fame in the era of silent film. He became a worldwide icon through his screen persona, the Tramp, and is considered one of the film industry's most important figures. His career spanned more than 75 years, from childhood in the Victorian era until a year before his death in 1977, and encompassed both accolade and controversy.</p>
	HTML;


$page->parent('list_of_people.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Charlie Chaplin");
$page->body($div_wikipedia_Charlie_Chaplin);
