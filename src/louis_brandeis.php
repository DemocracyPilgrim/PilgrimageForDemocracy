<?php
$page = new Page();
$page->h1("Louis Brandeis");
$page->alpha_sort("Brandeis, Louis");
$page->tags("Person");
$page->keywords("Louis Brandeis");
$page->stars(0);

//$page->snp("description", "");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref('', '');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;


$div_Quotes = new ContentSection();
$div_Quotes->content = <<<HTML
	<h3>Quotes</h3>

	<blockquote>We must make our choice. We may have democracy,
	or we may have wealth concentrated in the hands of a few, but we cannot have both.</blockquote>

	<blockquote>
	The most important political office is that of private citizen.
	</blockquote>
	HTML;


$div_wikipedia_Louis_Brandeis = new WikipediaContentSection();
$div_wikipedia_Louis_Brandeis->setTitleText("Louis Brandeis");
$div_wikipedia_Louis_Brandeis->setTitleLink("https://en.wikipedia.org/wiki/Louis_Brandeis");
$div_wikipedia_Louis_Brandeis->content = <<<HTML
	<p>Louis Dembitz Brandeis (1856 – 1941) was an American lawyer
	who served as an associate justice on the Supreme Court of the United States from 1916 to 1939.</p>
	HTML;


$page->parent('list_of_people.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_Quotes);
$page->body($div_wikipedia_Louis_Brandeis);
