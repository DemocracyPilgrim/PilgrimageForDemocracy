<?php
$page = new CountryPage("Belgium");
$page->h1("Belgium");
$page->tags("Country");
$page->keywords("Belgium");
$page->stars(0);
$page->viewport_background("");

$page->snp("description", "11,8 million inhabitants");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Belgium = new WikipediaContentSection();
$div_wikipedia_Belgium->setTitleText("Belgium");
$div_wikipedia_Belgium->setTitleLink("https://en.wikipedia.org/wiki/Belgium");
$div_wikipedia_Belgium->content = <<<HTML
	<p>
	</p>
	HTML;


$page->parent('world.html');
$page->template("stub");
$page->body($div_introduction);
$page->country_indices();



$page->related_tag("Belgium");
$page->body($div_wikipedia_Belgium);
