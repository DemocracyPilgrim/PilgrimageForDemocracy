<?php
$page = new Page();
$page->h1("Shameless Republicans Deliberate Dysfunction and the Battle to Preserve Democracy");
$page->tags("Book", "USA", "MAGA", "Donald Trump", "Information: Media");
$page->keywords("Shameless: Republicans' Deliberate Dysfunction and the Battle to Preserve Democracy");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>"Shameless: Republicans' Deliberate Dysfunction and the Battle to Preserve Democracy"
	is a book by ${"Brian Tyler Cohen"}.</p>
	HTML;


$div_Shameless_Republicans_Deliberate_Dysfunction_and_the_Battle_to_Preserve_Democracy = new WebsiteContentSection();
$div_Shameless_Republicans_Deliberate_Dysfunction_and_the_Battle_to_Preserve_Democracy->setTitleText("Shameless Republicans Deliberate Dysfunction and the Battle to Preserve Democracy ");
$div_Shameless_Republicans_Deliberate_Dysfunction_and_the_Battle_to_Preserve_Democracy->setTitleLink("https://www.harpercollins.com/products/shameless-brian-tyler-cohen");
$div_Shameless_Republicans_Deliberate_Dysfunction_and_the_Battle_to_Preserve_Democracy->content = <<<HTML
	<p>From the first content creator to interview President Biden,
	leading progressive voice Brian Tyler Cohen takes a step back from the day-to-day news cycle
	to explain how American politics has turned into such a dumpster fire—and what Democrats need to do to get us out of it.</p>

	<p>In Shameless, Brian Tyler Cohen lays bare the long con of the modern Republican Party.
	While the radical right continues hiding behind gaslighting maneuvers, artificial slogans, and hollow catchphrases,
	Cohen’s unflinching narrative illuminates the realities and dangers
	of the ever-widening gulf between the vaunted Republican brand and their actual behavior.</p>

	<p>With a foreword by Congressman Jamie Raskin, drawing on interviews
	and insights from Pete Buttigieg, Mehdi Hasan, Jen Psaki, and other luminaries of the Left, Cohen reveals:</p>

	<ul>
	<li>How Republicans have leaned on their historical branding to give themselves a permission structure to behave antithetically to everything they say;</li>
	<li>Why the mainstream media has proved itself a willing participant in this ongoing farce— particularly since the rise of toxic, sensational­ist MAGA mania; and</li>
	<li>What lessons Democrats can glean from a clear-eyed view of the landscape we’re operating in—and the steps we must take to rebalance our political landscape.</li>
	</ul>

	<p>During this all-hands-on-deck moment in our his­tory, Shameless is essential reading for those seeking to understand our dire situation,
	and a rallying cry for those fighting to preserve democracy.</p>
	HTML;



$page->parent('list_of_books.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_Shameless_Republicans_Deliberate_Dysfunction_and_the_Battle_to_Preserve_Democracy);
