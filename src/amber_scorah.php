<?php
$page = new PersonPage();
$page->h1("Amber Scorah");
$page->alpha_sort("Scorah, Amber");
$page->tags("Person", "Whistleblower", "Cult", "Religion");
$page->keywords("Amber Scorah");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Amber_Scorah = new WikipediaContentSection();
$div_wikipedia_Amber_Scorah->setTitleText("Amber Scorah");
$div_wikipedia_Amber_Scorah->setTitleLink("https://en.wikipedia.org/wiki/Amber_Scorah");
$div_wikipedia_Amber_Scorah->content = <<<HTML
	<p>
	</p>
	HTML;


$page->parent('list_of_people.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Amber Scorah");
$page->body($div_wikipedia_Amber_Scorah);
