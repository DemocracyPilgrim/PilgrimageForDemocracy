<?php
$page = new Page();
$page->h1("United for Peace and Justice");
$page->keywords("United for Peace and Justice");
$page->tags("Organisation");
$page->stars(0);

//$page->snp("description", "");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;



$div_United_for_Peace_and_Justice_website = new WebsiteContentSection();
$div_United_for_Peace_and_Justice_website->setTitleText("United for Peace and Justice website ");
$div_United_for_Peace_and_Justice_website->setTitleLink("https://www.unitedforpeace.org/");
$div_United_for_Peace_and_Justice_website->content = <<<HTML
	<p>United for Peace and Justice was founded in late 2002, bringing together a broad spectrum of organizations in the United States
	opposed to the war that all could see our government preparing to launch against Iraq.
	UFPJ played a leading role in organizing the global protests against the war in February 2003,
	including a February 15 demonstration in New York City that drew over 500,000 participants.
	Later in 2003, UFPJ member groups met in Chicago, and drafted the statement of unity below, adopted as a work in progress.
	The opening paragraph expresses opposition to the particular war danger of that time.
	The remainder of the Unity Statement sets forth fundamental principles that continue to ground and guide our work today.</p>
	HTML;



$div_wikipedia_United_for_Peace_and_Justice = new WikipediaContentSection();
$div_wikipedia_United_for_Peace_and_Justice->setTitleText("United for Peace and Justice");
$div_wikipedia_United_for_Peace_and_Justice->setTitleLink("https://en.wikipedia.org/wiki/United_for_Peace_and_Justice");
$div_wikipedia_United_for_Peace_and_Justice->content = <<<HTML
	<p>United for Peace and Justice (UFPJ) is a coalition of more than 1,300 international and U.S.-based organizations
	opposed to "our government's policy of permanent warfare and empire-building."</p>

	<p>The organization was founded in October 2002 during the build-up to the United States' 2003 invasion of Iraq by dozens of groups
	including the National Organization for Women, National Council of Churches, Peace Action, the American Friends Service Committee,
	Black Voices for Peace, Not In Our Name, September Eleventh Families for Peaceful Tomorrows, and Veterans for Peace.
	Its first joint action was anti-war protests on International Human Rights Day, December 10, 2002.
	The direct precursor to UFPJ was "United We March!", initiated by Global Exchange, the Green Party of the United States, and others,
	which organized the April 20, 2002, demonstration against the U.S. invasion of Afghanistan.</p>
	HTML;


$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_United_for_Peace_and_Justice_website);
$page->body($div_wikipedia_United_for_Peace_and_Justice);
