<?php
$page = new Page();
$page->h1('Duverger symptom 4: negative campaigning');
$page->stars(0);
$page->tags("Elections: Duverger Syndrome");
$page->keywords('negative campaigning');

//$page->snp('description', "");
//$page->snp('image', "/copyrighted/");

$page->preview( <<<HTML
	<p>Negative campaigning and dirty politics. It does not have to be this way!</p>
	HTML );


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>$Politics is a noble art.
	Being a politician should be a noble profession, an act of public service.</p>

	<p>Unfortunately, negative campaigning and dirty politics is spoiling everything.</p>

	<p>Everybody recognises negative campaigning.
	Nobody like it.
	It is everywhere.</p>

	<p>Politics really does not have to be the way it is today.
	With the use of a proper ${'voting method'}, we could solve most of the dirtiest aspects of politics,
	and mostly turn negative campaigning as a distant memory from the past.</p>
	HTML;



$div_wikipedia_Negative_campaigning = new WikipediaContentSection();
$div_wikipedia_Negative_campaigning->setTitleText("Negative campaigning");
$div_wikipedia_Negative_campaigning->setTitleLink("https://en.wikipedia.org/wiki/Negative_campaigning");
$div_wikipedia_Negative_campaigning->content = <<<HTML
	<p>Negative campaigning is the process of deliberately spreading negative information
	about someone or something to worsen the public image of the described.
	A colloquial, and somewhat more derogatory, term for the practice is mudslinging.
	</p>
	HTML;




$page->parent('duverger_syndrome.html');
$page->parent('duverger_syndrome_party_politics.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_wikipedia_Negative_campaigning);

$page->body('duverger_syndrome_lack_choice.html');
