<?php
$page = new Page();
$page->h1("A Night at the Garden");
$page->tags("USA", "Documentary");
$page->keywords("A Night at the Garden");
$page->stars(2);

$page->snp("description", "Documentary film about a 1939 Nazi rally in New York City.");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p>A Night at the Garden is a 2017 short documentary film about the 1939 Nazi rally that filled Madison Square Garden in New York City.</p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>A Night at the Garden is a 2017 short documentary film about the 1939 Nazi rally that filled Madison Square Garden in New York City.</p>
	HTML;



$div_A_Night_at_the_Garden = new WebsiteContentSection();
$div_A_Night_at_the_Garden->setTitleText("Watch online: A Night at the Garden");
$div_A_Night_at_the_Garden->setTitleLink("https://anightatthegarden.com/");
$div_A_Night_at_the_Garden->content = <<<HTML
	<p>In 1939, 20,000 Americans rallied in New York’s Madison Square Garden
	to celebrate the rise of Nazism – an event largely forgotten from American history.
	A NIGHT AT THE GARDEN, made entirely from archival footage filmed that night,
	transports audiences to this chilling gathering and
	shines a light on the power of demagoguery and anti-Semitism in the United States.</p>
	HTML;



$div_Revisiting_the_American_Nazi_Supporters_of_A_Night_at_the_Garden = new WebsiteContentSection();
$div_Revisiting_the_American_Nazi_Supporters_of_A_Night_at_the_Garden->setTitleText("The New Yorker: Revisiting the American Nazi Supporters of “A Night at the Garden”");
$div_Revisiting_the_American_Nazi_Supporters_of_A_Night_at_the_Garden->setTitleLink("https://www.newyorker.com/news/daily-comment/revisiting-the-american-nazi-supporters-of-a-night-at-the-garden");
$div_Revisiting_the_American_Nazi_Supporters_of_A_Night_at_the_Garden->content = <<<HTML
	<p>But even more unnerving than the strangeness of the spectacle is the creeping sense of familiarity it evokes.
	Kuhn’s snarky excoriation of the “Jewish-controlled” press,
	his demand “that our government shall be returned to the American people who founded it,”
	and even the idolatry of the Founding Fathers all have their echoes in far-right politics today.
	No moment in the film seems more redolent of our current demagogue’s maga rallies
	than the one in which a protester scrambles onto the stage—he was Isadore Greenbaum,
	a twenty-six-year-old plumber’s helper from Brooklyn—and is promptly tackled and pummelled by Kuhn supporters,
	amid appreciative laughter and hooting from the crowd.</p>

	<p>One advantage to living through Trumpism is that it has compelled
	a reckoning with aspects of our country’s past that, for a long time, many Americans preferred not to acknowledge.
	Donald Trump’s encouragement of white supremacists accelerated the examination of why we were still living among so many monuments to the Confederacy.
	The work undertaken by historians and activists to document America’s full, horrifying legacy of lynching acquired a new urgency.</p>
	HTML;



$div_80_years_ago_20_000_New_Yorkers_cheered_for_Nazis_at_Madison_Square_Garden = new WebsiteContentSection();
$div_80_years_ago_20_000_New_Yorkers_cheered_for_Nazis_at_Madison_Square_Garden->setTitleText("Vox: 80 years ago, 20,000 New Yorkers cheered for Nazis at Madison Square Garden");
$div_80_years_ago_20_000_New_Yorkers_cheered_for_Nazis_at_Madison_Square_Garden->setTitleLink("https://www.vox.com/culture/2019/2/20/18230052/night-at-garden-oscars-2019-madison-square-bund-anniversary-interview");
$div_80_years_ago_20_000_New_Yorkers_cheered_for_Nazis_at_Madison_Square_Garden->content = <<<HTML
	<p>“Most Americans assume that in the leadup to World War II, all of our countrymen and women were appalled by Nazi ideology.
	But when you see 20,000 New Yorkers carrying swastika flags and cheering for Nazi ideas, you realize that things weren’t so clear back then.
	There was a significant minority of Americans who thought fascism and racism and anti-semitism were okay.”</p>

	<p>This historical detail was largely erased from our collective memories once the war started and Nazis started killing American soldiers.
	But that particular turn of events wasn’t a foregone conclusion. And Curry says it’s important to remember because of the lessons it holds for today.</p>

	<p>“We can’t be complacent. If things are going to work out okay now,
	it’s going to be because of the efforts of people who push back on the anti-immigrant, anti-semitic, anti-Muslim ideas
	that seem to be creeping back in our country.”</p>

	<p>The film’s contemporary resonance is why it was supported by Field of Vision,
	a documentary unit that commissions short documentary films on urgent matters around the globe.
	You can’t miss the echoes with current events. And that’s on purpose.</p>

	<p>“When you see the leader take the stage and attack the press and tell the audience to take back America from the minorities who are destroying it
	— and then laugh and sneer when a protester is beaten up — this all feels very familiar.”
	“And I hoped that when Trump supporters saw what this kind of behavior led to in 1939,
	it might make them more sensitive to it when they saw it today.
	And when casual Trump critics saw it, they might take the threat a little more seriously.”</p>
	HTML;



$div_wikipedia_A_Night_at_the_Garden = new WikipediaContentSection();
$div_wikipedia_A_Night_at_the_Garden->setTitleText("A Night at the Garden");
$div_wikipedia_A_Night_at_the_Garden->setTitleLink("https://en.wikipedia.org/wiki/A_Night_at_the_Garden");
$div_wikipedia_A_Night_at_the_Garden->content = <<<HTML
	<p>A Night at the Garden is a 2017 short documentary film about the 1939 Nazi rally that filled Madison Square Garden in New York City.
	The seven-minute film is composed entirely of archival footage and features a speech from Fritz Julius Kuhn,
	the leader of the German American Bund, in which anti-Semitic and pro white-Christian sentiments are espoused.</p>
	HTML;


$page->parent('list_of_documentaries.html');
$page->template("stub");
$page->body($div_introduction);
$page->body($div_A_Night_at_the_Garden);


$page->body('american_fascism.html');
$page->body('prequel_an_american_fight_against_fascism.html');

$page->body($div_80_years_ago_20_000_New_Yorkers_cheered_for_Nazis_at_Madison_Square_Garden);
$page->body($div_Revisiting_the_American_Nazi_Supporters_of_A_Night_at_the_Garden);

$page->body($div_wikipedia_A_Night_at_the_Garden);
