<?php
$page = new Page();
$page->h1("Center for Election Innovation & Research");
$page->tags("Organisation", "Elections", "Election Integrity");
$page->keywords("Center for Election Innovation & Research", "CEIR");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

$r1 = $page->ref("https://electioninnovation.org/research/overview-of-private-funding-bans/", "Restrictions on Private Funding for Elections");
$r2 = $page->ref("https://electioninnovation.org/update/american-bar-association-names-ceir-an-unsung-hero-of-democracy/", "American Bar Association names CEIR an ‘Unsung Hero of Democracy’");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>The Center for Election Innovation & Research (CEIR) is conducting research on the different states in the $USA
	which have legislation restricting private funding of elections. $r1 See also: ${'funding for elections'}.</p>

	<p>In 2024, the CEIR was the recipient of the ${"ABA Task Force for American Democracy"}'s ${"Unsung Heroes of Democracy"} $Award
	for their nonpartisan work with election officials of both parties, conducting research,
	coordinating pro bono legal defense for election officials under threat of harassment,
	and advocating for best practices that have tangibly improved the security, transparency, and efficiency of elections. $r2</p>

	<p>The CEIR was founded by ${'David Becker'}.</p>
	HTML;




$div_Center_for_Election_Innovation_Research = new WebsiteContentSection();
$div_Center_for_Election_Innovation_Research->setTitleText("Center for Election Innovation & Research");
$div_Center_for_Election_Innovation_Research->setTitleLink("https://electioninnovation.org/");
$div_Center_for_Election_Innovation_Research->content = <<<HTML
	<p>The Center for Election Innovation & Research (CEIR) is a nonprofit whose core mission is to work with election officials
	and build confidence in elections that voters should trust and do trust.
	CEIR has a proven record of working with election officials from around the country and both sides of the aisle.
	Our team has spent decades building trust with key stakeholders in the field and is committed to a fiercely nonpartisan approach.</p>
	HTML;



$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);

$page->body($div_Center_for_Election_Innovation_Research);
