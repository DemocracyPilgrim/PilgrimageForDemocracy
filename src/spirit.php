<?php
$page = new Page();
$page->h1('Addendum C: Spirit');
$page->viewport_background("/free/spirit.png");
$page->keywords("Spirit");
$page->stars(0);
$page->tags("Topic portal", "Religion", "Philosophy");

$page->snp("description", "The Human Spirit and human development.");
$page->snp("image",       "/free/spirit.1200-630.png");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>The Human Spirit, from all humanist, philosophical or religious perspectives.</p>
	HTML;

$div_wikipedia_Spirit_animating_force = new WikipediaContentSection();
$div_wikipedia_Spirit_animating_force->setTitleText("Spirit animating force");
$div_wikipedia_Spirit_animating_force->setTitleLink("https://en.wikipedia.org/wiki/Spirit_(animating_force)");
$div_wikipedia_Spirit_animating_force->content = <<<HTML
	<p>In philosophy and religion, spirit is the vital principle or animating essence within humans or, in some views, all living things. Although views of spirit vary between different belief systems, when spirit is contrasted with the soul, the former is often seen as a basic natural force, principle or substance, whereas the latter is used to describe the organized structure of an individual being's consciousness, in humans including their personality. Spirit as a substance may also be contrasted with matter, where it is usually seen as more subtle, an idea put forth for example in the Principia Mathematica.</p>
	HTML;


$page->parent('saints_and_little_devils.html');
$page->previous("teamwork.html");
$page->next("education.html");

$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Spirit");
$page->body($div_wikipedia_Spirit_animating_force);
