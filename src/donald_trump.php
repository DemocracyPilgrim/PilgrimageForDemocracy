<?php
$page = new Page();
$page->h1('Donald Trump');
$page->alpha_sort("Trump, convicted felon");
$page->tags("Person", "USA", "Elections", "Racism", "Disinformation", "POTUS");
$page->keywords('Donald Trump', 'Donald Trump: Research', 'Trump');
$page->viewport_background('/free/donald_trump.png');
$page->stars(1);

$page->snp('description', 'An existential threat to democracy');
$page->snp('image',       '/free/donald_trump.1200-630.png');

$page->preview( <<<HTML
	<p></p>
	HTML );


$div_A_Call_for_Change = new ContentSection();
$div_A_Call_for_Change->content = <<<HTML
	<h3>Learning from Trumpism: A Call for Change</h3>

	<p>The rise of Donald Trump as a political figure has exposed deep fissures in our $society, highlighting the need for profound change.
	Trump's divisive rhetoric, disregard for truth, and embrace of populist tactics have contributed to a climate of polarization,
	mistrust in institutions, and an erosion of shared values.</p>

	<p>To move forward, we must learn from this experience and work towards a more resilient society.
	This requires a multi-pronged approach:</p>

	<ul>
	<li><strong>Strengthening Democratic Institutions</strong>:
	Reforming our democratic institutions to ensure transparency, accountability, and equitable representation is essential.</li>

	<li><strong>Promoting Critical Thinking and Media Literacy</strong>:
	Empowering citizens to discern fact from fiction and critically evaluate information is critical in combating misinformation and manipulative rhetoric.</li>

	<li><strong>Cultivating Empathy and Compassion</strong>:
	Building a society rooted in empathy and understanding can help bridge divides and foster a more inclusive and equitable future.</li>
	</ul>

	<p>By addressing these critical areas, we can work towards a society that is less susceptible to the allure of divisive figures
	and fosters a more just and equitable future for all.</p>
	HTML;


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Donald Trump's presidency has sparked widespread concern among scholars, legal experts, and the media,
	as his actions have been widely seen as posing a significant threat to $democratic principles and $institutions.
	The damage he has inflicted on $American society and the rule of law is well-documented
	(refer to the extensive list of $Wikipedia articles provided below).
	However, our task goes beyond simply cataloguing the damage.
	We must delve deeper into the root causes that allowed Trump to rise to power and the factors that enabled his destructive behaviour.
	By understanding these contributing factors,
	we can identify concrete steps to safeguard democratic institutions globally and prevent the rise of similar figures in the future.</ul>

	<ul>
		<li><strong>Trump's actions have eroded faith in elections</strong>:
		Trump's persistent, knowingly false claims about a "stolen" election have led millions of Americans to doubt the legitimacy of US elections,
		damaging trust in democratic institutions.</li>
		<li><strong>Trump's actions have undermined confidence in democracy</strong>:
		Many Americans, particularly young people, are questioning the viability of constitutional democracy due to Trump's actions
		and the ongoing attacks on the election process.</li>
		<li><strong>Trump's actions are an unprecedented attack on democracy</strong>:
		The former President's actions, including attempting to overturn the election results and refusing to accept the peaceful transfer of power,
		are unprecedented in American history and represent a dangerous assault on democratic norms.</li>
		<li><strong>The Republican Party's stance poses a further threat</strong>:
		The Republican Party's continued embrace of Trump's false claims and their refusal to commit to upholding election results are deeply concerning,
		creating the potential for further unrest and violence.</li>
		<li><strong>The peaceful transfer of power is essential</strong>:
		The peaceful transition of power between presidents, a cornerstone of American democracy,
		is under threat due to the former president's actions and the Republican Party's unwillingness to denounce his behavior.</li>
	</ul>
	HTML;


$div_AI_image = new ContentSection();
$div_AI_image->content = <<<HTML
	<p><em>Note: the cover picture is an $AI generated image inspired by the mugshot of Donald Trump in Fulton County, Georgia (2024).</em></p>
	HTML;



$div_Donald_Trump_Research = new ContentSection();
$div_Donald_Trump_Research->content = <<<HTML
	<h3>Donald Trump: Research</h3>

	<p>We aim to eventually learn everything that society ought to learn from all the misfortunes caused by Donald Trump's political career.</p>
	HTML;



$div_wikipedia_Donald_Trump = new WikipediaContentSection();
$div_wikipedia_Donald_Trump->setTitleText('Donald Trump');
$div_wikipedia_Donald_Trump->setTitleLink('https://en.wikipedia.org/wiki/Donald_Trump');
$div_wikipedia_Donald_Trump->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Personal_and_business_legal_affairs_of_Donald_Trump = new WikipediaContentSection();
$div_wikipedia_Personal_and_business_legal_affairs_of_Donald_Trump->setTitleText('Personal and business legal affairs of Donald Trump');
$div_wikipedia_Personal_and_business_legal_affairs_of_Donald_Trump->setTitleLink('https://en.wikipedia.org/wiki/Personal_and_business_legal_affairs_of_Donald_Trump');
$div_wikipedia_Personal_and_business_legal_affairs_of_Donald_Trump->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Trump_University = new WikipediaContentSection();
$div_wikipedia_Trump_University->setTitleText('Trump University');
$div_wikipedia_Trump_University->setTitleLink('https://en.wikipedia.org/wiki/Trump_University');
$div_wikipedia_Trump_University->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Donald_Trump_photo_op_at_St_John_s_Church = new WikipediaContentSection();
$div_wikipedia_Donald_Trump_photo_op_at_St_John_s_Church->setTitleText('Donald Trump photo op at St John\'s Church');
$div_wikipedia_Donald_Trump_photo_op_at_St_John_s_Church->setTitleLink('https://en.wikipedia.org/wiki/Donald_Trump_photo_op_at_St._John\'s_Church');
$div_wikipedia_Donald_Trump_photo_op_at_St_John_s_Church->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Trump_administration_family_separation_policy = new WikipediaContentSection();
$div_wikipedia_Trump_administration_family_separation_policy->setTitleText('Trump administration family separation policy');
$div_wikipedia_Trump_administration_family_separation_policy->setTitleLink('https://en.wikipedia.org/wiki/Trump_administration_family_separation_policy');
$div_wikipedia_Trump_administration_family_separation_policy->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Trump_administration_political_interference_with_science_agencies = new WikipediaContentSection();
$div_wikipedia_Trump_administration_political_interference_with_science_agencies->setTitleText('Trump administration political interference with science agencies');
$div_wikipedia_Trump_administration_political_interference_with_science_agencies->setTitleLink('https://en.wikipedia.org/wiki/Trump_administration_political_interference_with_science_agencies');
$div_wikipedia_Trump_administration_political_interference_with_science_agencies->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Trump_Ukraine_scandal = new WikipediaContentSection();
$div_wikipedia_Trump_Ukraine_scandal->setTitleText('Trump Ukraine scandal');
$div_wikipedia_Trump_Ukraine_scandal->setTitleLink('https://en.wikipedia.org/wiki/Trump–Ukraine_scandal');
$div_wikipedia_Trump_Ukraine_scandal->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_First_impeachment_of_Donald_Trump = new WikipediaContentSection();
$div_wikipedia_First_impeachment_of_Donald_Trump->setTitleText('First impeachment of Donald Trump');
$div_wikipedia_First_impeachment_of_Donald_Trump->setTitleLink('https://en.wikipedia.org/wiki/First_impeachment_of_Donald_Trump');
$div_wikipedia_First_impeachment_of_Donald_Trump->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_First_impeachment_trial_of_Donald_Trump = new WikipediaContentSection();
$div_wikipedia_First_impeachment_trial_of_Donald_Trump->setTitleText('First impeachment trial of Donald Trump');
$div_wikipedia_First_impeachment_trial_of_Donald_Trump->setTitleLink('https://en.wikipedia.org/wiki/First_impeachment_trial_of_Donald_Trump');
$div_wikipedia_First_impeachment_trial_of_Donald_Trump->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Big_lie = new WikipediaContentSection();
$div_wikipedia_Big_lie->setTitleText('Big lie');
$div_wikipedia_Big_lie->setTitleLink('https://en.wikipedia.org/wiki/Big_lie');
$div_wikipedia_Big_lie->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Attempts_to_overturn_the_2020_United_States_presidential_election = new WikipediaContentSection();
$div_wikipedia_Attempts_to_overturn_the_2020_United_States_presidential_election->setTitleText('Attempts to overturn the 2020 United States presidential election');
$div_wikipedia_Attempts_to_overturn_the_2020_United_States_presidential_election->setTitleLink('https://en.wikipedia.org/wiki/Attempts_to_overturn_the_2020_United_States_presidential_election');
$div_wikipedia_Attempts_to_overturn_the_2020_United_States_presidential_election->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Election_denial_movement = new WikipediaContentSection();
$div_wikipedia_Election_denial_movement->setTitleText('Election denial movement');
$div_wikipedia_Election_denial_movement->setTitleLink('https://en.wikipedia.org/wiki/Election_denial_movement');
$div_wikipedia_Election_denial_movement->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_January_6_United_States_Capitol_attack = new WikipediaContentSection();
$div_wikipedia_January_6_United_States_Capitol_attack->setTitleText('January 6 United States Capitol attack');
$div_wikipedia_January_6_United_States_Capitol_attack->setTitleLink('https://en.wikipedia.org/wiki/January_6_United_States_Capitol_attack');
$div_wikipedia_January_6_United_States_Capitol_attack->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Timeline_of_the_January_6_United_States_Capitol_attack = new WikipediaContentSection();
$div_wikipedia_Timeline_of_the_January_6_United_States_Capitol_attack->setTitleText('Timeline of the January 6 United States Capitol attack');
$div_wikipedia_Timeline_of_the_January_6_United_States_Capitol_attack->setTitleLink('https://en.wikipedia.org/wiki/Timeline_of_the_January_6_United_States_Capitol_attack');
$div_wikipedia_Timeline_of_the_January_6_United_States_Capitol_attack->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Second_impeachment_of_Donald_Trump = new WikipediaContentSection();
$div_wikipedia_Second_impeachment_of_Donald_Trump->setTitleText('Second impeachment of Donald Trump');
$div_wikipedia_Second_impeachment_of_Donald_Trump->setTitleLink('https://en.wikipedia.org/wiki/Second_impeachment_of_Donald_Trump');
$div_wikipedia_Second_impeachment_of_Donald_Trump->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Second_impeachment_trial_of_Donald_Trump = new WikipediaContentSection();
$div_wikipedia_Second_impeachment_trial_of_Donald_Trump->setTitleText('Second impeachment trial of Donald Trump');
$div_wikipedia_Second_impeachment_trial_of_Donald_Trump->setTitleLink('https://en.wikipedia.org/wiki/Second_impeachment_trial_of_Donald_Trump');
$div_wikipedia_Second_impeachment_trial_of_Donald_Trump->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_FBI_investigation_into_Donald_Trump_s_handling_of_government_documents = new WikipediaContentSection();
$div_wikipedia_FBI_investigation_into_Donald_Trump_s_handling_of_government_documents->setTitleText('FBI investigation into Donald Trump\' s handling of government documents');
$div_wikipedia_FBI_investigation_into_Donald_Trump_s_handling_of_government_documents->setTitleLink('https://en.wikipedia.org/wiki/FBI_investigation_into_Donald_Trump\'s_handling_of_government_documents');
$div_wikipedia_FBI_investigation_into_Donald_Trump_s_handling_of_government_documents->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Smith_special_counsel_investigation = new WikipediaContentSection();
$div_wikipedia_Smith_special_counsel_investigation->setTitleText('Smith special counsel investigation');
$div_wikipedia_Smith_special_counsel_investigation->setTitleLink('https://en.wikipedia.org/wiki/Smith_special_counsel_investigation');
$div_wikipedia_Smith_special_counsel_investigation->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_FBI_search_of_Mar_a_Lago = new WikipediaContentSection();
$div_wikipedia_FBI_search_of_Mar_a_Lago->setTitleText('FBI search of Mar a Lago');
$div_wikipedia_FBI_search_of_Mar_a_Lago->setTitleLink('https://en.wikipedia.org/wiki/FBI_search_of_Mar-a-Lago');
$div_wikipedia_FBI_search_of_Mar_a_Lago->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_United_States_House_Select_Committee_on_the_January_6_Attack = new WikipediaContentSection();
$div_wikipedia_United_States_House_Select_Committee_on_the_January_6_Attack->setTitleText('United States House Select Committee on the January 6 Attack');
$div_wikipedia_United_States_House_Select_Committee_on_the_January_6_Attack->setTitleLink('https://en.wikipedia.org/wiki/United_States_House_Select_Committee_on_the_January_6_Attack');
$div_wikipedia_United_States_House_Select_Committee_on_the_January_6_Attack->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Prosecution_of_Donald_Trump_in_New_York = new WikipediaContentSection();
$div_wikipedia_Prosecution_of_Donald_Trump_in_New_York->setTitleText('Prosecution of Donald Trump in New York');
$div_wikipedia_Prosecution_of_Donald_Trump_in_New_York->setTitleLink('https://en.wikipedia.org/wiki/Prosecution_of_Donald_Trump_in_New_York');
$div_wikipedia_Prosecution_of_Donald_Trump_in_New_York->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Federal_prosecution_of_Donald_Trump_classified_documents_case = new WikipediaContentSection();
$div_wikipedia_Federal_prosecution_of_Donald_Trump_classified_documents_case->setTitleText('Federal prosecution of Donald Trump classified documents case');
$div_wikipedia_Federal_prosecution_of_Donald_Trump_classified_documents_case->setTitleLink('https://en.wikipedia.org/wiki/Federal_prosecution_of_Donald_Trump_(classified_documents_case)');
$div_wikipedia_Federal_prosecution_of_Donald_Trump_classified_documents_case->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Federal_prosecution_of_Donald_Trump_election_obstruction_case = new WikipediaContentSection();
$div_wikipedia_Federal_prosecution_of_Donald_Trump_election_obstruction_case->setTitleText('Federal prosecution of Donald Trump election obstruction case');
$div_wikipedia_Federal_prosecution_of_Donald_Trump_election_obstruction_case->setTitleLink('https://en.wikipedia.org/wiki/Federal_prosecution_of_Donald_Trump_(election_obstruction_case)');
$div_wikipedia_Federal_prosecution_of_Donald_Trump_election_obstruction_case->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Georgia_election_racketeering_prosecution = new WikipediaContentSection();
$div_wikipedia_Georgia_election_racketeering_prosecution->setTitleText('Georgia election racketeering prosecution');
$div_wikipedia_Georgia_election_racketeering_prosecution->setTitleLink('https://en.wikipedia.org/wiki/Georgia_election_racketeering_prosecution');
$div_wikipedia_Georgia_election_racketeering_prosecution->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_New_York_civil_investigation_of_The_Trump_Organization = new WikipediaContentSection();
$div_wikipedia_New_York_civil_investigation_of_The_Trump_Organization->setTitleText('New York civil investigation of The Trump Organization');
$div_wikipedia_New_York_civil_investigation_of_The_Trump_Organization->setTitleLink('https://en.wikipedia.org/wiki/New_York_civil_investigation_of_The_Trump_Organization');
$div_wikipedia_New_York_civil_investigation_of_The_Trump_Organization->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_False_or_misleading_statements_by_Donald_Trump = new WikipediaContentSection();
$div_wikipedia_False_or_misleading_statements_by_Donald_Trump->setTitleText('False or misleading statements by Donald Trump');
$div_wikipedia_False_or_misleading_statements_by_Donald_Trump->setTitleLink('https://en.wikipedia.org/wiki/False_or_misleading_statements_by_Donald_Trump');
$div_wikipedia_False_or_misleading_statements_by_Donald_Trump->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_List_of_conspiracy_theories_promoted_by_Donald_Trump = new WikipediaContentSection();
$div_wikipedia_List_of_conspiracy_theories_promoted_by_Donald_Trump->setTitleText('List of conspiracy theories promoted by Donald Trump');
$div_wikipedia_List_of_conspiracy_theories_promoted_by_Donald_Trump->setTitleLink('https://en.wikipedia.org/wiki/List_of_conspiracy_theories_promoted_by_Donald_Trump');
$div_wikipedia_List_of_conspiracy_theories_promoted_by_Donald_Trump->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Racial_views_of_Donald_Trump = new WikipediaContentSection();
$div_wikipedia_Racial_views_of_Donald_Trump->setTitleText('Racial views of Donald Trump');
$div_wikipedia_Racial_views_of_Donald_Trump->setTitleLink('https://en.wikipedia.org/wiki/Racial_views_of_Donald_Trump');
$div_wikipedia_Racial_views_of_Donald_Trump->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Donald_Trump_sexual_misconduct_allegations = new WikipediaContentSection();
$div_wikipedia_Donald_Trump_sexual_misconduct_allegations->setTitleText('Donald Trump sexual misconduct allegations');
$div_wikipedia_Donald_Trump_sexual_misconduct_allegations->setTitleLink('https://en.wikipedia.org/wiki/Donald_Trump_sexual_misconduct_allegations');
$div_wikipedia_Donald_Trump_sexual_misconduct_allegations->content = <<<HTML
	<p>
	</p>
	HTML;



$div_Donald_Trump_Built_a_National_Debt_So_Big = new WebsiteContentSection();
$div_Donald_Trump_Built_a_National_Debt_So_Big->setTitleText("Donald Trump Built a National Debt So Big (Even Before the Pandemic) That It’ll Weigh Down the Economy for Years");
$div_Donald_Trump_Built_a_National_Debt_So_Big->setTitleLink("https://www.propublica.org/article/national-debt-trump");
$div_Donald_Trump_Built_a_National_Debt_So_Big->content = <<<HTML
	<p>[By $ProPublica] The “King of Debt” promised to reduce the national debt — then his tax cuts made it surge.
	Add in the pandemic, and he oversaw the third-biggest deficit increase of any president.</p>
	HTML;



$div_wikipedia_List_of_Republicans_who_oppose_the_Donald_Trump_2024_presidential_campaign = new WikipediaContentSection();
$div_wikipedia_List_of_Republicans_who_oppose_the_Donald_Trump_2024_presidential_campaign->setTitleText("List of Republicans who oppose the Donald Trump 2024 presidential campaign");
$div_wikipedia_List_of_Republicans_who_oppose_the_Donald_Trump_2024_presidential_campaign->setTitleLink("https://en.wikipedia.org/wiki/List_of_Republicans_who_oppose_the_Donald_Trump_2024_presidential_campaign");
$div_wikipedia_List_of_Republicans_who_oppose_the_Donald_Trump_2024_presidential_campaign->content = <<<HTML
	<p>
	</p>
	HTML;



$div_wikipedia_List_of_former_Trump_administration_officials_who_endorsed_Joe_Biden = new WikipediaContentSection();
$div_wikipedia_List_of_former_Trump_administration_officials_who_endorsed_Joe_Biden->setTitleText("List of former Trump administration officials who endorsed Joe Biden");
$div_wikipedia_List_of_former_Trump_administration_officials_who_endorsed_Joe_Biden->setTitleLink("https://en.wikipedia.org/wiki/List_of_former_Trump_administration_officials_who_endorsed_Joe_Biden");
$div_wikipedia_List_of_former_Trump_administration_officials_who_endorsed_Joe_Biden->content = <<<HTML
	<p>
	</p>
	HTML;


$div_wikipedia_Rhetoric_of_Donald_Trump = new WikipediaContentSection();
$div_wikipedia_Rhetoric_of_Donald_Trump->setTitleText("Rhetoric of Donald Trump");
$div_wikipedia_Rhetoric_of_Donald_Trump->setTitleLink("https://en.wikipedia.org/wiki/Rhetoric_of_Donald_Trump");
$div_wikipedia_Rhetoric_of_Donald_Trump->content = <<<HTML
	<p>
	</p>
	HTML;






$page->parent('list_of_people.html');

$page->body($div_A_Call_for_Change);

$page->body($div_introduction);
$page->related_tag("Donald Trump: Research", $div_Donald_Trump_Research);
$page->related_tag("Donald Trump");
$page->body($div_AI_image);


$page->body($div_Donald_Trump_Built_a_National_Debt_So_Big);

$page->body($div_wikipedia_Donald_Trump);
$page->body($div_wikipedia_Personal_and_business_legal_affairs_of_Donald_Trump);
$page->body($div_wikipedia_Trump_University);
$page->body($div_wikipedia_Donald_Trump_photo_op_at_St_John_s_Church);
$page->body($div_wikipedia_Trump_administration_family_separation_policy);
$page->body($div_wikipedia_Trump_administration_political_interference_with_science_agencies);
$page->body($div_wikipedia_Trump_Ukraine_scandal);
$page->body($div_wikipedia_First_impeachment_of_Donald_Trump);
$page->body($div_wikipedia_First_impeachment_trial_of_Donald_Trump);
$page->body($div_wikipedia_Big_lie);
$page->body($div_wikipedia_Attempts_to_overturn_the_2020_United_States_presidential_election);
$page->body($div_wikipedia_Election_denial_movement);
$page->body($div_wikipedia_January_6_United_States_Capitol_attack);
$page->body($div_wikipedia_Timeline_of_the_January_6_United_States_Capitol_attack);
$page->body($div_wikipedia_Second_impeachment_of_Donald_Trump);
$page->body($div_wikipedia_Second_impeachment_trial_of_Donald_Trump);
$page->body($div_wikipedia_FBI_investigation_into_Donald_Trump_s_handling_of_government_documents);
$page->body($div_wikipedia_Smith_special_counsel_investigation);
$page->body($div_wikipedia_FBI_search_of_Mar_a_Lago);
$page->body($div_wikipedia_United_States_House_Select_Committee_on_the_January_6_Attack);
$page->body($div_wikipedia_Prosecution_of_Donald_Trump_in_New_York);
$page->body($div_wikipedia_Federal_prosecution_of_Donald_Trump_classified_documents_case);
$page->body($div_wikipedia_Federal_prosecution_of_Donald_Trump_election_obstruction_case);
$page->body($div_wikipedia_Georgia_election_racketeering_prosecution);
$page->body($div_wikipedia_New_York_civil_investigation_of_The_Trump_Organization);
$page->body($div_wikipedia_False_or_misleading_statements_by_Donald_Trump);
$page->body($div_wikipedia_List_of_conspiracy_theories_promoted_by_Donald_Trump);
$page->body($div_wikipedia_Racial_views_of_Donald_Trump);
$page->body($div_wikipedia_Donald_Trump_sexual_misconduct_allegations);
$page->body($div_wikipedia_List_of_Republicans_who_oppose_the_Donald_Trump_2024_presidential_campaign);
$page->body($div_wikipedia_List_of_former_Trump_administration_officials_who_endorsed_Joe_Biden);
$page->body($div_wikipedia_Rhetoric_of_Donald_Trump);
