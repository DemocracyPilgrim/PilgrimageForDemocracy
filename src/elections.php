<?php
$page = new Page();
$page->h1("4: Elections");
$page->tags("Topic portal");
$page->keywords("Elections", "Elections: Duverger Syndrome", "Elections: Solution", "election", "elections", 'electoral');
$page->stars(1);
$page->viewport_background("/free/elections.png");

$page->snp("description", "Individuals choosing their leaders and representatives.");
$page->snp("image",       "/free/elections.1200-630.png");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Putting the power back into the hands of $individuals,
	$society selects its leaders and representatives through elections.</p>
	HTML;



$list_Duverger_Syndrome = ListOfPeoplePages::WithTags("Elections: Duverger Syndrome");
$print_list_Duverger_Syndrome = $list_Duverger_Syndrome->print();

$div_list_Duverger_Syndrome = new ContentSection();
$div_list_Duverger_Syndrome->content = <<<HTML
	<h3>Duverger Syndrome</h3>

	<p>The most pressing need for our $democracies is to understand what the ${'Duverger Syndrome'} is,
	its root causes and how to overcome it.</p>

	$print_list_Duverger_Syndrome
	HTML;

$list_Elections_Solution = ListOfPeoplePages::WithTags("Elections: Solution");
$print_list_Elections_Solution = $list_Elections_Solution->print();

$div_list_Elections_Solution = new ContentSection();
$div_list_Elections_Solution->content = <<<HTML
	<h3>Solutions</h3>

	<p>The remedy to the ${'Duverger Syndrome'} are well known:</p>

	$print_list_Elections_Solution
	HTML;




$list_Elections = ListOfPeoplePages::WithTags("Elections");
$print_list_Elections = $list_Elections->print();

$div_list_Elections = new ContentSection();
$div_list_Elections->content = <<<HTML
	<h3>Other related content</h3>

	$print_list_Elections
	HTML;


$div_wikipedia_Election = new WikipediaContentSection();
$div_wikipedia_Election->setTitleText("Election");
$div_wikipedia_Election->setTitleLink("https://en.wikipedia.org/wiki/Election");
$div_wikipedia_Election->content = <<<HTML
	<p>An election is a formal group decision-making process by which a population chooses an individual or multiple individuals to hold public office.</p>
	HTML;


$page->parent('fourth_level_of_democracy.html');
$page->previous("institutions.html");
$page->next("information.html");


$page->body($div_introduction);


$page->body('duverger_syndrome.html');
$page->body($div_list_Duverger_Syndrome);

$page->body('voting_methods.html');
$page->body($div_list_Elections_Solution);

$page->body($div_list_Elections);


$page->body($div_wikipedia_Election);
