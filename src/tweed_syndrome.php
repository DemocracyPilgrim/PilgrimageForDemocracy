<?php
require_once 'section/tweed.php';

$page = new Page();
$page->h1("Tweed Syndrome");
$page->viewport_background('/free/tweed_syndrome.png');
$page->stars(1);
$page->keywords("Tweed Syndrome");
$page->tags("Danger");

$page->snp("description", "The corruption of democratic processes for private gain and sustained power");
$page->snp("image",       "/free/tweed_syndrome.1200-630.png");

$page->preview( <<<HTML
	<p>Tweed Syndrome is the deliberate undermining of democratic institutions and processes
	through corruption to secure and perpetuate power and profit.</p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Tweed Syndrome is the deliberate undermining of democratic institutions and processes
	through corruption to secure and perpetuate power and profit.</p>
	HTML;


$page->parent('dangers.html');

$page->body($div_introduction);


tweed_syndrome_article_menu($page);
$page->related_tag("Tweed Syndrome");
