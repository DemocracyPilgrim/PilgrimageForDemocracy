<?php
$page = new Page();
$page->h1("China (People's Republic of China) and Taiwan (Republic of China)");
$page->stars(0);
$page->keywords('China and Taiwan');

$page->preview( <<<HTML
	<p>Relationship between the two Chinas: the Republic of China (Taiwan), and the People's Republic of China,
	and the ways the latter is trying to take over the former...</p>
	HTML );


$page->snp('description', "A tale of two Chinas.");
//$page->snp('image', "/copyrighted/");

$r1 = $page->ref('https://www.taipeitimes.com/News/front/archives/2023/07/01/2003802464', 'Blockade would cost world US$2.7tn: report');
$r2 = $page->ref('https://www.visionofhumanity.org/assessing-the-global-economic-ramifications-of-a-chinese-blockade-on-taiwan/', 'Assessing the Global Economic Ramifications of a Chinese Blockade on Taiwan');
$r3 = $page->ref('https://www.taipeitimes.com/News/front/archives/2023/10/30/2003808419', 'Official warns of Fujian residence risks');


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>This article is about the two Chinas: the People's Republic of China (PRC, commonly simply named: $China),
	and the Republic of China (ROC, commonly known as $Taiwan).
	In particular, the article shall cover the different ways China attempts to gain control of Taiwan.</p>

	<p>Despite China's insistence over its "One China policy", there exist, de facto, two Chinas: the ROC and the PRC.
	Taiwanese people would be willing to change the official name of their country to "Republic of Taiwan",
	such that there would be only one China left,
	but it cannot be done for fear of triggering a full on war with the PRC.</p>

	<p>In 2014, ROC president Ma Ying-Jeou almost signed a trade deal with the PRC,
	that would ultimately have handed the keys of Taiwan's economy to China.
	Only mass pro-democracy protests in Taiwan, with students occupying the legislature,
	prevented that law to be activated.</p>

	<p>As of 2023, the CCP is trying to provide favourable deals to Taiwanese business people
	if they invest in China, allowing them to buy property there.
	However, China is reported to collect information on their movements, accounts and purchases, etc. $r3</p>
	HTML;


$div_China_and_the_economy_of_Taiwan = new ContentSection();
$div_China_and_the_economy_of_Taiwan->content = <<<HTML
	<h3>China and the economy of Taiwan</h3>

	<p>The Chinese Communist Party is trying in every possible ways to take over Taiwan.
	The CCP is developing the means take over Taiwan through a military invasion if necessary,
	but its preference would be to do it more peacefully, through economic annexation.</p>


	HTML;


$div_blockade = new ContentSection();
$div_blockade->content = <<<HTML
	<h3>PRC's blockade of Taiwan</h3>

	<p>Any attempt by the $PRC to militarily invade $Taiwan would certainly include and attempt to blockade the island of Taiwan
	so as to prevent military aid and other resourced to be shipped to Taiwan from allied countries.</p>

	<p>A July 2023 report estimated that a blockade of Taiwan would cost the world economy US$2.7tn. ${r1} $r2</p>
	HTML;


$div_wikipedia_Political_status_of_Taiwan = new WikipediaContentSection();
$div_wikipedia_Political_status_of_Taiwan->setTitleText('Political status of Taiwan');
$div_wikipedia_Political_status_of_Taiwan->setTitleLink('https://en.wikipedia.org/wiki/Political_status_of_Taiwan');
$div_wikipedia_Political_status_of_Taiwan->content = <<<HTML
	<p>The controversy surrounding the political status of Taiwan or the Taiwan issue
	is a result of World War II, the second phase of the Chinese Civil War (1945–1949), and the Cold War.</p>
	HTML;

$div_wikipedia_Taiwan_China = new WikipediaContentSection();
$div_wikipedia_Taiwan_China->setTitleText('"Taiwan, China"');
$div_wikipedia_Taiwan_China->setTitleLink('https://en.wikipedia.org/wiki/Taiwan,_China');
$div_wikipedia_Taiwan_China->content = <<<HTML
	<p>"Taiwan, China", "Taiwan, Province of China", and "Taipei, China"
	are controversial political terms that claim Taiwan and its associated territories as a province or territory of "China".</p>
	HTML;

$page->parent('world.html');
$page->template("stub");

$page->body($div_introduction);


$page->body($div_China_and_the_economy_of_Taiwan);
$page->body($div_blockade);

$page->body('chinese_expansionism.html');
$page->body('prc_china.html');
$page->body('military_and_security_developments_involving_the_people_s_republic_of_china_2023_dod_report.html');
$page->body('taiwan.html');
$page->body('one_country_two_systems.html');

$page->body($div_wikipedia_Political_status_of_Taiwan);
$page->body($div_wikipedia_Taiwan_China);
