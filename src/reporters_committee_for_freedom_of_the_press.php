<?php
$page = new OrganisationPage();
$page->h1("Reporters Committee for Freedom of the Press");
$page->viewport_background("");
$page->keywords("Reporters Committee for Freedom of the Press");
$page->stars(0);
$page->tags("Organisation", "Information: Media", "Freedom of the Press");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;




$div_Reporters_Committee_for_Freedom_of_the_Press = new WebsiteContentSection();
$div_Reporters_Committee_for_Freedom_of_the_Press->setTitleText("Reporters Committee for Freedom of the Press ");
$div_Reporters_Committee_for_Freedom_of_the_Press->setTitleLink("https://www.rcfp.org/");
$div_Reporters_Committee_for_Freedom_of_the_Press->content = <<<HTML
	<p>The Reporters Committee for Freedom of the Press provides pro bono legal representation, amicus curiae support, and other legal resources to protect First Amendment freedoms and the newsgathering rights of journalists.</p>
	HTML;



$div_wikipedia_Reporters_Committee_for_Freedom_of_the_Press = new WikipediaContentSection();
$div_wikipedia_Reporters_Committee_for_Freedom_of_the_Press->setTitleText("Reporters Committee for Freedom of the Press");
$div_wikipedia_Reporters_Committee_for_Freedom_of_the_Press->setTitleLink("https://en.wikipedia.org/wiki/Reporters_Committee_for_Freedom_of_the_Press");
$div_wikipedia_Reporters_Committee_for_Freedom_of_the_Press->content = <<<HTML
	<p>The Reporters Committee for Freedom of the Press (RCFP) is a nonprofit organization based in Washington, D.C., that provides pro bono legal services and resources to and on behalf of journalists. The organization pursues litigation, offers direct representation, submits amicus curiae briefs, and provides other legal assistance on matters involving the First Amendment, press freedom, freedom of information, and court access issues.</p>
	HTML;


$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Reporters Committee for Freedom of the Press");

$page->body($div_Reporters_Committee_for_Freedom_of_the_Press);
$page->body($div_wikipedia_Reporters_Committee_for_Freedom_of_the_Press);
