<?php
$page = new Page();
$page->h1("World Inequality Database");
$page->keywords("World Inequality Database");
$page->tags("Organisation", "Taxes");
$page->stars(0);

//$page->snp("description", "");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref('', '');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>The World Inequality Database is published by the ${'World Inequality Lab'}.</p>

	<p>See also: $poverty, $taxes.</p>
	HTML;



$div_World_Inequality_Database_website = new WebsiteContentSection();
$div_World_Inequality_Database_website->setTitleText("World Inequality Database website ");
$div_World_Inequality_Database_website->setTitleLink("https://wid.world/");
$div_World_Inequality_Database_website->content = <<<HTML
	<p>During the past fifteen years, the renewed interest for the long-run evolution of income and wealth inequality
	gave rise to a flourishing literature.
	In particular, a succession of studies has constructed top income share series for a large number of countries
	(see Thomas Piketty 2001, 2003, T. Piketty and Emmanuel Saez 2003,
	and the two multi-country volumes on top incomes edited by Anthony B. Atkinson and T. Piketty 2007, 2010;
	see also A. B. Atkinson et al. 2011 and Facundo Alvaredo et al. 2013 for surveys of this literature).
	These projects generated a large volume of data, intended as a research resource for further analysis,
	as well as a source to inform the public debate on income inequality.
	To a large extent, this literature follows the pioneering work of Simon Kuznets 1953, and A. B. Atkinson and Alan Harrison 1978,
	and extends it to many more countries and years.</p>
	HTML;



$div_World_Inequality_Report_2022 = new WebsiteContentSection();
$div_World_Inequality_Report_2022->setTitleText("World Inequality Report 2022 ");
$div_World_Inequality_Report_2022->setTitleLink("https://wir2022.wid.world/");
$div_World_Inequality_Report_2022->content = <<<HTML
	<p>This report presents the most up-to-date
	synthesis of international research efforts
	to track global inequalities. The data and
	analysis presented here are based on the work
	of more than 100 researchers over four years,
	located on all continents, contributing to
	the World Inequality Database (WID.world),
	maintained by the World Inequality Lab. This
	vast network collaborates with statistical
	institutions, tax authorities, universities and
	international organizations, to harmonize,
	analyze and disseminate comparable
	international inequality data.</p>
	HTML;



$div_wikipedia_World_Inequality_Database = new WikipediaContentSection();
$div_wikipedia_World_Inequality_Database->setTitleText("World Inequality Database");
$div_wikipedia_World_Inequality_Database->setTitleLink("https://en.wikipedia.org/wiki/World_Inequality_Database");
$div_wikipedia_World_Inequality_Database->content = <<<HTML
	<p>World Inequality Database (WID), previously The World Wealth and Income Database, also known as WID.world,
	is an extensive, open and accessible database "on the historical evolution of the world distribution of income and wealth,
	both within countries and between countries".</p>
	HTML;


$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);

$page->body($div_World_Inequality_Database_website);
$page->body($div_World_Inequality_Report_2022);

$page->body($div_wikipedia_World_Inequality_Database);
