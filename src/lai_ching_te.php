<?php
$page = new Page();
$page->h1("William Lai (Lai Ching-te 賴清德)");
$page->alpha_sort("Lai, William");
$page->tags("Person", "Taiwan");
$page->keywords("Lai Ching-te");
$page->stars(0);

//$page->snp("description", "");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref('', '');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;



$div_youtube_President_Lai_inauguration_ceremony = new YoutubeContentSection();
$div_youtube_President_Lai_inauguration_ceremony->setTitleText("President Lai inauguration ceremony (Chinese)");
$div_youtube_President_Lai_inauguration_ceremony->setTitleLink("https://www.youtube.com/watch?v=vyZNbRkgyr0");
$div_youtube_President_Lai_inauguration_ceremony->content = <<<HTML
	HTML;



$div_wikipedia_Lai_Ching_te = new WikipediaContentSection();
$div_wikipedia_Lai_Ching_te->setTitleText("Lai Ching te");
$div_wikipedia_Lai_Ching_te->setTitleLink("https://en.wikipedia.org/wiki/Lai_Ching-te");
$div_wikipedia_Lai_Ching_te->content = <<<HTML
	<p>Lai Ching-te (賴清德; born 6 October 1959), also known as William Lai, is a Taiwanese politician and former physician
	who is serving as the 8th president of the Republic of China (Taiwan) since May 2024.
	He is the third member from the Democratic Progressive Party (DPP) to assume the office of the President,
	and also the third incumbent vice president of Taiwan to succeed presidency,
	and the first of which to assume the office through election instead of immediate succession.
	He has also served as the chair of the DPP since 2023.</p>
	HTML;


$page->parent('list_of_people.html');
$page->parent('taiwan.html');
$page->parent('project/taiwan_democracy.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_youtube_President_Lai_inauguration_ceremony);
$page->body($div_wikipedia_Lai_Ching_te);
