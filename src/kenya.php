<?php
$page = new CountryPage('Kenya');
$page->h1('Kenya');
$page->tags("Country");
$page->keywords('Kenya');
$page->stars(0);

$page->snp('description', '51,5 million inhabitants.');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Kenya = new WikipediaContentSection();
$div_wikipedia_Kenya->setTitleText('Kenya');
$div_wikipedia_Kenya->setTitleLink('https://en.wikipedia.org/wiki/Kenya');
$div_wikipedia_Kenya->content = <<<HTML
	<p>Kenya, officially the Republic of Kenya, is a country in East Africa.</p>
	HTML;

$div_wikipedia_Human_rights_in_Kenya = new WikipediaContentSection();
$div_wikipedia_Human_rights_in_Kenya->setTitleText('Human rights in Kenya');
$div_wikipedia_Human_rights_in_Kenya->setTitleLink('https://en.wikipedia.org/wiki/Human_rights_in_Kenya');
$div_wikipedia_Human_rights_in_Kenya->content = <<<HTML
	<p>Human rights in Kenya internationally maintain a variety of mixed opinions;
	specifically, political freedoms are highlighted as being poor and homosexuality remains a crime.</p>
	HTML;

$div_wikipedia_Human_trafficking_in_Kenya = new WikipediaContentSection();
$div_wikipedia_Human_trafficking_in_Kenya->setTitleText('Human trafficking in Kenya');
$div_wikipedia_Human_trafficking_in_Kenya->setTitleLink('https://en.wikipedia.org/wiki/Human_trafficking_in_Kenya');
$div_wikipedia_Human_trafficking_in_Kenya->content = <<<HTML
	<p>The Government of Kenya does not fully comply with the minimum standards for the elimination of human trafficking.
	In 2008 it was reported that Kenya's anti-trafficking efforts improved markedly over the reporting period,
	particularly through greater investigations of suspected trafficking cases.
	$US State Department's Office to Monitor and Combat Trafficking in Persons placed the country in "Tier 2" in 2017.
	Their efforts remain uncoordinated and lack strong oversight, creating an environment conducive to trafficking.</p>
	HTML;


$page->parent('world.html');
$page->template("stub");
$page->body($div_introduction);
$page->body('Country indices');

$page->body($div_wikipedia_Kenya);
$page->body($div_wikipedia_Human_rights_in_Kenya);
$page->body($div_wikipedia_Human_trafficking_in_Kenya);
