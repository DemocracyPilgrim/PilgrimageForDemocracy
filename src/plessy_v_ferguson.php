<?php
$page = new Page();
$page->h1("Plessy v. Ferguson (1896)");
$page->tags("Institutions: Judiciary", "SCOTUS Landmark Decision", "USA", "Racism");
$page->keywords("Plessy v. Ferguson");
$page->stars(1);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>"Plessy v. Ferguson" is a 1896 landmark decision by the ${'Supreme Court of the United States'}.</p>

	<p>This decision upheld the constitutionality of racial segregation laws, establishing the "separate but equal" doctrine.</p>

	<p>This decision legitimized segregation and contributed to the Jim Crow era, which saw widespread discrimination against African Americans.
	It was ultimately overturned by ${'Brown v. Board of Education'} (1954).</p>

	<p>Supporters of the decision argued that it protected the rights of states to regulate their own affairs
	and did not necessarily imply that one race was superior to another.</p>
	HTML;

$div_wikipedia_Plessy_v_Ferguson = new WikipediaContentSection();
$div_wikipedia_Plessy_v_Ferguson->setTitleText("Plessy v Ferguson");
$div_wikipedia_Plessy_v_Ferguson->setTitleLink("https://en.wikipedia.org/wiki/Plessy_v._Ferguson");
$div_wikipedia_Plessy_v_Ferguson->content = <<<HTML
	<p>Plessy v. Ferguson, 163 U.S. 537 (1896), was a landmark U.S. Supreme Court decision
	ruling that racial segregation laws did not violate the U.S. Constitution as long as the facilities for people of color
	were equal in quality to those of white people, a doctrine that came to be known as "separate but equal".
	The decision legitimized the many state "Jim Crow laws" re-establishing racial segregation that had been passed in the American South
	after the end of the Reconstruction era in 1877.
	Such legally enforced segregation in the South lasted into the 1960s.</p>
	HTML;


$page->parent('supreme_court_of_the_united_states.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_wikipedia_Plessy_v_Ferguson);
