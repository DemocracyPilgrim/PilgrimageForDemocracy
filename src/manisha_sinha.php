<?php
$page = new Page();
$page->h1("Manisha Sinha");
$page->alpha_sort("Sinha, Manisha");
$page->tags("Person", "USA", "Historian");
$page->keywords("Manisha Sinha");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Manisha Sinha is an $American historian</p>

	<p>She is the author of the books:</p>

	<ul>
		<li>The Counterrevolution of Slavery: Politics and Ideology in Antebellum South Carolina (2000)</li>
		<li>The Slave's Cause: A History of Abolition New Haven (2016)</li>
		<li>The Rise and Fall of the Second American Republic: Reconstruction, 1860-1920 (2024)</li>
	</ul>

	<p>The author discussed her latest book on the podcast "We the People" by the ${'National Constitution Center'} on August 16 2024.</p>
	HTML;

$div_wikipedia_Manisha_Sinha = new WikipediaContentSection();
$div_wikipedia_Manisha_Sinha->setTitleText("Manisha Sinha");
$div_wikipedia_Manisha_Sinha->setTitleLink("https://en.wikipedia.org/wiki/Manisha_Sinha");
$div_wikipedia_Manisha_Sinha->content = <<<HTML
	<p>Manisha Sinha is an Indian-born American historian, and the Draper Chair in American History at the University of Connecticut.
	She is the author of The Slave's Cause: A History of Abolition (2016), which won the Frederick Douglass Book Prize.</p>
	HTML;


$page->parent('list_of_people.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_wikipedia_Manisha_Sinha);
