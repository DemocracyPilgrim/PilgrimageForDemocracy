<?php
$page = new VideoPage();
$page->h1("National Constitution Center: A Conversation With Justice Neil Gorsuch on ‘The Human Toll of Too Much Law’");
$page->tags("Video: Podcast", "Podcast: We the People", "Neil Gorsuch", "Institutions: Legislative", "lawmaking", "Size of Government");
$page->keywords("National Constitution Center: A Conversation With Justice Neil Gorsuch on ‘The Human Toll of Too Much Law’");
$page->stars(1);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>The ${'National Constitution Center'} invites ${'Neil Gorsuch'} to talk about his book:
	"Over Ruled: The Human Toll of Too Much Law".</p>
	HTML;


$div_A_Conversation_With_Justice_Neil_Gorsuch_on_The_Human_Toll_of_Too_Much_Law = new WebsiteContentSection();
$div_A_Conversation_With_Justice_Neil_Gorsuch_on_The_Human_Toll_of_Too_Much_Law->setTitleText("A Conversation With Justice Neil Gorsuch on ‘The Human Toll of Too Much Law’");
$div_A_Conversation_With_Justice_Neil_Gorsuch_on_The_Human_Toll_of_Too_Much_Law->setTitleLink("https://constitutioncenter.org/news-debate/podcasts/a-conversation-with-justice-neil-gorsuch-on-the-human-toll-of-too-much-law");
$div_A_Conversation_With_Justice_Neil_Gorsuch_on_The_Human_Toll_of_Too_Much_Law->content = <<<HTML
	<p>On September 17, 2024, the Honorable Neil M. Gorsuch, associate justice of the U.S. Supreme Court and NCC honorary co-chair, and his co-author and former law clerk Janie Nitze, joined Jeffrey Rosen for an America’s Town Hall program in celebration of Constitution Day 2024 and the release of their latest book, Over Ruled: The Human Toll of Too Much Law.</p>
	HTML;


$page->parent('list_of_videos.html');
$page->template("stub");
$page->body($div_introduction);

$page->body($div_A_Conversation_With_Justice_Neil_Gorsuch_on_The_Human_Toll_of_Too_Much_Law);
$page->body('over_ruled_the_human_toll_of_too_much_law.html');
