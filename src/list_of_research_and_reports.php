<?php
$page = new Page();
$page->h1('List of research and reports');
$page->keywords("Report", "Research");
$page->stars(0);

$page->preview( <<<HTML
	<p>Research for peace, democracy and social justice.</p>
	HTML );


$page->snp('description', "Research for peace, democracy and social justice.");
//$page->snp('image', "/copyrighted/");



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Research for peace, democracy and social justice.</p>
	HTML;

$list = new ListOfPages();
$list->add('military_and_security_developments_involving_the_people_s_republic_of_china_2023_dod_report.html');
$list->add('the_american_journalist_under_attack_media_trust_and_democracy.html');
$list->add('the_hidden_dimension_in_democracy.html');
$list->add('truth_and_reality_with_chinese_characteristics.html');
$print_list = $list->print();


$list_Research_Report = ListOfPeoplePages::WithTags("Research", "Report");
$print_list_Research_Report = $list_Research_Report->print();

$div_list_Research_Report = new ContentSection();
$div_list_Research_Report->content = <<<HTML
	<h3>Related content</h3>

	$print_list
	$print_list_Research_Report
	HTML;




$page->parent('lists.html');
$page->template("stub");

$page->body($div_introduction);
$page->body($div_list_Research_Report);
