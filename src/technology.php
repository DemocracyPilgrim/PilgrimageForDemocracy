<?php
$page = new Page();
$page->h1("Technology");
$page->tags("Society", "Economy", "Science");
$page->keywords("Technology", "Technologies", "technology", "technologies");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>We aim to discuss technology as they play a role in $democracy and ${'social justice'}.</p>

	<p>Also, we can highlight the fact that despite our high level of technological advancement and sophistication,
	we are still facing huge $wealth disparities and profound $social $injustice.
	Why can't technology and access to technology be shared more equitably?
	Why can't it be used to solve critical problems.</p>

	<p>In addition, we can investigate how technology is being used by anti-democratic forces for nefarious purposes.</p>
	HTML;

$div_wikipedia_Technology = new WikipediaContentSection();
$div_wikipedia_Technology->setTitleText("Technology");
$div_wikipedia_Technology->setTitleLink("https://en.wikipedia.org/wiki/Technology");
$div_wikipedia_Technology->content = <<<HTML
	<p>Technology is the application of conceptual knowledge to achieve practical goals, especially in a reproducible way. The word technology can also mean the products resulting from such efforts, including both tangible tools such as utensils or machines, and intangible ones such as software. Technology plays a critical role in science, engineering, and everyday life.</p>
	HTML;


$page->parent('society.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag(array("Technology", "Technologies"));
$page->body($div_wikipedia_Technology);
