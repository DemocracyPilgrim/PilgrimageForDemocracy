<?php
$page = new PersonPage();
$page->h1("Sarah Longwell");
$page->alpha_sort("Longwell, Sarah");
$page->tags("Person", "Donald Trump", "Republican Party Evolution");
$page->keywords("Sarah Longwell");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Sarah_Longwell = new WikipediaContentSection();
$div_wikipedia_Sarah_Longwell->setTitleText("Sarah Longwell");
$div_wikipedia_Sarah_Longwell->setTitleLink("https://en.wikipedia.org/wiki/Sarah_Longwell");
$div_wikipedia_Sarah_Longwell->content = <<<HTML
	<p>Sarah Longwell is an American political strategist and publisher of the conservative news and opinion website The Bulwark. A member of the Republican Party, she is the founder of Republican Accountability (originally named Republican Voters Against Trump), which spent millions of dollars to defeat then-President Donald Trump in 2020. According to The New Yorker, Longwell has "dedicated her career to fighting Trump's takeover of her party".</p>
	HTML;


$page->parent('list_of_people.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Sarah Longwell");
$page->body($div_wikipedia_Sarah_Longwell);
