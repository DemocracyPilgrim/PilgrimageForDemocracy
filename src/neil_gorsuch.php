<?php
$page = new PersonPage();
$page->h1("Neil Gorsuch");
$page->alpha_sort("Gorsuch, Neil");
$page->tags("Person", "SCOTUS", "USA", "Institutions: Judiciary");
$page->keywords("Neil Gorsuch");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Neil_Gorsuch = new WikipediaContentSection();
$div_wikipedia_Neil_Gorsuch->setTitleText("Neil Gorsuch");
$div_wikipedia_Neil_Gorsuch->setTitleLink("https://en.wikipedia.org/wiki/Neil_Gorsuch");
$div_wikipedia_Neil_Gorsuch->content = <<<HTML
	<p>Neil McGill Gorsuch is an American jurist who serves as an associate justice of the Supreme Court of the United States. He was nominated by President Donald Trump on January 31, 2017, and has served since April 10, 2017.</p>
	HTML;


$page->parent('list_of_people.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Neil Gorsuch");
$page->body($div_wikipedia_Neil_Gorsuch);
