<?php
$page = new AwardPage();
$page->h1("Liberty Medal");
$page->tags("Award", "Individual: Liberties", "National Constitution Center");
$page->keywords("Liberty Medal", "Liberty Medal Recipient");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;



$div_Liberty_Medal = new WebsiteContentSection();
$div_Liberty_Medal->setTitleText("Liberty Medal");
$div_Liberty_Medal->setTitleLink("https://constitutioncenter.org/about/liberty-medal");
$div_Liberty_Medal->content = <<<HTML
	<p>The National Constitution Center’s annual Liberty Medal honors men and women of courage and conviction
	who strive to secure the blessings of liberty to people around the globe.</p>
	HTML;



$div_wikipedia_Liberty_Medal = new WikipediaContentSection();
$div_wikipedia_Liberty_Medal->setTitleText("Liberty Medal");
$div_wikipedia_Liberty_Medal->setTitleLink("https://en.wikipedia.org/wiki/Liberty_Medal");
$div_wikipedia_Liberty_Medal->content = <<<HTML
	<p>The Liberty Medal is an annual award administered by the National Constitution Center (NCC) of the United States to recognize leadership in the pursuit of freedom. It was founded by the Philadelphia Foundation. In 2006 an agreement was made with the National Constitution Center (NCC) that the NCC would take over the organizing, selecting and presenting of the award to recipients. Recipients are now chosen by the NCC and its board of trustees.</p>
	HTML;


$div_Liberty_Medal_Recipient = new ContentSection();
$div_Liberty_Medal_Recipient->content = <<<HTML
	<h3>Liberty Medal Recipients</h3>
	HTML;



$page->parent('list_of_awards.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_Liberty_Medal);

$page->related_tag("Liberty Medal");
$page->related_tag("Liberty Medal Recipient", $div_Liberty_Medal_Recipient);
$page->body($div_wikipedia_Liberty_Medal);
