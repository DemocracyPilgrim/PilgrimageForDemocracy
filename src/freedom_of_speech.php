<?php
$page = new Page();
$page->h1('Freedom of speech');
$page->stars(1);
$page->tags("Information: Discourse", "Individual: Liberties", "Political Discourse", "Society");
$page->keywords('Freedom of Speech', 'freedom of speech', 'free speech', 'Free Speech');

$page->preview( <<<HTML
	<p>Freedom of speech is not what people think it is...</p>
	HTML );

$page->snp('description', "Freedom of speech is not what people think it is...");
//$page->snp('image', "/copyrighted/");

$r1 = $page->ref("https://www.americanbar.org/groups/public_interest/election_law/american-democracy/resources/how-first-amendment-right-free-speech-intersect-democracy/", "How Does the First Amendment Right of Free Speech Intersect with Democracy?");


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Freedom of speech is often championed as a cornerstone of $democracy, but its complexities and limitations are frequently overlooked.
	This page aims to provide a nuanced understanding of this fundamental $right, clarifying common misconceptions and exploring its importance in a democratic society.
	Understanding the legal frameworks and practical challenges surrounding this right is essential.
	We'll delve into the legal boundaries of free speech, examine the threats to its protection,
	and explore how this crucial right safeguards democratic values and contributes to a just and equitable society.</p>

	<p>The advocacy of $organisations like ${'Reporters Without Borders'} is important in safeguarding freedom of speech and information.</p>
	HTML;


$div_Legal_Restrictions_to_Freedom_of_Speech = new ContentSection();
$div_Legal_Restrictions_to_Freedom_of_Speech->content = <<<HTML
	<h3>Legal Restrictions to Freedom of Speech</h3>

	<p>While freedom of speech is often hailed as a cornerstone of democratic societies, it's crucial to recognize that this right is not absolute.
	Legal restrictions on speech, though often controversial, exist to protect individuals and society from harm.
	These restrictions aim to balance the right to free expression with other fundamental values, such as public safety, national security,
	and the prevention of discrimination and hate speech.
	Understanding the legal limitations on freedom of speech is essential for navigating the complex interplay
	between individual rights and the common good in a democratic society.</p>

	<p>The First Amendment to the US $Constitution guarantees freedom of speech, yet, as $lawyer Jennifer Cook Purcell,
	writing for the ${'American Bar Association'}'s ${'Task Force for American Democracy'}, notes, certain categories of restrictions exist:" $r1</p>

	<ul>
		<li>Restrictions against libel;</li>
		<li>Restrictions against insurrection;</li>
		<li>Restrictions against contempt of court;</li>
		<li>Restrictions against inciting statements (especially when the record shows that the statements will be successful);</li>
		<li>Restrictions against true threats;</li>
		<li>Restrictions aimed at curtailing speech that is calculated to provoke a fight;</li>
		<li>Restrictions aimed at curtailing speech to ensure public safety and order (especially when the record that shows prior abusive conduct);</li>
		<li>Restrictions that regulate truthfulness in commercial speech;</li>
		<li>Restrictions that prohibit obscenity;</li>
		<li>Restrictions that protect the well-being, tranquility, and privacy of the home.</li>
	</ul>

	HTML;


$list_Freedom_of_Speech = ListOfPeoplePages::WithTags("Freedom of Speech");
$print_list_Freedom_of_Speech = $list_Freedom_of_Speech->print();

$div_list_Freedom_of_Speech = new ContentSection();
$div_list_Freedom_of_Speech->content = <<<HTML
	<h3>Related content</h3>

	$print_list_Freedom_of_Speech
	HTML;




$div_How_Does_the_First_Amendment_Right_of_Free_Speech_Intersect_with_Democracy = new WebsiteContentSection();
$div_How_Does_the_First_Amendment_Right_of_Free_Speech_Intersect_with_Democracy->setTitleText("How Does the First Amendment Right of Free Speech Intersect with Democracy? ");
$div_How_Does_the_First_Amendment_Right_of_Free_Speech_Intersect_with_Democracy->setTitleLink("https://www.americanbar.org/groups/public_interest/election_law/american-democracy/resources/how-first-amendment-right-free-speech-intersect-democracy/");
$div_How_Does_the_First_Amendment_Right_of_Free_Speech_Intersect_with_Democracy->content = <<<HTML
	<p>The First Amendment, which among other things protects an American’s right to free speech,
	was added to the Constitution as part of the Bill of Rights to strengthen an individual’s civil liberties against governmental interference.
	It is integral to a healthy democracy and has been historically valued by Americans and protected by American courts.</p>
	HTML;




$div_wikipedia_Freedom_of_speech = new WikipediaContentSection();
$div_wikipedia_Freedom_of_speech->setTitleText('Freedom of speech');
$div_wikipedia_Freedom_of_speech->setTitleLink('https://en.wikipedia.org/wiki/Freedom_of_speech');
$div_wikipedia_Freedom_of_speech->content = <<<HTML
	<p>Freedom of speech is a principle that supports the freedom of an individual or a community to articulate their opinions and ideas
	without fear of retaliation, censorship, or legal sanction.
	The right to freedom of expression has been recognised as a human right in the Universal Declaration of Human Rights
	and international human rights law by the United Nations.
	Many countries have constitutional law that protects free speech.</p>
	HTML;

$div_wikipedia_Freedom_of_speech_by_country = new WikipediaContentSection();
$div_wikipedia_Freedom_of_speech_by_country->setTitleText('Freedom of speech by country');
$div_wikipedia_Freedom_of_speech_by_country->setTitleLink('https://en.wikipedia.org/wiki/Freedom_of_speech_by_country');
$div_wikipedia_Freedom_of_speech_by_country->content = <<<HTML
	<p>The list is partially composed of the respective countries' government claims and does not fully reflect the de facto situation.</p>
	HTML;

$div_wikipedia_Speech_crimes = new WikipediaContentSection();
$div_wikipedia_Speech_crimes->setTitleText('Speech crimes');
$div_wikipedia_Speech_crimes->setTitleLink('https://en.wikipedia.org/wiki/Speech_crimes');
$div_wikipedia_Speech_crimes->content = <<<HTML
	<p>Speech crimes are certain kinds of speech that are criminalized by promulgated laws or rules.
	Criminal speech is a direct preemptive restriction on freedom of speech, and the broader concept of freedom of expression.</p>
	HTML;


$page->parent('freedom.html');

$page->body($div_introduction);
$page->body($div_Legal_Restrictions_to_Freedom_of_Speech);



$page->body($div_list_Freedom_of_Speech);

// External resources:
$page->body($div_How_Does_the_First_Amendment_Right_of_Free_Speech_Intersect_with_Democracy);

$page->body($div_wikipedia_Freedom_of_speech);
$page->body($div_wikipedia_Freedom_of_speech_by_country);
$page->body($div_wikipedia_Speech_crimes);
