<?php
$page = new CountryPage('Cuba');
$page->h1('Cuba');
$page->tags("Country");
$page->keywords('Cuba');
$page->stars(0);

$page->snp('description', '10,9 million inhabitants.');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Cuba = new WikipediaContentSection();
$div_wikipedia_Cuba->setTitleText('Cuba');
$div_wikipedia_Cuba->setTitleLink('https://en.wikipedia.org/wiki/Cuba');
$div_wikipedia_Cuba->content = <<<HTML
	<p>Cuba is one of a few extant Marxist–Leninist one-party socialist states,
	in which the role of the vanguard Communist Party is enshrined in the Constitution.
	Cuba has an authoritarian regime where political opposition is not permitted.
	Censorship of information is extensive and independent journalism is repressed in Cuba;
	Reporters Without Borders has characterized Cuba as one of the worst countries in the world for press freedom.</p>
	HTML;

$div_wikipedia_Human_rights_in_Cuba = new WikipediaContentSection();
$div_wikipedia_Human_rights_in_Cuba->setTitleText('Human rights in Cuba');
$div_wikipedia_Human_rights_in_Cuba->setTitleLink('https://en.wikipedia.org/wiki/Human_rights_in_Cuba');
$div_wikipedia_Human_rights_in_Cuba->content = <<<HTML
	<p>Human rights in Cuba are under the scrutiny of human rights organizations,
	which accuse the Cuban government of committing systematic human rights abuses against the Cuban people,
	including arbitrary imprisonment and unfair trials.</p>
	HTML;

$div_wikipedia_Censorship_in_Cuba = new WikipediaContentSection();
$div_wikipedia_Censorship_in_Cuba->setTitleText('Censorship in Cuba');
$div_wikipedia_Censorship_in_Cuba->setTitleLink('https://en.wikipedia.org/wiki/Censorship_in_Cuba');
$div_wikipedia_Censorship_in_Cuba->content = <<<HTML
	<p>The Inter American Press Association reported that "repression against independent journalists,
	mistreatment of jailed reporters, and very strict government surveillance
	limiting the people's access to alternative sources of information are continuing".</p>
	HTML;

$div_wikipedia_Cuban_dissident_movement = new WikipediaContentSection();
$div_wikipedia_Cuban_dissident_movement->setTitleText('Cuban dissident movement');
$div_wikipedia_Cuban_dissident_movement->setTitleLink('https://en.wikipedia.org/wiki/Cuban_dissident_movement');
$div_wikipedia_Cuban_dissident_movement->content = <<<HTML
	<p>The Cuban dissident movement is a political movement in Cuba whose aim is to replace the current government with a liberal democracy.</p>
	HTML;


$page->parent('world.html');
$page->template("stub");
$page->body($div_introduction);
$page->body('Country indices');

$page->body($div_wikipedia_Cuba);
$page->body($div_wikipedia_Human_rights_in_Cuba);
$page->body($div_wikipedia_Censorship_in_Cuba);
$page->body($div_wikipedia_Cuban_dissident_movement);
