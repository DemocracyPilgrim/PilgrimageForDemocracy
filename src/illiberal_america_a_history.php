<?php
$page = new Page();
$page->h1("Illiberal America: a History");
$page->tags("Book", "USA", "History");
$page->keywords("Illiberal America: A History");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>"Illiberal America: a History" is a book by ${'Steven Hahn'}.</p>

	<p>The author discussed his book on the podcast "We the People" by the ${'National Constitution Center'} on August 16 2024.</p>
	HTML;



$div_Publisher_Illiberal_America_a_History = new WebsiteContentSection();
$div_Publisher_Illiberal_America_a_History->setTitleText("Publisher - Illiberal America: a History ");
$div_Publisher_Illiberal_America_a_History->setTitleLink("https://wwnorton.com/books/9780393635928");
$div_Publisher_Illiberal_America_a_History->content = <<<HTML
	<p>If your reaction to the January 6, 2021, insurrection at the Capitol was to think, 'That’s not us,' think again:
	in Illiberal America, a Pulitzer Prize–winning historian uncovers a powerful illiberalism as deep-seated in the American past as the founding ideals.</p>

	<p>A storm of illiberalism, building in the United States for years, unleashed its destructive force in the Capitol insurrection of January 6, 2021.
	The attack on American democracy and images of mob violence led many to recoil, thinking “That’s not us.”
	But now we must think again, for Steven Hahn shows in his startling new history that illiberalism has deep roots in our past.
	To those who believe that the ideals announced in the Declaration of Independence set us apart as a nation,
	Hahn shows that Americans have long been animated by competing values, equally deep-seated,
	in which the illiberal will of the community overrides individual rights, and often protects itself by excluding perceived threats,
	whether on grounds of race, religion, gender, economic status, or ideology.</p>

	<p>Driven by popular movements and implemented through courts and legislation, illiberalism is part of the American bedrock.
	The United States was born a republic of loosely connected states and localities that demanded control of their domestic institutions, including slavery.
	As white settlement expanded west and immigration exploded in eastern cities, the democracy of the 1830s
	fueled expulsions of Blacks, Native Americans, Catholics, Mormons, and abolitionists.
	After the Civil War, southern states denied new constitutional guarantees of civil rights and enforced racial exclusions in everyday life.
	Illiberalism was modernized during the Progressive movement through advocates of eugenics
	who aimed to reduce the numbers of racial and ethnic minorities as well as the poor.
	The turmoil of the 1960s enabled George Wallace to tap local fears of unrest and build support outside the South, a politics adopted by Richard Nixon in 1968.
	Today, with illiberalism shaping elections and policy debates over guns, education, and abortion, it is urgent to understand its long history,
	and how that history bears on the present crisis.</p>
	HTML;



$page->parent('list_of_books.html');
$page->template("stub");
$page->body($div_introduction);

$page->body($div_Publisher_Illiberal_America_a_History);
