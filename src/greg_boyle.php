<?php
$page = new Page();
$page->h1("Greg Boyle");
$page->alpha_sort("Boyle, Greg");
$page->tags("Person", "Humanity");
$page->keywords("Greg Boyle", "Gregory Boyle");
$page->stars(2);

$page->snp("description", "Founder of Homeboy Industries");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p>Father Greg Boyle, a Jesuit priest and founder of Homeboy Industries,
	is widely recognized for his work in promoting peace and reconciliation within communities affected by gang violence.</p>
	HTML );

// $r1 = $page->ref('', '');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Father Greg Boyle is a Jesuit priest and the founder of ${'Homeboy Industries'},
	a non-profit organization in Los Angeles that provides job training, employment, and support services
	to formerly incarcerated individuals and gang members.
	He is widely recognized for his work in promoting peace and reconciliation within communities affected by gang violence.</p>

	<p>Father Greg Boyle is a remarkable individual who has dedicated his life to serving the most vulnerable communities
	and creating a more just and compassionate world.
	He stands as an example of the transformative power of love, empathy, and the unwavering belief in the inherent goodness of all people.</p>
	HTML;


$div_Early_Life_and_Calling = new ContentSection();
$div_Early_Life_and_Calling->content = <<<HTML
	<h3>Early Life and Calling</h3>

	<p>Born in Los Angeles in 1951, Boyle attended Loyola Marymount University and later studied theology at the Jesuit School of Theology in Berkeley, California.</p>

	<p>He was ordained as a Jesuit priest in 1984 and began working in the Dolores Mission in Boyle Heights, a predominantly Latino neighborhood in East Los Angeles.</p>
	</p>
	HTML;




$div_Homeboy_Industries_and_its_Impact = new ContentSection();
$div_Homeboy_Industries_and_its_Impact->content = <<<HTML
	<h3>Homeboy Industries and its Impact</h3>

	<p>Greg Boyle set out on a mission to Serve the Marginalized.
	He became deeply involved in the lives of the community, particularly those affected by gang violence and poverty.</p>

	<p>Boyle established the ${'Homeboy Industries'} program in 1988, inspired by the belief that everyone deserves a second chance.</p>

	<p>Homeboy Industries has grown into a multifaceted organization offering job training in various fields like baking, tattoo removal, landscaping, and more.</p>

	<p>It provides employment placement, counseling, case management, and life skills training,
	aiming to help individuals rebuild their lives and become contributing members of society.</p>

	<p>The organization has significantly impacted the lives of countless individuals,
	helping them avoid recidivism, break the cycle of violence, and find purpose and fulfillment.</p>
	HTML;



$div_Recognition_and_Legacy = new ContentSection();
$div_Recognition_and_Legacy->content = <<<HTML
	<h3>Recognition and Legacy</h3>

	<p>Boyle has received numerous awards and accolades for his work.</p>

	<p>He is the author of several books,
	including "<em>Tattoos on the Heart: The Power of Boundless Compassion</em>"
	and "<em>Barking to the Choir: The Power of Radical Kinship</em>,"
	which have become influential in promoting social justice and understanding.</p>
	HTML;



$div_Philosophy_and_Beliefs = new ContentSection();
$div_Philosophy_and_Beliefs->content = <<<HTML
	<h3>Philosophy and Beliefs</h3>

	<p>Boyle's work is rooted in the belief that everyone deserves compassion and dignity, regardless of their background or past mistakes.</p>

	<p>He emphasizes the power of radical kinship, recognizing the inherent worth of every human being.</p>

	<p>Boyle's work and writings have inspired countless individuals and organizations to address social injustices and create opportunities for the marginalized.</p>

	<p>He has become a leading voice in promoting peace, understanding, and reconciliation within communities affected by violence and poverty.</p>
	HTML;




$div_youtube_Compassion_and_Kinship_Fr_Gregory_Boyle_at_TEDxConejo_2012 = new YoutubeContentSection();
$div_youtube_Compassion_and_Kinship_Fr_Gregory_Boyle_at_TEDxConejo_2012->setTitleText("Compassion and Kinship: Fr Gregory Boyle at TEDxConejo 2012");
$div_youtube_Compassion_and_Kinship_Fr_Gregory_Boyle_at_TEDxConejo_2012->setTitleLink("https://www.youtube.com/watch?v=ipR0kWt1Fkc");
$div_youtube_Compassion_and_Kinship_Fr_Gregory_Boyle_at_TEDxConejo_2012->content = <<<HTML
	<p>Father Gregory Boyle, founder and executive director of Homeboy Industries, is an acknowledged expert on gangs, intervention and re-entry and today serves on the U.S. Attorney General's Defending Childhood Task Force.</p>
	<p>Born in Los Angeles, one of eight children, Fr. Greg worked in the family-owned dairy, loading milk trucks to earn his high school tuition. An enduring memory of that youthful time is when "...these weathered old truckers would come up to me, put their arms around me and point at my father in the distance, on the loading dock, and say, 'Your dad is a great man.'" Lessons from that first job apply at Homeboy Industries today where employees come to change for themselves and their children.</p>
	<p>Homeboy Industries traces its roots to "Jobs For A Future" (JFF), created in 1988 by Boyle at Dolores Mission. To address the escalating problems of gang-involved youth, he and the community developed an elementary school, day care program and sought legitimate employment for young people.</p>
	<p>Boyle serves on the National Gang Center Advisory Board (Bureau of Justice Assistance and the Office of Juvenile Justice and Delinquency Prevention). He is also a member of the Advisory Board for the Loyola Law School Center for Juvenile Law and Policy and previously served on the California Commission on Juvenile Justice, Crime and Delinquency Prevention. The National Child Labor Committee recognized Fr. Greg with the first Nancy M. Daly Advocacy Award for Service to Children and Youth on January 30, 2012.</p>
	<p>Homeboy Industries, now located in the heart of downtown Los Angeles, is recognized as a national and international model for youth seeking to move beyond gangs and achieve a life of hope.</p>
	HTML;



$div_wikipedia_Greg_Boyle = new WikipediaContentSection();
$div_wikipedia_Greg_Boyle->setTitleText("Greg Boyle");
$div_wikipedia_Greg_Boyle->setTitleLink("https://en.wikipedia.org/wiki/Greg_Boyle");
$div_wikipedia_Greg_Boyle->content = <<<HTML
	<p>Gregory Joseph Boyle, S.J. (born May 19, 1954) is an American Catholic priest of the Jesuit order.
	He is the founder and director of Homeboy Industries,
	the world's largest gang intervention and rehabilitation program,
	and former pastor of Dolores Mission Church in Los Angeles.</p>
	HTML;



$div_youtube_Father_Greg_Boyle_Welcoming_the_Unwelcome_THE_THREAD_Documentary_Series = new YoutubeContentSection();
$div_youtube_Father_Greg_Boyle_Welcoming_the_Unwelcome_THE_THREAD_Documentary_Series->setTitleText("Father Greg Boyle: Welcoming the Unwelcome | THE THREAD Documentary Series");
$div_youtube_Father_Greg_Boyle_Welcoming_the_Unwelcome_THE_THREAD_Documentary_Series->setTitleLink("https://www.youtube.com/watch?v=JjrJAP0tQbY");


$div_youtube_Greg_Boyle_The_Whole_Language_The_Power_of_Extravagant_Tenderness_Talks_at_Google = new YoutubeContentSection();
$div_youtube_Greg_Boyle_The_Whole_Language_The_Power_of_Extravagant_Tenderness_Talks_at_Google->setTitleText("Greg Boyle | The Whole Language: The Power of Extravagant Tenderness | Talks at Google");
$div_youtube_Greg_Boyle_The_Whole_Language_The_Power_of_Extravagant_Tenderness_Talks_at_Google->setTitleLink("https://www.youtube.com/watch?v=mAqFfqzaJ5c");


$div_youtube_Gang_Members_Transformed_By_One_Man_Father_Gregory_Boyle_Goalcast = new YoutubeContentSection();
$div_youtube_Gang_Members_Transformed_By_One_Man_Father_Gregory_Boyle_Goalcast->setTitleText("Gang Members Transformed By One Man | Father Gregory Boyle | Goalcast");
$div_youtube_Gang_Members_Transformed_By_One_Man_Father_Gregory_Boyle_Goalcast->setTitleLink("https://www.youtube.com/watch?v=CguIO5lh87k");


$div_youtube_One_Of_The_Most_Inspirational_Speeches_From_Gangsters_Father_Gregory_Boyle_Goalcast = new YoutubeContentSection();
$div_youtube_One_Of_The_Most_Inspirational_Speeches_From_Gangsters_Father_Gregory_Boyle_Goalcast->setTitleText("One Of The Most Inspirational Speeches From Gangsters | Father Gregory Boyle | Goalcast");
$div_youtube_One_Of_The_Most_Inspirational_Speeches_From_Gangsters_Father_Gregory_Boyle_Goalcast->setTitleLink("https://www.youtube.com/watch?v=zk--XN4ozr8");


$div_youtube_On_Cultivating_Courage_An_Evening_with_Pema_Chodron_and_Father_Greg_Boyle_06_23_18 = new YoutubeContentSection();
$div_youtube_On_Cultivating_Courage_An_Evening_with_Pema_Chodron_and_Father_Greg_Boyle_06_23_18->setTitleText("On Cultivating Courage: An Evening with Pema Chödrön and Father Greg Boyle - 06/23/18");
$div_youtube_On_Cultivating_Courage_An_Evening_with_Pema_Chodron_and_Father_Greg_Boyle_06_23_18->setTitleLink("https://www.youtube.com/watch?v=DT_lJa_riSg");


$div_youtube_How_to_Transition_from_Gangs_to_Growth_Father_Greg_Boyle_amp_Richard_Cabral_REAL_ONES = new YoutubeContentSection();
$div_youtube_How_to_Transition_from_Gangs_to_Growth_Father_Greg_Boyle_amp_Richard_Cabral_REAL_ONES->setTitleText("How to Transition from Gangs to Growth: Father Greg Boyle &amp; Richard Cabral | REAL ONES");
$div_youtube_How_to_Transition_from_Gangs_to_Growth_Father_Greg_Boyle_amp_Richard_Cabral_REAL_ONES->setTitleLink("https://www.youtube.com/watch?v=bqyj-1TZER0");


$div_youtube_Notre_Dame_Commencement_2017_Rev_Gregory_J_Boyle_S_J_s_Laetare_Speech = new YoutubeContentSection();
$div_youtube_Notre_Dame_Commencement_2017_Rev_Gregory_J_Boyle_S_J_s_Laetare_Speech->setTitleText("Notre Dame Commencement 2017: Rev. Gregory J. Boyle, S. J.'s Laetare Speech");
$div_youtube_Notre_Dame_Commencement_2017_Rev_Gregory_J_Boyle_S_J_s_Laetare_Speech->setTitleLink("https://www.youtube.com/watch?v=8eQsHEjfRrY");


$div_youtube_Consider_This_with_Father_Greg_Boyle = new YoutubeContentSection();
$div_youtube_Consider_This_with_Father_Greg_Boyle->setTitleText("Consider This with Father Greg Boyle");
$div_youtube_Consider_This_with_Father_Greg_Boyle->setTitleLink("https://www.youtube.com/watch?v=ZQgO65fsKVE");


$div_youtube_Father_Greg_Boyle_Founder_Homeboy_Industries_On_Radical_Compassion = new YoutubeContentSection();
$div_youtube_Father_Greg_Boyle_Founder_Homeboy_Industries_On_Radical_Compassion->setTitleText("Father Greg Boyle, Founder, Homeboy Industries - On Radical Compassion");
$div_youtube_Father_Greg_Boyle_Founder_Homeboy_Industries_On_Radical_Compassion->setTitleLink("https://www.youtube.com/watch?v=4rLTkgkTTa0");


$div_youtube_The_Rev_Greg_Boyle_March_15_2023 = new YoutubeContentSection();
$div_youtube_The_Rev_Greg_Boyle_March_15_2023->setTitleText("The Rev. Greg Boyle: March 15, 2023");
$div_youtube_The_Rev_Greg_Boyle_March_15_2023->setTitleLink("https://www.youtube.com/watch?v=h6izWw63mjg");


$div_youtube_An_Evening_with_Greg_Boyle_Healing_Communities_Through_Kinship = new YoutubeContentSection();
$div_youtube_An_Evening_with_Greg_Boyle_Healing_Communities_Through_Kinship->setTitleText("An Evening with Greg Boyle: Healing Communities Through Kinship");
$div_youtube_An_Evening_with_Greg_Boyle_Healing_Communities_Through_Kinship->setTitleLink("https://www.youtube.com/watch?v=A6xA4vdo8hw");


$div_youtube_Commencement_Speaker_Rev_Greg_Boyle_S_J_People_change_when_they_are_cherished_SCU2023 = new YoutubeContentSection();
$div_youtube_Commencement_Speaker_Rev_Greg_Boyle_S_J_People_change_when_they_are_cherished_SCU2023->setTitleText("Commencement Speaker Rev. Greg Boyle, S.J.: People change when they are cherished | #SCU2023");
$div_youtube_Commencement_Speaker_Rev_Greg_Boyle_S_J_People_change_when_they_are_cherished_SCU2023->setTitleLink("https://www.youtube.com/watch?v=oWvq34Fgcvs");


$div_youtube_The_Power_of_Boundless_Compassion_An_Evening_with_Fr_Greg_Boyle = new YoutubeContentSection();
$div_youtube_The_Power_of_Boundless_Compassion_An_Evening_with_Fr_Greg_Boyle->setTitleText("The Power of Boundless Compassion: An Evening with Fr. Greg Boyle");
$div_youtube_The_Power_of_Boundless_Compassion_An_Evening_with_Fr_Greg_Boyle->setTitleLink("https://www.youtube.com/watch?v=IYLV9Kr_uw8");



$div_youtube_Reforming_Gang_Members_Through_Unconditional_Love_Father_Greg_Boyle_on_The_Last_Mile_Radio = new YoutubeContentSection();
$div_youtube_Reforming_Gang_Members_Through_Unconditional_Love_Father_Greg_Boyle_on_The_Last_Mile_Radio->setTitleText("Reforming Gang Members Through Unconditional Love - Father Greg Boyle on The Last Mile Radio");
$div_youtube_Reforming_Gang_Members_Through_Unconditional_Love_Father_Greg_Boyle_on_The_Last_Mile_Radio->setTitleLink("https://www.youtube.com/watch?v=TDviuX64P4g");



$div_youtube_The_Issue_Is_Father_Greg_Boyle_s_Homeboy_Industries = new YoutubeContentSection();
$div_youtube_The_Issue_Is_Father_Greg_Boyle_s_Homeboy_Industries->setTitleText("The Issue Is: Father Greg Boyle's Homeboy Industries (Full documentary)");
$div_youtube_The_Issue_Is_Father_Greg_Boyle_s_Homeboy_Industries->setTitleLink("https://www.youtube.com/watch?C3_UQM5BXKQ");




$page->parent('list_of_people.html');

$page->body($div_introduction);
$page->body($div_Early_Life_and_Calling);

$page->body($div_Homeboy_Industries_and_its_Impact);
$page->body('homeboy_industries.html');

$page->body($div_Recognition_and_Legacy);
$page->body($div_Philosophy_and_Beliefs);

$page->body($div_youtube_Compassion_and_Kinship_Fr_Gregory_Boyle_at_TEDxConejo_2012);


$page->body($div_youtube_The_Power_of_Boundless_Compassion_An_Evening_with_Fr_Greg_Boyle);
$page->body($div_youtube_Commencement_Speaker_Rev_Greg_Boyle_S_J_People_change_when_they_are_cherished_SCU2023);
$page->body($div_youtube_An_Evening_with_Greg_Boyle_Healing_Communities_Through_Kinship);
$page->body($div_youtube_The_Rev_Greg_Boyle_March_15_2023);
$page->body($div_youtube_Father_Greg_Boyle_Founder_Homeboy_Industries_On_Radical_Compassion);
$page->body($div_youtube_Consider_This_with_Father_Greg_Boyle);
$page->body($div_youtube_Notre_Dame_Commencement_2017_Rev_Gregory_J_Boyle_S_J_s_Laetare_Speech);
$page->body($div_youtube_How_to_Transition_from_Gangs_to_Growth_Father_Greg_Boyle_amp_Richard_Cabral_REAL_ONES);
$page->body($div_youtube_On_Cultivating_Courage_An_Evening_with_Pema_Chodron_and_Father_Greg_Boyle_06_23_18);
$page->body($div_youtube_One_Of_The_Most_Inspirational_Speeches_From_Gangsters_Father_Gregory_Boyle_Goalcast);
$page->body($div_youtube_Gang_Members_Transformed_By_One_Man_Father_Gregory_Boyle_Goalcast);
$page->body($div_youtube_Greg_Boyle_The_Whole_Language_The_Power_of_Extravagant_Tenderness_Talks_at_Google);
$page->body($div_youtube_Father_Greg_Boyle_Welcoming_the_Unwelcome_THE_THREAD_Documentary_Series);
$page->body($div_youtube_Reforming_Gang_Members_Through_Unconditional_Love_Father_Greg_Boyle_on_The_Last_Mile_Radio);
$page->body($div_youtube_The_Issue_Is_Father_Greg_Boyle_s_Homeboy_Industries);

$page->body($div_wikipedia_Greg_Boyle);
