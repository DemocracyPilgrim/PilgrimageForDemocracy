<?php
$page = new Page();
$page->h1('Poverty');
$page->keywords('Poverty', 'poverty');
$page->stars(0);
$page->tags("Living: Fair Income", "Fair Share");

//$page->snp('description', '');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );

$r1 = $page->ref("https://www.poorpeoplescampaign.org/resource/2023-national-fact-sheet/", "Poor People's Campaign: 2023 National Fact Sheet");
$r2 = $page->ref("https://www.poorpeoplescampaign.org/wp-content/uploads/2024/02/National-Fact-Sheet-Summary.pdf", "[PDF] Poor People's Campaign: 2024 National Fact Sheet");


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>We are primarily concerned by extreme poverty on a $global scale.
	However, poverty is also a grave concern within rich countries like the UK or the USA.</p>
	HTML;



$div_Poverty_in_the_United_States = new ContentSection();
$div_Poverty_in_the_United_States->content = <<<HTML
	<h3>Poverty in the United States</h3>

	<p>According to the ${"Poor People's Campaign"} 2023 fact sheet$r1,
	140 million Americans were living in poverty or just one emergency away from economic ruin:</p>
	<ul>
		<li>60% of Black people (24 million)</li>
		<li>64% of Hispanic/Latino people (38 million)</li>
		<li>40% of Asian people (8 million)</li>
		<li>60% of Indigenous and Native people (2 million)</li>
		<li>33% of white people (66 million)</li>
	</ul>

	<p>Their 2024 fact sheet also reflects the weight of poverty in American elections:$r2</p>
	<ul>
		<li>1/3 of the electorate (85 million people) are poor or low-income.</li>
		<li>In 2020, 58 million people in this group cast ballots, making up 34-46% of
		voters in 9 battleground states and more than 20% in all but 5 states.</li>
		<li>If just 20% of poor and low-income voters who didn’t vote in 2020 went to the
		polls in 2024, they would have the power to sway elections in every state.</li>
	</ul>
	HTML;



$list = new ListOfPages();
$list->add('poor_people_s_campaign_a_national_call_for_a_moral_revival.html');
$list->add('richard_bluhm.html');
$list->add('william_barber_ii.html');
$list->add('white_poverty_how_exposing_myths_about_race_and_class_can_reconstruct_american_democracy.html');
$list->add('world_inequality_database.html');
$list->add('world_inequality_lab.html');
$print_list = $list->print();



$list_Poverty = ListOfPeoplePages::WithTags("Poverty");
$print_list_Poverty = $list_Poverty->print();

$div_list_Poverty = new ContentSection();
$div_list_Poverty->content = <<<HTML
	<h3>Related content</h3>

	$print_list
	$print_list_Poverty
	HTML;



$div_wikipedia_Poverty = new WikipediaContentSection();
$div_wikipedia_Poverty->setTitleText('Poverty');
$div_wikipedia_Poverty->setTitleLink('https://en.wikipedia.org/wiki/Poverty');
$div_wikipedia_Poverty->content = <<<HTML
	<p>Poverty is a state or condition in which one lacks the financial resources and essentials for a certain standard of living.
	Poverty can have diverse social, economic, and political causes and effects.</p>
	HTML;


$page->parent('social_justice.html');
$page->parent('fair_share.html');
$page->template("stub");
$page->body($div_introduction);

$page->body($div_list_Poverty);
$page->body($div_Poverty_in_the_United_States);

$page->body($div_wikipedia_Poverty);
