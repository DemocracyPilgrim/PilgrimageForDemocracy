<?php
$page = new VideoPage();
$page->h1("Lessig: Our democracy no longer represents the people. Here's how to fix it.");
$page->viewport_background("/free/lessig_our_democracy_no_longer_represents_the_people.png");
$page->keywords("Lessig: Our democracy no longer represents the people");
$page->stars(4);
$page->tags("Video: Speech", "Lawrence Lessig", '"Boss" Tweed', "Tweedism", "Danger", "Money Politics", "Corruption", "TED Talk", "Tweed Syndrome");

$page->snp("description", "");
$page->snp("image",       "/free/lessig_our_democracy_no_longer_represents_the_people.1200-630.png");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>"Our democracy no longer represents the people. Here's how we fix it" is a 2015 ${"TED Talk"} by ${"Lawrence Lessig"}.</p>

	<p>Lessig argues that $American $democracy is fundamentally flawed
	due to a system of "$Tweedism" where a tiny fraction of the population – wealthy donors – exert disproportionate influence over political outcomes.
	He asserts that this "institutional $corruption" stems from campaign finance practices and systemic inequalities,
	resulting in a government that is unresponsive to the needs and desires of the average citizen.
	He proposes a "Citizen Equality Act" to address these issues,
	emphasizing equal funding, equal representation, and equal freedom to vote.</p>
	HTML;




$div_youtube_Our_democracy_no_longer_represents_the_people_Here_s_how_we_fix_it_Larry_Lessig_TEDxMidAtlantic = new YoutubeContentSection();
$div_youtube_Our_democracy_no_longer_represents_the_people_Here_s_how_we_fix_it_Larry_Lessig_TEDxMidAtlantic->setTitleText("Our democracy no longer represents the people. Here&#39;s how we fix it | Larry Lessig | TEDxMidAtlantic");
$div_youtube_Our_democracy_no_longer_represents_the_people_Here_s_how_we_fix_it_Larry_Lessig_TEDxMidAtlantic->setTitleLink("https://www.youtube.com/watch?v=PJy8vTu66tE");
$div_youtube_Our_democracy_no_longer_represents_the_people_Here_s_how_we_fix_it_Larry_Lessig_TEDxMidAtlantic->content = <<<HTML
	<p>
	</p>
	HTML;


$h2_Key_Arguments_and_Examples = new h2HeaderContent("Key Arguments and Examples");



$div_1_Tweedism = new ContentSection();
$div_1_Tweedism->content = <<<HTML
	<h3>1. Tweedism</h3>

	<p><strong>Definition:</strong>
	Any two-stage process where a small group controls the nomination of candidates, effectively controlling the outcome of elections, even if the public votes.</p>

	<p><strong>Origin:</strong>
	Named after ${'"Boss" Tweed'} of Tammany Hall, who famously said, "I don't care who does the electing, as long as I get to do the nominating."</p>

	<p><strong>Example 1: Hong Kong</strong>
	The 2014 Hong Kong protests against the ${"People's Republic of China"}'s proposed electoral law,
	which gave a tiny (0.02%) pro-Beijing committee the power to nominate candidates for Chief Executive.
	This demonstrates how a biased nominating process can produce a democracy responsive only to a select few.</p>

	<p><strong>Example 2: Texas All-White Primary (1923):</strong>
	In Texas, only whites were allowed to vote in the Democratic primary, the dominant party at the time,
	effectively excluding African Americans (16% of the population) from the crucial first stage of the electoral process.
	This resulted in a democracy responsive to whites only.</p>
	HTML;



$div_2_The_Money_Primary = new ContentSection();
$div_2_The_Money_Primary->content = <<<HTML
	<h3>2. The Money Primary</h3>

	<p><strong>Concept:</strong>
	Campaign funding is its own primary, where candidates must appeal to wealthy donors to gain access to resources necessary to run a campaign.</p>

	<p><strong>Impact on Politicians:</strong>
	Candidates spend vast amounts of time "dialing for dollars," developing a "sixth sense" of how their actions will affect fundraising.
	This leads them to constantly adjust their views to please potential donors.
	This has an impact on issues not on the public agenda.</p>

	<p><strong>Quote:</strong>
	Leslie Byrne, a Democrat from Virginia, was told to "Always lean to the green" (meaning money, not environmentalism).</p>

	<p><strong>Statistics:</strong></p>

	<ul>
	<li>In 2014, the top 100 donors gave as much to congressional campaigns as the bottom 4.75 million donors.</li>

	<li>In the current election cycle (at the time of the talk, in 2015), 400 families had given half the money to campaigns and Super PACs.</li>

	<li>57,874 Americans "maxed out" their donations in 2014, effectively giving them disproportionate influence over the process.
	Lessig points out this is also 0.02% of Americans.</li>

	</ul>

	<p><strong>Result:</strong>
	This "Green Primary" is dominated by a tiny fraction of the 1%, leading to a democracy responsive to the funders only.</p>
	HTML;



$div_3_Government_Responsiveness_to_Elites = new ContentSection();
$div_3_Government_Responsiveness_to_Elites->content = <<<HTML
	<h3>3. Government Responsiveness to Elites</h3>

	<p><strong>Princeton Study (Gilens and Page):</strong>
	The largest empirical study of government decisions found a strong correlation
	between the views of the economic elite and organized interest groups with actual government policies.
	The study found a flat-line correlation between the views of the average voters and policies.</p>

	<p><strong>Analysis:</strong>
	The preferences of average Americans have a "miniscule, near-zero, statistically non-significant impact on public policy."</p>

	<p><strong>Analogy:</strong>
	The image of citizens driving a bus, but with the steering wheel detached,
	symbolizing that citizens no longer have control over the direction of the country.</p>
	HTML;



$div_4_Corruption_as_a_Systemic_Problem = new ContentSection();
$div_4_Corruption_as_a_Systemic_Problem->content = <<<HTML
	<h3>4. Corruption as a Systemic Problem</h3>

	<p><strong>Argument:</strong>
	The problem is not just individual corrupt politicians, but a corrupt *system*
	that incentivizes them to prioritize wealthy donors and special interests, rather than the people.</p>

	<p><strong>Example:</strong>
	Congress, initially designed to be "dependent on the people alone," is now "dependent on the people *and the Tweeds*."</p>
	HTML;



$div_5_Inequality = new ContentSection();
$div_5_Inequality->content = <<<HTML
	<h3>5. Inequality</h3>

	<p><strong>Focus:</strong>
	Lessig is not primarily focused on wealth inequality but rather on political inequality – the unequal influence of citizens within the democratic system.</p>

	<p><strong>Orwellian Analogy:</strong>
	"All animals are created equal, but the Tweeds are more equal than others."
	This echoes the idea that some have greater access to power than others.</p>

	<p><strong>Core Problem:</strong>
	This inequality allows the Tweeds to control the system and makes real democracy impossible.</p>
	HTML;


$div_6_The_Citizen_Equality_Act = new ContentSection();
$div_6_The_Citizen_Equality_Act->content = <<<HTML
	<h3>6. The Citizen Equality Act</h3>

	<p><strong>Goal:</strong>
	To remove the fundamental inequalities within our representative system and to make it possible for Congress to "be dependent on the people alone".</p>

	<p><strong>Key Provisions:</strong></p>

	<ul>
	<li><strong>Citizen-Funded Campaigns:</strong>
	Implement small-dollar public funding of congressional campaigns, so that they would no longer be dependent on the tiny elite.
	(Lessig mentions the American Anti-Corruption Act and the Government by the People Act.)</li>

	<li><strong>Equal Representation:</strong>
	Reform gerrymandered districts to achieve proportional and fair representation through measures suggested by Fair Vote.</li>

	<li><strong>Equal Freedom to Vote:</strong>
	Eliminate barriers to voting, such as excessive wait times and discriminatory laws.
	(Lessig mentions the Voting Rights Advancement Act and Bernie Sanders' proposal for a "Democracy Day.")</li>

	</ul>
	HTML;



$div_7_Why_Prioritize_This_Issue = new ContentSection();
$div_7_Why_Prioritize_This_Issue->content = <<<HTML
	<h3>7. Why Prioritize This Issue</h3>

	<p><strong>Practical Reason:</strong>
	No other major issue (climate change, social security, student debt, etc.) will be effectively addressed
	until this fundamental problem of democratic inequality is resolved.
	It's the "first problem" to solve.</p>

	<p><strong>Moral Reason:</strong>
	400 years after slavery, America has not yet achieved the goal of equality.
	The system is still designed to allow a political class of "second class citizens".</p>

	<p><strong>Reference to Martin Luther King:</strong>
	His quote “America is essentially a dream ... that all are created equal” highlights the gap between the dream of democracy and the current reality.</p>
	HTML;



$div_Conclusion = new ContentSection();
$div_Conclusion->content = <<<HTML
	<h3>Conclusion</h3>

	<p>Lessig concludes by emphasizing the need for a national movement to fight for equality
	and restore the true meaning of representative democracy.
	He calls on people to learn from past generations who sacrificed for equality
	and to avoid squandering the opportunity to build the greatest democracy in the world.</p>

	<p>In essence, Lessig's TED talk is a passionate and compelling call to action,
	urging citizens to recognize the systemic flaws in American democracy and to demand meaningful change.</p>
	HTML;



$page->parent('list_of_videos.html');

$page->body($div_introduction);

$page->body($div_youtube_Our_democracy_no_longer_represents_the_people_Here_s_how_we_fix_it_Larry_Lessig_TEDxMidAtlantic);

$page->body($h2_Key_Arguments_and_Examples);
$page->body($div_1_Tweedism);
$page->body($div_2_The_Money_Primary);
$page->body($div_3_Government_Responsiveness_to_Elites);
$page->body($div_4_Corruption_as_a_Systemic_Problem);
$page->body($div_5_Inequality);
$page->body($div_6_The_Citizen_Equality_Act);
$page->body($div_7_Why_Prioritize_This_Issue);

$page->body($div_Conclusion);



$page->related_tag("Lessig: Our democracy no longer represents the people");
