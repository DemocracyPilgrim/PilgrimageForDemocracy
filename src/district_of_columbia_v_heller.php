<?php
$page = new Page();
$page->h1("District of Columbia v. Heller (2008)");
$page->tags("Institutions: Judiciary", "SCOTUS Landmark Decision", "USA", "Gun Legislation");
$page->keywords("District of Columbia v. Heller");
$page->stars(1);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>"District of Columbia v. Heller" is a 2008 landmark decision by the ${'Supreme Court of the United States'}.</p>

	<p>This ruling together with ${'McDonald v. City of Chicago'}, concerning the Second Amendment's guarantee of the right to bear arms,
	have made it significantly more difficult for states and local governments to regulate firearms.
	This has contributed to ongoing debates about ${'gun control'} and its impact on public safety.</p>
	HTML;

$div_wikipedia_District_of_Columbia_v_Heller = new WikipediaContentSection();
$div_wikipedia_District_of_Columbia_v_Heller->setTitleText("District of Columbia v Heller");
$div_wikipedia_District_of_Columbia_v_Heller->setTitleLink("https://en.wikipedia.org/wiki/District_of_Columbia_v._Heller");
$div_wikipedia_District_of_Columbia_v_Heller->content = <<<HTML
	<p>District of Columbia v. Heller, 554 U.S. 570 (2008), is a landmark decision of the Supreme Court of the United States.
	It ruled that the Second Amendment to the U.S. Constitution
	protects an individual's right to keep and bear arms—unconnected with service in a militia—for traditionally lawful purposes such as self-defense within the home,
	and that the District of Columbia's handgun ban and requirement that lawfully owned rifles and shotguns be kept
	"unloaded and disassembled or bound by a trigger lock" violated this guarantee.
	It also stated that the right to bear arms is not unlimited and that certain restrictions on guns and gun ownership were permissible.
	It was the first Supreme Court case to decide whether the Second Amendment protects an individual right
	to keep and bear arms for self-defense or whether the right was only intended for state militias.</p>
	HTML;


$page->parent('supreme_court_of_the_united_states.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_wikipedia_District_of_Columbia_v_Heller);
