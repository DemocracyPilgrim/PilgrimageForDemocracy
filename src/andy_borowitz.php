<?php
$page = new PersonPage();
$page->h1("Andy Borowitz");
$page->alpha_sort("Borowitz, Andy");
$page->viewport_background("");
$page->keywords("Andy Borowitz");
$page->stars(0);
$page->tags("Person");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Andy Borowitz is an $American author and comedian.</p>

	<p>He is the author of the book: "Profiles in Ignorance – How America's Politicians Got Dumb and Dumber".</p>
	HTML;

$div_wikipedia_Andy_Borowitz = new WikipediaContentSection();
$div_wikipedia_Andy_Borowitz->setTitleText("Andy Borowitz");
$div_wikipedia_Andy_Borowitz->setTitleLink("https://en.wikipedia.org/wiki/Andy_Borowitz");
$div_wikipedia_Andy_Borowitz->content = <<<HTML
	<p>Andy Borowitz is an American writer, comedian, satirist, and actor. Borowitz is a New York Times-bestselling author who won the first National Press Club award for humor. He is known for creating the NBC sitcom The Fresh Prince of Bel-Air and the satirical column The Borowitz Report.</p>
	HTML;


$page->parent('list_of_people.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Andy Borowitz");
$page->body($div_wikipedia_Andy_Borowitz);
