<?php
$page = new BookPage();
$page->h1("Erasing History: How Fascists rewrite the Past to Control the Future");
$page->tags("Book", "Jason Stanley", "Fascism", "History", "Disinformation");
$page->keywords("Erasing History: How Fascists rewrite the Past to Control the Future");
$page->stars(1);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>"Erasing History: How Fascists Rewrite the Past to Control the Future" is a book by ${'Jason Stanley'}.</p>
	HTML;




$div_Erasing_HistoryErasing_History = new WebsiteContentSection();
$div_Erasing_HistoryErasing_History->setTitleText("Publisher: Erasing History");
$div_Erasing_HistoryErasing_History->setTitleLink("https://www.simonandschuster.com/books/Erasing-History/Jason-Stanley/9781668056912");
$div_Erasing_HistoryErasing_History->content = <<<HTML
	<p>The human race finds itself again under threat of a rising global fascist movement. In the United States, democracy is under attack by an authoritarian movement that has found fertile ground among the country’s conservative politicians and voters, but similar movements have found homes in the hearts and minds of people all across the globe. To understand the shape, form, and stakes of this assault, we must go back to extract lessons from our past.</p>

	<p>Democracy requires a common understanding of reality, a shared view of what has happened, that informs ordinary citizens’ decisions about what should happen, now and in the future. Authoritarians target this shared understanding, seeking to separate us from our own history to destroy our self-understanding and leave us unmoored, resentful, and confused. By setting us against each other, authoritarians represent themselves as the sole solution.</p>

	<p>In authoritarian countries, critical examination of those nations’ history and traditions is discouraged if not an outright danger to those who do it. And it is no accident that local and global institutions of education have become a battleground, the authoritarian right’s tip of the spear, where learning and efforts to upend a hierarchal status quo can be put to end by coercion and threats of violence. Democracies entrust schools and universities to preserve a common memory of positive change, generated by protests, social movements, and rebellions. The authoritarian right must erase this history, and, along with it, the very practice of critical inquiry that has so often been the engine of future progress.</p>

	<p>In Erasing History, Yale professor of philosophy Jason Stanley exposes the true danger of the authoritarian right’s attacks on education, identifies their key tactics and funders, and traces their intellectual roots. He illustrates how fears of a fascist future have metastasized, from hypothetical threat to present reality. And he shows that hearts and minds are won in our schools and universities—places, he explains, that democratic societies across the world are now ill-prepared to defend against the fascist assault currently underway.</p>
	HTML;



$page->parent('list_of_books.html');
$page->template("stub");
$page->body($div_introduction);



$page->body($div_Erasing_HistoryErasing_History);
