<?php
$page = new Page();
$page->h1("List of documentaries");
$page->keywords("documentaries", "documentary", "podcast", "podcasts", "Documentary", "Podcast", 'list of documentaries');
$page->stars(0);
$page->viewport_background('/free/list_of_documentaries.png');

$page->snp("description", "Documentaries and podcasts related to peace, democracy or social justice.");
$page->snp("image",       "/free/list_of_documentaries.1200-630.png");

$page->preview( <<<HTML
	<p>Documentaries and podcasts related to peace, democracy or social justice.</p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Some documentaries which are related to the themes discussed in the $Pilgrimage.</p>

	<p><em>You can suggest any relevant documentary to be added to this list.
	See the related Codeberg issue.</p>
	HTML;

$div_codeberg_List_of_documentaries_and_movies = new CodebergContentSection();
$div_codeberg_List_of_documentaries_and_movies->setTitleText('List of documentaries and movies');
$div_codeberg_List_of_documentaries_and_movies->setTitleLink('https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy/issues/3');
$div_codeberg_List_of_documentaries_and_movies->content = <<<HTML
	<p>list of relevant documentaries and movies.</p>
	HTML;


$div_List_of_Documentaries = new ContentSection();
$div_List_of_Documentaries->content = <<<HTML
	<h3>List of Documentaries</h3>

	<p>
	</p>
	HTML;


$div_List_of_Podcasts = new ContentSection();
$div_List_of_Podcasts->content = <<<HTML
	<h3>List of Podcasts</h3>

	<p>
	</p>
	HTML;


$page->parent('lists.html');
$page->template("stub");
$page->body($div_introduction);

$page->related_tag("Documentary", $div_List_of_Documentaries);
$page->related_tag("Podcast", $div_List_of_Podcasts);
$page->body($div_codeberg_List_of_documentaries_and_movies);
$page->body('list_of_movies.html');
