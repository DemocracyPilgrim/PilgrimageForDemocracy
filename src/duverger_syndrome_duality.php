<?php
$page = new Page();
$page->h1('Duverger symptom 1: divisive dualism');
$page->stars(0);
$page->tags("Elections: Duverger Syndrome");

$page->preview( <<<HTML
	<p>Democratic countries throughout the world
	all have electorates of millions of voters
	who seem to neatly fall into two broad camps.
	Is that natural?
	Is that healthy?</p>
	HTML );

//$page->snp('description', "");
//$page->snp('image', "/copyrighted/");


$r1 = $page->ref('https://thehill.com/blogs/congress-blog/politics/267222-the-two-party-system-is-destroying-america/', 'The two-party system is destroying America');


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<blockquote>
		<p><strong>The two-party system is destroying America</strong></p>

		<p>Democrats and Republicans are in a death match and the American people are caught in the middle. $r1</p>
	</blockquote>

	<p>This was headline and opening sentence of an op-ed in 2016 in a major American political website.
	Politics today is extremely dualistic and antagonistic, and not only in the $USA.
	This same duality can be observed, throughout the $world, with local variations due to the local electoral system, history and culture.
	This is the first and by far the most obvious symptom of the Duverger Syndrome.</p>
	HTML;


$div_Duality_throughout_the_democratic_world = new ContentSection();
$div_Duality_throughout_the_democratic_world->content = <<<HTML
	<h3>Duality throughout the democratic world</h3>

	<p>The bi-polarisation of the US politics is an excellent case in point:
	it is a rule much more than an exception.</p>

	<p>In every democracy, we have by and large two largely defined political camps:
	the right vs the left, the progressive vs the conservative, the Catholics vs the Protestants,
	the Royalists vs the Republican, the Jacobins vs the Montagnards, etc. etc.</p>

	<p>How could we consider natural the fact that democratic countries throughout the world
	all have electorates of millions or tens of millions of voters
	who seem to neatly fall into two broad camps?
	Can't democracy cope with a wider variety of opinions?
	What are the costs of such a division?</p>
	HTML;


$div_dualism_united_states = new ContentSection();
$div_dualism_united_states->content = <<<HTML
	<h3>Bi-polarisation in the United States</h3>

	<p>American politics has long been dominated by two parties: the Republican Party and the Democratic Party.</p>

	<p>The increased polarisation of politics in the United States is such that there is less and less split-ticket voting .
	After the 2020 elections, out of 100 senators, only five caucus with the party that lost the 2020 presidential race in their state.
	In the House of Representatives, only 18 Republicans are in districts that Biden won, and 5 Democrats are in districts that Trump won.
	It is increasingly the same for governors: only nine states out of fifty have governors from the "wrong" party.</p>
	HTML;



$div_Mitigating_factor_two_round_elections = new ContentSection();
$div_Mitigating_factor_two_round_elections->content = <<<HTML
	<h3>Mitigating factor: two round elections</h3>

	<p>Some countries, like $France, have a two-round election system,
	whereby the top two contenders of the first round go to the second round.
	This type of electoral system gives some breathing room to smaller parties,
	whose supporters can support more freely in the first round.</p>

	<p>As we shall see, however, this approach is not without its major drawbacks.</p>
	HTML;


$div_Mitigating_factor_parliamentary_systems = new ContentSection();
$div_Mitigating_factor_parliamentary_systems->content = <<<HTML
	<h3>Mitigating factor: parliamentary systems</h3>

	<p>What influence, if any, does a parliamentary system, like in the UK, affect the tendency for dualism.</p>
	HTML;


$div_Mitigating_factor_federal_system = new ContentSection();
$div_Mitigating_factor_federal_system->content = <<<HTML
	<h3>Mitigating factor: federal systems</h3>

	<p>Federal systems, like $Germany and the $EU, without a strong central leader,
	or maybe combined with a parliamentary system,
	can leave room for a multitude of regional parties to thrive.</p>

	<p>In the end, however, we still end up with two broad camps: progressives vs conservatives,
	with each regional party falling in line with one side or the other.</p>
	HTML;



$div_wedge_issues = new ContentSection();
$div_wedge_issues->content = <<<HTML
	<h3>wedge issues</h3>

	<p>It a political field which, in most $countries, is basically reduced to two main contenders or two main parties,
	it is critical for candidates to cultivate ${'wedge issues'} in order to reduce the inherent complexity of any serious political debate
	to a single issue that casual voters can relate to more easily, dumbing down the issue to simply being "for or against"
	a narrow aspect of the issue.</p>
	HTML;


$page->parent('duverger_syndrome.html');
$page->template("stub");
$page->body($div_introduction);
$page->body($div_dualism_united_states);
$page->body($div_Duality_throughout_the_democratic_world);
$page->body($div_Mitigating_factor_two_round_elections);
$page->body($div_Mitigating_factor_parliamentary_systems);
$page->body($div_Mitigating_factor_federal_system);

$page->body($div_wedge_issues);

$page->body('duverger_syndrome_alternance.html');
