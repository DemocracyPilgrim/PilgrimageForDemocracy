<?php
$page = new Page();
$page->h1("Supreme court");
$page->tags("Institutions: Judiciary");
$page->keywords("Supreme Court", "supreme court");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$list_Supreme_Court = ListOfPeoplePages::WithTags("Supreme Court");
$print_list_Supreme_Court = $list_Supreme_Court->print();

$div_list_Supreme_Court = new ContentSection();
$div_list_Supreme_Court->content = <<<HTML
	<h3>Related content</h3>

	$print_list_Supreme_Court
	HTML;



$div_wikipedia_Supreme_court = new WikipediaContentSection();
$div_wikipedia_Supreme_court->setTitleText("Supreme court");
$div_wikipedia_Supreme_court->setTitleLink("https://en.wikipedia.org/wiki/Supreme_court");
$div_wikipedia_Supreme_court->content = <<<HTML
	<p>In most legal jurisdictions, a supreme court, also known as a court of last resort, apex court,
	and high (or final) court of appeal, and court of final appeal, is the highest court within the hierarchy of courts.
	Broadly speaking, the decisions of a supreme court are binding on all other courts in a nation
	and are not subject to further review by any other court.
	Supreme courts typically function primarily as appellate courts, hearing appeals from decisions of lower trial courts,
	or from intermediate-level appellate courts.
	A supreme court can also, in certain circumstances, act as a court of original jurisdiction.</p>
	HTML;


$page->parent('justice.html');
$page->template("stub");
$page->body($div_introduction);

$page->body($div_list_Supreme_Court);

$page->body($div_wikipedia_Supreme_court);
