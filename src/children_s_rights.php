<?php
$page = new Page();
$page->h1("Children's Rights");
$page->tags("Individual: Rights", "Children", "Education", "Rights");
$page->keywords("Children's Rights", "child", "children");
$page->stars(0);
$page->viewport_background("/free/children_s_rights.png");

$page->snp("description", "/free/children_s_rights.1200-630.png");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Children_s_rights = new WikipediaContentSection();
$div_wikipedia_Children_s_rights->setTitleText("Children s rights");
$div_wikipedia_Children_s_rights->setTitleLink("https://en.wikipedia.org/wiki/Children's_rights");
$div_wikipedia_Children_s_rights->content = <<<HTML
	<p>Children's rights or the rights of children are a subset of human rights with particular attention to the rights of special protection and care afforded to minors. The 1989 Convention on the Rights of the Child (CRC) defines a child as "any human being below the age of eighteen years, unless under the law applicable to the child, majority is attained earlier." Children's rights includes their right to association with both parents, human identity as well as the basic needs for physical protection, food, universal state-paid education, health care, and criminal laws appropriate for the age and development of the child, equal protection of the child's civil rights, and freedom from discrimination on the basis of the child's race, gender, sexual orientation, gender identity, national origin, religion, disability, color, ethnicity, or other characteristics.</p>
	HTML;


$page->parent('individuals.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Children's Rights");
$page->body($div_wikipedia_Children_s_rights);
