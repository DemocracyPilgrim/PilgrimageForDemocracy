<?php
$page = new Page();
$page->h1("International Institute for Democracy and Electoral Assistance (International IDEA)");
$page->keywords("International Institute for Democracy and Electoral Assistance", "International IDEA");
$page->tags("Organisation", "International Organisation", "Elections", "Taiwan");
$page->stars(0);

//$page->snp("description", "");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;


$div_International_IDEA_website = new WebsiteContentSection();
$div_International_IDEA_website->setTitleText("International IDEA website ");
$div_International_IDEA_website->setTitleLink("https://www.idea.int/");
$div_International_IDEA_website->content = <<<HTML
	<p>Advancing democracy worldwide, as a universal human aspiration and an enabler of sustainable development,
	through support to the building, strengthening and safeguarding of democratic political institutions and processes at all levels.</p>
	HTML;


$div_codeberg_International_Institute_for_Democracy_and_Electoral_Assistance_International_IDEA = new CodebergContentSection();
$div_codeberg_International_Institute_for_Democracy_and_Electoral_Assistance_International_IDEA->setTitleText("International Institute for Democracy and Electoral Assistance (International IDEA)");
$div_codeberg_International_Institute_for_Democracy_and_Electoral_Assistance_International_IDEA->setTitleLink("https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy/issues/54");
$div_codeberg_International_Institute_for_Democracy_and_Electoral_Assistance_International_IDEA->content = <<<HTML
	<p>This organisation ought to be much better known.
	We need to dig deeper on what they do.</p>
	HTML;



$div_codeberg_Has_Taiwan_Republic_of_China_applied_to_join_Internation_IDEA = new CodebergContentSection();
$div_codeberg_Has_Taiwan_Republic_of_China_applied_to_join_Internation_IDEA->setTitleText("Has Taiwan (Republic of China) applied to join Internation IDEA?");
$div_codeberg_Has_Taiwan_Republic_of_China_applied_to_join_Internation_IDEA->setTitleLink("https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy/issues/55");
$div_codeberg_Has_Taiwan_Republic_of_China_applied_to_join_Internation_IDEA->content = <<<HTML
	<p>Has the government of $Taiwan applied to join the International Institute for Democracy and Electoral Assistance (International IDEA)?
	Would Taiwan's application be accepted?
	What are the membership requirements?</p>
	HTML;



$div_wikipedia_International_Institute_for_Democracy_and_Electoral_Assistance = new WikipediaContentSection();
$div_wikipedia_International_Institute_for_Democracy_and_Electoral_Assistance->setTitleText("International Institute for Democracy and Electoral Assistance");
$div_wikipedia_International_Institute_for_Democracy_and_Electoral_Assistance->setTitleLink("https://en.wikipedia.org/wiki/International_Institute_for_Democracy_and_Electoral_Assistance");
$div_wikipedia_International_Institute_for_Democracy_and_Electoral_Assistance->content = <<<HTML
	<p>The International Institute for Democracy and Electoral Assistance (International IDEA)
	is an intergovernmental organization that works to support and strengthen democratic institutions and processes around the world,
	to develop sustainable, effective and legitimate democracies.</p>
	HTML;


$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_International_IDEA_website);

$page->body($div_codeberg_International_Institute_for_Democracy_and_Electoral_Assistance_International_IDEA);
$page->body($div_codeberg_Has_Taiwan_Republic_of_China_applied_to_join_Internation_IDEA);

$page->body($div_wikipedia_International_Institute_for_Democracy_and_Electoral_Assistance);
