<?php
$page = new Page();
$page->h1("Electoral system");
$page->viewport_background('/free/electoral_system.png');
$page->keywords("electoral system", "electoral systems", "Electoral System");
$page->tags("Elections");
$page->stars(0);

//$page->snp("description", "");
$page->snp("image",       "/free/electoral_system.1200-630.png");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref('', '');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>A $country's electoral system covers all rules and aspects that have an influence on any election.
	The most important aspect is the ${'voting method'}.</p>
	HTML;


$list_Electoral_System = ListOfPeoplePages::WithTags("Electoral System");
$print_list_Electoral_System = $list_Electoral_System->print();

$div_list_Electoral_System = new ContentSection();
$div_list_Electoral_System->content = <<<HTML
	<h3>Related content</h3>

	$print_list_Electoral_System
	HTML;



$div_wikipedia_Electoral_system = new WikipediaContentSection();
$div_wikipedia_Electoral_system->setTitleText("Electoral system");
$div_wikipedia_Electoral_system->setTitleLink("https://en.wikipedia.org/wiki/Electoral_system");
$div_wikipedia_Electoral_system->content = <<<HTML
	<p>An electoral system or voting system is a set of rules that determine
	how elections and referendums are conducted and how their results are determined.
	Electoral systems are used in politics to elect governments,
	while non-political elections may take place in business, non-profit organisations and informal organisations.
	These rules govern all aspects of the voting process:
	when elections occur, who is allowed to vote, who can stand as a candidate, how ballots are marked and cast, how the ballots are counted,
	how votes translate into the election outcome, limits on campaign spending, and other factors that can affect the result.
	Political electoral systems are defined by constitutions and electoral laws,
	are typically conducted by election commissions, and can use multiple types of elections for different offices.</p>
	HTML;

$page->parent('elections.html');

$page->template("stub");
$page->body($div_introduction);


$page->body('duverger_syndrome.html');
$page->body('voting_methods.html');
$page->body($div_list_Electoral_System);

$page->template('cover-picture-ai-generated');
$page->body($div_wikipedia_Electoral_system);
