<?php
$page = new Page();
$page->h1('Military and Security Developments Involving the People\'s Republic of China (2023 DOD report)');
$page->stars(2);

$page->snp('description', 'DOD report on China\'s military buildup.');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p>This report illustrates the importance of meeting the pacing challenge presented by the PRC’s increasingly capable military.</p>
	HTML );

$r1 = $page->ref('https://www.defense.gov/News/News-Stories/Article/Article/3562442/dod-report-details-chinese-efforts-to-build-military-power/',
                 'DOD Report Details Chinese Efforts to Build Military Power');


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>This report was published by the US Department of Defense in 2023. $r1</p>

	<h3>Abstract</h3>

	<div class="download-pdf">
		<a href="https://media.defense.gov/2023/Oct/19/2003323409/-1/-1/1/2023-MILITARY-AND-SECURITY-DEVELOPMENTS-INVOLVING-THE-PEOPLES-REPUBLIC-OF-CHINA.PDF">
			<img src="/copyrighted/pdf/2023-MILITARY-AND-SECURITY-DEVELOPMENTS-INVOLVING-THE-PEOPLES-REPUBLIC-OF-CHINA-001.png">
			<div class="download"><i class="outline file pdf white large icon"></i>Download</div>
		</a>
	</div>

	<p><em>"The 2022 National Security Strategy states that the People’s Republic of China (PRC) is the only
	competitor to the United States with the intent and, increasingly, the capacity to reshape the
	international order. As a result, the 2022 National Defense Strategy identifies the PRC as the
	“pacing challenge” for the Department of Defense. As the PRC seeks to achieve “national
	rejuvenation” by its centenary in 2049, Chinese Communist Party (CCP) leaders view a modern,
	capable, and “world class” military as essential to overcoming what Beijing sees as an increasingly
	turbulent international environment.</em>"</p>

	<p><em>"The DoD annual report on Military and Security Developments Involving the People’s Republic
	of China charts the current course of the PRC’s national, economic, and military strategy, and
	offers insight on the People’s Liberation Army’s (PLA) strategy, current capabilities and activities,
	as well as its future modernization goals.</em>"</p>

	<p><em>"In 2022, the PRC turned to the PLA as an increasingly capable instrument of statecraft. Throughout
	the year, the PLA adopted more coercive actions in the Indo-Pacific region, while accelerating its
	development of capabilities and concepts to strengthen the PRC’s ability to “fight and win wars”
	against a “strong enemy,” counter an intervention by a third party in a conflict along the PRC’s
	periphery, and to project power globally. At the same time, the PRC largely denied, cancelled, and
	ignored recurring bilateral defense engagements, as well as DoD requests for military-to-military
	communication at multiple levels."</em></p>

	<p><em>"This report illustrates the importance of meeting the pacing challenge presented by the PRC’s
	increasingly capable military."</em></p>
	HTML;


$page->parent('list_of_research_and_reports.html');
$page->parent('china_and_taiwan.html');
$page->template("stub");
$page->body($div_introduction);


