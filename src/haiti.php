<?php
$page = new CountryPage('Haiti');
$page->h1('Haiti');
$page->tags("Country");
$page->keywords('Haiti');
$page->stars(1);

$page->preview( <<<HTML
	<p></p>
	HTML );

$page->snp('description', '11 million inhabitants.');
//$page->snp('image',       '/copyrighted/');


$r1 = $page->ref('https://impactful.ninja/best-charities-impacting-haiti/', '9 Best Charities Impacting Haiti');
$r2 = $page->ref('https://cpj.org/reports/2023/10/haiti-joins-list-of-countries-where-killers-of-journalists-most-likely-to-go-unpunished/', 'Haiti joins list of countries where killers of journalists most likely to go unpunished');
$r3 = $page->ref('https://cpj.org/2023/10/in-haiti-murders-of-journalists-go-unpunished-amid-instability-and-gang-violence/', 'In Haiti, murders of journalists go unpunished amid instability and gang violence');
$r4 = $page->ref('https://news.un.org/en/story/2023/09/1141632', 'UN: Explainer: Why Haiti is calling for a new international mission');


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Haiti is a country in dire need of help.
	Haiti is facing so many internal challenges that it necessarily will need outside intervention,
	in the name of international solidarity, to help the country get back on track.</p>

	<p>We shall see, in turn, the succession of crisis and disasters that befell upon Haiti,
	the critical challenges that Haiti is currently facing,
	and the international governmental and non-governmental help that Haiti may rely upon to restore order and democracy.</p>

	<p>Haiti is a member of the ${'Caribbean Community'}.</p>
	HTML;



$div_codeberg_Haiti = new CodebergContentSection();
$div_codeberg_Haiti->setTitleText('Haiti');
$div_codeberg_Haiti->setTitleLink('https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy/issues/45');
$div_codeberg_Haiti->content = <<<HTML
	<p>The country has suffered such an unprecedented series of disasters.
	Who is helping Haiti?</p>
	HTML;


$h2_A_string_of_bad_luck = new h2HeaderContent('A string of bad luck');

$div_bad_luck = new ContentSection();
$div_bad_luck->content = <<<HTML
	<p>The title of this section is an euphemism.
	What Haiti has gone through is in fact a terrible series of disasters.</p>
	HTML;

$div_public_debt = new ContentSection();
$div_public_debt->content = <<<HTML
	<h3>Public debt</h3>

	<p> $1.25 billion
	</p>
	HTML;


$div_wikipedia_External_debt_of_Haiti = new WikipediaContentSection();
$div_wikipedia_External_debt_of_Haiti->setTitleText('External debt of Haiti');
$div_wikipedia_External_debt_of_Haiti->setTitleLink('https://en.wikipedia.org/wiki/External_debt_of_Haiti');
$div_wikipedia_External_debt_of_Haiti->content = <<<HTML
	HTML;



$div_2010_Haiti_earthquake = new ContentSection();
$div_2010_Haiti_earthquake->content = <<<HTML
	<h3>2010 Haiti earthquake</h3>

	<p>A catastrophic magnitude 7.0 Mw earthquake struck Haiti on 12 January 2010.
	The epicenter was only 25 kilometres (16 mi) west of Port-au-Prince, Haiti's capital.</p>

	<p>Death toll estimates range from 100,000 to about 160,000, with contested government estimates going up to 316,000.</p>

	<p>The government of Haiti estimated that 250,000 residences and 30,000 commercial buildings had collapsed or were severely damaged.</p>

	<p>The devastation caused by the earthquake did much to worsen the already fragile economic situation
	and deepened the country's external debt.</p>

	<p>Presaging to the political turmoil that would befall on Haiti in the following years,
	government buildings were badly damaged or collapsed, including the Presidential Palace, the National Assembly,
	the headquarters of the United Nations Stabilization Mission in Haiti (MINUSTAH).</p>
	HTML;


$div_wikipedia_2010_Haiti_earthquake = new WikipediaContentSection();
$div_wikipedia_2010_Haiti_earthquake->setTitleText('2010 Haiti earthquake');
$div_wikipedia_2010_Haiti_earthquake->setTitleLink('https://en.wikipedia.org/wiki/2010_Haiti_earthquake');
$div_wikipedia_2010_Haiti_earthquake->content = <<<HTML
	HTML;


$div_Assassination_of_the_president_of_Haiti = new ContentSection();
$div_Assassination_of_the_president_of_Haiti->content = <<<HTML
	<h3>Assassination of the president of Haiti</h3>

	<p>
	</p>
	HTML;




$div_2021_Haiti_earthquake = new ContentSection();
$div_2021_Haiti_earthquake->content = <<<HTML
	<h3>2021 Haiti earthquake</h3>

	<p>
	</p>
	HTML;




$div_Socioeconomic_crisis = new ContentSection();
$div_Socioeconomic_crisis->content = <<<HTML
	<h3>Socioeconomic crisis</h3>

	<p>
	</p>
	HTML;


$div_wikipedia_2022_Haitian_crisis = new WikipediaContentSection();
$div_wikipedia_2022_Haitian_crisis->setTitleText('2022 Haitian crisis');
$div_wikipedia_2022_Haitian_crisis->setTitleLink('https://en.wikipedia.org/wiki/2022_Haitian_crisis');
$div_wikipedia_2022_Haitian_crisis->content = <<<HTML
	HTML;



$h2_Current_challenges = new h2HeaderContent('Current challenges');


$div_Gang_violence = new ContentSection();
$div_Gang_violence->content = <<<HTML
	<h3>Gang violence</h3>

	<p>Without a functioning government, Haitian city streets are being ruled by violent gangs.
	According to the ${'United Nations'}, in the first nine months of 2023:
	"<em>3,000 homicides were reported; there were also over 1,500 victims of kidnapping for ransom.
	The UN says that some 200,000 people (half of whom are children) have been forced to flee their homes
	because it’s just too dangerous to remain.</em>" $r4</p>
	HTML;


$div_wikipedia_External_debt_of_Haiti = new WikipediaContentSection();
$div_wikipedia_External_debt_of_Haiti->setTitleText('External debt of Haiti');
$div_wikipedia_External_debt_of_Haiti->setTitleLink('https://en.wikipedia.org/wiki/External_debt_of_Haiti');
$div_wikipedia_External_debt_of_Haiti->content = <<<HTML
	HTML;


$div_attacks_on_journalists = new ContentSection();
$div_attacks_on_journalists->content = <<<HTML
	<h3>Attacks on journalists</h3>

	<p>According to the ${'Committee to Protect Journalists'},
	Haiti one of the $countries where killers of journalists are most likely to go unpunished.$r2 $r3</p>
	HTML;


$div_CPJ_Haiti_Americas = new WebsiteContentSection();
$div_CPJ_Haiti_Americas->setTitleText('Committee to Protect Journalists: Haiti profile page');
$div_CPJ_Haiti_Americas->setTitleLink('https://cpj.org/americas/haiti/');
$div_CPJ_Haiti_Americas->content = <<<HTML
	<p>Haiti now ranks as the world’s third-worst impunity offender, behind Syria and Somalia respectively.
	Somalia, along with Iraq, Mexico, the Philippines, Pakistan, and India, have been on the index every year since its inception.
	Syria, South Sudan, Afghanistan, and Brazil also have been there for years – a sobering reminder of the persistent and pernicious nature of impunity.</p>
	HTML;



$h2_Help = new h2HeaderContent('Help');



$div_wikipedia_United_Nations_Stabilisation_Mission_in_Haiti = new WikipediaContentSection();
$div_wikipedia_United_Nations_Stabilisation_Mission_in_Haiti->setTitleText('[Discontinued] United Nations Stabilisation Mission in Haiti');
$div_wikipedia_United_Nations_Stabilisation_Mission_in_Haiti->setTitleLink('https://en.wikipedia.org/wiki/United_Nations_Stabilisation_Mission_in_Haiti');
$div_wikipedia_United_Nations_Stabilisation_Mission_in_Haiti->content = <<<HTML
	HTML;



$div_wikipedia_United_Nations_Mission_for_Justice_Support_in_Haiti = new WikipediaContentSection();
$div_wikipedia_United_Nations_Mission_for_Justice_Support_in_Haiti->setTitleText('[Discontinued] United Nations Mission for Justice Support in Haiti');
$div_wikipedia_United_Nations_Mission_for_Justice_Support_in_Haiti->setTitleLink('https://en.wikipedia.org/wiki/United_Nations_Mission_for_Justice_Support_in_Haiti');
$div_wikipedia_United_Nations_Mission_for_Justice_Support_in_Haiti->content = <<<HTML
	HTML;

$div_United_Nations_Integrated_Office_in_Haiti = new ContentSection();
$div_United_Nations_Integrated_Office_in_Haiti->content = <<<HTML
	<h3>United Nations Integrated Office in Haiti (BINUH)</h3>

	<p>
	</p>
	HTML;




$div_charities_helping_Haiti = new ContentSection();
$div_charities_helping_Haiti->content = <<<HTML
	<h3>Charities helping Haiti</h3>

	<p>Impactful Ninja is listing nine charities that have the most effective impact on Haiti. $r1:</p>

	<ul>
		<li>Hope for Haiti</li>
		<li>Fonkoze</li>
		<li>UNICEF</li>
		<li>Save the Children</li>
		<li>Sustainable Organic Integrated Livelihoods (SOIL)</li>
		<li>Partners in Health</li>
		<li>Mission of Hope Haiti International</li>
		<li>Haitian Health Foundation</li>
		<li>Haiti Partners</li>
	</ul>

	HTML;







$div_UN_news_Haiti = new WebsiteContentSection();
$div_UN_news_Haiti->setTitleText('UN: Haiti ');
$div_UN_news_Haiti->setTitleLink('https://news.un.org/en/tags/haiti');
$div_UN_news_Haiti->content = <<<HTML
	<p>${'United Nations'}'s news feed on Haiti.</p>
	HTML;



$div_youtube_Divided_island_How_Haiti_and_the_DR_became_two_worlds = new YoutubeContentSection();
$div_youtube_Divided_island_How_Haiti_and_the_DR_became_two_worlds->setTitleText('Divided island: How Haiti and the DR became two worlds');
$div_youtube_Divided_island_How_Haiti_and_the_DR_became_two_worlds->setTitleLink('https://www.youtube.com/watch?v=4WvKeYuwifc&ab_channel=Vox');
$div_youtube_Divided_island_How_Haiti_and_the_DR_became_two_worlds->content = <<<HTML
	HTML;


$div_youtube_Fighting_for_Haiti_CBS_Reports = new YoutubeContentSection();
$div_youtube_Fighting_for_Haiti_CBS_Reports->setTitleText('Fighting for Haiti | CBS Reports');
$div_youtube_Fighting_for_Haiti_CBS_Reports->setTitleLink('https://www.youtube.com/watch?v=IlDYHCFShv8&ab_channel=CBSNews');
$div_youtube_Fighting_for_Haiti_CBS_Reports->content = <<<HTML
	HTML;




$div_wikipedia_Haiti = new WikipediaContentSection();
$div_wikipedia_Haiti->setTitleText('Haiti');
$div_wikipedia_Haiti->setTitleLink('https://en.wikipedia.org/wiki/Haiti');
$div_wikipedia_Haiti->content = <<<HTML
	<p>Haiti is a country located on the island of Hispaniola in the Caribbean Sea.
	It occupies the western three-eighths of the island which it shares with the Dominican Republic.</p>
	HTML;

$div_wikipedia_Haitian_crisis_2018_present = new WikipediaContentSection();
$div_wikipedia_Haitian_crisis_2018_present->setTitleText('Haitian crisis 2018 present');
$div_wikipedia_Haitian_crisis_2018_present->setTitleLink('https://en.wikipedia.org/wiki/Haitian_crisis_(2018–present)');
$div_wikipedia_Haitian_crisis_2018_present->content = <<<HTML
	<p>Series of protests and street violence fuelled by rising energy prices,
	with armed gangs taking over streets.</p>
	HTML;

$div_wikipedia_Assassination_of_Jovenel_Moise = new WikipediaContentSection();
$div_wikipedia_Assassination_of_Jovenel_Moise->setTitleText('Assassination of Jovenel Moïse');
$div_wikipedia_Assassination_of_Jovenel_Moise->setTitleLink('https://en.wikipedia.org/wiki/Assassination_of_Jovenel_Moïse');
$div_wikipedia_Assassination_of_Jovenel_Moise->content = <<<HTML
	<p>Jovenel Moïse, the president of Haiti, was assassinated on 7 July 2021 at his residence in Port-au-Prince.
	A group of 28 foreign mercenaries, mostly Colombians, are alleged to be responsible for the killing.</p>
	HTML;

$div_wikipedia_2021_Haiti_earthquake = new WikipediaContentSection();
$div_wikipedia_2021_Haiti_earthquake->setTitleText('2021 Haiti earthquake');
$div_wikipedia_2021_Haiti_earthquake->setTitleLink('https://en.wikipedia.org/wiki/2021_Haiti_earthquake');
$div_wikipedia_2021_Haiti_earthquake->content = <<<HTML
	<p>On 14 August 2021, a magnitude 7.2 earthquake struck the Tiburon Peninsula in the Caribbean nation of Haiti.
	At least 2,248 people were confirmed killed as of 1 September 2021 and more than 12,200 injured.</p>
	HTML;

$div_wikipedia_Politics_of_Haiti = new WikipediaContentSection();
$div_wikipedia_Politics_of_Haiti->setTitleText('Politics of Haiti');
$div_wikipedia_Politics_of_Haiti->setTitleLink('https://en.wikipedia.org/wiki/Politics_of_Haiti');
$div_wikipedia_Politics_of_Haiti->content = <<<HTML
	<p>The politics of Haiti are considered historically unstable due to various coups d'état,
	regime changes, military juntas and internal conflicts.</p>
	HTML;

$div_wikipedia_Foreign_aid_to_Haiti = new WikipediaContentSection();
$div_wikipedia_Foreign_aid_to_Haiti->setTitleText('Foreign aid to Haiti');
$div_wikipedia_Foreign_aid_to_Haiti->setTitleLink('https://en.wikipedia.org/wiki/Foreign_aid_to_Haiti');
$div_wikipedia_Foreign_aid_to_Haiti->content = <<<HTML
	<p>Haiti has received billions in foreign assistance,
	yet persists as one of the poorest countries and has the lowest human development index in the Americas.
	There have been more than 15 natural disasters since 2001 including tropical storms, flooding, earthquakes and hurricanes.
	The international donor community classifies Haiti as a fragile state.
	Haiti is also considered a post-conflict state—one emerging from a recent coup d'état and civil war.</p>
	HTML;


$page->parent('world.html');
$page->template("stub");

$page->body($div_introduction);
$page->body($div_codeberg_Haiti);



$page->body($h2_A_string_of_bad_luck);
$page->body($div_bad_luck);

$page->body($div_public_debt);
$page->body($div_wikipedia_External_debt_of_Haiti);

$page->body($div_2010_Haiti_earthquake);
$page->body($div_wikipedia_2010_Haiti_earthquake);

$page->body($div_Assassination_of_the_president_of_Haiti);
$page->body($div_wikipedia_Assassination_of_Jovenel_Moise);

$page->body($div_2021_Haiti_earthquake);
$page->body($div_wikipedia_2021_Haiti_earthquake);

$page->body($div_Socioeconomic_crisis);
$page->body($div_wikipedia_2022_Haitian_crisis);


$page->body($h2_Current_challenges);
$page->body($div_Gang_violence);
$page->body($div_wikipedia_External_debt_of_Haiti);
$page->body($div_attacks_on_journalists);
$page->body($div_CPJ_Haiti_Americas);

$page->body($h2_Help);
$page->body($div_wikipedia_United_Nations_Stabilisation_Mission_in_Haiti);
$page->body($div_wikipedia_United_Nations_Mission_for_Justice_Support_in_Haiti);
$page->body($div_United_Nations_Integrated_Office_in_Haiti);
$page->body($div_charities_helping_Haiti);


$page->body('Country indices');

$page->body($div_UN_news_Haiti);

$page->body($div_youtube_Fighting_for_Haiti_CBS_Reports);
$page->body($div_youtube_Divided_island_How_Haiti_and_the_DR_became_two_worlds);

$page->body($div_wikipedia_Haiti);
$page->body($div_wikipedia_Haitian_crisis_2018_present);
$page->body($div_wikipedia_Assassination_of_Jovenel_Moise);
$page->body($div_wikipedia_2021_Haiti_earthquake);
$page->body($div_wikipedia_Politics_of_Haiti);
$page->body($div_wikipedia_Foreign_aid_to_Haiti);
