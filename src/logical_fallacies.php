<?php
$page = new Page();
$page->h1("Logical fallacies");
$page->tags("Information: Discourse", "Political Discourse");
$page->keywords("Logical Fallacy", "logical fallacies");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Formal_fallacy = new WikipediaContentSection();
$div_wikipedia_Formal_fallacy->setTitleText("Formal fallacy");
$div_wikipedia_Formal_fallacy->setTitleLink("https://en.wikipedia.org/wiki/Formal_fallacy");
$div_wikipedia_Formal_fallacy->content = <<<HTML
	<p>In logic and philosophy, a formal fallacy is a pattern of reasoning rendered invalid by a flaw in its logical structure that can neatly be expressed in a standard logic system, for example propositional logic. It is defined as a deductive argument that is invalid. The argument itself could have true premises, but still have a false conclusion. Thus, a formal fallacy is a fallacy in which deduction goes wrong, and is no longer a logical process. This may not affect the truth of the conclusion, since validity and truth are separate in formal logic.</p>
	HTML;

$div_wikipedia_Fallacy = new WikipediaContentSection();
$div_wikipedia_Fallacy->setTitleText("Fallacy");
$div_wikipedia_Fallacy->setTitleLink("https://en.wikipedia.org/wiki/Fallacy");
$div_wikipedia_Fallacy->content = <<<HTML
	<p>A fallacy is the use of invalid or otherwise faulty reasoning in the construction of an argument that may appear to be well-reasoned if unnoticed. The term was introduced in the Western intellectual tradition by the Aristotelian De Sophisticis Elenchis.</p>

	<p>Fallacies may be committed intentionally to manipulate or persuade by deception, unintentionally because of human limitations such as carelessness, cognitive or social biases and ignorance, or potentially due to the limitations of language and understanding of language. These delineations include not only the ignorance of the right reasoning standard but also the ignorance of relevant properties of the context. For instance, the soundness of legal arguments depends on the context in which they are made.</p>
	HTML;


$page->parent('political_discourse.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Logical Fallacy");
$page->body($div_wikipedia_Formal_fallacy);
$page->body($div_wikipedia_Fallacy);
