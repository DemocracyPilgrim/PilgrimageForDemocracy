<?php
$page = new OrganisationPage();
$page->h1("Approval Voting Party");
$page->tags("Organisation", "Approval Voting", "Colorado", "Elections");
$page->keywords("Approval Voting Party");
$page->stars(2);
$page->viewport_background("/free/approval_voting_party.png");

$page->snp("description", "Party promoting implementing approval voting in the United States.");
$page->snp("image",       "/free/approval_voting_party.1200-630.png");

$page->preview( <<<HTML
	<p></p>
	HTML );

$r1 = $page->ref("https://www.approvalvotingparty.com/platform/", "Approval Voting Party Platform");
$r2 = $page->ref("https://ballotpedia.org/Presidential_candidates,_2024", "Presidential candidates, 2024");
$r3 = $page->ref("https://ballotpedia.org/Presidential_candidates,_2020", "Presidential candidates, 2020");
$r4 = $page->ref("https://www.approvalvotingparty.com/utah-voting-methods-analysis/", "Utah Association of Counties Voting Methods Analysis");
// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>The Approval Voting Party (AVP) is a political party dedicated to implementing ${'approval voting'} in the United States.</p>

	<p>The party platform is composed of the following two policy issues: $r1</p>

	<ul>
		<li>Replacing the First-Past-The-Post ${'Plurality Voting'} system with Approval Voting for all political $elections.</li>
		<li>Eliminate $Gerrymandering by moving from single-winner districts to multi-winner districts using proportional ${'Approval Voting'}.</li>
	</ul>

	<p>The Approval Voting Party is a recognized minor party in the state of $Colorado.</p>

	HTML;



$div_Electoral_history = new ContentSection();
$div_Electoral_history->content = <<<HTML
	<h3>Electoral history</h3>

	<p><strong>2024 presidential election</strong>: the presidential candidate was Blake Huber and the vice-presidential candidate was Andrea Denault.
	The party had ballot access in the state of $Colorado, where they received 2,134 votes, which is also their nationwide results.$r2</p>

	<p><strong>2020 presidential election</strong>: the presidential candidate was Blake Huber and the vice-presidential candidate was Frank Atwood.
	The party received 409 votes nationwide, including 355 in the state of Colorado. $r3</p>
	HTML;


$div_Research_and_advocacy = new ContentSection();
$div_Research_and_advocacy->content = <<<HTML
	<h3>Research and advocacy</h3>

	<p>The Approval Voting Party conducted a ${'voting method'} research on behalf of the Utah Association of Counties. $r4
	They compare Ranked Choice Voting to Approval Voting analysing important features and performance of both voting methods.</p>
	HTML;



$div_Approval_Voting_Party = new WebsiteContentSection();
$div_Approval_Voting_Party->setTitleText("Approval Voting Party");
$div_Approval_Voting_Party->setTitleLink("https://www.approvalvotingparty.com/");
$div_Approval_Voting_Party->content = <<<HTML
	<p>Official Party Website</p>
	HTML;




$div_wikipedia_Approval_Voting_Party = new WikipediaContentSection();
$div_wikipedia_Approval_Voting_Party->setTitleText("Approval Voting Party");
$div_wikipedia_Approval_Voting_Party->setTitleLink("https://en.wikipedia.org/wiki/Approval_Voting_Party");
$div_wikipedia_Approval_Voting_Party->content = <<<HTML
	<p>The Approval Voting Party (AVP) is a single-issue American political party dedicated to implementing approval voting in the United States. In 2019, the party became recognized as a minor party in Colorado.</p>
	HTML;


$page->parent('list_of_organisations.html');
$page->body($div_introduction);


$page->body($div_Electoral_history);
$page->body($div_Research_and_advocacy);

$page->body($div_Approval_Voting_Party);
$page->related_tag("Approval Voting Party");
$page->body($div_wikipedia_Approval_Voting_Party);
