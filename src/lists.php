<?php
$page = new Page();
$page->h1('Lists and topics');
$page->tags("Topic portal");
$page->keywords("Topic portal");
$page->stars(2);
$page->viewport_background('/free/lists.png');

$page->preview( <<<HTML
	<p>List of lists and topical entry pages.</p>
	HTML );

$page->snp('description', 'An entrance into the rabbit hole...');
$page->snp('image', '/free/lists.1200-630.png');

$h2_Lists = new h2HeaderContent('Lists');


$div_lists = new ContentSection();
$div_lists->content = <<<HTML
	<p>List of lists.</p>
	HTML;


$h2_Topics = new h2HeaderContent('Topics');

$div_topics = new ContentSection();
$div_topics->content = <<<HTML
	<p>Here are the main topical entry pages.</p>
	HTML;




$page->parent('menu.html');

$page->body($h2_Lists);
$page->body($div_lists);
$page->body('list_of_awards.html');
$page->body('list_of_books.html');
$page->body('list_of_documentaries.html');
$page->body('list_of_indices.html');
$page->body('list_of_movies.html');
$page->body('list_of_organisations.html');
$page->body('list_of_people.html');
$page->body('list_of_research_and_reports.html');
$page->body('list_of_videos.html');
$page->body('world.html');

$page->body($h2_Topics);
$page->body($div_topics);

$page->body('democracy.html');

$page->body('individuals.html');
$page->body('society.html');
$page->body('institutions.html');
$page->body('elections.html');
$page->body('information.html');
$page->body('living.html');
$page->body('international.html');
$page->body('humanity.html');

$page->body('wip.html');
$page->body('teamwork.html');
$page->body('spirit.html');
$page->body('education.html');
$page->body('taxes.html');
$page->body('environment.html');
$page->body('web.html');
