<?php
$page = new Page();
$page->h1("Constitution Annotated");
$page->tags("Report", "USA", "Constitution", "Institutions: Legislative", "SCOTUS");
$page->keywords("Constitution Annotated");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

$r1 = $page->ref("https://constitution.congress.gov/about/constitution-annotated/", "About the Constitution Annotated");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;



$div_Constitution_Annotated = new WebsiteContentSection();
$div_Constitution_Annotated->setTitleText("Constitution Annotated ");
$div_Constitution_Annotated->setTitleLink("https://constitution.congress.gov/");
$div_Constitution_Annotated->content = <<<HTML
	<p>The Constitution of the United States of America: Analysis and Interpretation ("Constitution Annotated" or "CONAN")
	provides a legal analysis and interpretation of the United States Constitution based on a comprehensive review of Supreme Court case law
	and, where relevant, historical practices that have defined the text of the Constitution.
	This regularly updated resource is written in "plain English" and useful for a wide audience:
	from constitutional scholars to those just beginning to learn about the nation's most important legal document.$r1</p>
	HTML;



$div_wikipedia_The_Constitution_of_the_United_States_of_America_Analysis_and_Interpretation = new WikipediaContentSection();
$div_wikipedia_The_Constitution_of_the_United_States_of_America_Analysis_and_Interpretation->setTitleText("The Constitution of the United States of America Analysis and Interpretation");
$div_wikipedia_The_Constitution_of_the_United_States_of_America_Analysis_and_Interpretation->setTitleLink("https://en.wikipedia.org/wiki/The_Constitution_of_the_United_States_of_America:_Analysis_and_Interpretation");
$div_wikipedia_The_Constitution_of_the_United_States_of_America_Analysis_and_Interpretation->content = <<<HTML
	<p>The Constitution of the United States of America: Analysis and Interpretation (popularly known as the Constitution Annotated or CONAN)
	is a publication encompassing the United States Constitution with analysis and interpretation by the Congressional Research Service
	along with in-text annotations of cases decided by the Supreme Court of the United States.
	The centennial edition of the Constitution Annotated was published in 2013 by the 112th Congress,
	containing more than 2,300 pages and referencing almost 6,000 cases.</p>
	HTML;


$page->parent('list_of_research_and_reports.html');
$page->template("stub");
$page->body($div_introduction);

$page->body($div_Constitution_Annotated);

$page->body($div_wikipedia_The_Constitution_of_the_United_States_of_America_Analysis_and_Interpretation);
