<?php
$page = new Page();
$page->h1("Open Source Democracy");
$page->tags("Information");
$page->keywords("Open Source Democracy", "Open Source");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;


$page->parent('information.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Open Source");
