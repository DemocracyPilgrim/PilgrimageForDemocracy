<?php
$page = new Page();
$page->h1("Bootleggers and Baptists");
$page->tags("Information: Discourse", "Disinformation");
$page->keywords("Bootleggers and Baptists");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Bootleggers_and_Baptists = new WikipediaContentSection();
$div_wikipedia_Bootleggers_and_Baptists->setTitleText("Bootleggers and Baptists");
$div_wikipedia_Bootleggers_and_Baptists->setTitleLink("https://en.wikipedia.org/wiki/Bootleggers_and_Baptists");
$div_wikipedia_Bootleggers_and_Baptists->content = <<<HTML
	<p>Bootleggers and Baptists is a concept put forth by regulatory economist Bruce Yandle, derived from the observation that regulations are supported both by groups that want the ostensible purpose of the regulation, and by groups that profit from undermining that purpose.</p>
	HTML;


$page->parent('disinformation.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Bootleggers and Baptists");
$page->body($div_wikipedia_Bootleggers_and_Baptists);
