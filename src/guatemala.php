<?php
$page = new CountryPage('Guatemala');
$page->h1('Guatemala');
$page->tags("Country");
$page->keywords('Guatemala');
$page->stars(0);

$page->snp('description', '18 million inhabitants.');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Guatemala = new WikipediaContentSection();
$div_wikipedia_Guatemala->setTitleText('Guatemala');
$div_wikipedia_Guatemala->setTitleLink('https://en.wikipedia.org/wiki/Guatemala');
$div_wikipedia_Guatemala->content = <<<HTML
	<p>Guatemala, officially the Republic of Guatemala, is a country in Central America.
	It is bordered to the north and west by Mexico, to the northeast by Belize, to the east by Honduras, to the southeast by El Salvador.
	It is touched to the south by the Pacific Ocean and to the northeast by the Gulf of Honduras.
	With an estimated population of around 17.6 million, Guatemala is the most populous country in Central America.</p>
	HTML;

$div_wikipedia_Politics_of_Guatemala = new WikipediaContentSection();
$div_wikipedia_Politics_of_Guatemala->setTitleText('Politics of Guatemala');
$div_wikipedia_Politics_of_Guatemala->setTitleLink('https://en.wikipedia.org/wiki/Politics_of_Guatemala');
$div_wikipedia_Politics_of_Guatemala->content = <<<HTML
	<p>Politics of Guatemala takes place in a framework of a presidential representative democratic republic,
	where by the President of Guatemala is both head of state, head of government, and of a multi-party system.
	Executive power is exercised by the government. Legislative power is vested in both the government and the Congress of the Republic.
	The judiciary is independent of the executive and the legislature. Guatemala is a Constitutional Republic.</p>
	HTML;

$div_wikipedia_Human_rights_in_Guatemala = new WikipediaContentSection();
$div_wikipedia_Human_rights_in_Guatemala->setTitleText('Human rights in Guatemala');
$div_wikipedia_Human_rights_in_Guatemala->setTitleLink('https://en.wikipedia.org/wiki/Human_rights_in_Guatemala');
$div_wikipedia_Human_rights_in_Guatemala->content = <<<HTML
	<p>Human rights is an issue in Guatemala.
	The establishment of the International Commission against Impunity in Guatemala
	has helped the Attorney General prosecute extrajudicial killings and corruption.
	There remains widespread impunity for abusers from the Guatemalan Civil War, which ran from 1960 to 1996,
	and Human Rights Watch considers threats and violence against unionists, journalists and lawyers a major concern.
	A trial for eight former Army members on charges related to the alleged disappearances of 130 people
	whose bodies were found among 550 at a base now run by the UN called CREOMPAZ has been stalled since it began in 2016
	due to witness intimidation, among other factors.</p>
	HTML;

$div_wikipedia_Violence_against_women_in_Guatemala = new WikipediaContentSection();
$div_wikipedia_Violence_against_women_in_Guatemala->setTitleText('Violence against women in Guatemala');
$div_wikipedia_Violence_against_women_in_Guatemala->setTitleLink('https://en.wikipedia.org/wiki/Violence_against_women_in_Guatemala');
$div_wikipedia_Violence_against_women_in_Guatemala->content = <<<HTML
	<p>Violence against women in Guatemala reached severe levels during the long-running Guatemalan Civil War (1960-1996),
	and the continuing impact of that conflict has contributed to the present high levels of violence against women in that nation.
	During the armed conflict, rape was used as a weapon of war.</p>
	HTML;


$page->parent('world.html');
$page->template("stub");
$page->body($div_introduction);
$page->body('Country indices');

$page->body($div_wikipedia_Guatemala);
$page->body($div_wikipedia_Politics_of_Guatemala);
$page->body($div_wikipedia_Human_rights_in_Guatemala);
$page->body($div_wikipedia_Violence_against_women_in_Guatemala);
