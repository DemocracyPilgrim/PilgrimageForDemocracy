<?php
$page = new Page();
$page->h1("Mary B. McCord");
$page->alpha_sort("McCord, Mary");
$page->tags("Person", "USA", "Institutions: Judiciary", "Lawyer");
$page->keywords("Mary McCord");
$page->stars(0);
$page->viewport_background("/free/mary_mccord.png");

$page->snp("description", "American lawyer and security analyst.");
$page->snp("image",       "/free/mary_mccord.1200-630.png");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Mary B. McCord is an American lawyer, national security analyst, and former government official.
	Together with ${'Andrew Weissmann'}, she is the host the award-winning podcast ${"Prosecuting Donald Trump"}.</p>
	HTML;

$div_wikipedia_Mary_B_McCord = new WikipediaContentSection();
$div_wikipedia_Mary_B_McCord->setTitleText("Mary B. McCord");
$div_wikipedia_Mary_B_McCord->setTitleLink("https://en.wikipedia.org/wiki/Mary_B._McCord");
$div_wikipedia_Mary_B_McCord->content = <<<HTML
	<p>Mary B. McCord is an American lawyer, national security analyst, and former government official.
	For almost 20 years, McCord served as a federal prosecutor in the office of the United States Attorney for the District of Columbia.
	She was also Principal Deputy Assistant Attorney General and Acting Assistant Attorney General for National Security at the U.S. Department of Justice.
	McCord has written articles on the rule of law and domestic terrorism and has appeared on televised media outlets as a national security analyst.</p>
	HTML;


$page->parent('list_of_people.html');
$page->template("stub");
$page->body($div_introduction);
$page->related_tag("Mary McCord");


$page->body($div_wikipedia_Mary_B_McCord);
