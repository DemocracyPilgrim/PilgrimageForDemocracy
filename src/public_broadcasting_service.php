<?php
$page = new Page();
$page->h1("Public Broadcasting Service");
$page->tags("Organisation", "Information: Media");
$page->keywords("Public Broadcasting Service", "PBS");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_PBS = new WikipediaContentSection();
$div_wikipedia_PBS->setTitleText("PBS");
$div_wikipedia_PBS->setTitleLink("https://en.wikipedia.org/wiki/PBS");
$div_wikipedia_PBS->content = <<<HTML
	<p>The Public Broadcasting Service (PBS) is an American public broadcaster and non-commercial, free-to-air television network based in Arlington, Virginia.
	PBS is a publicly funded nonprofit organization and the most prominent provider of educational programs to public television stations in the United States,
	distributing shows such as Frontline, Nova, PBS News Hour, Masterpiece, Sesame Street, and This Old House.</p>
	HTML;


$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_wikipedia_PBS);
