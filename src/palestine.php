<?php
$page = new CountryPage('Palestine');
$page->h1('Palestine');
$page->tags("Country");
$page->keywords('Palestine', 'Palestinian', 'Palestinians', 'Gaza');
$page->stars(0);

$page->snp('description', '5.4 million inhabitants.');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_State_of_Palestine = new WikipediaContentSection();
$div_wikipedia_State_of_Palestine->setTitleText('State of Palestine');
$div_wikipedia_State_of_Palestine->setTitleLink('https://en.wikipedia.org/wiki/State_of_Palestine');
$div_wikipedia_State_of_Palestine->content = <<<HTML
	<p>Palestine is a state located in the Southern Levant region of West Asia.
	Officially governed by the Palestine Liberation Organization (PLO),
	it claims the West Bank (including East Jerusalem) and the Gaza Strip as its territory,
	though the entirety of that territory has been under Israeli occupation since the 1967 Six-Day War.
	The West Bank is currently divided into 165 Palestinian enclaves that are under partial Palestinian rule,
	but the remainder, including 200 Israeli settlements, is under full Israeli control.
	The Gaza Strip is ruled by the militant group Hamas and has been blockaded by Israel and Egypt since 2007.</p>
	HTML;


$page->parent('world.html');
$page->template("stub");
$page->body($div_introduction);
$page->body('Country indices');
$page->body('israel_palestine.html');

$page->body($div_wikipedia_State_of_Palestine);
