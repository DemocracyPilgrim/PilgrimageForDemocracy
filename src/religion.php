<?php
$page = new Page();
$page->h1('Religion');
$page->keywords('Religion', 'religion', 'Religions', 'religions', 'religious');
$page->tags("Individual: Liberties");
$page->stars(1);

//$page->snp('description', '');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );

$r1 = $page->ref('https://en.wikipedia.org/wiki/List_of_religious_populations', 'List of religious populations');
$r2 = $page->ref('https://www.pewresearch.org/religion/2022/12/21/key-findings-from-the-global-religious-futures-project/', 'Key Findings From the Global Religious Futures Project');


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>According to a study by the Pew Research Center,
	about seven billion people are adherent of some form of religion,
	leaving about 1.2 billion secular/nonreligious/agnostic/atheist people
	out of over 8 billion Earth population.</p>

	<p>${'Freedom of conscience'}, and thus religious $freedom, is one of the most important tenets of $democracy,
	and part of the ${'first level of democracy'}.</p>

	<p>For these reasons, religious tolerance and interfaith dialogue are critical topics to achieve global peace and democracy.</p>
	HTML;


$list_Religion = ListOfPeoplePages::WithTags("Religion");
$print_list_Religion = $list_Religion->print();

$div_list_Religion = new ContentSection();
$div_list_Religion->content = <<<HTML
	<h3>Related content</h3>

	$print_list_Religion
	HTML;




$div_freedomhouse_Protecting_the_Rights_of_Religious_Minorities_is_Crucial_to_Protecting_Democracy_Itself = new FreedomHouseContentSection();
$div_freedomhouse_Protecting_the_Rights_of_Religious_Minorities_is_Crucial_to_Protecting_Democracy_Itself->setTitleText("Protecting the Rights of Religious Minorities is Crucial to Protecting Democracy Itself");
$div_freedomhouse_Protecting_the_Rights_of_Religious_Minorities_is_Crucial_to_Protecting_Democracy_Itself->setTitleLink("https://freedomhouse.org/article/protecting-rights-religious-minorities-crucial-protecting-democracy-itself");
$div_freedomhouse_Protecting_the_Rights_of_Religious_Minorities_is_Crucial_to_Protecting_Democracy_Itself->content = <<<HTML
	<p>When the beliefs of a religious majority infringe upon the rights of smaller groups, civil society must push back.</p>

	<p>The freedom of religion and belief—that people have the right to believe and worship, or not, as they so choose—is an integral component of democracy.
	Often, discussions on the concept of religious freedom focus on bringing together Christians, Muslims, and Jews
	to discuss their differences—in other words, creating interfaith dialogue.
	While such efforts are important, a full understanding of religious freedom and its importance to democracy
	encompasses more than just promoting and enabling interfaith dialogue.
	When talking about freedom of religion or belief,
	it is also critical to look at the very real social and political impacts of a trend that undermines religious freedom:
	religious majoritarianism, a view that dismisses minority protection and elevates the beliefs of only the majority.</p>
	HTML;


$div_wikipedia_Religion = new WikipediaContentSection();
$div_wikipedia_Religion->setTitleText('Religion');
$div_wikipedia_Religion->setTitleLink('https://en.wikipedia.org/wiki/Religion');
$div_wikipedia_Religion->content = <<<HTML
	test
	<p>Religion is a range of social-cultural systems, including designated behaviors and practices,
	morals, beliefs, worldviews, texts, sanctified places, prophecies, ethics, or organizations,
	that generally relate humanity to supernatural, transcendental, and spiritual elements—although
	there is no scholarly consensus over what precisely constitutes a religion.</p>
	HTML;


$page->parent('individuals.html');
$page->template("stub");
$page->body($div_introduction);

$page->body($div_list_Religion);
$page->body('freedom_of_conscience.html');

$page->body($div_freedomhouse_Protecting_the_Rights_of_Religious_Minorities_is_Crucial_to_Protecting_Democracy_Itself);
$page->body($div_wikipedia_Religion);
