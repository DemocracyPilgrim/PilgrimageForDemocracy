<?php
$page = new Page();
$page->h1('Disfranchisement');
$page->keywords('disfranchisement');
$page->tags("Elections", "USA", "Electoral System");
$page->stars(1);

//$page->snp('description', '');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>The topic of voting $rights and restrictions (or ${'voter suppression'}) in the ${"United States"} has been extensively documented on $Wikipedia.
	Voter disfranchisement is often done under the pretext of "${'election integrity'}".

	<p>The $Pilgrimage aims to go further by comparing voting practices in various $democracies around the $world.
	This comparison will help evaluate the extent to which voting restrictions in the $USA
	align with international standards and best practices.</p>
	HTML;

$div_wikipedia_Disfranchisement = new WikipediaContentSection();
$div_wikipedia_Disfranchisement->setTitleText('Disfranchisement');
$div_wikipedia_Disfranchisement->setTitleLink('https://en.wikipedia.org/wiki/Disfranchisement');
$div_wikipedia_Disfranchisement->content = <<<HTML
	<p>Disfranchisement, also called disenfranchisement, or voter disqualification is
	the restriction of suffrage (the right to vote) of a person or group of people,
	or a practice that has the effect of preventing a person exercising the right to vote.
	Disfranchisement can also refer to the revocation of power or control of a particular individual, community or being to the natural amenity they have;
	that is to deprive of a franchise, of a legal right, of some privilege or inherent immunity.
	Disfranchisement may be accomplished explicitly by law or implicitly
	through requirements applied in a discriminatory fashion, through intimidation,
	or by placing unreasonable requirements on voters for registration or voting.
	High barriers to entry to the political competition can disenfranchise political movements.</p>
	HTML;

$div_wikipedia_Voter_suppression_in_the_United_States = new WikipediaContentSection();
$div_wikipedia_Voter_suppression_in_the_United_States->setTitleText('Voter suppression in the United States');
$div_wikipedia_Voter_suppression_in_the_United_States->setTitleLink('https://en.wikipedia.org/wiki/Voter_suppression_in_the_United_States');
$div_wikipedia_Voter_suppression_in_the_United_States->content = <<<HTML
	<p>Voter suppression in the United States consists of various legal and illegal efforts
	to prevent eligible citizens from exercising their right to vote.
	Such voter suppression efforts vary by state, local government, precinct, and election.
	Voter suppression has historically been used for racial, economic, gender, age and disability discrimination.</p>
	HTML;




$div_wikipedia_Republican_efforts_to_restrict_voting_following_the_2020_presidential_election = new WikipediaContentSection();
$div_wikipedia_Republican_efforts_to_restrict_voting_following_the_2020_presidential_election->setTitleText("Republican efforts to restrict voting following the 2020 presidential election");
$div_wikipedia_Republican_efforts_to_restrict_voting_following_the_2020_presidential_election->setTitleLink("https://en.wikipedia.org/wiki/Republican_efforts_to_restrict_voting_following_the_2020_presidential_election");
$div_wikipedia_Republican_efforts_to_restrict_voting_following_the_2020_presidential_election->content = <<<HTML
	<p>Following the 2020 United States presidential election
	and the unsuccessful attempts by Donald Trump and various other Republican officials to overturn it,
	Republican lawmakers initiated a sweeping effort to make voting laws more restrictive within several states across the country.
	According to the Brennan Center for Justice, as of October 4, 2021, more than 425 bills that would restrict voting access
	have been introduced in 49 states—with 33 of these bills enacted across 19 states so far.
	The bills are largely centered around limiting mail-in voting, strengthening voter ID laws, shortening early voting,
	eliminating automatic and same-day voter registration, curbing the use of ballot drop boxes,
	and allowing for increased purging of voter rolls.
	Republicans in at least eight states have also introduced bills that would give lawmakers greater power over election administration
	after they were unsuccessful in their attempts to overturn election results in swing states won by Democratic candidate Joe Biden in the 2020 election.
	The efforts garnered press attention and public outrage from Democrats,
	and by 2023 Republicans had adopted a more "under the radar" approach to achieve their goals.</p>
	HTML;



$page->parent('election_integrity.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_wikipedia_Disfranchisement);
$page->body($div_wikipedia_Voter_suppression_in_the_United_States);
$page->body($div_wikipedia_Republican_efforts_to_restrict_voting_following_the_2020_presidential_election);
