<?php
$page = new CountryPage('Mexico');
$page->h1('Mexico');
$page->tags("Country");
$page->keywords('Mexico');
$page->stars(0);

$page->snp('description', '129.8 million inhabitants.');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Mexico = new WikipediaContentSection();
$div_wikipedia_Mexico->setTitleText('Mexico');
$div_wikipedia_Mexico->setTitleLink('https://en.wikipedia.org/wiki/Mexico');
$div_wikipedia_Mexico->content = <<<HTML
	<p>The United Mexican States, is a country in the southern portion of North America.
	It is bordered to the north by the United States;
	to the south and west by the Pacific Ocean; to the southeast by Guatemala, Belize, and the Caribbean Sea;
	and to the east by the Gulf of Mexico.</p>
	HTML;

$div_wikipedia_Politics_of_Mexico = new WikipediaContentSection();
$div_wikipedia_Politics_of_Mexico->setTitleText('Politics of Mexico');
$div_wikipedia_Politics_of_Mexico->setTitleLink('https://en.wikipedia.org/wiki/Politics_of_Mexico');
$div_wikipedia_Politics_of_Mexico->content = <<<HTML
	<p>The politics of Mexico function within a framework of a federal presidential representative democratic republic
	whose government is based on a multi-party congressional system , where the President of Mexico is both head of state and head of government.
	The federal government represents the United Mexican States and is divided into three branches: executive, legislative and judicial,
	as established by the Political Constitution of the United Mexican States, published in 1917.</p>
	HTML;

$div_wikipedia_History_of_democracy_in_Mexico = new WikipediaContentSection();
$div_wikipedia_History_of_democracy_in_Mexico->setTitleText('History of democracy in Mexico');
$div_wikipedia_History_of_democracy_in_Mexico->setTitleLink('https://en.wikipedia.org/wiki/History_of_democracy_in_Mexico');
$div_wikipedia_History_of_democracy_in_Mexico->content = <<<HTML
	<p>The history of democracy in Mexico dates to the establishment of the federal republic of Mexico in 1824.</p>
	HTML;

$div_wikipedia_Human_trafficking_in_Mexico = new WikipediaContentSection();
$div_wikipedia_Human_trafficking_in_Mexico->setTitleText('Human trafficking in Mexico');
$div_wikipedia_Human_trafficking_in_Mexico->setTitleLink('https://en.wikipedia.org/wiki/Human_trafficking_in_Mexico');
$div_wikipedia_Human_trafficking_in_Mexico->content = <<<HTML
	<p>Human trafficking is the trade of humans, most commonly for the purpose of forced labour, sexual slavery,
	or commercial sexual exploitation for the trafficker or others.
	Mexico is a large source, transit, and destination country for victims of human trafficking.
	Government and NGO statistics indicate that the magnitude of forced labor surpasses that of forced prostitution in Mexico.
	Groups considered most vulnerable to human trafficking in Mexico include women, children, indigenous persons, and undocumented migrants.</p>
	HTML;

$div_wikipedia_Femicide_in_Mexico = new WikipediaContentSection();
$div_wikipedia_Femicide_in_Mexico->setTitleText('Femicide in Mexico');
$div_wikipedia_Femicide_in_Mexico->setTitleLink('https://en.wikipedia.org/wiki/Femicide_in_Mexico');
$div_wikipedia_Femicide_in_Mexico->content = <<<HTML
	<p>Femicide is the act of murdering women, because they are women.
	Mexico, particularly in Ciudad Juárez, is one of the leading countries in the amount of feminicides that occur each year,
	with as much as 3% of murder victims being classified as feminicide.</p>
	HTML;


$page->parent('world.html');
$page->template("stub");
$page->body($div_introduction);
$page->body('Country indices');

$page->body($div_wikipedia_Mexico);
$page->body($div_wikipedia_Politics_of_Mexico);
$page->body($div_wikipedia_History_of_democracy_in_Mexico);
$page->body($div_wikipedia_Human_trafficking_in_Mexico);
$page->body($div_wikipedia_Femicide_in_Mexico);
