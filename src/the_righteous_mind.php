<?php
$page = new BookPage();
$page->h1("The Righteous Mind: Why Good People are Divided by Politics and Religion");
$page->tags("Book", "USA", "Religion", "Political Discourse", "Jonathan Haidt");
$page->keywords("The Righteous Mind");
$page->stars(1);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>"The Righteous Mind: Why Good People are Divided by Politics and Religion" is a book by ${'Jonathan Haidt'}.
	It is highly relevant to the way we conduct ${'political discourse'}.</p>
	HTML;


$div_emotions_reason_and_the_moral_matrix = new ContentSection();
$div_emotions_reason_and_the_moral_matrix->content = <<<HTML
	<h3>Emotions, Reason and the Moral Matrix</h3>

	<p>According to ${'Jonathan Haidt'}, emotions come first, and they heavily influence our logical reasoning.
	He argues that our moral intuitions, which are often based on emotions, are the foundation of our moral judgments, not the other way around.</p>

	<p>Intuitions are the foundation.
	He uses the metaphor of a "rider" (reason) on an "elephant" (emotions).
	The elephant, representing emotions and intuitions, is much larger and stronger than the rider.
	The rider can sometimes steer the elephant, but often, the elephant takes the lead, pulling the rider along with it.</p>


	<p>Haidt proposes that our moral judgments are shaped by six fundamental moral intuitions, constituting our moral matrix:</p>

	<ul>
		<li>Care/Harm</li>
		<li>Fairness/Cheating</li>
		<li>Loyalty/Betrayal</li>
		<li>Authority/Subversion</li>
		<li>Sanctity/Degradation</li>
		<li>Liberty/Oppression</li>
	</ul>

	<p>These intuitions are often triggered by emotions before we consciously engage in logical reasoning.
	He argues that people often arrive at their moral judgments quickly and intuitively, and then use reason to justify those judgments after the fact.
	This is known as the "intuitionist" approach to moral reasoning.
	Think about a situation where someone is being treated unfairly.
	You might immediately feel anger or indignation, which leads you to believe that the situation is wrong.
	Then, you might use reason to justify your feelings, citing principles of fairness or equality.</p>

	<p>Haidt's theory suggests that our moral judgments are often guided by our emotions, which can sometimes lead to biased or irrational decisions.
	However, he also argues that acknowledging the role of emotions in moral reasoning can help us to better understand and navigate moral conflicts.</p>
	HTML;



$div_wikipedia_The_Righteous_Mind = new WikipediaContentSection();
$div_wikipedia_The_Righteous_Mind->setTitleText("The Righteous Mind");
$div_wikipedia_The_Righteous_Mind->setTitleLink("https://en.wikipedia.org/wiki/The_Righteous_Mind");
$div_wikipedia_The_Righteous_Mind->content = <<<HTML
	<p>The Righteous Mind: Why Good People are Divided by Politics and Religion is a 2012 social psychology book by Jonathan Haidt,
	in which the author describes human morality as it relates to politics and religion.</p>

	<p>In the first section, Haidt demonstrates that people's beliefs are driven primarily by intuition,
	with reason operating mostly to justify beliefs that are intuitively obvious.
	In the second section, he lays out his theory that the human brain is organized to respond to several distinct types of moral violations,
	much like a tongue is organized to respond to different sorts of foods. In the last section,
	Haidt proposes that humans have an innate capacity to sometimes be "groupish" rather than "selfish".</p>
	HTML;


$page->parent('list_of_books.html');

$page->body($div_introduction);
$page->body($div_emotions_reason_and_the_moral_matrix);


$page->body($div_wikipedia_The_Righteous_Mind);
