<?php
$page = new BookPage();
$page->h1("All labor has dignity");
$page->keywords("All labor has dignity");
$page->stars(1);
$page->tags("Book", "Fair Share");

$page->snp("description", "Collection of Martin Luther King, Jr speeches on labor and civil rights.");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p>Collection of Martin Luther King, Jr speeches on labor and civil rights.</p>
	HTML );

// $r1 = $page->ref('', '');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>"All labor has dignity" is a collection of Martin Luther King, Jr speeches on $labor and ${'civil rights'},
	edited by ${'Michael Honey'}.</p>
	HTML;

$div_All_labor_has_dignity = new ContentSection();
$div_All_labor_has_dignity->content = <<<HTML
	<h3>All labor has dignity</h3>

	<div class="download-pdf">
			<img src="/copyrighted/all_labor_has_dignity.jpg">
	</div>
	<blockquote>
	You are doing many things here in this struggle.
	You are demanding that this city will respect the dignity of labor.
	So often we overlook the work and the significance of those who are not in professional jobs, of those who are not in the so-called big jobs.
	But let me say to you tonight, that whenever you are engaged in work that serves humanity and is for the building of humanity,
	it has dignity, and it has worth.
	One day our society must come to see this.
	One day our society will come to respect the sanitation worker if it is to survive,
	for the person who picks up our garbage, in the final analysis,
	is as significant as the physician, for if he doesn't do his job, diseases are rampant.
	All labor has dignity.
	<br>
	<br>
	-- Dr. Martin Luther King, Jr., March 18, 1968, Memphis, TN.
	</blockquote>
	HTML;



$div_Beacon_Press_All_Labor_Has_Dignity = new WebsiteContentSection();
$div_Beacon_Press_All_Labor_Has_Dignity->setTitleText("Beacon Press: All Labor Has Dignity");
$div_Beacon_Press_All_Labor_Has_Dignity->setTitleLink("https://www.beacon.org/All-Labor-Has-Dignity-P921.aspx");
$div_Beacon_Press_All_Labor_Has_Dignity->content = <<<HTML
	<p>People forget that Dr. King was every bit as committed to economic justice as he was to ending racial segregation.
	He fought throughout his life to connect the labor and civil rights movements, envisioning them as twin pillars for social reform.
	As we struggle with massive unemployment, a staggering racial wealth gap, and the near collapse of a financial system that puts profits before people,
	King’s prophetic writings and speeches underscore his relevance for today.
	They help us imagine King anew: as a human rights leader whose commitment to unions and an end to poverty was a crucial part of his civil rights agenda.</p>

	<p>Covering all the civil rights movement highlights—Montgomery, Albany, Birmingham, Selma, Chicago,
	and Memphis—award-winning historian Michael K. Honey introduces and traces King’s dream of economic equality.
	Gathered in one volume for the first time, the majority of these speeches will be new to most readers.
	The collection begins with King’s lectures to unions in the 1960s and includes his addresses during his Poor People’s Campaign,
	culminating with his momentous “Mountaintop” speech, delivered in support of striking black sanitation workers in Memphis.
	Unprecedented and timely, “All Labor Has Dignity” will more fully restore our understanding of King’s lasting vision of economic justice,
	bringing his demand for equality right into the present.</p>
	HTML;




$page->parent('list_of_books.html');
$page->template("stub");
$page->body($div_introduction);

$page->body($div_All_labor_has_dignity);
$page->body($div_Beacon_Press_All_Labor_Has_Dignity);
