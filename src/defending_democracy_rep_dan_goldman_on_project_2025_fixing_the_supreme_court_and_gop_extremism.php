<?php
$page = new VideoPage();
$page->h1("Defending Democracy: Rep. Dan Goldman on Project 2025, Fixing the Supreme Court and GOP Extremism");
$page->tags("Video: Interview", "Dan Goldman", "Marc Elias", "Defending Democracy Podcast", "SCOTUS", "Republican Party Evolution");
$page->stars(1);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;


$div_youtube_Rep_Dan_Goldman_on_Project_2025_Fixing_the_Supreme_Court_and_GOP_Extremism = new YoutubeContentSection();
$div_youtube_Rep_Dan_Goldman_on_Project_2025_Fixing_the_Supreme_Court_and_GOP_Extremism->setTitleText("Rep. Dan Goldman on Project 2025, Fixing the Supreme Court and GOP Extremism");
$div_youtube_Rep_Dan_Goldman_on_Project_2025_Fixing_the_Supreme_Court_and_GOP_Extremism->setTitleLink("https://www.youtube.com/watch?v=oq0kSRjYaIM");
$div_youtube_Rep_Dan_Goldman_on_Project_2025_Fixing_the_Supreme_Court_and_GOP_Extremism->content = <<<HTML
	<p>New York Congressman Dan Goldman (D) joins Marc Elias to discuss why he’s made voting rights a top issue, the major concerns he has about Project 2025, efforts to fix the U.S. Supreme Court and Republican voter suppression.</p>
	HTML;



$page->parent('list_of_videos.html');
$page->template("stub");
$page->body($div_introduction);

$page->body($div_youtube_Rep_Dan_Goldman_on_Project_2025_Fixing_the_Supreme_Court_and_GOP_Extremism);
