<?php
$page = new Page();
$page->h1('Credo23');
$page->keywords('Credo23');
$page->tags("Organisation", "Artificial Intelligence");
$page->stars(0);

//$page->snp('description', '');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;


$div_Credo23_website = new WebsiteContentSection();
$div_Credo23_website->setTitleText('Credo23 website ');
$div_Credo23_website->setTitleLink('https://credo23.com/');
$div_Credo23_website->content = <<<HTML
	<p>The CREDO 23 stamp on a project assures the audience of certain standards.
	We guarantee to our audience that we are giving them the best possible films/series. Films/series that were sweated over, that were bled for.
	In the tradition of Lars von Trier’s Dogme95, we make a way for filmmakers to differentiate their work, and for audiences to trust what they are watching.</p>
	HTML;


$div_wikipedia_Justine_Bateman = new WikipediaContentSection();
$div_wikipedia_Justine_Bateman->setTitleText('Justine Bateman');
$div_wikipedia_Justine_Bateman->setTitleLink('https://en.wikipedia.org/wiki/Justine_Bateman');
$div_wikipedia_Justine_Bateman->content = <<<HTML
	<p>Justine Tanya Bateman is an American actress, writer, director, and producer.</p>

	<p>Bateman earned a degree in computer science and digital media management from the University of California, Los Angeles (UCLA) in 2016.
	During the film industry strikes in 2023, Bateman was a vocal critic of the use of AI for human characters in productions,
	and has proposed a label designating that AI was not used for the actors.</p>
	HTML;


$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);

$page->body($div_Credo23_website);
$page->body('artificial_intelligence.html');


$page->body($div_wikipedia_Justine_Bateman);
