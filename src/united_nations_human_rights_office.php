<?php
$page = new Page();
$page->h1('United Nations Human Rights Office (OHCHR)');
$page->keywords('United Nations Human Rights Office', 'OHCHR');
$page->tags("Organisation", "Humanity", "Rights", "International");
$page->stars(0);

//$page->snp('description', '');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );

$r1 = $page->ref('https://www.ohchr.org/en/about-us', 'OHCHR: About us');


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;


$div_OHCHR_website = new WebsiteContentSection();
$div_OHCHR_website->setTitleText('OHCHR\'s website');
$div_OHCHR_website->setTitleLink('https://www.ohchr.org/en/ohchr_homepage');
$div_OHCHR_website->content = <<<HTML
	<p>"<em>The Office of the High Commissioner for Human Rights (UN Human Rights) is the leading UN entity on human rights.
	We represent the world's commitment to the promotion and protection of the full range of human rights and freedoms
	set out in the Universal Declaration of Human Rights.</em>" $r1</p>
	HTML;



$div_wikipedia_Office_of_the_United_Nations_High_Commissioner_for_Human_Rights = new WikipediaContentSection();
$div_wikipedia_Office_of_the_United_Nations_High_Commissioner_for_Human_Rights->setTitleText('Office of the United Nations High Commissioner for Human Rights');
$div_wikipedia_Office_of_the_United_Nations_High_Commissioner_for_Human_Rights->setTitleLink('https://en.wikipedia.org/wiki/Office_of_the_United_Nations_High_Commissioner_for_Human_Rights');
$div_wikipedia_Office_of_the_United_Nations_High_Commissioner_for_Human_Rights->content = <<<HTML
	<p>The Office of the United Nations High Commissioner for Human Rights,
	commonly known as the Office of the High Commissioner for Human Rights (OHCHR)
	or the United Nations Human Rights Office,
	is a department of the Secretariat of the ${'United Nations'} that works to promote and protect ${'human rights'}
	that are guaranteed under international law and stipulated in the Universal Declaration of Human Rights of 1948.
	The office was established by the United Nations General Assembly on 20 December 1993
	in the wake of the 1993 World Conference on Human Rights.</p>
	HTML;


$page->parent('united_nations.html');
$page->template("stub");
$page->body($div_introduction);
$page->body($div_OHCHR_website);

$page->body('human_rights.html');
$page->body('economic_social_and_cultural_rights.html');


$page->body($div_wikipedia_Office_of_the_United_Nations_High_Commissioner_for_Human_Rights);
