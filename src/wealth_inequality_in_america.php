<?php
$page = new VideoPage();
$page->h1("Wealth Inequality in America");
$page->tags("Video: News Story", "Living: Fair Income", "Wealth", "Economic Injustice");
$page->keywords("Wealth Inequality in America");
$page->stars(0);
$page->viewport_background("/free/wealth_inequality_in_america.png");

$page->snp("description", "Infographics on the distribution of wealth in America.");
$page->snp("image",       "/free/wealth_inequality_in_america.1200-630.png");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	HTML;



$div_youtube_Wealth_Inequality_in_America = new YoutubeContentSection();
$div_youtube_Wealth_Inequality_in_America->setTitleText("Wealth Inequality in America");
$div_youtube_Wealth_Inequality_in_America->setTitleLink("https://www.youtube.com/watch?v=QPKKQnijnsM");
$div_youtube_Wealth_Inequality_in_America->content = <<<HTML
	<p>Infographics on the distribution of wealth in America,
	highlighting both the inequality and the difference between our perception of inequality and the actual numbers.
	The reality is often not what we think it is.</p>
	HTML;


$page->parent('list_of_videos.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Wealth Inequality in America");
$page->body($div_youtube_Wealth_Inequality_in_America);
