<?php
$page = new Page();
$page->h1('Duverger symptom 2: destructive alternance');
$page->stars(1);
$page->tags("Elections: Duverger Syndrome");
$page->keywords('alternance');

//$page->snp('description', "");
//$page->snp('image', "/copyrighted/");

$page->preview( <<<HTML
	<p>What is alternance and what's wrong with it?</p>
	HTML );


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Political alternance is often seen as a hallmark of $democracy,
	but what is it exactly?
	What induces alternance and what is wrong with it?</p>

	<p>A form of destructive political alternance is actually one pernicious symptom of ${'Duverger Syndrome'}.
	It is deceptive because it is perceived a natural result of democracy,
	whereas it actually is one of its gravest disease.</p>
	HTML;


$div_Political_alternance = new ContentSection();
$div_Political_alternance->content = <<<HTML
	<h3>Political alternance</h3>

	<p>Democracy rightly operates on a multi-party system,
	as opposed to dictatorial one-party system,
	like the Chinese Communist Party in the ${"People's Republic of China"}.</p>

	<p>Obviously, the ruling party in a healthy democracy can never be guaranteed to constantly remain in power,
	since citizens have the power to vote another faction to run the country.</p>

	<p>Thus, it stands to reason that various candidates from various political parties
	rise to power, one after the other.</p>
	HTML;

$div_Do_undo_and_redo = new ContentSection();
$div_Do_undo_and_redo->content = <<<HTML
	<h3>Do, undo and redo...</h3>

	<p>The system of political alternance is not efficient as successive governments keep undoing
	what previous governments have previously done,
	only for their own work to be undone by the following government.</p>

	<p>A notable example is in the US, where a highly divided legislative branch
	and crippling filibuster rules in the US senate make it very hard to pass meaningful legislation.
	Presidents' only avenue to implement their policies is by the use of Executive Orders.
	Thus, when President Obama took office in 2009, he signed dozens of Executive Orders
	to reverse the policies previously enacted by President Bush.
	When President Trump took office, he in turn reverted Obama's policies by Executive Orders.
	Then, when Biden became president, he undid Trump's Executive Oders by signing dozens of his own,
	reestablishing the Obama's policies.</p>

	<p>This create a highly inefficient back and forth.
	Instead of each administration building on each other's accomplishments,
	they regularly destroy what their predecessors did, to try a different course.</p>

	<p>In this context, there are two critical things to consider:
	First, this constant change of direction does not necessarily represent the Will of the People.</p>

	<p>Secondly, here is an aspect where long standing single party authoritarian regimes
	have a huge advantage over democracies.
	A good example is $China, where the Chinese Communist Party has absolute power.
	Since there are no elections at all, the Party has remained in power
	ever since the founding of the People's Republic of China.
	They can then afford to have a truly long term view of their policies
	and patiently wait, over decades, to achieve their goals.</p>
	HTML;


$div_A_weakness_used_by_authoritarian_regimes_against_us = new ContentSection();
$div_A_weakness_used_by_authoritarian_regimes_against_us->content = <<<HTML
	<h3>A weakness used by authoritarian regimes against us</h3>

	<p>Authoritarian regimes attack us at our weakest points.</p>

	<p>They can more or less covertly fund opposition parties with extreme policies
	in the hopes that such parties will eventually win national elections.
	It is well known that Marine Le Pen in $France had her party funded by a $Russian bank.
	Similarly, Russia has been interfering in US elections, supporting Donald $Trump.</p>
	HTML;


$div_The_case_of_China_s_suppression_of_Taiwan = new ContentSection();
$div_The_case_of_China_s_suppression_of_Taiwan->content = <<<HTML
	<h3>The case of China's suppression of Taiwan</h3>

	<p>Since its founding, the ${"People's Republic of China"} has enforced a "One China Policy"
	upon its diplomatic partners,
	refusing to establish mutual diplomatic recognition with other $countries
	unless they broke ties with the ${"Republic of China"}, $Taiwan.</p>

	<p>In the early 1950s, every country in the world understood China to be the Republic in China,
	presently in exile in Taiwan.
	By the 1990s, most countries had switched recognition from the $ROC to the $PRC,
	with Taiwan still enjoying diplomatic relationships with over 30 countries</p>

	<p>The PRC patiently pursued its long term goals of isolating Taiwan: remember that there are no political alternance
	so their policies are long standing.
	So they established ties with opposition parties in every democratic countries still recognizing the ROC over the PRC.
	Everything was set: the only thing needed was to wait for the election where the governing party would lose power.
	The party supported by the Chinese Communist Party would gain executive powers and switch alliance from the ROC to the PRC.</p>

	<p>The CCP has mastered this game, and by 2024, only 12 countries are left that still have full diplomatic relations with Taiwan.</p>
	HTML;


$div_We_need_a_strong_consentual_stable_center = new ContentSection();
$div_We_need_a_strong_consentual_stable_center->content = <<<HTML
	<h3>A better solution?</h3>

	<p>We need a strong, consensual, stable centre that is truly representative of the Will of the People.</p>

	<p>If we used a better ${'voting method'}, we could still enjoy a multi-party system,
	with various candidates succeeding each other in elected office,
	but where, once in office, they would build upon what their predecessors already accomplished.</p>
	HTML;


$page->parent('duverger_syndrome.html');
$page->parent('duverger_syndrome_duality.html');
$page->template("stub");
$page->body($div_introduction);

$page->body($div_Political_alternance);
$page->body($div_Do_undo_and_redo);
$page->body($div_A_weakness_used_by_authoritarian_regimes_against_us);
$page->body($div_The_case_of_China_s_suppression_of_Taiwan);
$page->body($div_We_need_a_strong_consentual_stable_center);

$page->body('duverger_syndrome_party_politics.html');
