<?php
$page = new Page();
$page->h1("Eddie Glaude");
$page->alpha_sort("Glaude, Eddie");
$page->tags("Person");
$page->keywords("Eddie Glaude");
$page->stars(0);

//$page->snp("description", "");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Eddie Glaude is a professor of African American studies.
	He is the author of the book: "We Are the Leaders We Have Been Looking For".</p>
	HTML;

$div_wikipedia_Eddie_Glaude = new WikipediaContentSection();
$div_wikipedia_Eddie_Glaude->setTitleText("Eddie Glaude");
$div_wikipedia_Eddie_Glaude->setTitleLink("https://en.wikipedia.org/wiki/Eddie_Glaude");
$div_wikipedia_Eddie_Glaude->content = <<<HTML
	<p>Eddie S. Glaude Jr. is an American academic, author, and pundit.
	He is the James S. McDonnell Distinguished University Professor of African American Studies at Princeton University.</p>
	HTML;


$page->parent('list_of_people.html');
$page->template("stub");
$page->body($div_introduction);


$page->body('we_are_the_leaders_we_have_been_looking_for.html');
$page->body($div_wikipedia_Eddie_Glaude);
