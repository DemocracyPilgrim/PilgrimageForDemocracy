<?php
$page = new Page();
$page->h1('Immigration');
$page->keywords('Immigration', 'immigration', 'migrants', 'migration', 'immigrants');
$page->tags("International", "Humanity");
$page->stars(2);

$page->snp('description', 'The root causes and the human cost of the "immigration crisis".');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p>Massive, illegal immigration comes with a huge cost in terms of human suffering.
	What are its causes and human cost?
	What preventions and barriers are put in place?
	What holistic solutions are there?</p>
	HTML );

$r1 = $page->ref('https://news.gallup.com/poll/508520/americans-value-immigration-concerns.aspx', 'Gallup: Americans Still Value Immigration, but Have Concerns');
$r2 = $page->ref('https://www.pewresearch.org/short-reads/2023/11/16/what-we-know-about-unauthorized-immigrants-living-in-the-us/', 'Pew Research: What we know about unauthorized immigrants living in the U.S.');
$r3 = $page->ref('https://www.migrationdataportal.org/resource/key-global-migration-figures', 'Migration and Human Mobility: Key Global Figures');


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Massive, illegal immigration comes with a huge cost in terms of human suffering.
	The scale of immigration is driven by lack of $democracy and basic human $rights in the $countries of origin,
	and by the lack of ${'social justice'} on a $global scale,
	with the destination countries, like the $USA or $Europe being vastly wealthier
	than the countries migrants and $refugees originate from.
	All of these factors create a massive imbalance on a global scale, which drives migratory forces.</p>

	<p>The ideal situation can easily be stated but reaching that situation is another matter.
	Given the ability to live safely, in good conditions, free of repression and extreme economic hardship,
	people would naturally prefer to stay close to home and family.</p>

	<p>However, creating the conditions for such an idea situation is a tall order.
	How do we create global democracy?
	How do we create a just international trading framework wherein each country gets its fair share
	and is able to provide food, $housing and security to its people?</p>

	<p>Creating an ever stronger barrier between rich countries and poor countries,
	between the haves and the have-nots, is the politically convenient solution,
	and in the short term there does not seem to be any way around it.
	It is however the source of much human suffering and it cannot be seen as an appropriate, humane, long term solution.</p>
	HTML;


$div_Key_figures = new ContentSection();
$div_Key_figures->content = <<<HTML
	<h3>Key figures</h3>

	<p>Here are some key figures, quoted from the November 2023 report by the ${'Global Migration Data Analysis Centre'}: $r3</p>

	<blockquote>
	<ul>
	<li>Globally, 281 MILLION PEOPLE were estimated to be international migrants
	- people who were born in another country and/or held foreign citizenship - at mid-year 2020.</li>

	<li>An estimated 13 PER CENT of the total number of international migrants in 2020 were children below 18 years of age.</li>

	<li>Between 2014 and 2022, at least 54,403 people lost their lives while migrating internationally.
	In 2023, another 5,034 migrant deaths and disappearances were recorded globally as of 6 November 2023.</li>

	<li>156,330 INDIVIDUAL CASES of trafficking have been identified by or reported to
	Counter Trafficking Data Collaborative (CTDC) partners between 2002 and 2021.</li>

	<li>By mid-year 2023, there were an estimated total of 36.4 MILLION REFUGEES and 6.1 MILLION ASYLUM-SEEKERS worldwide.</li>

	<li>At the end of 2022, an estimated total of 71.1 MILLION people remained displaced
	within the borders of their own country – 62.5 million as a result of conflict and violence and 8.7 million as a result of disasters.</li>
	</ul>
	</blockquote>
	HTML;


$div_MIGRATION_AND_HUMAN_MOBILITY_KEY_GLOBAL_FIGURES = new WebsiteContentSection();
$div_MIGRATION_AND_HUMAN_MOBILITY_KEY_GLOBAL_FIGURES->setTitleText("Migration and Human Mobility: Key Global Figures");
$div_MIGRATION_AND_HUMAN_MOBILITY_KEY_GLOBAL_FIGURES->setTitleLink("https://www.migrationdataportal.org/resource/key-global-migration-figures");
$div_MIGRATION_AND_HUMAN_MOBILITY_KEY_GLOBAL_FIGURES->content = <<<HTML
	<p>This one-page PDF document from $IOM $GMDAC summarizes key global migration figures, regularly updated.
	The data ncludes key figures on: international migrants, female migrants, child migrants, youth & migration,
	labour migrants, remittances, missing migrants, trafficking, refugees and asylum-seekers, resettlement and internal displacement.</p>
	HTML;



$div_debate_on_immigration = new ContentSection();
$div_debate_on_immigration->content = <<<HTML
	<h3>Debate on immigration</h3>

	<p>Before we even begin to debate about immigration,
	we ought to make sure that our debate is based on factual premises.
	Immigration is a hot topic issue in many rich $democratic $countries,
	and as we know from $Duverger_Extremism,
	candidates and political parties are prone to push forward narratives based on false premises.</p>

	<p>For example, in the ${'United States'}, many candidates within the Republican party
	keep pushing forward the idea that it is critical to build a wall at the US southern border,
	and keep talking about illegal immigration as if it were a growing problem getting out of hand.
	Some even push forward theories about an "invasion" or a "big replacement".
	Yet, a Gallop poll found that “68% of Americans say immigration is good for the country today.”
	When asked about levels of immigration, 57% either want it to remain at or above its present levels,
	while 41% favor a decrease in our immigration levels." $r1</p>

	<p>What's more, still in the USA, a study by the Pew Research Center shows that
	far from having a growing invasion of illegal immigrants,
	the country’s unauthorized immigrant population peaked at 12.2 million in 2007,
	and has been steadily decreasing since then.
	The Pew report found that the unauthorized immigrant population in the United States stood at 10.5 million in 2021,
	a 14% decrease from what it was in 2007.$r2</p>

	<p>Thus, you can help in developing this section by providing accurate data and reliable information,
	and make sure that any debate on immigration and other hotly contested topics are based on accurate premises.</p>
	HTML;

$div_Migrants_are_humans_not_political_tools = new ContentSection();
$div_Migrants_are_humans_not_political_tools->content = <<<HTML
	<h3>Migrants are humans, not political tools</h3>

	<p>We shall always keep in mind that each migrant is a human being.
	And yet, we can see how these people are used by people in positions of power for political purposes.
	In $Russia and Belarus, migrants are used against their western neighbours.
	Finland has accused Moscow of weaponising desperate refugees and migrants in retaliation for its recent entry into NATO.
	In the ${'United States'}, migrants are flown from one state and dumped into another state to score political points.
	And populist party leaders in every rich countries are using migrants as scarecrows to gain votes.</p>

	<p>Let's consider migrants as human beings,
	and as such, they are no better nor worse than any other being,
	but they deserve a humane treatment, our understanding and our compassion.</p>
	HTML;


$div_why = new ContentSection();
$div_why->content = <<<HTML
	<h3>The root causes of the "immigration crisis"</h3>

	<p>The scale of immigration is driven by lack of $democracy and basic human $rights in the $countries of origin.
	In addition, the difficulties for families to cover their basic living expenses force many to try their luck in rich countries.</p>
	HTML;

$div_human_cost = new ContentSection();
$div_human_cost->content = <<<HTML
	<h3>The human cost of illegal immigration</h3>

	<p>We have to keep in mind that what people in destination countries perceive as an "immigration crisis" is only the tip of the iceberg.
	For every migrant that reaches their destination countries, or at least their shores and borders,
	many more would be migrants face worse fates along the way, meeting with slavery, human trafficking, and for many more: death.</p>

	<p>The human cost of immigration are huge.
	This should be kept in mind whenever immigration is being debated.</p>

	<p><strong>Shipwrecks</strong> are particularly likely to occur in the Mediterranean Sea,
	as migrants attempt to cross from Africa to Europe.
	In 2013, a boat carrying migrants from Libya to Italy sank off the Italian island of Lampedusa:
	in all, over 360 people perished in this single incident.</p>
	HTML;

$div_barriers = new ContentSection();
$div_barriers->content = <<<HTML
	<h3>Barriers put in place by destination countries</h3>

	<p>Rich destination countries try to deal with immigration by erecting more and more obstacles, hurdles and barriers
	in order to prevent migrants from ever reaching their soil.</p>

	<p>However, for the most part, those barriers do nothing to alleviate the human cost of illegal immigration.
	They do even less to address the root cause of the immigration drives.</p>
	HTML;

$div_Immigration_to_Europe = new ContentSection();
$div_Immigration_to_Europe->content = <<<HTML
	<h3>Immigration to Europe</h3>

	<p>The ${'European Union'} is trying to prevent migrants from ever reaching its shores.</p>

	<p>In 2023, the $EU offered $Tunisia 100 million euros for border management, search and rescue,
	and returns “rooted in respect for human rights” to address migration,
	as part of a larger 1 billion euros loan package.</p>
	HTML;

$list_Immigration = ListOfPeoplePages::WithTags("Immigration");
$print_list_Immigration = $list_Immigration->print();

$div_list_Immigration = new ContentSection();
$div_list_Immigration->content = <<<HTML
	<h3>Related content</h3>

	$print_list_Immigration
	HTML;



$div_wikipedia_Immigration = new WikipediaContentSection();
$div_wikipedia_Immigration->setTitleText('Immigration');
$div_wikipedia_Immigration->setTitleLink('https://en.wikipedia.org/wiki/Immigration');
$div_wikipedia_Immigration->content = <<<HTML
	<p>Immigration is the international movement of people
	to a destination country of which they are not natives or where they do not possess citizenship
	in order to settle as permanent residents or naturalized citizens.
	Commuters, tourists, and other short-term stays in a destination country
	do not fall under the definition of immigration or migration;
	seasonal labour immigration is sometimes included, however.</p>
	HTML;

$div_wikipedia_International_migration = new WikipediaContentSection();
$div_wikipedia_International_migration->setTitleText('International migration');
$div_wikipedia_International_migration->setTitleLink('https://en.wikipedia.org/wiki/International_migration');
$div_wikipedia_International_migration->content = <<<HTML
	<p>International migration occurs when people cross state boundaries
	and stay in the host state for some minimum length of the time.
	Migration occurs for many reasons.
	Many people leave their home countries in order to look for economic opportunities in another country.
	Others migrate to be with family members who have migrated or because of political conditions in their countries.
	Education is another reason for international migration, as students pursue their studies abroad,
	although this migration is often temporary, with a return to the home country after the studies are completed.</p>
	HTML;

$div_wikipedia_Human_migration = new WikipediaContentSection();
$div_wikipedia_Human_migration->setTitleText('Human migration');
$div_wikipedia_Human_migration->setTitleLink('https://en.wikipedia.org/wiki/Human_migration');
$div_wikipedia_Human_migration->content = <<<HTML
	<p>Human migration is the movement of people from one place to another
	with intentions of settling, permanently or temporarily, at a new location (geographic region).</p>
	HTML;


$div_wikipedia_2013_Lampedusa_migrant_shipwreck = new WikipediaContentSection();
$div_wikipedia_2013_Lampedusa_migrant_shipwreck->setTitleText('2013 Lampedusa migrant shipwreck');
$div_wikipedia_2013_Lampedusa_migrant_shipwreck->setTitleLink('https://en.wikipedia.org/wiki/2013_Lampedusa_migrant_shipwreck');
$div_wikipedia_2013_Lampedusa_migrant_shipwreck->content = <<<HTML
	<p>On 3 October 2013, a boat carrying migrants from Libya to Italy sank off the Italian island of Lampedusa.
	It was reported that the boat had sailed from Misrata, Libya, but that many of the migrants were originally from Eritrea, Somalia and Ghana.
	An emergency response involving the Italian Coast Guard resulted in the rescue of 155 survivors.
	On 12 October it was reported that the confirmed death toll after searching the boat was 359,
	but that further bodies were still missing;[5] a figure of "more than 360" deaths was later reported.</p>
	HTML;



$page->parent('international.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_Key_figures);
$page->body($div_MIGRATION_AND_HUMAN_MOBILITY_KEY_GLOBAL_FIGURES);

$page->body($div_debate_on_immigration);
$page->body($div_Migrants_are_humans_not_political_tools);

$page->body($div_why);

$page->body($div_human_cost);
$page->body('deaths_of_migrants.html');

$page->body($div_barriers);
$page->body('migration_law.html');


$page->body($div_Immigration_to_Europe);

$page->body($div_list_Immigration);



$page->body($div_wikipedia_Immigration);
$page->body($div_wikipedia_International_migration);
$page->body($div_wikipedia_Human_migration);
$page->body($div_wikipedia_2013_Lampedusa_migrant_shipwreck);
