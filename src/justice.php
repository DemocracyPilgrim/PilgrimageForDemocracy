<?php
$page = new Page();
$page->h1('Justice');
$page->stars(2);
$page->keywords('Justice', 'justice', 'judiciary', 'judicial', 'Judicial', 'Judiciary');
$page->viewport_background('/free/justice.png');

$page->snp('description', 'All the facets of justice in democratic and fair societies.');
$page->snp('image', "/free/justice.1200-630.png");

$page->preview( <<<HTML
	<p>A strong judiciary is important in any democracy,
	as it can balance the powers of the executive and of the legislative.</p>
	HTML );


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Justice, in a free, fair, and democratic society, is a multifaceted concept that encompasses several key aspects,
	extending beyond the traditional legal framework.</p>
	HTML;




$h2_Judiciary = new h2HeaderContent("The Judiciary");


$div_Judiciary = new ContentSection();
$div_Judiciary->content = <<<HTML
	<p>The judiciary is one of the ${'branches of government'}.</p>

	<p>Justice is not simply a set of legal principles but a fundamental value that underpins the entire fabric of society.
	It is a commitment to upholding the rights of all individuals,
	ensuring that the legal system is a force for good, and promoting a just and equitable society for all.</p>

	<p>The judiciary branch includes the court system, with judges, $lawyers, and different court levels up to the ${'supreme court'}.</p>
	HTML;


$div_Equality_before_the_law = new ContentSection();
$div_Equality_before_the_law->content = <<<HTML
	<h3>Equality before the law</h3>

	<p>Every $individual, regardless of their background, status, or beliefs, should be treated equally under the $law.
	This principle ensures that all citizens are subject to the same rules and have equal access to justice.</p>
	HTML;

$div_Due_process_of_law = new ContentSection();
$div_Due_process_of_law->content = <<<HTML
	<h3>Due process of law</h3>

	<p>Every $individual has the $right to a fair trial, where they are treated with respect, have access to legal representation,
	and are given the opportunity to present their case.
	This principle protects against arbitrary arrest and imprisonment.</p>
	HTML;


$div_Rule_of_law = new ContentSection();
$div_Rule_of_law->content = <<<HTML
	<h3>Rule of law</h3>

	<p>The law should be applied consistently and fairly, without bias or favouritism.
	This principle ensures that everyone is accountable to the same legal standards and that no one is above the law.</p>
	HTML;



$div_Access_to_justice = new ContentSection();
$div_Access_to_justice->content = <<<HTML
	<h3>Access to justice</h3>

	<p>Every $individual should have equal access to legal services and remedies, regardless of their financial means.
	This principle ensures that everyone has the opportunity to seek redress for grievances and protect their $rights.
	The $US court ruling ${'Gideon v. Wainwright'} ensures that all defendants have access to legal counsel.</p>
	HTML;


$div_Fair_and_transparent_processes = new ContentSection();
$div_Fair_and_transparent_processes->content = <<<HTML
	<h3>Fair and transparent processes</h3>

	<p>Legal procedures should be conducted in a transparent and impartial manner, with due process and access to information for all parties involved.
	This principle fosters public trust in the justice system and ensures that decisions are made based on facts and evidence.</p>
	HTML;



$div_Accountability_and_redress = new ContentSection();
$div_Accountability_and_redress->content = <<<HTML
	<h3>Accountability and redress</h3>

	<p>Those who violate the law or abuse power should be held $accountable for their actions,
	and victims should have access to justice and remedies for the harms they have suffered.
	This principle ensures that the justice system serves as a mechanism for restoring fairness and protecting the $rights of $individuals.</p>
	HTML;


$div_Judicial_independence = new ContentSection();
$div_Judicial_independence->content = <<<HTML
	<h3>Judicial independence</h3>

	<p>A critical pillar of justice is a judiciary that is independent from political interference, ensuring fair and impartial rulings.</p>

	<p>While judicial appointments and terms are designed to ensure stability and independence, the system remains vulnerable to $corruption,
	as the influence of powerful interests and political pressure can undermine the integrity of individual judges and the judiciary as a whole.</p>
	HTML;



$h2_Applicable_Domains = new h2HeaderContent("Applicable Domains");



$div_Environmental_Justice = new ContentSection();
$div_Environmental_Justice->content = <<<HTML
	<h3>Environmental Justice</h3>

	<p>This concept recognizes the inherent right of all people to live in a healthy and sustainable environment,
	free from disproportionate environmental burdens, especially on marginalized communities.</p>
	HTML;


$div_Economic_Justice = new ContentSection();
$div_Economic_Justice->content = <<<HTML
	<h3>Economic Justice</h3>

	<p>Economic justice calls for a fair and equitable distribution of resources and opportunities,
	ensuring that everyone has a chance to thrive economically, regardless of their background or circumstances.</p>
	HTML;


$div_Social_Justice = new ContentSection();
$div_Social_Justice->content = <<<HTML
	<h3>Social Justice</h3>

	<p>This broader concept encompasses the fight against all forms of discrimination, inequality, and oppression,
	striving for a society where everyone is treated with respect and dignity.</p>
	HTML;


$div_Restorative_Justice = new ContentSection();
$div_Restorative_Justice->content = <<<HTML
	<h3>Restorative Justice</h3>

	<p>This approach focuses on repairing harm caused by crime and fostering reconciliation between victims, offenders, and communities.
	It emphasizes healing, rehabilitation, and accountability over traditional punitive measures.</p>
	HTML;


$div_Intersectional_Justice = new ContentSection();
$div_Intersectional_Justice->content = <<<HTML
	<h3>Intersectional Justice</h3>

	<p>This framework recognizes that various forms of oppression and injustice are interconnected,
	and it emphasizes the need for solutions that address all forms of discrimination simultaneously.</p>
	HTML;


$h2_Justice_in_Action = new h2HeaderContent("Justice in Action");


$div_Justice_in_Action = new ContentSection();
$div_Justice_in_Action->content = <<<HTML
	<p>Justice is not merely a theoretical concept but a call to action.
	It demands a commitment to creating a society that upholds the rights of all individuals,
	ensuring equal access to opportunities and resources, and promoting a just and equitable world for all.</p>
	HTML;


$div_wikipedia_Justice = new WikipediaContentSection();
$div_wikipedia_Justice->setTitleText('Justice');
$div_wikipedia_Justice->setTitleLink('https://en.wikipedia.org/wiki/Justice');
$div_wikipedia_Justice->content = <<<HTML
	<p>To achieve justice, individuals should receive that which they deserve,
	with the interpretation of what "deserve" means, in turn, drawing on numerous viewpoints and perspectives,
	including fields like ethics, rationality, law, religion, equity and fairness.</p>
	HTML;





$page->parent('institutions.html');
$page->body($div_introduction);


$page->body($h2_Judiciary);
$page->body($div_Judiciary);
$page->body($div_Equality_before_the_law);
$page->body($div_Due_process_of_law);
$page->body($div_Rule_of_law);
$page->body($div_Access_to_justice);
$page->body($div_Fair_and_transparent_processes);
$page->body($div_Accountability_and_redress);
$page->body($div_Judicial_independence);


$page->body($h2_Applicable_Domains);
$page->body($div_Environmental_Justice);
$page->body($div_Economic_Justice);
$page->body($div_Social_Justice);
$page->body($div_Restorative_Justice);
$page->body($div_Intersectional_Justice);


$page->body($h2_Justice_in_Action);
$page->body($div_Justice_in_Action);

$page->body($div_wikipedia_Justice);
