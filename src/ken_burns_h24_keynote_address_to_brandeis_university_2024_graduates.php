<?php
$page = new VideoPage();
$page->h1("Ken Burns: H24 Keynote Address to Brandeis University's 2024 Graduates");
$page->tags("Video: Speech", "Ken Burns", "Brandeis University", "Elections");
$page->keywords("Ken Burns: H24 Keynote Address to Brandeis University 2024 Graduates");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;


$div_youtube_Ken_Burns_H_24_Keynote_Address_to_Brandeis_University_s_2024_Graduates = new YoutubeContentSection();
$div_youtube_Ken_Burns_H_24_Keynote_Address_to_Brandeis_University_s_2024_Graduates->setTitleText("Ken Burns, H’24 Keynote Address to Brandeis University&#39;s 2024 Graduates");
$div_youtube_Ken_Burns_H_24_Keynote_Address_to_Brandeis_University_s_2024_Graduates->setTitleLink("https://www.youtube.com/watch?v=9n1OqPzIKH4");
$div_youtube_Ken_Burns_H_24_Keynote_Address_to_Brandeis_University_s_2024_Graduates->content = <<<HTML
	<p>Ken Burns, H’24 delivered the keynote address to the 2024 undergraduate class during the 73rd Commencement Exercises.</p>
	HTML;



$page->parent('list_of_videos.html');
$page->template("stub");
$page->body($div_introduction);
$page->body($div_youtube_Ken_Burns_H_24_Keynote_Address_to_Brandeis_University_s_2024_Graduates);
