<?php
$page = new ReportPage();
$page->h1("In Deep Trouble: Surfacing Tech-Powered Sexual Harassment in K-12 Schools");
$page->tags("Report", "Artificial Intelligence", "Education", "Sexuality", "Center for Democracy and Technology");
$page->keywords("In Deep Trouble: Surfacing Tech-Powered Sexual Harassment in K-12 Schools");
$page->stars(1);
$page->viewport_background("/free/in_deep_trouble_surfacing_tech_powered_sexual_harassment_in_k_12_schools.png");

$page->snp("description", "When schools have to tackle explicit deepfakes...");
$page->snp("image",       "/free/in_deep_trouble_surfacing_tech_powered_sexual_harassment_in_k_12_schools.1200-630.png");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;




$div_Report_In_Deep_Trouble_Surfacing_Tech_Powered_Sexual_Harassment_in_K_12 = new WebsiteContentSection();
$div_Report_In_Deep_Trouble_Surfacing_Tech_Powered_Sexual_Harassment_in_K_12->setTitleText("Report – In Deep Trouble: Surfacing Tech-Powered Sexual Harassment in K-12");
$div_Report_In_Deep_Trouble_Surfacing_Tech_Powered_Sexual_Harassment_in_K_12->setTitleLink("https://cdt.org/insights/report-in-deep-trouble-surfacing-tech-powered-sexual-harassment-in-k-12-schools/");
$div_Report_In_Deep_Trouble_Surfacing_Tech_Powered_Sexual_Harassment_in_K_12->content = <<<HTML
	<p>Generative artificial intelligence (AI) tools continue to capture the imagination, but increasingly the technology’s damaging potential is revealing itself. An often problematic use of generative AI is in the creation and distribution of deepfakes online, especially because the vast majority contain sexually explicit intimate depictions. In the past school year (2023-2024), the rise of generative AI has collided with a long-standing problem in schools: the act of sharing non-consensual intimate imagery (NCII). K-12 schools are often the first to encounter large-scale manifestations of the risks and harms facing young people when it comes to technology, and NCII, both deepfake and authentic, is no exception. Over the past year, anecdotes of children being the perpetrators and victims of deepfake NCII have been covered by major news outlets, elevating concerns about how to curb the issue in schools. But just how widespread is NCII really? And how well equipped are schools to handle this challenge?</p>
	HTML;



$page->parent('list_of_research_and_reports.html');
$page->template("stub");
$page->body($div_introduction);



$page->body($div_Report_In_Deep_Trouble_Surfacing_Tech_Powered_Sexual_Harassment_in_K_12);
$page->body('center_for_democracy_and_technology.html');
