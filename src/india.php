<?php
$page = new CountryPage('India');
$page->h1('India');
$page->tags("Country");
$page->keywords("India", "Indian");
$page->stars(0);
$page->viewport_background('/free/india.png');

$page->snp('description', '1,4 billion inhabitants.');
$page->snp('image',       '/free/india.1200-630.png');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>India has protested an official "standard map" published by $China in 2023
	which show Indian state of Arunachal Pradesh and Aksai Chin plateau as being within China's territory.
	See ${'Chinese expansionism'}.</p>
	HTML;

$div_wikipedia_India = new WikipediaContentSection();
$div_wikipedia_India->setTitleText('India');
$div_wikipedia_India->setTitleLink('https://en.wikipedia.org/wiki/India');
$div_wikipedia_India->content = <<<HTML
	<p>India has been a federal republic since 1950, governed through a democratic parliamentary system.
	It is a pluralistic, multilingual and multi-ethnic society.
	India's population grew from 361 million in 1951 to almost 1.4 billion in 2022.</p>
	HTML;


$page->parent('world.html');
$page->template("stub");
$page->body($div_introduction);
$page->related_tag("India");
$page->body('Country indices');

$page->body($div_wikipedia_India);
