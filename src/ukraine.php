<?php
$page = new CountryPage('Ukraine');
$page->h1('Ukraine');
$page->tags("Country");
$page->stars(2);
$page->keywords('Ukraine', "Ukrainian");
$page->viewport_background('/free/ukraine.png');

$page->preview( <<<HTML
	<p>The front line of the war for democracy.</p>
	HTML );

$page->snp('description', '36 million inhabitants.');
$page->snp('image', "/free/ukraine.1200-630.png");

$r1 = $page->ref('https://kyivindependent.com/judicial-reform-has-mixed-results-as-good-candidates-are-vetoed-without-clear-reasons/', 'Ukraine\'s judicial reform has mixed reviews as it nears key point');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Ukraine is literally the front line of the war for democracy.
	All democratic countries should continue supporting Ukraine in every possible way
	until the Russian invader is completely defeated.</p>

	<p>${'Volodymyr Zelenskyy'} is the president of Ukraine and a hero of Ukrainian freedom and $democracy.</p>
	HTML;


$h2_Democracy_vs_Totalitarianism = new h2HeaderContent('Democracy vs Totalitarianism');


$div_democracy_vs_totalitarianism = new ContentSection();
$div_democracy_vs_totalitarianism->content = <<<HTML
	<p>With the Russian full scale invasion of Ukraine,
	together with the rise in power of the People's Republic of China and its increasingly aggressive assertiveness,
	this decade is marked by a direct confrontation between democracies and totalitarian regimes.</p>

	<p>Ukraine must win the war and Putin's regime must be thoroughly defeated.</p>

	<p>In a televised interview on Aug. 27 2023, President ${'Volodymyr Zelensky'} said he has submitted a proposal to parliament
	to equate $corruption with treason while martial law is in effect in Ukraine.</p>
	HTML;



$h2_long_road_towards_independence_and_democracy = new h2HeaderContent("Ukraine's journey to independence and democracy");

$div_road_to_democracy = new ContentSection();
$div_road_to_democracy->content = <<<HTML
	<p>Ukraine's road towards independence and democracy has been very long and very painful,
	and Ukraine still has to defeat its most terrible enemy, the present day Russian Federation.</p>

	HTML;


$list_Ukraine_Road_to_Democracy = new UnsortedListOfPages();
$list_Ukraine_Road_to_Democracy->add('holodomor.html');
$list_Ukraine_Road_to_Democracy->add('orange_revolution.html');
$list_Ukraine_Road_to_Democracy->add('euromaidan.html');
$list_Ukraine_Road_to_Democracy->add('revolution_of_dignity.html');
$print_list_Ukraine_Road_to_Democracy = $list_Ukraine_Road_to_Democracy->print();

$div_list_Ukraine_Road_to_Democracy = new ContentSection();
$div_list_Ukraine_Road_to_Democracy->content = <<<HTML
	$print_list_Ukraine_Road_to_Democracy
	HTML;





$h2_Post_war_democracy = new h2HeaderContent('Post-war democracy');


$div_democratic_reforms = new ContentSection();
$div_democratic_reforms->content = <<<HTML
	<p>While we hope that Ukraine will quickly be able to defeat Russia and recover its 1991 borders,
	its democratization process will not end with a victory in the ongoing war.
	Already today, aside from the military confrontation, reformers are struggling to define
	the democratic nature of post-war Ukraine.</p>
	HTML;


$div_ukraine_NATO = new ContentSection();
$div_ukraine_NATO->content = <<<HTML
	<h3>Ukraine to join NATO</h3>

	<p>At the very beginning of Russia's full scale invasion,
	it was almost inconceivable that Ukraine would join NATO.
	At some stage, president Zelensky himself conceded that they had to give up such hope in order to secure peace.
	However, the subsequent events on the battlefield, and the scope of the crimes committed by Russia,
	dramatically changed the conditions.
	Today, not many people doubt that Ukraine will eventually join NATO,
	although it is clear that it shall happen only after the war.</p>

	<p>A few short years ago, NATO had almost lost its raison d'être.
	Today, NATO can be seen as the armed forces of democratic countries.
	Ukraine joining NATO will be beneficial for all.
	First, at long last, Ukraine's peace and military protection shall be assured.
	Also, NATO shall gain a new, powerful democratic ally.</p>
	HTML;

$div_Ukraine_European_Union = new ContentSection();
$div_Ukraine_European_Union->content = <<<HTML
	<h3>Ukraine to join the European Union</h3>

	<p>Ukraine shall join the European Union as certainly it will NATO.</p>

	<p>Ukraine is in the process of conducting reforms that are required to be admitted in the EU.</p>
	HTML;

$div_Judicial_reform = new ContentSection();
$div_Judicial_reform->content = <<<HTML
	<h3>Judicial reform</h3>

	<p>Right in the midst of a brutal war against Russia,
	Ukraine is battling on another front:
	its war against corruption.</p>

	<p>Judicial reforms are urgently needed; they have been ongoing for years, and the process has suffered many setbacks</p>

	<p>Among existing problems are: ${r1}</p>
	<ul>
	<li>The head of the Supreme Court, Vsevolod Kniaziev, being charged with bribery.</li>
	<li>Trusted and independent candidates have been excluded from competitions for key judicial posts, while tainted ones have been green-lighted.</li>
	<li>The independence of the judiciary from the executive branch is not yet fully established.</li>
	</ul>
	HTML;



$div_wikipedia_Russo_Ukrainian_War = new WikipediaContentSection();
$div_wikipedia_Russo_Ukrainian_War->setTitleText('Russo Ukrainian War');
$div_wikipedia_Russo_Ukrainian_War->setTitleLink('https://en.wikipedia.org/wiki/Russo-Ukrainian_War');
$div_wikipedia_Russo_Ukrainian_War->content = <<<HTML
	<p>This article is about the war ongoing since 2014.</p>
	HTML;


$div_wikipedia_Judiciary_of_Ukraine = new WikipediaContentSection();
$div_wikipedia_Judiciary_of_Ukraine->setTitleText('Judiciary of Ukraine');
$div_wikipedia_Judiciary_of_Ukraine->setTitleLink('https://en.wikipedia.org/wiki/Judiciary_of_Ukraine');
$div_wikipedia_Judiciary_of_Ukraine->content = <<<HTML
	<p>Although judicial independence exists in principle,
	in practice there is little separation of juridical and political powers.
	Judges are subjected to pressure by political and business interests.
	Ukraine's court system is widely regarded as corrupt.</p>
	HTML;

$div_codeberg = new CodebergContentSection();
$div_codeberg->setTitleText('Ukraine: road to democracy, and post-war democratic reforms');
$div_codeberg->setTitleLink('https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy/issues/32');
$div_codeberg->content = <<<HTML
	<p>Provide a short outline of Ukraine's road to independence and democracy,
	and document the democratic reforms that Ukraine will need to continue after its victory (hopefully soon) over Russia.</p>
	HTML;


$page->parent('world.html');
$page->template("stub");
$page->body($div_introduction);



$page->body($h2_Democracy_vs_Totalitarianism);
$page->body($div_democracy_vs_totalitarianism);
$page->body($div_wikipedia_Russo_Ukrainian_War);




$page->body($h2_long_road_towards_independence_and_democracy);
$page->body($div_road_to_democracy);
$page->body($div_list_Ukraine_Road_to_Democracy);


$page->body($div_codeberg);

$page->body($h2_Post_war_democracy);
$page->body($div_democratic_reforms);
$page->body($div_ukraine_NATO);
$page->body($div_Ukraine_European_Union);
$page->body($div_Judicial_reform);
$page->body($div_wikipedia_Judiciary_of_Ukraine);

$page->related_tag("Ukraine");

$page->body('Country indices');
