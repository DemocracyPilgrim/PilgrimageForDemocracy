<?php
$page = new OrganisationPage();
$page->h1("United Nations Volunteers");
$page->tags("Organisation", "United Nations", "Public Service", "Volunteerism");
$page->keywords("United Nations Volunteers", "UNV");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;



$div_United_Nations_Volunteers = new WebsiteContentSection();
$div_United_Nations_Volunteers->setTitleText("United Nations Volunteers ");
$div_United_Nations_Volunteers->setTitleLink("https://www.unv.org/");
$div_United_Nations_Volunteers->content = <<<HTML
	<p>
	</p>
	HTML;



$div_wikipedia_United_Nations_Volunteers = new WikipediaContentSection();
$div_wikipedia_United_Nations_Volunteers->setTitleText("United Nations Volunteers");
$div_wikipedia_United_Nations_Volunteers->setTitleLink("https://en.wikipedia.org/wiki/United_Nations_Volunteers");
$div_wikipedia_United_Nations_Volunteers->content = <<<HTML
	<p>The United Nations Volunteers (UNV) programme is a United Nations organization that contributes to peace and development through volunteerism worldwide.
	Volunteerism is a powerful means of engaging people in tackling development challenges, and it can transform the pace and nature of development. Volunteerism benefits both society at large and the individual volunteer by strengthening trust, solidarity and reciprocity among citizens, and by purposefully creating opportunities for participation.</p>
	HTML;


$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("United Nations Volunteers");
$page->body($div_United_Nations_Volunteers);
$page->body($div_wikipedia_United_Nations_Volunteers);
