<?php
$page = new Page();
$page->h1("Unsung Heroes of Democracy Award");
$page->tags("Award", "Democracy", "Institutions", "Electoral System");
$page->keywords("Unsung Heroes of Democracy Award", "Unsung Heroes of Democracy");
$page->stars(1);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

$r1 = $page->ref("https://www.americanbar.org/news/abanews/aba-news-archives/2024/07/unsung-heroes-of-democracy-recipients/", "ABA announces recipients of Unsung Heroes of Democracy Award");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>The "Unsung Heroes of Democracy" $Award is bestowed by the ${'American Bar Association'}'s ${'Task Force for American Democracy'}.</p>
	HTML;


$div_2024_Awards = new ContentSection();
$div_2024_Awards->content = <<<HTML
	<h3>2024 Awards</h3>

	<p>In July 2024, the ${'ABA Task Force for American Democracy'} announced the recipients of Unsung Heroes of Democracy Award,
	celebrating 22 individuals and organizations.
	Among the awardees are 8 organizations and 14 individuals, including five election officials,
	who "significantly advanced the cause of protecting American democracy across the nation". $r1</p>

	<p>Organizational winners:</p>

	<ul>
	<li>Arizona Native Vote Election Protection Project</li>
	<li>Braver Angels</li>
	<li>${'Center for Election Innovation & Research'}</li>
	<li>Committee of 70</li>
	<li>Equip for Equality</li>
	<li>Keep Our Republic</li>
	<li>New York City Bar Association Task Force on the Rule of Law</li>
	<li>North Dakota Native Vote</li>
	</ul>

	<p>Individual winners:</p>

	<ul>
	<li>Bob Bauer and Ben Ginsberg (Washington, D.C.);</li>
	<li>Katherine Culliton-González (Silver Spring, Maryland);</li>
	<li>Gent Haviari (Dartmouth, New Hampshire);</li>
	<li>William J. Kresse (Chicago, Illinois;</li>
	<li>Michael Stiegler (Washington, D.C.);</li>
	<li>Christine Torbett (Columbus, Ohio);</li>
	<li>Suzanne Spaulding (Washington, D.C.);</li>
	<li>Robert Weisenbach (Erie, Pennsylvania);</li>
	</ul>

	<p>Election Officials</p>

	<ul>
	<li>Cathy Darling Allen (Shasta County Clerk and Registrar of Voters, Shasta County, California)</li>
	<li>Bill Gates (Maricopa County Board of Supervisors, Phoenix, Arizona).</li>
	<li>Michella Huff (Surry County Board of Elections, Dobson, North Carolina).</li>
	<li>Stephen Richer (Maricopa County Recorder, Phoenix, Arizona).</li>
	<li>Meagan Wolfe (Wisconsin Elections Commission, Madison, Wisconsin).</li>
	</ul>
	HTML;




$div_ABA_announces_recipients_of_Unsung_Heroes_of_Democracy_Award = new WebsiteContentSection();
$div_ABA_announces_recipients_of_Unsung_Heroes_of_Democracy_Award->setTitleText("ABA announces recipients of Unsung Heroes of Democracy Award ");
$div_ABA_announces_recipients_of_Unsung_Heroes_of_Democracy_Award->setTitleLink("https://www.americanbar.org/news/abanews/aba-news-archives/2024/07/unsung-heroes-of-democracy-recipients/");
$div_ABA_announces_recipients_of_Unsung_Heroes_of_Democracy_Award->content = <<<HTML
	<p>The award recognizes those individuals and organizations who work every day, often behind the scenes or without fanfare,
	to ensure that our elections are secure and that the democratic ideals set forth in the U.S. Constitution are upheld.
	The award was open to everyday lawyers, state and local officials and other citizens who have stood up for democracy and free and fair elections in their communities.
	Nonprofit organizations that support election workers and lawyers under threat and exhibit a strong commitment to the Constitution,
	rule of law and democracy also were eligible.</p>

	<p>“Our democracy and our election process have been under attack in recent years,” ABA President Mary Smith said.
	“With our Unsung Heroes of Democracy Award, the ABA wants to recognize those individuals and groups
	that have answered the call to stand up for our system of government.
	By simply doing their jobs, they help ensure our democratic system will endure.
	By honoring these people and organizations, we hope we can inspire more people to become involved and to protect democracy and the rule of law.”</p>
	HTML;



$page->parent('list_of_awards.html');
$page->body($div_introduction);

$page->body($div_2024_Awards);
$page->body($div_ABA_announces_recipients_of_Unsung_Heroes_of_Democracy_Award);
