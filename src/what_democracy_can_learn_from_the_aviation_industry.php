<?php
$page = new Page();
$page->h1("What Democracy can learn from the aviation industry");
$page->tags("Democracy: WIP");
$page->keywords("What Democracy can learn from the aviation industry");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

$r1 = $page->ref("https://www.youtube.com/watch?v=QoblMHJzEbY", "The Terrifying Flight Scandal That Shook the Aviation World! | Mayday: Air Disaster (47'26)");
$r2 = $page->ref("https://www.kxan.com/business/press-releases/ein-presswire/612028623/los-angeles-lawyer-ronald-l-m-goldman-retires-after-60-years-in-practice/", "Los Angeles Lawyer Ronald L. M. Goldman Retires After 60 Years in Practice");
// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<blockquote>The acceptance of accountability suggests that the recommendations made by the safety investigators
	are not to be taken as a book to throw in a drawer and forget about but are to be taken seriously,
	because if you don't take them seriously there's going to be public accountability as well as private accountability.<br>
	- Ron Goldman, Aviation attorney, representing the families of the victims of Flight 5481.$r1 $r2
	</blockquote>

	<p>Flying a commercial airplane is one of the safest way to travel because after every singe incident or accident,
	there is a full scale investigation in order to clearly determine the root causes of the mishaps, and prevent future loss of life.</p>

	<p>Democracies should do the same: we have to learn at least from the most troubling events which affect our democracies.</p>
	HTML;


$page->parent('wip.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("What Democracy can learn from the aviation industry");
