<?php
$page = new Page();
$page->h1("Rights");
$page->tags("Individual: Rights");
$page->keywords("Rights", "rights", "right");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;


$page->parent('individuals.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Rights");
