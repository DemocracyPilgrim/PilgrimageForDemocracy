<?php
$page = new Page();
$page->h1("Gideon v. Wainwright (1963)");
$page->tags("Institutions: Judiciary", "SCOTUS Landmark Decision", "USA", "Justice");
$page->keywords("Gideon v. Wainwright");
$page->stars(1);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>"Gideon v. Wainwright" is a 1963 landmark decision by the ${'Supreme Court of the United States'}.</p>

	<p>This decision established the right to legal representation for all defendants in criminal cases, even those who cannot afford to hire an attorney.</p>

	<p>This ruling ensured that all defendants have access to legal counsel, regardless of their financial means,
	promoting a fairer and more equitable criminal justice system.</p>
	HTML;

$div_wikipedia_Gideon_v_Wainwright = new WikipediaContentSection();
$div_wikipedia_Gideon_v_Wainwright->setTitleText("Gideon v Wainwright");
$div_wikipedia_Gideon_v_Wainwright->setTitleLink("https://en.wikipedia.org/wiki/Gideon_v._Wainwright");
$div_wikipedia_Gideon_v_Wainwright->content = <<<HTML
	<p>Gideon v. Wainwright, 372 U.S. 335 (1963), was a landmark U.S. Supreme Court decision
	in which the Court ruled that the Sixth Amendment of the U.S. Constitution requires U.S. states to provide attorneys to criminal defendants
	who are unable to afford their own.
	The case extended the right to counsel, which had been found under the Fifth and Sixth Amendments to impose requirements on the federal government,
	by imposing those requirements upon the states as well.</p>

	<p>The Court reasoned that the assistance of counsel is
	"one of the safeguards of the Sixth Amendment deemed necessary to insure fundamental human rights of life and liberty",
	and that the Sixth Amendment serves as a warning that "if the constitutional safeguards it provides be lost, justice will not still be done."</p>
	HTML;


$page->parent('supreme_court_of_the_united_states.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_wikipedia_Gideon_v_Wainwright);
