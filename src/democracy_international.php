<?php
$page = new Page();
$page->h1("Democracy International ");
$page->keywords("Democracy International ");
$page->tags("Organisation");
$page->stars(0);

//$page->snp("description", "");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref('', '');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;



$div_Democracy_International_website = new WebsiteContentSection();
$div_Democracy_International_website->setTitleText("Democracy International website ");
$div_Democracy_International_website->setTitleLink("https://democracyinternational.com/");
$div_Democracy_International_website->content = <<<HTML
	<p>We are committed to supporting active citizens, responsive governments, and engaged civil society and political organizations
	to achieve a more peaceful, democratic world. By developing and using new knowledge, tools, and approaches,
	we change people’s lives and improve development assistance in fundamental ways.
	To do this, Democracy International works in four core practice areas:
	Politics, Governance, Peace & Resilience, and Learning.</p>
	HTML;



$div_wikipedia_Democracy_International_American_organization = new WikipediaContentSection();
$div_wikipedia_Democracy_International_American_organization->setTitleText("Democracy International American organization");
$div_wikipedia_Democracy_International_American_organization->setTitleLink("https://en.wikipedia.org/wiki/Democracy_International_(American_organization)");
$div_wikipedia_Democracy_International_American_organization->content = <<<HTML
	<p>'Democracy International, Inc., is a US-based organization
	that advises and assists, on behalf of governments, ministries and NGOs in democracy and governance projects,
	such as in the conduct of elections, election monitoring and building of multi-party systems.</p>
	HTML;


$page->parent('democracy_international.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_Democracy_International_website);
$page->body($div_wikipedia_Democracy_International_American_organization);
