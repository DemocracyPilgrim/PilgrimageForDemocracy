<?php
$page = new Page();
$page->h1("Stanford Internet Observatory");
$page->tags("Organisation", "Information: Social Networks", "Artificial Intelligence", "Social Networks");
$page->keywords("Stanford Internet Observatory");
$page->stars(1);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

$r1 = $page->ref("https://cyber.fsi.stanford.edu/io/about", "About the Internet Observatory");
$r2 = $page->ref("https://www.platformer.news/stanford-internet-observatory-shutdown-stamos-diresta-sio/", "The Stanford Internet Observatory is being dismantled");
$r3 = $page->ref("https://www.theguardian.com/commentisfree/article/2024/jun/29/closing-the-stanford-internet-observatory-will-edge-the-us-towards-the-end-of-democracy", "Closing the Stanford Internet Observatory will edge the US towards the end of democracy");
$r4 = $page->ref("https://www.npr.org/2024/06/14/g-s1-4570/a-major-disinformation-research-teams-future-is-uncertain-after-political-attacks", "A major disinformation research team's future is uncertain after political attacks");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>The Stanford Internet Observatory is a research program by Stanford University on information technologies, ${'artificial intelligence'},
	and ${'social networks'}.</p>

	<p>In 2024, the program was wound down, although not halted, due to pressure and abusive lawsuits from Republican and right wing groups. $r2 $r3 $r4</p>

	<p>${'Renée DiResta'} was its former research director.</p>
	HTML;



$div_Stanford_Internet_Observatory = new WebsiteContentSection();
$div_Stanford_Internet_Observatory->setTitleText("Stanford Internet Observatory ");
$div_Stanford_Internet_Observatory->setTitleLink("https://cyber.fsi.stanford.edu/io");
$div_Stanford_Internet_Observatory->content = <<<HTML
	<p>No area of society has avoided disruption by new internet technologies.
	Democracy, media, education… these fields will face continued upheavals as we deal with the impact of billions of people
	having access to both high-quality information and disinformation on demand.</p>

	<p>Unfortunately, the political and social sciences have been slow to build their capabilities to study the negative impact of technology,
	partially due to a lack of data access, information processing resources and individuals with the necessary backgrounds to sift through exabytes of data.
	For centuries, physicists and astronomers have coordinated resources to build massive technological infrastructure to further their field.
	With infinitely expanding data and content, researchers need infrastructural capabilities to research this new information frontier.</p>

	<p>The Stanford Internet Observatory is a cross-disciplinary program of research, teaching and policy engagement
	for the study of abuse in current information technologies, with a focus on social media.
	The Observatory was created to learn about the abuse of the internet in real time,
	to develop a novel curriculum on trust and safety that is a first in computer science,
	and to translate our research discoveries into training and policy innovations for the public good.</p>

	<p>By providing researchers across Stanford with cutting edge data analytics and machine learning resources we will unlock completely unforeseen fields of research.
	We envision a world where researchers do not limit themselves to the data that is easy to access,
	but instead dive into the toughest and most important questions by leveraging the capabilities of the Stanford Internet Observatory. $r1</p>
	HTML;



$div_wikipedia_Stanford_Internet_Observatory = new WikipediaContentSection();
$div_wikipedia_Stanford_Internet_Observatory->setTitleText("Stanford Internet Observatory");
$div_wikipedia_Stanford_Internet_Observatory->setTitleLink("https://en.wikipedia.org/wiki/Stanford_Internet_Observatory");
$div_wikipedia_Stanford_Internet_Observatory->content = <<<HTML
	<p>The Stanford Internet Observatory (SIO) is a multidisciplinary program for the study of abuse in information technologies,
	with a focus on social media, established in 2019.
	It is part of the Stanford Cyber Policy Center, a joint initiative of the Freeman Spogli Institute for International Studies and Stanford Law School.</p>
	HTML;


$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_Stanford_Internet_Observatory);
$page->body($div_wikipedia_Stanford_Internet_Observatory);
