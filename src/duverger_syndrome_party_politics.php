<?php
$page = new Page();
$page->h1('Duverger symptom 3: party politics');
$page->stars(0);
$page->tags("Elections: Duverger Syndrome");
$page->keywords('party politics');

//$page->snp('description', "");
//$page->snp('image', "/copyrighted/");

$page->preview( <<<HTML
	<p>Putting party over country, the rat race for power...</p>
	HTML );

$r1 = $page->ref('https://www.ajc.com/opinion/geoff-duncan-why-im-voting-for-biden-and-other-republicans-should-too/LFLE5YWCBBA6VDGJAJKMNPCDKQ/',
                 'Geoff Duncan: Why I’m voting for Biden and other Republicans should, too');
$r2 = $page->ref('https://www.nytimes.com/2024/04/11/us/burt-jones-trump-georgia.html',
                 'Georgia’s Lieutenant Governor to Face Inquiry for Role as Fake Trump Elector');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Putting party over country, the rat race for power...
	Party politics today is a very poor indication of what democracy could be.
	Indeed, it is one of the most obvious symptom of the ${'Duverger Syndrome'},
	the most critical disease of our current political system.</p>

	<p>$Politics is a noble art. At least, it should be.
	Politics should be all about serious, open-minded even if partisan, discussions
	and exchange of ideas about policies that may help improve our society,
	and make people's lives easier.</p>

	<p>Instead, we get a very sorry, disquieting media circus that puts people off politics...
	This is not how it should be.</p>

	<p>In a healthy democracy, there is a role to play for political parties.
	It is ok to be partisan and to have opinions and values.
	But what we have today is anything but healthy and productive.</p>
	HTML;


$div_Horse_race = new ContentSection();
$div_Horse_race->content = <<<HTML
	<h3>Horse race</h3>

	<p>Whenever news media cover politics, especially during the run up to an election,
	all we hear is about horse races:
	The necessary discussion about policies is largely overshadowed by frantic analysis on who is leading in the polls.</p>
	HTML;


$div_Putting_party_over_country = new ContentSection();
$div_Putting_party_over_country->content = <<<HTML
	<h3>Putting party over country</h3>

	<p>We all recognize it, when politicians put party over country.</p>

	<p>One typical example is when elected lawmakers create mayhem while in opposition,
	just so that they can use the ensuing disastrous results against their opponent during the next election.</p>

	<p>We could explore many more such examples and define patterns of behaviour...</p>
	HTML;


$div_Lieutenant_Governor_of_Georgia = new ContentSection();
$div_Lieutenant_Governor_of_Georgia->content = <<<HTML
	<h3>The case of the Lieutenant Governor of Georgia</h3>

	<p>In May 2024, Geoff Duncan, the former Lieutenant Governor of Georgia, did the unthinkable:
	by the standards of US politics, he should have put party over country,
	and as a Republican, he should have put ${'Donald Trump'} MAGA cult agenda over his party's traditional conservative values.
	However, he did neither.
	Not only is he one of the rare Republicans who stood fast against Trumpian fascism,
	he is one of the even rare Republicans who explicitly endorsed president Biden.
	He wrote: $r1</p>

	<blockquote>
		<p>It’s disappointing to watch an increasing number of Republicans fall in line behind former president Donald Trump. (...) This mentality is dead wrong.</p>

		<p>Yes, elections are a binary choice.
		Yes, serious questions linger about President Biden’s ability to serve until the age of 86. His progressive policies aren’t to conservatives’ liking.</p>

		<p>(...) The alternative is another term of Trump, a man who has disqualified himself through his conduct and his character.
		(...) Most important, Trump fanned the flames of unfounded conspiracy theories that led to the horrific events of Jan. 6, 2021.
		He refuses to admit he lost the last election and has hinted he might do so again after the next one.</p>
	</blockquote>

	<p>And before concluding, Duncan refers to the Time magazine's interview of Trump,
	where the disgraced president is not shy to expose his authoritarian tendency:</p>

	<blockquote>
		<p>The healing of the Republican Party cannot begin with Trump as president
		(and that’s aside from the untold damage that potentially awaits our country).
		A forthcoming Time magazine cover story lays out in stark terms
		“the outlines of an imperial presidency that would reshape America and its role in the world.”</p>

		<p>Unlike Trump, I’ve belonged to the GOP my entire life.
		This November, I am voting for a decent person I disagree with on policy
		over a criminal defendant without a moral compass.</p>
	</blockquote>

	<p>Duncan acted as Georgia’s lieutenant governor until 2023, and was succeeded by Burt Jones,
	an election denier who acted as a fake Trump elector. $r2</p>

	<p>We could quote other brave Republicans who stood strong against the worst inclinations of their party,
	like Georgia Secretary of State Brad Raffensperger,
	former RNC chairman Michael Steele or Liz Cheney, Mitt Romney, Adam Kinzinger, late John McCain
	and a few others..., unfortunately, too few...</p>
	HTML;


$div_The_proper_role_of_political_parties = new ContentSection();
$div_The_proper_role_of_political_parties->content = <<<HTML
	<h3>The proper role of political parties</h3>

	<p>The role that political parties play today is not what it should be.
	Once we have fixes the root causes of the ${'Duverger Syndrome'}
	by adopting a better ${'voting method'}, we should be able to see
	what more constructive role political parties and civil societies can play
	within the society, during the electoral process and in government.</p>
	HTML;



$page->parent('duverger_syndrome.html');
$page->parent('duverger_syndrome_alternance.html');
$page->template("stub");
$page->body($div_introduction);
$page->body($div_Horse_race);
$page->body($div_Putting_party_over_country);
$page->body($div_Lieutenant_Governor_of_Georgia);

$page->body($div_The_proper_role_of_political_parties);

$page->related_tag("Party Politics");
$page->body('the_end_of_political_parties.html');

$page->body('duverger_syndrome_negative_campaigning.html');
