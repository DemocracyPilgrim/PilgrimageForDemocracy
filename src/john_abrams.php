<?php
$page = new PersonPage();
$page->h1('John Abrams');
$page->alpha_sort("Abrams, John");
$page->keywords('John Abrams');
$page->stars(0);
$page->tags("Person", "Fair Share");

$page->snp('description', 'American entrepreneur.');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p>John Abrams is the founder of ${'South Mountain'} company, which is a ${'B Corp'} rated company.</p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>John Abrams is the founder of ${'South Mountain'} company, which is a ${'B Corp'} rated company.</p>
	HTML;


$div_John_Abrams_Kim_Angell = new WebsiteContentSection();
$div_John_Abrams_Kim_Angell->setTitleText('John Abrams & Kim Angell');
$div_John_Abrams_Kim_Angell->setTitleLink('https://abramsangell.com/');
$div_John_Abrams_Kim_Angell->content = <<<HTML
	<p>"<em>We work with progressive small businesses in any sector, as well as non-profits, families, individuals, and communities.
	We provide guidance to increase effectiveness and achieve social, environmental, and financial goals.</em>"</p>
	HTML;



$div_youtube_John_Abrams_Thinking_Like_Cathedral_Builders = new YoutubeContentSection();
$div_youtube_John_Abrams_Thinking_Like_Cathedral_Builders->setTitleText('John Abrams: Thinking Like Cathedral Builders');
$div_youtube_John_Abrams_Thinking_Like_Cathedral_Builders->setTitleLink('https://www.youtube.com/watch?v=iEyZHOTS2Yo');
$div_youtube_John_Abrams_Thinking_Like_Cathedral_Builders->content = <<<HTML
	HTML;



$div_Our_people_make_the_difference = new WebsiteContentSection();
$div_Our_people_make_the_difference->setTitleText('South Mountain Company: team');
$div_Our_people_make_the_difference->setTitleLink('https://www.southmountain.com/who-we-are/our-team/');
$div_Our_people_make_the_difference->content = <<<HTML
	<p>John Abrams: Founder & President Emeritus of South Mountain Company.</p>
	HTML;



$page->parent('list_of_people.html');
$page->template("stub");
$page->body($div_introduction);

$page->body('south_mountain_company.html');

$page->body($div_John_Abrams_Kim_Angell);
$page->body($div_Our_people_make_the_difference);
$page->body($div_youtube_John_Abrams_Thinking_Like_Cathedral_Builders);
