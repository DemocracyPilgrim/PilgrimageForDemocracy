<?php
$page = new Page();
$page->h1("Addendum B: Teamwork");
$page->tags("Topic portal", "Society", "Democracy: WIP");
$page->keywords("Teamwork", "teamwork");
$page->stars(0);
$page->viewport_background("/free/teamwork.png");

$page->snp("description", "Democracy is a job for all of us.");
$page->snp("image",       "/free/teamwork.1200-630.png");

$page->preview( <<<HTML
	<p></p>
	HTML );

$r1 = $page->ref("https://barackobama.com/about/", "About the Office of Barack Obama");
// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<blockquote>
		“True democracy is a project that’s much bigger than any one of us.
		It’s bigger than any one person, any one president, and any one government.
		It’s a job for all of us.”
		<br>— ${'Barack Obama'} $r1
	</blockquote>
	HTML;

$div_wikipedia_Teamwork = new WikipediaContentSection();
$div_wikipedia_Teamwork->setTitleText("Teamwork");
$div_wikipedia_Teamwork->setTitleLink("https://en.wikipedia.org/wiki/Teamwork");
$div_wikipedia_Teamwork->content = <<<HTML
	<p>Teamwork is the collaborative effort of a group to achieve a common goal or to complete a task in an effective and efficient way. Teamwork is seen within the framework of a team, which is a group of interdependent individuals who work together towards a common goal.</p>
	HTML;


$page->parent('the_soloist_and_the_choir.html');
$page->previous("wip.html");
$page->next("spirit.html");

$page->body($div_introduction);



$page->related_tag("Teamwork");
$page->body($div_wikipedia_Teamwork);
