<?php
$page = new Page();
$page->h1("Concession speech");
$page->tags("Elections", "Concession");
$page->keywords("concession speech", "Concession Speech");
$page->stars(0);
$page->viewport_background("/free/concession_speech.png");

$page->snp("description", "A nice concession speech goes a long way towards preserving democracy...");
$page->snp("image",       "/free/concession_speech.1200-630.png");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;


$page->parent('elections.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Concession Speech");
