<?php
$page = new PersonPage();
$page->h1("A. J. Muste");
$page->alpha_sort("Muste, Abraham Johannes");
$page->tags("Person", "Peace");
$page->keywords("A. J. Muste");
$page->stars(0);
$page->viewport_background("/free/a_j_muste.png");

$page->snp("description", "American clergyman and peace activist");
$page->snp("image",       "/free/a_j_muste.1200-630.png");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_peace_is_the_only_way = new ContentSection();
$div_peace_is_the_only_way->content = <<<HTML
	<blockquote>
	There is no way to peace; peace is the way.
	<br>
	— A. J. Muste
	</blockquote>
	HTML;


$div_quote_war_and_peace = new ContentSection();
$div_quote_war_and_peace->content = <<<HTML

	<blockquote>
	We cannot have peace if we are only concerned with peace.
	War is not an accident.
	It is the logical outcome of a certain way of life.
	If we want to attack war, we have to attack that way of life.
	<br>
	— A. J. Muste
	</blockquote>
	HTML;


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_A_J_Muste = new WikipediaContentSection();
$div_wikipedia_A_J_Muste->setTitleText("A J Muste");
$div_wikipedia_A_J_Muste->setTitleLink("https://en.wikipedia.org/wiki/A._J._Muste");
$div_wikipedia_A_J_Muste->content = <<<HTML
	<p>Abraham Johannes Muste (1885 – 1967), usually cited as A. J. Muste, was a Dutch-born American clergyman and political activist. He is best remembered for his work in the labor movement, pacifist movement, antiwar movement, and civil rights movement.</p>
	HTML;


$page->parent('list_of_people.html');
$page->template("stub");

$page->body($div_peace_is_the_only_way);
$page->body($div_quote_war_and_peace);

$page->body($div_introduction);



$page->related_tag("A. J. Muste");
$page->body($div_wikipedia_A_J_Muste);
