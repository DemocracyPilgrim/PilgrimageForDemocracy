<?php
$page = new Page();
$page->h1('Property Tax');
$page->keywords('property tax');
$page->tags("Tax");
$page->stars(0);

//$page->snp('description', '');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>In $Taiwan, lawmakers propose a bill to levy a vacancy tax on homeowners
	to release more vacant homes onto the housing market and lower property prices.</p>
	HTML;

$div_wikipedia_Property_tax = new WikipediaContentSection();
$div_wikipedia_Property_tax->setTitleText('Property tax');
$div_wikipedia_Property_tax->setTitleLink('https://en.wikipedia.org/wiki/Property_tax');
$div_wikipedia_Property_tax->content = <<<HTML
	<p>A property tax is an ad valorem tax on the value of a property.</p>
	HTML;

$div_wikipedia_Bedroom_tax = new WikipediaContentSection();
$div_wikipedia_Bedroom_tax->setTitleText('Bedroom tax');
$div_wikipedia_Bedroom_tax->setTitleLink('https://en.wikipedia.org/wiki/Bedroom_tax');
$div_wikipedia_Bedroom_tax->content = <<<HTML
	<p>The under-occupancy penalty results from a provision of the British Welfare Reform Act 2012
	whereby tenants living in public housing with rooms deemed "spare" face a reduction in Housing Benefit.</p>
	HTML;


$page->parent('taxes.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_wikipedia_Property_tax);
$page->body($div_wikipedia_Bedroom_tax);
