<?php
$page = new Page();
$page->h1("Russian interference in US elections");
$page->tags("Elections", "Russia", "USA", "Donald Trump");
$page->keywords("Russian interference in US elections");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Russian_interference_in_the_2016_United_States_elections = new WikipediaContentSection();
$div_wikipedia_Russian_interference_in_the_2016_United_States_elections->setTitleText("Russian interference in the 2016 United States elections");
$div_wikipedia_Russian_interference_in_the_2016_United_States_elections->setTitleLink("https://en.wikipedia.org/wiki/Russian_interference_in_the_2016_United_States_elections");
$div_wikipedia_Russian_interference_in_the_2016_United_States_elections->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Timeline_of_Russian_interference_in_the_2016_United_States_elections = new WikipediaContentSection();
$div_wikipedia_Timeline_of_Russian_interference_in_the_2016_United_States_elections->setTitleText("Timeline of Russian interference in the 2016 United States elections");
$div_wikipedia_Timeline_of_Russian_interference_in_the_2016_United_States_elections->setTitleLink("https://en.wikipedia.org/wiki/Timeline_of_Russian_interference_in_the_2016_United_States_elections");
$div_wikipedia_Timeline_of_Russian_interference_in_the_2016_United_States_elections->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Mueller_special_counsel_investigation = new WikipediaContentSection();
$div_wikipedia_Mueller_special_counsel_investigation->setTitleText("Mueller special counsel investigation");
$div_wikipedia_Mueller_special_counsel_investigation->setTitleLink("https://en.wikipedia.org/wiki/Mueller_special_counsel_investigation");
$div_wikipedia_Mueller_special_counsel_investigation->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Russian_interference_in_the_2024_United_States_elections = new WikipediaContentSection();
$div_wikipedia_Russian_interference_in_the_2024_United_States_elections->setTitleText("Russian interference in the 2024 United States elections");
$div_wikipedia_Russian_interference_in_the_2024_United_States_elections->setTitleLink("https://en.wikipedia.org/wiki/Russian_interference_in_the_2024_United_States_elections");
$div_wikipedia_Russian_interference_in_the_2024_United_States_elections->content = <<<HTML
	<p>
	</p>
	HTML;


$page->parent('elections.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_wikipedia_Russian_interference_in_the_2016_United_States_elections);
$page->body($div_wikipedia_Timeline_of_Russian_interference_in_the_2016_United_States_elections);
$page->body($div_wikipedia_Mueller_special_counsel_investigation);
$page->body($div_wikipedia_Russian_interference_in_the_2024_United_States_elections);
