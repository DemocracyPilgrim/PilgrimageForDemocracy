<?php
$page = new Page();
$page->h1("Observer-expectancy effect (expectation bias)");
$page->tags("Information: Discourse", "Logical Fallacy");
$page->keywords("Observer-expectancy effect", "expectation bias");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Observer_expectancy_effectrver_expectancy_effect = new WikipediaContentSection();
$div_wikipedia_Observer_expectancy_effectrver_expectancy_effect->setTitleText("Observer expectancy effectrver expectancy effect");
$div_wikipedia_Observer_expectancy_effectrver_expectancy_effect->setTitleLink("https://en.wikipedia.org/wiki/Observer-expectancy_effectrver-expectancy_effect");
$div_wikipedia_Observer_expectancy_effectrver_expectancy_effect->content = <<<HTML
	<p>The observer-expectancy effect is a form of reactivity in which a researcher's cognitive bias causes them to subconsciously influence the participants of an experiment. Confirmation bias can lead to the experimenter interpreting results incorrectly because of the tendency to look for information that conforms to their hypothesis, and overlook information that argues against it. It is a significant threat to a study's internal validity, and is therefore typically controlled using a double-blind experimental design.</p>
	HTML;


$page->parent('information.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Observer-expectancy effect");
$page->body($div_wikipedia_Observer_expectancy_effectrver_expectancy_effect);
