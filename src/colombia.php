<?php
$page = new CountryPage('Colombia');
$page->h1('Colombia');
$page->tags("Country");
$page->keywords('Colombia');
$page->stars(2);
$page->viewport_background('/free/colombia.png');

$page->preview( <<<HTML
	<p></p>
	HTML );

$page->snp('description', '49 million inhabitants.');
$page->snp('image',       '/free/colombia.1200-630.png');



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Colombia, a country of vibrant culture and breathtaking landscapes, is also a nation grappling with the complex legacies of a long and often violent history.
	While it has made significant strides in strengthening its democracy, the challenges surrounding justice and equality remain considerable.</p>

	<p>Colombia's democratic framework, built upon a constitutionally defined multi-party system, has seen periods of both progress and strain.
	The country has successfully transitioned from armed conflict toward peace negotiations, expanding opportunities for political participation.
	Elections are held regularly, and freedom of speech, while often under threat, is generally upheld.</p>

	<p>However, the roots of Colombia’s internal conflict continue to cast a shadow.
	The justice system faces immense challenges in addressing past human rights abuses, impunity for criminal actors,
	and inequalities that pervade many sectors of society.
	While there have been notable steps toward reconciliation and truth-telling,
	the pursuit of accountability for perpetrators of violence and displacement remains incomplete.</p>

	<p>Furthermore, the rise of organized crime, including drug trafficking and illegal mining,
	poses significant threats to democratic institutions and the rule of law.
	Corruption remains a pervasive issue, eroding public trust and hindering effective governance.
	Indigenous communities, Afro-Colombian populations, and rural areas often experience systemic discrimination and limited access to justice.</p>

	<p>Despite these challenges, Colombia also demonstrates remarkable resilience.
	Civil society organizations are actively working to promote human rights, advocate for marginalized groups, and monitor the justice system.
	The country is actively engaging in ongoing efforts to strengthen democratic institutions and address long-standing inequalities.</p>

	<p>The situation in Colombia underscores a crucial reality: democracy is not simply a matter of holding elections.
	It also requires a robust, independent, and equitable justice system capable of protecting the rights of all citizens,
	regardless of their background or social standing.
	As Colombia continues on its path, progress towards durable peace and inclusive justice will be crucial to the future of its democracy.</p>

	<p>As of September 2021, 611 ${'environmental defenders'} had already been assassinated in Colombia
	since the signing of the 2016 Peace Deal,
	according to the Colombian Institute of Studies for Development and Peace (Indepaz).
	Of those 611 people, 332 were Indigenous.</p>
	HTML;






$div_wikipedia_Colombia = new WikipediaContentSection();
$div_wikipedia_Colombia->setTitleText('Colombia');
$div_wikipedia_Colombia->setTitleLink('https://en.wikipedia.org/wiki/Colombia');
$div_wikipedia_Colombia->content = <<<HTML
	<p>The Republic of Colombia is a country mostly in South America with insular regions in North America.
	The Colombian mainland is bordered by the Caribbean Sea to the north, Venezuela to the east and northeast,
	Brazil to the southeast, Ecuador and Peru to the south and southwest, the Pacific Ocean to the west, and Panama to the northwest.</p>
	HTML;

$div_wikipedia_Crime_in_Colombia = new WikipediaContentSection();
$div_wikipedia_Crime_in_Colombia->setTitleText('Crime in Colombia');
$div_wikipedia_Crime_in_Colombia->setTitleLink('https://en.wikipedia.org/wiki/Crime_in_Colombia');
$div_wikipedia_Crime_in_Colombia->content = <<<HTML
	<p>Colombia has a high crime rate due to being a center for the cultivation and trafficking of cocaine.</p>
	HTML;

$div_wikipedia_Freedom_of_religion_in_Colombia = new WikipediaContentSection();
$div_wikipedia_Freedom_of_religion_in_Colombia->setTitleText('Freedom of religion in Colombia');
$div_wikipedia_Freedom_of_religion_in_Colombia->setTitleLink('https://en.wikipedia.org/wiki/Freedom_of_religion_in_Colombia');
$div_wikipedia_Freedom_of_religion_in_Colombia->content = <<<HTML
	<p>Freedom of religion in Colombia is enforced by the State and well tolerated in the Colombian culture.</p>
	HTML;

$div_wikipedia_Colombian_conflict = new WikipediaContentSection();
$div_wikipedia_Colombian_conflict->setTitleText('Colombian conflict');
$div_wikipedia_Colombian_conflict->setTitleLink('https://en.wikipedia.org/wiki/Colombian_conflict');
$div_wikipedia_Colombian_conflict->content = <<<HTML
	<p>The Colombian conflict began in the mid-1960s and is a low-intensity asymmetric war between Colombian governments,
	paramilitary groups, crime syndicates,
	and left-wing guerrillas such as the Revolutionary Armed Forces of Colombia (FARC), and the National Liberation Army (ELN),
	fighting each other to increase their influence in Colombian territory.
	Two of the most important international actors that have contributed to the Colombian conflict are multinational companies and the United States.</p>
	HTML;

$div_wikipedia_Colombian_peace_process = new WikipediaContentSection();
$div_wikipedia_Colombian_peace_process->setTitleText('Colombian peace process');
$div_wikipedia_Colombian_peace_process->setTitleLink('https://en.wikipedia.org/wiki/Colombian_peace_process');
$div_wikipedia_Colombian_peace_process->content = <<<HTML
	<p>The Colombian peace process is the peace process between the Colombian government of President Juan Manuel Santos
	and the Revolutionary Armed Forces of Colombia (FARC–EP)
	to bring an end to the Colombian conflict,
	which eventually led to the Peace Agreements between the Colombian Government of Juan Manuel Santos and FARC-EP.</p>
	HTML;


$page->parent('world.html');
$page->body($div_introduction);

$page->related_tag("Colombia");

$page->body('Country indices');


$page->body($div_wikipedia_Colombia);
$page->body($div_wikipedia_Crime_in_Colombia);
$page->body($div_wikipedia_Freedom_of_religion_in_Colombia);
$page->body($div_wikipedia_Colombian_conflict);
$page->body($div_wikipedia_Colombian_peace_process);
