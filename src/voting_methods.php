<?php
require_once 'section/voting.php';

$page = new Page();
$page->h1("Voting methods");
$page->viewport_background('/free/voting_methods.png');
$page->keywords("Voting Methods", "voting methods", "voting method", "vote", "votes", "voting", "ballot", "ballots");
$page->stars(1);
$page->tags("Elections: Solution", "Electoral System");

//$page->snp("description", "");
$page->snp("image",       "/free/voting_methods.1200-630.png");

$page->preview( <<<HTML
	<p>One of the most critical priority for any democracy is to improve its electoral system and start using a much better voting method.</p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>One of the most critical priority for any democracy is to improve its ${'electoral system'} and start using a much better voting method.</p>

	<p>The topic of voting methods is a subset of the topic on electoral systems.</p>
	<ul>
		<li><strong>Electoral system</strong>: covers all rules and aspects that have an influence on any election.
		It include the voting method.</li>

		<li><strong>Voting method</strong>: covers more specifically the design of the ballot,
		the way voters are asked to express their preferences about candidates
		and how the ballots are processed and the vote counted.</li>
	</ul>
	HTML;

$h2_The_problem = new h2HeaderContent("The problem");

$h2_The_solution = new h2HeaderContent("The solution");




$page->parent('electoral_system.html');
$page->body($div_introduction);


voting_methods_article_menu($page);
