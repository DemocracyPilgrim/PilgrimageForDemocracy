<?php
$page = new Page();
$page->h1("Making Democracy Work");
$page->tags("Institutions");
$page->keywords("Making Democracy Work");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;


$page->parent('institutions.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Making Democracy Work");
