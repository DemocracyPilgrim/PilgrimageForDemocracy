<?php
$page = new Page();
$page->h1("Brown v. Board of Education (1954)");
$page->tags("Institutions: Judiciary", "SCOTUS Landmark Decision", "USA", "Education");
$page->keywords("Brown v. Board of Education");
$page->stars(1);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>"Brown v. Board of Education" is a 1954 landmark decision by the ${'Supreme Court of the United States'}.</p>

	<p>This monumental decision declared state laws establishing separate public $schools for black and white students unconstitutional,
	effectively overturning the "separate but equal" doctrine established by ${'Plessy v. Ferguson'} (1896).</p>

	<p>This ruling marked a turning point in the ${'Civil Rights'} Movement, paving the way for the desegregation of schools and other public institutions.
	It was a major victory for equality and social justice.</p>
	HTML;

$div_wikipedia_Brown_v_Board_of_Education = new WikipediaContentSection();
$div_wikipedia_Brown_v_Board_of_Education->setTitleText("Brown v Board of Education");
$div_wikipedia_Brown_v_Board_of_Education->setTitleLink("https://en.wikipedia.org/wiki/Brown_v._Board_of_Education");
$div_wikipedia_Brown_v_Board_of_Education->content = <<<HTML
	<p>Brown v. Board of Education of Topeka, 347 U.S. 483 (1954), was a landmark decision of the U.S. Supreme Court
	ruling that U.S. state laws establishing racial segregation in public schools are unconstitutional, even if the segregated schools are otherwise equal in quality.
	The decision partially overruled the Court's 1896 decision Plessy v. Ferguson, which had held that racial segregation laws did not violate the U.S. Constitution
	as long as the facilities for each race were equal in quality, a doctrine that had come to be known as "separate but equal".
	The Court's unanimous decision in Brown, and its related cases, paved the way for integration and was a major victory of the civil rights movement,
	and a model for many future impact litigation cases.</p>
	HTML;


$page->parent('supreme_court_of_the_united_states.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_wikipedia_Brown_v_Board_of_Education);
