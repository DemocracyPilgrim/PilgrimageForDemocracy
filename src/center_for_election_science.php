<?php
$page = new OrganisationPage();
$page->h1("Center for Election Science");
$page->tags("Organisation", "Elections", "Approval Voting");
$page->keywords("Center for Election Science");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;



$div_Center_for_Election_Science = new WebsiteContentSection();
$div_Center_for_Election_Science->setTitleText("Center for Election Science ");
$div_Center_for_Election_Science->setTitleLink("https://electionscience.org/");
$div_Center_for_Election_Science->content = <<<HTML
	<p>Founded in 2011, The Center for Election science is an national nonpartisan nonprofit focused on election analysis and voting reform advocacy.
	We are committed to translating knowledge and discovery into tangible, real-world changes to the voting system that empower all.</p>

	<p>We study elections, how people vote in them, and the obstacles - new and old, seen and unseen - that make voters feel disconnected from democracy.
	While the study of voting often gets overly academic or purely political, we strive to remain firmly grounded in the practical.</p>
	HTML;



$div_wikipedia_Center_for_Election_Science = new WikipediaContentSection();
$div_wikipedia_Center_for_Election_Science->setTitleText("Center for Election Science");
$div_wikipedia_Center_for_Election_Science->setTitleLink("https://en.wikipedia.org/wiki/Center_for_Election_Science");
$div_wikipedia_Center_for_Election_Science->content = <<<HTML
	<p>The Center for Election Science (CES) is an American 501(c)(3) organization that focuses on voter education and promoting election science.
	The organization promotes electoral systems favoured by social choice theorists, primarily cardinal voting methods such as approval and score voting.
	They have their early roots in effective altruism.</p>

	<p>The Center for Election Science helped pass approval voting in the city of Fargo, North Dakota, during the 2018 elections alongside Reform Fargo.
	In St. Louis, Missouri, the organization passed an approval voting law in 2020 with the help of St. Louis Approves.</p>
	HTML;


$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_Center_for_Election_Science);
$page->body($div_wikipedia_Center_for_Election_Science);
