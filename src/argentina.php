<?php
$page = new CountryPage('Argentina');
$page->h1('Argentina');
$page->tags("Country");
$page->keywords('Argentina');
$page->stars(0);

$page->snp('description', '47 million inhabitants.');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Argentina = new WikipediaContentSection();
$div_wikipedia_Argentina->setTitleText('Argentina');
$div_wikipedia_Argentina->setTitleLink('https://en.wikipedia.org/wiki/Argentina');
$div_wikipedia_Argentina->content = <<<HTML
	<p>Argentina is a regional power, and retains its historic status as a middle power in international affairs.
	A major non-NATO ally of the United States, Argentina is a developing country
	with the second-highest HDI (human development index) in Latin America after Chile.
	It maintains the second-largest economy in South America, and is a member of G-15 and G20.</p>
	HTML;

$div_wikipedia_Politics_of_Argentina = new WikipediaContentSection();
$div_wikipedia_Politics_of_Argentina->setTitleText('Politics of Argentina');
$div_wikipedia_Politics_of_Argentina->setTitleLink('https://en.wikipedia.org/wiki/Politics_of_Argentina');
$div_wikipedia_Politics_of_Argentina->content = <<<HTML
	<p>The politics of Argentina take place in the framework of what the Constitution defines as a federal presidential representative democratic republic,
	where the President of Argentina is both Head of State and Head of Government.
	Legislative power is vested in the two chambers of the Argentine National Congress.
	The Judiciary is independent, as well as the Executive and the Legislature.
	Elections take place regularly on a multi-party system.</p>
	HTML;


$page->parent('world.html');
$page->template("stub");
$page->body($div_introduction);
$page->body('Country indices');

$page->body($div_wikipedia_Argentina);
$page->body($div_wikipedia_Politics_of_Argentina);
