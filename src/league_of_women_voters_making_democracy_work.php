<?php
$page = new Page();
$page->h1("League of Women Voters: Making Democracy Work");
$page->tags("Institutions", "League of Women Voters", "Making Democracy Work", "Elections");
$page->keywords("League of Women Voters: Making Democracy Work");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>${"Making Democracy Work"} is a campaign launched in 2016 by the ${'League of Women Voters'}.</p>

	<p>The campaign focusses on the following areas:</p>

	<ul>
		<li>Voting Rights</li>
		<li>Improving Elections</li>
		<li>Campaign Finance/Money in Politics</li>
		<li>Redistricting</li>
		<li>Direct Election of the President by Popular Vote </li>
	</ul>
	HTML;




$div_Making_Democracy_Work = new WebsiteContentSection();
$div_Making_Democracy_Work->setTitleText("LWV: Making Democracy Work®");
$div_Making_Democracy_Work->setTitleLink("https://www.lwv.org/making-democracy-work");
$div_Making_Democracy_Work->content = <<<HTML
	<p>In 2016 the League of Women Voters launched the League-wide Campaign for Making Democracy Work®. This comprehensive program engages Leagues nationwide in advancing core democracy issues.</p>
	HTML;



$page->parent('institutions.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("League of Women Voters: Making Democracy Work");
$page->body($div_Making_Democracy_Work);
