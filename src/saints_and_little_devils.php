<?php
$page = new Page();
$page->h1("Addendum C: Democracy — Saints and Little Devils");
$page->viewport_background('/free/saints_and_little_devils.png');
$page->stars(0);
$page->tags();

//$page->snp("description", "");
$page->snp("image",       "/free/saints_and_little_devils.1200-630.png");

$page->preview( <<<HTML
	<p>Is democracy a fragile system destined to fail because of the flaws of human nature?
	Democracy is only as good as the people who wield its power.
	We ask: what kind of democracy would emerge from a nation of saints versus a nation of 'little devils,'
	and what does it tell us about our own responsibility?</p>
	HTML );


$h2_A_thought_experiment = new h2HeaderContent("A thought experiment");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Since in a $democracy, by definition, the power is in our hands,
	it is ultimately our responsibility what kind of democracy we shall have.
	What we are, collectively and individually, will ultimately dictate the quality of our democracy.</p>

	<p>A thought experiment: imagine a democracy of saints vs a democracy of little devils...</p>
	HTML;


$h2_Saints_and_Little_Devils = new h2HeaderContent("Saints and Little Devils");

$div_Two_countries = new ContentSection();
$div_Two_countries->content = <<<HTML
	<h3>Two countries</h3>

	<p>Let's imagine two different countries, both democracies.
	At the start, they have very similar democratic institutions.
	Their population, however, are different.
	The first country is populated by humble saints.
	The second country is populated by little devils...</p>

	<p>TBC (<em>Although you probably already can see where we're heading... ;) </em>)</p>
	HTML;


$h2_The_human_factor = new h2HeaderContent("The human factor");


$div_The_size_of_the_government = new ContentSection();
$div_The_size_of_the_government->content = <<<HTML
	<h3>The size of the government</h3>

	<p>
	</p>
	HTML;



$div_Criminality = new ContentSection();
$div_Criminality->content = <<<HTML
	<h3>Criminality</h3>

	<p>It is obvious that the fewer criminals there are in a society, the easier it would be for the society to police itself.
	The role of ${'criminal justice'} is critical in this aspect.
	Instead of turning first time criminals into hardened criminals,
	we should ensure that criminals are treated as diseased human beings in need of treatment.
	We ought to consider which treatment would reduce the probability of recidivism
	and enhance the potential for the converted criminal to become productive members of the society.</p>
	HTML;


$div_Humanity = new ContentSection();
$div_Humanity->content = <<<HTML
	<h3>Humanity</h3>

	<p>
	</p>
	HTML;



$list_all_related_topics = new ListOfPages();
$list_all_related_topics->add('spirit.html');
$print_list_all_related_topics = $list_all_related_topics->print();

$div_list_all_related_topics = new ContentSection();
$div_list_all_related_topics->content = <<<HTML
	$print_list_all_related_topics
	HTML;



$h2_Next = new h2HeaderContent("Next...");





$page->parent('democracy.html');
$page->previous('the_soloist_and_the_choir.html');
$page->next('teaching_democracy.html');

$page->body($h2_A_thought_experiment);
$page->template("stub");
$page->body($div_introduction);

$page->body($h2_Saints_and_Little_Devils);
$page->body($div_Two_countries);


$page->body($div_list_all_related_topics);

$page->body($h2_The_human_factor);
$page->body('institutional_vs_human_factors_in_democracy.html');

$page->body($div_Criminality);
$page->body('mass_shootings_the_human_factor.html');

$page->body($div_The_size_of_the_government);
$page->body('small_government.html');

$page->body($div_Humanity);
$page->body('eighth_level_of_democracy.html');

$page->body($h2_Next);
