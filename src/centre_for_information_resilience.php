<?php
$page = new Page();
$page->h1("Centre for Information Resilience");
$page->tags("Information: Media", "Organisation");
$page->keywords("Centre for Information Resilience");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>See: ${'fake accounts on social networks'}.</p>
	HTML;




$div_CENTRE_FOR_INFORMATION_RESILIENCE = new WebsiteContentSection();
$div_CENTRE_FOR_INFORMATION_RESILIENCE->setTitleText("Centre for Information Resilience");
$div_CENTRE_FOR_INFORMATION_RESILIENCE->setTitleLink("https://www.info-res.org/");
$div_CENTRE_FOR_INFORMATION_RESILIENCE->content = <<<HTML
	<p>The Centre for Information Resilience (CIR) is an independent, non-profit social enterprise
	dedicated to exposing human rights abuses and war crimes, countering disinformation
	and combating online behaviour harmful to women and minorities.</p>

	<p>We achieve these goals through open source research, digital investigations, building the capacity of local partners,
	and collaborating with media to amplify the impact of our work.</p>

	<p>CIR was born out of a determination to expose those spreading harm – online and offline – around the world, particularly in areas of violent conflict.
	Our projects in Myanmar, Ukraine and Afghanistan are at the forefront of efforts to investigate and document human rights abuses, war crimes,
	harms targeting women and minorities, and disinformation. We work closely with multilateral and national justice accountability bodies.</p>
	HTML;



$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);

$page->body($div_CENTRE_FOR_INFORMATION_RESILIENCE);
