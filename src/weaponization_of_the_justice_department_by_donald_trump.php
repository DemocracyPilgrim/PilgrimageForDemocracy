<?php
$page = new Page();
$page->h1("Weaponization of the Justice Department by Donald Trump");
$page->tags("Institutions: Executive", "Donald Trump", "Separation of Powers", "Donald Trump: Research");
$page->keywords("Trump DOJ Weaponization", "weaponization of the Justice Department by Donald Trump");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;


$page->parent('institutions.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Trump DOJ Weaponization");
