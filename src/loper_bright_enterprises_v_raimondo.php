<?php
$page = new Page();
$page->h1("Loper Bright Enterprises v. Raimondo (2024)");
$page->tags("Institutions: Judiciary", "SCOTUS Landmark Decision", "USA", "Institutions: Executive", "Pollution", "Separation of Powers");
$page->keywords("Loper Bright Enterprises v. Raimondo");
$page->stars(2);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>"Loper Bright Enterprises v. Raimondo" is a 2024 landmark decision by the ${'Supreme Court of the United States'}.</p>

	<p>This case revolved around a rule implemented by the Department of Commerce (led by Secretary Gina Raimondo)
	that required seafood importers to certify that their products were caught in a way that didn't harm endangered species.</p>

	<p>Loper Bright Enterprises, a small business, challenged the rule, arguing that the Department of Commerce lacked the authority to implement such a rule.
	They claimed that it was a "major question" that Congress had not explicitly authorized the agency to address.</p>

	<p>The Supreme Court ruled in favour of Loper Bright Enterprises, 6-3.
	The Court held that the Commerce Department's rule was indeed a "major question" that Congress had not clearly delegated to the agency.</p>

	<p>This decision significantly expands the major questions doctrine, making it more difficult for federal agencies
	to implement regulations that courts deem to have "vast economic and political significance" unless Congress has explicitly authorized them to do so.</p>

	<p>It overrules the principle of ${'Chevron Deference'} established in ${'Chevron U.S.A., Inc. v. Natural Resources Defense Council, Inc.'} (1984).</p>
	HTML;


$div_Impact = new ContentSection();
$div_Impact->content = <<<HTML
	<h3>Impact</h3>

	<p>The Loper Bright decision is likely to have a significant impact on agency regulations across various sectors,
	making it easier for businesses to challenge those regulations on the grounds that they exceed the agency's authority.</p>

	<p>The ruling is particularly relevant to environmental regulations,
	as it could make it more difficult for agencies like the EPA to implement rules related to climate change, pollution, and endangered species.</p>

	<p>The decision has sparked ongoing debate about the scope of the major questions doctrine and its implications for agency authority.</p>

	<p>Loper Bright is likely to lead to further litigation, as businesses and other entities seek to use the expanded doctrine to challenge regulations they oppose.</p>
	HTML;




$div_Loper_Bright_Enterprises_v_Raimondo = new WebsiteContentSection();
$div_Loper_Bright_Enterprises_v_Raimondo->setTitleText("Loper Bright Enterprises v. Raimondo and Relentless, Inc. v. Department of Commerce: Separation of Powers and Chevron Deference
");
$div_Loper_Bright_Enterprises_v_Raimondo->setTitleLink("https://constitution.congress.gov/browse/essay/intro.9-2-26/ALDE_00002830/");
$div_Loper_Bright_Enterprises_v_Raimondo->content = <<<HTML
	<p>Though the phrase separation of powers appears nowhere in the text of the Constitution,
	the Supreme Court has recognized separation of powers as a constitutional principle in cases throughout the nation’s history.
	The expansion of administrative agencies in the 20th Century led the Court to consider the degree to which executive branch agencies
	may exercise legislative power in implementing broad or ambiguous statutory mandates.
	(...)</p>

	<p>Loper Bright Enterprises v. Raimondo and Relentless, Inc. v. Department of Commerce
	involved whether the National Marine Fisheries Service (NMFS) properly interpreted the Magnuson-Stevens Fishery Conservation and Management Act (MSA).
	The MSA delegates regulatory authority over fisheries to the Secretary of Commerce, who in turn has delegated authority to NMFS to administer the statute.
	An MSA provision permits NMFS to require commercial fishing vessels to carry federal observers.
	The MSA expressly provides for vessels to cover the cost of these observers in three specific circumstances,
	but does not otherwise provide that vessels will cover the cost of observers.
	Another MSA provision authorizes any regulations deemed necessary and appropriate for the conservation and management of the fishery.
	NMFS issued a regulation generally requiring vessels to pay a per diem fee to federal observers where funds have not been appropriated to cover the observers’ costs.</p>
	HTML;


$div_wikipedia_Loper_Bright_Enterprises_v_Raimondo = new WikipediaContentSection();
$div_wikipedia_Loper_Bright_Enterprises_v_Raimondo->setTitleText("Loper Bright Enterprises v Raimondo");
$div_wikipedia_Loper_Bright_Enterprises_v_Raimondo->setTitleLink("https://en.wikipedia.org/wiki/Loper_Bright_Enterprises_v._Raimondo");
$div_wikipedia_Loper_Bright_Enterprises_v_Raimondo->content = <<<HTML
	<p>Loper Bright Enterprises v. Raimondo, 603 U.S. ___ (2024), is a landmark decision of the United States Supreme Court
	in the field of administrative law, the law governing regulatory agencies.
	Together with its companion case, Relentless, Inc. v. Department of Commerce, it overruled the principle of Chevron deference
	established in Chevron U.S.A., Inc. v. Natural Resources Defense Council, Inc. (1984),
	which had directed courts to defer to an agency's reasonable interpretation of an ambiguity in a law that the agency enforces.</p>
	HTML;


$page->parent('supreme_court_of_the_united_states.html');
$page->body($div_introduction);
$page->body($div_Impact);
$page->body($div_Loper_Bright_Enterprises_v_Raimondo);


$page->body($div_wikipedia_Loper_Bright_Enterprises_v_Raimondo);
