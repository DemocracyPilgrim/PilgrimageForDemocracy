<?php
$page = new CountryPage("New Zealand");
$page->h1("New Zealand");
$page->viewport_background("/free/new_zealand.png");
$page->keywords("New Zealand");
$page->stars(2);
$page->tags("Country");

$page->snp("description", "5.4 million inhabitants.");
$page->snp("image",       "/free/new_zealand.1200-630.png");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<h3>New Zealand: A Beacon of Democracy, Human Rights, and Social Justice</h3>

	<p>New Zealand, a nation renowned for its stunning natural beauty and vibrant culture,
	also stands as a notable example of a well-functioning democracy that actively promotes human rights, freedoms, and social justice.
	While no system is perfect, New Zealand is consistently recognized for its commitment to these values
	and for its ongoing efforts to address existing inequalities.</p>

	<p>New Zealand's parliamentary democracy is characterized by its stability, transparency, and high levels of citizen participation.
	Regular and fair elections, a robust legal framework, and a free press contribute to a healthy political landscape.
	The country’s electoral system, with mixed-member proportional representation, ensures that diverse voices are represented in Parliament.</p>

	<p>Human rights and civil liberties are strongly protected in New Zealand.
	The Human Rights Act 1993 prohibits discrimination based on a wide range of grounds,
	and the Bill of Rights Act 1990 guarantees fundamental freedoms such as freedom of expression, religion, and assembly.
	The country has a strong judiciary that plays a crucial role in upholding these rights.</p>

	<p>However, New Zealand also faces ongoing challenges in the area of social justice.
	Disparities in income and wealth, as well as inequities faced by Māori (the indigenous people of New Zealand), are key areas of concern.
	While significant efforts have been made to address the historical injustices faced by Māori,
	challenges related to land rights, cultural preservation, and socio-economic inequalities persist.</p>

	<p>New Zealand also recognizes the rights of other marginalized groups, including women, and people with disabilities.
	While laws are in place to protect their rights,
	ensuring these rights are fully realized in practice is an ongoing process that requires continuous effort.</p>

	<p>Moreover, New Zealand is actively engaged in international efforts to promote human rights and democratic values.
	It has taken a leading role in advocating for environmental protection and combating climate change, reflecting its commitment to global social justice.</p>

	<p>The success of New Zealand in these areas is not due to a perfect system, but rather to the country’s consistent efforts toward improvement,
	its open and transparent dialogue about problems and inequities, and the active engagement of its citizens in the democratic process.
	While work remains to be done, New Zealand's commitment to creating a society where everyone can flourish,
	regardless of their background or identity, sets an example for other nations to follow.</p>
	HTML;

$div_wikipedia_New_Zealand = new WikipediaContentSection();
$div_wikipedia_New_Zealand->setTitleText("New Zealand");
$div_wikipedia_New_Zealand->setTitleLink("https://en.wikipedia.org/wiki/New_Zealand");
$div_wikipedia_New_Zealand->content = <<<HTML
	<p>New Zealand is an island country in the southwestern Pacific Ocean. It consists of two main landmasses—the North Island (Te Ika-a-Māui) and the South Island (Te Waipounamu)—and over 600 smaller islands.</p>
	HTML;

$div_wikipedia_Human_rights_in_New_Zealand = new WikipediaContentSection();
$div_wikipedia_Human_rights_in_New_Zealand->setTitleText("Human rights in New Zealand");
$div_wikipedia_Human_rights_in_New_Zealand->setTitleLink("https://en.wikipedia.org/wiki/Human_rights_in_New_Zealand");
$div_wikipedia_Human_rights_in_New_Zealand->content = <<<HTML
	<p>Human rights in New Zealand are addressed in the various documents which make up the constitution of the country. Specifically, the two main laws which protect human rights are the New Zealand Human Rights Act 1993 and the New Zealand Bill of Rights Act 1990. In addition, New Zealand has also ratified numerous international United Nations treaties. The 2009 Human Rights Report by the United States Department of State noted that the government generally respected the rights of individuals, but voiced concerns regarding the social status of the indigenous population.</p>
	HTML;


$page->parent('world.html');
$page->body($div_introduction);



$page->related_tag("New Zealand");
$page->country_indices();
$page->body($div_wikipedia_New_Zealand);
$page->body($div_wikipedia_Human_rights_in_New_Zealand);
