<?php
$page = new PersonPage();
$page->h1("Jason Stanley");
$page->alpha_sort("Stanley, Jason");
$page->tags("Person", "Information: Discourse", "Political Discourse", "Fascism", "USA");
$page->keywords("Jason Stanley");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Jason Stanley is an American professor of philosophy.</p>

	<p>He is the author of the books:</p>

	<ul>
		<li>How Propaganda Works (2015)</li>
		<li>How Fascism Works: The Politics of Us and Them (2018)</li>
		<li>The Politics of Language (2023)</li>
		<li>${'Erasing History: How Fascists rewrite the Past to Control the Future'} (2024)</li>
	</ul>
	HTML;



$div_Jason_Stanley_homepage = new WebsiteContentSection();
$div_Jason_Stanley_homepage->setTitleText("Jason Stanley homepage ");
$div_Jason_Stanley_homepage->setTitleLink("https://campuspress.yale.edu/jasonstanley/");
$div_Jason_Stanley_homepage->content = <<<HTML
	<p>I am the Jacob Urowsky Professor of Philosophy at Yale University. I am also an honorary professor at the Kyiv School of Economics, where I use my salary to support the Come Back Alive Foundation.</p>
	HTML;



$div_wikipedia_Jason_Stanley = new WikipediaContentSection();
$div_wikipedia_Jason_Stanley->setTitleText("Jason Stanley");
$div_wikipedia_Jason_Stanley->setTitleLink("https://en.wikipedia.org/wiki/Jason_Stanley");
$div_wikipedia_Jason_Stanley->content = <<<HTML
	<p>Jason Stanley (born 1969) is an American philosopher who is the Jacob Urowsky Professor of Philosophy at Yale University. He is best known for his contributions to philosophy of language and epistemology, which often draw upon and influence other fields, including linguistics and cognitive science. He has written for popular audiences in The New York Times, The Guardian, The Washington Post, Rolling Stone, The New Republic, and many other publications in the United States and abroad. In his more recent work, Stanley has brought tools from philosophy of language and epistemology to bear on questions of political philosophy, for example in his 2015 book How Propaganda Works, and his 2023 book, The Politics of Language.</p>
	HTML;


$page->parent('list_of_people.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_Jason_Stanley_homepage);

$page->related_tag("Jason Stanley");
$page->body($div_wikipedia_Jason_Stanley);
