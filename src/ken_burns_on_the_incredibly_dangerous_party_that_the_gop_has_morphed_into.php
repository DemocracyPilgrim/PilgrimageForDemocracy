<?php
$page = new VideoPage();
$page->h1("Ken Burns on the ‘incredibly dangerous’ party that the GOP has ‘morphed’ into");
$page->tags("Video: Interview", "Ken Burns", "Republican Party", "Donald Trump", "Ali Velshi", "Liz Cheney", "Republican Party Evolution");
$page->stars(3);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>In an interview with ${'Ali Velshi'}, ${'Ken Burns'}, who had just been awarded the ${'Liberty Medal'},
	discusses the evolution of the ${'Republican Party'} from its inception until today.</p>
	HTML;



$div_youtube_Ken_Burns_on_the_incredibly_dangerous_party_that_the_GOP_has_morphed_into = new YoutubeContentSection();
$div_youtube_Ken_Burns_on_the_incredibly_dangerous_party_that_the_GOP_has_morphed_into->setTitleText("Ken Burns on the ‘incredibly dangerous’ party that the GOP has ‘morphed’ into");
$div_youtube_Ken_Burns_on_the_incredibly_dangerous_party_that_the_GOP_has_morphed_into->setTitleLink("https://www.youtube.com/watch?v=l96Xd4qjyCA");
$div_youtube_Ken_Burns_on_the_incredibly_dangerous_party_that_the_GOP_has_morphed_into->content = <<<HTML
	<p>
	</p>
	HTML;

$div_Excerpts = new ContentSection();
$div_Excerpts->content = <<<HTML
	<h3>Excerpts</h3>

	<blockquote><strong>Ken Burns</strong>:
	The Civil War was a sectional War but the peaceful transfer of power, the free and fair elections, the independence of the Judiciary didn't seem in doubt.</blockquote>

	<blockquote><strong>Ken Burns</strong>:
	We have very similar echoes, rhymes as Mark Twain would say, between these period.
	But of course each moment as Lincoln pointed out is unique to the present and as he pointed out in a speech to Congress:
	"the dogmas of The Quiet Past are inadequate to the stormy present as our case is new we must think anew we must act anew and then we can save our country".
	</blockquote>

	<p>Liz Cheney suggested that a new political party might be needed going forward. She said:</p>

	<blockquote><strong>Liz Cheney</strong>:
	There is certainly going to be a big shift I think in how our politics work.
	I don't know exactly what that will look like I don't think it will simply be the Republican party is going to put up a new late of candidates and Off to the Races.
	I think far too much has happened that's too damaging.
	</blockquote>

	<blockquote><strong>Ken Burns</strong>:
	There is so much entrenched and ingrained about the two parties
	that it's going to be difficult for the Republican party to figure out a way to escape the specific gravity of the darkness that now engulfs it.
	I mean this is a party born in the 1850s in a Schoolhouse in Ripon, Wisconsin.
	They had one thing they were going to do out of the ashes of the Whig Party and other disaffected people who are gravitating to something new:
	they wanted the Emancipation,  they wanted the end of slavery in the United States.
	Today's Republican party has now morphed into some incredibly dangerous, almost resembling the Know-Nothings of that period,
	that is racist and and certainly anti-immigrant.
	Abraham Lincoln is is turning over in his grave that this still has the label of the Republican party.
	</blockquote>

	<blockquote><strong>Ken Burns</strong>:
	The great legislation in the 20th century involve not only Democratic but Republican votes,
	whether it's voting rights or civil rights or Medicare or Medicaid, even going back to Social Security.
	The Republic Republican party was always from the beginning of the 20th century an engine of progressivism as much if not more than the Democratic party.
	States like Wisconsin were Laboratories of experimentation in Social ideas that would make government more responsive, level the playing field.
	Theodore Roosevelt is of course a perfect example of that kind of progressive in the White House.</blockquote>

	<blockquote><strong>Ken Burns</strong>:
	There's nothing more important in a democracy than educating the public about its history.</blockquote>

	<p>Ken Burns also speaks about the upcoming 250th anniversary of the United States, the Revolutionary War which started in 1775,
	and his related upcoming documentary project.</p>
	HTML;



$page->parent('list_of_videos.html');
$page->body($div_introduction);
$page->body($div_youtube_Ken_Burns_on_the_incredibly_dangerous_party_that_the_GOP_has_morphed_into);
$page->body($div_Excerpts);
