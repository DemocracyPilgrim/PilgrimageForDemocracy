<?php
$page = new Page();
$page->h1("6: Sixth level of democracy — We, the Breadwinners");
$page->stars(1);
$page->keywords('sixth level of democracy', 'sixth level');
$page->viewport_background('/free/sixth_level_of_democracy.png');

$page->snp("description", "We may have democracy, or we may have wealth concentrated in the hands of a few, but we cannot have both.");
$page->snp("image",       "/free/sixth_level_of_democracy.1200-630.png");

$page->preview( <<<HTML
	<p>The promise of democracy falters when wealth is concentrated in the hands of a few.
	We cannot have democracy with a starving population.
	Instead, we must ensure a dignified livelihood for all.
	This level explores fair wages, equitable profit sharing, taxes, and the crucial link between economic security and political freedom.</p>
	HTML );




$h2_Let_them_eat_croissants = new h2HeaderContent('"Let them eat croissants!"');


$div_the_tale_of_Silas_Thorne_and_Marie_from_the_favelas = new ContentSection();
$div_the_tale_of_Silas_Thorne_and_Marie_from_the_favelas->content = <<<HTML
	<p>Could you imagine ever living in a world where the following story would be realistic?</p>

	<blockquote>
		<p>The headline blared:<br>
			<strong style="margin-left: 5em;">"Tech Mogul Unveils $100 Million Yacht—'Croissant'</strong><br>
			<strong style="margin-left: 5em;">—Features Michelin-Starred Pastry Chef." </strong><br>
		The accompanying image showed a vessel the size of a small cruise ship, gleaming under a Caribbean sun.
		Its deck, visible in a stunning aerial shot, was a riot of polished teak and infinity pools,
		the air thick with the scent of freshly baked pastries from a dedicated onboard bakery.
		This was the "<strong>Croissant</strong>," the latest extravagant folly of billionaire entrepreneur, Silas Thorne.</p>

		<p>That same day, a few thousand miles away, in a sprawling favela clinging precariously to the side of a mountain,
		Maria clutched a single, day-old, rock-hard roll.
		It was all she could afford for her three children.
		The news report about the "Croissant," shown on a cracked, hand-me-down television, flickered intermittently,
		a cruel juxtaposition to the gnawing emptiness in her stomach.
		The children huddled around her, their eyes wide with a mixture of wonder and despair.</p>

		<p>Later that evening, a well-meaning but naive journalist, reporting from the favela for a global news network, interviewed Maria.
		He showed her a picture of the luxury yacht on his phone.
		"Can you believe this?" he asked, clearly trying to connect with her plight.
		Maria looked at the image, her expression unreadable.
		The journalist, eager to illustrate the disparity, commented,
		"<em>Imagine, Madame, they're even serving freshly baked croissants onboard...a luxury many here can only dream of.</em>"
		Then, Maria, with a weary sigh and a hint of quiet bitterness, uttered the words that resonated through the ages:
		"${'Let them eat croissant'}."
		Her tone wasn't one of childish naiveté like Marie Antoinette's, but one of profound, resigned sadness.
		The journalist, finally understanding the depth of the situation, felt the weight of her words.</p>
	<blockquote>
	HTML;


$h2_A_wealth_of_Inequalities = new h2HeaderContent("A wealth of Inequalities");



$div_democracy_and_concentration_of_wealth_louis_brandeis_quote = new ContentSection();
$div_democracy_and_concentration_of_wealth_louis_brandeis_quote->content = <<<HTML
	<blockquote>
		We must make our choice. We may have $democracy,
		or we may have wealth concentrated in the hands of a few, but we cannot have both.
		<br>
		— ${'Louis Brandeis'}, US Supreme court justice
	</blockquote>
	HTML;


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>It is a cruel irony: scarcity abounds amidst a wealth of inequality.
	Everywhere we look, we see a grotesque excess of wealth concentrated in the hands of a few,
	existing in jarring contrast to the widespread deprivation and hardship experienced by the vast majority.
	This isn't simply a matter of uneven distribution;
	it's a systemic failure, a perverse abundance that simultaneously creates and sustains an environment of scarcity for billions.
	This "wealth of inequality" isn't a natural phenomenon;
	it's a manufactured reality, the outcome of deliberate choices and deeply entrenched power structures
	that perpetuate injustice on a global scale.
	The following section will explore the multifaceted dimensions of this unsettling paradox.</p>

	<blockquote>
		In politics, we will have equality; and in social and economic life, we will have inequality.
		In politics we will be recognizing the principle of one man one vote and one vote one value.
		In our social and economic life, we shall, by reason of our social and economic structure, continue to deny the principle of one man one value.
		How long shall we continue to live this life of contradictions?
		How long shall we continue to deny equality in our social and economic life?
		I we continue to deny it for long, we will do so only by putting our political democracy in peril.
		<br>
		— ${'B.R. Amdebkar'}, Indian constitutionalist, 25 November 1949.
	</blockquote>

	<p>All about income, $taxes, livelihood, for a fair and just society, where everybody can make a living.</p>

	<p>The sixth level of democracy is about people's hierarchy of needs.
	It is very fine to talk about lofty ideals such as democracy and liberties.
	However, when people are mostly concerned about making ends meet, about putting food on the table,
	about feeding their children or even finding a safe place to sleep at night,
	they might be excused for not being so focused on the future of democracy.
	If democracy is to work at all, it has to work for all.
	It has to work for all segment of a country's society, and,
	as we shall see at the ${'seventh level'}, it has to work for all countries around the globe.</p>

	<p>For these reasons, we shall now be more focused on financial issues,
	and ways to ensure that workers get decent living wages commensurate to their time and labour.</p>

	<p>In the ${'fifth level of democracy'}, we talked about professionals within the context of $democratic institutions.
	Here we shall consider the economic imperative for people to make a living,
	in order to survive in this materialistic society.</p>

	<p>We shall speak about wealth disparity, $poverty, labour unions.
	Also, in this age of booming $AI, what are the implications for the labour market?
	Most importantly, we shall discuss what should be considered a fair wage.</p>

	<p>Democracy ought to be the best system of government
	that allows each citizen to get up the social, human and spiritual ladder,
	to ascend Maslow's hierarchy of needs, according to their own individual merits.</p>
	HTML;


$div_Understanding_populist_and_authoritarian_tendencies_within_democracies = new ContentSection();
$div_Understanding_populist_and_authoritarian_tendencies_within_democracies->content = <<<HTML
	<h3>Understanding populist and authoritarian tendencies within democracies</h3>

	<p>Within many western $democracies, there is a marked tendency towards populism,
	or even fascistic, authoritarian tendencies.</p>

	<p> We see that in many European countries.
	It is also most obvious within the $USA, where a growing segment of the electorate seem
	comfortable with the idea that $Trump openly declares wanting to be a dictator.</p>

	<p>Populist political figures are supported by working class people.
	In the US, a study concluded that working class people supporting Biden and those supporting Trump
	actually agreed on the most important issues for them (economy, immigration, etc.).
	It can be argued that a democratic candidate like Biden, officially endorsed by labour organizations,
	offers better and substantive solutions to their concerns,
	while Trump on the other hand is only utilizing deceptive propaganda in pursuit of his personal interest.</p>

	<p>So, what does drive a certain segment of the population to vote against their economic interest in support of a populist candidate?
	There is no simple answer to that question, and this website welcomes any academic research that offer some clues.
	We can start by observing that the feeling of losing control over their own lives
	pushes many working class people towards populist, fascistic candidates who promises to solve their problems
	with a strong authoritarian hand.</p>

	<p>Democracy has to work for everyone, and we ignore the needs and concerns of the poorest and weakest members of our society to our peril.</p>
	HTML;


$h2_We_the_Breadwinners = new h2HeaderContent("We, the Breadwinners");


$div_Hierarchy_of_needs = new ContentSection();
$div_Hierarchy_of_needs->content = <<<HTML
	<h3>Hierarchy of needs</h3>

	<p>Before we can engage meaningfully with the abstract ideals of democracy,
	${"Maslow's hierarchy of needs"} reminds us that fundamental necessities must be met.
	As breadwinners, our primary focus often lies in securing food, shelter, employment, and financial security for ourselves and our families.
	This fundamental drive for survival and well-being leaves us vulnerable to $populist messaging.
	Demagogues exploit this vulnerability, making alluring—though often unrealistic—promises to address these basic needs,
	thus diverting attention from broader democratic principles and processes in favor of immediate, tangible solutions.
	This prioritization of immediate needs over long-term democratic values creates fertile ground for authoritarian tendencies to take root.</p>
	HTML;



$list_all_related_topics = new ListOfPages();
$list_all_related_topics->add('living.html');
$print_list_all_related_topics = $list_all_related_topics->print();

$div_list_all_related_topics = new ContentSection();
$div_list_all_related_topics->content = <<<HTML
	$print_list_all_related_topics
	HTML;



$page->parent('democracy.html');
$page->previous('fifth_level_of_democracy.html');
$page->next('seventh_level_of_democracy.html');

$page->body($h2_Let_them_eat_croissants);
$page->body($div_the_tale_of_Silas_Thorne_and_Marie_from_the_favelas);


$page->body($h2_A_wealth_of_Inequalities);
$page->body($div_democracy_and_concentration_of_wealth_louis_brandeis_quote);

$page->body($div_introduction);

$page->body($div_Understanding_populist_and_authoritarian_tendencies_within_democracies);

$page->body($h2_We_the_Breadwinners);
$page->body($div_Hierarchy_of_needs);

$page->body($div_list_all_related_topics);
