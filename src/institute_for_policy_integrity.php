<?php
$page = new OrganisationPage();
$page->h1("Institute for Policy Integrity");
$page->viewport_background("");
$page->keywords("Institute for Policy Integrity");
$page->stars(0);
$page->tags("Organisation", "Environment", "Policy", "Institutions", "SCOTUS");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;



$div_Institute_for_Policy_Integrity = new WebsiteContentSection();
$div_Institute_for_Policy_Integrity->setTitleText("Institute for Policy Integrity ");
$div_Institute_for_Policy_Integrity->setTitleLink("https://policyintegrity.org/");
$div_Institute_for_Policy_Integrity->content = <<<HTML
	<p>We use economics and law to support smart policies for the environment, public health and consumers.</p>
	HTML;

$page->body($div_Institute_for_Policy_Integrity);


$div_wikipedia_Institute_for_Policy_Integrity = new WikipediaContentSection();
$div_wikipedia_Institute_for_Policy_Integrity->setTitleText("Institute for Policy Integrity");
$div_wikipedia_Institute_for_Policy_Integrity->setTitleLink("https://en.wikipedia.org/wiki/Institute_for_Policy_Integrity");
$div_wikipedia_Institute_for_Policy_Integrity->content = <<<HTML
	<p>The Institute for Policy Integrity (“Policy Integrity”) is a non-partisan think tank housed within the New York University School of Law. Policy Integrity is dedicated to improving government decisionmaking, and its primary area of focus is climate and energy policy. Policy Integrity produces original scholarly research and advocates for reform before courts, legislatures, and executive agencies at both the federal and state level.</p>
	HTML;


$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Institute for Policy Integrity");
$page->body($div_wikipedia_Institute_for_Policy_Integrity);
