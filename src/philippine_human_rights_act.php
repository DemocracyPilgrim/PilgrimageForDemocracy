<?php
$page = new Page();
$page->h1("Philippine Human Rights Act");
$page->tags("International", "Philippines", "US Congress", "Human Rights");
$page->keywords("Philippine Human Rights Act");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>The ""Philippine Human Rights Act" was a bill introduced in the $US Congress,
	proposing to impose limitations on providing assistance to the police or military of the Philippines
	at least until the human rights conditions improved.</p>
	HTML;



$div_H_R_1433_Philippine_Human_Rights_Act = new WebsiteContentSection();
$div_H_R_1433_Philippine_Human_Rights_Act->setTitleText("H.R.1433 - Philippine Human Rights Act ");
$div_H_R_1433_Philippine_Human_Rights_Act->setTitleLink("https://www.congress.gov/bill/118th-congress/house-bill/1433");
$div_H_R_1433_Philippine_Human_Rights_Act->content = <<<HTML
	<p>This bill imposes limitations on providing assistance to the police or military of the Philippines.
	No federal funds may be used to provide such assistance until the Philippines government has taken certain actions, including (1) investigating and successfully prosecuting members of its military and police forces who have violated human rights; (2) withdrawing the military from domestic policing activities; and (3) establishing that it effectively protects the rights of journalists, civil society activists, and certain other groups.
	The President shall also direct U.S. representatives at multilateral development banks to vote against providing loans to the police or military of the Philippines.</p>
	HTML;




$div_Philippine_Human_Rights_Act = new WebsiteContentSection();
$div_Philippine_Human_Rights_Act->setTitleText("Philippine Human Rights Act ");
$div_Philippine_Human_Rights_Act->setTitleLink("https://humanrightsph.org/");
$div_Philippine_Human_Rights_Act->content = <<<HTML
	<p>The Philippines has been deemed one of the deadliest countries in the world for land defenders, journalists and trade unionists.</p>
	HTML;



$div_wikipedia_Human_rights_in_the_Philippines = new WikipediaContentSection();
$div_wikipedia_Human_rights_in_the_Philippines->setTitleText("Human rights in the Philippines");
$div_wikipedia_Human_rights_in_the_Philippines->setTitleLink("https://en.wikipedia.org/wiki/Human_rights_in_the_Philippines");
$div_wikipedia_Human_rights_in_the_Philippines->content = <<<HTML
	HTML;


$page->parent('philippines.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Philippine Human Rights Act");

$page->body($div_H_R_1433_Philippine_Human_Rights_Act);
$page->body($div_Philippine_Human_Rights_Act);
$page->body($div_wikipedia_Human_rights_in_the_Philippines);
