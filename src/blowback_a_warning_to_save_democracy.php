<?php
$page = new Page();
$page->h1('Blowback: A Warning to Save Democracy from the next Trump');
$page->tags("USA", "Donald Trump", "Book");
$page->keywords("Blowback: A Warning to Save Democracy from the next Trump");
$page->stars(0);

//$page->snp('description', '');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>"Blowback: A Warning to Save Democracy from the next $Trump" is a book by ${'Miles Taylor'}.</p>
	HTML;

$div_wikipedia_Miles_Taylor_security_expert = new WikipediaContentSection();
$div_wikipedia_Miles_Taylor_security_expert->setTitleText('Miles Taylor security expert');
$div_wikipedia_Miles_Taylor_security_expert->setTitleLink('https://en.wikipedia.org/wiki/Miles_Taylor_(security_expert)');
$div_wikipedia_Miles_Taylor_security_expert->content = <<<HTML
	<p>${'Miles Taylor'} is an American government official who served in the administrations of George W. Bush and Donald Trump.</p>
	HTML;


$page->parent('list_of_books.html');
$page->template("stub");
$page->body($div_introduction);

$page->body('miles_taylor.html');

$page->body($div_wikipedia_Miles_Taylor_security_expert);
