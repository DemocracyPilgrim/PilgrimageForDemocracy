<?php
$page = new Page();
$page->h1("Truth and reality with Chinese characteristics");
$page->keywords("Truth and reality with Chinese characteristics");
$page->tags("China", "Propaganda", "Information: Government", "Political Discourse", "Disinformation");
$page->stars(1);

$page->snp("description", "The building blocks of the propaganda system enabling CCP information campaigns.");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p>Report revealing how the Chinese Communist Party (CCP)
	is using its propaganda system to brainwash people in other countries.</p>
	HTML );

// $r1 = $page->ref('', '');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>"Truth and reality with $Chinese characteristics" is a report by the ${'Australian Strategic Policy Institute'},
	that reveals how the ${'Chinese Communist Party'} (CCP) is using its $propaganda system to brainwash people in other countries.</p>

	<h3>Executive summary</h2>

	<div class="download-pdf">
		<a href="https://ad-aspi.s3.ap-southeast-2.amazonaws.com/2024-05/Truth%20and%20reality%20with%20Chinese%20characteristics.pdf">
			<img src="/copyrighted/pdf/Truth and reality with Chinese characteristics-01.png">
			<div class="download"><i class="outline file pdf white large icon"></i>Download</div>
		</a>
	</div>

	<p>The CCP seeks to maintain total control over the information environment within China, while simultaneously working to extend its influence abroad to reshape the global information ecosystem. That includes not only controlling media and communications platforms outside China, but also ensuring that Chinese technologies and companies become the foundational layer for the future of information and data exchange worldwide.</p>

	<p>This research report finds that the CCP seeks to harvest data from various sources, including commercial entities, to gain insights into target audiences for its information campaigns. We define an information campaign as a targeted, organised plan of related and integrated information operations, employing information-related capabilities (tools, techniques or activities) with other lines of operation to influence, disrupt, corrupt or manipulate information — including the individual or collective decision making based on that information — and deliberately disseminated on a large scale. The party also invests in emerging technologies such as artificial intelligence (AI) and immersive technologies that shape how people perceive reality and engage with information. The aim is to gain greater control, if not dominance, over the global information ecosystem.</p>

	<p>To understand the drivers, tools and outcomes of that process, this report and its accompanying website (ChinaInfoBlocks.aspi.org.au) examine the activities of the People’s Republic of China (PRC) in the information domain, particularly its investments in technology and research and development (R&D) companies that might serve as ‘building blocks’ for the party’s information campaigns.</p>
	HTML;




$div_Truth_and_reality_with_Chinese_characteristics = new WebsiteContentSection();
$div_Truth_and_reality_with_Chinese_characteristics->setTitleText("Truth and reality with Chinese characteristics ");
$div_Truth_and_reality_with_Chinese_characteristics->setTitleLink("https://www.aspi.org.au/report/truth-and-reality-chinese-characteristics");
$div_Truth_and_reality_with_Chinese_characteristics->content = <<<HTML
	<p><strong>Executive Summary</strong>:
	The Chinese Communist Party (CCP) is leveraging its propaganda system to build a toolkit to enable information campaigns.
	Its objective is to control communication and shape narratives and perceptions about China
	in order to present a specific version of truth and reality, both domestically and internationally.
	Ultimately, the CCP aims to strengthen its grip on power, legitimise its activities
	and bolster China’s cultural, technological, economic and military influence.</p>
	HTML;



$page->parent('list_of_research_and_reports.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_Truth_and_reality_with_Chinese_characteristics);

$page->body('australian_strategic_policy_institute.html');
