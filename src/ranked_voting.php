<?php
$page = new Page();
$page->h1("Ranked Voting (class of voting methods)");
$page->tags("Elections", "Voting Methods");
$page->keywords("Ranked Voting");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Ranked_voting = new WikipediaContentSection();
$div_wikipedia_Ranked_voting->setTitleText("Ranked voting");
$div_wikipedia_Ranked_voting->setTitleLink("https://en.wikipedia.org/wiki/Ranked_voting");
$div_wikipedia_Ranked_voting->content = <<<HTML
	<p>Ranked voting is any voting system that uses voters' rankings of candidates to choose a single winner or multiple winners.</p>
	HTML;


$page->parent('voting_methods.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Ranked Voting");
$page->body($div_wikipedia_Ranked_voting);
