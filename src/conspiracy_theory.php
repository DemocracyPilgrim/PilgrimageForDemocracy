<?php
$page = new Page();
$page->h1("Conspiracy theory");
$page->tags("Information: Discourse", "Information: Social Networks", "Disinformation");
$page->keywords("Conspiracy theory");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Conspiracy_theory = new WikipediaContentSection();
$div_wikipedia_Conspiracy_theory->setTitleText("Conspiracy theory");
$div_wikipedia_Conspiracy_theory->setTitleLink("https://en.wikipedia.org/wiki/Conspiracy_theory");
$div_wikipedia_Conspiracy_theory->content = <<<HTML
	<p>A conspiracy theory is an explanation for an event or situation that asserts the existence of a conspiracy by powerful and sinister groups,
	often political in motivation, when other explanations are more probable.
	The term generally has a negative connotation, implying that the appeal of a conspiracy theory is based in prejudice, emotional conviction, or insufficient evidence.
	A conspiracy theory is distinct from a conspiracy; it refers to a hypothesized conspiracy with specific characteristics,
	including but not limited to opposition to the mainstream consensus among those who are qualified to evaluate its accuracy, such as scientists or historians.</p>
	HTML;

$div_wikipedia_List_of_conspiracy_theories_promoted_by_Donald_Trump = new WikipediaContentSection();
$div_wikipedia_List_of_conspiracy_theories_promoted_by_Donald_Trump->setTitleText("List of conspiracy theories promoted by Donald Trump");
$div_wikipedia_List_of_conspiracy_theories_promoted_by_Donald_Trump->setTitleLink("https://en.wikipedia.org/wiki/List_of_conspiracy_theories_promoted_by_Donald_Trump");
$div_wikipedia_List_of_conspiracy_theories_promoted_by_Donald_Trump->content = <<<HTML
	<p>This article contains a list of conspiracy theories, many of them misleading, disproven, or false,
	which were either created or promoted by Donald Trump, the president of the United States from 2017 to 2021.</p>
	HTML;


$page->parent('information.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_wikipedia_Conspiracy_theory);
$page->body($div_wikipedia_List_of_conspiracy_theories_promoted_by_Donald_Trump);
