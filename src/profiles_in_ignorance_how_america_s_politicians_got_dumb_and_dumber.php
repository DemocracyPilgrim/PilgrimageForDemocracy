<?php
$page = new BookPage();
$page->h1("Profiles in Ignorance – How America's Politicians Got Dumb and Dumber");
$page->viewport_background("");
$page->keywords("Profiles in Ignorance – How America's Politicians Got Dumb and Dumber");
$page->stars(0);
$page->tags("Book", "Ignorance", "Education", "Donald Trump", "Andy Borowitz", "USA");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>"Profiles in Ignorance – How America's Politicians Got Dumb and Dumber" is a book by ${'Andy Borowitz'}.</p>

	<blockquote>
	By elevating candidates who can entertain over those who can think, mass media have made the election of dunces more likely.
	</blockquote>

	<blockquote>
	What happens when you combine ignorance with performing talent? A president who tells the country to inject bleach.
	</blockquote>
	HTML;



$div_Profiles_in_Ignorance = new WebsiteContentSection();
$div_Profiles_in_Ignorance->setTitleText("Publisher: Profiles in Ignorance – How America's Politicians Got Dumb and Dumber");
$div_Profiles_in_Ignorance->setTitleLink("https://www.simonandschuster.com/books/Profiles-in-Ignorance/Andy-Borowitz/9781668003893");
$div_Profiles_in_Ignorance->content = <<<HTML
	<p>Borowitz argues that over the past fifty years, American politicians have grown increasingly allergic to knowledge, and mass media have encouraged the election of ignoramuses by elevating candidates who are better at performing than thinking. Starting with Ronald Reagan’s first campaign for governor of California in 1966 and culminating with the election of Donald J. Trump to the White House, Borowitz shows how, during the age of twenty-four-hour news and social media, the US has elected politicians to positions of great power whose lack of the most basic information is terrifying. In addition to Reagan, Quayle, Bush, Palin, and Trump, Borowitz covers a host of congresspersons, senators, and governors who have helped lower the bar over the past five decades.</p>

	<p>Profiles in Ignorance aims to make us both laugh and cry: laugh at the idiotic antics of these public figures, and cry at the cataclysms these icons of ignorance have caused. But most importantly, the book delivers a call to action and a cause for optimism: History doesn’t move in a straight line, and we can change course if we act now.</p>
	HTML;




$page->parent('list_of_books.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Profiles in Ignorance – How America's Politicians Got Dumb and Dumber");
$page->body($div_Profiles_in_Ignorance);
