<?php
$page = new Page();
$page->h1("Fake accounts on social networks");
$page->tags("Information: Social Networks", "Disinformation", "Donald Trump");
$page->keywords("fake accounts on social networks");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>The specific information shared below are based on an investigative report by the ${'Centre for Information Resilience'}.</p>
	HTML;





$div_Unmasking_the_fake_MAGA_accounts_stolen_photos_and_digital_lies = new WebsiteContentSection();
$div_Unmasking_the_fake_MAGA_accounts_stolen_photos_and_digital_lies->setTitleText("Unmasking the fake MAGA accounts: stolen photos and digital lies");
$div_Unmasking_the_fake_MAGA_accounts_stolen_photos_and_digital_lies->setTitleLink("https://www.info-res.org/post/unmasking-the-fake-maga-accounts-stolen-photos-and-digital-lies");
$div_Unmasking_the_fake_MAGA_accounts_stolen_photos_and_digital_lies->content = <<<HTML
	<p>A network of fake accounts are posing as young American women and posting pro-Trump content online,
	but they’re hiding behind – and in some cases manipulating – the images of European fashion influencers.</p>
	HTML;



$div_youtube_What_the_f_Woman_reacts_to_pro_Trump_accounts_stealing_her_identity = new YoutubeContentSection();
$div_youtube_What_the_f_Woman_reacts_to_pro_Trump_accounts_stealing_her_identity->setTitleText("&#39;What the f***&#39;: Woman reacts to pro-Trump accounts stealing her identity");
$div_youtube_What_the_f_Woman_reacts_to_pro_Trump_accounts_stealing_her_identity->setTitleLink("https://www.youtube.com/watch?v=m1QkXtawyao");
$div_youtube_What_the_f_Woman_reacts_to_pro_Trump_accounts_stealing_her_identity->content = <<<HTML
	<p>European influencers have become the face of fake MAGA accounts against their will.
	Katie Polglase investigates the accounts on X identified by CNN and Centre for Information Resilience.</p>
	HTML;




$page->parent('information.html');
$page->template("stub");
$page->body($div_introduction);

$page->body($div_Unmasking_the_fake_MAGA_accounts_stolen_photos_and_digital_lies);
$page->body($div_youtube_What_the_f_Woman_reacts_to_pro_Trump_accounts_stealing_her_identity);
