<?php
$page = new Page();
$page->h1("Wedge issues");
$page->keywords("wedge issues");
$page->tags("Information: Discourse", "Political Discourse");
$page->stars(0);

//$page->snp("description", "");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Wedge_issue = new WikipediaContentSection();
$div_wikipedia_Wedge_issue->setTitleText("Wedge issue");
$div_wikipedia_Wedge_issue->setTitleLink("https://en.wikipedia.org/wiki/Wedge_issue");
$div_wikipedia_Wedge_issue->content = <<<HTML
	<p>A wedge issue is a political or social issue which is controversial or divisive within a usually-united group.
	Wedge issues can be advertised or publicly aired in an attempt to strengthen the unity of a population,
	with the goal of enticing polarized individuals to give support to an opponent or to withdraw their support entirely out of disillusionment.
	The use of wedge issues gives rise to wedge politics.</p>
	HTML;


$page->parent('political_discourse.html');
$page->template("stub");
$page->body($div_introduction);


$page->body('duverger_syndrome_duality.html');
$page->body($div_wikipedia_Wedge_issue);
