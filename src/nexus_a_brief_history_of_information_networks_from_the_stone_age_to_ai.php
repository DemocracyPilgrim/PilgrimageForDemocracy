<?php
$page = new Page();
$page->h1("Nexus: A Brief History of Information Networks from the Stone Age to AI");
$page->tags("Book", "Information", "Artificial Intelligence");
$page->keywords("Nexus: A Brief History of Information Networks from the Stone Age to AI");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>"Nexus: A Brief History of Information Networks from the Stone Age to AI" is a book by ${'Yuval Noah Harari'}.</p>
	HTML;



$div_Nexus_A_Brief_History_of_Information_Networks_from_the_Stone_Age_to_AI = new WebsiteContentSection();
$div_Nexus_A_Brief_History_of_Information_Networks_from_the_Stone_Age_to_AI->setTitleText("Nexus: A Brief History of Information Networks from the Stone Age to AI ");
$div_Nexus_A_Brief_History_of_Information_Networks_from_the_Stone_Age_to_AI->setTitleLink("https://www.ynharari.com/book/nexus/");
$div_Nexus_A_Brief_History_of_Information_Networks_from_the_Stone_Age_to_AI->content = <<<HTML
	<p>This non-fiction book looks through the long lens of human history to consider how the flow of information has made, and unmade, our world.
	We are living through the most profound information revolution in human history.
	To understand it, we need to understand what has come before.
	We have named our species Homo sapiens, the wise human – but if humans are so wise, why are we doing so many self-destructive things?
	In particular, why are we on the verge of committing ecological and technological suicide?
	Humanity gains power by building large networks of cooperation, but the easiest way to build and maintain these networks is
	by spreading fictions, fantasies, and mass delusions.
	In the 21st century, AI may form the nexus for a new network of delusions that could prevent future generations
	from even attempting to expose its lies and fictions.
	However, history is not deterministic, and neither is technology:
	by making informed choices, we can still prevent the worst outcomes.
	Because if we can’t change the future, then why waste time discussing it?</p>
	HTML;



$div_wikipedia_Yuval_Noah_Harari = new WikipediaContentSection();
$div_wikipedia_Yuval_Noah_Harari->setTitleText("Yuval Noah Harari");
$div_wikipedia_Yuval_Noah_Harari->setTitleLink("https://en.wikipedia.org/wiki/Yuval_Noah_Harari");
$div_wikipedia_Yuval_Noah_Harari->content = <<<HTML
	<p>Yuval Noah Harari is an Israeli medievalist, military historian, public intellectual, and writer.
	He currently serves as professor in the Department of History at the Hebrew University of Jerusalem.
	He is the author of the popular science bestsellers Sapiens:
	A Brief History of Humankind (2011),
	Homo Deus: A Brief History of Tomorrow (2016),
	and 21 Lessons for the 21st Century (2018).
	His writings examine free will, consciousness, intelligence, happiness, and suffering.</p>
	HTML;


$page->parent('list_of_books.html');
$page->template("stub");
$page->body($div_introduction);

$page->body($div_Nexus_A_Brief_History_of_Information_Networks_from_the_Stone_Age_to_AI);

$page->body($div_wikipedia_Yuval_Noah_Harari);
