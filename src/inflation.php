<?php
$page = new Page();
$page->h1("Inflation");
$page->tags("Living: Economy", "Economy");
$page->keywords("Inflation");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Inflation = new WikipediaContentSection();
$div_wikipedia_Inflation->setTitleText("Inflation");
$div_wikipedia_Inflation->setTitleLink("https://en.wikipedia.org/wiki/Inflation");
$div_wikipedia_Inflation->content = <<<HTML
	<p>In economics, inflation is a general increase in the prices of goods and services in an economy. This is usually measured using the consumer price index (CPI). When the general price level rises, each unit of currency buys fewer goods and services; consequently, inflation corresponds to a reduction in the purchasing power of money. The opposite of CPI inflation is deflation, a decrease in the general price level of goods and services. The common measure of inflation is the inflation rate, the annualized percentage change in a general price index. As prices faced by households do not all increase at the same rate, the consumer price index (CPI) is often used for this purpose.</p>
	HTML;


$page->parent('economy.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Inflation");
$page->body($div_wikipedia_Inflation);
