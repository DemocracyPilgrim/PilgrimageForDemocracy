<?php
$page = new Page();
$page->h1("List of taxes");
$page->tags("Taxes");
$page->keywords("list of taxes");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_List_of_taxes = new WikipediaContentSection();
$div_wikipedia_List_of_taxes->setTitleText("List of taxes");
$div_wikipedia_List_of_taxes->setTitleLink("https://en.wikipedia.org/wiki/List_of_taxes");
$div_wikipedia_List_of_taxes->content = <<<HTML
	<p>Taxes generally fall into the following broad categories:
	Income tax, Payroll tax, Property tax, Consumption tax, Tariff (taxes on international trade), Capitation, a fixed tax charged per person, Fees and tolls,
	Effective taxes, government policies that are not explicitly taxes, but result in income to the government through losses to the public.</p>
	HTML;


$page->parent('taxes.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Tax");
$page->body($div_wikipedia_List_of_taxes);
