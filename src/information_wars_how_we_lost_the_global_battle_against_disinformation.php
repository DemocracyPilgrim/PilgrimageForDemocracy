<?php
$page = new Page();
$page->h1("Information Wars: How We Lost the Global Battle Against Disinformation");
$page->tags("Book", "USA", "Richard Stengel", "Propaganda", "Disinformation", "Russia");
$page->keywords("Information Wars: How We Lost the Global Battle Against Disinformation");
$page->stars(1);

$page->snp("description", "A book by Richard Stengel, Obama's Under Secretary of State.");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref('', '');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>"Information Wars: How We Lost the Global Battle Against Disinformation and What We Can Do About It"
	is a 2019 book by ${'Richard Stengel'}, the Under Secretary of State during Obama's presidency.
	Stengel recounts his time in the State Department countering $Russian disinformation and ISIS $propaganda.</p>
	HTML;




$div_Information_Wars = new WebsiteContentSection();
$div_Information_Wars->setTitleText("Information Wars: How We Lost the Global Battle Against Disinformation and What We Can Do About It");
$div_Information_Wars->setTitleLink("https://groveatlantic.com/book/information-wars/");
$div_Information_Wars->content = <<<HTML
	<p>Disinformation is as old as humanity.
	When Satan told Eve nothing would happen if she bit the apple, that was disinformation.
	But the rise of social media has made disinformation even more pervasive and pernicious in our current era.
	In a disturbing turn of events, governments are increasingly using disinformation to create their own false narratives,
	and democracies are proving not to be very good at fighting it.</p>

	<p>During the final three years of the Obama administration, Richard Stengel, the former editor of Time and an Under Secretary of State,
	was on the front lines of this new global information war.
	At the time, he was the single person in government tasked with unpacking, disproving, and combating both ISIS’s messaging and Russian disinformation.
	Then, in 2016, as the presidential election unfolded,
	Stengel watched as Donald Trump used disinformation himself, weaponizing the grievances of Americans who felt left out by modernism.
	In fact, Stengel quickly came to see how all three players had used the same playbook:
	ISIS sought to make Islam great again; Putin tried to make Russia great again; and we all know about Trump.</p>

	<p>In a narrative that is by turns dramatic and eye-opening, Information Wars walks readers through of this often frustrating battle.
	Stengel moves through Russia and Ukraine, Saudi Arabia and Iraq, and introduces characters from Putin to Hillary Clinton, John Kerry and Mohamed bin Salman
	to show how disinformation is impacting our global society.
	He illustrates how ISIS terrorized the world using social media,
	and how the Russians launched a tsunami of disinformation around the annexation of Crimea – a scheme that became the model
	for their interference with the 2016 presidential election.
	An urgent book for our times, Information Wars stresses that we must find a way to combat this ever growing threat to democracy.</p>
	HTML;



$page->parent('list_of_books.html');
$page->parent('disinformation.html');
$page->template("stub");
$page->body($div_introduction);
$page->body($div_Information_Wars);
