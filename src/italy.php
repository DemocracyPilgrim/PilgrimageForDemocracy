<?php
$page = new CountryPage("Italy");
$page->h1("Italy");
$page->tags("Country");
$page->keywords("Italy");
$page->stars(0);
$page->viewport_background("");

$page->snp("description", "59 million inhabitants");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Italy = new WikipediaContentSection();
$div_wikipedia_Italy->setTitleText("Italy");
$div_wikipedia_Italy->setTitleLink("https://en.wikipedia.org/wiki/Italy");
$div_wikipedia_Italy->content = <<<HTML
	<p>
	</p>
	HTML;


$page->parent('world.html');
$page->template("stub");
$page->body($div_introduction);




$page->related_tag("Italy");
$page->country_indices();

$page->body($div_wikipedia_Italy);
