<?php
$page = new Page();
$page->h1("McDonald v. City of Chicago (2010)");
$page->tags("Institutions: Judiciary", "SCOTUS Landmark Decision", "USA", "Gun Legislation");
$page->keywords("McDonald v. City of Chicago");
$page->stars(1);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>"McDonald v. City of Chicago" is a 2010 landmark decision by the ${'Supreme Court of the United States'}.</p>

	<p>This ruling together with ${'District of Columbia v. Heller'}, concerning the Second Amendment's guarantee of the right to bear arms,
	have made it significantly more difficult for states and local governments to regulate firearms.
	This has contributed to ongoing debates about ${'gun control'} and its impact on public safety.</p>
	HTML;

$div_wikipedia_McDonald_v_City_of_Chicago = new WikipediaContentSection();
$div_wikipedia_McDonald_v_City_of_Chicago->setTitleText("McDonald v City of Chicago");
$div_wikipedia_McDonald_v_City_of_Chicago->setTitleLink("https://en.wikipedia.org/wiki/McDonald_v._City_of_Chicago");
$div_wikipedia_McDonald_v_City_of_Chicago->content = <<<HTML
	<p>McDonald v. City of Chicago, 561 U.S. 742 (2010), was a landmark decision of the Supreme Court of the United States
	that found that the right of an individual to "keep and bear arms", as protected under the Second Amendment,
	is incorporated by the Fourteenth Amendment and is thereby enforceable against the states.
	The decision cleared up the uncertainty left in the wake of District of Columbia v. Heller (2008)
	as to the scope of gun rights in regard to the states.</p>
	HTML;


$page->parent('supreme_court_of_the_united_states.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_wikipedia_McDonald_v_City_of_Chicago);
