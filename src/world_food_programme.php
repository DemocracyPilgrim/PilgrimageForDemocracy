<?php
$page = new Page();
$page->h1("World Food Program");
$page->keywords("World Food Program", "WFP");
$page->tags("Organisation", "Humanity");
$page->stars(0);

//$page->snp("description", "");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;



$div_World_Food_Programme_website = new WebsiteContentSection();
$div_World_Food_Programme_website->setTitleText("World Food Programme website");
$div_World_Food_Programme_website->setTitleLink("https://www.wfp.org/");
$div_World_Food_Programme_website->content = <<<HTML
	<p>We are the largest humanitarian organization saving and changing lives worldwide.</p>

	<p>We bring life-saving relief in emergencies and use food assistance to build a pathway to peace, stability and prosperity
	for people recovering from conflict, disasters and the impacts of climate change.</p>
	HTML;



$div_wikipedia_World_Food_Programme = new WikipediaContentSection();
$div_wikipedia_World_Food_Programme->setTitleText("World Food Programme");
$div_wikipedia_World_Food_Programme->setTitleLink("https://en.wikipedia.org/wiki/World_Food_Programme");
$div_wikipedia_World_Food_Programme->content = <<<HTML
	<p>The World Food Programme (WFP) is an international organization within the United Nations
	that provides food assistance worldwide.
	It is the world's largest humanitarian organization and the leading provider of school meals.
	As of 2021, it supported over 128 million people across more than 120 countries and territories.</p>
	HTML;


$page->parent('united_nations.html');
$page->template("stub");
$page->body($div_introduction);
$page->body('hunger.html');


$page->body($div_World_Food_Programme_website);
$page->body($div_wikipedia_World_Food_Programme);
