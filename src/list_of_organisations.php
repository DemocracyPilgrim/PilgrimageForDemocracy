<?php
$page = new Page();
$page->h1('List of organisations');
$page->stars(1);
$page->keywords('organisations', 'organisation', 'Organisation');
$page->viewport_background('/free/list_of_organisations.png');

$page->preview( <<<HTML
	<p>Organisation related to peace, democracy or social justice.</p>
	HTML );


$page->snp('description', "Working for peace, democracy and social justice.");
$page->snp('image', "/free/list_of_organisations.1200-630.png");



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Below is a list of organisations that are covered in this website so far.</p>
	HTML;

$list_Organisation = ListOfPeoplePages::WithTags("Organisation");
$print_list_Organisation = $list_Organisation->print();

$div_list_Organisation = new ContentSection();
$div_list_Organisation->content = <<<HTML
	<h3>list</h3>

	$print_list_Organisation
	HTML;




$page->parent('lists.html');

$page->body($div_introduction);
$page->body($div_stars);
$page->body($div_list_Organisation);
$page->body('institutions.html');
