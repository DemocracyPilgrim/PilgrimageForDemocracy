<?php
$page = new Page();
$page->h1("Miranda v. Arizona (1966)");
$page->tags("Institutions: Judiciary", "SCOTUS Landmark Decision", "USA", "Justice");
$page->keywords("Miranda v. Arizona");
$page->stars(1);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>"Miranda v. Arizona" is a 1966 landmark decision by the ${'Supreme Court of the United States'}.</p>

	<p>This decision established the Miranda warnings, requiring law enforcement officers to inform suspects of their rights to remain silent
	and to have legal counsel present during questioning.</p>

	<p>This ruling strengthened the Fifth Amendment's right against self-incrimination,
	ensuring that individuals are not coerced into making incriminating statements without proper legal protection.</p>
	HTML;

$div_wikipedia_Miranda_v_Arizona = new WikipediaContentSection();
$div_wikipedia_Miranda_v_Arizona->setTitleText("Miranda v Arizona");
$div_wikipedia_Miranda_v_Arizona->setTitleLink("https://en.wikipedia.org/wiki/Miranda_v._Arizona");
$div_wikipedia_Miranda_v_Arizona->content = <<<HTML
	<p>Miranda v. Arizona, 384 U.S. 436 (1966), was a landmark decision of the U.S. Supreme Court
	in which the Court ruled that law enforcement in the United States must warn a person of their constitutional rights before interrogating them,
	or else the person's statements cannot be used as evidence at their trial.
	Specifically, the Court held that under the Fifth Amendment to the U.S. Constitution,
	the government cannot use a person's statements made in response to an interrogation while in police custody as evidence at the person's criminal trial
	unless they can show that the person was informed of the right to consult with a lawyer before and during questioning,
	and of the right against self-incrimination before police questioning,
	and that the defendant not only understood these rights but also voluntarily waived them before answering questions.</p>
	HTML;


$page->parent('supreme_court_of_the_united_states.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_wikipedia_Miranda_v_Arizona);
