<?php
$page = new Page();
$page->h1('Debt');
$page->keywords('debt', "Debt");
$page->stars(0);
$page->tags("Living", "Banking", "Economy", "Institutions", "Poverty");
$page->viewport_background('');


//$page->snp('description', '');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Government_debt = new WikipediaContentSection();
$div_wikipedia_Government_debt->setTitleText('Government debt');
$div_wikipedia_Government_debt->setTitleLink('https://en.wikipedia.org/wiki/Government_debt');
$div_wikipedia_Government_debt->content = <<<HTML
	<p>In 2020, the value of government debt worldwide was $87.4 US trillion, or 99% measured as a share of gross domestic product (GDP).
	Government debt accounted for almost 40% of all debt (which includes corporate and household debt), the highest share since the 1960s.
	The rise in government debt since 2007 is largely attributable to the global financial crisis of 2007–2008, and the COVID-19 pandemic.
	The ability of government to issue debt has been central to state formation and to state building.
	Public debt has been linked to the rise of democracy, private financial markets, and modern economic growth.</p>
	HTML;

$div_wikipedia_Odious_debt = new WikipediaContentSection();
$div_wikipedia_Odious_debt->setTitleText('Odious debt');
$div_wikipedia_Odious_debt->setTitleLink('https://en.wikipedia.org/wiki/Odious_debt');
$div_wikipedia_Odious_debt->content = <<<HTML
	<p>In international law, odious debt, also known as illegitimate debt, is a legal theory that says that
	the national debt incurred by a despotic regime should not be enforceable.
	Such debts are, thus, considered by this doctrine to be personal debts of the government that incurred them and not debts of the state.
	In some respects, the concept is analogous to the invalidity of contracts signed under coercion.
	Whether or not it is possible to discharge debts in this manner is a matter of dispute.</p>
	HTML;

$div_wikipedia_Heavily_indebted_poor_countries = new WikipediaContentSection();
$div_wikipedia_Heavily_indebted_poor_countries->setTitleText('Heavily indebted poor countries');
$div_wikipedia_Heavily_indebted_poor_countries->setTitleLink('https://en.wikipedia.org/wiki/Heavily_indebted_poor_countries');
$div_wikipedia_Heavily_indebted_poor_countries->content = <<<HTML
	<p>The heavily indebted poor countries (HIPC) are a group of 39 developing countries with high levels of poverty and debt overhang
	which are eligible for special assistance from the International Monetary Fund (IMF) and the World Bank.</p>

	HTML;


$page->parent('economy.html');
$page->template("stub");
$page->body($div_introduction);


$page->related_tag("Debt");
$page->body($div_wikipedia_Government_debt);
$page->body($div_wikipedia_Odious_debt);
$page->body($div_wikipedia_Heavily_indebted_poor_countries);
