<?php
$page = new BookPage();
$page->h1("Small Is Beautiful");
$page->keywords("Small Is Beautiful");
$page->stars(0);
$page->tags("Book", "Fair Share");

//$page->snp("description", "");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Small_Is_Beautiful = new WikipediaContentSection();
$div_wikipedia_Small_Is_Beautiful->setTitleText("Small Is Beautiful");
$div_wikipedia_Small_Is_Beautiful->setTitleLink("https://en.wikipedia.org/wiki/Small_Is_Beautiful");
$div_wikipedia_Small_Is_Beautiful->content = <<<HTML
	<p>Small Is Beautiful: A Study of Economics As If People Mattered is a collection of essays
	published in 1973 by German-born British economist E. F. Schumacher.
	The title "Small Is Beautiful" came from a principle espoused by Schumacher's teacher Leopold Kohr (1909–1994)
	advancing small, appropriate technologies, policies, and polities
	as a superior alternative to the mainstream ethos of "bigger is better".</p>

	<p>Overlapping environmental, social, and economic forces such as the 1973 energy crisis
	and popularisation of the concept of globalisation helped bring Schumacher's Small Is Beautiful critiques of mainstream economics
	to a wider audience during the 1970s.
	In 1995 The Times Literary Supplement ranked Small Is Beautiful among the 100 most influential books published since World War II.
	A further edition with commentaries was published in 1999.</p>
	HTML;


$page->parent('small_is_beautiful.html');
$page->template("stub");
$page->body($div_introduction);

$page->body('ernst_friedrich_schumacher.html');
$page->body('schumacher_center_for_a_new_economics.html');
$page->body('appropriate_technology.html');
$page->body('buddhist_economics.html');

$page->body($div_wikipedia_Small_Is_Beautiful);
