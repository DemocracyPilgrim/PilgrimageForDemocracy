<?php
$page = new OrganisationPage();
$page->h1("American Immigration Council");
$page->tags("Organisation", "USA", "Immigration");
$page->keywords("American Immigration Council");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;




$div_American_Immigration_Council = new WebsiteContentSection();
$div_American_Immigration_Council->setTitleText("American Immigration Council ");
$div_American_Immigration_Council->setTitleLink("https://www.americanimmigrationcouncil.org/");
$div_American_Immigration_Council->content = <<<HTML
	<p>The Council strives to strengthen the United States by shaping immigration policies and practices through innovative programs, cutting-edge research, and strategic legal and advocacy efforts grounded in evidence, compassion, justice and fairness. We collaborate with diverse stakeholders, including policymakers, grassroots organizations, and immigrant communities, to advance results-driven solutions to the challenges facing immigrants and communities throughout the United States.</p>
	HTML;


$div_Immigration_Impact = new WebsiteContentSection();
$div_Immigration_Impact->setTitleText("Immigration Impact ");
$div_Immigration_Impact->setTitleLink("https://immigrationimpact.com/");
$div_Immigration_Impact->content = <<<HTML
	<p>
	</p>
	HTML;




$div_wikipedia_American_Immigration_Council = new WikipediaContentSection();
$div_wikipedia_American_Immigration_Council->setTitleText("American Immigration Council");
$div_wikipedia_American_Immigration_Council->setTitleLink("https://en.wikipedia.org/wiki/American_Immigration_Council");
$div_wikipedia_American_Immigration_Council->content = <<<HTML
	<p>The American Immigration Council is a Washington, D.C.–based 501(c)(3) nonprofit organization and advocacy group. It was established in 1987, originally as the American Immigration Law Foundation, by the American Immigration Lawyers Association.</p>

	<p>The organization conducts research and policy analysis, provides legal resources for attorneys, produces resources for public education, engages in grassroots and direct advocacy, and files litigation related to immigration issues. It operates at the national level as well as through state-based and local programs.</p>
	HTML;


$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("American Immigration Council");
$page->body($div_American_Immigration_Council);
$page->body($div_Immigration_Impact);
$page->body($div_wikipedia_American_Immigration_Council);
