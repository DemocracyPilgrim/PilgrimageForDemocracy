<?php
$page = new Page();
$page->h1('Organization of American States');
$page->tags("International Organisation", "Organisation");
$page->keywords('Organization of American States', 'OAS');
$page->stars(0);

//$page->snp('description', '');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$list = new ListOfPages();
$list->add('argentina.html');
$list->add('bolivia.html');
$list->add('brazil.html');
$list->add('chile.html');
$list->add('colombia.html');
$list->add('costa_rica.html');
$list->add('cuba.html');
$list->add('dominican_republic.html');
$list->add('ecuador.html');
$list->add('el_salvador.html');
$list->add('guatemala.html');
$list->add('haiti.html');
$list->add('honduras.html');
$list->add('mexico.html');
$list->add('nicaragua.html');
$list->add('panama.html');
$print_list = $list->print();

$div_list_countries = new ContentSection();
$div_list_countries->content = <<<HTML
	<h3>Member states</h3>

	$print_list
	HTML;



$div_wikipedia_Organization_of_American_States = new WikipediaContentSection();
$div_wikipedia_Organization_of_American_States->setTitleText('Organization of American States');
$div_wikipedia_Organization_of_American_States->setTitleLink('https://en.wikipedia.org/wiki/Organization_of_American_States');
$div_wikipedia_Organization_of_American_States->content = <<<HTML
	<p>The Organization of American States is an international organization founded on 30 April 1948
	to promote cooperation among its member states within the Americas.
	Headquartered in Washington, D.C., United States, the OAS is a "multilateral regional body focused on human rights,
	electoral oversight, social and economic development, and security in the Western Hemisphere",
	according to the Council on Foreign Relations.
	As of June 2023, 35 states in the Americas are OAS members.
	Since the 1990s, the organization has focused on election monitoring.</p>
	HTML;


$page->parent('world.html');
$page->template("stub");
$page->body($div_introduction);

$page->body($div_list_countries);


$page->body($div_wikipedia_Organization_of_American_States);
