<?php
$page = new Page();
$page->h1("The social cost of Donald Trump political carreer");
$page->tags("Society", "Donald Trump: Research", "Donald Trump", "USA", "WIP");
$page->keywords("The social cost of Donald Trump political carreer");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;



$h2_Democratic_decline = new h2HeaderContent("Democratic decline");


$div_Large_scale_decline_of_democratic_institutions_and_political_discourse = new ContentSection();
$div_Large_scale_decline_of_democratic_institutions_and_political_discourse->content = <<<HTML
	<h3>Large scale decline of democratic institutions and political discourse</h3>

	<p>
	</p>
	HTML;



$h2_Russian_invasion_of_Ukraine = new h2HeaderContent("Russian invasion of Ukraine");


$div_Failing_to_support_Ukraine_against_the_Russian_invasion = new ContentSection();
$div_Failing_to_support_Ukraine_against_the_Russian_invasion->content = <<<HTML
	<h3>Failing to support Ukraine against the Russian invasion</h3>

	<p>Ukraine's city of Avdiivka was completely lost to Russian invaders directly because of Trump
	and his forcing the Republican party to vote against a critical aid package for Ukraine.
	The months-long delay in passing the aid package caused the loss of the city.</p>
	HTML;



$h2_COVID_Pandemic = new h2HeaderContent("COVID Pandemic");


$div_Handling_of_the_Pandemic_and_COVID_deaths = new ContentSection();
$div_Handling_of_the_Pandemic_and_COVID_deaths->content = <<<HTML
	<h3>Handling of the Pandemic and COVID deaths</h3>

	<p>The USA lost about 1 million lives to COVID.
	It may be that up to half of those deaths could have been prevented if not for Trump's politicization of the pandemic and the response to it.</p>
	HTML;



$div_wikipedia_COVID_19_pandemic_death_rates_by_country = new WikipediaContentSection();
$div_wikipedia_COVID_19_pandemic_death_rates_by_country->setTitleText("COVID-19 pandemic death rates by country");
$div_wikipedia_COVID_19_pandemic_death_rates_by_country->setTitleLink("https://en.wikipedia.org/wiki/COVID-19_pandemic_death_rates_by_country");
$div_wikipedia_COVID_19_pandemic_death_rates_by_country->content = <<<HTML
	<p>
	</p>
	HTML;


$div_wikipedia_COVID_19_vaccine_hesitancy_in_the_United_States = new WikipediaContentSection();
$div_wikipedia_COVID_19_vaccine_hesitancy_in_the_United_States->setTitleText("COVID-19 vaccine hesitancy in the United States");
$div_wikipedia_COVID_19_vaccine_hesitancy_in_the_United_States->setTitleLink("https://en.wikipedia.org/wiki/COVID-19_vaccine_hesitancy_in_the_United_States");
$div_wikipedia_COVID_19_vaccine_hesitancy_in_the_United_States->content = <<<HTML
	<p>
	</p>
	HTML;


$div_wikipedia_Communication_of_the_Trump_administration_during_the_COVID_19_pandemic = new WikipediaContentSection();
$div_wikipedia_Communication_of_the_Trump_administration_during_the_COVID_19_pandemic->setTitleText("Communication of the Trump administration during the COVID-19 pandemic");
$div_wikipedia_Communication_of_the_Trump_administration_during_the_COVID_19_pandemic->setTitleLink("https://en.wikipedia.org/wiki/Communication_of_the_Trump_administration_during_the_COVID-19_pandemic");
$div_wikipedia_Communication_of_the_Trump_administration_during_the_COVID_19_pandemic->content = <<<HTML
	<p>
	</p>
	HTML;


$div_wikipedia_U_S_federal_government_response_to_the_COVID_19_pandemic = new WikipediaContentSection();
$div_wikipedia_U_S_federal_government_response_to_the_COVID_19_pandemic->setTitleText("U.S. federal government response to the COVID-19 pandemic");
$div_wikipedia_U_S_federal_government_response_to_the_COVID_19_pandemic->setTitleLink("https://en.wikipedia.org/wiki/U.S._federal_government_response_to_the_COVID-19_pandemic");
$div_wikipedia_U_S_federal_government_response_to_the_COVID_19_pandemic->content = <<<HTML
	<p>
	</p>
	HTML;



$div_wikipedia_Trump_administration_political_interference_with_science_agencies = new WikipediaContentSection();
$div_wikipedia_Trump_administration_political_interference_with_science_agencies->setTitleText("Trump administration political interference with science agencies");
$div_wikipedia_Trump_administration_political_interference_with_science_agencies->setTitleLink("https://en.wikipedia.org/wiki/Trump_administration_political_interference_with_science_agencies");
$div_wikipedia_Trump_administration_political_interference_with_science_agencies->content = <<<HTML
	<p>
	</p>
	HTML;



$div_wikipedia_COVID_19_pandemic_in_the_United_States = new WikipediaContentSection();
$div_wikipedia_COVID_19_pandemic_in_the_United_States->setTitleText("COVID-19 pandemic in the United States");
$div_wikipedia_COVID_19_pandemic_in_the_United_States->setTitleLink("https://en.wikipedia.org/wiki/COVID-19_pandemic_in_the_United_States");
$div_wikipedia_COVID_19_pandemic_in_the_United_States->content = <<<HTML
	<p>
	</p>
	HTML;



$page->parent('society.html');
$page->template("stub");
$page->body($div_introduction);



$page->body($h2_Democratic_decline);

$page->body($div_Large_scale_decline_of_democratic_institutions_and_political_discourse);



$page->body($h2_Russian_invasion_of_Ukraine);

$page->body($div_Failing_to_support_Ukraine_against_the_Russian_invasion);




$page->body($h2_COVID_Pandemic);

$page->body($div_Handling_of_the_Pandemic_and_COVID_deaths);

$page->body($div_wikipedia_COVID_19_pandemic_in_the_United_States);
$page->body($div_wikipedia_Trump_administration_political_interference_with_science_agencies);
$page->body($div_wikipedia_U_S_federal_government_response_to_the_COVID_19_pandemic);
$page->body($div_wikipedia_Communication_of_the_Trump_administration_during_the_COVID_19_pandemic);
$page->body($div_wikipedia_COVID_19_vaccine_hesitancy_in_the_United_States);
$page->body($div_wikipedia_COVID_19_pandemic_death_rates_by_country);
