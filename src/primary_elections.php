<?php
$page = new Page();
$page->h1("Primary Elections");
$page->viewport_background("");
$page->keywords("Primary Elections");
$page->stars(0);
$page->tags("Elections", "Voting Methods");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Primary_election = new WikipediaContentSection();
$div_wikipedia_Primary_election->setTitleText("Primary election");
$div_wikipedia_Primary_election->setTitleLink("https://en.wikipedia.org/wiki/Primary_election");
$div_wikipedia_Primary_election->content = <<<HTML
	<p>
	</p>
	HTML;


$page->parent('voting_methods.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Primary Elections");
$page->body($div_wikipedia_Primary_election);
