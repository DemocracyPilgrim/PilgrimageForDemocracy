<?php
$page = new Page();
$page->h1("Alleged bribery of Donald Trump by the Egyptian president");
$page->viewport_background("");
$page->keywords("Alleged bribery of Donald Trump by the Egyptian president");
$page->stars(2);
$page->tags("International", "USA", "Egypt", "Donald Trump", "Corruption", "Corruption in the United States");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;





$div_Oversight_Democrats_Launch_Investigation_into_Allegations_Trump_DOJ_Covered_Up = new WebsiteContentSection();
$div_Oversight_Democrats_Launch_Investigation_into_Allegations_Trump_DOJ_Covered_Up->setTitleText(" Oversight Democrats Launch Investigation into Allegations Trump DOJ Covered Up $10 Million Cash Bribe to Donald Trump from Egyptian President");
$div_Oversight_Democrats_Launch_Investigation_into_Allegations_Trump_DOJ_Covered_Up->setTitleLink("https://oversightdemocrats.house.gov/news/press-releases/oversight-democrats-launch-investigation-allegations-trump-doj-covered-10");
$div_Oversight_Democrats_Launch_Investigation_into_Allegations_Trump_DOJ_Covered_Up->content = <<<HTML
	<p>Washington, D.C. (September 3, 2024)—Today, Rep. Jamie Raskin, Ranking Member of the Committee on Oversight and Accountability, and Rep. Robert Garcia, Ranking Member of the Subcommittee on National Security, the Border, and Foreign Affairs, are demanding answers  from Donald Trump after a bombshell report by the Washington Post surfaced new evidence suggesting the former president may have illegally accepted a $10 million campaign contribution from Egyptian President Abdel Fatah El-Sisi during the final weeks of the 2016 presidential campaign and that appointees in his own Department of Justice (DOJ) covered it up.</p>
	HTML;




$div_RCFP_Washington_Post_seek_to_unseal_records_related_to_special_counsel = new WebsiteContentSection();
$div_RCFP_Washington_Post_seek_to_unseal_records_related_to_special_counsel->setTitleText("RCFP, Washington Post seek to unseal records related to special counsel investigation into allegations Egypt sought to fund 2016 Trump campaign");
$div_RCFP_Washington_Post_seek_to_unseal_records_related_to_special_counsel->setTitleLink("https://www.rcfp.org/trump-egypt-special-counsel/");
$div_RCFP_Washington_Post_seek_to_unseal_records_related_to_special_counsel->content = <<<HTML
	<p>The Reporters Committee for Freedom of the Press and The Washington Post are asking two federal district courts to unseal judicial records in separate closed matters that could help shed further light on allegations that Egypt sought to financially support the first presidential campaign of Donald J. Trump.
	The unsealing effort follows The Post’s exclusive investigation published last month that revealed that federal investigators, including former special counsel Robert Mueller, led a secret criminal investigation looking into reports that “Egyptian President Abdel Fatah El-Sisi sought to give [then-candidate] Trump $10 million to boost his 2016 presidential campaign,” and whether “money from Sisi might have factored into Trump’s decision in the final days of his run for the White House to inject his campaign with $10 million of his own money.”
	The Post’s investigation, which is based on dozens of interviews and a review of thousands of pages of documents, raises pressing questions about why the federal probe into Egypt’s alleged effort to inject money into Trump’s campaign was closed under the Trump administration, why it remained dormant under the Biden administration, and what might have been found had it continued.</p>
	HTML;


$page->parent('united_states.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Alleged bribery of Donald Trump by the Egyptian president");

$page->body($div_Oversight_Democrats_Launch_Investigation_into_Allegations_Trump_DOJ_Covered_Up);
$page->body($div_RCFP_Washington_Post_seek_to_unseal_records_related_to_special_counsel);
