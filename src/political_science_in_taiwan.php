<?php
$page = new Page();
$page->h1("Political science in Taiwan");
$page->tags("Electoral System", "Taiwan", "Elections");
$page->stars(0);

$page->snp("description", "Academic research on political science in Taiwan.");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p>Academic research on political science in Taiwan.</p>
	HTML );

// $r1 = $page->ref('', '');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Academic research on political science in Taiwan.</p>
	HTML;

$list_universities = new ListOfPages();
$list_universities->add('soochow_university.html');
$list_universities->add('national_chengchi_university.html');
$list_universities->add('taiwan_election_and_democratization_study.html');
$print_list_universities = $list_universities->print();

$div_list_universities = new ContentSection();
$div_list_universities->content = <<<HTML
	<h3>Universities</h3>

	<p>List of universities in Taiwan which have a political science department.</p>

	<p><em>This list is far from complete. Let us know of other political science departments in Taiwan universities that you may know of.</p>

	$print_list_universities
	HTML;


$page->parent('taiwan.html');
$page->parent('electoral_system.html');
$page->template("stub");
$page->body($div_introduction);
$page->body($div_list_universities);
