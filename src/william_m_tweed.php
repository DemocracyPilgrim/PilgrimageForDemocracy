<?php
$page = new PersonPage();
$page->h1("William M. Tweed, a.k.a. Boss Tweed");
$page->alpha_sort("Tweed, William");
$page->viewport_background("/free/william_m_tweed.png");
$page->keywords("William M. Tweed", "Tweed");
$page->stars(1);
$page->tags("Person");

//$page->snp("description", "");
$page->snp("image",       "/free/william_m_tweed.1200-630.png");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<h3>William M. "Boss" Tweed: A Corrupt Titan of New York Politics</h3>

	<p>William M. Tweed, better known as "Boss" Tweed, remains one of the most infamous figures in American political history.
	Active during the mid-19th century, Tweed rose to become the head of Tammany Hall, the Democratic political machine that dominated New York City politics.
	He wasn't just a politician; he was a masterful manipulator of power, building an elaborate network of patronage, graft,
	and outright theft that allowed him and his cronies to amass enormous wealth at the expense of the city's taxpayers.
	While some might argue his infrastructure projects modernized New York, his legacy is overwhelmingly defined by the staggering scale of his corruption.
	From his humble beginnings as a chairmaker to his reign as the “Boss,”
	Tweed’s story is a cautionary tale about unchecked power and the fragility of democratic institutions.</p>
	HTML;


$div_Manipulating_the_Ballot_Box = new ContentSection();
$div_Manipulating_the_Ballot_Box->content = <<<HTML
	<h3>Manipulating the Ballot Box</h3>

	<p>Tweed’s power wasn't solely derived from his influence within Tammany Hall;
	it was also bolstered by a systematic campaign of electoral manipulation.
	He and his associates employed a variety of tactics to ensure their favored candidates won elections, regardless of the will of the people.
	One common method was blatant voter intimidation.
	Tammany-backed toughs would be stationed at polling places,
	threatening or physically assaulting those who looked like they might vote against their preferred candidates.
	This discouraged many from participating, effectively suppressing opposing viewpoints.</p>

	<p>Beyond intimidation, Tweed's machine excelled at ballot stuffing and multiple voting.
	Repeaters, often paid by Tammany, would move from precinct to precinct casting multiple ballots.
	In some cases, the Tammany organization would even print their own fraudulent ballots or manipulate the tallying process after polls closed.
	These tactics were all designed to create the illusion of popular support for the machine and guarantee victory for Tammany-backed candidates.
	They created an environment where fair elections were virtually impossible, effectively undermining the democratic process in New York City.
	Through these dishonest means, Tweed and Tammany Hall ensured their continued control over the city,
	and a continued flow of funds into their own pockets.</p>
	HTML;


$div_wikipedia_William_M_Tweed = new WikipediaContentSection();
$div_wikipedia_William_M_Tweed->setTitleText("William M Tweed");
$div_wikipedia_William_M_Tweed->setTitleLink("https://en.wikipedia.org/wiki/William_M._Tweed");
$div_wikipedia_William_M_Tweed->content = <<<HTML
	<p>William Magear "Boss" Tweed (April 3, 1823 – April 12, 1878) was an American politician most notable for being the political boss of Tammany Hall, the Democratic Party's political machine that played a major role in the politics of 19th-century New York City and State.</p>
	HTML;


$page->parent('tweed_syndrome.html');
$page->body($div_introduction);
$page->body($div_Manipulating_the_Ballot_Box);
$page->template('cover-picture-ai-generated');



$page->related_tag(array("William M. Tweed", '"Boss" Tweed'));
$page->body($div_wikipedia_William_M_Tweed);
