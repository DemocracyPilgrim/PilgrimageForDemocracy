<?php
$page = new Page();
$page->h1("Comparative Study of Electoral Systems");
$page->keywords("Comparative Study of Electoral Systems", "CSES");
$page->tags("Organisation", "Elections", "Electoral System", "Taiwan");
$page->stars(0);

$page->snp("description", "Collaborative research among election study teams from around the world.");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref('', '');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>The CSES is a collaborative program of research on ${'electoral systems'} among election study teams from around the world.</p>
	HTML;

$div_Taiwan_modules = new ContentSection();
$div_Taiwan_modules->content = <<<HTML
	<h3>Taiwan modules</h3>

	<p>Professor ${'Chi Huang'} from the ${'National Chengchi University'} contributed in collecting data
	for CSES module 1~5.</p>

	<ul>
		<li><a href="https://cses.org/wp-content/uploads/2019/05/2003Plenary_panels_Huang.pdf">[PDF] Taiwan’s Election and Democratization Study, 2001 (TEDS 2001)</a></li>
	</ul>
	</p>
	HTML;


$div_CSES_website = new WebsiteContentSection();
$div_CSES_website->setTitleText("CSES website ");
$div_CSES_website->setTitleLink("https://cses.org/");
$div_CSES_website->content = <<<HTML
	<p>Participating countries and provinces include a common module of survey questions in their post-election studies.
	The resulting data are deposited along with voting, demographic, district and macro/electoral system variables.
	The studies are then merged into a single, free, public dataset for use in comparative study and cross-level analysis.</p>

	<p>The research agenda, questionnaires, and study design are developed by an international committee
	of leading scholars of electoral politics, political science, and survey research.
	The design is then implemented in each country and province by their foremost social scientists.</p>
	HTML;



$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);

$page->body($div_Taiwan_modules);
$page->body('electoral_system.html');

$page->body($div_CSES_website);
