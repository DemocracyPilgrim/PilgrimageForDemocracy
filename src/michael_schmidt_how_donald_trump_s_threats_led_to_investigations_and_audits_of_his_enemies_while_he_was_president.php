<?php
$page = new VideoPage();
$page->h1("Michael Schmidt: How Donald Trump’s threats led to investigations and audits of his enemies while he was President");
$page->tags("Video: Interview", "Donald Trump", "Michael Schmidt", "Trump DOJ Weaponization", "Separation of Powers");
$page->keywords("Michael Schmidt: How Donald Trump’s threats led to investigations and audits of his enemies while he was President");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;


$div_youtube_How_Donald_Trump_s_threats_led_to_investigations_and_audits_of_his_enemies_while_he_was_President = new YoutubeContentSection();
$div_youtube_How_Donald_Trump_s_threats_led_to_investigations_and_audits_of_his_enemies_while_he_was_President->setTitleText("How Donald Trump’s threats led to investigations and audits of his enemies while he was President");
$div_youtube_How_Donald_Trump_s_threats_led_to_investigations_and_audits_of_his_enemies_while_he_was_President->setTitleLink("https://www.youtube.com/watch?v=X6X_UQbqqdQ");
$div_youtube_How_Donald_Trump_s_threats_led_to_investigations_and_audits_of_his_enemies_while_he_was_President->content = <<<HTML
	<p>
	</p>
	HTML;



$page->parent('list_of_videos.html');
$page->template("stub");
$page->body($div_introduction);
$page->body($div_youtube_How_Donald_Trump_s_threats_led_to_investigations_and_audits_of_his_enemies_while_he_was_President);



$page->related_tag("Michael Schmidt: How Donald Trump’s threats led to investigations and audits of his enemies while he was President");
