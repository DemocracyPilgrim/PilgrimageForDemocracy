<?php
$page = new Page();
$page->h1("Ernst Friedrich Schumacher");
$page->alpha_sort("Schumacher, Ernst Friedrich");
$page->tags("Person");
$page->keywords("Ernst Friedrich Schumacher");
$page->stars(0);

//$page->snp("description", "");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_E_F_Schumacher = new WikipediaContentSection();
$div_wikipedia_E_F_Schumacher->setTitleText("E F Schumacher");
$div_wikipedia_E_F_Schumacher->setTitleLink("https://en.wikipedia.org/wiki/E._F._Schumacher");
$div_wikipedia_E_F_Schumacher->content = <<<HTML
	<p>Ernst Friedrich Schumacher CBE (16 August 1911 – 4 September 1977) was a German-British statistician and economist
	who is best known for his proposals for human-scale, decentralised and appropriate technologies.
	He served as Chief Economic Advisor to the British National Coal Board from 1950 to 1970,
	and founded the Intermediate Technology Development Group (now known as Practical Action) in 1966.</p>

	<p>In 1995, his 1973 book Small Is Beautiful: A Study of Economics As If People Mattered
	was ranked by The Times Literary Supplement as one of the 100 most influential books published since World War II.
	In 1977 he published A Guide for the Perplexed as a critique of materialistic scientism
	and as an exploration of the nature and organisation of knowledge.</p>
	HTML;


$page->parent('list_of_people.html');
$page->template("stub");
$page->body($div_introduction);

$page->body('schumacher_center_for_a_new_economics.html');
$page->body('small_is_beautiful.html');
$page->body('appropriate_technology.html');
$page->body('buddhist_economics.html');

$page->body($div_wikipedia_E_F_Schumacher);
