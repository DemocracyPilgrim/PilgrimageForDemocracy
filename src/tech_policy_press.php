<?php
$page = new OrganisationPage();
$page->h1("Tech Policy Press");
$page->viewport_background("");
$page->keywords("Tech Policy Press");
$page->stars(1);
$page->tags("Organisation", "Technology", "The Web Defenders", "Texas");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

$r1 = $page->ref("https://www.techpolicy.press/about-us/", "Tech Policy Press: Who we are");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Tech Policy is a 501(c)3 nonprofit organization incorporated in Texas,
	publishing analysis in the domain of $technology, tech policy and $democracy.</p>

	<p>They cover topics related to: $r1</p>

	<ul>
	<li><strong>Concentration of power</strong>:
	The role and interaction of tech platforms, governments and the media and the future of the public sphere.</li>

	<li><strong>Technology and the economy</strong>:
	The relationship between markets, business, and labor.</li>

	<li><strong>Ethics of technology</strong>:
	How technology should be viewed alongside existing democratic ethos, especially with regard to privacy, surveillance and personal freedoms.</li>

	<li><strong>Geopolitics of technology</strong>:
	How nation-states approach technology in the pursuit of advantage.</li>

	<li><strong>Racism, bigotry, violence and oppression</strong>:
	How tech exacerbates or solves such challenges.</li>

	<li><strong>Election integrity and participation</strong>:
	Mechanisms of democracy, problems such as disinformation and how citizens come to consensus.</li>

	</ul>
	HTML;


$div_Tech_Policy_Press = new WebsiteContentSection();
$div_Tech_Policy_Press->setTitleText("Tech Policy Press ");
$div_Tech_Policy_Press->setTitleLink("https://www.techpolicy.press/");
$div_Tech_Policy_Press->content = <<<HTML
	<p>We publish opinion and analysis.
	At a time of great challenge to democracies globally, we seek to advance a pro-democracy movement in tech and tech policy.</p>
	HTML;



$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Tech Policy Press");
$page->body($div_Tech_Policy_Press);
