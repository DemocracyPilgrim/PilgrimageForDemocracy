<?php
$page = new Page();
$page->h1('Alexis de Tocqueville');
$page->alpha_sort("Tocqueville, Alexis de");
$page->tags("Person");
$page->keywords('Alexis de Tocqueville', 'Tocqueville');
$page->stars(0);

//$page->snp('description', '');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Alexis_de_Tocqueville = new WikipediaContentSection();
$div_wikipedia_Alexis_de_Tocqueville->setTitleText('Alexis de Tocqueville');
$div_wikipedia_Alexis_de_Tocqueville->setTitleLink('https://en.wikipedia.org/wiki/Alexis_de_Tocqueville');
$div_wikipedia_Alexis_de_Tocqueville->content = <<<HTML
	<p>Alexis Charles Henri Clérel, comte de Tocqueville, usually known as just Tocqueville,
	was a French aristocrat, diplomat, political scientist, political philosopher and historian.
	He is best known for his works Democracy in America (appearing in two volumes, 1835 and 1840) and The Old Regime and the Revolution (1856).
	In both, he analyzed the living standards and social conditions of individuals as well as their relationship to the market and state in Western societies.
	Democracy in America was published after Tocqueville's travels in the United States and is today considered an early work of sociology and political science.</p>
	HTML;


$page->parent('list_of_people.html');
$page->template("stub");
$page->body($div_introduction);
$page->body('democracy_in_america.html');


$page->body($div_wikipedia_Alexis_de_Tocqueville);
