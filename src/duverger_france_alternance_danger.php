<?php
$page = new Page();
$page->h1('Duverger example: France and the dangers of alternance');
$page->stars(0);
$page->tags("Elections: Duverger Syndrome", "France");

//$page->snp('description', "");
//$page->snp('image', "/copyrighted/");

$page->preview( <<<HTML
	<p>
	</p>
	HTML );


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;


$page->parent('duverger_syndrome.html');
$page->template("stub");
$page->body($div_introduction);
