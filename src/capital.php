<?php
$page = new Page();
$page->h1("Capital");
$page->viewport_background("/free/capital.png");
$page->keywords("Capital", "capital");
$page->stars(4);
$page->tags("Living: Economy", "Economy", "Fair Share");

$page->snp("description", "Challenging traditional views of capital");
$page->snp("image",       "/free/capital.1200-630.png");

$page->preview( <<<HTML
	<p>Is capital just money, or something more?
	Tools, technology, and even the very concept of wealth are all linked to the human effort that creates them,
	and how this understanding is vital for a truly "Fair Share" economy.</p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<h3>Capital: A Foundation for Understanding Fair Share</h3>

	<p>In our ongoing exploration of "Fair Share,"
	we’ve previously established a working definition of $labour as the human effort—physical, mental, and emotional—applied to productive activities.
	Now, we turn our attention to another crucial factor in economic production: capital.
	Understanding capital, in its various forms and implications,
	is fundamental to discussing economic justice and the fair distribution of wealth and resources.</p>
	HTML;


$div_Defining_Capital_More_Than_Just_Money = new ContentSection();
$div_Defining_Capital_More_Than_Just_Money->content = <<<HTML
	<h3>Defining Capital: More Than Just Money</h3>

	<p>Capital is broadly defined as a factor of production—a resource used to create goods and services.
	It's not merely money itself, but rather the tools, resources, and assets that enable productive activity.
	We can broadly categorize capital into several key forms:</p>

	<ul>
	<li><strong>Physical Capital:</strong>
	This includes tangible assets like machinery, equipment, tools, buildings, infrastructure, and any other physical item used in the production process.
	From a simple hammer to a complex factory, physical capital represents the tangible means of production.</li>

	<li><strong>Financial Capital:</strong>
	This category encompasses money, investments (stocks, bonds, mutual funds, etc.), loans,
	and other financial instruments that represent a claim on assets or future earnings.
	Financial capital serves as a medium of exchange and a store of value, enabling investment and facilitating economic transactions.</li>

	<li><strong>Human Capital:</strong>
	Though distinct from labour itself, human capital refers to the skills, knowledge, education, and experience of individuals that enhance their productivity.
	Investing in human capital—through education and training—is essential for economic growth and individual prosperity.</li>

	<li><strong>Natural Capital:</strong>
	Often considered separately, but vital to production, natural capital refers to the resources provided by nature,
	including land, water, minerals, forests, and other ecosystems.
	These resources form the raw material base for the creation of physical capital.</li>

	</ul>
	HTML;


$div_The_Accumulation_and_Investment_of_Capital = new ContentSection();
$div_The_Accumulation_and_Investment_of_Capital->content = <<<HTML
	<h3>The Accumulation and Investment of Capital</h3>

	<p>Capital is not static; it is accumulated through a process of saving and investment.
	Individuals, businesses, and governments make choices to forgo current consumption in order to invest in activities
	that are expected to yield future benefits. This process of accumulation and investment is essential for:</p>

	<ul>
	<li><strong>Increased Productivity:</strong>
	Capital allows us to produce more with the same amount of labour.
	A carpenter with a power saw is more productive than one with a hand saw.</li>

	<li><strong>Technological Advancement:</strong>
	Investment in research and development, often involving sophisticated capital equipment, drives innovation and technological progress.</li>

	<li><strong>Economic Growth:</strong>
	By increasing productivity and fostering innovation, investment in capital fuels economic growth and allows societies to achieve higher standards of living.</li>

	</ul>

	<p>However, we must note that the rate and direction of this investment have crucial implications for economic justice.
	Does investment focus on sustainable and inclusive development,
	or does it primarily prioritize profits, potentially leading to environmental degradation, worker exploitation, and increased inequality?</p>
	HTML;


$div_The_Return_on_Capital = new ContentSection();
$div_The_Return_on_Capital->content = <<<HTML
	<h3>The Return on Capital</h3>

	<p>Owners of capital receive a return on their investment, typically in the form of profits, interest, rent, or dividends.
	This return on capital is necessary to incentivize investment and ensure the continuous accumulation of capital.
	However, several important questions emerge regarding these returns:</p>

	<ul>
	<li><strong>Just Returns:</strong>
	What constitutes a “just” or fair return on capital?
	Are returns always reflective of the actual value created, or are they sometimes inflated by market power, exploitation, speculation, or rent-seeking behaviours?</li>

	<li><strong>Power Dynamics:</strong>
	The ownership and control of capital often leads to imbalances of power, influencing the distribution of wealth, income, and political power.</li>

	<li><strong>Rent-Seeking:</strong>
	Some individuals or groups use their control over capital to extract excessive profits
	without contributing to genuinely productive activities, creating economic distortions and inequalities.</li>

	</ul>
	HTML;


$div_The_Interplay_of_Labour_and_Capital = new ContentSection();
$div_The_Interplay_of_Labour_and_Capital->content = <<<HTML
	<h3>The Interplay of Labour and Capital</h3>

	<p>Capital and labour are often complementary factors of production, working together to create goods and services.
	However, their relationship is not always harmonious.
	The pursuit of maximum returns on capital can sometimes come at the expense of labour,
	leading to low wages, poor working conditions, and job displacement.</p>

	<ul>
	<li><strong>Fair Labour Practices:</strong>
	It is crucial to ensure that labour receives a fair share of the value it creates through decent wages, good working conditions, and the right to organize.</li>

	<li><strong>Negotiation and Regulation:</strong>
	Collective bargaining, worker protection laws, and regulations are essential to prevent the exploitation of labour
	and to create a more equitable balance of power between capital and labour.</li>

	</ul>
	HTML;


$div_Capital_as_Accumulated_Labour_A_Critical_Insight = new ContentSection();
$div_Capital_as_Accumulated_Labour_A_Critical_Insight->content = <<<HTML
	<h3>Capital as Accumulated Labour: A Critical Insight</h3>

	<p>Here is a vital point that links capital and labour: physical and financial capital are, at their core, the product of past labour.
	A machine is built by human hands, a financial instrument represents a claim on the value created by human activity.
	This means that:</p>

	<ul>
	<li><strong>Challenging the Narrative:</strong>
	It challenges the narrative that wealth is solely a result of capital ownership, ignoring the essential role of labour in its creation.</li>

	<li><strong>Justifying Fair Shares:</strong>
	It strengthens the argument for equitable distribution of wealth and income.
	If all goods are the product of labour, then all contributors to the production process should have a fair share in their results.</li>

	<li><strong>Recognizing Labour's Value:</strong>
	It highlights the fundamental role of labour as the ultimate source of wealth and economic value.</li>

	<li><strong>Critiquing Inequality:</strong>
	It prompts a critique of concentrated capital, excessive income inequality,
	and calls for economic models that prioritize social benefit alongside economic efficiency.</li>

	</ul>
	HTML;



$div_Capital_and_Inequality = new ContentSection();
$div_Capital_and_Inequality->content = <<<HTML
	<h3>Capital and Inequality</h3>

	<p>The way capital is accumulated, owned, and distributed has a profound impact on economic inequality.
	The concentration of capital in the hands of a relatively small portion of the population
	can perpetuate and exacerbate inequality across generations.</p>

	<ul>
	<li><strong>Inheritance of Capital:</strong>
	The inheritance of wealth can create advantages for some while disadvantaging others, irrespective of merit or hard work.</li>

	<li><strong>Social Mobility:</strong>
	It is crucial to implement policies that promote social mobility, provide equal opportunities,
	and ensure that access to capital is not determined by birth, class, or status.</li>

	</ul>
	HTML;



$div_Conclusion_Setting_the_Stage_for_Fair_Share = new ContentSection();
$div_Conclusion_Setting_the_Stage_for_Fair_Share->content = <<<HTML
	<h3>Conclusion: Setting the Stage for Fair Share</h3>

	<p>This exploration of capital provides a critical foundation for our discussions of “Fair Share.”
	It reveals that capital is not simply a neutral tool, but a complex economic force with profound social implications.
	Understanding how capital is created, accumulated, and distributed
	is essential to addressing the root causes of economic inequality and building a more just and equitable society.
	We must constantly evaluate how capital is used and ask:</p>

	<ul>
	<li>Does it serve the common good?</li>

	<li>Does it contribute to a sustainable future?</li>

	<li>Does it promote fair and equitable outcomes for all?</li>

	</ul>

	<p>By grappling with these questions, we can move towards a more just economic system where the benefits of productivity are shared more widely.
	We'll continue to delve into these themes in our coming discussions
	as we explore the practical implications of achieving "Fair Share."</p>
	HTML;


$div_wikipedia_Capital_economics = new WikipediaContentSection();
$div_wikipedia_Capital_economics->setTitleText("Capital economics");
$div_wikipedia_Capital_economics->setTitleLink("https://en.wikipedia.org/wiki/Capital_(economics)");
$div_wikipedia_Capital_economics->content = <<<HTML
	<p>In economics, capital goods or capital are "those durable produced goods that are in turn used as productive inputs for further production" of goods and services. A typical example is the machinery used in a factory. At the macroeconomic level, "the nation's capital stock includes buildings, equipment, software, and inventories during a given year."</p>
	HTML;

$div_wikipedia_Financial_capital = new WikipediaContentSection();
$div_wikipedia_Financial_capital->setTitleText("Financial capital");
$div_wikipedia_Financial_capital->setTitleLink("https://en.wikipedia.org/wiki/Financial_capital");
$div_wikipedia_Financial_capital->content = <<<HTML
	<p>Financial capital (also simply known as capital or equity in finance, accounting and economics) is any economic resource measured in terms of money used by entrepreneurs and businesses to buy what they need to make their products or to provide their services to the sector of the economy upon which their operation is based (e.g. retail, corporate, investment banking). In other words, financial capital is internal retained earnings generated by the entity or funds provided by lenders (and investors) to businesses in order to purchase real capital equipment or services for producing new goods or services.</p>
	HTML;


$page->parent('fair_share.html');
$page->previous('labour_and_value_creation.html');
$page->next('factors_of_production.html');

$page->body($div_introduction);
$page->body($div_Defining_Capital_More_Than_Just_Money);
$page->body($div_The_Accumulation_and_Investment_of_Capital);
$page->body($div_The_Return_on_Capital);
$page->body($div_The_Interplay_of_Labour_and_Capital);
$page->body($div_Capital_as_Accumulated_Labour_A_Critical_Insight);
$page->body($div_Capital_and_Inequality);
$page->body($div_Conclusion_Setting_the_Stage_for_Fair_Share);



$page->related_tag("Capital");
$page->body($div_wikipedia_Capital_economics);
$page->body($div_wikipedia_Financial_capital);
