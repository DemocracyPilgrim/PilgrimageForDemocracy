<?php
$page = new Page();
$page->h1("Jan-Werner Müller");
$page->alpha_sort("Mueller, Jan-Werner");
$page->tags("Person");
$page->keywords("Jan-Werner Müller");
$page->stars(0);

//$page->snp("description", "");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref('', '');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Jan-Werner Müller is an historian, and author of the book: ${"What is populism?"}.</p>
	HTML;

$div_wikipedia_Jan_Werner_Muller = new WikipediaContentSection();
$div_wikipedia_Jan_Werner_Muller->setTitleText("Jan Werner Müller");
$div_wikipedia_Jan_Werner_Muller->setTitleLink("https://en.wikipedia.org/wiki/Jan-Werner_Müller");
$div_wikipedia_Jan_Werner_Muller->content = <<<HTML
	<p>Jan-Werner Müller is a German political philosopher and historian of political ideas working at Princeton University.</p>
	HTML;


$page->parent('list_of_people.html');
$page->template("stub");
$page->body($div_introduction);


$page->body('what_is_populism.html');

$page->body($div_wikipedia_Jan_Werner_Muller);
