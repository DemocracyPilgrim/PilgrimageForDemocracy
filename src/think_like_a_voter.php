<?php
$page = new Page();
$page->h1("Think Like a Voter: A Kid's Guide to Shaping Our Country's Future");
$page->tags("Book");
$page->stars(0);

//$page->snp("description", "");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref('', '');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;




$div_David_s_Books = new WebsiteContentSection();
$div_David_s_Books->setTitleText("David Pakman's Books");
$div_David_s_Books->setTitleLink("https://davidpakman.com/davids-books/");
$div_David_s_Books->content = <<<HTML
	<p>Think Like a Voter: A Kid’s Guide to Shaping Our Country’s Future is an exciting and interactive children’s book
	designed to spark curiosity about voting and civic engagement in young readers.
	Dive deep into the fascinating world of voting, how government and elections work,
	and discover how to participate in and impact the world around you.</p>
	HTML;



$div_wikipedia_David_Pakman = new WikipediaContentSection();
$div_wikipedia_David_Pakman->setTitleText("David Pakman");
$div_wikipedia_David_Pakman->setTitleLink("https://en.wikipedia.org/wiki/David_Pakman");
$div_wikipedia_David_Pakman->content = <<<HTML
	<p>David Pakman (born 2 February 1984) is an American progressive talk show host and political commentator.
	He is the host of the talk radio program The David Pakman Show.</p>
	HTML;


$page->parent('list_of_books.html');
$page->parent('teaching_democracy.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_David_s_Books);
$page->body($div_wikipedia_David_Pakman);
