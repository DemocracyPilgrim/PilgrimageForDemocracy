<?php
$page = new Page();
$page->h1("Revolution of Dignity");
$page->tags("International", "Ukraine");
$page->keywords("Revolution of Dignity");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Revolution_of_Dignity = new WikipediaContentSection();
$div_wikipedia_Revolution_of_Dignity->setTitleText("Revolution of Dignity");
$div_wikipedia_Revolution_of_Dignity->setTitleLink("https://en.wikipedia.org/wiki/Revolution_of_Dignity");
$div_wikipedia_Revolution_of_Dignity->content = <<<HTML
	<p>The Revolution of Dignity, also known as the Maidan Revolution or the Ukrainian Revolution, took place in Ukraine in February 2014 at the end of the Euromaidan protests, when deadly clashes between protesters and state forces in the capital Kyiv culminated in the ousting of President Viktor Yanukovych, the return to the 2004 Constitution of Ukraine, and the outbreak of the 2014 Russo-Ukrainian War.</p>
	HTML;


$page->parent('ukraine.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Revolution of Dignity");
$page->body($div_wikipedia_Revolution_of_Dignity);
