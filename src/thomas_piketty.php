<?php
$page = new PersonPage();
$page->h1('Thomas Piketty');
$page->alpha_sort("Piketty, Thomas");
$page->keywords('Thomas Piketty', 'Piketty');
$page->stars(0);
$page->tags("Person", "Fair Share");

$page->snp('description', 'French economist');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p>Thomas Piketty is a French economist, author of the book: 'Capital in the Twenty-First century'.</p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Thomas Piketty is a French economist, author of the book: ${'Capital in the Twenty-First Century'}.</p>
	HTML;

$div_wikipedia_Thomas_Piketty = new WikipediaContentSection();
$div_wikipedia_Thomas_Piketty->setTitleText('Thomas Piketty');
$div_wikipedia_Thomas_Piketty->setTitleLink('https://en.wikipedia.org/wiki/Thomas_Piketty');
$div_wikipedia_Thomas_Piketty->content = <<<HTML
	<p>Thomas Piketty is a French economist who is a professor of economics at the School for Advanced Studies in the Social Sciences,
	associate chair at the Paris School of Economics
	and Centennial Professor of Economics in the International Inequalities Institute at the London School of Economics.</p>
	HTML;


$page->parent('list_of_people.html');
$page->template("stub");
$page->body($div_introduction);

$page->body('capital_in_the_twenty_first_century.html');
$page->body('fair_share.html');

$page->body($div_wikipedia_Thomas_Piketty);
