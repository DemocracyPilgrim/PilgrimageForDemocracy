<?php
$page = new Page();
$page->h1('Ruth Ben-Ghiat');
$page->alpha_sort("Ben-Ghiat, Ruth");
$page->tags("Person");
$page->keywords('Ruth Ben-Ghiat', 'Ben-Ghiat');
$page->stars(0);

$page->snp('description', 'American historian.');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p>Ruth Ben-Ghiat is Professor of History and Italian Studies at New York University.
	She writes about fascism, authoritarianism, propaganda, and democracy protection.</p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Ruth Ben-Ghiat is Professor of History and Italian Studies at New York University.
	She writes about fascism, authoritarianism, propaganda, and democracy protection.</p>
	HTML;


$div_Ruth_Ben_Ghiat_website = new WebsiteContentSection();
$div_Ruth_Ben_Ghiat_website->setTitleText("Ruth Ben-Ghiat's website");
$div_Ruth_Ben_Ghiat_website->setTitleLink("https://ruthbenghiat.com/");
$div_Ruth_Ben_Ghiat_website->content = <<<HTML
	<p>Ruth Ben-Ghiat is Professor of History and Italian Studies at New York University.
	She writes about fascism, authoritarianism, propaganda, and democracy protection.
	She is the recipient of Guggenheim and other fellowships, an advisor to Protect Democracy, and an MSNBC opinion columnist.
	She appears frequently on MSNBC, PBS, and other networks.</p>

	<p>Her latest book, Strongmen: Mussolini to the Present, examines how illiberal leaders use corruption, violence, propaganda,
	and machismo to stay in power, and how resistance to them has unfolded over a century.</p>
	HTML;


$div_wikipedia_Ruth_Ben_Ghiat = new WikipediaContentSection();
$div_wikipedia_Ruth_Ben_Ghiat->setTitleText('Ruth Ben-Ghiat');
$div_wikipedia_Ruth_Ben_Ghiat->setTitleLink('https://en.wikipedia.org/wiki/Ruth_Ben-Ghiat');
$div_wikipedia_Ruth_Ben_Ghiat->content = <<<HTML
	<p>Ruth Ben-Ghiat is an American historian and cultural critic.
	She is a scholar on fascism and authoritarian leaders.
	Ben-Ghiat is professor of history and Italian studies at New York University.</p>
	HTML;


$page->parent('list_of_people.html');
$page->template("stub");
$page->body($div_introduction);


$page->body('strongmen_mussolini_to_the_present.html');
$page->body($div_Ruth_Ben_Ghiat_website);
$page->body($div_wikipedia_Ruth_Ben_Ghiat);
