<?php
$page = new Page();
$page->h1("PACER (Public Access to Court Electronic Records)");
$page->keywords("PACER");
$page->tags("Institutions: Judiciary");
$page->stars(0);

$page->snp("description", "Public Access to Court Electronic Records");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref('', '');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;



$div_The_Cost_of_PACER_Data_Around_One_Billion_Dollars = new WebsiteContentSection();
$div_The_Cost_of_PACER_Data_Around_One_Billion_Dollars->setTitleText("The Cost of PACER Data? Around One Billion Dollars.");
$div_The_Cost_of_PACER_Data_Around_One_Billion_Dollars->setTitleLink("https://free.law/2016/10/10/pacer-costs-a-billion-dollars");
$div_The_Cost_of_PACER_Data_Around_One_Billion_Dollars->content = <<<HTML
	<p>Recently, we (${'Free Law Project'} started a new project to analyze a few million PACER documents that we acquired through the RECAP Project.
	(...) The average length of a PACER document is 9.1 pages...
	There are more than one billion documents in in PACER.
	With these two statistics and the knowledge that downloading a document costs ten cents per page,
	we can once again see how PACER---the biggest paywall the world has ever known---is a deeply troubling system.
	At this price, purchasing the contents of PACER would cost somewhere on the order of one billion dollars.</p>
	HTML;



$div_As_Bloomberg_Law_imposes_caps_on_PACER_access_PACER_must_support_academics = new WebsiteContentSection();
$div_As_Bloomberg_Law_imposes_caps_on_PACER_access_PACER_must_support_academics->setTitleText("As Bloomberg Law imposes caps on PACER access, PACER must support academics.");
$div_As_Bloomberg_Law_imposes_caps_on_PACER_access_PACER_must_support_academics->setTitleLink("https://free.law/2020/04/04/as-bloomberg-law-imposes-caps-on-pacer-access-pacer-must-support-academics");
$div_As_Bloomberg_Law_imposes_caps_on_PACER_access_PACER_must_support_academics->content = <<<HTML
	<p>${'Free Law Project'}: For years, Bloomberg Law has shared its immense collection of PACER data with academic researchers in subscribing institutions.
	Bloomberg Law has done so as a standard feature of its subscription service,
	and would even go purchase documents from PACER if a researcher so requested it.
	As the director of Free Law Project I have talked to numerous researchers that used this system as a backbone of their legal research.
	From a researcher's perspective it was great:
	Your institution paid an annual fee and in exchange you had access to the PACER information you needed.</p>

	<p>Well, it appears those days are coming to an end....</p>
	</p>
	HTML;



$div_wikipedia_PACER_law = new WikipediaContentSection();
$div_wikipedia_PACER_law->setTitleText("PACER law");
$div_wikipedia_PACER_law->setTitleLink("https://en.wikipedia.org/wiki/PACER_(law)");
$div_wikipedia_PACER_law->content = <<<HTML
	<p>PACER (acronym for Public Access to Court Electronic Records)
	is an electronic public access service for United States federal court documents.
	It allows authorized users to obtain case and docket information from the United States district courts,
	United States courts of appeals, and United States bankruptcy courts.
	The system is managed by the Administrative Office of the United States Courts in accordance with the policies of the Judicial Conference,
	headed by the Chief Justice of the United States.
	As of 2013, it holds more than 500 million documents.</p>
	HTML;


$page->parent('justice.html');
$page->template("stub");
$page->body($div_introduction);

$page->body('free_law_project.html');
$page->body($div_The_Cost_of_PACER_Data_Around_One_Billion_Dollars);
$page->body($div_As_Bloomberg_Law_imposes_caps_on_PACER_access_PACER_must_support_academics);

$page->body($div_wikipedia_PACER_law);
