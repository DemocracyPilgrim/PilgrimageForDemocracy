<?php
$page = new Page();
$page->h1("Advertising");
$page->keywords("advertising");
$page->tags("Taxes");
$page->stars(0);

//$page->snp("description", "");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref('', '');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>We shall investigate the role that advertising plays in our society:</p>

	<ul>
		<li>Negative effects of advertising in general.</li>
		<li>Effect on promoting consumerism, creating artificial needs.</li>
		<li>Taxing advertising (see $taxes).</li>
		<li>Political advertising, propaganda, use by illiberal democracies, populists, negative campaigning (See ${'Duverger Syndrome'}.</li>
		<li>${'social networks'} and advertising revenues.</li>
		<li>Making citizens (consumers) the product.</li>
	</ul>
	HTML;

$div_wikipedia_Advertising = new WikipediaContentSection();
$div_wikipedia_Advertising->setTitleText("Advertising");
$div_wikipedia_Advertising->setTitleLink("https://en.wikipedia.org/wiki/Advertising");
$div_wikipedia_Advertising->content = <<<HTML
	<p>Advertising is the practice and techniques employed to bring attention to a product or service.
	Advertising aims to put a product or service in the spotlight in hopes of drawing it attention from consumers.
	It is typically used to promote a specific good or service, but there are wide range of uses,
	the most common being the commercial advertisement.</p>
	HTML;


$page->parent('taxes.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_wikipedia_Advertising);
