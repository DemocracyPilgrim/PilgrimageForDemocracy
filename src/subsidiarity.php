<?php
$page = new Page();
$page->h1("Subsidiarity");
$page->tags("Institutions: Executive");
$page->keywords("Subsidiarity");
$page->stars(1);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>The principle of subsidiarity is a fundamental concept in political philosophy and governance, particularly within the context of the ${'European Union'}.
	It essentially states that decisions should be taken at the lowest possible level of government, closest to the people affected by those decisions.</p>
	HTML;


$div_Core_principles = new ContentSection();
$div_Core_principles->content = <<<HTML
	<h3>Core principles</h3>

	<p>Here's a breakdown of the key aspects of subsidiarity:</p>


	<p><strong>Decentralization of Power</strong>: Subsidiarity promotes the distribution of power away from central authorities
	and towards local or regional levels of government.</p>

	<p><strong>Local Knowledge and Needs</strong>: The idea is that local communities are
	better equipped to understand and address their specific needs and challenges.</p>

	<p>Citizen Participation</strong>: By making decisions at the local level,
	subsidiarity encourages citizen involvement and engagement in governance.</p>

	<p><strong>Efficiency and Effectiveness</strong>: Local governments often have
	a better understanding of their constituents' needs and can tailor solutions accordingly.</p>

	<p><strong>Citizen Empowerment</strong>: Greater local decision-making empowers citizens
	and promotes a sense of ownership in governance.</p>

	<p><strong>Flexibility and Innovation</strong>: Local governments can experiment with different approaches to address challenges,
	leading to greater innovation.</p>

	<p>The principle of subsidiarity is a valuable framework for promoting democratic governance
	by ensuring that decisions are made as close as possible to the people affected.
	It encourages citizen participation, local knowledge, and efficient governance,
	but it requires careful consideration of challenges related to coordination, resource allocation, and potential fragmentation.</p>
	HTML;


$div_Application = new ContentSection();
$div_Application->content = <<<HTML
	<h3>Application</h3>

	<p><strong>EU Framework</strong>: The principle of subsidiarity is enshrined in the $EU treaties,
	guiding the allocation of responsibilities between the EU institutions and national governments.</p>

	<p>While the EU has formally adopted subsidiarity, the principle has influenced other political systems,
	including federal states like the ${'United States'} and $Germany.</p>
	HTML;


$div_Challenges = new ContentSection();
$div_Challenges->content = <<<HTML
	<h3>Challenges</h3>

	<p>Ensuring effective coordination between different levels of government can be complex, especially in large or diverse societies.</p>

	<p>Local governments may have unequal resources, potentially leading to disparities in service delivery.</p>

	<p>If not properly managed, subsidiarity could lead to fragmentation and inconsistencies in policy implementation.</p>
	HTML;


$list_Subsidiarity = ListOfPeoplePages::WithTags("Subsidiarity");
$print_list_Subsidiarity = $list_Subsidiarity->print();

$div_list_Subsidiarity = new ContentSection();
$div_list_Subsidiarity->content = <<<HTML
	<h3>Related content</h3>

	$print_list_Subsidiarity
	HTML;




$div_wikipedia_Subsidiarity = new WikipediaContentSection();
$div_wikipedia_Subsidiarity->setTitleText("Subsidiarity");
$div_wikipedia_Subsidiarity->setTitleLink("https://en.wikipedia.org/wiki/Subsidiarity");
$div_wikipedia_Subsidiarity->content = <<<HTML
	<p>Subsidiarity is a principle of social organization that holds that social and political issues
	should be dealt with at the most immediate or local level that is consistent with their resolution.
	The Oxford English Dictionary defines subsidiarity as "the principle that a central authority should have a subsidiary function,
	performing only those tasks which cannot be performed at a more local level".
	The concept is applicable in the fields of government, political science, neuropsychology,
	cybernetics, management and in military command (mission command).</p>
	HTML;


$page->parent('institutions.html');

$page->body($div_introduction);
$page->body($div_Core_principles);
$page->body($div_Application);
$page->body($div_Challenges);

$page->body($div_list_Subsidiarity);


$page->body($div_wikipedia_Subsidiarity);
