<?php
$page = new Page();
$page->h1("5: Information");
$page->tags("Topic portal");
$page->keywords("Information", "information", "Information: Government", "Information: Media", "Information: Discourse", "Information: Social Networks", "Information: Facts");
$page->stars(1);
$page->viewport_background("/free/information.png");

$page->snp("description", "Access to accurate information is a necessity in a democracy.");
$page->snp("image",       "/free/information.1200-630.png");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Access to accurate information is a necessity in a democracy.</p>
	HTML;




$div_list_Information_Government = new ContentSection();
$div_list_Information_Government->content = <<<HTML
	<h3>Government</h3>
	HTML;




$div_list_Information_Media = new ContentSection();
$div_list_Information_Media->content = <<<HTML
	<h3>Media</h3>
	HTML;



$div_Information_Facts = new ContentSection();
$div_Information_Facts->content = <<<HTML
	<h3>Information: Facts</h3>

	<p>
	</p>
	HTML;



$div_list_Information_Discourse = new ContentSection();
$div_list_Information_Discourse->content = <<<HTML
	<h3>Discourse</h3>
	HTML;



$div_list_Information_Social_Networks = new ContentSection();
$div_list_Information_Social_Networks->content = <<<HTML
	<h3>Social Networks</h3>
	HTML;



$div_list_Information = new ContentSection();
$div_list_Information->content = <<<HTML
	<h3>Related content</h3>
	HTML;



$div_wikipedia_Information = new WikipediaContentSection();
$div_wikipedia_Information->setTitleText("Information");
$div_wikipedia_Information->setTitleLink("https://en.wikipedia.org/wiki/Information");
$div_wikipedia_Information->content = <<<HTML
	<p>The concept of information is relevant or connected to various concepts,
	including constraint, communication, control, data, form, education, knowledge, meaning, understanding,
	mental stimuli, pattern, perception, proposition, representation, and entropy.</p>
	HTML;


$page->parent('fifth_level_of_democracy.html');
$page->previous("elections.html");
$page->next("living.html");

$page->body($div_introduction);

$page->related_tag("Information: Government",      $div_list_Information_Government);
$page->related_tag("Information: Media",           $div_list_Information_Media);
$page->related_tag("Information: Facts",           $div_Information_Facts);
$page->related_tag("Information: Social Networks", $div_list_Information_Social_Networks);
$page->related_tag("Information: Discourse",       $div_list_Information_Discourse);
$page->related_tag("Information",                  $div_list_Information);

$page->body($div_wikipedia_Information);
