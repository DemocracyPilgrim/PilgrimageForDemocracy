<?php
$page = new Page();
$page->h1("Springfield, Ohio, cat-eating hoax");
$page->tags("Information: Discourse", "Disinformation", "Stochastic Terrorism", "Donald Trump");
$page->keywords("Springfield, Ohio, cat-eating hoax");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Springfield_Ohio_cat_eating_hoax = new WikipediaContentSection();
$div_wikipedia_Springfield_Ohio_cat_eating_hoax->setTitleText("Springfield Ohio cat eating hoax");
$div_wikipedia_Springfield_Ohio_cat_eating_hoax->setTitleLink("https://en.wikipedia.org/wiki/Springfield,_Ohio,_cat-eating_hoax");
$div_wikipedia_Springfield_Ohio_cat_eating_hoax->content = <<<HTML
	<p>Starting in September 2024, baseless claims and rumors spread online that Haitian immigrants were stealing pets in Springfield, Ohio, and eating them. The claims began with a local Facebook group post sharing a neighbor's story that her daughter's friend's cat had been butchered, then spread quickly among far-right and neo-Nazi groups. The claims were amplified by prominent figures in the American right, most notably Republican vice-presidential nominee and U.S. Senator representing Ohio, JD Vance, followed by his running mate Donald Trump and allies such as Laura Loomer and Twitter owner Elon Musk. The person whose Facebook story started the controversy later said she never spoke to the cat owner and admitted the story lacked credibility.</p>
	HTML;


$page->parent('disinformation.html');
$page->template("stub");
$page->body($div_introduction);

$page->body('glenn_kirschner_on_trump_s_legal_issues_for_haitian_immigrant_hoax.html');


$page->related_tag("Springfield, Ohio, cat-eating hoax");
$page->body($div_wikipedia_Springfield_Ohio_cat_eating_hoax);
