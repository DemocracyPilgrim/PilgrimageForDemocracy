<?php
$page = new Page();
$page->h1("Orwellian Doublespeak in today's political discourse");
$page->tags("Information: Discourse", "Disinformation", "Orwellian", "Donald Trump");
$page->keywords("Orwellian Doublespeak");
$page->stars(1);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");
$r1 = $page->ref("https://en.wikipedia.org/wiki/Estate_tax_in_the_United_States#The_term_%22death_tax%22", 'The term "death tax"');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>In ${'George Orwell'}'s novel ${'Nineteen Eighty-Four'}, Oceania's four government ministries display the Party's three slogans:</p>

	<ul>
		<li>"WAR IS PEACE"</li>
		<li>"FREEDOM IS SLAVERY"</li>
		<li>"IGNORANCE IS STRENGTH"</li>
	</ul>

	<p>The ministries are deliberately named after the opposite (doublethink) of their true functions:</p>

	<ul>
		<li>The Ministry of Peace concerns itself with war,</li>
		<li>the Ministry of Truth with lies,</li>
		<li>the Ministry of Love with torture, and</li>
		<li>the Ministry of Plenty with starvation.</li>
	</ul>

	<p>Such "Doublespeak" language is very prevalent in today's ${'political discourse'} and often forms an intentional part of $disinformation.</p>
	HTML;



$div_Death_tax = new ContentSection();
$div_Death_tax->content = <<<HTML
	<h3>Death tax</h3>

	<p>The "Estate $Tax", only levied from extremely rich estate holders when they pass, was being advertised as a "Death Tax"
	which increased the tax's unpopularity with middle class and poor people, who wouldn't pay it anyway. $r1</p>
	HTML;



$div_Election_Integrity = new ContentSection();
$div_Election_Integrity->content = <<<HTML
	<h3>Election Integrity</h3>

	<p>The $US Republican Party and ${"Donald Trump"}'s campaigning for "${'election integrity'}" is doublespeak to cover the true intents
	of frivolous, voter $disfranchisement lawsuits they file.</p>
	HTML;



$div_wikipedia_Doublespeak = new WikipediaContentSection();
$div_wikipedia_Doublespeak->setTitleText("Doublespeak");
$div_wikipedia_Doublespeak->setTitleLink("https://en.wikipedia.org/wiki/Doublespeak");
$div_wikipedia_Doublespeak->content = <<<HTML
	<p>Doublespeak is language that deliberately obscures, disguises, distorts, or reverses the meaning of words. Doublespeak may take the form of euphemisms (e.g., "downsizing" for layoffs and "servicing the target" for bombing), in which case it is primarily meant to make the truth sound more palatable. It may also refer to intentional ambiguity in language or to actual inversions of meaning. In such cases, doublespeak disguises the nature of the truth. Doublespeak is most closely associated with political language.</p>
	HTML;

$div_wikipedia_Doublespeak_Award = new WikipediaContentSection();
$div_wikipedia_Doublespeak_Award->setTitleText("Doublespeak Award");
$div_wikipedia_Doublespeak_Award->setTitleLink("https://en.wikipedia.org/wiki/Doublespeak_Award");
$div_wikipedia_Doublespeak_Award->content = <<<HTML
	<p>The Doublespeak Award was a humorous award in the United States of America. It was described as an "ironic tribute to public speakers who have perpetuated language that is grossly deceptive, evasive, euphemistic, confusing, or self-centered", i.e. those who have engaged in doublespeak. It was issued by the US National Council of Teachers of English between 1974 and 2020. Nominees needed to be from the US, though in 1975 the award was given to Yasser Arafat. In 2022, it was announced the award would be superseded by an annual list of multiple examples of such language from a public spokesperson or group, to be called The Year in Doublespeak. Its opposite is the Orwell Award for authors, editors, or producers of a print or non-print work that "contributes to honesty and clarity in public language".</p>
	HTML;


$page->parent('disinformation.html');
$page->body($div_introduction);

$page->body($div_Death_tax);
$page->body($div_Election_Integrity);


$page->related_tag("Orwellian Doublespeak");
$page->body($div_wikipedia_Doublespeak);
$page->body($div_wikipedia_Doublespeak_Award);
