<?php
require_once 'section/all.php';

$page = new Page();
$page->h1('Menu');
$page->viewport_background('/free/menu.png');
$page->stars(-1);
$page->keywords('menu');

$page->snp('description', 'Resources and articles about democracy and social justice.');
$page->snp('image', "/free/menu.1200-630.png");

$page->preview( <<<HTML
	<p>Welcome to our exploration of democracy!
	Our website is an ever-evolving resource, a space for inquiry and shared understanding.
	Join the conversation and contribute to the future we all want.
	Explore our sections and articles here.</p>
	HTML );


$div_build_good_media = new ContentSection();
$div_build_good_media->content = <<<HTML
	<h3>3: Good media environment</h3>
	HTML;



$h2_media = new h2HeaderContent('About the Media');


$div_media = new ContentSection();
$div_media->content = <<<HTML
	<h3><a href="/media.html">Media</a></h3>
	HTML;


$h2_justice = new h2HeaderContent('About Justice');







$list_Topics = ListOfPeoplePages::WithTags("Topic portal");

$div_Topics = new ContentSection();
$div_Topics->content = $list_Topics->print();



if (!function_exists('democracy_definition_menu')) {
	function democracy_definition_menu(&$page) {
		global $democracy;
		$h2_what_is_democracy = new h2HeaderContent('What is democracy?');

		$div_what_is_democracy = new ContentSection();
		$div_what_is_democracy->content = <<<HTML
			<h3>Definition of Democracy</h3>

			<p>Before we can even start discussing the ways our $democracy can be improved,
			we must agree on a common sense definition of the term.</p>

			<p>Searching how countless thinkers and scholars define the word "democracy",
			we can't fail to notice that there is no simple, standard definition.
			Similarly, if we were to ask common people on the street to define democracy, we would get a variety of different answers
			and we would be no closer to having an exact definition of the term.</p>

			<p>However, by putting together all the possible answers and definitions,
			we can highlight the intrinsic qualities of democracies.
			A definite series of features stand out,
			with each one deriving from the previously enumerated ones.</p>

			<p>It is, after all, critical to gain a clear understanding of the defining features of a democracy,
			because it provides a bedrock upon which to build all of that which will be discussed afterwards,
			a standard that we can use to gauge our democratic achievements.</p>
			HTML;


		$page->body($h2_what_is_democracy);
		$page->body($div_what_is_democracy);
		$page->body('invoking_democracy.html');
		$page->body('first_level_of_democracy.html');
		$page->body('second_level_of_democracy.html');
		$page->body('third_level_of_democracy.html');
		$page->body('fourth_level_of_democracy.html');
		$page->body('fifth_level_of_democracy.html');
		$page->body('sixth_level_of_democracy.html');
		$page->body('seventh_level_of_democracy.html');
		$page->body('eighth_level_of_democracy.html');
		$page->body('democracy_wip.html');
		$page->body('the_soloist_and_the_choir.html');
		$page->body('saints_and_little_devils.html');
		$page->body('teaching_democracy.html');
		$page->body('democracy_taxes.html');
		$page->body('environmental_stewardship.html');
		$page->body('digital_habitat.html');
	}
}




$page->body($div_stars);

democracy_definition_menu($page);

$page->body($div_Topics);


world_menu($page);

threats_menu($page);

duverger_syndrome_menu($page);

tweed_syndrome_menu($page);

voting_methods_menu($page);




$page->body($h2_media);
$page->body('media.html');
$page->body('foreign_influence_local_media.html');


$page->body($h2_justice);
$page->body('justice.html');
$page->body('social_justice.html');

taxes_menu($page);
fair_share_menu($page);
leadership_menu($page);
external_threats_menu($page);
speech_menu($page);



