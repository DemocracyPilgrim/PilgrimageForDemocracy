<?php
$page = new Page();
$page->h1("Habitat");
$page->tags("Environment");
$page->keywords("Habitat");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Here, habitat may refer to our natural habitat, which it our planet Earth and the natural $environment.
	It may also refer to our mental habitat, with the web, the ${'digital habitat'}, being an important part of it.</p>
	HTML;

$div_wikipedia_Habitat = new WikipediaContentSection();
$div_wikipedia_Habitat->setTitleText("Habitat");
$div_wikipedia_Habitat->setTitleLink("https://en.wikipedia.org/wiki/Habitat");
$div_wikipedia_Habitat->content = <<<HTML
	<p>In ecology, habitat refers to the array of resources, physical and biotic factors that are present in an area, such as to support the survival and reproduction of a particular species. A species habitat can be seen as the physical manifestation of its ecological niche. Thus "habitat" is a species-specific term, fundamentally different from concepts such as environment or vegetation assemblages, for which the term "habitat-type" is more appropriate.</p>
	HTML;


$page->parent('environment.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Habitat");
$page->body($div_wikipedia_Habitat);
