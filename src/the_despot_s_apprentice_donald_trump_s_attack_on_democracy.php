<?php
$page = new Page();
$page->h1('The Despot\'s Apprentice: Donald Trump\'s Attack on Democracy');
$page->tags("USA", "Donald Trump", "Book", "Elections");
$page->stars(0);

//$page->snp('description', '');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>The Despot's Apprentice: ${'Donald Trump'}'s Attack on $Democracy is a book by Brian Klaas.</p>
	HTML;

$div_wikipedia_Brian_Klaas = new WikipediaContentSection();
$div_wikipedia_Brian_Klaas->setTitleText('Brian Klaas');
$div_wikipedia_Brian_Klaas->setTitleLink('https://en.wikipedia.org/wiki/Brian_Klaas');
$div_wikipedia_Brian_Klaas->content = <<<HTML
	<p>Brian Paul Klaas is an American political scientist and contributing writer at The Atlantic.
	He is an associate professor in global politics at University College London.</p>
	HTML;


$page->parent('list_of_books.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_wikipedia_Brian_Klaas);
