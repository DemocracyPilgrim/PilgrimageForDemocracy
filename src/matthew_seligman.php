<?php
$page = new Page();
$page->h1("Matthew Seligman");
$page->alpha_sort("Seligman, Matthew");
$page->tags("Person", "USA", "Lawyer", "Institutions: Judiciary", "Electoral System");
$page->keywords("Matthew Seligman");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

$r1 = $page->ref("https://www.matthewseligman.com/about-me", "About: Matthew A. Seligman");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Matthew Seligman is an $American $lawyer.</p>

	<p>He is the co-author, together with ${'Lawrence Lessig'} of the book: ${"How to Steal a Presidential Election"}.</p>
	HTML;



$div_Matthew_A_Seligman = new WebsiteContentSection();
$div_Matthew_A_Seligman->setTitleText("Matthew A. Seligman ");
$div_Matthew_A_Seligman->setTitleLink("https://www.matthewseligman.com/");
$div_Matthew_A_Seligman->content = <<<HTML
	<p>Matthew A. Seligman is a lawyer and legal scholar based in Washington, D.C.
	His legal practice focuses on Supreme Court litigation and election law.
	He is currently a Fellow at the Constitutional Law Center at Stanford Law School.</p>

	<p>In the fall of 2020, Matthew co-taught a seminar at Harvard Law School on disputed presidential elections with Professor Lawrence Lessig.
	He previously served as a Climenko Fellow and Lecturer on Law at Harvard Law School, a Fellow at the Center for Private Law at Yale Law School,
	and a Visiting Assistant Professor of Law at the Cardozo School of Law.
	He has published scholarship in numerous law reviews, including the Stanford Law Review, the Michigan Law Review, and the Vanderbilt Law Review En Banc.</p>
	HTML;



$div_Stanford_Matthew_Seligman = new WebsiteContentSection();
$div_Stanford_Matthew_Seligman->setTitleText("Stanford: Matthew Seligman ");
$div_Stanford_Matthew_Seligman->setTitleLink("https://law.stanford.edu/matthew-seligman/");
$div_Stanford_Matthew_Seligman->content = <<<HTML
	<p>Matthew Seligman is a lawyer and legal scholar whose academic research focuses on election law,
	with a particular emphasis on disputed presidential elections.
	His broader research interests span constitutional law, federal courts, contracts, and private law theory.</p>

	<p>His scholarship has appeared in the Stanford Law Review, the Michigan Law Review, the Vanderbilt Law Review, Philosophy and Public Affairs, and elsewhere.
	His work on disputed presidential elections has received extensive coverage in the New York Times, the Washington Post, and dozens of other venues.
	He is a frequent commentator on election law issues, including numerous appearances on CNN, MSNBC, and other news channels.</p>
	HTML;





$page->parent('list_of_people.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_Matthew_A_Seligman);
$page->body($div_Stanford_Matthew_Seligman);
