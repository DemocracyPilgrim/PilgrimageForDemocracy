<?php
$page = new CountryPage('Egypt');
$page->h1('Egypt');
$page->viewport_background('/free/egypt.png');
$page->keywords('Egypt');
$page->stars(2);
$page->tags("Country", "BRICS");


$page->snp('description', '109 million inhabitants.');
$page->snp('image',       '/free/egypt.1200-630.png');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Egypt, a land of ancient history and vibrant culture,
	is also a nation navigating a complex path in its pursuit of democracy, justice, and social equity.
	While it has undergone significant political shifts in recent decades, challenges persist in establishing a fully inclusive and just society.</p>

	<p>Egypt's political landscape has been marked by periods of both authoritarian rule and popular movements for democratic change.
	While a constitution exists and elections are held, the country's political system faces criticisms for limitations on political freedoms,
	restrictions on civil society organizations, and the suppression of dissent.
	The space for open political debate and independent journalism is often constrained, impacting the health of its democracy.</p>

	<p>The justice system in Egypt also faces significant challenges.
	Concerns persist about due process, fair trials, and the independence of the judiciary.
	Cases of human rights abuses, arbitrary arrests, and prolonged detentions are frequently reported.
	Access to justice for marginalized communities is often limited, contributing to a cycle of inequality.</p>

	<p>Social justice is another pressing issue in Egypt.
	Despite some economic progress, significant disparities in wealth and opportunities persist.
	Poverty rates remain high, especially in rural areas, and access to quality education, healthcare, and employment remains uneven.
	Discrimination based on gender, religious identity, and social class continue to hinder progress towards a more just society.</p>

	<p>Economic reforms and development projects have been undertaken by the government, but their impact on reducing inequality remains debated.
	Moreover, environmental challenges, including water scarcity and pollution, disproportionately affect poorer communities.</p>

	<p>Egypt is also a country with a very young and dynamic population,
	with a significant youth demographic that is increasingly demanding better opportunities, greater freedoms, and more transparent governance.
	There is a vibrant civil society, despite constraints, which continues to advocate for human rights and social justice.</p>

	<p>The future of Egypt will depend on its ability to foster a more inclusive and accountable democracy,
	ensuring equitable access to justice for all, and address the root causes of social inequality.
	Balancing economic progress with the protection of civil liberties and human rights will be crucial for achieving a more stable, prosperous, and just society.</p>
	HTML;

$div_wikipedia_Egypt = new WikipediaContentSection();
$div_wikipedia_Egypt->setTitleText('Egypt');
$div_wikipedia_Egypt->setTitleLink('https://en.wikipedia.org/wiki/Egypt');
$div_wikipedia_Egypt->content = <<<HTML
	<p>Egypt is considered to be a regional power in North Africa, the Middle East and the Muslim world, and a middle power worldwide.
	It is a developing country having a diversified economy, which is the third-largest in Africa
	the 41st-largest economy by nominal GDP, and the 20th-largest globally by PPP.</p>
	HTML;

$div_wikipedia_Politics_of_Egypt = new WikipediaContentSection();
$div_wikipedia_Politics_of_Egypt->setTitleText('Politics of Egypt');
$div_wikipedia_Politics_of_Egypt->setTitleLink('https://en.wikipedia.org/wiki/Politics_of_Egypt');
$div_wikipedia_Politics_of_Egypt->content = <<<HTML
	<p>The politics of Egypt takes place within the framework of a republican semi-presidential system of government.
	The current political system was established following the 2013 Egyptian military coup d'état,
	and the takeover of President Abdel Fattah el-Sisi.</p>
	HTML;

$div_wikipedia_Human_rights_in_Egypt = new WikipediaContentSection();
$div_wikipedia_Human_rights_in_Egypt->setTitleText('Human rights in Egypt');
$div_wikipedia_Human_rights_in_Egypt->setTitleLink('https://en.wikipedia.org/wiki/Human_rights_in_Egypt');
$div_wikipedia_Human_rights_in_Egypt->content = <<<HTML
	<p>Human rights in Egypt are guaranteed by the Constitution of the Arab Republic of Egypt under the various articles of Chapter 3.
	The country is also a party to numerous international human rights treaties,
	including the International Covenant on Civil and Political Rights and the International Covenant on Economic, Social and Cultural Rights.
	However, the state of human rights in the country has been criticized both in the past and the present,
	especially by foreign human rights organisations such as Amnesty International and Human Rights Watch.</p>
	HTML;

$div_wikipedia_Egyptian_Organization_for_Human_Rights = new WikipediaContentSection();
$div_wikipedia_Egyptian_Organization_for_Human_Rights->setTitleText('Egyptian Organization for Human Rights');
$div_wikipedia_Egyptian_Organization_for_Human_Rights->setTitleLink('https://en.wikipedia.org/wiki/Egyptian_Organization_for_Human_Rights');
$div_wikipedia_Egyptian_Organization_for_Human_Rights->content = <<<HTML
	<p>The Egyptian Organization for Human Rights (EOHR), founded in April 1985 and with its headquarters in Cairo, Egypt,
	is a non-profit NGO and one of the longest-standing bodies for the defense of human rights in Egypt.
	It investigates, monitors, and reports on human rights violations and defends people's rights regardless of the identity, gender or color of the victim.</p>
	HTML;


$page->parent('world.html');
$page->body($div_introduction);
$page->related_tag("Egypt");
$page->body('Country indices');

$page->body($div_wikipedia_Egypt);
$page->body($div_wikipedia_Politics_of_Egypt);
$page->body($div_wikipedia_Human_rights_in_Egypt);
$page->body($div_wikipedia_Egyptian_Organization_for_Human_Rights);
