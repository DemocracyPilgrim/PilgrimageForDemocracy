<?php
$page = new Page();
$page->h1("Steven Hahn");
$page->alpha_sort("Hahn, Steven");
$page->tags("Person", "USA", "Historian");
$page->keywords("Steven Hahn");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Steven Hahn is an $American historian.</p>

	<p>He is the author of the book: ${"Illiberal America: A History"}.</p>
	HTML;

$div_wikipedia_Steven_Hahn = new WikipediaContentSection();
$div_wikipedia_Steven_Hahn->setTitleText("Steven Hahn");
$div_wikipedia_Steven_Hahn->setTitleLink("https://en.wikipedia.org/wiki/Steven_Hahn");
$div_wikipedia_Steven_Hahn->content = <<<HTML
	<p>Steven Howard Hahn (born 1951) is Professor of History at New York University.</p>
	HTML;


$page->parent('list_of_people.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_wikipedia_Steven_Hahn);
