<?php
$page = new OrganisationPage();
$page->h1('United Auto Workers');
$page->keywords('United Auto Workers', 'UAW');
$page->tags("Organisation", "Labor's Rights", "Fair Share" );
$page->stars(0);

//$page->snp('description', '');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_United_Auto_Workers = new WikipediaContentSection();
$div_wikipedia_United_Auto_Workers->setTitleText('United Auto Workers');
$div_wikipedia_United_Auto_Workers->setTitleLink('https://en.wikipedia.org/wiki/United_Auto_Workers');
$div_wikipedia_United_Auto_Workers->content = <<<HTML
	<p>The United Auto Workers (UAW) is an American labor union that represents workers
	in the United States (including Puerto Rico) and southern Ontario, Canada.</p>
	HTML;


$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);


$page->body('fair_share.html');
$page->body($div_wikipedia_United_Auto_Workers);
