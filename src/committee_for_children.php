<?php
$page = new OrganisationPage();
$page->h1("Committee for Children");
$page->tags("Organisation", "Children's Rights", "Education", "Peace");
$page->keywords("Committee for Children");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>On a Mission to Ensure Kids Everywhere Can Thrive:
	by empowering students with social-emotional skills, the Committee for Children is creating good citizens and making progress toward a more peaceful world.</p>
	HTML;




$div_Committee_for_Children = new WebsiteContentSection();
$div_Committee_for_Children->setTitleText("Committee for Children ");
$div_Committee_for_Children->setTitleLink("https://www.cfchildren.org/");
$div_Committee_for_Children->content = <<<HTML
	<p>Since 1979, Committee for Children has advocated for policies to enhance, gathered research to support, and developed educational programs to advance the safety and well-being of children through social-emotional learning (SEL).
	We are a 501(c)(3) nonprofit best known for our innovative Second Step® family of SEL programs. Combining classroom-based programs with SEL for out-of-school time settings and SEL for adults, Second Step programs help educators take a holistic approach to building supportive communities for every child through social-emotional learning.
	We collaborate with experts in the field to share experiences and champion the cause of educating the whole child, advocating for policies and legislation that place importance on creating safe and supportive learning environments.
	Reaching more than 26.9 million children worldwide each year, Committee for Children supports children today so they’ll be able to create a safe and positive society in the future.</p>
	HTML;



$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag(" Committee for Children");
$page->body($div_Committee_for_Children);
