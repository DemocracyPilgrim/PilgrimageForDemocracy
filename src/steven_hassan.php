<?php
$page = new PersonPage();
$page->h1("Steven Hassan");
$page->alpha_sort("Hassan, Steven");
$page->tags("Person", "USA", "MAGA", "Donald Trump", "Cults");
$page->keywords("Steven Hassan");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

$r1 = $page->ref("https://freedomofmind.com/about/about-steven-hassan/", "About Steven Hassan Ph.D.");
// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Dr. Steven Hassan is a licensed mental health professional, a recognized expert on cults and undue influence.</p>

	<p>He is the author of ${"The Cult of Trump: A Leading Cult Expert Explains How the President Uses Mind Control"}.</p>
	HTML;



$div_2023_Home_PageDr_Steven_HassanFreedom_of_Mind = new WebsiteContentSection();
$div_2023_Home_PageDr_Steven_HassanFreedom_of_Mind->setTitleText("Dr. Steven Hassan: Freedom of Mind");
$div_2023_Home_PageDr_Steven_HassanFreedom_of_Mind->setTitleLink("https://freedomofmind.com/");
$div_2023_Home_PageDr_Steven_HassanFreedom_of_Mind->content = <<<HTML
	<p>At the Freedom of Mind Resource Center we believe that everyone deserves the right to build their life free from undue influence.</p>

	<p>We support those affected by undue influence by providing coaching and consulting services as well as training and educational resources for individuals, families and professionals. Our team uses the Strategic Interactive Approach (SIA), developed by founder Steven Hassan, an ethical, systems-oriented method to enable loved ones to help an individual regain their freedom of mind.</p>

	<p>We also provide information to the media to help the public understand undue influence (mind control) in all of its forms including controlling people, cults, beliefs, estrangement, and parental alienation, so that everyone can have the tools and support they need to protect themselves from undue influence.</p>
	HTML;




$div_wikipedia_Steven_Hassan = new WikipediaContentSection();
$div_wikipedia_Steven_Hassan->setTitleText("Steven Hassan");
$div_wikipedia_Steven_Hassan->setTitleLink("https://en.wikipedia.org/wiki/Steven_Hassan");
$div_wikipedia_Steven_Hassan->content = <<<HTML
	<p>Steven Alan Hassan is an American writer and mental health counselor who specializes in the area of cults and new religious movements. He worked as a deprogrammer in the late 1970s, but since then has advocated a non-coercive form of exit counseling.
	Hassan has written several books on the subject of mind control and is sometimes described in the media as an expert on mind control and cults. Some researchers in the sociology of religion, however, are critical of his application of mind-control theory to new religious movements.</p>
	HTML;


$page->parent('list_of_people.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_2023_Home_PageDr_Steven_HassanFreedom_of_Mind);

$page->related_tag("Steven Hassan");
$page->body($div_wikipedia_Steven_Hassan);
