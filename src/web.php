<?php
$page = new Page();
$page->h1("Addendum G: The Web");
$page->tags("Media", "Society", "Topic portal");
$page->keywords("The Web", "The Ugly Web", "The Bad Web", "The Enlightened Web", "The Web Defenders", "World Wide Web", "web", "internet");
$page->stars(0);
$page->viewport_background("/free/web.png");

$page->snp("description", "Fixing today's Web, building tomorrow's enlightened Web.");
$page->snp("image",       "/free/web.1200-630.png");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Fixing today's World Wide Web, building tomorrow's enlightened Web.</p>
	HTML;


$list_The_Bad_Web = ListOfPeoplePages::WithTags("The Bad Web");
$print_list_The_Bad_Web = $list_The_Bad_Web->print();

$div_list_The_Bad_Web = new ContentSection();
$div_list_The_Bad_Web->content = <<<HTML
	<h3>The Bad Web</h3>

	$print_list_The_Bad_Web
	HTML;

$list_The_Ugly_Web = ListOfPeoplePages::WithTags("The Ugly Web");
$print_list_The_Ugly_Web = $list_The_Ugly_Web->print();

$div_list_The_Ugly_Web = new ContentSection();
$div_list_The_Ugly_Web->content = <<<HTML
	<h3>The Ugly Web</h3>

	$print_list_The_Ugly_Web
	HTML;

$list_The_Web_Defenders = ListOfPeoplePages::WithTags("The Web Defenders");
$print_list_The_Web_Defenders = $list_The_Web_Defenders->print();

$div_list_The_Web_Defenders = new ContentSection();
$div_list_The_Web_Defenders->content = <<<HTML
	<h3>The Web Defenders</h3>

	$print_list_The_Web_Defenders
	HTML;

$list_The_Enlightened_Web = ListOfPeoplePages::WithTags("The Enlightened Web");
$print_list_The_Enlightened_Web = $list_The_Enlightened_Web->print();

$div_list_The_Enlightened_Web = new ContentSection();
$div_list_The_Enlightened_Web->content = <<<HTML
	<h3>The Enlightened Web</h3>

	$print_list_The_Enlightened_Web
	HTML;



$page->parent('digital_habitat.html');
$page->previous("environment.html");

$page->template("stub");
$page->body($div_introduction);





$page->body($div_list_The_Ugly_Web);
$page->body($div_list_The_Bad_Web);
$page->body($div_list_The_Web_Defenders);
$page->body($div_list_The_Enlightened_Web);


$page->related_tag("The Web");
