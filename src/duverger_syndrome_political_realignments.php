<?php
$page = new Page();
$page->h1("Duverger symptom 7: Periodical political realignments");
$page->stars(0);
$page->tags("Elections: Duverger Syndrome");

//$page->snp("description", "");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p>Regular upheavals in political and social conditions cause major changes in the two-party system.</p>
	HTML );

// $r1 = $page->ref('', '');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Regular upheavals in political and social conditions cause major changes in the two-party system.</p>

	<p>${'Plurality voting'} is causing an artificial duopoly, with two main parties or two main camps
	alternating in power.
	However, as mentioned, this duopoly is artificial, not based in any social reality.
	The divide is not and cannot be stable.
	Thus, social and political forces cause regular realignments,
	often caused by an electorate fatigued by a fruitless alternance between the previous two main parties.
	In some cases, the two main parties change, with one previously smaller party taking one of the two main thrones
	(as happened in the UK, with the Liberal Party being replaced by the Labour Party, or in France,
	where both the main right party and the main left party were replaced at the same time by two challengers).
	In other cases, the two parties remain the same, but their policies and leadership become the subject of a major internal upheaval
	(as happened in the $US where $Trump completely took over the Republican Party).</p>
	HTML;


$div_Political_realignment_in_France = new ContentSection();
$div_Political_realignment_in_France->content = <<<HTML
	<h3>Political realignment in France</h3>

	<p>The two-round voting system used in $France gives more room for third parties to grow.
	What happened in France in slow motion from the 1980s to the 2020s
	with the National Front party in France, led by Jean-Marie Le Pen, then by his daughter Marine Le Pen,
	is not unlike what happened much more quickly in the ${'United States'} with $Trumpism.
	The traditional Right-Left political divide in France gave way to Macron-Le Pen dual...</p>
	HTML;


$div_Party_systems_in_the_United_States = new ContentSection();
$div_Party_systems_in_the_United_States->content = <<<HTML
	<h3>Party systems in the United States</h3>

	<ul>
		<li>First Party System: 1792–1824</li>
		<li>Second Party System: 1828–1854</li>
		<li>Third Party System: 1854–1890s</li>
		<li>Fourth Party System: 1896–1932</li>
		<li>Fifth Party System: 1932–1976</li>
		<li>Sixth Party System: 1980s–2016</li>
		<li>Seventh Party System (2016–present)</li>
	<ul>
	HTML;



$div_wikipedia_Political_realignment = new WikipediaContentSection();
$div_wikipedia_Political_realignment->setTitleText("Political realignment");
$div_wikipedia_Political_realignment->setTitleLink("https://en.wikipedia.org/wiki/Political_realignment");
$div_wikipedia_Political_realignment->content = <<<HTML
	<p>A political realignment, often called a critical election, critical realignment, or realigning election,
	in the academic fields of political science and political history,
	is a set of sharp changes in party ideology, issues, party leaders, regional and demographic bases of power of political parties,
	and the structure or rules of the political system, such as voter eligibility or financing.
	The changes result in a new political power structure that lasts for decades, replacing an older dominant coalition.</p>
	HTML;


$div_wikipedia_Political_parties_in_the_United_States = new WikipediaContentSection();
$div_wikipedia_Political_parties_in_the_United_States->setTitleText("Political parties in the United States: History and political eras");
$div_wikipedia_Political_parties_in_the_United_States->setTitleLink("https://en.wikipedia.org/wiki/Political_parties_in_the_United_States#History_and_political_eras");
$div_wikipedia_Political_parties_in_the_United_States->content = <<<HTML
	<p>Political scientists and historians have divided the development of America's two-party system
	into six or so eras or "party systems",
	starting with the Federalist Party, which supported the ratification of the Constitution,
	and the Anti-Administration party (Anti-Federalists), which opposed a powerful central government
	and later became the Democratic-Republican Party.</p>
	HTML;



$page->parent('duverger_syndrome.html');
$page->parent('duverger_syndrome_extremism.html');

$page->template("stub");
$page->body($div_introduction);



$page->body($div_Party_systems_in_the_United_States);
$page->body($div_wikipedia_Political_parties_in_the_United_States);

$page->body($div_Political_realignment_in_France);
$page->body('duverger_france_alternance_danger.html');

$page->body($div_wikipedia_Political_realignment);

$page->body('duverger_law.html');
