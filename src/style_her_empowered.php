<?php
$page = new OrganisationPage();
$page->h1("Style Her Empowered (SHE)");
$page->tags("Organisation", "Education", "Togo", "Humanity", "Payton McGriff");
$page->keywords("Style Her Empowered");
$page->stars(1);
$page->viewport_background("/free/style_her_empowered.png");

$page->snp("description", "Providing schoolgirls with the confidence to pursue their education.");
$page->snp("image",       "/free/style_her_empowered.1200-630.png");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Style Her Empowered is a charity organization founded by ${'Payton McGriff'}.</p>
	HTML;


$div_Style_Her_Empowered_website = new WebsiteContentSection();
$div_Style_Her_Empowered_website->setTitleText("Style Her Empowered website ");
$div_Style_Her_Empowered_website->setTitleLink("https://www.styleherempowered.org/");
$div_Style_Her_Empowered_website->content = <<<HTML
	<p>S H E is a grassroots, female-founded, and female-led organization working in Togo, Africa. Originally founded in 2017 as a class project, we have evolved into an international organization serving 1,500 girls annually and employing 33 amazing women across 21 rural communities in Togo.</p>

	<p>Our promise:</p>

	<ol>
	<li>To equip and empower people in all we do.</li>
	<li>To create a global community that's uplifting and empowering.</li>
	<li>To share the truth of our journey - the successes, the failures and the progress in the middle.</li>
	<li>To convey the stories of the girls and women we serve with dignity and respect and to never exploit their circumstances.</li>
	<li>To get out of the way.  We are a launch pad for our girls to determine their own future.
	We provide the resources and support our communities need, and they decide the rest.</li>
	</ol>
	HTML;



$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);



$page->template('cover-picture-ai-generated');
$page->related_tag("Style Her Empowered");
$page->body($div_Style_Her_Empowered_website);
