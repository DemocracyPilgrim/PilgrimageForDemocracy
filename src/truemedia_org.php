<?php
$page = new Page();
$page->h1("TrueMedia.org");
$page->keywords("TrueMedia.org");
$page->tags("Organisation", "Information: Discourse", "Artificial Intelligence");
$page->stars(0);

$page->snp("description", "Fighting disinformation in political campaigns by identifying manipulated media.");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );

$r1 = $page->ref('https://www.truemedia.org/post/announcement', 'Announcing TrueMedia.org');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>TrueMedia.org is a non-profit, non-partisan $AI project to fight $disinformation in political campaigns by identifying manipulated $media.</p>
	HTML;


$div_TrueMedia_org_website = new WebsiteContentSection();
$div_TrueMedia_org_website->setTitleText("TrueMedia.org website");
$div_TrueMedia_org_website->setTitleLink("https://www.truemedia.org/");
$div_TrueMedia_org_website->content = <<<HTML
	<p>Disinformation, transmitted virally over social networks, has emerged as the Achilles heel of $democracy in the 21st Century.
	This phenomenon is not limited to a single event or region but has been observed globally, impacting critical political decisions.</p>

	<p>In 2024, we anticipate this challenge to grow explosively
	due to the increased availability of generative AI and associated tools that facilitate manipulating and forging video, audio, images, and text.
	The cost of AI-based forgery (“deepfakes”) has plunged sharply during one of the most important political elections in history.
	As a result, we anticipate a tsunami of $disinformation.
	In the past, disinformation was spread by well-funded state actors but now, due to generative AI, we see the potential for disinformation terrorism.
	A recent Pew study found the percentage of TikTok users that get news from the platform has doubled since 2020 and is now at 43%.</p>

	<p>According to Pew, half of U.S. adults get news at least sometimes from social media.
	This is a recipe for disaster, and one that we are poised to fight
	by creating a free, non-partisan, and broadly accessible dashboard for identifying manipulated media across social media platforms. $r1</p>
	HTML;



$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);

$page->body($div_TrueMedia_org_website);
$page->body('artificial_intelligence.html');
