<?php
$page = new Page();
$page->h1('Stochastic terrorism');
$page->keywords('Stochastic Terrorism', 'stochastic terrorism');
$page->tags("Information: Discourse", "Political Discourse", "Hate Speech", "Racism", "Donald Trump");
$page->stars(0);

$page->snp('description', 'Terrorism randomly incited by divisive discourse.');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Stochastic_terrorism = new WikipediaContentSection();
$div_wikipedia_Stochastic_terrorism->setTitleText('Stochastic terrorism');
$div_wikipedia_Stochastic_terrorism->setTitleLink('https://en.wikipedia.org/wiki/Stochastic_terrorism');
$div_wikipedia_Stochastic_terrorism->content = <<<HTML
	<p>Since 2018, the term "stochastic terrorism" has become a popular descriptor when discussing lone wolf attacks.
	While the exact definition has evolved over time, it commonly refers to the idea that
	demonizing or dehumanizing a targeted group or individual incites violence
	that is statistically likely, but cannot be precisely predicted.</p>
	HTML;



$div_wikipedia_Lone_wolf_attack = new WikipediaContentSection();
$div_wikipedia_Lone_wolf_attack->setTitleText('Lone wolf attack');
$div_wikipedia_Lone_wolf_attack->setTitleLink('https://en.wikipedia.org/wiki/Lone_wolf_attack');
$div_wikipedia_Lone_wolf_attack->content = <<<HTML
	<p>A lone wolf attack, or lone actor attack, is a particular kind of mass murder,
	committed in a public setting by an individual who plans and commits the act on their own.
	In the United States, such attacks are usually committed with firearms.</p>
	HTML;



$page->parent('political_discourse.html');
$page->template("stub");
$page->body($div_introduction);

$page->related_tag("Stochastic Terrorism");

$page->body($div_wikipedia_Stochastic_terrorism);
$page->body($div_wikipedia_Lone_wolf_attack);
