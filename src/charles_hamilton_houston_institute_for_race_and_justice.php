<?php
$page = new OrganisationPage();
$page->h1("Charles Hamilton Houston Institute for Race & Justice");
$page->viewport_background("");
$page->keywords("Charles Hamilton Houston Institute for Race and Justice", "CHHIRJ");
$page->stars(0);
$page->tags("Organisation", "Harvard Law School", "Charles Hamilton", "Racism", "Justice", "Social Justice", "Institutions: Judiciary");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

$r1 = $page->ref("https://charleshamiltonhouston.org/about/", "About CHHIRJ");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>The Charles Hamilton Houston Institute for Race & Justice is a research program of ${'Harvard Law School'}.</p>
	HTML;





$div_Charles_Hamilton_Houston_Institute_for_Race_and_Justice = new WebsiteContentSection();
$div_Charles_Hamilton_Houston_Institute_for_Race_and_Justice->setTitleText("Charles Hamilton Houston Institute for Race and Justice ");
$div_Charles_Hamilton_Houston_Institute_for_Race_and_Justice->setTitleLink("https://charleshamiltonhouston.org/");
$div_Charles_Hamilton_Houston_Institute_for_Race_and_Justice->content = <<<HTML
	<p>The Charles Hamilton Houston Institute for Race & Justice at Harvard Law School (CHHIRJ) was launched in September 2005 by Charles J. Ogletree, Jr., Jesse Climenko Professor of Law. The Institute honors and carries on the unfinished work of Charles Hamilton Houston, one of the 20th century’s most important, but relatively unknown, legal scholars and civil rights litigators.
	CHHIRJ aims to be an organizing force for impactful scholarship, innovative strategic advocacy, coalition building, socially-concerned legal education, and community engagement on matters central to civil rights and equal opportunity in the 21st century.</p>
	HTML;



$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag(" Charles Hamilton Houston Institute for Race and Justice");

$page->body($div_Charles_Hamilton_Houston_Institute_for_Race_and_Justice);
