<?php
$page = new Page();
$page->h1("Tweed Symptom: Uncontested Elections");
$page->viewport_background("");
$page->keywords("Tweed Symptom: Uncontested Elections");
$page->stars(0);
$page->tags("Elections", "Party Politics");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Uncontested elections are the result of both the ${'Duverger Syndrome'} and more specifically the ${'Tweed Syndrome'}.</p>
	HTML;


$page->parent('duverger_syndrome_party_politics.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Tweed Symptom: Uncontested Elections");
