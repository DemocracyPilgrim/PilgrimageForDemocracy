<?php
$page = new VideoPage();
$page->h1("TED Talk: The art of misdirection");
$page->tags("Video: Speech", "Apollo Robbins", "Media", "Social Networks", "TED Talk");
$page->keywords("TED Talk: The art of misdirection");
$page->stars(0);
$page->viewport_background("/free/ted_talk_the_art_of_misdirection.png");

$page->snp("description", "If you could control somebody's attention, what would you do with it?");
$page->snp("image",       "/free/ted_talk_the_art_of_misdirection.1200-630.png");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>"The art of misdirection" is a ${"TED Talk"} by ${"Apollo Robbins"}.</p>

	<p>This TED Talk is featured in the introduction to the ${'fifth level of democracy'},
	to illustrate the power of the $media and ${'social networks'} over our $social and $political awareness.</p>
	HTML;


$div_TED_The_art_of_misdirection = new TEDtalkContentSection();
$div_TED_The_art_of_misdirection->setTitleText("The art of misdirection");
$div_TED_The_art_of_misdirection->setTitleLink("https://www.ted.com/talks/apollo_robbins_the_art_of_misdirection");
$div_TED_The_art_of_misdirection->content = <<<HTML
	<p>Hailed as the greatest pickpocket in the world, Apollo Robbins studies the quirks of human behavior as he steals your watch.
	In a hilarious demonstration, Robbins samples the buffet of the TEDGlobal 2013 audience,
	showing how the flaws in our perception make it possible to swipe a wallet and leave it on its owner's shoulder while they remain clueless.</p>
	HTML;

$page->parent('list_of_videos.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("TED Talk: The art of misdirection");

$page->body($div_TED_The_art_of_misdirection);
