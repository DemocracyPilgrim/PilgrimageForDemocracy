<?php
$page = new Page();
$page->h1("Duverger symptom 6: extremism");
$page->stars(0);
$page->tags("Elections: Duverger Syndrome");

//$page->snp("description", "");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p>Our electoral system, far from helping us finding consensual candidates with broad appeal,
	seem to favour divisive politicians coming from the extremes.</p>
	HTML );

$r1 = $page->ref('https://www.msnbc.com/rachel-maddow/watch/-he-is-not-the-lesser-of-two-evils-cheney-emphasizes-severity-of-trump-threat-199373893792',
                 "'He is not the lesser of two evils': Cheney emphasizes severity of Trump threat");


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>The current political framework favours extremes to the detriment of a more consensual centre.</p>

	<p>Parties and candidates are more likely to push forward more extreme narratives and extreme agendas,
	even when the majority of the country would disagree.
	And the parties pushing such extreme points of view would still win elections
	because the electorate is not given the authority to fully express their opinions on a variety of issues,
	being restricted to a binary choice between two candidates, none of which is ideal from their point of view.</p>

	<p>Parties are more likely to push forward narratives based on false premises,
	because such narratives help to invigorate their base.</p>

	<p>We could analyse each and every wedge issue in any country,
	and find that on each issue, the majority of the electorate does not favour any of the extreme positions on the right and on the left,
	but would prefer a more balanced, practical and common sense position.</p>

	<p>See for example the debate on $immigration.</p>
	HTML;


$div_Primaries = new ContentSection();
$div_Primaries->content = <<<HTML
	<h3>Primaries</h3>

	<p>The current voting method forces political parties to hold primary elections
	so that they have a single candidate facing their opponents, rather than splitting the vote with several candidates.</p>

	<p>The problem is most acute in legislative election, where a majority of the single representative districts are non competitive.
	Whomever wins the primary for the party that has a lock on the district is assured to be elected representative.
	Thus, it pushes primary candidates to adopt more and more extreme positions
	in order to gain the ascent of the most rabid, radical elements of their party.</p>

	<p>This way, more and more legislators come from the extreme fringes of their respective parties,
	leaving a big chasm between the two main parties,
	who are less and less able to work together, compromise and legislate.</p>
	HTML;


$div_He_is_not_the_lesser_of_two_evils = new ContentSection();
$div_He_is_not_the_lesser_of_two_evils->content = <<<HTML
	<h3>"He is not the lesser of two evils"</h3>

	<p>What is happening in 2024 in the ${"United States"} is both extreme and unprecedented.
	And yet, it properly illustrate our point:
	the current ${"voting method"}, is working against voters in electing a moderate, middle-of-the road
	candidate who has a broad support by a super majority of the electorate.</p>

	<p>What we have instead, is a system that allows more and more extreme candidates to reach the highest echelons of power.
	And it is the most obvious in the United States.</p>

	<p>In our previous article about the <a href="/duverger_syndrome_lack_choice.html">perceived lack of good candidates</a>,
	we spoke about bad apples and voters feeling limited into choosing the least rotten one.
	It's often referred to as voting for the lesser of two evils.
	But in Trump, we have a candidate who so blatantly disregard the Rule of Law,
	who has so completely destroyed the concept of peaceful transition of power,
	who is pushing the judicial system to its limits,
	and who has stated again and again, that he would make away with any democratic norm,
	using the Justice Department to go after political opponents and media critics.</p>

	<p>In an interview, staunch Republican and hard-core conservative former Representative Liz Cheney,
	daughter of former Republican Vice President Dick Cheney, said: $r1:</p>

	<blockquote>
		<p>I also think about it from the perspective of of my kids.
		There was a moment right after January 6th where I was having dinner with my husband and our two youngest kids.
		I looked at my sons across the dinner table and and I had this realization:
		I grew up in a country where I didn't have to wonder if we were going to have a peaceful transfer of power in the United States
		and all of a sudden it occurred to me:
		"My God! Maybe they won't be able to say the same thing!"</p>

		<p>That is why it's so fundamentally important that we ensure Democrats, Independents, Republicans all work together,
		that we vote together, that we make clear that Donald Trump is not an acceptable alternative.
		<strong>He is not the lesser of two evils.</strong>
		He is completely unfit man for office.
		He's already shown us what he would do:
		he should never be near the Oval Office again.</p>
	</blockquote>

	<p>One important aspect to remember is that this is not a partisan issue, a Republicans vs Democrats issue.
	Both sides traditionally hold respectable values.
	In this particular election, the part of the electorate which holds traditional conservative values
	is deprived of the possibility to vote for a decent conservative candidate,
	who would respect the Rule of Law and uphold the democratic principles.</p>
	HTML;


$page->parent('duverger_syndrome.html');
$page->parent('duverger_syndrome_lack_choice.html');
$page->template("stub");
$page->body($div_introduction);

$page->body($div_Primaries);
$page->body($div_He_is_not_the_lesser_of_two_evils);

$page->body('duverger_syndrome_political_realignments.html');
