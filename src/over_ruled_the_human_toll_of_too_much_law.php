<?php
$page = new BookPage();
$page->h1("Over Ruled: The Human Toll of Too Much Law");
$page->tags("Book", "Neil Gorsuch", "Janie Nitze", "Institutions: Legislative", "lawmaking", "Size of Government");
$page->keywords("Over Ruled: The Human Toll of Too Much Law");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>"Over Ruled: The Human Toll of Too Much Law" is a book by ${'Neil Gorsuch'} and Janie Nitze.</p>
	HTML;




$div_Publisher_Over_Ruled_The_Human_Toll_of_Too_Much_Law = new WebsiteContentSection();
$div_Publisher_Over_Ruled_The_Human_Toll_of_Too_Much_Law->setTitleText("Publisher: Over Ruled: The Human Toll of Too Much Law ");
$div_Publisher_Over_Ruled_The_Human_Toll_of_Too_Much_Law->setTitleLink("https://www.harpercollins.com/products/over-ruled-neil-gorsuchjanie-nitze");
$div_Publisher_Over_Ruled_The_Human_Toll_of_Too_Much_Law->content = <<<HTML
	<p>America has always been a nation of laws. But today our laws have grown so vast and reach so deeply into our lives that it’s worth asking: In our reverence for law, have we gone too far?
	Over just the last few decades, laws in this nation have exploded in number; they are increasingly complex; and the punishments they carry are increasingly severe. Some of these laws come from our elected representatives, but many now come from agency officials largely insulated from democratic accountability.

	In Over Ruled, Neil Gorsuch and Janie Nitze explore these developments and the human toll so much law can carry for ordinary Americans. At its heart, this is a book of stories—about fishermen in Florida, families in Montana, monks in Louisiana, a young Internet entrepreneur in Massachusetts, and many others who have found themselves trapped unex­pectedly in a legal maze.

	Some law is essential to our lives and our freedoms. But too much law can place those very same freedoms at risk and even undermine respect for law itself. And often those who feel the cost most acutely are those without wealth, power, and status.

	Deeply researched and superbly written, Over Ruled is one of the most significant books of the year. It is a must-read for every citizen concerned about the erosion of our constitutional system, and its insights will be key to the preservation of our liberties for generations to come.</p>
	HTML;



$page->parent('list_of_books.html');
$page->template("stub");
$page->body($div_introduction);
$page->body($div_Publisher_Over_Ruled_The_Human_Toll_of_Too_Much_Law);
$page->body('national_constitution_center_a_conversation_with_justice_neil_gorsuch_on_the_human_toll_of_too_much_law.html');
