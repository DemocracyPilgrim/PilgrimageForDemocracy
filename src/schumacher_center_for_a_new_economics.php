<?php
$page = new Page();
$page->h1("Schumacher Center for a New Economics");
$page->keywords("Schumacher Center for a New Economics");
$page->tags("Organisation");
$page->stars(0);

//$page->snp("description", "");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;



$div_Schumacher_Center_for_a_New_Economics_website = new WebsiteContentSection();
$div_Schumacher_Center_for_a_New_Economics_website->setTitleText("Schumacher Center for a New Economics website ");
$div_Schumacher_Center_for_a_New_Economics_website->setTitleLink("https://centerforneweconomics.org/");
$div_Schumacher_Center_for_a_New_Economics_website->content = <<<HTML
	<p>Our mission is to envision a just and regenerative economy; apply the concepts locally;
	then share the results for broad replication.</p>
	HTML;



$div_wikipedia_Schumacher_Center_for_a_New_Economics = new WikipediaContentSection();
$div_wikipedia_Schumacher_Center_for_a_New_Economics->setTitleText("Schumacher Center for a New Economics");
$div_wikipedia_Schumacher_Center_for_a_New_Economics->setTitleLink("https://en.wikipedia.org/wiki/Schumacher_Center_for_a_New_Economics");
$div_wikipedia_Schumacher_Center_for_a_New_Economics->content = <<<HTML
	<p>The Schumacher Center for a New Economics (formerly the E. F. Schumacher Society)
	is a tax exempt nonprofit organization based in Great Barrington, Massachusetts.</p>

	<p>The Schumacher Center promotes the 'new economy', which includes the concepts buy local, local currency and self-sufficiency.
	The Schumacher Center aims to combine theoretical research with practical application at the local, regional, national, and international levels.
	Further, the use of transformative systems and clear communication are part of its principles.</p>
	HTML;


$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);

$page->body('ernst_friedrich_schumacher.html');
$page->body('small_is_beautiful.html');

$page->body($div_Schumacher_Center_for_a_New_Economics_website);
$page->body($div_wikipedia_Schumacher_Center_for_a_New_Economics);
