<?php
$page = new Page();
$page->h1("Election denialism");
$page->tags("Elections", "USA");
$page->keywords("Election Denialism", "Election Denialism");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;


$list_Election_Denialism = ListOfPeoplePages::WithTags("Election Denialism");
$print_list_Election_Denialism = $list_Election_Denialism->print();

$div_list_Election_Denialism = new ContentSection();
$div_list_Election_Denialism->content = <<<HTML
	<h3>Related content</h3>

	$print_list_Election_Denialism
	HTML;



$div_wikipedia_Election_denial_movement_in_the_United_States = new WikipediaContentSection();
$div_wikipedia_Election_denial_movement_in_the_United_States->setTitleText("Election denial movement in the United States");
$div_wikipedia_Election_denial_movement_in_the_United_States->setTitleLink("https://en.wikipedia.org/wiki/Election_denial_movement_in_the_United_States");
$div_wikipedia_Election_denial_movement_in_the_United_States->content = <<<HTML
	<p>The election denial movement in the United States is a widespread false belief among many Republicans
	that elections in the United States are rigged and stolen through election fraud by Democrats.
	Adherents of the movement are referred to as election deniers.
	Election fraud conspiracy theories have spread online and through conservative conferences, community events, and door-to-door canvassing.
	Since the 2020 United States presidential election, many Republican politicians have sought elective office
	or taken legislative steps to address what they assert is weak election integrity leading to widespread fraudulent elections,
	though no evidence of systemic election fraud has come to light and many studies have found that it is extremely rare.</p>
	HTML;


$page->parent('elections.html');
$page->template("stub");
$page->body($div_introduction);

$page->body($div_list_Election_Denialism);

$page->body($div_wikipedia_Election_denial_movement_in_the_United_States);
