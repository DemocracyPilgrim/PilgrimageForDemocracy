<?php
$page = new Page();
$page->h1('Corporate Crime and Punishment: The Crisis of Underenforcement');
$page->tags("Book");
$page->stars(0);

//$page->snp('description', '');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>"Corporate Crime and Punishment: The Crisis of Underenforcement" is a book by John C. Coffee.
	</p>
	HTML;

$div_wikipedia_John_C_Coffee = new WikipediaContentSection();
$div_wikipedia_John_C_Coffee->setTitleText('John C Coffee');
$div_wikipedia_John_C_Coffee->setTitleLink('https://en.wikipedia.org/wiki/John_C._Coffee');
$div_wikipedia_John_C_Coffee->content = <<<HTML
	<p>John C. Coffee Jr. is a Professor of Law and director of the Center on Corporate Governance at Columbia Law School.</p>
	HTML;


$page->parent('list_of_books.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_wikipedia_John_C_Coffee);
