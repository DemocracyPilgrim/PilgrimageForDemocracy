<?php
$page = new Page();
$page->h1("Wealth");
$page->viewport_background("");
$page->keywords("Wealth", "wealth", "wealthy");
$page->stars(0);
$page->tags("Living: Fair Income", "Economic Injustice", "Economy", "Taxes");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Wealth = new WikipediaContentSection();
$div_wikipedia_Wealth->setTitleText("Wealth");
$div_wikipedia_Wealth->setTitleLink("https://en.wikipedia.org/wiki/Wealth");
$div_wikipedia_Wealth->content = <<<HTML
	<p>Wealth is the abundance of valuable financial assets or physical possessions which can be converted into a form that can be used for transactions. This includes the core meaning as held in the originating Old English word weal, which is from an Indo-European word stem. The modern concept of wealth is of significance in all areas of economics, and clearly so for growth economics and development economics, yet the meaning of wealth is context-dependent.</p>
	HTML;


$page->parent('economic_injustice.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Wealth");
$page->body($div_wikipedia_Wealth);
