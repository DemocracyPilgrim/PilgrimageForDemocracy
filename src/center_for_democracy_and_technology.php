<?php
$page = new OrganisationPage();
$page->h1("Center for Democracy and Technology");
$page->tags("Organisation", "Technology and Democracy", "Information");
$page->keywords("Center for Democracy and Technology");
$page->stars(1);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;



$div_Center_for_Democracy_and_Technology = new WebsiteContentSection();
$div_Center_for_Democracy_and_Technology->setTitleText("Center for Democracy and Technology ");
$div_Center_for_Democracy_and_Technology->setTitleLink("https://cdt.org/");
$div_Center_for_Democracy_and_Technology->content = <<<HTML
	<p>The Center for Democracy & Technology (CDT) is the leading nonpartisan, nonprofit organization fighting to advance civil rights and civil liberties in the digital age.</p>

	<p>We shape technology policy, governance, and design with a focus on equity and democratic values. Established in 1994, CDT has been a trusted advocate for digital rights since the earliest days of the internet.</p>
	HTML;



$div_wikipedia_Center_for_Democracy_and_Technology = new WikipediaContentSection();
$div_wikipedia_Center_for_Democracy_and_Technology->setTitleText("Center for Democracy and Technology");
$div_wikipedia_Center_for_Democracy_and_Technology->setTitleLink("https://en.wikipedia.org/wiki/Center_for_Democracy_and_Technology");
$div_wikipedia_Center_for_Democracy_and_Technology->content = <<<HTML
	<p>Center for Democracy & Technology (CDT) is a Washington, D.C.-based 501(c)(3) nonprofit organisation that advocates for digital rights and freedom of expression. CDT seeks to promote legislation that enables individuals to use the internet for purposes of well-intent, while at the same time reducing its potential for harm. It advocates for transparency, accountability, and limiting the collection of personal information.</p>
	HTML;


$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_Center_for_Democracy_and_Technology);

$page->related_tag("Center for Democracy and Technology");
$page->body($div_wikipedia_Center_for_Democracy_and_Technology);
