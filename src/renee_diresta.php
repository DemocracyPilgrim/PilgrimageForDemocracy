<?php
$page = new Page();
$page->h1("Renée DiResta");
$page->alpha_sort("DiResta, Renée");
$page->tags("Person", "Information: Social Networks", "Disinformation", "Social Networks");
$page->keywords("Renée DiResta");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Renée DiResta is a writer and former research manager at ${'Stanford Internet Observatory'}.</p>

	<p>She is the author of ${"Invisible Rulers: The People Who Turn Lies into Reality"}.</p>
	HTML;




$div_Renee_DiResta = new WebsiteContentSection();
$div_Renee_DiResta->setTitleText("Renée DiResta ");
$div_Renee_DiResta->setTitleLink("https://www.reneediresta.com/");
$div_Renee_DiResta->content = <<<HTML
	<p>Disinformation. Spam and Scams. Trolls.
	I study online manipulation, and what we can do about it.</p>

	<p>I'm Renee, and I study adversarial abuse online – ways that people attempt to manipulate, harass, or target others within the constantly evolving landscape of digital platforms. Sometimes this involves tracking state actors running influence operations with the intent of increasing their political power, or tearing a rival's society apart. Sometimes it's investigating spam and scams...which also aim to influence people, though the manipulators are usually motivated by money rather than ideology. And sometimes my work involves studying child safety issues – including, increasingly, their intersection with generative AI.</p>

	<p>The internet is an ecosystem, and these challenges are interconnected: new technologies transform old problems.</p>

	<p>I'm interested in exploring not only how problems manifest, but what we can do about them. And so, I write about content moderation; not only explaining how it currently works, but envisioning how it might better maximize free expression while minimizing abuse. I write about design: our tools shape how we interact with each other, and we should create them to serve us well. And finally, I translate research into policy suggestions – it's critical to ensure sure that powerful companies remain accountable.</p>

	<p>Everyone should understand how our tech ecosystem works, the impact that it has on society, and the role we each play within it. To that end, I try to write accessible (and entertaining!) explainers and books to help make sense of it all.</p>
	HTML;



$div_wikipedia_Renee_DiResta = new WikipediaContentSection();
$div_wikipedia_Renee_DiResta->setTitleText("Renée DiResta");
$div_wikipedia_Renee_DiResta->setTitleLink("https://en.wikipedia.org/wiki/Renée_DiResta");
$div_wikipedia_Renee_DiResta->content = <<<HTML
	<p>Renée DiResta (born 1981) is a writer and former research manager at Stanford Internet Observatory (SIO).
	DiResta has written about pseudoscience, conspiracy theories, terrorism, and state-sponsored information warfare.
	She has also served as an advisor to the U.S. Congress on ongoing efforts to prevent online and social media disinformation.</p>
	HTML;


$page->parent('list_of_people.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_Renee_DiResta);
$page->body($div_wikipedia_Renee_DiResta);
