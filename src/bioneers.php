<?php
$page = new Page();
$page->h1('Bioneers');
$page->keywords('Bioneers');
$page->tags("Organisation");
$page->stars(0);

//$page->snp('description', '');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );

$r1 = $page->ref('https://bioneers.org/about/purpose/', 'Bionners: About purpose');


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;



$div_Bioneers_website = new WebsiteContentSection();
$div_Bioneers_website->setTitleText("Bioneers website");
$div_Bioneers_website->setTitleLink("https://bioneers.org/");
$div_Bioneers_website->content = <<<HTML
	<p>Bioneers is inspiring and realizing a shift to live on Earth in ways that honor the web of life, each other and future generations.</p>

	<p>Bioneers is an innovative nonprofit organization that highlights breakthrough solutions for restoring people and planet.
	Founded in 1990 in Santa Fe, New Mexico by social entrepreneurs Kenny Ausubel and Nina Simons,
	we act as a fertile hub of social and scientific innovators with practical and visionary solutions
	for the world’s most pressing environmental and social challenges.</p>

	<p>A celebration of the genius of nature and human ingenuity, Bioneers connects people with solutions and each other.
	Our acclaimed annual national conference and local Bioneers Network events are complemented by extensive media production
	including a vibrant online media presence, award-winning radio and podcast series, book series,
	and role in third-party media projects such as Leonardo DiCaprio’s movie The 11th Hour
	and Michael Pollan’s best-selling book The Omnivore’s Dilemma.</p>

	<p>Our dynamic programs and initiatives focus on game-changing initiatives
	related to Restorative Food Systems, Biomimicry, Rights of Nature, Indigeneity, Women’s Leadership and Youth Leadership. $r1</p>
	HTML;



$div_youtube_Bioneers = new YoutubeContentSection();
$div_youtube_Bioneers->setTitleText("Bioneers");
$div_youtube_Bioneers->setTitleLink("https://www.youtube.com/@bioneers/featured");




$div_wikipedia_Bioneers = new WikipediaContentSection();
$div_wikipedia_Bioneers->setTitleText('Bioneers');
$div_wikipedia_Bioneers->setTitleLink('https://en.wikipedia.org/wiki/Bioneers');
$div_wikipedia_Bioneers->content = <<<HTML
	<p>Bioneers is a nonprofit environmental advocacy organization based in New Mexico and California.
	Founded in 1990 their philosophy recognizes and cultivates the value and wisdom of the natural world,
	emphasizing that responses to problems must be in harmony with the design of natural systems.</p>
	HTML;


$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_Bioneers_website);
$page->body($div_youtube_Bioneers);
$page->body($div_wikipedia_Bioneers);
