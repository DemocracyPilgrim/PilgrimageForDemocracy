<?php
$page = new Page();
$page->h1("Chi Huang (黃 紀)");
$page->alpha_sort("Huang, Chi");
$page->tags("Person", "Elections", "Taiwan");
$page->keywords("Chi Huang");
$page->stars(0);

$page->snp("description", "Taiwanese political science professor");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );

$r1 = $page->ref('https://cses.org/collect-data/', 'CSES Collaborator Credits');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Chi Huang (黃 紀) is a $Taiwanese political science professor from the ${'National Chengchi University'}.</p>

	<p>Huang worked with the ${'Comparative Study of Electoral Systems'} and contributed CSES data from $Taiwan for module 1 to 5. $r1</p>
	HTML;


$page->parent('list_of_people.html');
$page->template("stub");
$page->body($div_introduction);
