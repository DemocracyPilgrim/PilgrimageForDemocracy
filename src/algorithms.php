<?php
$page = new Page();
$page->h1("Algorithms");
$page->viewport_background('/free/algorithms.png');
$page->keywords("algorithms", "Algorithms");
$page->stars(2);
$page->tags("Information: Social Networks", "Technology and Democracy", "The Ugly Web", "Technology");


$page->snp("description", "Social Media algorithms: the fuel for the Ugly Web.");
$page->snp("image",       "/free/algorithms.1200-630.png");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref('', '');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<h3>Social Media Algorithms: Amplifying Division and Undermining Democracy</h3>

	<p>Social media platforms, once envisioned as tools for connection and empowerment,
	are increasingly being recognized as vehicles for division, disinformation, and even societal decay.
	At the heart of this issue lie the algorithms that govern what users see, and how content is prioritized.
	These algorithms, often designed to maximize engagement, can inadvertently amplify the worst aspects of human behavior,
	posing significant threats to democracy, social justice, and social cohesion.</p>

	<p>One of the most alarming consequences is the amplification of disinformation and hate speech.
	Algorithms often prioritize sensational and inflammatory content, regardless of its accuracy, as it tends to generate more engagement.
	This creates echo chambers where misinformation spreads rapidly,
	reinforcing biased views and making it harder for users to distinguish truth from falsehood.
	Hate speech also thrives in this environment, as algorithms can inadvertently elevate bigoted content,
	exposing more vulnerable people to abuse and promoting social division.</p>

	<p>Furthermore, algorithms can contribute to radicalization.
	By constantly feeding users content that aligns with their existing views, they create filter bubbles that limit exposure to diverse perspectives.
	Over time, this can push individuals towards more extreme positions, making them susceptible to extremist ideologies and conspiracy theories.
	This "algorithmic radicalization" poses a grave threat to social stability and democratic discourse.</p>

	<p>The adverse effects of these algorithmic choices are particularly harmful to democracy.
	By creating a climate of distrust, misinformation, and polarization,
	social media algorithms undermine the foundations of healthy democratic debate and civic participation.
	The ability to reason collectively based on shared facts is severely compromised, and the process of informed decision-making is greatly damaged.</p>

	<p>The impact extends to social justice and cohesion.
	The very same dynamics that amplify hate speech and radicalization also marginalize and silence vulnerable populations.
	Algorithmic bias can reinforce existing inequalities, making it harder for marginalized communities to have their voices heard and their needs addressed.</p>

	<p>The need to enforce strong regulation on social media algorithms is now more urgent than ever.
	Governments must work to hold platforms accountable for the harmful consequences of their algorithms.</p>

	<p>This regulation should focus on:</p>

	<ul>
	<li><strong>Transparency:</strong>
	Mandating social media platforms to disclose how their algorithms work and the parameters they use to prioritize content.</li>

	<li><strong>Accountability:</strong>
	Holding platforms responsible for the amplification of disinformation and hate speech.</li>

	<li><strong>User Control:</strong>
	Giving users more control over the content they see and the data they share.</li>
	</ul>

	<p>In addition to regulation, there is a growing call for open-source algorithms,
	allowing independent researchers and experts to scrutinize their code, identify potential biases, and offer solutions for improvement.
	By opening up the "black box" of social media algorithms,
	we can work together to create a digital environment that is more transparent, ethical, and conducive to democratic values.</p>

	<p>The current state of social media algorithms presents a clear and present danger to our societies.
	It is time to take action to reclaim control of our digital spaces,
	fostering an online environment that serves the common good and promotes truth, justice, and social cohesion.
	This will require bold action from governments, social media platforms, and individuals alike.</p>
	HTML;

$div_wikipedia_Regulation_of_algorithms = new WikipediaContentSection();
$div_wikipedia_Regulation_of_algorithms->setTitleText("Regulation of algorithms");
$div_wikipedia_Regulation_of_algorithms->setTitleLink("https://en.wikipedia.org/wiki/Regulation_of_algorithms");
$div_wikipedia_Regulation_of_algorithms->content = <<<HTML
	<p>Regulation of algorithms, or algorithmic regulation, is the creation of laws, rules and public sector policies
	for promotion and regulation of algorithms, particularly in artificial intelligence and machine learning.</p>
	HTML;



$div_wikipedia_Regulation_of_artificial_intelligence = new WikipediaContentSection();
$div_wikipedia_Regulation_of_artificial_intelligence->setTitleText("Regulation of artificial intelligence");
$div_wikipedia_Regulation_of_artificial_intelligence->setTitleLink("https://en.wikipedia.org/wiki/Regulation_of_artificial_intelligence");
$div_wikipedia_Regulation_of_artificial_intelligence->content = <<<HTML
	<p>The regulation of artificial intelligence refers to the development of public sector policies and laws
	for promoting and regulating artificial intelligence (AI).
	It is part of the broader regulation of algorithms.
	The regulatory and policy landscape for AI is an emerging issue in jurisdictions worldwide,
	including for international organizations without direct enforcement power like the IEEE or the OECD.</p>
	HTML;




$div_wikipedia_Algorithmic_curation = new WikipediaContentSection();
$div_wikipedia_Algorithmic_curation->setTitleText("Algorithmic curation");
$div_wikipedia_Algorithmic_curation->setTitleLink("https://en.wikipedia.org/wiki/Algorithmic_curation");
$div_wikipedia_Algorithmic_curation->content = <<<HTML
	<p>Algorithmic curation is the selection of online media by recommendation algorithms and personalized searches. Examples include search engine and social media products such as the Twitter feed, Facebook's News Feed, and the Google Personalized Search.
	Curation algorithms are typically proprietary or "black box", leading to concern about algorithmic bias and the creation of filter bubbles.</p>
	HTML;



$div_wikipedia_Algorithmic_radicalization = new WikipediaContentSection();
$div_wikipedia_Algorithmic_radicalization->setTitleText("Algorithmic radicalization");
$div_wikipedia_Algorithmic_radicalization->setTitleLink("https://en.wikipedia.org/wiki/Algorithmic_radicalization");
$div_wikipedia_Algorithmic_radicalization->content = <<<HTML
	<p>Algorithmic radicalization is the concept that recommender algorithms on popular social media sites such as YouTube and Facebook drive users toward progressively more extreme content over time, leading to them developing radicalized extremist political views. Algorithms record user interactions, from likes/dislikes to amount of time spent on posts, to generate endless media aimed to keep users engaged. Through echo chamber channels, the consumer is driven to be more polarized through preferences in media and self-confirmation.
	Algorithmic radicalization remains a controversial phenomenon as it is often not in the best interest of social media companies to remove echo chamber channels. To what extent recommender algorithms are actually responsible for radicalization remains controversial; studies have found contradictory results as to whether algorithms have promoted extremist content.</p>
	HTML;




$page->parent('web.html');
$page->body($div_introduction);
$page->related_tag("Algorithms");


$page->body($div_wikipedia_Regulation_of_algorithms);
$page->body($div_wikipedia_Regulation_of_artificial_intelligence);
$page->body($div_wikipedia_Algorithmic_curation);
$page->body($div_wikipedia_Algorithmic_radicalization);
