<?php
$page = new Page();
$page->h1("Brian Tyler Cohen");
$page->alpha_sort("Cohen, Brian Tyler");
$page->tags("Person", "USA", "Information: Media", "Donald Trump");
$page->keywords("Brian Tyler Cohen", "BTC");
$page->stars(0);
$page->viewport_background("");

$page->snp("description", "American political commentator.");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Brian Tyler Cohen is an $American political commentator.
	He is a strong advocate for $democracy and he regularly debunks $MAGA talking points.</p>

	<p>He is the author of the book: ${"Shameless: Republicans' Deliberate Dysfunction and the Battle to Preserve Democracy"}.</p>

	<p>His YouTube show includes the following regular programs:</p>

	<ul>
		<li>The Legal Breakdown with ${'Glenn Kirschner'}.</li>
		<li>Inside the Right with Tim Miller.</li>
		<li>Democracy Watch with ${'Marc Elias'}.</li>
	</ul>
	HTML;


$div_youtube_Brian_Tyler_Cohen = new YoutubeContentSection();
$div_youtube_Brian_Tyler_Cohen->setTitleText("Brian Tyler Cohen");
$div_youtube_Brian_Tyler_Cohen->setTitleLink("https://www.youtube.com/channel/UCQANb2YPwAtK-IQJrLaaUFw");
$div_youtube_Brian_Tyler_Cohen->content = <<<HTML
	<p>Brian Tyler Cohen is one of the most viewed independent progressive political hosts on YouTube.
	In just a few minutes a day, Brian's videos will keep you informed on what's going on, why it's important,
	and how it affects you-- always in a way that you can understand.</p>
	HTML;



$div_youtube_LIVE_Obama_s_speechwriter_issues_MUST_SEE_Trump_takedown = new YoutubeContentSection();
$div_youtube_LIVE_Obama_s_speechwriter_issues_MUST_SEE_Trump_takedown->setTitleText("LIVE: Brian Tyler Cohen and Jon Favreau");
$div_youtube_LIVE_Obama_s_speechwriter_issues_MUST_SEE_Trump_takedown->setTitleLink("https://www.youtube.com/watch?v=xhsT4SD93_o");
$div_youtube_LIVE_Obama_s_speechwriter_issues_MUST_SEE_Trump_takedown->content = <<<HTML
	<p>31 Aug 2024:  Brian Tyler Cohen and Jon Favreau discuss Brian's new book, Shameless and how Republicans branding has been upended.
	Cohen tells how he gave up an acting career to become a pro-democracy Youtuber.</p>
	HTML;



$div_wikipedia_Brian_Tyler_Cohen = new WikipediaContentSection();
$div_wikipedia_Brian_Tyler_Cohen->setTitleText("Brian Tyler Cohen");
$div_wikipedia_Brian_Tyler_Cohen->setTitleLink("https://en.wikipedia.org/wiki/Brian_Tyler_Cohen");
$div_wikipedia_Brian_Tyler_Cohen->content = <<<HTML
	<p>Brian Tyler Cohen (born January 12, 1989) is an American progressive YouTuber, podcast host, author, political commentator, actor and MSNBC contributor.
	His political podcast is called No Lie with Brian Tyler Cohen.
	In his YouTube channel, titled "Brian Tyler Cohen," he interviews political figures, reports on politics,
	and live-streams events, including debates and election results.
	His channel has over 3 million subscribers and 2.3 billion views.</p>
	HTML;


$page->parent('list_of_people.html');
$page->template("stub");
$page->body($div_introduction);

$page->related_tag("Brian Tyler Cohen");

$page->body($div_youtube_Brian_Tyler_Cohen);
$page->body($div_youtube_LIVE_Obama_s_speechwriter_issues_MUST_SEE_Trump_takedown);


$page->body($div_wikipedia_Brian_Tyler_Cohen);
