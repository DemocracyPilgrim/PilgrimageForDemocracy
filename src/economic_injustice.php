<?php
$page = new Page();
$page->h1("Economic Injustice");
$page->tags("Living: Fair Income", "Injustice", "Economy", "Poverty");
$page->keywords("Economic Injustice");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Economic_inequality = new WikipediaContentSection();
$div_wikipedia_Economic_inequality->setTitleText("Economic inequality");
$div_wikipedia_Economic_inequality->setTitleLink("https://en.wikipedia.org/wiki/Economic_inequality");
$div_wikipedia_Economic_inequality->content = <<<HTML
	<p>Economic inequality is an umbrella term for a) income inequality or distribution of income (how the total sum of money paid to people is distributed among them), b) wealth inequality or distribution of wealth (how the total sum of wealth owned by people is distributed among the owners), and c) consumption inequality (how the total sum of money spent by people is distributed among the spenders). Each of these can be measured between two or more nations, within a single nation, or between and within sub-populations (such as within a low-income group, within a high-income group and between them, within an age group and between inter-generational groups, within a gender group and between them etc, either from one or from multiple nations).</p>
	HTML;


$page->parent('living.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Economic Injustice");
$page->body($div_wikipedia_Economic_inequality);
