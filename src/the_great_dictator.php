<?php
$page = new MoviePage();
$page->h1("The Great Dictator");
$page->tags("Movie", "Charlie Chaplin");
$page->keywords("The Great Dictator");
$page->stars(0);
$page->viewport_background("");

$page->snp("description", "1940 American movie with Charlie Chaplin");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>"The Great Dictator" is a 1940 movie with ${'Charlie Chaplin'}.</p>
	HTML;

$div_wikipedia_The_Great_Dictator = new WikipediaContentSection();
$div_wikipedia_The_Great_Dictator->setTitleText("The Great Dictator");
$div_wikipedia_The_Great_Dictator->setTitleLink("https://en.wikipedia.org/wiki/The_Great_Dictator");
$div_wikipedia_The_Great_Dictator->content = <<<HTML
	<p>The Great Dictator is a 1940 American anti-fascist, political satire, and black comedy film written, directed, produced, scored by, and starring British filmmaker Charlie Chaplin. Having been the only Hollywood filmmaker to continue to make silent films well into the period of sound films, Chaplin made this his first true sound film.</p>
	HTML;


$page->parent('list_of_movies.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("The Great Dictator");
$page->body('charlie_chaplin_final_speech_from_the_great_dictator.html');
$page->body($div_wikipedia_The_Great_Dictator);
