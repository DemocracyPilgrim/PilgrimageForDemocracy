<?php
$page = new Page();
$page->h1("Amnesty International");
$page->tags("Organisation", "Humanity", "Individual: Rights", "International");
$page->keywords("Amnesty International");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;



$div_Amnesty_International = new WebsiteContentSection();
$div_Amnesty_International->setTitleText("Amnesty International ");
$div_Amnesty_International->setTitleLink("https://www.amnesty.org/en/");
$div_Amnesty_International->content = <<<HTML
	<p>Amnesty International is a global movement of more than 10 million people who are committed to creating a future where human rights are enjoyed by everyone.
	United by our shared humanity, we know that the power to create positive change is within all of us.</p>

	<p>We are funded by members and people like you.
	We are independent of any political ideology, economic interest or religion.
	We stand with victims of human rights violations whoever they are, wherever they are.</p>

	<p>No government is beyond scrutiny. We uncover the truth. We hold human rights violators to account.</p>
	HTML;



$div_wikipedia_Amnesty_International = new WikipediaContentSection();
$div_wikipedia_Amnesty_International->setTitleText("Amnesty International");
$div_wikipedia_Amnesty_International->setTitleLink("https://en.wikipedia.org/wiki/Amnesty_International");
$div_wikipedia_Amnesty_International->content = <<<HTML
	<p>Amnesty International (also referred to as Amnesty or AI) is an international non-governmental organization focused on human rights,
	with its headquarters in the United Kingdom.
	The organization says it has more than ten million members and supporters around the world.
	The stated mission of the organization is to campaign for "a world in which every person enjoys all of the human rights
	enshrined in the Universal Declaration of Human Rights and other international human rights instruments."
	The organization has played a notable role on human rights issues due to its frequent citation in media and by world leaders.</p>
	HTML;


$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_Amnesty_International);
$page->body($div_wikipedia_Amnesty_International);
