<?php
$page = new Page();
$page->h1("Campaign finance");
$page->tags("Elections", "Electoral System");
$page->keywords("campaign finance");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Campaign finance is different from ${'funding for elections'}.</p>
	HTML;

$div_wikipedia_Campaign_finance = new WikipediaContentSection();
$div_wikipedia_Campaign_finance->setTitleText("Campaign finance");
$div_wikipedia_Campaign_finance->setTitleLink("https://en.wikipedia.org/wiki/Campaign_finance");
$div_wikipedia_Campaign_finance->content = <<<HTML
	<p>Campaign finance, also known as election finance, political donations or political finance,
	refers to the funds raised to promote candidates, political parties, or policy initiatives and referendums.
	Donors and recipients include individuals, corporations, political parties, and charitable organizations.</p>
	HTML;

$div_wikipedia_Campaign_finance_in_the_United_States = new WikipediaContentSection();
$div_wikipedia_Campaign_finance_in_the_United_States->setTitleText("Campaign finance in the United States");
$div_wikipedia_Campaign_finance_in_the_United_States->setTitleLink("https://en.wikipedia.org/wiki/Campaign_finance_in_the_United_States");
$div_wikipedia_Campaign_finance_in_the_United_States->content = <<<HTML
	<p>The financing of electoral campaigns in the United States happens at the federal, state, and local levels
	by contributions from individuals, corporations, political action committees, and sometimes the government.
	Campaign spending has risen steadily at least since 1990.
	For example, a candidate who won an election to the U.S. House of Representatives in 1990 spent on average $407,600 (equivalent to $950,000 in 2023),
	while the winner in 2022 spent on average $2.79 million;
	in the Senate, average spending for winning candidates went from $3.87 million (equivalent to $9.03 million in 2023) to $26.53 million.</p>
	HTML;


$page->parent('elections.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_wikipedia_Campaign_finance);
$page->body($div_wikipedia_Campaign_finance_in_the_United_States);
