<?php
$page = new PersonPage();
$page->h1("Lawrence O'Donnell");
$page->alpha_sort("O'Donnell, Lawrence");
$page->viewport_background("");
$page->keywords("Lawrence O'Donnell");
$page->stars(0);
$page->tags("Person", "Journalist", "USA");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Lawrence O'Donnell is an American television anchor on MSNBC.</p>
	HTML;

$div_wikipedia_Lawrence_O_Donnell = new WikipediaContentSection();
$div_wikipedia_Lawrence_O_Donnell->setTitleText("Lawrence O Donnell");
$div_wikipedia_Lawrence_O_Donnell->setTitleLink("https://en.wikipedia.org/wiki/Lawrence_O'Donnell");
$div_wikipedia_Lawrence_O_Donnell->content = <<<HTML
	<p>Lawrence Francis O'Donnell Jr. is an American television anchor, actor, author, screenwriter, liberal political commentator, and host of The Last Word with Lawrence O'Donnell, an MSNBC opinion and news program that airs on weeknights.</p>
	HTML;


$page->parent('list_of_people.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Lawrence O'Donnell");
$page->body($div_wikipedia_Lawrence_O_Donnell);
