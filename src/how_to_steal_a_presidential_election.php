<?php
$page = new Page();
$page->h1("How to Steal a Presidential Election");
$page->tags("Book", "USA", "Elections", "Electoral System", "Donald Trump");
$page->keywords("How to Steal a Presidential Election");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>"How to Steal a Presidential Election" is a book by ${'Lawrence Lessig'} and ${'Matthew Seligman'}.</p>
	HTML;

$div_wikipedia_How_to_Steal_a_Presidential_Election = new WikipediaContentSection();
$div_wikipedia_How_to_Steal_a_Presidential_Election->setTitleText("How to Steal a Presidential Election");
$div_wikipedia_How_to_Steal_a_Presidential_Election->setTitleLink("https://en.wikipedia.org/wiki/How_to_Steal_a_Presidential_Election");
$div_wikipedia_How_to_Steal_a_Presidential_Election->content = <<<HTML
	<p>How to Steal a Presidential Election is a 2024 non-fiction book written by Lawrence Lessig and Matthew Seligman
	which examines the various legal frameworks that the loser of a presidential election in the United States could use to assume office in spite of the election results.
	Cataloging several courses of action a losing presidential election candidate or members of their party could take in the aftermath of a contested election,
	the book also examines critical failures in the Electoral Count Act of 1887 (ECA) and the Electoral Count Reform Act of 2022 (ECRA).
	The book was inspired by the events of the 2020 United States presidential election and the subsequent attack on the Capitol.</p>
	HTML;


$page->parent('list_of_books.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_wikipedia_How_to_Steal_a_Presidential_Election);
