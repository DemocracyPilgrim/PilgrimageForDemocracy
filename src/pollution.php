<?php
$page = new Page();
$page->h1("Pollution");
//$page->alpha_sort("Pollution");
$page->tags("Society");
$page->keywords("Pollution");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;



$list_Pollution = ListOfPeoplePages::WithTags("Pollution");
$print_list_Pollution = $list_Pollution->print();

$div_list_Pollution = new ContentSection();
$div_list_Pollution->content = <<<HTML
	<h3>Related content</h3>

	$print_list_Pollution
	HTML;



$div_wikipedia_Pollution = new WikipediaContentSection();
$div_wikipedia_Pollution->setTitleText("Pollution");
$div_wikipedia_Pollution->setTitleLink("https://en.wikipedia.org/wiki/Pollution");
$div_wikipedia_Pollution->content = <<<HTML
	<p>Pollution is the introduction of contaminants into the natural environment that cause adverse change.
	Pollution can take the form of any substance (solid, liquid, or gas) or energy (such as radioactivity, heat, sound, or light).
	Pollutants, the components of pollution, can be either foreign substances/energies or naturally occurring contaminants.</p>

	<p>Although environmental pollution can be caused by natural events,
	the word pollution generally implies that the contaminants have an anthropogenic source – that is, a source created by human activities,
	such as manufacturing, extractive industries, poor waste management, transportation or agriculture.
	Pollution is often classed as point source (coming from a highly concentrated specific site, such as a factory, mine, construction site),
	or nonpoint source pollution (coming from a widespread distributed sources, such as microplastics or agricultural runoff).</p>
	HTML;


$page->parent('society.html');
$page->template("stub");
$page->body($div_introduction);

$page->body($div_list_Pollution);

$page->body($div_wikipedia_Pollution);
