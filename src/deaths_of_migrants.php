<?php
$page = new Page();
$page->h1('Deaths of migrants');
$page->tags("Humanity", "Immigration", "International");
$page->stars(1);

$page->snp('description', 'Seeking security, countless migrants find death...');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p>In their desperate efforts to find safety, countless migrants only find death
	on the journey to the safer, better life they aspired to...</p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>In their desperate efforts to find safety, countless $migrants only find death
	on the journey to the safer, better life they aspired to...</p>

	<p>It is an indication of the dire circumstances that they face at home,
	that migrants are willing to face uncertain odds and undertake a perilous journey,
	putting their own lives, and often the lives of their children and loved ones on the line.</p>

	<p>The phenomenon is not new.
	The history of humanity is punctuated by periods of forced migration.
	In recent history, many still remember the plight of the "<em>Vietnamese boat people</em>"
	who, at the end of the $Vietnam war, tried to find safety by attempting a perilous journey on small row boats across the ocean
	towards a dreamed of sanctuary.
	According to the ${'United Nations High Commissioner for Refugees'}, between 200,000 and 400,000 boat people unfortunately died at sea.</p>

	<p>In current times, also according to the $UNHCR, thousands of people die in the Mediterranean Sea
	in their attempts to reach Europe.</p>

	<p>Migrants face all kinds of dangers in order to flee from danger and from harsh living conditions.
	Many die on the way to the promised land.</p>
	HTML;


$div_Migrant_deaths_and_disappearances = new WebsiteContentSection();
$div_Migrant_deaths_and_disappearances->setTitleText("MMP: Migrant deaths and disappearances");
$div_Migrant_deaths_and_disappearances->setTitleLink("https://www.migrationdataportal.org/themes/migrant-deaths-and-disappearances");
$div_Migrant_deaths_and_disappearances->content = <<<HTML
	<p>${'Missing Migrants Project'}: Since 2014, more than 4,000 fatalities have been recorded annually on migratory routes worldwide.
	The number of deaths recorded, however, represent only a minimum estimate because the majority of migrant deaths around the world go unrecorded.
	These data not only highlight the issue of migrant fatalities and the consequences for families left behind,
	but can also be used to assess the risks of irregular migration and to design policies and programmes to make migration safer.</p>
	HTML;



$div_codeberg_Migrants_number_and_cause_of_death = new CodebergContentSection();
$div_codeberg_Migrants_number_and_cause_of_death->setTitleText('Migrants: number and cause of death');
$div_codeberg_Migrants_number_and_cause_of_death->setTitleLink('https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy/issues/46');
$div_codeberg_Migrants_number_and_cause_of_death->content = <<<HTML
	<p>What estimates exist about the number of migrants who die on the way to their promised land?
	What are the most common causes of death?</p>
	HTML;


$div_wikipedia_Vietnamese_boat_people = new WikipediaContentSection();
$div_wikipedia_Vietnamese_boat_people->setTitleText('Vietnamese boat people');
$div_wikipedia_Vietnamese_boat_people->setTitleLink('https://en.wikipedia.org/wiki/Vietnamese_boat_people');
$div_wikipedia_Vietnamese_boat_people->content = <<<HTML
	<p>Vietnamese boat people were refugees who fled Vietnam by boat and ship following the end of the Vietnam War in 1975.
	This migration and humanitarian crisis was at its highest in 1978 and 1979, but continued into the early 1990s.
	The number of boat people leaving Vietnam and arriving safely in another country totaled almost 800,000 between 1975 and 1995.
	Many of the refugees failed to survive the passage, facing danger from pirates, over-crowded boats, and storms.
	According to the United Nations High Commission for Refugees, between 200,000 and 400,000 boat people died at sea.</p>
	HTML;


$div_wikipedia_List_of_migrant_vessel_incidents_on_the_Mediterranean_Sea = new WikipediaContentSection();
$div_wikipedia_List_of_migrant_vessel_incidents_on_the_Mediterranean_Sea->setTitleText("List of migrant vessel incidents on the Mediterranean Sea");
$div_wikipedia_List_of_migrant_vessel_incidents_on_the_Mediterranean_Sea->setTitleLink("https://en.wikipedia.org/wiki/List_of_migrant_vessel_incidents_on_the_Mediterranean_Sea");
$div_wikipedia_List_of_migrant_vessel_incidents_on_the_Mediterranean_Sea->content = <<<HTML
	<p>Over 26,000 people have died or have been declared missing while in migration across the Mediterranean Sea since 2014.
	The Deaths at the Border project at the University of Amsterdam
	estimates that 3,188 people died while trying to reach Europe between 1990 and 2013.</p>
	HTML;



$page->parent('immigration.html');
$page->template("stub");

$page->body($div_introduction);
$page->body($div_Migrant_deaths_and_disappearances);

$page->body($div_codeberg_Migrants_number_and_cause_of_death);
$page->body('missing_migrants_project.html');


$page->body($div_wikipedia_Vietnamese_boat_people);
$page->body($div_wikipedia_List_of_migrant_vessel_incidents_on_the_Mediterranean_Sea);
