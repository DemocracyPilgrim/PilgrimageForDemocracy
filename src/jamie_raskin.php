<?php
$page = new Page();
$page->h1("Jamie Raskin");
$page->alpha_sort("Raskin, Jamie");
$page->tags("Person", "Institutions: Judiciary", "USA", "Lawyer");
$page->keywords("Jamie Raskin");
$page->stars(0);

$page->snp("description", "U.S. representative for Maryland's 8th congressional district");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref('', '');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Jamie Raskin is the U.S. representative for Maryland's 8th congressional district.</p>

	<p>Raskin was one of seven Democrats appointed to the United States House Select Committee on the January 6 Attack.
	He appears in the related documentary ${"The Sixth"}.</p>

	<p>Raskin is a law professor. He appears on many videos, explaining the $American $constitution.</p>
	HTML;

$div_wikipedia_Jamie_Raskin = new WikipediaContentSection();
$div_wikipedia_Jamie_Raskin->setTitleText("Jamie Raskin");
$div_wikipedia_Jamie_Raskin->setTitleLink("https://en.wikipedia.org/wiki/Jamie_Raskin");
$div_wikipedia_Jamie_Raskin->content = <<<HTML
	<p>Jamin Ben Raskin (born December 13, 1962), better known as Jamie Raskin, is an American attorney, law professor, and politician
	serving as the U.S. representative for Maryland's 8th congressional district since 2017.
	A member of the Democratic Party, he served in the Maryland State Senate from 2007 to 2016.</p>
	HTML;


$page->parent('list_of_people.html');
$page->parent('united_states.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_wikipedia_Jamie_Raskin);
