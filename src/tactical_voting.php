<?php
$page = new Page();
$page->h1("Tactical voting");
$page->keywords("Tactical voting");
$page->tags("Elections");
$page->stars(0);

//$page->snp("description", "");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Strategic_voting = new WikipediaContentSection();
$div_wikipedia_Strategic_voting->setTitleText("Strategic voting");
$div_wikipedia_Strategic_voting->setTitleLink("https://en.wikipedia.org/wiki/Strategic_voting");
$div_wikipedia_Strategic_voting->content = <<<HTML
	<p>Strategic voting, also called tactical voting, sophisticated voting or insincere voting,
	occurs in voting systems when a voter votes for a candidate or party other than their sincere preference to prevent an undesirable outcome.
	For example, in a simple plurality election, a voter might gain a better outcome by voting for a less preferred but more generally popular candidate.</p>
	HTML;


$page->parent('elections.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_wikipedia_Strategic_voting);
