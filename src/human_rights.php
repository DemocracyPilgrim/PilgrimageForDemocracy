<?php
$page = new Page();
$page->h1("Human rights");
$page->keywords("Human rights", "human rights", "Human Rights");
$page->tags("Humanity", "Rights", "Individual: Rights");
$page->stars(0);

//$page->snp("description", "");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Human_rights = new WikipediaContentSection();
$div_wikipedia_Human_rights->setTitleText("Human rights");
$div_wikipedia_Human_rights->setTitleLink("https://en.wikipedia.org/wiki/Human_rights");
$div_wikipedia_Human_rights->content = <<<HTML
	<p>Human rights are moral principles or norms for certain standards of human behaviour and are regularly protected in municipal and international law.
	They are commonly understood as inalienable, fundamental rights "to which a person is inherently entitled simply because she or he is a human being"
	and which are "inherent in all human beings", regardless of their age, ethnic origin, location, language, religion, ethnicity, or any other status.
	They are applicable everywhere and at every time in the sense of being universal, and they are egalitarian in the sense of being the same for everyone.
	They are regarded as requiring empathy and the rule of law and imposing an obligation on persons to respect the human rights of others,
	and it is generally considered that they should not be taken away except as a result of due process based on specific circumstances.</p>
	HTML;


$page->parent('social_justice.html');
$page->template("stub");
$page->body($div_introduction);

$page->body('united_nations_human_rights_office.html');

$page->related_tag("Human Rights");

$page->body($div_wikipedia_Human_rights);
