<?php
$page = new Page();
$page->h1("Prequel: An American Fight Against Fascism");
$page->tags("USA", "Book", "Donald Trump");
$page->stars(0);

//$page->snp("description", "");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;



$div_Rachel_Maddow_Prequel = new WebsiteContentSection();
$div_Rachel_Maddow_Prequel->setTitleText("Rachel Maddow: Prequel");
$div_Rachel_Maddow_Prequel->setTitleLink("https://www.rachelmaddow.com/prequel-by-rachel-maddow/");
$div_Rachel_Maddow_Prequel->content = <<<HTML
	<p>Inspired by her research for the hit podcast Ultra, Rachel Maddow charts the rise of a wild American strain of authoritarianism
	that has been alive on the far-right edge of our politics for the better part of a century.</p>
	HTML;



$div_youtube_Extended_interviews_Rachel_Maddow_Anderson_Cooper_and_Tom_Brokaw = new YoutubeContentSection();
$div_youtube_Extended_interviews_Rachel_Maddow_Anderson_Cooper_and_Tom_Brokaw->setTitleText("CBS: Extended interviews: Rachel Maddow, Anderson Cooper and Tom Brokaw");
$div_youtube_Extended_interviews_Rachel_Maddow_Anderson_Cooper_and_Tom_Brokaw->setTitleLink("https://www.youtube.com/watch?v=8wBgXezIixQ&ab_channel=CBSSundayMorning");
$div_youtube_Extended_interviews_Rachel_Maddow_Anderson_Cooper_and_Tom_Brokaw->content = <<<HTML
	HTML;


$div_youtube_Weve_Been_Here_Before_Americas_Fight_Against_Fascism_With_Rachel_Maddow = new YoutubeContentSection();
$div_youtube_Weve_Been_Here_Before_Americas_Fight_Against_Fascism_With_Rachel_Maddow->setTitleText("Michael Steel: We've Been Here Before– America's Fight Against Fascism: With Rachel Maddow");
$div_youtube_Weve_Been_Here_Before_Americas_Fight_Against_Fascism_With_Rachel_Maddow->setTitleLink("https://www.youtube.com/watch?v=12ceIcrzHz8&ab_channel=MichaelSteeleNetwork");
$div_youtube_Weve_Been_Here_Before_Americas_Fight_Against_Fascism_With_Rachel_Maddow->content = <<<HTML
	HTML;



$div_youtube_Rachel_Maddow_in_Conversation_with_Kathleen_Belew = new YoutubeContentSection();
$div_youtube_Rachel_Maddow_in_Conversation_with_Kathleen_Belew->setTitleText("Chicago Humanities Festival: Rachel Maddow in Conversation with Kathleen Belew");
$div_youtube_Rachel_Maddow_in_Conversation_with_Kathleen_Belew->setTitleLink("https://www.youtube.com/watch?v=wvyzRCIWfVI&ab_channel=ChicagoHumanitiesFestival");
$div_youtube_Rachel_Maddow_in_Conversation_with_Kathleen_Belew->content = <<<HTML
	HTML;


$div_wikipedia_Rachel_Maddow = new WikipediaContentSection();
$div_wikipedia_Rachel_Maddow->setTitleText("Rachel Maddow");
$div_wikipedia_Rachel_Maddow->setTitleLink("https://en.wikipedia.org/wiki/Rachel_Maddow");
$div_wikipedia_Rachel_Maddow->content = <<<HTML
	<p>Rachel Anne Maddow is an American television news program host and political commentator.</p>

	<p>Maddow's fourth book, Prequel: An American Fight Against Fascism was published on October 17, 2023.
	It is based on her podcast Ultra.</p>
	HTML;


$page->parent('list_of_books.html');
$page->template("stub");
$page->body($div_introduction);

$page->body($div_Rachel_Maddow_Prequel);
$page->body('american_fascism.html');
$page->body('a_night_at_the_garden.html');

$page->body($div_youtube_Rachel_Maddow_in_Conversation_with_Kathleen_Belew);
$page->body($div_youtube_Weve_Been_Here_Before_Americas_Fight_Against_Fascism_With_Rachel_Maddow);
$page->body($div_youtube_Extended_interviews_Rachel_Maddow_Anderson_Cooper_and_Tom_Brokaw);

$page->body($div_wikipedia_Rachel_Maddow);
