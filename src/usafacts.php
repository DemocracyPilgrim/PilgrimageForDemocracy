<?php
$page = new OrganisationPage();
$page->h1("USAFacts");
$page->tags("Organisation", "Steve Ballmer", "Information: Facts");
$page->keywords("USAFacts");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>USAFacts is a not-for-profit organization founded by ${'Steve Ballmer'}.</p>
	HTML;



$div_USAFacts_website = new WebsiteContentSection();
$div_USAFacts_website->setTitleText("USAFacts website ");
$div_USAFacts_website->setTitleLink("https://usafacts.org/");
$div_USAFacts_website->content = <<<HTML
	<p>Researchers. Analysts. Statisticians. Designers. No politicians. No one at USAFacts is trying to convince you of anything. The only opinion we have is that government data should be easier to access. Our entire mission is to provide you with facts about the United States that are rooted in data. We believe once you have the solid, unbiased numbers behind the issues you can make up your own mind.</p>
	HTML;



$div_wikipedia_USAFacts = new WikipediaContentSection();
$div_wikipedia_USAFacts->setTitleText("USAFacts");
$div_wikipedia_USAFacts->setTitleLink("https://en.wikipedia.org/wiki/USAFacts");
$div_wikipedia_USAFacts->content = <<<HTML
	<p>USAFacts is a not-for-profit organization and website that provides data and reports on the United States population,
	its government's finances, and government's impact on society.
	It was launched in 2017.</p>
	HTML;


$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);



$page->body($div_USAFacts_website);
$page->related_tag("USAFacts");
$page->body($div_wikipedia_USAFacts);
