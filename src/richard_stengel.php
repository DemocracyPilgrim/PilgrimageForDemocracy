<?php
$page = new Page();
$page->h1("Richard Stengel");
$page->alpha_sort("Stengel, Richard");
$page->tags("Person");
$page->keywords("Richard Stengel");
$page->stars(0);

$page->snp("description", "American author, editor and former government official.");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref('', '');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Richard Stengel is an $American author, editor and former government official.</p>

	<p>Among other books, he wrote a biography of ${'Nelson Mandela'}, "${'Long Walk to Freedom'}",
	in collaboration with Mandela himself,
	and "${'Information Wars: How We Lost the Global Battle Against Disinformation'} and What to Do About It",
	recounting his time in the State Department countering $Russian disinformation and ISIS $propaganda.
	</p>
	HTML;

$div_wikipedia_Richard_Stengel = new WikipediaContentSection();
$div_wikipedia_Richard_Stengel->setTitleText("Richard Stengel");
$div_wikipedia_Richard_Stengel->setTitleLink("https://en.wikipedia.org/wiki/Richard_Stengel");
$div_wikipedia_Richard_Stengel->content = <<<HTML
	<p>Richard Allen Stengel (born May 2, 1955) is an American editor, author, and former government official.
	He was Time magazine's 16th managing editor from 2006 to 2013.
	He was also chief executive of the National Constitution Center from 2004 to 2006,
	and served as President Obama's Under Secretary of State for Public Diplomacy and Public Affairs from 2014 to 2016.
	Stengel has written a number of books, including a collaboration with Nelson Mandela on Mandela's autobiography, Long Walk to Freedom.</p>
	HTML;


$page->parent('list_of_people.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_wikipedia_Richard_Stengel);
