<?php
$page = new Page();
$page->h1("Industry self-regulation");
$page->keywords("self-regulation");
$page->stars(0);

//$page->snp("description", "");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Industry_self_regulation = new WikipediaContentSection();
$div_wikipedia_Industry_self_regulation->setTitleText("Industry self regulation");
$div_wikipedia_Industry_self_regulation->setTitleLink("https://en.wikipedia.org/wiki/Industry_self-regulation");
$div_wikipedia_Industry_self_regulation->content = <<<HTML
	<p>Industry self-regulation is the process whereby members of an industry, trade or sector of the economy
	monitor their own adherence to legal, ethical, or safety standards,
	rather than have an outside, independent agency such as a third party entity or governmental regulator monitor and enforce those standards.
	Self-regulation may ease compliance and ownership of standards, but it can also give rise to conflicts of interest.</p>
	HTML;


$page->parent('institutions.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_wikipedia_Industry_self_regulation);
