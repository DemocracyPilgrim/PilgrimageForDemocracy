<?php
$page = new Page();
$page->h1("Lawyers");
$page->tags("Institutions: Judiciary");
$page->keywords("Lawyers", "Lawyer", "lawyer", "lawyers");
$page->stars(0);
$page->viewport_background("/free/lawyers.png");

$page->snp("description", "Upholding the law: safeguarding independence and integrity of the legal profession.");
$page->snp("image",       "/free/lawyers.1200-630.png");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Lawyers play a crucial role in safeguarding democracy and upholding the principles of justice.
	They serve as advocates for individuals and communities, ensuring that everyone has access to legal representation and that the rule of law is upheld.
	However, around the world, lawyers face numerous challenges and threats, including intimidation, harassment, and violence,
	often stemming from their efforts to defend human rights and expose corruption.
	The legal profession faces critical issues like access to justice for marginalized communities,
	the erosion of judicial independence, and the growing influence of powerful interests on legal systems.
	The fight for a just and equitable legal system demands a commitment to supporting lawyers,
	ensuring their safety, and promoting the independence and integrity of the legal profession.</p>
	HTML;

$list_Lawyer = ListOfPeoplePages::WithTags("Lawyer");
$print_list_Lawyer = $list_Lawyer->print();

$div_list_Lawyer = new ContentSection();
$div_list_Lawyer->content = <<<HTML
	<h3>Lawyers</h3>

	$print_list_Lawyer
	HTML;



$div_wikipedia_Lawyer = new WikipediaContentSection();
$div_wikipedia_Lawyer->setTitleText("Lawyer");
$div_wikipedia_Lawyer->setTitleLink("https://en.wikipedia.org/wiki/Lawyer");
$div_wikipedia_Lawyer->content = <<<HTML
	<p>A lawyer is a person who practices law.
	The role of a lawyer varies greatly, across different legal jurisdictions.
	A lawyer can be classified as an advocate, attorney, barrister, canon lawyer, civil law notary, counsel, solicitor, legal executive, and public servant
	— with each role having different functions and privileges.</p>
	HTML;


$page->parent('justice.html');
$page->template("stub");
$page->body($div_introduction);

$page->body($div_list_Lawyer);

$page->body($div_wikipedia_Lawyer);
