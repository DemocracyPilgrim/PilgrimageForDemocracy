<?php
$page = new Page();
$page->h1("At War with Ourselves: My Tour of Duty in the Trump White House");
$page->viewport_background("/free/at_war_with_ourselves.png");
$page->tags("Book", "USA", "Donald Trump", "Military");
$page->keywords("At War with Ourselves");
$page->stars(1);

$page->snp("description", "Memoir by national security adviser shows how Trump harmed U.S. foreign policy.");
$page->snp("image",       "/free/at_war_with_ourselves.1200-630.png");

$page->preview( <<<HTML
	<p></p>
	HTML );

$r1 = $page->ref("https://www.nytimes.com/2024/08/27/books/review/at-war-with-ourselves-hr-mcmaster.html", "H.R. McMaster Doesn’t Think Donald Trump Is Very Good at Making Deals");
$r1 = $page->ref("https://edition.cnn.com/2024/08/25/politics/mcmaster-trump-book-account", "Gen. McMaster’s blistering account of the Trump White House");
$r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>"At War with Ourselves: My Tour of Duty in the Trump White House" is a memoir by Lt. Gen. H.R. McMaster,
	who acted as ${'Donald Trump'}'s national security adviser.
	He shows how the former president’s insecurities and weaknesses harmed U.S. foreign policy.</p>
	HTML;



$div_HarperCollins_At_War_with_Ourselves = new WebsiteContentSection();
$div_HarperCollins_At_War_with_Ourselves->setTitleText("HarperCollins At War with Ourselves ");
$div_HarperCollins_At_War_with_Ourselves->setTitleLink("https://www.harpercollins.com/products/at-war-with-ourselves-h-r-mcmaster");
$div_HarperCollins_At_War_with_Ourselves->content = <<<HTML
	<p><strong>A revealing account of National Security Advisor H.R. McMaster’s turbulent and consequential thirteen months in the Trump White House.</strong></p>

	<p>At War with Ourselves is the story of helping a disruptive President drive necessary shifts in U.S. foreign policy at a critical moment in history.
	McMaster entered an administration beset by conflict and the hyper partisanship of American politics.
	With the candor of a soldier and the perspective of a historian, McMaster rises above the fray
	to lay bare the good, the bad, and the ugly of Trump’s presidency and give readers insight into what a second Trump term would look like.</p>

	<p>While all administrations are subject to backstabbing and infighting,
	some of Trump’s more unscrupulous political advisors were determined to undermine McMaster and others to advance their narrow agendas.
	McMaster writes candidly about Cabinet officials who, deeply disturbed by Trump’s language and behavior,
	prioritized controlling the President over collaborating to provide the President with options.</p>

	<p>McMaster offers a frank and fresh assessment of the achievements and failures of his tenure as National Security Advisor
	and the challenging task of maintaining one’s bearings and focus on the mission in a hectic and malicious environment.</p>

	<p>Determined to transcend the war within the administration and focus on national security priorities,
	McMaster forged coalitions in Washington and internationally to help Trump advance U.S. interests.
	Trump’s character and personality helped him make tough decisions, but sometimes prevented him from sticking to them.
	McMaster adroitly assesses the record of Trump’s presidency in comparison to the Obama and Biden administrations.</p>

	<p>With the 2024 election on the horizon, At War with Ourselves highlights the crucial importance of competence in foreign policy,
	and makes plain the need for leaders who possess the character and intellect to guide the United States in a tumultuous world.</p>
	HTML;



$div_wikipedia_H_R_McMaster = new WikipediaContentSection();
$div_wikipedia_H_R_McMaster->setTitleText("H R McMaster");
$div_wikipedia_H_R_McMaster->setTitleLink("https://en.wikipedia.org/wiki/H._R._McMaster");
$div_wikipedia_H_R_McMaster->content = <<<HTML
	<p>Herbert Raymond McMaster (born 1962) is a retired United States Army lieutenant general
	who served as the 25th United States National Security Advisor from 2017 to 2018.
	He is also known for his roles in the Gulf War, Operation Enduring Freedom, and Operation Iraqi Freedom.</p>

	<p>In February 2017, McMaster succeeded Michael Flynn as President Donald Trump's National Security Advisor.
	He remained on active duty as a lieutenant general while serving as National Security Advisor, and retired in May 2018.
	McMaster resigned as National Security Advisor on March 22, 2018</p>
	HTML;


$page->parent('list_of_books.html');
$page->template("stub");
$page->body($div_introduction);
$page->body($div_HarperCollins_At_War_with_Ourselves);


$page->body($div_wikipedia_H_R_McMaster);
