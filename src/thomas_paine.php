<?php
$page = new Page();
$page->h1("Thomas Paine");
$page->alpha_sort("Paine, Thomas");
$page->tags("Person");
$page->keywords("Thomas Paine");
$page->stars(0);

//$page->snp("description", "");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref('', '');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;


$div_Quotes = new ContentSection();
$div_Quotes->content = <<<HTML
	<h3>Quotes</h3>

	<blockquote>
	Moderation in temper is always a virtue; but moderation in principle is always a vice.
	</blockquote>

	<blockquote>
	…the individuals themselves, each in his own personal and sovereign right,
	entered into a compact with each other to produce a government:
	and this is the only mode in which governments have a right to arise,
	and the only principle on which they have a right to exist.”
	</blockquote>

	<blockquote>
	The strength and power of despotism consists wholly in the fear of resistance.
	</blockquote>
	HTML;


$div_wikipedia_Thomas_Paine = new WikipediaContentSection();
$div_wikipedia_Thomas_Paine->setTitleText("Thomas Paine");
$div_wikipedia_Thomas_Paine->setTitleLink("https://en.wikipedia.org/wiki/Thomas_Paine");
$div_wikipedia_Thomas_Paine->content = <<<HTML
	<p>Thomas Paine (1737 – 1809) was an English-born American Founding Father, French Revolutionary,
	political activist, philosopher, political theorist, and revolutionary.
	He authored Common Sense (1776) and The American Crisis (1776–1783),
	two of the most influential pamphlets at the start of the American Revolution,
	and he helped to inspire the colonial era patriots in 1776 to declare independence from Great Britain.
	His ideas reflected Enlightenment-era ideals of human rights.</p>
	HTML;


$page->parent('list_of_people.html');
$page->template("stub");
$page->body($div_introduction);

$page->body($div_Quotes);

$page->body($div_wikipedia_Thomas_Paine);
