<?php
$page = new Page();
$page->h1("0: Dangers");
$page->tags("Topic portal");
$page->keywords("Dangers", "Danger", "danger", "dangers", "threats", "threat", "anti-democratic");
$page->stars(0);
$page->viewport_background("/free/dangers.png");

$page->snp("description", "Threats to democracy and social justice in all their forms.");
$page->snp("image",       "/free/dangers.1200-630.png");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;


$page->parent('invoking_democracy.html');
$page->next("individuals.html");
$page->template("stub");
$page->body($div_introduction);



$page->related_tag(array("Dangers", "Danger"));
