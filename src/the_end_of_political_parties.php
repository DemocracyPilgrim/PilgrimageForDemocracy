<?php
$page = new Page();
$page->h1("The end of political parties");
$page->tags("Elections: Solution", "Electoral System");
$page->stars(0);

//$page->snp("description", "");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p>Placeholder</p>
	HTML );

// $r1 = $page->ref('', '');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>The current role of political parties is tightly bound to the use of ${'plurality voting'} as a ${'voting method'}.
	By adopting a better voting method, the role of political parties with necessarily change.
	We can imagine the changes in the political landscape, and what type of structures and civil society organisations
	would replace political parties as they exist today.</p>
	HTML;


$page->parent('voting_methods.html');
$page->template("stub");
$page->body($div_introduction);

$page->body('duverger_syndrome_party_politics.html');
