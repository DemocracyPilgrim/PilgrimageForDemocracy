<?php
$page = new DocumentaryPage();
$page->h1("From Russia With Lev");
$page->tags("Documentary", "Rachel Maddow", "Donald Trump", "Volodymyr Zelenskyy", "Ukraine", "Russia", "USA", "Lev Parnas");
$page->keywords("From Russia With Lev");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>"From Russia With Lev" is a 2024 $documentary film about ${'Lev Parnas'} produced by ${'Rachel Maddow'}.</p>
	HTML;

$div_wikipedia_From_Russia_With_Lev = new WikipediaContentSection();
$div_wikipedia_From_Russia_With_Lev->setTitleText("From Russia With Lev");
$div_wikipedia_From_Russia_With_Lev->setTitleLink("https://en.wikipedia.org/wiki/From_Russia_With_Lev");
$div_wikipedia_From_Russia_With_Lev->content = <<<HTML
	<p>From Russia With Lev is a 2024 documentary film about Lev Parnas.
	It is the first documentary produced by cable news anchor Rachel Maddow.</p>
	HTML;


$page->parent('list_of_documentaries.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("From Russia With Lev");
$page->body($div_wikipedia_From_Russia_With_Lev);
