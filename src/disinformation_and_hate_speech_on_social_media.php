<?php
$page = new Page();
$page->h1("Disinformation and hate speech on social media");
$page->keywords("disinformation and hate speech on social media");
$page->viewport_background("");
$page->stars(0);
$page->tags("The Ugly Web", "Disinformation", "Hate speech", "Freedom of Speech", "Social Networks");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>The ${'Integrity Institute'} published a report showing that social media amplifies misinformation more than information.</p>
	HTML;



$div_EU_deal_will_push_Google_Facebook_others_to_police_platforms_against_hate = new WebsiteContentSection();
$div_EU_deal_will_push_Google_Facebook_others_to_police_platforms_against_hate->setTitleText("EU deal will push Google, Facebook, others to police platforms against hate");
$div_EU_deal_will_push_Google_Facebook_others_to_police_platforms_against_hate->setTitleLink("https://www.pbs.org/newshour/world/eu-deal-will-push-google-facebook-others-to-police-platforms-against-hate-speech-disinformation");
$div_EU_deal_will_push_Google_Facebook_others_to_police_platforms_against_hate->content = <<<HTML
	<p>
	</p>
	HTML;



$div_Facebook_s_system_approved_dehumanizing_hate_speech = new WebsiteContentSection();
$div_Facebook_s_system_approved_dehumanizing_hate_speech->setTitleText("Facebook’s system approved dehumanizing hate speech");
$div_Facebook_s_system_approved_dehumanizing_hate_speech->setTitleLink("https://www.pbs.org/newshour/world/facebooks-system-approved-dehumanizing-hate-speech");
$div_Facebook_s_system_approved_dehumanizing_hate_speech->content = <<<HTML
	<p>
	</p>
	HTML;




$div_Amnesty_report_finds_Facebook_amplified_hate_ahead_of_Rohingya_massacre_in = new WebsiteContentSection();
$div_Amnesty_report_finds_Facebook_amplified_hate_ahead_of_Rohingya_massacre_in->setTitleText("Amnesty report finds Facebook amplified hate ahead of Rohingya massacre in");
$div_Amnesty_report_finds_Facebook_amplified_hate_ahead_of_Rohingya_massacre_in->setTitleLink("https://www.pbs.org/newshour/world/amnesty-report-finds-facebook-amplified-hate-ahead-of-rohingya-massacre-in-myanmar");
$div_Amnesty_report_finds_Facebook_amplified_hate_ahead_of_Rohingya_massacre_in->content = <<<HTML
	<p>
	</p>
	HTML;



$div_Facebook_Telegram_and_the_Ongoing_Struggle_Against_Online_Hate_Speech = new WebsiteContentSection();
$div_Facebook_Telegram_and_the_Ongoing_Struggle_Against_Online_Hate_Speech->setTitleText("Facebook, Telegram, and the Ongoing Struggle Against Online Hate Speech");
$div_Facebook_Telegram_and_the_Ongoing_Struggle_Against_Online_Hate_Speech->setTitleLink("https://carnegieendowment.org/research/2023/09/facebook-telegram-and-the-ongoing-struggle-against-online-hate-speech?lang=en");
$div_Facebook_Telegram_and_the_Ongoing_Struggle_Against_Online_Hate_Speech->content = <<<HTML
	<p>
	</p>
	HTML;



$div_Facebook_and_Genocide_How_Facebook_contributed_to_genocide_in_Myanmar_and_why = new WebsiteContentSection();
$div_Facebook_and_Genocide_How_Facebook_contributed_to_genocide_in_Myanmar_and_why->setTitleText("Facebook and Genocide: How Facebook contributed to genocide in Myanmar and why it will not be held accountable");
$div_Facebook_and_Genocide_How_Facebook_contributed_to_genocide_in_Myanmar_and_why->setTitleLink("https://systemicjustice.org/article/facebook-and-genocide-how-facebook-contributed-to-genocide-in-myanmar-and-why-it-will-not-be-held-accountable/");
$div_Facebook_and_Genocide_How_Facebook_contributed_to_genocide_in_Myanmar_and_why->content = <<<HTML
	<p>
	</p>
	HTML;



$div_Myanmar_Facebook_s_systems_promoted_violence_against_Rohingya_Meta_owes = new WebsiteContentSection();
$div_Myanmar_Facebook_s_systems_promoted_violence_against_Rohingya_Meta_owes->setTitleText("Myanmar: Facebook’s systems promoted violence against Rohingya; Meta owes");
$div_Myanmar_Facebook_s_systems_promoted_violence_against_Rohingya_Meta_owes->setTitleLink("https://www.amnesty.org/en/latest/news/2022/09/myanmar-facebooks-systems-promoted-violence-against-rohingya-meta-owes-reparations-new-report/");
$div_Myanmar_Facebook_s_systems_promoted_violence_against_Rohingya_Meta_owes->content = <<<HTML
	<p>
	</p>
	HTML;




$div_wikipedia_Online_hate_speech = new WikipediaContentSection();
$div_wikipedia_Online_hate_speech->setTitleText("Online hate speech");
$div_wikipedia_Online_hate_speech->setTitleLink("https://en.wikipedia.org/wiki/Online_hate_speech");
$div_wikipedia_Online_hate_speech->content = <<<HTML
	<p>Online hate speech is a type of speech that takes place online with the purpose of attacking a person or a group based on their race, religion, ethnic origin, sexual orientation, disability, and/or gender. Online hate speech is not easily defined, but can be recognized by the degrading or dehumanizing function it serves.</p>
	HTML;

$div_wikipedia_Social_media_use_by_Donald_Trump = new WikipediaContentSection();
$div_wikipedia_Social_media_use_by_Donald_Trump->setTitleText("Social media use by Donald Trump");
$div_wikipedia_Social_media_use_by_Donald_Trump->setTitleLink("https://en.wikipedia.org/wiki/Social_media_use_by_Donald_Trump");
$div_wikipedia_Social_media_use_by_Donald_Trump->content = <<<HTML
	<p>Donald Trump's use of social media attracted attention worldwide since he joined Twitter in May 2009. Over nearly twelve years, Trump tweeted around 57,000 times, including about 8,000 times during the 2016 election campaign and over 25,000 times during his presidency. The White House said the tweets should be considered official statements. When Twitter banned Trump from the platform in January 2021 during the final days of his term, his handle @realDonaldTrump had over 88.9 million followers. On November 19, 2022, Twitter's new owner, Elon Musk, reinstated his account.</p>
	HTML;

$page->parent('web.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Disinformation and hate speech on social media");

$page->body($div_EU_deal_will_push_Google_Facebook_others_to_police_platforms_against_hate);
$page->body($div_Facebook_s_system_approved_dehumanizing_hate_speech);
$page->body($div_Amnesty_report_finds_Facebook_amplified_hate_ahead_of_Rohingya_massacre_in);
$page->body($div_Facebook_Telegram_and_the_Ongoing_Struggle_Against_Online_Hate_Speech);
$page->body($div_Facebook_and_Genocide_How_Facebook_contributed_to_genocide_in_Myanmar_and_why);
$page->body($div_Myanmar_Facebook_s_systems_promoted_violence_against_Rohingya_Meta_owes);

$page->body($div_wikipedia_Online_hate_speech);
$page->body($div_wikipedia_Social_media_use_by_Donald_Trump);
