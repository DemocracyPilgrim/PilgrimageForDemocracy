<?php
$page = new Page();
$page->h1("Global Migration Data Analysis Centre");
$page->keywords("Global Migration Data Analysis Centre", 'GMDAC');
$page->tags("Organisation", "International", "Immigration");
$page->stars(1);

$page->snp("description", "A project of the International Organization for Migration.");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p>The Global Migration Data Analysis Centre is an initiative of the ${'International Organization for Migration'}
	to provide comprehensive data and analysis to inform migration governance, improve programming and promote a better public understanding of migration.</p>
	HTML );

$r1 = $page->ref('https://www.migrationdataportal.org/about', 'Welcome to the Migration Data Portal');


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>The Global Migration Data Analysis Centre is an initiative of the ${'International Organization for Migration'}
	to provide comprehensive data and analysis to inform migration governance, improve programming and promote a better public understanding of migration.</p>
	HTML;



$div_GMDAC_website = new WebsiteContentSection();
$div_GMDAC_website->setTitleText("GMDAC website ");
$div_GMDAC_website->setTitleLink("https://gmdac.iom.int/");
$div_GMDAC_website->content = <<<HTML
	<p>Established in September 2015, the International Organization for Migration’s Global Migration Data Analysis Centre
	was set up to respond to calls for better international migration data and analysis.
	Data are key to inform migration governance, improve programming and promote a better public understanding of migration.</p>
	HTML;


$div_Migration_Data_Portal = new ContentSection();
$div_Migration_Data_Portal->content = <<<HTML
	<h3>Migration Data Portal</h3>

	<p>The Portal was launched in December 2017 and is managed and developed by $IOM's Global Migration Data Analysis Centre (GMDAC).</p>
	HTML;


$div_website_Migration_Data_Portal = new WebsiteContentSection();
$div_website_Migration_Data_Portal->setTitleText("Migration Data Portal ");
$div_website_Migration_Data_Portal->setTitleLink("https://www.migrationdataportal.org/");
$div_website_Migration_Data_Portal->content = <<<HTML
	<p>The Portal is a unique access point to timely, comprehensive migration statistics and reliable information about migration data globally.
	The site is designed to help policy makers, national statistics officers, journalists and the general public interested in the field of migration
	to navigate the increasingly complex landscape of international migration data, currently scattered across different organisations and agencies.</p>

	<p>Especially in critical times, such as those faced today, it is essential to ensure that responses to migration are based on sound facts and accurate analysis.
	By making the evidence about migration issues accessible and easy to understand, the Portal aims to contribute to a more informed public debate.$r1</p>
	HTML;



$page->parent('international_organization_for_migration.html');
$page->template("stub");
$page->body($div_introduction);

$page->body($div_GMDAC_website);
$page->body('immigration.html');
$page->body('missing_migrants_project.html');

$page->body($div_Migration_Data_Portal);
$page->body($div_website_Migration_Data_Portal);
