<?php
$page = new Page();
$page->h1("Supreme Court of the United States (SCOTUS)");
$page->tags("Institutions: Judiciary", "Supreme Court", "USA");
$page->keywords("Supreme Court of the United States", "SCOTUS", "SCOTUS Landmark Decision");
$page->stars(1);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;


$list_SCOTUS_Landmark_Decisions = ListOfPeoplePages::WithTags("SCOTUS Landmark Decision");
$print_list_SCOTUS_Landmark_Decisions = $list_SCOTUS_Landmark_Decisions->print();

$div_list_SCOTUS_Landmark_Decisions = new ContentSection();
$div_list_SCOTUS_Landmark_Decisions->content = <<<HTML
	<h3>SCOTUS Landmark Decisions</h3>

	$print_list_SCOTUS_Landmark_Decisions
	HTML;


$list_SCOTUS = ListOfPeoplePages::WithTags("SCOTUS");
$print_list_SCOTUS = $list_SCOTUS->print();

$div_list_SCOTUS = new ContentSection();
$div_list_SCOTUS->content = <<<HTML
	<h3>Related content</h3>

	$print_list_SCOTUS
	HTML;



$div_wikipedia_Supreme_Court_of_the_United_States = new WikipediaContentSection();
$div_wikipedia_Supreme_Court_of_the_United_States->setTitleText("Supreme Court of the United States");
$div_wikipedia_Supreme_Court_of_the_United_States->setTitleLink("https://en.wikipedia.org/wiki/Supreme_Court_of_the_United_States");
$div_wikipedia_Supreme_Court_of_the_United_States->content = <<<HTML
	<p>The Supreme Court of the United States (SCOTUS) is the highest court in the federal judiciary of the United States.
	It has ultimate appellate jurisdiction over all U.S. federal court cases, and over state court cases that turn on questions of U.S. constitutional or federal law.
	It also has original jurisdiction over a narrow range of cases, specifically "all Cases affecting Ambassadors,
	other public Ministers and Consuls, and those in which a State shall be Party."
	The court holds the power of judicial review: the ability to invalidate a statute for violating a provision of the Constitution.
	It is also able to strike down presidential directives for violating either the Constitution or statutory law.</p>
	HTML;



$div_wikipedia_List_of_landmark_court_decisions_in_the_United_States = new WikipediaContentSection();
$div_wikipedia_List_of_landmark_court_decisions_in_the_United_States->setTitleText("List of landmark court decisions in the United States");
$div_wikipedia_List_of_landmark_court_decisions_in_the_United_States->setTitleLink("https://en.wikipedia.org/wiki/List_of_landmark_court_decisions_in_the_United_States");
$div_wikipedia_List_of_landmark_court_decisions_in_the_United_States->content = <<<HTML
	<p>The following landmark court decisions in the United States contains
	landmark court decisions which changed the interpretation of existing law in the United States.</p>
	HTML;



$page->parent('supreme_court.html');
$page->template("stub");
$page->body($div_introduction);

$page->body($div_list_SCOTUS_Landmark_Decisions);
$page->body($div_list_SCOTUS);

$page->body($div_wikipedia_Supreme_Court_of_the_United_States);
$page->body($div_wikipedia_List_of_landmark_court_decisions_in_the_United_States);
