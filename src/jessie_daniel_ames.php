<?php
$page = new PersonPage();
$page->h1("Jessie Daniel Ames");
$page->alpha_sort("Daniel Ames, Jessie");
$page->tags("Person", "American", "Suffragist", "Civil Rights", "Texas", "Slavery", "Elections", "Voting Rights");
$page->keywords("Jessie Daniel Ames");
$page->stars(0);
$page->viewport_background("/free/jessie_daniel_ames.png");

$page->snp("description", "American suffragist and civil rights leader");
$page->snp("image",       "/free/jessie_daniel_ames.1200-630.png");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Jessie_Daniel_Ames = new WikipediaContentSection();
$div_wikipedia_Jessie_Daniel_Ames->setTitleText("Jessie Daniel Ames");
$div_wikipedia_Jessie_Daniel_Ames->setTitleLink("https://en.wikipedia.org/wiki/Jessie_Daniel_Ames");
$div_wikipedia_Jessie_Daniel_Ames->content = <<<HTML
	<p>Jessie Daniel Ames (November 2, 1883 – February 21, 1972) was a suffragist and civil rights leader from Texas who helped create the anti-lynching movement in the American South. She was one of the first Southern white women to speak out and work publicly against lynching of African Americans, murders which white men claimed to commit in an effort to protect women's "virtue." Despite risks to her personal safety, Ames stood up to these men and led organized efforts by white women to protest lynchings. She gained 40,000 signatures of Southern white women to oppose lynching, helping change attitudes and bring about a decline in these murders in the 1930s and 1940s.</p>
	HTML;


$page->parent('list_of_people.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Jessie Daniel Ames");
$page->body($div_wikipedia_Jessie_Daniel_Ames);
