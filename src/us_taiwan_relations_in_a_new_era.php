<?php
$page = new Page();
$page->h1("U.S.-Taiwan Relations in a New Era: Responding to a More Assertive China");
$page->tags("Report", "USA", "International", "Taiwan", "People's Republic of China");
$page->keywords("U.S.-Taiwan Relations in a New Era");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<div class="download-pdf">
		<a href="https://live-tfr-cdn.cfr.org/cdn/ff/Ig_ZOuCbVzeNEasQrQ3Evyn2UdT3rn-2TreCp8i-BqE/1687531766/public/2023-06/TFR81_U.S.-TaiwanRelationsNewEra_SinglePages_2023-06-05_Online.pdf">
			<div class="download"><i class="outline file pdf white large icon"></i>Download</div>
		</a>
	</div>
	HTML;



$div_U_S_Taiwan_Relations_in_a_New_Era = new WebsiteContentSection();
$div_U_S_Taiwan_Relations_in_a_New_Era->setTitleText("U.S.-Taiwan Relations in a New Era");
$div_U_S_Taiwan_Relations_in_a_New_Era->setTitleLink("https://www.cfr.org/task-force-report/us-taiwan-relations-in-a-new-era");
$div_U_S_Taiwan_Relations_in_a_New_Era->content = <<<HTML
	<p>U.S. policy toward Taiwan needs to evolve to contend with a more capable, assertive, and risk-acceptant China
	that is increasingly dissatisfied with the status quo.</p>

	<p>A conflict between the United States and the People’s Republic of China (PRC, or China) over Taiwan is becoming increasingly imaginable,
	a result of China’s growing military capabilities and assertiveness, the emergence and coalescence of a separate Taiwanese identity,
	and evolving U.S. calculations about its interests at stake in the Taiwan Strait.
	If deterrence fails and a war erupts, the result would be calamitous for Taiwan, China, the United States, and the world,
	resulting in thousands of casualties on all sides and a profound global economic depression.</p>
	HTML;



$page->parent('list_of_research_and_reports.html');
$page->template("stub");
$page->body($div_introduction);

$page->body($div_U_S_Taiwan_Relations_in_a_New_Era);
