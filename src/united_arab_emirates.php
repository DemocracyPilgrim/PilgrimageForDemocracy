<?php
$page = new CountryPage('United Arab Emirates');
$page->h1('United Arab Emirates');
$page->tags("Country");
$page->keywords('United Arab Emirates', 'UAE');
$page->stars(0);

$page->snp('description', '9.2 million inhabitants.');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>The United Arab Emirates may join $BRICS.</p>
	HTML;

$div_wikipedia_United_Arab_Emirates = new WikipediaContentSection();
$div_wikipedia_United_Arab_Emirates->setTitleText('United Arab Emirates');
$div_wikipedia_United_Arab_Emirates->setTitleLink('https://en.wikipedia.org/wiki/United_Arab_Emirates');
$div_wikipedia_United_Arab_Emirates->content = <<<HTML
	<p>The United Arab Emirates is an elective monarchy formed from a federation of seven emirates,
	consisting of Abu Dhabi (the capital), Ajman, Dubai, Fujairah, Ras Al Khaimah, Sharjah and Umm Al Quwain.
	Each emirate is governed by a ruler and together the rulers form the Federal Supreme Council.</p>
	HTML;

$div_wikipedia_Human_rights_in_the_United_Arab_Emirates = new WikipediaContentSection();
$div_wikipedia_Human_rights_in_the_United_Arab_Emirates->setTitleText('Human rights in the United Arab Emirates');
$div_wikipedia_Human_rights_in_the_United_Arab_Emirates->setTitleLink('https://en.wikipedia.org/wiki/Human_rights_in_the_United_Arab_Emirates');
$div_wikipedia_Human_rights_in_the_United_Arab_Emirates->content = <<<HTML
	<p>According to human rights organisations, the government of the UAE violates a number of fundamental human rights.
	The UAE does not have democratically elected institutions and citizens do not have the right to change their government or to form political parties.
	Activists and academics who criticize the regime are detained and imprisoned, and their families are often harassed by the state security apparatus.</p>
	HTML;

$div_wikipedia_Politics_of_the_United_Arab_Emirates = new WikipediaContentSection();
$div_wikipedia_Politics_of_the_United_Arab_Emirates->setTitleText('Politics of the United Arab Emirates');
$div_wikipedia_Politics_of_the_United_Arab_Emirates->setTitleLink('https://en.wikipedia.org/wiki/Politics_of_the_United_Arab_Emirates');
$div_wikipedia_Politics_of_the_United_Arab_Emirates->content = <<<HTML
	<p>Politics of the United Arab Emirates take place in a framework of a federal presidential elective constitutional monarchy
	(a federation of absolute monarchies).</p>
	HTML;


$page->parent('world.html');
$page->template("stub");
$page->body($div_introduction);
$page->body('Country indices');

$page->body($div_wikipedia_United_Arab_Emirates);
$page->body($div_wikipedia_Human_rights_in_the_United_Arab_Emirates);
$page->body($div_wikipedia_Politics_of_the_United_Arab_Emirates);
