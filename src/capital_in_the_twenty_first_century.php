<?php
$page = new BookPage();
$page->h1('Capital in the Twenty-First Century');
$page->keywords('Capital in the Twenty-First Century');
$page->stars(0);
$page->tags("Book", "Fair Share");

$page->snp('description', 'A book by French economist Thomas Piketty.');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p>Capital in the Twenty-First Century is a book by French economist Thomas Piketty.</p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Capital in the Twenty-First Century is a book by French economist ${'Thomas Piketty'}.</p>

	<p>In the introduction of the book, Piketty writes:</p>

	<blockquote>"When the rate of return on capital exceeds the rate of growth of output and income,
	as it did in the nineteenth century and seems quite likely to do again in the twenty-first,
	capitalism automatically generates arbitrary and unsustainable inequalities that
	radically undermine the meritocratic values on which $democratic societies are based."</blockquote>
	HTML;

$div_wikipedia_Capital_in_the_Twenty_First_Century = new WikipediaContentSection();
$div_wikipedia_Capital_in_the_Twenty_First_Century->setTitleText('Capital in the Twenty First Century');
$div_wikipedia_Capital_in_the_Twenty_First_Century->setTitleLink('https://en.wikipedia.org/wiki/Capital_in_the_Twenty-First_Century');
$div_wikipedia_Capital_in_the_Twenty_First_Century->content = <<<HTML
	<p>Capital in the Twenty-First Century is a book written by French economist Thomas Piketty.
	It focuses on wealth and income inequality in Europe and the United States since the 18th century.
	It was first published in French in August 2013;
	an English translation by Arthur Goldhammer followed in April 2014.</p>

	<p>The book's central thesis is that when the rate of return on capital (r) is greater than the rate of economic growth (g) over the long term,
	the result is concentration of wealth, and this unequal distribution of wealth causes social and economic instability.
	Piketty proposes a global system of progressive wealth taxes to help reduce inequality
	and avoid the vast majority of wealth coming under the control of a tiny minority.</p>
	HTML;


$page->parent('list_of_books.html');
$page->template("stub");
$page->body($div_introduction);

$page->body('thomas_piketty.html');
$page->body('fair_share.html');


$page->body($div_wikipedia_Capital_in_the_Twenty_First_Century);
