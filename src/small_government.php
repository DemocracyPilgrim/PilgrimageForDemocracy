<?php
$page = new Page();
$page->h1('Small government');
$page->keywords('small government');
$page->stars(1);

$page->snp('description', 'What kind of small government do people want?');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p>What kind of small government do people want?</p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>In many $countries, notably in the ${'United States'}, the notion of "small government"
	regularly crops up during political debates.
	While we are rather in favour of a <em>naturally small government</em>,
	we must warn citizens and voters of every country of its ugly cousins:
	the <em>artificially small government</em>
	and the <em>nefariously small government</em>.</p>
	HTML;

$div_Naturally_small_government = new ContentSection();
$div_Naturally_small_government->content = <<<HTML
	<h3>Naturally small government</h3>

	<p>A naturally small government occurs when people in the society are evolved enough that there is little to no crime,
	that people behave civilly by default, that they take care of their families, neighbours, etc.
	in the name of mutual help, solidarity and compassion.
	Then there would be naturally little need for a government to keep society in order...</p>
	HTML;

$div_Artificially_small_government = new ContentSection();
$div_Artificially_small_government->content = <<<HTML
	<h3>Artificially small government</h3>

	<p>An artificially small government is what would get if people shrunk the government by force
	in the misguided notion that a smaller government would mean more individual freedoms and liberties.
	Such people might mean well, but they are ultimately misguided, most probably mislead by certain medias
	who keep selling them the idea of a small government.
	There are unfortunately very good reasons why we need such  gargantuan governments, and removing the guardrails prematurely might prove disastrous.</p>
	HTML;

$div_Nefariously_small_government = new ContentSection();
$div_Nefariously_small_government->content = <<<HTML
	<h3>Nefariously small government</h3>

	<p>A nefariously small government is what we would get if some ultra-conservative leaders and their enablers had their way.
	They want to get rid of the parts of the government that hinder them in their agenda, in order to facilitate their own power grab...
	The government is an obstacle to their criminal ambitions so they need to get rid of it.</p>
	HTML;

$page->parent('institutions.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_Naturally_small_government);
$page->body('saints_and_little_devils.html');

$page->body($div_Artificially_small_government);
$page->body($div_Nefariously_small_government);
