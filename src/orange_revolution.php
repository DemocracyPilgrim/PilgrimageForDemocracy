<?php
$page = new Page();
$page->h1("Orange Revolution");
$page->tags("International", "Ukraine");
$page->keywords("Orange Revolution");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Orange_Revolution = new WikipediaContentSection();
$div_wikipedia_Orange_Revolution->setTitleText("Orange Revolution");
$div_wikipedia_Orange_Revolution->setTitleLink("https://en.wikipedia.org/wiki/Orange_Revolution");
$div_wikipedia_Orange_Revolution->content = <<<HTML
	<p>The Orange Revolution was a series of protests and political events that took place in Ukraine from late November 2004 to January 2005,
	in the immediate aftermath of the run-off vote of the 2004 Ukrainian presidential election,
	which was claimed to be marred by massive corruption, voter intimidation and electoral fraud.</p>
	HTML;


$page->parent('ukraine.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Orange Revolution");
$page->body($div_wikipedia_Orange_Revolution);
