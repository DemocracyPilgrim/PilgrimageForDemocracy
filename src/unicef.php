<?php
$page = new Page();
$page->h1("UNICEF");
$page->keywords("UNICEF");
$page->tags("Organisation");
$page->stars(0);

//$page->snp("description", "");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;


$div_UNICEF_website = new WebsiteContentSection();
$div_UNICEF_website->setTitleText("UNICEF website");
$div_UNICEF_website->setTitleLink("https://www.unicef.org/");
$div_UNICEF_website->content = <<<HTML
	<p>UNICEF works in over 190 countries and territories to save children’s lives, to defend their rights,
	and to help them fulfil their potential, from early childhood through adolescence.
	And we never give up.</p>
	HTML;


$div_wikipedia_UNICEF = new WikipediaContentSection();
$div_wikipedia_UNICEF->setTitleText("UNICEF");
$div_wikipedia_UNICEF->setTitleLink("https://en.wikipedia.org/wiki/UNICEF");
$div_wikipedia_UNICEF->content = <<<HTML
	<p>UNICEF, originally called the United Nations International Children's Emergency Fund in full, now officially United Nations Children's Fund,
	is an agency of the United Nations responsible for providing humanitarian and developmental aid to children worldwide.
	The agency is among the most widespread and recognizable social welfare organizations in the world,
	with a presence in 192 countries and territories.
	UNICEF's activities include providing immunizations and disease prevention, administering treatment for children and mothers with HIV,
	enhancing childhood and maternal nutrition, improving sanitation, promoting education, and providing emergency relief in response to disasters.</p>
	HTML;


$page->parent('united_nations.html');
$page->template("stub");
$page->body($div_introduction);

$page->body($div_UNICEF_website);

$page->body($div_wikipedia_UNICEF);
