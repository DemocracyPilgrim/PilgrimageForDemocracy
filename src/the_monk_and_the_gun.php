<?php
$page = new Page();
$page->h1("The Monk and the Gun");
$page->tags("Movie", "Bhutan");
$page->keywords("The Monk and the Gun");
$page->stars(0);

$page->snp("description", "2023 Bhutanese movie");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;



$div_The_Monk_and_the_Gun_Review_Bhutan_Delivers_Another_Feel_Good_Mountain = new WebsiteContentSection();
$div_The_Monk_and_the_Gun_Review_Bhutan_Delivers_Another_Feel_Good_Mountain->setTitleText("‘The Monk and the Gun’ Review: Bhutan Delivers Another Feel-Good Mountain Escapade");
$div_The_Monk_and_the_Gun_Review_Bhutan_Delivers_Another_Feel_Good_Mountain->setTitleLink("https://www.thewrap.com/the-monk-and-the-gun-review-bhutan/");
$div_The_Monk_and_the_Gun_Review_Bhutan_Delivers_Another_Feel_Good_Mountain->content = <<<HTML
	<p>A winding, tangled parable about modern life coming to a people that don’t understand
	why they can’t keep doing things the way that’s always worked for them,
	“The Monk and the Gun” starts with a crazy premise and quietly gets sillier and wilder.
	But Dorji never loses his light touch, even as you can’t help but wonder what the lama has in mind with those guns.</p>
	HTML;



$div_wikipedia_The_Monk_and_the_Gun = new WikipediaContentSection();
$div_wikipedia_The_Monk_and_the_Gun->setTitleText("The Monk and the Gun");
$div_wikipedia_The_Monk_and_the_Gun->setTitleLink("https://en.wikipedia.org/wiki/The_Monk_and_the_Gun");
$div_wikipedia_The_Monk_and_the_Gun->content = <<<HTML
	<p>Set in 2006, the Kingdom of Bhutan is looking to become a $democracy,
	so looking to hold an election, government officials organize a mock election as a training exercise.
	In the town of Ura, an old lama orders a monk to get a pair of weapons to face the imminent change in the kingdom.</p>
	HTML;


$page->parent('list_of_movies.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_The_Monk_and_the_Gun_Review_Bhutan_Delivers_Another_Feel_Good_Mountain);
$page->body($div_wikipedia_The_Monk_and_the_Gun);
