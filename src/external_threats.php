<?php
require_once 'section/external_threats.php';

$page = new Page();
$page->h1('External Threats to Democracy');
$page->viewport_background('/free/external_threats.png');
$page->stars(0);
$page->tags("Danger", "International");

//$page->snp('description', "");
$page->snp('image', "/free/external_threats.1200-630.png");

$page->preview( <<<HTML
	<p>Established democracies are under attack from authoritarian regimes.</p>
	HTML );





$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Democracy is under attack:</p>
	<ul>
	<li>$Russia's war in $Ukraine</li>
	<li>$Cyberwarfare</li>
	<li>The ${"People's Republic of China"} against $Taiwan (Republic of China)</li>
	<li>${'Transnational repression'}</li>
	<li>Foreign interference in elections</li>
	<li>From within: greed, indifference and lack of solidarity</li>
	</ul>

	<p>Authoritarian regimes are using democratic $countries' very own weaknesses as a propaganda tool against $democracy itself.
	As part of this series, we shall see all the ways, obvious and not so obvious,
	that authoritarian regimes are turning the weaknesses of democracies against them.
	The best way to protect ourselves and to help democracy spread around the world is
	necessarily to be honest about the current shortcomings of today's democracies, and remedy the situation.</p>

	<p>$China is known to broadcast within its internal media truthful but carefully selected news about Western democracies
	in order to show the Chinese population how 'flawed' the concept of democracy is.
	For example, Donald Trump’s indictments have spurred incredulity and ridicule in China,
	and strengthened Chinese state narratives of the US in decline.</p>
	HTML;



$page->parent('dangers.html');
$page->template("stub");

$page->body($div_introduction);

external_threats_article_menu($page);
