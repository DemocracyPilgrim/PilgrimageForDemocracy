<?php
$page = new Page();
$page->h1("ProPublica");
$page->keywords("ProPublica");
$page->tags("Organisation");
$page->stars(0);

//$page->snp("description", "");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );

$r1 = $page->ref('https://www.propublica.org/atpropublica/propublica-wins-nellie-bly-award-for-supreme-court-coverage',
                 'ProPublica Wins Nellie Bly Award for Supreme Court Coverage');
$r2 = $page->ref('https://drive.google.com/file/d/14KhKt1cvd6OCmoUROO_MtxYochtWPlWm/view',
                 'ProPublica’s Supreme Court Investigation Team: Winner of the 2024 Nellie Bly Award for Investigative Reporting');


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>In 2024, ProPublica won the Nellie Bly Award for Investigative Reporting. $r1 $r2
	This award is bestowed by the ${'Museum of Political Corruption'} for distinguished reporting on $corruption.
	“ProPublica’s courageous reporting on the Supreme Court was a seismic wake-up call for the public.
	It forced a long overdue reckoning on the Court regarding its ethical standards,” said museum founder and president Bruce Roter.</p>
	HTML;



$div_ProPublica_website = new WebsiteContentSection();
$div_ProPublica_website->setTitleText("ProPublica website");
$div_ProPublica_website->setTitleLink("https://www.propublica.org/");
$div_ProPublica_website->content = <<<HTML
	<p>ProPublica is an independent, nonprofit newsroom that produces investigative journalism with moral force.
	We dig deep into important issues, shining a light on abuses of power and betrayals of public trust
	— and we stick with those issues as long as it takes to hold power to account.</p>
	HTML;




$div_wikipedia_ProPublica = new WikipediaContentSection();
$div_wikipedia_ProPublica->setTitleText("ProPublica");
$div_wikipedia_ProPublica->setTitleLink("https://en.wikipedia.org/wiki/ProPublica");
$div_wikipedia_ProPublica->content = <<<HTML
	<p>ProPublica is a nonprofit organization based in New York City dedicated to investigative journalism.
	ProPublica states that its investigations are conducted by its staff of full-time investigative reporters,
	and the resulting stories are distributed to news partners for publication or broadcast.</p>
	HTML;


$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_ProPublica_website);
$page->body($div_wikipedia_ProPublica);
