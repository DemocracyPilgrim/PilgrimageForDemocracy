<?php
$page = new PersonPage();
$page->h1("Jimmy Carter");
$page->alpha_sort("Jimmy Carter");
$page->tags("Person", "USA", "POTUS");
$page->keywords("Jimmy Carter");
$page->stars(0);
$page->viewport_background("/free/jimmy_carter.png");

$page->snp("description", "What every American president should be.");
$page->snp("image",       "/free/jimmy_carter.1200-630.png");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Jimmy_Carter = new WikipediaContentSection();
$div_wikipedia_Jimmy_Carter->setTitleText("Jimmy Carter");
$div_wikipedia_Jimmy_Carter->setTitleLink("https://en.wikipedia.org/wiki/Jimmy_Carter");
$div_wikipedia_Jimmy_Carter->content = <<<HTML
	<p>James Earl Carter Jr. (October 1, 1924 – December 29, 2024) was the 39th president of the United States, serving from 1977 to 1981. As a member of the Democratic Party, he was the longest-lived U.S. president and the first to have reached 100 years of age.</p>
	HTML;


$page->parent('list_of_people.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Jimmy Carter");
$page->body($div_wikipedia_Jimmy_Carter);
