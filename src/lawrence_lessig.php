<?php
$page = new Page();
$page->h1("Lawrence Lessig");
$page->viewport_background("/free/lawrence_lessig.png");
$page->alpha_sort("Lessig, Lawrence");
$page->keywords("Lawrence Lessig", "Lessig");
$page->tags("Person", "Lawyer", "USA", "SCOTUS", "Electoral System", "Institutions: Judiciary");
$page->stars(2);

$page->snp("description", "American lawyer and political activist.");
$page->snp("image",       "/free/lawrence_lessig.1200-630.png");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Larry Lessig is a prominent American academic, lawyer, and political activist whose work has significantly shaped the discourse on democracy,
	particularly in the context of the digital age.
	Born in 1961, Lessig has dedicated his career to exploring the complex intersections of law, technology, and politics,
	often challenging conventional thinking and advocating for systemic reforms.</p>

	<p>Lessig's early work centered on copyright law and the rise of the internet.
	He was a vocal advocate for a more open and accessible internet,
	leading to the creation of the Creative Commons licenses, a revolutionary tool for sharing and remixing creative works
	while respecting creators' rights.
	This work demonstrated his commitment to empowering individuals and fostering a more collaborative digital landscape.</p>

	<p>However, Lessig's focus shifted significantly towards the realm of political reform and the health of democracy.
	He became increasingly concerned about the influence of money in politics,
	arguing that the current campaign finance system in the United States allows wealthy donors and special interests to wield disproportionate power,
	undermining the principle of "one person, one vote."</p>

	<p>Lessig's concept of "institutional corruption" is central to his analysis.
	He argues that the core issue is not individual bribery or malfeasance,
	but rather the legal and systemic ways in which money distorts the political process,
	leading to policies that benefit a select few at the expense of the broader public.
	He sees this as a fundamental threat to the integrity of democracy itself.</p>

	<p>To counter this threat, Lessig has become a leading voice for campaign finance reform.
	His most prominent proposal is the creation of a system of citizen-funded elections,
	where campaigns are primarily funded by small contributions from ordinary citizens, often matched by public funds.
	This approach, he believes, will lessen the dependence of politicians on wealthy donors
	and ensure that they are more accountable to their constituents.</p>

	<p>Lessig's activism is not limited to academic writings and policy proposals.
	He has founded organizations like Equal Citizens, dedicated to promoting campaign finance reform,
	and actively engages with the public through his talks, videos, and writings.
	He has also run a symbolic campaign for the 2016 presidential election to raise the public's awareness of the role of money in politics,
	even though his campaign had no intention of winning any election.</p>

	<p>While his focus is primarily on American democracy,
	his analysis and proposed remedies have broader implications for all countries seeking to strengthen their democratic processes.
	His emphasis on the influence of money in politics is a challenge to any society
	where the wealthy and powerful can bend the rules in their favor.</p>

	<p>Larry Lessig's work is not only a critique of the current system but also a call to action.
	He emphasizes that democracy is not a static achievement, but a constant struggle.
	He challenges us to be vigilant against the forces that seek to undermine it,
	and to work towards creating a political system that is truly representative, fair, and accountable to all its citizens.
	His contributions to the debate on democracy continue to resonate, inspiring many to seek a more just and equitable political future.</p>
	HTML;


$div_Bibliography = new ContentSection();
$div_Bibliography->content = <<<HTML
	<h3>Bibliography</h3>

	<p>He is the author of the books:</p>

	<ul>
		<li>Republic, Lost: How Money Corrupts Congress—and a Plan to Stop It (2011)</li>
		<li>One Way Forward: The Outsider's Guide to Fixing the Republic (2012)</li>
		<li>Lesterland: The Corruption of Congress and How to End It (2013)</li>
		<li>Republic, Lost: The Corruption of Equality and the Steps to End It (2015)</li>
		<li>Fidelity & Constraint: How the Supreme Court Has Read the American Constitution (2019)</li>
		<li>They Don't Represent Us: Reclaiming Our Democracy (2019)</li>
		<li>${'How to Steal a Presidential Election'} (2024, with ${'Matthew Seligman'})</li>
	</ul>
	HTML;

$div_wikipedia_Lawrence_Lessig = new WikipediaContentSection();
$div_wikipedia_Lawrence_Lessig->setTitleText("Lawrence Lessig");
$div_wikipedia_Lawrence_Lessig->setTitleLink("https://en.wikipedia.org/wiki/Lawrence_Lessig");
$div_wikipedia_Lawrence_Lessig->content = <<<HTML
	<p>Lester Lawrence Lessig III (born June 3, 1961) is an American legal scholar and political activist.
	He is the Roy L. Furman Professor of Law at Harvard Law School and the former director of the Edmond J. Safra Center for Ethics at Harvard University.
	He is the founder of Creative Commons and of Equal Citizens.
	Lessig was a candidate for the Democratic Party's nomination for president of the United States in the 2016 U.S. presidential election but withdrew before the primaries.</p>
	HTML;


$page->parent('list_of_people.html');

$page->body($div_introduction);
$page->body($div_Bibliography);

$page->related_tag("Lawrence Lessig");


$page->body($div_wikipedia_Lawrence_Lessig);
