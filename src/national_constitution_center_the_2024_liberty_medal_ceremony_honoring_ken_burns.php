<?php
$page = new VideoPage();
$page->h1("National Constitution Center: The 2024 Liberty Medal Ceremony Honoring Ken Burns");
$page->tags("Video: Podcast", "National Constitution Center", "Podcast: We the People", "Ken Burns", "Liberty Medal");
$page->keywords("National Constitution Center: The 2024 Liberty Medal Ceremony Honoring Ken Burns");
$page->stars(1);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>In 2024, the ${'National Constitution Center'} awarded ${'Ken Burns'} with the ${'Liberty Medal'}.</p>
	HTML;



$div_The_2024_Liberty_Medal_Ceremony_Honoring_Ken_Burns = new WebsiteContentSection();
$div_The_2024_Liberty_Medal_Ceremony_Honoring_Ken_Burns->setTitleText("The 2024 Liberty Medal Ceremony Honoring Ken Burns");
$div_The_2024_Liberty_Medal_Ceremony_Honoring_Ken_Burns->setTitleLink("https://constitutioncenter.org/news-debate/podcasts/the-2024-liberty-medal-ceremony-honoring-ken-burns");
$div_The_2024_Liberty_Medal_Ceremony_Honoring_Ken_Burns->content = <<<HTML
	<p>This week, the National Constitution Center held its annual Liberty Medal ceremony honoring America’s storyteller, Ken Burns, for illuminating the nation’s greatest triumphs and tragedies and inspiring all of us to learn about the principles at the heart of the American idea. In this episode, Jeffrey Rosen and Burns’s co-director Sarah Botstein talk about Burns’s life and work, followed by Ken Burns’s inspiring acceptance speech. Burns then sits down with Rosen for a conversation about the American Idea.</p>
	HTML;




$page->parent('list_of_videos.html');
$page->template("stub");
$page->body($div_introduction);
$page->body($div_The_2024_Liberty_Medal_Ceremony_Honoring_Ken_Burns);
