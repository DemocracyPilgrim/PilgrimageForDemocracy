<?php
$page = new Page();
$page->h1('Rohingya people');
$page->keywords('Rohingya people', 'Rohingya');
$page->tags("Myanmar", "Humanity", "Bangladesh", "Genocide");
$page->stars(2);
$page->viewport_background('/free/rohingya_people.png');

$page->snp('description', 'A stateless ethnic group following Islam.');
$page->snp('image',       '/free/rohingya_people.png');

$page->snp('description', 'A Stateless People in Search of Justice and Security');
$page->snp('image', "/free/rohingya_people.1200-630.png");

$page->preview( <<<HTML
	<p></p>
	HTML );


$r1 = $page->ref('https://www.aljazeera.com/news/longform/2023/8/25/what-is-life-like-inside-the-worlds-largest-refugee-camp', 'What is life like inside the world’s biggest refugee camp?');


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<h3>The Rohingya: A Stateless People in Search of Justice and Security</h3>

	<p>The Rohingya are a stateless ethnic group, predominantly Muslim, who have faced decades of persecution and discrimination in $Myanmar.
	Concentrated primarily in Rakhine State, they have long been denied citizenship and basic rights by the Myanmar government,
	leading to their marginalization and vulnerability.
	The Rohingya's plight is a tragic example of ethnic and religious intolerance,
	culminating in a brutal genocide that forced hundreds of thousands to flee their homes.</p>

	<p>Before the harrowing events of 2017, an estimated 1.4 million Rohingya people lived in Myanmar.
	However, they have historically been viewed as illegal immigrants from $Bangladesh, despite having lived in the region for generations.
	This denial of citizenship, coupled with systemic discrimination, has rendered the Rohingya effectively stateless,
	leaving them without legal protection or the right to basic human dignities.
	They have faced restrictions on their movement, access to education and healthcare, and have been subjected to various forms of discrimination and violence.</p>

	<p>In August 2017, a coordinated campaign of violence by the Myanmar military and local vigilante groups unleashed a brutal genocide against the Rohingya.
	This horrific campaign involved mass killings, sexual violence, arson, and the deliberate destruction of Rohingya villages.
	Over 740,000 Rohingya were forced to flee across the border into neighboring Bangladesh, creating one of the world's largest and fastest-growing refugee crises.
	The vast majority of these refugees, now numbering close to a million,
	remain in overcrowded and often precarious ${'refugee camps'} in Cox's Bazar, Bangladesh, reliant on humanitarian aid for survival.</p>

	<p>The use of ${'social media platforms'}, particularly Facebook, in exacerbating the conflict has also come under scrutiny.
	Evidence suggests that Facebook failed to adequately moderate hate speech and anti-Rohingya propaganda,
	which played a significant role in inciting violence and dehumanizing the Rohingya population, ultimately contributing to the genocide.
	The platform has faced severe criticism for its slow response to the issue
	and for not doing enough to prevent the spread of misinformation and incitement.</p>

	<p>The Rohingya crisis is not just a humanitarian catastrophe but also a significant challenge to international justice.
	The United Nations and other international organizations have conducted investigations
	and concluded that the Myanmar military committed acts of genocide against the Rohingya.
	The International Criminal Court (ICC) has also opened investigations into these crimes.</p>

	<p>Despite the ongoing challenges, the Rohingya people continue to fight for their dignity, their rights, and a safe return to their homeland.
	The plight of the Rohingya serves as a stark reminder of the devastating consequences of ethnic and religious intolerance,
	the power of misinformation, and the critical importance of upholding human rights for all, regardless of their status.
	Their story demands our attention, our compassion, and our unwavering commitment to justice.</p>
	HTML;




$div_U_N_calls_for_Myanmar_generals_to_be_tried_for_genocide_blames_Facebook_for = new WebsiteContentSection();
$div_U_N_calls_for_Myanmar_generals_to_be_tried_for_genocide_blames_Facebook_for->setTitleText('U.N. calls for Myanmar generals to be tried for genocide, blames Facebook');
$div_U_N_calls_for_Myanmar_generals_to_be_tried_for_genocide_blames_Facebook_for->setTitleLink('https://www.reuters.com/article/us-myanmar-rohingya-un/myanmar-generals-had-genocidal-intent-against-rohingya-must-face-justice-u-n-idUSKCN1LC0KN');
$div_U_N_calls_for_Myanmar_generals_to_be_tried_for_genocide_blames_Facebook_for->content = <<<HTML
	<p>Myanmar’s military carried out mass killings and gang rapes of Muslim Rohingya with “genocidal intent”,
	and the commander-in-chief and five generals should be prosecuted for the gravest crimes under international law,
	United Nations investigators said.</p>

	<p>(...) A report by investigators was the first time the United Nations has explicitly called for Myanmar officials
	to face genocide charges over their campaign against the Rohingya, and is likely to deepen the country’s isolation.</p>

	<p>(...) The report also could serve as a major catalyst for change in how
	the world’s big social media companies handle hate speech in parts of the world
	where they have limited direct presence but their platforms command huge influence.
	The investigators sharply criticized Facebook,
	which has become Myanmar’s dominant social media network despite having no employees there,
	for letting its platform be used to incite violence and hatred.</p>
	HTML;



$div_wikipedia_Rohingya_people = new WikipediaContentSection();
$div_wikipedia_Rohingya_people->setTitleText('Rohingya people');
$div_wikipedia_Rohingya_people->setTitleLink('https://en.wikipedia.org/wiki/Rohingya_people');
$div_wikipedia_Rohingya_people->content = <<<HTML
	<p>The Rohingya people are a stateless Indo-Aryan ethnic group who predominantly follow Islam and reside in Rakhine State, Myanmar.
	Before the Rohingya genocide in 2017, when over 740,000 fled to Bangladesh, an estimated 1.4 million Rohingya lived in Myanmar.</p>
	HTML;

$div_wikipedia_2012_Rakhine_State_riots = new WikipediaContentSection();
$div_wikipedia_2012_Rakhine_State_riots->setTitleText('2012 Rakhine State riots');
$div_wikipedia_2012_Rakhine_State_riots->setTitleLink('https://en.wikipedia.org/wiki/2012_Rakhine_State_riots');
$div_wikipedia_2012_Rakhine_State_riots->content = <<<HTML
	<p>The 2012 Rakhine State riots were a series of conflicts primarily between ethnic Rakhine Buddhists and Rohingya Muslims
	in northern Rakhine State, Myanmar, though by October Muslims of all ethnicities had begun to be targeted.</p>
	HTML;

$div_wikipedia_Rohingya_genocide = new WikipediaContentSection();
$div_wikipedia_Rohingya_genocide->setTitleText('Rohingya genocide');
$div_wikipedia_Rohingya_genocide->setTitleLink('https://en.wikipedia.org/wiki/Rohingya_genocide');
$div_wikipedia_Rohingya_genocide->content = <<<HTML
	<p>The Rohingya genocide is a series of ongoing persecutions and killings of the Muslim Rohingya people by the military of Myanmar.</p>
	HTML;

$div_wikipedia_Rohingya_conflict = new WikipediaContentSection();
$div_wikipedia_Rohingya_conflict->setTitleText('Rohingya conflict');
$div_wikipedia_Rohingya_conflict->setTitleLink('https://en.wikipedia.org/wiki/Rohingya_conflict');
$div_wikipedia_Rohingya_conflict->content = <<<HTML
	<p>The Rohingya conflict is an ongoing conflict in the northern part of Myanmar's Rakhine State,
	characterised by sectarian violence between the Rohingya Muslim and Rakhine Buddhist communities,
	a military crackdown on Rohingya civilians by Myanmar's security forces,
	and militant attacks by Rohingya insurgents in Buthidaung, Maungdaw, and Rathedaung Townships, which border Bangladesh.</p>
	HTML;

$div_wikipedia_2015_Rohingya_refugee_crisis = new WikipediaContentSection();
$div_wikipedia_2015_Rohingya_refugee_crisis->setTitleText('2015 Rohingya refugee crisis');
$div_wikipedia_2015_Rohingya_refugee_crisis->setTitleLink('https://en.wikipedia.org/wiki/2015_Rohingya_refugee_crisis');
$div_wikipedia_2015_Rohingya_refugee_crisis->content = <<<HTML
	<p>In 2015, hundreds of thousands of Rohingya people were forcibly displaced from their villages and IDP camps in Rakhine State, Myanmar,
	due to sectarian violence. Nearly one million fled to neighbouring Bangladesh and some travelled to Southeast Asian countries.</p>
	HTML;


$page->parent('humanity.html');
$page->body($div_introduction);

$page->related_tag("Rohingya People");
$page->template('cover-picture-ai-generated');
$page->body($div_U_N_calls_for_Myanmar_generals_to_be_tried_for_genocide_blames_Facebook_for);

$page->body($div_wikipedia_Rohingya_people);
$page->body($div_wikipedia_2012_Rakhine_State_riots);
$page->body($div_wikipedia_Rohingya_genocide);
$page->body($div_wikipedia_Rohingya_conflict);
$page->body($div_wikipedia_2015_Rohingya_refugee_crisis);
