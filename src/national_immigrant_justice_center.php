<?php
$page = new Page();
$page->h1("National Immigrant Justice Center");
$page->tags("Organisation", "Humanity", "Immigration");
$page->keywords("National Immigrant Justice Center");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;



$div_National_Immigrant_Justice_Center = new WebsiteContentSection();
$div_National_Immigrant_Justice_Center->setTitleText("National Immigrant Justice Center ");
$div_National_Immigrant_Justice_Center->setTitleLink("https://immigrantjustice.org/");
$div_National_Immigrant_Justice_Center->content = <<<HTML
	<p>The National Immigrant Justice Center (NIJC) is dedicated to ensuring human rights protections and access to justice
	for all immigrants, refugees and asylum seekers.</p>

	<p>With offices in Chicago, Indiana, San Diego, and Washington, D.C., NIJC provides direct legal services to and advocates for these populations
	through policy reform, impact litigation, and public education.
	Since its founding in 1984, NIJC has been unique in blending individual client advocacy with broad-based systemic change.</p>
	HTML;



$div_wikipedia_National_Immigrant_Justice_Center = new WikipediaContentSection();
$div_wikipedia_National_Immigrant_Justice_Center->setTitleText("National Immigrant Justice Center");
$div_wikipedia_National_Immigrant_Justice_Center->setTitleLink("https://en.wikipedia.org/wiki/National_Immigrant_Justice_Center");
$div_wikipedia_National_Immigrant_Justice_Center->content = <<<HTML
	<p>The National Immigrant Justice Center (NIJC) is a center affiliated with the Heartland Alliance in the United States
	that "is dedicated to ensuring human rights protections and access to justice for all immigrants, refugees and asylum seekers."
	Its executive director is Mary Meg McCarthy[2][3] and it is headquartered in Chicago.</p>
	HTML;


$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);


$page->body($div_National_Immigrant_Justice_Center);
$page->body($div_wikipedia_National_Immigrant_Justice_Center);
