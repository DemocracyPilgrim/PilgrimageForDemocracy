<?php
$page = new Page();
$page->h1("Russian Federation and People's Republic of China joint statement, February 2022");
$page->stars(3);

//$page->snp("description", "");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p></p>
	HTML );

$r1 = $page->ref('http://en.kremlin.ru/supplement/5770', 'Joint Statement of the Russian Federation and the People’s Republic of China');
$r2 = $page->ref('https://freedomhouse.org/article/autocrats-favorite-word-democracy', 'Autocrats’ Favorite Word? Democracy.');
$r3 = $page->ref('https://en.wikipedia.org/wiki/Russian_invasion_of_Ukraine', 'Russian invasion of Ukraine');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>In February 2022, Putin, the ruler of the ${'Russian Federation'}, went to the ${"People's Republic of China"},
	officially to take part in the opening ceremony of the XXIV Olympic Winter.
	He met with Chinese ruler Xi Jin-ping.</p>

	<p>On February 4th, 2022, the Russian and Chinese rulers issued a joint statement, $r1 $r2
	reproduced in full below,
	and which mentions the word "$democracy" twelve times.</p>

	<p>On February 24th, only a few short weeks later, Putin started a land grabbing war against $Ukraine. $r3</p>

	<p>We might be tempted to agree with the vision of democracy as described in the statement.
	However, the actions of both the Russian and Chinese leaders within their $countries,
	and the full scale invasion of Ukraine unleashed by Putin,
	thoroughly belie everything they write.</p>

	<p>We keep here a copy of the full text of the said joint statement for the following reasons:</p>

	<ul>
		<li>Preserve the full statement for posterity.</li>
		<li>When Putin's regime falls, we expect this and other documents from Putin's reign
		to be removed from the official website of the Russian government (en.kremlin.ru).</li>
		<li>Allow future researchers to study the language of democracy that autocrats use
		for their own propaganda purposes.</li>
	</ul>
	HTML;



$div_Joint_Statement_of_the_Russian_Federation_and_the_People_s_Republic_of_China = new WebsiteContentSection();
$div_Joint_Statement_of_the_Russian_Federation_and_the_People_s_Republic_of_China->setTitleText("Joint Statement of the Russian Federation and the People’s Republic of China on the International Relations Entering a New Era and the Global Sustainable Development");
$div_Joint_Statement_of_the_Russian_Federation_and_the_People_s_Republic_of_China->setTitleLink("http://en.kremlin.ru/supplement/5770");
$div_Joint_Statement_of_the_Russian_Federation_and_the_People_s_Republic_of_China->content = <<<HTML
	<p><strong>February 4, 2022</strong></p>

	<p>At the invitation of President of the People’s
Republic of China Xi Jinping, President of the Russian Federation Vladimir V.
Putin visited China on 4 February 2022. The Heads of State held talks
in Beijing and took part in the opening ceremony of the XXIV Olympic Winter
Games.</p><p>The Russian Federation and the People's Republic of China, hereinafter referred to as the sides, state as follows.</p><p>Today, the world is going through momentous changes,
and humanity is entering a new era of rapid development and profound
transformation. It sees the development of such processes and phenomena as multipolarity, economic globalization, the advent of information society,
cultural diversity, transformation of the global governance architecture and world order; there is increasing interrelation and interdependence between the States;
a trend has emerged towards redistribution of power in the world; and the international community is showing a growing demand for the leadership aiming
at peaceful and gradual development. At the same time, as the pandemic of the new coronavirus infection continues, the international and regional security
situation is complicating and the number of global challenges and threats is
growing from day to day. Some actors representing but the minority on the international scale continue to advocate unilateral approaches to addressing
international issues and resort to force; they interfere in the internal
affairs of other states, infringing their legitimate rights and interests, and incite contradictions, differences and confrontation, thus hampering the development and progress of mankind, against the opposition from the international community.</p><p>The sides call on all States to pursue well-being for all and, with these ends, to build dialogue and mutual trust, strengthen mutual
understanding, champion such universal human values as peace, development,
equality, justice, democracy and freedom, respect the rights of peoples to independently determine the development paths of their countries and the sovereignty and the security and development interests of States, to protect
the United Nations-driven international architecture and the international
law-based world order, seek genuine multipolarity with the United Nations and its Security Council playing a central and coordinating role, promote more
democratic international relations, and ensure peace, stability and sustainable
development across the world. </p><p>I</p><p>The sides share the understanding that democracy is a universal human value, rather than a privilege of a limited number of States,
and that its promotion and protection is a common responsibility of the entire world
community.</p><p>The sides believe that democracy is a means of citizens' participation in the government of their country with the view to improving the well-being of population and implementing the principle of popular government. Democracy is exercised in all spheres of public life as part of a nation-wide process and reflects the interests of all the people, its
will, guarantees its rights, meets its needs and protects its interests. There
is no one-size-fits-all template to guide countries in establishing democracy.
A nation can choose such forms and methods of implementing democracy that would
best suit its particular state, based on its social and political system, its
historical background, traditions and unique cultural characteristics. It is
only up to the people of the country to decide whether their State is a democratic one. </p><p>The sides note that Russia and China as world powers
with rich cultural and historical heritage have long-standing traditions of democracy, which rely on thousand-years of experience of development, broad
popular support and consideration of the needs and interests of citizens. Russia
and China guarantee their people the right to take part through various means
and in various forms in the administration of the State and public life in accordance with the law. The people of both countries are certain of the way
they have chosen and respect the democratic systems and traditions of other States.</p><p>The sides note that democratic principles are
implemented at the global level, as well as in administration of State. Certain
States' attempts to impose their own ”democratic standards“ on other
countries, to monopolize the right to assess the level of compliance with
democratic criteria, to draw dividing lines based on the grounds of ideology,
including by establishing exclusive blocs and alliances of convenience, prove
to be nothing but flouting of democracy and go against the spirit and true
values of democracy. Such attempts at hegemony pose serious threats to global
and regional peace and stability and undermine the stability of the world
order.</p><p>The sides believe that the advocacy of democracy and human rights must not be used to put pressure on other countries. They oppose
the abuse of democratic values and interference in the internal affairs of sovereign states under the pretext of protecting democracy and human rights,
and any attempts to incite divisions and confrontation in the world. The sides call
on the international community to respect cultural and civilizational diversity
and the rights of peoples of different countries to self-determination. They
stand ready to work together with all the interested partners to promote
genuine democracy.</p><p>The sides note that the Charter of the United Nations
and the Universal Declaration of Human Rights set noble goals in the area of universal human rights, set forth fundamental principles, which all the States
must comply with and observe in deeds. At the same time, as every nation has
its own unique national features, history, culture, social system and level of social and economic development, universal nature of human rights should be
seen through the prism of the real situation in every particular country, and human rights should be protected in accordance with the specific situation in each country and the needs of its population. Promotion and protection of human
rights is a shared responsibility of the international community. The states
should equally prioritize all categories of human rights and promote them in a systemic manner. The international human rights cooperation should be carried
out as a dialogue between the equals involving all countries. All States must
have equal access to the right to development. Interaction and cooperation on human rights matters should be based on the principle of equality of all
countries and mutual respect for the sake of strengthening the international
human rights architecture.</p><p><b>II</b></p><p>The sides believe that peace, development and cooperation lie at the core of the modern international system. Development is
a key driver in ensuring the prosperity of the nations. The ongoing pandemic of the new coronavirus infection poses a serious challenge to the fulfilment of the UN 2030 Agenda for Sustainable Development. It is vital to enhance
partnership relations for the sake of global development and make sure that the new stage of global development is defined by balance, harmony and inclusiveness.</p><p>The sides are seeking to advance their work to link
the development plans for the Eurasian Economic Union and the Belt and Road
Initiative with a view to intensifying practical cooperation between the EAEU
and China in various areas and promoting greater interconnectedness between the Asia Pacific and Eurasian regions. The sides reaffirm their focus on building
the Greater Eurasian Partnership in parallel and in coordination with the Belt
and Road construction to foster the development of regional associations as well as bilateral and multilateral integration processes for the benefit of the peoples on the Eurasian continent.</p><p>The sides agreed to continue consistently intensifying
practical cooperation for the sustainable development of the Arctic. </p><p>The sides will strengthen cooperation within
multilateral mechanisms, including the United Nations, and encourage the international community to prioritize development issues in the global
macro-policy coordination. They call on the developed countries to implement in good faith their formal commitments on development assistance, provide more
resources to developing countries, address the uneven development of States,
work to offset such imbalances within States, and advance global and international development cooperation. The Russian side
confirms its readiness to continue working on the China-proposed Global
Development Initiative, including participation in the activities of the Group
of Friends of the Global Development Initiative under the UN auspices. In order
to accelerate the implementation of the UN 2030 Agenda for Sustainable
Development, the sides call on the international community to take practical
steps in key areas of cooperation such as poverty reduction, food security,
vaccines and epidemics control, financing for development, climate change,
sustainable development, including green development, industrialization,
digital economy, and infrastructure connectivity.</p><p>The sides call on the international community to create open, equal,
fair and non-discriminatory conditions for scientific and technological
development, to step up practical implementation of scientific and technological advances in order to identify new drivers of economic growth.</p><p>The sides call upon all countries to strengthen
cooperation in sustainable transport, actively build contacts and share
knowledge in the construction of transport facilities, including smart
transport and sustainable transport, development and use of Arctic routes, as well as to develop other areas to support global post-epidemic recovery. </p><p>The sides are taking serious action and making an important contribution to the fight against climate change. Jointly celebrating
the 30th anniversary of the adoption of the UN Framework Convention on Climate
Change, they reaffirm their commitment to this Convention as well as to the goals, principles and provisions of the Paris Agreement, including the principle of common but differentiated responsibilities. The sides work
together to ensure the full and effective implementation of the Paris
Agreement, remain committed to fulfilling the obligations they have undertaken
and expect that developed countries will actually ensure the annual provision
of $100 billion of climate finance to developing states. The sides oppose
setting up new barriers in international trade under the pretext of fighting
climate change.</p><p>The sides strongly support the development of international cooperation
and exchanges in the field of biological diversity, actively participating in the relevant global governance process, and intend to jointly promote the harmonious development of humankind and nature as well as green transformation
to ensure sustainable global development.</p><p>The Heads of State positively assess the effective interaction between
Russia and China in the bilateral and multilateral formats focusing on the fight against the COVID-19 pandemic, protection of life and health of the population of the two countries and the peoples of the world. They will further
increase cooperation in the development and manufacture of vaccines against the new coronavirus infection, as well as medical drugs for its treatment, and enhance collaboration in public health and modern medicine. The sides plan to strengthen coordination on epidemiological measures to ensure strong protection
of health, safety and order in contacts between citizens of the two countries.
The sides have commended the work of the competent authorities and regions of the two countries on implementing quarantine measures in the border areas and ensuring the stable operation of the border crossing points, and intend to consider establishing a joint mechanism for epidemic control and prevention in the border areas to jointly plan anti-epidemic measures to be taken at the border checkpoints, share information, build infrastructure and improve the efficiency of customs clearance of goods.</p><p>The sides emphasize that ascertaining the origin of the new coronavirus
infection is a matter of science. Research on this topic must be based on global knowledge, and that requires cooperation among scientists from all over
the world. The sides oppose politicization of this issue. The Russian side
welcomes the work carried out jointly by China and WHO to identify the source
of the new coronavirus infection and supports the China – WHO joint report
on the matter. The sides call on the global community to jointly promote a serious scientific approach to the study of the coronavirus origin.</p><p>The Russian side supports a successful hosting by the Chinese side of the Winter Olympic and Paralympic Games in Beijing in 2022.</p><p>The sides highly appreciate the level of bilateral
cooperation in sports and the Olympic movement and express their readiness to contribute to its further progressive development.</p><p><b>III</b></p><p>The sides are gravely concerned about serious international security
challenges and believe that the fates of all nations are interconnected. No State
can or should ensure its own security separately from the security of the rest
of the world and at the expense of the security of other States. The international community should actively engage in global governance to ensure
universal, comprehensive, indivisible and lasting security.</p><p>The sides reaffirm their strong mutual support for the protection of their core interests, state sovereignty and territorial integrity, and oppose
interference by external forces in their internal affairs.</p><p>The Russian side reaffirms its support for the One-China principle,
confirms that Taiwan is an inalienable part of China, and opposes any forms of independence of Taiwan. </p><p>Russia and China stand against attempts by external forces to undermine
security and stability in their common adjacent regions, intend to counter
interference by outside forces in the internal affairs of sovereign countries
under any pretext, oppose colour revolutions, and will increase cooperation in the aforementioned areas.</p><p>The sides condemn terrorism in all its manifestations, promote the idea
of creating a single global anti-terrorism front, with the United Nations
playing a central role, advocate stronger political coordination and constructive engagement in multilateral counterterrorism efforts. The sides
oppose politicization of the issues of combating terrorism and their use as instruments of policy of double standards, condemn the practice of interference
in the internal affairs of other States for geopolitical purposes through the use of terrorist and extremist groups as well as under the guise of combating
international terrorism and extremism. </p><p>The sides believe that certain States, military and political alliances
and coalitions seek to obtain, directly or indirectly, unilateral military
advantages to the detriment of the security of others, including by employing
unfair competition practices, intensify geopolitical rivalry, fuel antagonism
and confrontation, and seriously undermine the international security order and global strategic stability. The sides oppose further enlargement of NATO and call on the North Atlantic Alliance to abandon its ideologized cold war
approaches, to respect the sovereignty, security and interests of other
countries, the diversity of their civilizational, cultural and historical
backgrounds, and to exercise a fair and objective attitude towards the peaceful
development of other States. The sides stand against the formation of closed
bloc structures and opposing camps in the Asia-Pacific region and remain highly
vigilant about the negative impact of the United States' Indo-Pacific strategy
on peace and stability in the region. Russia and China have made consistent
efforts to build an equitable, open and inclusive security system in the Asia-Pacific Region (APR) that is not directed against third countries and that
promotes peace, stability and prosperity.</p><p>The sides welcome the Joint Statement of the Leaders
of the Five Nuclear-Weapons States on Preventing Nuclear War and Avoiding Arms
Races and believe that all nuclear-weapons States should abandon the cold war
mentality and zero-sum games, reduce the role of nuclear weapons in their
national security policies, withdraw nuclear weapons deployed abroad, eliminate
the unrestricted development of global anti-ballistic missile defense (ABM)
system, and take effective steps to reduce the risks of nuclear wars and any
armed conflicts between countries with military nuclear capabilities.</p><p>The sides reaffirm that the Treaty on the Non-Proliferation of Nuclear Weapons is the cornerstone of the international
disarmament and nuclear non-proliferation system, an important part of the post-war international security system, and plays an indispensable role in world peace and development. The international community should promote the balanced implementation of the three pillars of the Treaty and work together to protect the credibility, effectiveness and the universal nature of the instrument. </p><p>The sides are seriously concerned about the trilateral
security partnership between Australia, the United States, and the United
Kingdom (AUKUS), which provides for deeper cooperation between its members in areas involving strategic stability, in particular their decision to initiate
cooperation in the field of nuclear-powered submarines. Russia and China
believe that such actions are contrary to the objectives of security and sustainable development of the Asia-Pacific region, increase the danger of an arms race in the region, and pose serious risks of nuclear proliferation. The sides strongly condemn such moves and call on AUKUS participants to fulfil
their nuclear and missile non-proliferation commitments in good faith and to work together to safeguard peace, stability, and development in the region.</p><p>Japan's plans to release nuclear contaminated water
from the destroyed Fukushima nuclear plant into the ocean and the potential
environmental impact of such actions are of deep concern to the sides. The sides emphasize that the disposal of nuclear contaminated water should be
handled with responsibility and carried out in a proper manner based on arrangements between the Japanese side and neighbouring States, other
interested parties, and relevant international agencies while ensuring
transparency, scientific reasoning, and in accordance with international law.</p><p>The sides believe that the U.S. withdrawal from the Treaty on the Elimination of Intermediate-Range and Shorter-Range Missiles, the acceleration of research and the development of intermediate-range and shorter-range ground-based missiles and the desire to deploy them in the Asia-Pacific and European regions, as well as their transfer to the allies,
entail an increase in tension and distrust, increase risks to international and regional security, lead to the weakening of international non-proliferation and arms control system, undermining global strategic stability. The sided call on the United States to respond positively to the Russian initiative and abandon
its plans to deploy intermediate-range and shorter-range ground-based missiles
in the Asia-Pacific region and Europe. The sides will continue to maintain
contacts and strengthen coordination on this issue.</p><p>The Chinese side is sympathetic to and supports the proposals put forward by the Russian Federation to create long-term legally
binding security guarantees in Europe.</p><p>The sides note that the denunciation by the United
States of a number of important international arms control agreements has an extremely negative impact on international and regional security and stability.
The sides express concern over the advancement of U.S. plans to develop global
missile defence and deploy its elements in various regions of the world,
combined with capacity building of high-precision non-nuclear weapons for disarming strikes and other strategic objectives. The sides stress the importance
of the peaceful uses of outer space, strongly support the central role of the UN Committee on the Peaceful Uses of Outer Space in promoting international
cooperation, maintaining and developing international space law and regulation
in the field of space activities. Russia and China will continue to increase
cooperation on such matters of mutual interest as the long-term sustainability
of space activities and the development and use of space resources. The sides
oppose attempts by some States to turn outer space into an arena of armed
confrontation and reiterate their intention to make all necessary efforts to prevent the weaponization of space and an arms race in outer space. They will
counteract activities aimed at achieving military superiority in space and using it for combat operations. The sides affirm the need for the early launch
of negotiations to conclude a legally binding multilateral instrument based on the Russian-Chinese draft treaty on the prevention of placement of weapons in outer space and the use or threat of force against space objects that would
provide fundamental and reliable guarantees against an arms race and the weaponization of outer space.</p><p>Russia and China emphasize that appropriate
transparency and confidence-building measures, including an international
initiative/political commitment not to be the first to place weapons in space,
can also contribute to the goal of preventing an arms race in outer space, but
such measures should complement and not substitute the effective legally
binding regime governing space activities.</p><p>The sides reaffirm their belief that the Convention on the Prohibition of the Development, Production and Stockpiling of Bacteriological (Biological) and Toxin Weapons and on their Destruction (BWC)
is an essential pillar of international peace and security. Russia and China
underscore their determination to preserve the credibility and effectiveness of the Convention.</p><p>The sides affirm the need to fully respect and further
strengthen the BWC, including by institutionalizing it, strengthening its
mechanisms, and adopting a legally binding Protocol to the Convention with an effective verification mechanism, as well as through regular consultation and cooperation in addressing any issues related to the implementation of the Convention. </p><p>The sides emphasize that domestic and foreign
bioweapons activities by the United States and its allies raise serious
concerns and questions for the international community regarding their
compliance with the BWC. The sides share the view that such activities pose a serious threat to the national security of the Russian Federation and China and are detrimental to the security of the respective regions. The sides call on the U.S. and its allies to act in an open, transparent, and responsible manner
by properly reporting on their military biological activities conducted
overseas and on their national territory, and by supporting the resumption of negotiations on a legally binding BWC Protocol with an effective verification
mechanism.</p><p>The sides, reaffirming their commitment to the goal of a world free of chemical weapons, call upon all parties to the Chemical Weapons
Convention to work together to uphold its credibility and effectiveness. Russia
and China are deeply concerned about the politicization of the Organization for the Prohibition of Chemical Weapons and call on all of its members to strengthen solidarity and cooperation and protect the tradition of consensual
decision-making. Russia and China insist that the United States, as the sole State
Party to the Convention that has not yet completed the process of eliminating
chemical weapons, accelerate the elimination of its stockpiles of chemical
weapons. The sides emphasize the importance of balancing the non-proliferation
obligations of states with the interests of legitimate international
cooperation in the use of advanced technology and related materials and equipment for peaceful purposes. The sides note the resolution entitled
”Promoting international Cooperation on Peaceful Uses in the Context of International Security“ adopted at the 76th session of the UN General
Assembly on the initiative of China and co‑sponsored by Russia, and look
forward to its consistent implementation in accordance with the goals set forth
therein.</p><p>The sides attach great importance to the issues of governance in the field of artificial intelligence. The sides are ready to strengthen dialogue and contacts on artificial intelligence.</p><p>The sides reiterate their readiness to deepen
cooperation in the field of international information security and to contribute to building an open, secure, sustainable and accessible ICT
environment. The sides emphasize that the principles of the non-use of force,
respect for national sovereignty and fundamental human rights and freedoms, and non-interference in the internal affairs of other States, as enshrined in the UN Charter, are applicable to the information space. Russia and China reaffirm
the key role of the UN in responding to threats to international information
security and express their support for the Organization in developing new norms
of conduct of states in this area.</p><p>The sides welcome the implementation of the global
negotiation process on international information security within a single
mechanism and support in this context the work of the UN Open-ended Working
Group on security of and in the use of information and communication
technologies (ICTs) 2021–2025 (OEWG) and express their willingness to speak
with one voice within it. The sides consider it necessary to consolidate the efforts of the international community to develop new norms of responsible behaviour
of States, including legal ones, as well as a universal international legal
instrument regulating the activities of States in the field of ICT. The sides
believe that the Global Initiative on Data Security, proposed by the Chinese
side and supported, in principle, by the Russian side, provides a basis for the Working Group to discuss and elaborate responses to data security threats and other threats to international information security.</p><p>The sides reiterate their support of United Nations
General Assembly resolutions 74/247 and 75/282, support the work of the relevant Ad Hoc Committee of Governmental Experts, facilitate the negotiations
within the United Nations for the elaboration of an international convention on countering the use of ICTs for criminal purposes. The sides encourage constructive
participation of all sides in the negotiations in order to agree as soon as possible on a credible, universal, and comprehensive convention and provide it
to the United Nations General Assembly at its 78<sup>th</sup> session in strict
compliance with resolution 75/282. For these purposes, Russia and China have
presented a joint draft convention as a basis for negotiations.</p><p>The sides support the internationalization of Internet
governance, advocate equal rights to its governance, believe that any attempts
to limit their sovereign right to regulate national segments of the Internet
and ensure their security are unacceptable, are interested in greater
participation of the International Telecommunication Union in addressing these
issues.</p><p>The sides intend to deepen bilateral cooperation in international information security on the basis of the relevant 2015
intergovernmental agreement. To this end, the sides have agreed to adopt in the near future a plan for cooperation between Russia and China in this area.</p><p><b>IV</b></p><p>The sides underline that Russia and China, as world
powers and permanent members of the United Nations Security Council, intend to firmly adhere to moral principles and accept their responsibility, strongly
advocate the international system with the central coordinating role of the United Nations in international affairs, defend the world order based on international law, including the purposes and principles of the Charter of the United Nations, advance multipolarity and promote the democratization of international relations, together create an even more prospering, stable, and just world, jointly build international relations of a new type.</p><p>The Russian side notes the significance of the concept
of constructing a ”community of common destiny for mankind“ proposed
by the Chinese side to ensure greater solidarity of the international community
and consolidation of efforts in responding to common challenges. The Chinese
side notes the significance of the efforts taken by the Russian side to establish a just multipolar system of international relations. </p><p>The sides intend to strongly uphold the outcomes of the Second World War and the existing post-war world order, defend the authority of the United Nations and justice in international relations, resist
attempts to deny, distort, and falsify the history of the Second World War.</p><p>In order to prevent the recurrence of the tragedy of the world war, the sides will strongly condemn actions aimed at denying the responsibility for atrocities of Nazi aggressors, militarist invaders, and their accomplices, besmirch and tarnish the honour of the victorious countries.</p><p>The sides call for the establishment of a new kind of relationships between world powers on the basis of mutual respect, peaceful
coexistence and mutually beneficial cooperation. They reaffirm that the new
inter-State relations between Russia and China are superior to political and military alliances of the Cold War era. Friendship between the two States has
no limits, there are no ”forbidden“ areas of cooperation,
strengthening of bilateral strategic cooperation is neither aimed against third
countries nor affected by the changing international environment and circumstantial changes in third countries.</p><p>The sides reiterate the need for consolidation, not
division of the international community, the need for cooperation, not
confrontation. The sides oppose the return of international relations to the state of confrontation between major powers, when the weak fall prey to the strong. The sides intend to resist attempts to substitute universally
recognized formats and mechanisms that are consistent with international law
for rules elaborated in private by certain nations or blocs of nations, and are
against addressing international problems indirectly and without consensus,
oppose power politics, bullying, unilateral sanctions, and extraterritorial
application of jurisdiction, as well as the abuse of export control policies,
and support trade facilitation in line with the rules of the World Trade
Organization (WTO).</p><p>The sides reaffirmed their intention to strengthen
foreign policy coordination, pursue true multilateralism, strengthen
cooperation on multilateral platforms, defend common interests, support the international and regional balance of power, and improve global governance.</p><p>The sides support and defend the multilateral trade system based on the central role of the World Trade Organization (WTO), take an active part in the WTO reform, opposing unilateral approaches and protectionism. The sides are
ready to strengthen dialogue between partners and coordinate positions on trade
and economic issues of common concern, contribute to ensuring the sustainable
and stable operation of global and regional value chains, promote a more open,
inclusive, transparent, non-discriminatory system of international trade and economic rules.</p><p>The sides support the G20 format as an important forum for discussing
international economic cooperation issues and anti-crisis response measures,
jointly promote the invigorated spirit of solidarity and cooperation within the G20, support the leading role of the association in such areas as the international fight against epidemics, world economic recovery, inclusive
sustainable development, improving the global economic governance system in a fair and rational manner to collectively address global challenges.</p><p>The sides support the deepened strategic partnership within BRICS,
promote the expanded cooperation in three main areas: politics and security,
economy and finance, and humanitarian exchanges. In particular, Russia and China intend to encourage interaction in the fields of public health, digital
economy, science, innovation and technology, including artificial intelligence
technologies, as well as the increased coordination between BRICS countries on international platforms. The sides strive to further strengthen the BRICS
Plus/Outreach format as an effective mechanism of dialogue with regional
integration associations and organizations of developing countries and States
with emerging markets.</p><p>The Russian side will fully support the Chinese side chairing the association in 2022, and assist in the fruitful holding of the XIV BRICS
summit.</p><p>Russia and China aim to comprehensively strengthen the Shanghai Cooperation
Organization (SCO) and further enhance its role in shaping a polycentric world
order based on the universally recognized principles of international law,
multilateralism, equal, joint, indivisible, comprehensive and sustainable
security.</p><p>They consider it important to consistently implement the agreements on improved mechanisms to counter challenges and threats to the security of SCO
member states and, in the context of addressing this task, advocate expanded functionality
of the SCO Regional Anti-Terrorist Structure.</p><p>The sides will contribute to imparting a new quality and dynamics to the economic interaction between the SCO member States in the fields of trade,
manufacturing, transport, energy, finance, investment, agriculture, customs, telecommunications,
innovation and other areas of mutual interest, including through the use of advanced, resource-saving, energy efficient and ”green“ technologies.</p><p>The sides note the fruitful interaction within the SCO under the 2009
Agreement between the Governments of the Shanghai Cooperation Organization member
States on cooperation in the field of international information security, as well as within the specialized Group of Experts. In this context, they welcome
the adoption of the SCO Joint Action Plan on Ensuring International Information
Security for 2022–2023 by the Council of Heads of State of SCO Member States on September 17, 2021 in Dushanbe.</p><p>Russia and China proceed from the ever-increasing importance of cultural
and humanitarian cooperation for the progressive development of the SCO. In order to strengthen mutual understanding between the people of the SCO member States,
they will continue to effectively foster interaction in such areas as cultural
ties, education, science and technology, healthcare, environmental protection,
tourism, people-to-people contacts, sports.</p><p>Russia and China will continue to work to strengthen the role of APEC as the leading platform for multilateral dialogue on economic issues in the Asia-Pacific region. The sides intend to step up coordinated action to successfully implement the ”Putrajaya guidelines for the development of APEC until 2040“ with a focus on creating a free, open, fair,
non-discriminatory, transparent and predictable trade and investment
environment in the region. Particular emphasis will be placed on the fight
against the novel coronavirus infection pandemic and economic recovery,
digitalization of a wide range of different spheres of life, economic growth in remote territories and the establishment of interaction between APEC and other
regional multilateral associations with a similar agenda.</p><p>The sides intend to develop cooperation within the ”Russia-India-China“ format, as well as to strengthen interaction on such
venues as the East Asia Summit, ASEAN Regional Forum on Security, Meeting of Defense Ministers of the ASEAN Member States and Dialogue Partners. Russia and China support ASEAN's central role in developing cooperation in East Asia,
continue to increase coordination on deepened cooperation with ASEAN, and jointly promote cooperation in the areas of public health, sustainable
development, combating terrorism and countering transnational crime. The sides
intend to continue to work in the interest of a strengthened role of ASEAN as a key element of the regional architecture.</p>
HTML;



$page->parent('archives/index.html');
$page->parent('invoking_democracy.html');
$page->body($div_introduction);
$page->body($div_Joint_Statement_of_the_Russian_Federation_and_the_People_s_Republic_of_China);
