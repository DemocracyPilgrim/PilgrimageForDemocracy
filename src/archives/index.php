<?php
$page = new Page();
$page->h1("Archives");
$page->keywords("archives");
$page->stars(0);

//$page->snp("description", "");
//$page->snp("image",       "/copyrighted/");

$page->preview( <<<HTML
	<p>The purpose of these archives is to preserve some important documents, texts, speeches, communiqués, etc.
	Typically, the documents in the public domain.</p>
	HTML );

// $r1 = $page->ref('', '');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>The purpose of these archives is to preserve some important documents, texts, speeches, communiqués, etc.
	that are relevant to the $Pilgrimage.
	Typically, the documents in the public domain.</p>
	HTML;

$list = new ListOfPages();
$list->add('archives/russian_federation_and_people_s_republic_of_china_joint_statement_february_2022.html');
$print_list = $list->print();

$div_list = new ContentSection();
$div_list->content = <<<HTML
	<h3>List of documents</h3>

	$print_list
	HTML;


$page->parent('project/index.html');
$page->template("stub");
$page->body($div_introduction);

$page->body($div_list);
