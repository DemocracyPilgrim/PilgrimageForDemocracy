<?php
$page = new AwardPage();
$page->h1("International Children's Peace Prize");
$page->tags("Award", "Peace", "Children's Rights");
$page->keywords("International Children's Peace Prize");
$page->stars(0);
$page->viewport_background("");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;



$div_Children_Peace_Prize = new WebsiteContentSection();
$div_Children_Peace_Prize->setTitleText("Children Peace Prize ");
$div_Children_Peace_Prize->setTitleLink("http://www.childrenspeaceprize.org/");
$div_Children_Peace_Prize->content = <<<HTML
	<p>The International Children’s Peace Prize is an initiative of the International children’s rights organization KidsRights.</p>
	HTML;




$div_wikipedia_International_Children_s_Peace_Prize = new WikipediaContentSection();
$div_wikipedia_International_Children_s_Peace_Prize->setTitleText("International Children s Peace Prize");
$div_wikipedia_International_Children_s_Peace_Prize->setTitleLink("https://en.wikipedia.org/wiki/International_Children's_Peace_Prize");
$div_wikipedia_International_Children_s_Peace_Prize->content = <<<HTML
	<p>The International Children's Peace Prize is awarded annually to a child who has made a significant contribution to advocating children's rights and improving the situation of vulnerable children such as orphans, child labourers and children with HIV/AIDS.</p>
	HTML;


$page->parent('list_of_awards.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("International Children's Peace Prize");

$page->body($div_Children_Peace_Prize);
$page->body($div_wikipedia_International_Children_s_Peace_Prize);
