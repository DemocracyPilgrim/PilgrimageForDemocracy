<?php
$page = new OrganisationPage();
$page->h1("Integrity Institute (New Zealand)");
$page->viewport_background("");
$page->keywords("Integrity Institute (New Zealand)");
$page->stars(0);
$page->tags("Organisation", "Integrity", "Corruption", "New Zealand");

//$page->snp("description", "");
//$page->snp("image",       "/free/");

$page->preview( <<<HTML
	<p></p>
	HTML );

// $r1 = $page->ref("", "");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>The Integrity Institute is a ${'New Zealand'} organization focused on promoting integrity, ethical behavior, and good governance within various sectors.
	It aims to support businesses, government agencies, and non-profit organizations in fostering a culture of integrity and accountability.</p>

	<p>The Institute provides resources, training, and guidance on best practices in ethics and compliance.
	It also conducts research and raises awareness about the importance of integrity in decision-making processes.</p>

	<p>The Integrity Institute operates as a non-profit organization.</p>
	HTML;



$div_The_Integrity_Institute = new WebsiteContentSection();
$div_The_Integrity_Institute->setTitleText("The Integrity Institute ");
$div_The_Integrity_Institute->setTitleLink("https://theintegrityinstitute.org.nz/");
$div_The_Integrity_Institute->content = <<<HTML
	<p>
	</p>
	HTML;




$page->parent('list_of_organisations.html');
$page->template("stub");
$page->body($div_introduction);



$page->related_tag("Integrity Institute (New Zealand)");

$page->body($div_The_Integrity_Institute);
