$list_%titlePath% = new ListOfPages();
$list_%titlePath%->add('');
$print_list_%titlePath% = $list_%titlePath%->print();

$div_list_%titlePath% = new ContentSection();
$div_list_%titlePath%->content = <<<HTML
	<h3>%title%</h3>

	$print_list_%titlePath%
	HTML;

$page->body($div_list_%titlePath%);
