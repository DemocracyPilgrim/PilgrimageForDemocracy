<?php


// https://en.wikipedia.org/wiki/Democracy_Index


$democracy_index = array(
	'2022' => array(
		'Afghanistan' => array(
			'score' => 0.32,
		),
		'Albania' => array(
			'score' => 6.41,
		),
		'Algeria' => array(
			'score' => 3.66,
		),
		'Angola' => array(
			'score' => 3.96,
		),
		'Argentina' => array(
			'score' => 6.85,
		),
		'Armenia' => array(
			'score' => 5.63,
		),
		'Australia' => array(
			'score' => 8.71,
		),
		'Austria' => array(
			'score' => 8.20,
		),
		'Azerbaijan' => array(
			'score' => 2.87,
		),
		'Bahrain' => array(
			'score' => 2.52,
		),
		'Bangladesh' => array(
			'score' => 5.99,
		),
		'Belarus' => array(
			'score' => 1.99,
		),
		'Belgium' => array(
			'score' => 7.64,
		),
		'Benin' => array(
			'score' => 4.28,
		),
		'Bhutan' => array(
			'score' => 5.54,
		),
		'Bolivia' => array(
			'score' => 4.51,
		),
		'Bosnia and Herzegovina' => array(
			'score' => 5.00,
		),
		'Botswana' => array(
			'score' => 7.73,
		),
		'Brazil' => array(
			'score' => 6.78,
		),
		'Bulgaria' => array(
			'score' => 6.53,
		),
		'Burkina Faso' => array(
			'score' => 3.08,
		),
		'Burundi' => array(
			'score' => 2.13,
		),
		'Cambodia' => array(
			'score' => 3.18,
		),
		'Cameroon' => array(
			'score' => 2.56,
		),
		'Canada' => array(
			'score' => 8.88,
		),
		'Cape Verde' => array(
			'score' => 7.65,
		),
		'Central African Republic' => array(
			'score' => 1.35,
		),
		'Chad' => array(
			'score' => 1.67,
		),
		'Chile' => array(
			'score' => 8.22,
		),
		'China' => array(
			'score' => 1.94,
		),
		'Colombia' => array(
			'score' => 6.72,
		),
		'Comoros' => array(
			'score' => 3.2,
		),
		'The Democratic Republic of the Congo' => array(
			'score' => 1.48,
		),
		'Republic of the Congo' => array(
			'score' => 2.79,
		),
		'Costa Rica' => array(
			'score' => 8.29,
		),
		'Croatia' => array(
			'score' => 6.50,
		),
		'CUba' => array(
			'score' => 2.65,
		),
		'Cyprus' => array(
			'score' => 7.38,
		),
		'Czech Republic' => array(
			'score' => 7.97,
		),
		'Denmark' => array(
			'score' => 9.28,
		),
		'Djibouti' => array(
			'score' => 2.74,
		),
		'Dominican Republic' => array(
			'score' => 6.39,
		),
		'East Timor' => array(
			'score' => 7.06,
		),
		'Ecuador' => array(
			'score' => 5.69,
		),
		'Egypt' => array(
			'score' => 2.93,
		),
		'El Salvador' => array(
			'score' => 5.06,
		),
		'Equatorial Guinea' => array(
			'score' => 1.92,
		),
		'Eritrea' => array(
			'score' => 2.03,
		),
		'Estonia' => array(
			'score' => 7.96,
		),
		'Estwatini' => array(
			'score' => 3.01,
		),
		'Ethiopia' => array(
			'score' => 3.17,
		),
		'Fiji' => array(
			'score' => 5.55,
		),
		'Finland' => array(
			'score' => 9.29,
		),
		'France' => array(
			'score' => 8.07,
		),
		'Gabon' => array(
			'score' => 3.40,
		),
		'Gambia' => array(
			'score' => 4.47,
		),
		'Georgia' => array(
			'score' => 5.20,
		),
		'Germany' => array(
			'score' => 8.80,
		),
		'Ghana' => array(
			'score' => 6.43,
		),
		'Greece' => array(
			'score' => 7.97,
		),
		'Guatemala' => array(
			'score' => 4.68,
		),
		'Guinea' => array(
			'score' => 2.62,
		),
		'Guinea-Bissau' => array(
			'score' => 2.56,
		),
		'Guyana' => array(
			'score' => 6.34,
		),
		'Haiti' => array(
			'score' => 2.81,
		),
		'Honduras' => array(
			'score' => 5.15,
		),
		'Hong Kong' => array(
			'score' => 5.28,
		),
		'Hungary' => array(
			'score' => 6.64,
		),
		'Iceland' => array(
			'score' => 9.52,
		),
		'India' => array(
			'score' => 7.04,
		),
		'Indonesia' => array(
			'score' => 6.71,
		),
		'Iran' => array(
			'score' => 1.96,
		),
		'Iraq' => array(
			'score' => 3.13,
		),
		'Ireland' => array(
			'score' => 9.13,
		),
		'Israel' => array(
			'score' => 7.93,
		),
		'Italy' => array(
			'score' => 7.69,
		),
		'Ivory Coast' => array(
			'score' => 4.22,
		),
		'Jamaica' => array(
			'score' => 7.13,
		),
		'Japan' => array(
			'score' => 8.33,
		),
		'Jordan' => array(
			'score' => 3.17,
		),
		'Kazakhstan' => array(
			'score' => 3.08,
		),
		'Kenya' => array(
			'score' => 5.05,
		),
		'North Korea' => array(
			'score' => 1.08,
		),
		'South Korea' => array(
			'score' => 8.03,
		),
		'Kuwait' => array(
			'score' => 3.83,
		),
		'Kyrgyztan' => array(
			'score' => 3.62,
		),
		'Laos' => array(
			'score' => 1.77,
		),
		'Latvia' => array(
			'score' => 7.37,
		),
		'Lebanon' => array(
			'score' => 3.64,
		),
		'Lesotho' => array(
			'score' => 6.19,
		),
		'Liberia' => array(
			'score' => 5.43,
		),
		'Libya' => array(
			'score' => 2.06,
		),
		'Lithuania' => array(
			'score' => 7.31,
		),
		'Luxembourg' => array(
			'score' => 8.81,
		),
		'Madagascar' => array(
			'score' => 5.70,
		),
		'Malawi' => array(
			'score' => 5.91,
		),
		'Malaysia' => array(
			'score' => 7.30,
		),
		'Mali' => array(
			'score' => 3.23,
		),
		'Malta' => array(
			'score' => 7.70,
		),
		'Mauritania' => array(
			'score' => 4.03,
		),
		'Mauritius' => array(
			'score' => 8.14,
		),
		'Mexico' => array(
			'score' => 5.25,
		),
		'Moldova' => array(
			'score' => 6.23,
		),
		'Mongolia' => array(
			'score' => 6.35,
		),
		'Montenegro' => array(
			'score' => 6.45,
		),
		'Morocco' => array(
			'score' => 5.04,
		),
		'Mozambique' => array(
			'score' => 3.51,
		),
		'Myanmar' => array(
			'score' => 0.74,
		),
		'Namibia' => array(
			'score' => 6.52,
		),
		'Nepal' => array(
			'score' => 4.49,
		),
		'Netherlands' => array(
			'score' => 9.00,
		),
		'New Zealand' => array(
			'score' => 9.61,
		),
		'Nicaragua' => array(
			'score' => 2.50,
		),
		'Niger' => array(
			'score' => 3.73,
		),
		'Nigeria' => array(
			'score' => 4.23,
		),
		'North Macedonia' => array(
			'score' => 6.10,
		),
		'Norway' => array(
			'score' => 9.81,
		),
		'Oman' => array(
			'score' => 3.12,
		),
		'Pakistan' => array(
			'score' => 4.13,
		),
		'Palestine' => array(
			'score' => 3.86,
		),
		'Panama' => array(
			'score' => 6.91,
		),
		'Papua New Guinea' => array(
			'score' => 5.97,
		),
		'Paraguay' => array(
			'score' => 5.89,
		),
		'Peru' => array(
			'score' => 5.92,
		),
		'Philippines' => array(
			'score' => 6.73,
		),
		'Poland' => array(
			'score' => 7.04,
		),
		'Portugal' => array(
			'score' => 7.95,
		),
		'Qatar' => array(
			'score' => 3.65,
		),
		'Romania' => array(
			'score' => 6.45,
		),
		'Russia' => array(
			'score' => 2.28,
		),
		'Rwanda' => array(
			'score' => 3.10,
		),
		'Saudi Arabia' => array(
			'score' => 2.08,
		),
		'Senegal' => array(
			'score' => 5.72,
		),
		'Serbia' => array(
			'score' => 6.33,
		),
		'Sierra Leone' => array(
			'score' => 5.03,
		),
		'Singapore' => array(
			'score' => 6.22,
		),
		'Slovakia' => array(
			'score' => 7.07,
		),
		'Slovenia' => array(
			'score' => 7.75,
		),
		'South Africa' => array(
			'score' => 7.05,
		),
		'Spain' => array(
			'score' => 8.07,
		),
		'Sri Lanka' => array(
			'score' => 6.47,
		),
		'Sudan' => array(
			'score' => 2.47,
		),
		'Suriname' => array(
			'score' => 6.95,
		),
		'Sweden' => array(
			'score' => 9.39,
		),
		'Switzerland' => array(
			'score' => 9.14,
		),
		'Syria' => array(
			'score' => 1.43,
		),
		'Taiwan' => array(
			'score' => 8.99,
		),
		'Tajikistan' => array(
			'score' => 1.94,
		),
		'Tanzania' => array(
			'score' => 5.10,
		),
		'Thailand' => array(
			'score' => 6.67,
		),
		'Togo' => array(
			'score' => 2.99,
		),
		'Trinidad and Tobago' => array(
			'score' => 7.16,
		),
		'Tunisia' => array(
			'score' => 5.51,
		),
		'Turkey' => array(
			'score' => 4.35,
		),
		'Turkmenistan' => array(
			'score' => 1.66,
		),
		'Uganda' => array(
			'score' => 4.55,
		),
		'Ukraine' => array(
			'score' => 5.42,
		),
		'United Arab Emirates' => array(
			'score' => 2.90,
		),
		'United Kingdom' => array(
			'score' => 8.28,
		),
		'United States' => array(
			'score' => 7.85,
		),
		'Urugua' => array(
			'score' => 8.91,
		),
		'Uzbekistan' => array(
			'score' => 2.12,
		),
		'Venezuala' => array(
			'score' => 2.23,
		),
		'Vietnam' => array(
			'score' => 2.73,
		),
		'Yemen' => array(
			'score' => 1.95,
		),
		'Zambia' => array(
			'score' => 5.80,
		),
		'Zimbabwe' => array(
			'score' => 2.92,
		),
	),
);
