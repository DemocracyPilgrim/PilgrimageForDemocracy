<?php
require_once('objects/book.php');
require_once('objects/award.php');
require_once('objects/codeberg.php');
require_once('objects/country.php');
require_once('objects/documentary.php');
require_once('objects/freedom_house.php');
if (PHP_VERSION > 8.0) {
	// Needed on the CLI, but not on the server which runs PHP 7.
	require_once('objects/list.php');
}
require_once('objects/google_scholar.php');
require_once('objects/movie.php');
require_once('objects/organisation.php');
require_once('objects/page.php');
require_once('objects/person.php');
require_once('objects/report.php');
require_once('objects/section.php');
require_once('objects/ted.php');
require_once('objects/video.php');
require_once('objects/website.php');
require_once('objects/wikipedia.php');
require_once('objects/world_policy_council.php');
require_once('objects/youtube.php');
