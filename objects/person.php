<?php
require_once 'objects/page.php';

class PersonPage extends Page {

	public function __construct() {
		parent::__construct();
	}

	protected function print_icon() {
		return "<i class='ui user icon'></i>";
	}

}
