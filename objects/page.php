<?php
include_once 'include/header.php';
include_once 'include/footer.php';
include_once 'objects/templates.php';

class Page {
	public function __construct() {
		global $templates;
		$this->preview_section = new ContentSection();
		$this->templates = $templates;
	}

	private $classes = array('main-article');
	protected $templates = array();
	private $body_classes = array();
	protected $css_files = array();
	protected $body_parts = array();
	private $parents   = array();
	private $previous_ = null;
	private $next_     = null;
	private $h1 = array(
		'en' => 'Pilgrimage for Democracy',
	);
	private $preview_ = '';
	private $preview_section;
	public  $stars_;
	private $print_wip_ = false;
	private $dest_;
	private $keywords_   = array();
	private $tags_       = array();
	private $references_ = array();
	private $snp_ = array(
		'description' => NULL,
		'image'       => NULL,
	);
	protected $alpha_sort_ = "";
	public function alpha_sort($name) {
		$this->alpha_sort_ = $name;
	}
	public function get_alpha_sort() {
		return $this->alpha_sort_;
	}

	private $viewport_background_ = "";
	public function viewport_background($url) {
		$this->viewport_background_ = $url;
	}

	public function keywords(...$keywords) {
		foreach($keywords AS $kw) {
			$this->keywords_[$kw] = $kw;
		}
	}

	public function tags(...$tags) {
		foreach($tags AS $tag) {
			$this->tags_[$tag] = $tag;
		}
	}


	public function getKeywords() {
		return $this->keywords_;
	}

	public function getTags() {
		return $this->tags_;
	}

	public function get($item) {
		switch ($item) {
			case 'snp description':
				return $this->snp_['description'];
				break;
			case 'snp image':
				return $this->snp_['image'];
				break;
		}
	}

	public function snp($key, $text) {
		$this->snp_[$key] = $text;
	}

	public function dest($dest) {
		$this->dest_ = $dest;
		$this->setTitleLink($dest);
	}

	public function print_styles() {
		if ($this->viewport_background_) {
			$this->css_files[] = 'css/viewport_background.css';
		}

		global $css_files;
		foreach ($css_files AS $key => $file) {
			$this->css_files[$key] = $file;
		}

		if (empty($this->css_files)) {
			return "";
		}

		$out  = "\n";
		$out .= "<style>\n";
		foreach ($this->css_files AS $file) {
			$out .= file_get_contents($file);
		}
		$out .= "</style>\n";

		if ($this->viewport_background_) {
			$base = substr($this->viewport_background_, 0, -4);
			$out .= '<div class="viewport-background-image"></div>';
			$out  = str_replace("%viewportBackgroundImage%", $base, $out);
		}

		return $out;
	}

	public function preview($preview) {
		$this->preview_ = $preview;
		$this->preview_section->content = $preview;
	}

	private function print_h1($lang = 'en') {
		$class = join(' ', $this->classes);

		$out  = '<h1 id="top" class="' . $class . '">';
		$out .= '<span lang="en">';
		$out .=        $this->print_icon() . " &nbsp; ";
		$out .=        $this->h1['en'];
		$out .=        $this->preview_section->printSectionStars();
		$out .= '</span>';
		$out .= '</h1>';
		return $out;
	}

	public function h1($title, $lang = 'en') {
		$this->h1[$lang] = $title;
		$this->preview_section->setTitleText($title);

	}

	public function getTitle($lang = 'en') {
		return $this->h1[$lang];
	}

	public function setTitleLink($link) {
		$this->preview_section->setTitleLink($link);
	}


	public function stars($stars) {
		$this->stars_ = $stars;
		$this->preview_section->stars($stars);
	}

	private function print_HTML_header() {
		global $url_prefix;

		$title       = $this->h1['en'];
		$sTitle      = htmlspecialchars($this->h1['en']);
		$description = is_null($this->snp_['description']) ? 'Sowing the seeds of future peace'                      : htmlspecialchars($this->snp_['description']);
		$image       = is_null($this->snp_['image'])       ? '/copyrighted/hansjorg-keller-p7av1ZhKGBQ-1200-630.jpg' : htmlspecialchars($this->snp_['image']);
		$body_class = join(' ', $this->body_classes);
		$out = <<<HTML
		<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
		    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
			<title>$title</title>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			<meta http-equiv="pragma" content="no-cache" />
			<meta name='viewport'   content='width=device-width, initial-scale=1.0, maximum-scale=1.0' />
			<link rel="stylesheet" href="${url_prefix}/semantic/semantic.min.css" type="text/css" media="all" />
			<link rel="stylesheet" href="/style/style7.css" type="text/css" media="all" />

			<meta property="og:title"         content="$sTitle">
			<meta name="twitter:title"        content="$sTitle">

			<meta name="description"          content="$description">
			<meta property="og:description"   content="$description">
			<meta name="twitter:description"  content="$description">

			<meta property="og:image"         content="${url_prefix}${image}">
			<meta property="og:image:secure"  content="${url_prefix}${image}">
			<meta name="twitter:image"        content="${url_prefix}${image}">
			<meta name="twitter:card"         content="summary_large_image">
		</head>

		<body id="pagetop" class="${body_class}">
			<svg width="0" height="0" aria-hidden="true" focusable="false" tabindex="-1">
				<symbol viewBox="0 0 24 24" id="icon-github"><path fill="#171515" d="M12 .3a12 12 0 0 0-3.8 23.38c.6.11.83-.26.83-.57L9 20.87c-3.34.73-4.04-1.41-4.04-1.41-.55-1.4-1.34-1.76-1.34-1.76-1.08-.74.09-.73.09-.73 1.2.09 1.84 1.24 1.84 1.24 1.07 1.83 2.8 1.3 3.49 1 .1-.78.42-1.31.76-1.61-2.67-.3-5.47-1.33-5.47-5.93 0-1.31.47-2.38 1.24-3.22a4.3 4.3 0 0 1 .12-3.18s1-.32 3.3 1.23a11.52 11.52 0 0 1 6 0c2.3-1.55 3.3-1.23 3.3-1.23a4.3 4.3 0 0 1 .12 3.18 4.64 4.64 0 0 1 1.24 3.22c0 4.6-2.81 5.62-5.48 5.92.43.37.81 1.1.81 2.22l-.01 3.3c0 .31.21.69.82.57A12 12 0 0 0 12 .3z"></path></symbol>
				<symbol viewBox="0 0 24 24" id="icon-google-scholar"><path fill="#4285f4" d="M12 19.3 0 9.5 12 0v19.3Z"></path><path fill="#356ac3" d="m12 19.3 12-9.8L12 0v19.3Z"></path><circle cx="12" cy="17" r="7" fill="#a0c3ff"></circle><path fill="#76a7fa" d="M5.7 14a7 7 0 0 1 12.6 0H5.7Z"></path></symbol>
				<symbol viewBox="0 0 24 24" id="icon-youtube"><path fill="#ed1f24" d="M21 24H3a3 3 0 0 1-3-3V3a3 3 0 0 1 3-3h18a3 3 0 0 1 3 3v18a3 3 0 0 1-3 3Z"></path><path fill="#fff" d="M19.7 8.1a2 2 0 0 0-1.4-1.4C17 6.4 12 6.4 12 6.4s-5 0-6.3.3a2 2 0 0 0-1.4 1.4C4 9.4 4 12 4 12s0 2.6.3 3.9a2 2 0 0 0 1.4 1.4c1.3.3 6.3.3 6.3.3s5 0 6.3-.3a2 2 0 0 0 1.4-1.4c.3-1.3.3-3.9.3-3.9s0-2.6-.3-3.9Z"></path><path fill="#ed1f24" d="m10.4 14.4 4.1-2.4-4.1-2.4v4.8Z"></path></symbol>
			</svg>
		HTML;
		return $out;
	}
	public function parent($part, $options = array()) {
		$this->parents[] = array('part' => $part, 'options' => $options);
	}
	public function previous($previous) {
		$this->previous_ = $previous;
	}
	public function next($next) {
		$this->next_ = $next;
	}

	public function body($part, $options = array()) {
		$this->body_parts[] = array('part' => $part, 'options' => $options);
	}

	public function template($template, $options = array()) {
		$this->body_parts[] = array('part' => $this->templates[$template], 'options' => $options);
	}


	public function related_tag($tag, $contentSection = NULL) {
		$list = ListOfPeoplePages::WithTags($tag);

		if (!($list->size())) {
			return;
		}
		$print_list = $list->print();

		if (!$contentSection) {
			$div_list = new ContentSection();
			$div_list->content = <<<HTML
				<h3>Related content</h3>

				$print_list
				HTML;
			$this->body($div_list);
		}
		else {
			$contentSection->content .= $print_list;
			$this->body($contentSection);
		}

	}

	public function print($format = 'body', $options = NULL) {
		switch ($format) {
			case 'body':
				return $this->print_body();
				break;
			case 'icon':
				return $this->print_icon();
				break;
			case 'preview':
				return $this->print_preview();
				break;
			case 'link':
				$title = $this->getTitle(); // 'en'
				$dest = $this->dest_;
				return "<a href='/$dest'>$title</a>";
				break;
			case 'stars':
				return $this->preview_section->printStars();
				break;
			case 'wip':
				$this->print_wip_ = true;
				return $this->print_body();
				break;
		}
	}

	private function print_preview() {
		return $this->preview_section->print();
	}

	protected function get_page($dest) {
		require('templates/keywords.php');
		require('templates/auto_keywords.php');
		global $div_comment_on_codeberg;
		global $div_stars;
		global $preprocess_index_dest;
		global $process_fully;
		$process_fully = false;
		if (!isset($preprocess_index_dest[$dest])) {
			fwrite(STDERR, "Error: page missing: ${dest} \n");
			return '';
		}

		require($preprocess_index_dest[$dest]['include']);
		$name = $preprocess_index_dest[$dest]['name'];


		if ($name == 'page' && is_object($$name)) {
			$$name->setTitleLink('/' . $dest);
			return $$name;
		}
		fwrite(STDERR, "[Page->get_page()] Error: '${name}' for page '${dest}' is not an object.\n");
		return '';
	}

	private function print_previous_and_next() {
		$previous = $this->previous_ ? $this->get_page($this->previous_) : null;
		$next     = $this->next_     ? $this->get_page($this->next_    ) : null;

		if (!$previous && !$next) {
			return;
		}

		$this->css_files["css/navigation.css"] = "css/navigation.css";

		$out  = "<div class='navigation'>";

		if ($previous) {
			$out .=     "<div class='previous'>";
			$out .=     $previous->preview_section->print_navigation();
			$out .=     "<i class='angle left icon'></i>";
			$out .=     "</div>";
		}
		else {
			$out .= "<div></div>";
		}

		if ($next) {
			$out .=     "<div class='next'>";
			$out .=     "<i class='angle right icon'></i>";
			$out .=     $next->preview_section->print_navigation();
			$out .=     "</div>";
		}
		else {
			$out .= "<div></div>";
		}

		$out .= "</div>";

		return $out;
	}

	protected function print_parts($parts) {
		$out = '';
		foreach ($parts AS $item) {
			$part    = $item['part'];
			$options = $item['options'];
			if (is_array($part)) {
				$out .=  printSection($part);
			}
			else if(is_object($part)) {
				$format = (!empty($options) && isset($options['format'])) ? $options['format'] : 'body';
				$out .= $part->print($format, $options);
			}
			else {
				$out .=	printPageSection($part);
			}
		}
		return $out;
	}

	protected function print_icon() {
		return '';
	}

	private function print_body() {
		$out  = $this->print_HTML_header();
		$out .= print_header();

		$out .= $this->print_parts($this->parents);
		$out .= $this->print_previous_and_next();
		$out .= $this->print_h1();

		if ($this->print_wip_) {
			$out .= $this->print_wip();
		}

		if ($this->viewport_background_) {
			$base = substr($this->viewport_background_, 0, -4);
			$out .= "<div id='title-image' class='main-article'>";
			$out .= "<picture>";
			$out .=     "<source media='(min-width: 1280px)' srcset='${base}.png'>";
			$out .=     "<source media='(min-width: 1024px)' srcset='${base}.1200-630.png'>";
			$out .=     "<source media='(min-width:  768px)' srcset='${base}.1000-525.png'>";
			$out .=     "<source media='(min-width:  640px)' srcset='${base}.800-420.png'>";
			$out .=     "<img src='${base}.600-315.png' alt='Illustration' >";
			$out .= "</picture>";
			$out .= "</div>";
		}

		$out .= $this->print_parts($this->body_parts);
		$out .= $this->print_previous_and_next();
		$out .= $this->print_references();
		$out .= $this->print_tags();
		$out .= print_footer();
		$out .= $this->print_styles();
		$out .= print_HTML_footer();

		return $out;
	}

	public function ref($link, $title) {
		$id = count($this->references_);
		$id++;
		$this->references_[] = array(
			'id'    => $id,
			'link'  => $link,
			'title' => $title,
		);
		return "<a id='see-reference-{$id}' href='#reference-{$id}' class='see reference'>[{$id}]</a>";
	}

	private function print_references() {
		if (empty($this->references_)) {
			return;
		}

		$h2_references = new h2HeaderContent('References');;

		$out = '<ol id="reference-list">';
		foreach ($this->references_ AS $ref) {
			$out .= "<li id='reference-{$ref['id']}' class='reference' >";
			$out .=     "<a href='#see-reference-{$ref['id']}'>^</a> &nbsp;&nbsp;&nbsp;";
			$out .=     "<a href='{$ref['link']}' class='reference source' >{$ref['title']}</a>";
			$out .= "</li>";
		}
		$out .= '</ol>';

		$div_references = new ContentSection();
		$div_references->id('references');
		$div_references->content = $out;

		$out_final  = $h2_references->print();
		$out_final .= $div_references->print();
		return $out_final;
	}

	private function print_tags() {
		if (empty($this->tags_)) {
			return;
		}

		$this->css_files["css/tags.css"] = "css/tags.css";
		require('templates/keywords.php');
		require('templates/auto_keywords.php');


		$out = '<div id="tags">Tags: <ul>';
		foreach ($this->tags_ AS $tag) {
			$out .= "<li>";
			$out .= isset ($$tag) ? $$tag : $tag;
			$out .= "</li>";
		}
		$out .= '</ul></div>';

		$div_tags = new ContentSection();
		$div_tags->id('references');
		$div_tags->content = $out;

		return $div_tags->print();
	}

	private function print_wip() {
		return $this->print_wip_search();
	}

	private function print_wip_search() {
		// Transform:
		//           http/file.html => file
		$file = substr($this->dest_, 0, -5);
		$file = substr($file, 5);



		$see = '<p>See also: ';
		$see .= "<a href='/wip/file.php?f=$file'>$file</a>.";
		$see .= " Search: ";
		$pattern = '/[._-]/';
		$search = preg_split($pattern, $file);

		foreach ($search AS $word) {
			$see .= "<a href='/wip/search.php?s=$word'>$word</a> ";
		}

		$see .= "</p>";

		$div_See_Also = new ContentSection();
		$div_See_Also->content = <<<HTML
			<h3>WIP</h3>

			<p>$see</p>
			HTML;

		return $div_See_Also->print('body');
	}

	private function print_wip_content() {
		// Transform:
		//           http/file.html => wip/file.txt
		$file = substr($this->dest_, 0, -5);
		$file = substr($file, 5);
		$file = "wip/" . $file . ".txt";

		if (file_exists($file)) {
			$out  = '<pre style="white-space: pre-wrap;">';
			$out .= file_get_contents($file);
			$out .= '</pre>';
			return $out;
		}
	}
}
