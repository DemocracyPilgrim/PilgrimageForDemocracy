<?php
require_once('objects/section.php');

class FreedomHouseContentSection extends ContentSection {
	private $key;

	public function __construct($country = '') {
		$special_cases = array(
			"The Democratic Republic of the Congo" => 'democratic-republic-congo',
		);

		if (isset($special_cases[$country])) {
			$this->country = $special_cases[$country];
		}
		else {
			$this->country = strtolower($country);
			$this->country = str_replace(" ", "-", $this->country);
		}

		$this->classes[] = 'freedom-house';
		parent::__construct();
		$this->stars(NULL);
	}

	protected function printLogo() {
		return '<img class="section-icon" src="copyrighted/freedom_house_logo.svg" >';
	}


	protected $country;
}

class FreedomHouseGlobalFreedomContentSection extends FreedomHouseContentSection {
	public function __construct($country) {
		parent::__construct($country);
		global $freedom_house_global_freedom;
		$this->has_content = isset($freedom_house_global_freedom[$this->country]['name']);
	}

	protected function printMainTitle() {
		global $freedom_house_global_freedom;
		$out = $this->printLogo();

		$out .= '<h3>';
			$out .= "<a href='/freedom_house.html'>Freedom House</a>: ";
			$out .= " country profile for ";
			$name = $freedom_house_global_freedom[$this->country]['name'];
			$out .= "<a href='https://freedomhouse.org/country/{$this->country}'>{$name}</a>";
		$out .= '</h3>';
		return $out;
	}

	protected function printMainContent() {
		global $freedom_house_global_freedom;
		return $freedom_house_global_freedom[$this->country]['summary'];
	}
}

class FreedomHouseInternetFreedomContentSection extends FreedomHouseContentSection {
	public function __construct($country) {
		parent::__construct($country);
		global $freedom_house_internet_freedom;
		$this->has_content = isset($freedom_house_internet_freedom[$this->country]['name']);
	}

	protected function printMainTitle() {
		global $freedom_house_internet_freedom;
		$out = $this->printLogo();

		$out .= '<h3>';
			$out .= "<a href='/freedom_house.html'>Freedom House</a>: ";
			$out .= " internet freedom in ";
			$name = $freedom_house_internet_freedom[$this->country]['name'];
			$out .= "<a href='https://freedomhouse.org/country/{$this->country}/freedom-net/2022'>{$name}</a>";
		$out .= '</h3>';
		return $out;
	}

	protected function printMainContent() {
		global $freedom_house_internet_freedom;
		return $freedom_house_internet_freedom[$this->country]['summary'];
	}
}
