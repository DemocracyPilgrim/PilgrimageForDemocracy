<?php
require_once('objects/section.php');

$templates = array();

$templates["stub"] = new ContentSection();
$templates["stub"]->classes('notice stub technical-section');
$templates["stub"]->content = <<<HTML
	<div><i class="exclamation triangle large red icon"></i></div>
	<p><em>This article is a stub.
	You can help by expanding it.
	You can <a href="/project/participate.html">join</a> and
	<a href="https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy/issues">suggest improvements or edits</a> to this page.
	</em></p>
	HTML;

$templates["cover-picture-ai-generated"] = new ContentSection();
$templates["cover-picture-ai-generated"]->classes('notice technical-section');
$templates["cover-picture-ai-generated"]->content = <<<HTML
	<div><i class="exclamation circle large  icon"></i></div>
	<p><em>The cover picture for this article is AI-generated.</em></p>
	HTML;
