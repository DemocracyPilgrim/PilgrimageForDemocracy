<?php
require_once 'objects/page.php';

class OrganisationPage extends Page {

	public function __construct() {
		parent::__construct();
	}

	protected function print_icon() {
		return "<i class='ui university icon'></i>";
	}

}
