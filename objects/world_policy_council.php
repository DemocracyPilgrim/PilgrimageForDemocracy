<?php
require_once('objects/section.php');

class WorldPolicyCouncilContentSection extends ContentSection {
	public function __construct($country = '') {
		$this->classes[] = 'world-policy-council';
		$this->title_classes .= ' world-policy-council ';
		parent::__construct();
		$this->stars(NULL);
	}

	protected function printLogo() {
		return '<img class="section-icon" src="copyrighted/Header_Logo_World_Future_Council_FIN_RGB-300x138.png" style="height: 40px;" >';
	}
}

class FuturePolicyContentSection extends WorldPolicyCouncilContentSection {
}
