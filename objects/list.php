<?php
require_once('objects/content.php');

class ListContent extends Content {
	public function __construct() {
		$this->tag = 'ul';
		parent::__construct();
	}

	protected $items_ = array();

	public function size() {
		return count($this->items_);
	}
}

class ListOfPages extends ListContent {
	public function __construct() {
		parent::__construct();
		$this->tag = 'p';
		$this->classes[] = "list-of-pages";
		global $css_files;
		$css_files["css/list.css"] = "css/list.css";
	}

	private $weight_by_type = array(
		'BookPage'         =>  2,
		'VideoPage'        =>  5,
		'CountryPage'      =>  5,
		'OrganisationPage' =>  8,
		'PersonPage'       => 10,
	);

	protected $tag_logical_operator = "";

	protected function load_items_from_tags($tags) {
		require('templates/keywords.php');
		require('templates/auto_keywords.php');
		global $preprocess_index_dest;
		global $div_comment_on_codeberg;
		global $div_stars;
		require 'index/tags.php';
		$list_dest = array();
		if ($this->tag_logical_operator == "OR") {
			foreach ($tags AS $tag) {
				if (!isset($tags_index[$tag])) {
					continue;
				}

				foreach ($tags_index[$tag] AS $dest) {
					$list_dest[$dest] = $dest;
				}
			}
		}
		else if ($this->tag_logical_operator == "AND") {
			$buckets = array();
			$index = 0;
			foreach ($tags AS $tag) {
				if (!isset($tags_index[$tag])) {
					continue;
				}

				$buckets[$index] = array();
				foreach ($tags_index[$tag] AS $dest) {
					$buckets[$index][$dest] = $dest;
				}
				$index++;
			}
			$list_dest = array_intersect(...$buckets);
		}

		foreach ($list_dest AS $dest) {
			require($preprocess_index_dest[$dest]['include']);
			$type = get_class($page);

			$this->items_[] = array(
				"dest"       => $dest,
				"sort_alpha" => $page->get_alpha_sort(),
				"title"      => $page->getTitle(),
				"weight"     => isset($this->weight_by_type[$type]) ? $this->weight_by_type[$type] : 1,
			);
		}
	}

	public static function WithTags(...$tags): static {
		$tags = is_array($tags[0]) ? $tags[0] : $tags;
		global $process_fully;
		$new = new static();
		if ($process_fully) {
			$process_fully = false;
			$new->tag_logical_operator = "OR";
			$new->load_items_from_tags($tags);
			$process_fully = true;
		}
		return $new;
	}

	public static function WithAllTags(...$tags): static {
		global $process_fully;
		$new = new static();
		if ($process_fully) {
			$process_fully = false;
			$new->tag_logical_operator = "AND";
			$new->load_items_from_tags($tags);
			$process_fully = true;
		}
		return $new;
	}


	public function add($dest, $sort_alpha = "") {
		$this->items_[] = array(
			"dest"       => $dest,
			"sort_alpha" => $sort_alpha,
			"title"      => "",
			"weight"     => 1,
		);
	}


	protected function sort_items() {
		$weight     = array_column($this->items_, "weight");
		$sort_alpha = array_column($this->items_, "sort_alpha");
		$sort_dest  = array_column($this->items_, "dest");
		$sort_title = array_column($this->items_, "title");
		array_multisort($weight, SORT_ASC, $sort_alpha, SORT_ASC, $sort_title, SORT_ASC, $sort_dest, SORT_ASC, $this->items_);
	}


	protected function format_page_item($item) {
		$dest = $item["dest"];
		global $div_comment_on_codeberg;
		global $div_stars;
		global $preprocess_index_dest;
		global $process_fully;
		$process_fully = false;
		$out  = '';
		require('templates/keywords.php');
		require('templates/auto_keywords.php');
		require($preprocess_index_dest[$dest]['include']);

		$page->dest($dest);
		$link = $page->print('link');
		$stars = $page->print('stars');
		$description = $page->get('snp description');
		$description = is_null($description) ? '' : " &mdash; $description";
		$out .= "$stars $link $description<br>";

		$process_fully = true;
		return $out;
	}

	protected function printContent($lang) {
		global $process_fully;
		if (!$process_fully) return;

		$this->sort_items();
		global $preprocess_index_dest;
		$out  = '';

		foreach ($this->items_ AS $item) {
			$dest = $item["dest"];
			if (!isset($preprocess_index_dest[$dest])) {
				fwrite(STDERR, "\t[ListContent] !!Error: page not found: ${dest} \n");
				continue;
			}

			$name = $preprocess_index_dest[$dest]['name'];
			if ($name != 'page') {
				fwrite(STDERR, "\t[ListContent] !!Error: page does not support API 1: ${dest} \n");
				continue;
			}

			$out .= $this->format_page_item($item);
		}
		return $out;
	}
}

class UnsortedListOfPages extends ListOfPages {
	protected function sort_items() {}
}

class ListOfPeoplePages extends ListOfPages {
	public function __construct() {
		parent::__construct();
	}

	protected function format_page_item($item) {
		$dest = $item["dest"];
		global $div_comment_on_codeberg;
		global $div_stars;
		global $preprocess_index_dest;
		global $process_fully;
		$process_fully = false;
		$out  = '';
		require('templates/keywords.php');
		require('templates/auto_keywords.php');
		require($preprocess_index_dest[$dest]['include']);

		$page->dest($dest);
		$link = $page->print('link');
		$stars = $page->print('stars');
		$icon = $page->print('icon');
		$description = $page->get('snp description');
		$description = is_null($description) ? '' : " &mdash; $description";
		$alpha = substr($item["sort_alpha"], 0, 3);
		$out .= "$stars $icon <em class='alpha-sort-key'>$alpha</em> $link $description<br>";

		$process_fully = true;
		return $out;
	}
}

class latestUpdatedPagesBySrc extends ListOfPages {
	public function add($src, $sort_alpha = "") {
		global $preprocess_index_src;
		global $src_by_change_time;

		$this->items_[$src] = array(
			"dest"             => substr($preprocess_index_src[$src]['dest'], 5),
			"src"              => $src,
			"git_changed_t"    => $src_by_change_time[$src]["git_changed_t"],
			"fs_changed_t"     => $src_by_change_time[$src]["fs_changed_t"],
		);
	}

	protected function sort_items() {
		global $src_by_change_time;
		$last_check = $src_by_change_time["settings"]["last_checked_t"];

		foreach ($src_by_change_time AS $src => $data) {
			if ($src == "settings") {
				continue;
			}

			// Querying git log is time consuming, so do it only when the file has changed since the last check.
			// Also, the file system change time is more relevant for recent changes,
			// while the git last commit time is more accurate for older changes,
			// because the FS change time can be the time of the last git clone or git checkout.
			$changed_t = filemtime($src);

			// Give 14 days to commit latest changes, as we do not yet have a way to update our index upon an actual git commit.
			if (   $changed_t   >    ($src_by_change_time[$src]["fs_changed_t"] + 3600 * 24 * 14)   ) {
				$src_by_change_time[$src]["fs_changed_t"]  = $changed_t;
				$src_by_change_time[$src]["git_changed_t"] = shell_exec("git log -1 --pretty='format:%at' $src");
			}

			$now = time();
			if ($changed_t > ($now - (3600 * 36)) ) {
				$this->items_[$src]["display_changed_t"] =   $changed_t;
			}
			else if (is_numeric($src_by_change_time[$src]["git_changed_t"])) {
				$this->items_[$src]["display_changed_t"] =   $src_by_change_time[$src]["git_changed_t"];
			}
			else {
				$git_changed_t = shell_exec("git log -1 --pretty='format:%at' $src");
				if (is_numeric($git_changed_t)) {
					$src_by_change_time[$src]["git_changed_t"] = $git_changed_t;
					$this->items_[$src]["display_changed_t"] =   $git_changed_t;
				}
				else {
					$this->items_[$src]["display_changed_t"] =   $changed_t;
				}
			}
		}

		$sort_changed = array_column($this->items_, "display_changed_t");
		array_multisort($sort_changed, SORT_DESC, $this->items_);
	}

	protected function format_page_item($item) {
		$dest = $item["dest"];
		global $div_comment_on_codeberg;
		global $div_stars;
		global $preprocess_index_dest;
		global $process_fully;
		date_default_timezone_set('Asia/Taipei');
		$process_fully = false;
		$out  = '';
		require('templates/keywords.php');
		require('templates/auto_keywords.php');
		require($preprocess_index_dest[$dest]['include']);

		$page->dest($dest);
		$link = $page->print('link');
		$stars = $page->print('stars');
		$description = $page->get('snp description');
		$description = is_null($description) ? '' : " &mdash; $description";
		$now = time();
		$format = $item["display_changed_t"] + 3600 * 24 > $now   ?   "Y-m-d H:i" : "Y-m-d";
		$changed = date($format, $item["display_changed_t"]);
		$out .= "<em class='alpha-sort-key'>$changed</em> &nbsp; $stars $link $description<br>";

		$process_fully = true;
		return $out;
	}
}
