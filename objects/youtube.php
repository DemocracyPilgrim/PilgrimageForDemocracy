<?php
require_once('objects/section.php');

class YoutubeContentSection extends ContentSection {
	public function __construct($country = '') {
		$this->classes[] = 'youtube';
		$this->title_classes .= ' youtube ';
		parent::__construct();
		$this->stars(NULL);
	}

	protected function printLogo() {
		return '<img class="section-icon" src="copyrighted/Youtube_logo.png" style="height: 40px;" >';
	}
}
