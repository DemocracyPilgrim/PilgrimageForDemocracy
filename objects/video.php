<?php
require_once 'objects/page.php';

class VideoPage extends Page {

	public function __construct() {
		parent::__construct();
	}

	protected function print_icon() {
		return "<i class='ui video icon'></i>";
	}


}
