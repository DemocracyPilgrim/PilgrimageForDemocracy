<?php
require_once('objects/section.php');

class TEDtalkContentSection extends ContentSection {
	public function __construct($country = '') {
		$this->classes[] = 'TED';
		$this->title_classes .= ' TED ';
		parent::__construct();
		$this->stars(NULL);
	}

	protected function printLogo() {
		return '<img class="section-icon" src="copyrighted/TED_three_letter_logo.svg" style="height: 40px;" >';
	}
}
