<?php
require_once('objects/content.php');

class HeaderContent extends Content {
	public function __construct($title) {
		$this->title_text_ = $title;
		$this->content     = $title;
		$this->classes[] = 'main-article no-star-rating';
	}

	public function setTitleText($text) {
		$this->title_text_ = $text;
	}

	public function setTitleLink($link) {
		$this->title_link_ = $link;
	}

	protected $title_link_   = '';
	protected $title_text_   = '';
	protected $title_classes = '';
}


class h2HeaderContent extends HeaderContent {
	public function __construct($title) {
		$this->tag = "h2";
		parent::__construct($title);
	}
}

class h3HeaderContent extends HeaderContent {
	public function __construct($title) {
		$this->tag = "h3";
		parent::__construct($title);
	}
}
