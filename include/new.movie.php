<?php
require_once('include/new.page.php');

class newMovie extends newPagePrototype {
	protected $parent_page_ = "list_of_movies.html";
	protected $tags_ = "Movie";

	public function name() {
		return "New movie";
	}

	public function content_newPage() {
		return "\$page = new MoviePage();";
	}

	public function select_parent_page() {
	}
}
