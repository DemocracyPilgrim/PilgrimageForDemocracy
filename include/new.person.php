<?php
require_once('include/new.page.php');

class newPerson extends newPagePrototype {
	protected $parent_page_ = "list_of_people.html";
	protected $tags_        = "Person";

	public function name() {
		return "New person";
	}

	public function content_newPage() {
		return "\$page = new PersonPage();";
	}
	public function content_alphaSort() {
		return "\n\$page->alpha_sort(\"" . $this->article_name_ . "\");";
	}

}
