<?php

class Base {
	protected $name_;
	protected $id_;
	protected $tags_         = "";

        public function __construct($name = "") { $this->name_   = $name; }
	public function set_id($id)             { $this->id_     = $id;   }

	public function name() { return $this->name_; }
	public function id()   { return $this->id_;   }
	public function tags() { return $this->tags_; }

	public function tags_as_string() {
		if (is_string($this->tags_)) {
			return '"' . $this->tags_ . '"';
		}
		else if (is_array($this->tags_)) {
			$out = '';
			foreach ($this->tags_ AS $tag) {
				$out .= $out == '' ? '' : ', ';
				$out .= '"' . $tag->name() . '"';
			}
			return $out;
		}
		else if (is_object($this->tags_)) {
			$tags = $this->tags_->name();
			if (is_string($tags)) {
				return $tags;
			}
			else if (is_array($tags)) {
				$out = '';
				foreach ($tags AS $tag) {
					$out .= $out == '' ? '' : ', ';
					$out .= '"' . $tag . '"';
				}
				return $out;
			}
		}
		return "";
	}

}

class Tag extends Base {
}

class parentPage extends Base {
	protected $parent_;

        public function __construct($name, $parent, $tags = "") {
		$this->name_   = $name;
		$this->parent_ = $parent;
		$this->tags_   = $tags;
        }

	public function parent() { return $this->parent_; }
}

class newPagePrototype extends Base {
	protected $parent;
	protected $parent_page_  = "";
	protected $article_name_ = "";

	public function name()         { return "New page";           }
	public function parent_page()  { return $this->parent_page_;  }
	public function article_name() { return $this->article_name_; }


	public function select_parent_page() {}
	public function select_tags()        {}

	public function select_article_name()        {
		$articleName = (string)readline("Title of article: ");
		$articleName = str_replace("\n", "", $articleName);

		if (!$articleName) {
			echo "Please provide an article name! \n";
			return $this->select_article_name();
		}
		$this->article_name_ = trim($articleName);
	}

	public function content_alphaSort() { return "";}
	public function content_newPage()   { return "\$page = new Page();";}
	public function extra_body_parts()  { return array();}
}
