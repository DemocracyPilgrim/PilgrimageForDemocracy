<?php
require_once('include/new.page.php');

class newDocumentary extends newPagePrototype {
	protected $parent_page_ = "list_of_documentaries.html";

	public function name() {
		return "New Documentary or Podcast";
	}

	public function content_newPage() {
		return "\$page = new DocumentaryPage();";
	}

	public function select_tags() {
		$tags   = array();
		$tags[] = new Tag("Documentary");
		$tags[] = new Tag("Podcast");
		$tag = get_selection("What tag?", $tags);
		$this->tags_ = $tag->name();
	}
}
