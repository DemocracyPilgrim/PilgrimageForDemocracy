<?php
require_once('include/new.page.php');

class newVideo extends newPagePrototype {
	protected $parent_page_ = "list_of_videos.html";

	public function name() {
		return "New video";
	}

	public function content_newPage() {
		return "\$page = new VideoPage();";
	}

	public function select_parent_page() {
	}

	public function select_tags() {
		$tags   = array();
		$tags[] = new Tag("Video: News Story");
		$tags[] = new Tag("Video: Interview");
		$tags[] = new Tag("Video: Podcast");
		$tags[] = new Tag("Video: Speech");
		$tag = get_selection("What tag?", $tags);
		$this->tags_ = $tag->name();
	}
}
