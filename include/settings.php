<?php

// Default values that can be overridden in settings.local.php
$url_prefix = 'https://pildem.org';

if (file_exists('settings.local.php')) {
	include 'settings.local.php';
}


$shortopts  = "";
$longopts  = array(
	"wip",
);

$options = getopt($shortopts, $longopts);

$show_wip = isset($options['wip']);
