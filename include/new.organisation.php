<?php
require_once('include/new.page.php');

class newOrganisation extends newPagePrototype {
	protected $parent_page_ = "list_of_organisations.html";
	protected $tags_        = "Organisation";

	public function name() {
		return "New Organisation";
	}

	public function content_newPage() {
		return "\$page = new OrganisationPage();";
	}
}
