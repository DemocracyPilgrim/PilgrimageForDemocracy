<?php
require_once('include/new.page.php');

class newCountry extends newPagePrototype {
	protected $parent_page_ = "world.html";
	protected $tags_        = "Country";

	public function name() {
		return "New country";
	}

	public function content_newPage() {
		return '$page = new CountryPage("' . $this->article_name_ . '");';
	}

	public function extra_body_parts()  {
		return array("\$page->country_indices();\n");
	}
}
