<?php
require_once('include/new.page.php');

class newBook extends newPagePrototype {
	protected $parent_page_ = "list_of_books.html";
	protected $tags_        = "Book";

	public function name() {
		return "New book";
	}

	public function content_newPage() {
		return "\$page = new BookPage();";
	}
}
