<?php
require_once('include/new.page.php');

class newAward extends newPagePrototype {
	protected $parent_page_ = "list_of_awards.html";
	protected $tags_        = "Award";

	public function name() {
		return "New Award or Prize";
	}

	public function content_newPage() {
		return "\$page = new AwardPage();";
	}

	public function select_tags() {
		$tags   = array();
		$tags[] = new Tag("Award");
		$tags[] = new Tag("Prize");
		$tag = get_selection("What tag?", $tags);
		$this->tags_ = $tag->name();
	}
}
