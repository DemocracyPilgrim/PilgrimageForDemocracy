<?php
require_once('include/new.page.php');

class newArticle extends newPagePrototype {
	protected $section;

	public function name()         { return "New article";           }

	public function select_parent_page() {
		$sections   = array();

		$sections[] = new parentPage("Menu",             "menu.html");

		$tags   = array();
		$tags[] = new Tag("Individual");
		$tags[] = new Tag("Individual: Liberties");
		$tags[] = new Tag("Individual: Rights");
		$tags[] = new Tag("Individual: Development");
		$sections[] = new parentPage("1. Individuals",   "individuals.html",  $tags);

		$sections[] = new parentPage("2. Society",       "society.html",      "Society");

		$tags   = array();
		$tags[] = new Tag("Institutions");
		$tags[] = new Tag("Institutions: Judiciary");
		$tags[] = new Tag("Institutions: Legislative");
		$tags[] = new Tag("Institutions: Executive");
		$tags[] = new Tag("Institutions: Security");
		$sections[] = new parentPage("3. Institutions",  "institutions.html",  $tags);

		$tags   = array();
		$tags[] = new Tag("Elections");
		$tags[] = new Tag("Elections: Duverger Syndrome");
		$tags[] = new Tag("Elections: Solution");
		$sections[] = new parentPage("4. Elections",     "elections.html",     $tags);

		$tags   = array();
		$tags[] = new Tag("Information");
		$tags[] = new Tag("Information: Government");
		$tags[] = new Tag("Information: Media");
		$tags[] = new Tag("Information: Discourse");
		$tags[] = new Tag("Information: Facts");
		$tags[] = new Tag("Information: Social Networks");
		$sections[] = new parentPage("5. Information",   "information.html",   $tags);

		$tags   = array();
		$tags[] = new Tag("Living");
		$tags[] = new Tag("Living: Fair Income");
		$tags[] = new Tag("Living: Fair Treatment");
		$tags[] = new Tag("Economy");
		$sections[] = new parentPage("6. Living",        "living.html",        $tags);

		$sections[] = new parentPage("7. International", "international.html", "International");

		$sections[] = new parentPage("8. Humanity",      "humanity.html",      "Humanity");

		$sections[] = new parentPage("Preface: Danger",  "dangers.html",       "Danger");

		$tags   = array();
		$tags[] = new Tag("Democracy: WIP");
		$tags[] = new Tag("WIP");
		$sections[] = new parentPage("A. WIP",               "wip.html",                       $tags);

		$sections[] = new parentPage("B. Soloist and Choir", "teamwork.html",                  "Teamwork");
		$sections[] = new parentPage("C. Saints and Devils", "saints_and_little_devils.html",  "Saints");
		$sections[] = new parentPage("D. Teaching",          "teaching_democracy.html",        "Education");
		$sections[] = new parentPage("E. Taxes",             "taxes.html",                     "Taxes");
		$sections[] = new parentPage("F. Environment",       "environment.html",               "Environment");
		$sections[] = new parentPage("G. The Web",           "web.html",                       "The Web");

		$section = get_selection("Which section?", $sections);
		$this->section = $section;

		$parents   = array();
		switch ($section->id()) {
			case 1:
				break;
			case 2:
				break;
			case 3:
				$parents[] = $section;

				$tags      = new Tag(array("Institutions: Legislative", "Constitution"));
				$parents[] = new parentPage("Constitution",             "constitution.html",        $tags);

				$tags      = new Tag(array("Institutions: Judiciary", "SCOTUS Landmark Decision", "USA"));
				$parents[] = new parentPage("SCOTUS Landmark Decision",        "supreme_court_of_the_united_states.html",        $tags);
				break;
			case 4:
				$parents[] = $section;

				$tags      = new Tag(array("Elections", "Party Politics"));
				$parents[] = new parentPage("Party Politics",   "duverger_syndrome_party_politics.html",                         $tags);

				$tags      = new Tag(array("Elections", "Voting Methods"));
				$parents[] = new parentPage("Voting methods",   "voting_methods.html",                         $tags);
				break;
			case 5:
				$parents[] = $section;

				$tags      = new Tag(array("Information: Discourse", "Political discourse"));
				$parents[] = new parentPage("Political discourse",   "political_discourse.html",                                 $tags);

				$tags      = new Tag(array("Information: Discourse", "Disinformation"));
				$parents[] = new parentPage("Disinformation",        "disinformation.html",                                      $tags);

				$tags      = new Tag(array("Information: Social Networks", "Social Networks"));
				$parents[] = new parentPage("Social Networks",   "social_networks.html",                                         $tags);

				break;
			case 6:
				$parents[] = $section;

				$tags      = new Tag(array("Living: Fair Income",   "Economic Injustice"));
				$parents[] = new parentPage("Economic Injustice",   "economic_injustice.html",                                   $tags);

				$tags      = new Tag(array("Living: Economy",       "Economy"));
				$parents[] = new parentPage("Economy",              "economy.html",                                              $tags);

				$tags      = new Tag(array("Living: Economy",       "Economy", "Fair Share"));
				$parents[] = new parentPage("Fair Share",           "fair_share.html",                                              $tags);
				break;
			case 7:
				$parents[] = $section;

				$tags      = new Tag(array("International",   "Ukraine"));
				$parents[] = new parentPage("Ukraine",   "ukraine.html",                                                         $tags);

				$tags      = new Tag(array("International",   "USA"));
				$parents[] = new parentPage("United States",   "united_states.html",                                             $tags);
				$tags      = new Tag(array("International",   "USA", "US State"));

				$parents[] = new parentPage("US States",   "us_states.html",                                                     $tags);

				$tags      = new Tag(array("International",   "Philippines"));
				$parents[] = new parentPage("Philippines",   "philippines.html",                                                 $tags);

				break;
			case 8:
				break;
			case 9:
				break;
			case 10:
				break;
			case 11:
				break;
			case 12:
				break;
			case 13:
				break;
			case 14:
				$parents[] = $section;

				$tags      = new Tag(array("Taxes"));
				$parents[] = new parentPage("Taxes",   "taxes.html",                                                 $tags);

				$tags      = new Tag(array("Taxes"));
				$parents[] = new parentPage("Tax Renaissance",   "tax_renaissance.html",                             $tags);


				$tags      = new Tag(array("Taxes", "Tax"));
				$parents[] = new parentPage("List of taxes",   "list_of_taxes.html",                                 $tags);
				break;
			case 15:
				break;
			case 16:
				break;
		}

		if (empty($parents)) {
			$this->parent       = $section;
			$this->parent_page_ = $section->parent();
		}
		else {
			$parent = get_selection("What parent?", $parents);
			$this->parent       = $parent;
			$this->parent_page_ = $parent->parent();
		}
		$this->tags_ = $this->parent->tags();
	}

	public function select_tags() {
		if (is_array($this->parent->tags())) {
			if (count($this->parent->tags()) == 1) {
				$this->tags_ = $this->parent->tags();
			}
			else {
				$tag = get_selection("What tags?", $this->parent->tags());
				$this->tags_ = $tag->name();
			}
		}
		else if (!empty($tags)) {
			$this->tags_ = $this->parent->tags();
		}
	}

}
