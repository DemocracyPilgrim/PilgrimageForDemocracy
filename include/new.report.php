<?php
require_once('include/new.page.php');

class newReport extends newPagePrototype {
	protected $parent_page_ = "list_of_research_and_reports.html";

	public function name() {
		return "New Research or Report";
	}

	public function content_newPage() {
		return "\$page = new ReportPage();";
	}

	public function select_tags() {
		$tags   = array();
		$tags[] = new Tag("Research");
		$tags[] = new Tag("Report");
		$tag = get_selection("What tag?", $tags);
		$this->tags_ = $tag->name();
	}
}
