<?php
	include_once('settings.php');
	include_once('index/pages.php');
	if (file_exists('index/src_by_change_time.php')) {
		include_once('index/src_by_change_time.php');
	}
	require_once('objects/all.php');
	require_once('templates/keywords.php');
	require_once('templates/auto_keywords.php');

// index/src_by_change_time.php
if (!isset($src_by_change_time)) {
        $src_by_change_time = array(
                "settings" => array("last_checked_t" => 0),
        );
}


$process_fully = false;

$h1 = array(
	'id' => 'top',
	'class' => array('main-article'),
	'en' => 'Pilgrimage for Democracy',
);

$body = '';

// Social network preview
$snp = array(
	'description' => "Sowing the seeds of future peace",
	'image'       => '/copyrighted/hansjorg-keller-p7av1ZhKGBQ-1200-630.jpg',
);

$references = array();
$css_files  = array();

function get_page($src) {
	global $preprocess_index_dest;
	include('templates/keywords.php');
	include('templates/auto_keywords.php');
	include $preprocess_index_dest[$src]['include'];
	return $page;
}

function process_page($src, $format = 'body') {
	global $keywords_index;
	global $tags_index;
	global $process_fully;
	global $preprocess_index_src;
	global $div_comment_on_codeberg;
	global $div_stars;
	global $nb_articles_by_stars;
	include('templates/keywords.php');
	include('templates/auto_keywords.php');
	$body = '';
	$process_fully = true;

	include $src;
	$page->dest($preprocess_index_src[$src]['dest']);
	$preprocess_index_src[$src]['stars'] = $page->stars_;
	$nb_articles_by_stars[$page->stars_]++;

	$out = $page->print($format);
	file_put_contents($preprocess_index_src[$src]['dest'], $out);

	global $preprocess_index_dest;
	$dest = substr($preprocess_index_src[$src]['dest'], 5);

	$keywords = $page->getKeywords();
	if ($keywords != NULL) {
		foreach ($keywords AS $kw) {
			if (isset($keywords_index[$kw])) {
				$previous_dest = $keywords_index[$kw];
				fwrite(STDERR, "\t!! The keyword '$kw' for $dest is already set for $previous_dest.\n");
			}
			else {
				$keywords_index[$kw] = $dest;
			}
		}
	}

	$tags = $page->getTags();
	foreach ($tags AS $tag) {
		$tags_index[$tag][$dest] = $dest;
	}
}

function save_array_recurse($level, $array) {
	$prefix = str_repeat("\t", $level);
	$content = '';

	foreach ($array as $key => $data) {
		$sKey = addslashes($key);
		if (is_array($data)) {
			$content .= "$prefix'${sKey}' => array(\n";
			$content .= save_array_recurse($level + 1, $data);
			$content .= "$prefix),\n";
		}
		else {
			$sData = addslashes($data);
			$content .= "$prefix'${sKey}'\t => '${sData}',\n";
		}
	}
	return $content;
}

function save_array($name, $file) {
	global $$name;
	ksort ($$name);

	$content  = "<?php\n\n\n";
	$content .= "\$$name = array(\n";
	$content .= save_array_recurse(1, $$name);
	$content .= ");\n";

	file_put_contents($file, $content);
}

function print_title() {
	global $h1;
	echo $h1['en'];
}

function print_h1() {
	global $h1;
	$class = join(' ', $h1['class']);

	$out  = '<h1 id="' . $h1['id'] . '" class="' . $class . '">';
	$out .= '<span lang="en">' . $h1['en'] . '</span>';
	$out .= '</h1>';
	return $out;
}

function newSection($type = NULL, $p1 = NULL, $p2 = NULL) {
	$section = array(
		'tag'   => 'div',
		'id'    => '',
		'class' => array('main-article'),
		'en'    => '',
		'stars' => -1,
			// -1: do not display.
			//  0: 4 grey stars   = not started.
			//  1: 1 red star     = stub.
			//  2: 2 orange stars = half written.
			//  3: 3 yellow stars = 1st draft completed / review / to complete.
			//  4: 4 blue stars  = almost complete.
			//  5: 5 green stars  = complete (but can be further improved if necessary).
	);

	if ($type == 'wikipedia') {
		$section['type']        = 'wikipedia';
		$section['link']['en']  = $p1;
		$section['title']['en'] = $p2;
		$section['class'][]     = 'wikipedia technical-section';
	}
	elseif ($type == 'codeberg') {
		$section['type']        = 'codeberg';
		$section['id']          = $p1;
		$section['title']['en'] = $p2;
		$section['class'][]     = 'codeberg technical-section';
	}

	return $section;
}


function printPageSection($dest) {
	require('templates/keywords.php');
	require('templates/auto_keywords.php');
	global $div_comment_on_codeberg;
	global $div_stars;
	global $preprocess_index_dest;
	global $process_fully;
	$process_fully = false;
	if (!isset($preprocess_index_dest[$dest])) {
		fwrite(STDERR, "Error: page section missing: ${dest} \n");
		return '';
	}

	require($preprocess_index_dest[$dest]['include']);
	$name = $preprocess_index_dest[$dest]['name'];


	if ($name == 'page' && is_object($$name)) {
		$$name->setTitleLink('/' . $dest);
		return $$name->print('preview');
	}
	else if (is_object($$name)) {
		return $$name->print();
	}
	fwrite(STDERR, "[printPageSection] Error: section '${name}' for page '${dest}' is not an object.\n");
	return '';
}

function printSection($section) {
	if (!is_array($section)) {
		fwrite(STDERR, "[printSection] Error: section is not an array: ${section} \n");
		return '';
	}

	$star_classes = array(
		-1 => "no-star-rating",
		0  => "star-rating zero-stars",
		1  => "star-rating one-star",
		2  => "star-rating two-stars",
		3  => "star-rating three-stars",
		4  => "star-rating four-stars",
		5  => "star-rating five-stars"
	);
	if (isset($star_classes[$section['stars']])) {
		$section['class'][] = $star_classes[$section['stars']];
	}


	$class = join(' ', $section['class']);

	$out  = "";
	$out .= "<{$section['tag']} id='{$section['id']}' class='$class'>";
	$out .= "<div lang='en'>" . printSectionStars($section['stars']);

	$out .= "{$section['en']} </div>";
	$out .= "</{$section['tag']}>";

	return $out;
}

function printStars($nb) {
	// fwrite(STDERR, "The function printStars() is deprecated. See ContentSection->printStars().\n");
	$out = '';
	switch ($nb) {
		case 0:
			$out .= "<i class='star outline icon'></i>";
			$out .= "<i class='star outline icon'></i>";
			$out .= "<i class='star outline icon'></i>";
			$out .= "<i class='star outline icon'></i>";
			$out .= "<i class='star outline icon'></i>";
			break;
		case 1:
			$out .= "<i class='star red     icon'></i>";
			$out .= "<i class='star outline icon'></i>";
			$out .= "<i class='star outline icon'></i>";
			$out .= "<i class='star outline icon'></i>";
			$out .= "<i class='star outline icon'></i>";
			break;
		case 2:
			$out .= "<i class='star orange  icon'></i>";
			$out .= "<i class='star orange  icon'></i>";
			$out .= "<i class='star outline icon'></i>";
			$out .= "<i class='star outline icon'></i>";
			$out .= "<i class='star outline icon'></i>";
			break;
		case 3:
			$out .= "<i class='star yellow  icon'></i>";
			$out .= "<i class='star yellow  icon'></i>";
			$out .= "<i class='star yellow  icon'></i>";
			$out .= "<i class='star outline icon'></i>";
			$out .= "<i class='star outline icon'></i>";
			break;
		case 4:
			$out .= "<i class='star blue    icon'></i>";
			$out .= "<i class='star blue    icon'></i>";
			$out .= "<i class='star blue    icon'></i>";
			$out .= "<i class='star blue    icon'></i>";
			$out .= "<i class='star outline icon'></i>";
			break;
		case 5:
			$out .= "<i class='star green   icon'></i>";
			$out .= "<i class='star green   icon'></i>";
			$out .= "<i class='star green   icon'></i>";
			$out .= "<i class='star green   icon'></i>";
			$out .= "<i class='star green   icon'></i>";
			break;
	}
	return $out;
}

function printSectionStars($nb) {
	// fwrite(STDERR, "The function printSectionStars() is deprecated. See ContentSection->printSectionStars().\n");
	$out = '';
	if ($nb >= 0) {
		$out .= "<div class='section-stars'>";
		$out .= printStars($nb);
		$out .= "</div>";
	}
	return $out;
}

function printH2($h2) {
	return printSection($h2);
}

function show_nb_articles_by_stars($div, $date, $zero, $one, $two, $three, $four, $five) {
	$out = "<li><strong>" . formatDateEn($date) . "</strong> article count update: </li> ";
	$out .= <<<HTML
		<p>
		<i class='star outline icon'></i><i class='star outline icon'></i><i class='star outline icon'></i><i class='star outline icon'></i><i class='star outline icon'></i>
		$zero articles
		<br>
		<i class='star red     icon'></i><i class='star outline icon'></i><i class='star outline icon'></i><i class='star outline icon'></i><i class='star outline icon'></i>
		$one articles
		<br>
		<i class='star orange  icon'></i><i class='star orange  icon'></i><i class='star outline icon'></i><i class='star outline icon'></i><i class='star outline icon'></i>
		$two articles
		<br>
		<i class='star yellow  icon'></i><i class='star yellow  icon'></i><i class='star yellow  icon'></i><i class='star outline icon'></i><i class='star outline icon'></i>
		$three articles
		<br>
		<i class='star blue   icon'></i><i class='star blue   icon'></i><i class='star blue   icon'></i><i class='star blue   icon'></i><i class='star outline icon'></i>
		$four articles
		<br>
		<i class='star green   icon'></i><i class='star green   icon'></i><i class='star green   icon'></i><i class='star green   icon'></i><i class='star green icon'></i>
		$five articles
		</p>
		HTML;

	$div->content .= $out;
}

function addUpdate(&$div_update, $date, $stars_from, $stars_to, $link, $title_en, $text_en = '') {
	$update_en  = '<li>';
	$update_en .= "<strong>" . formatDateEn($date) . "</strong> &nbsp; ";
	$update_en .= printStars($stars_from);
	$update_en .= $stars_to >= 0 ? "<i class='right arrow icon'></i>" : '';
	$update_en .= printStars($stars_to);
	$update_en .= empty($link) ? '' : "<a href='$link'>$title_en</a> ";
	$update_en .= $text_en;
	$update_en .= '</li>';
	$div_update->content .= $update_en;
}

function formatDateEn($date) {
	return date("Y F jS", strtotime($date));
}

$div_stars = newSection();
$div_stars['class'][] = 'technical-section';
$div_stars['en'] = <<<HTML
	<p>
	<i class='star outline icon'></i><i class='star outline icon'></i><i class='star outline icon'></i><i class='star outline icon'></i><i class='star outline icon'></i>
	Planned article that has not been published yet.
	<br>
	<i class='star red     icon'></i><i class='star outline icon'></i><i class='star outline icon'></i><i class='star outline icon'></i><i class='star outline icon'></i>
	Stub article, there is not much to read yet.
	<br>
	<i class='star orange  icon'></i><i class='star orange  icon'></i><i class='star outline icon'></i><i class='star outline icon'></i><i class='star outline icon'></i>
	Article in progress. It may be only half written.
	<br>
	<i class='star yellow  icon'></i><i class='star yellow  icon'></i><i class='star yellow  icon'></i><i class='star outline icon'></i><i class='star outline icon'></i>
	Article waiting for reader feedback, or in need of some improvement.
	<br>
	<i class='star blue   icon'></i><i class='star blue   icon'></i><i class='star blue   icon'></i><i class='star blue   icon'></i><i class='star outline icon'></i>
	Nearly completed article. It could still be further expanded and improved.
	<br>
	<i class='star green   icon'></i><i class='star green   icon'></i><i class='star green   icon'></i><i class='star green   icon'></i><i class='star green icon'></i>
	Completed article, albeit still subject to change.
	</p>
	HTML;


$div_comment_on_codeberg = new CodebergContentSection();
$div_comment_on_codeberg->setTitleText("Collaborative work area");
$div_comment_on_codeberg->setTitleLink("https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy/issues");
$div_comment_on_codeberg->content = <<<HTML
	<p>Do you have an idea on how to improve the content of this page?
	Share your thoughts
	at <a href="https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy/issues">Codeberg</a>:
	contribute your insights and research.</p>
	HTML;
